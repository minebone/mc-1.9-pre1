package net.minecraft.server;

import com.google.common.base.Optional;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Blocks;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityProjectile;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Items;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectType;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_afd;
import net.minecraft.server.class_aff;
import net.minecraft.server.class_afg;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_ro;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_yc;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_aab extends EntityProjectile {
   private static final class_ke d = DataWatcher.a(class_yc.class, class_kg.f);
   private static final Logger e = LogManager.getLogger();

   public class_aab(World var1) {
      super(var1);
   }

   public class_aab(World var1, class_rz var2, ItemStack var3) {
      super(var1, var2);
      this.a(var3);
   }

   public class_aab(World var1, double var2, double var4, double var6, ItemStack var8) {
      super(var1, var2, var4, var6);
      if(var8 != null) {
         this.a(var8);
      }

   }

   protected void i() {
      this.Q().a((class_ke)d, (Object)Optional.absent());
   }

   public ItemStack l() {
      ItemStack var1 = (ItemStack)((Optional)this.Q().a(d)).orNull();
      if(var1 == null || var1.b() != Items.bH && var1.b() != Items.bI) {
         if(this.world != null) {
            e.error("ThrownPotion entity " + this.getId() + " has no item?!");
         }

         return new ItemStack(Items.bH);
      } else {
         return var1;
      }
   }

   public void a(ItemStack var1) {
      this.Q().b(d, Optional.fromNullable(var1));
      this.Q().b(d);
   }

   protected float j() {
      return 0.05F;
   }

   protected void a(MovingObjectPosition var1) {
      if(!this.world.E) {
         ItemStack var2 = this.l();
         class_afd var3 = class_aff.c(var2);
         List var4 = class_aff.a(var2);
         Iterator var6;
         if(var1.a == MovingObjectPosition.EnumMovingObjectType.BLOCK && var3 == class_afg.b && var4.isEmpty()) {
            BlockPosition var18 = var1.a().a(var1.b);
            this.a(var18);
            var6 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

            while(var6.hasNext()) {
               EnumDirection var21 = (EnumDirection)var6.next();
               this.a(var18.a(var21));
            }

            this.world.b(2002, new BlockPosition(this), class_afd.a(var3));
            this.S();
         } else {
            if(!var4.isEmpty()) {
               if(this.n()) {
                  class_ro var5 = new class_ro(this.world, this.locX, this.locY, this.locZ);
                  var5.a(this.k());
                  var5.a(3.0F);
                  var5.b(-0.5F);
                  var5.e(10);
                  var5.c(-var5.j() / (float)var5.o());
                  var5.a(var3);
                  var6 = class_aff.b(var2).iterator();

                  while(var6.hasNext()) {
                     MobEffect var7 = (MobEffect)var6.next();
                     var5.a(new MobEffect(var7.a(), var7.b(), var7.c()));
                  }

                  this.world.a((Entity)var5);
               } else {
                  AxisAlignedBB var17 = this.bk().b(4.0D, 2.0D, 4.0D);
                  List var19 = this.world.a(class_rz.class, var17);
                  if(!var19.isEmpty()) {
                     Iterator var20 = var19.iterator();

                     label67:
                     while(true) {
                        class_rz var8;
                        double var9;
                        do {
                           do {
                              if(!var20.hasNext()) {
                                 break label67;
                              }

                              var8 = (class_rz)var20.next();
                           } while(!var8.cC());

                           var9 = this.h(var8);
                        } while(var9 >= 16.0D);

                        double var11 = 1.0D - Math.sqrt(var9) / 4.0D;
                        if(var8 == var1.d) {
                           var11 = 1.0D;
                        }

                        Iterator var13 = var4.iterator();

                        while(var13.hasNext()) {
                           MobEffect var14 = (MobEffect)var13.next();
                           MobEffectType var15 = var14.a();
                           if(var15.b()) {
                              var15.a(this, this.k(), var8, var14.c(), var11);
                           } else {
                              int var16 = (int)(var11 * (double)var14.b() + 0.5D);
                              if(var16 > 20) {
                                 var8.c(new MobEffect(var15, var16, var14.c()));
                              }
                           }
                        }
                     }
                  }
               }
            }

            this.world.b(2002, new BlockPosition(this), class_afd.a(var3));
            this.S();
         }
      }
   }

   private boolean n() {
      return this.l().b() == Items.bI;
   }

   private void a(BlockPosition var1) {
      if(this.world.getType(var1).getBlock() == Blocks.ab) {
         this.world.a((BlockPosition)var1, (IBlockData)Blocks.AIR.u(), 2);
      }

   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      ItemStack var2 = ItemStack.a(var1.o("Potion"));
      if(var2 == null) {
         this.S();
      } else {
         this.a(var2);
      }

   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      ItemStack var2 = this.l();
      if(var2 != null) {
         var1.a((String)"Potion", (NBTTag)var2.b(new NBTTagCompound()));
      }

   }
}
