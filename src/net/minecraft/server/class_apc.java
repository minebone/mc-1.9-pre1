package net.minecraft.server;

import java.util.List;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_apc extends Block {
   public static final class_arm b = class_arm.a("north");
   public static final class_arm c = class_arm.a("east");
   public static final class_arm d = class_arm.a("south");
   public static final class_arm e = class_arm.a("west");
   protected static final AxisAlignedBB[] f = new AxisAlignedBB[]{new AxisAlignedBB(0.4375D, 0.0D, 0.4375D, 0.5625D, 1.0D, 0.5625D), new AxisAlignedBB(0.4375D, 0.0D, 0.4375D, 0.5625D, 1.0D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.4375D, 0.5625D, 1.0D, 0.5625D), new AxisAlignedBB(0.0D, 0.0D, 0.4375D, 0.5625D, 1.0D, 1.0D), new AxisAlignedBB(0.4375D, 0.0D, 0.0D, 0.5625D, 1.0D, 0.5625D), new AxisAlignedBB(0.4375D, 0.0D, 0.0D, 0.5625D, 1.0D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.5625D, 1.0D, 0.5625D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.5625D, 1.0D, 1.0D), new AxisAlignedBB(0.4375D, 0.0D, 0.4375D, 1.0D, 1.0D, 0.5625D), new AxisAlignedBB(0.4375D, 0.0D, 0.4375D, 1.0D, 1.0D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.4375D, 1.0D, 1.0D, 0.5625D), new AxisAlignedBB(0.0D, 0.0D, 0.4375D, 1.0D, 1.0D, 1.0D), new AxisAlignedBB(0.4375D, 0.0D, 0.0D, 1.0D, 1.0D, 0.5625D), new AxisAlignedBB(0.4375D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.5625D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D)};
   private final boolean a;

   protected class_apc(Material var1, boolean var2) {
      super(var1);
      this.w(this.A.b().set(b, Boolean.valueOf(false)).set(c, Boolean.valueOf(false)).set(d, Boolean.valueOf(false)).set(e, Boolean.valueOf(false)));
      this.a = var2;
      this.a(CreativeModeTab.c);
   }

   public void a(IBlockData var1, World var2, BlockPosition var3, AxisAlignedBB var4, List var5, Entity var6) {
      var1 = this.b(var1, var2, var3);
      a(var3, var4, var5, f[0]);
      if(((Boolean)var1.get(b)).booleanValue()) {
         a(var3, var4, var5, f[a(EnumDirection.NORTH)]);
      }

      if(((Boolean)var1.get(d)).booleanValue()) {
         a(var3, var4, var5, f[a(EnumDirection.SOUTH)]);
      }

      if(((Boolean)var1.get(c)).booleanValue()) {
         a(var3, var4, var5, f[a(EnumDirection.EAST)]);
      }

      if(((Boolean)var1.get(e)).booleanValue()) {
         a(var3, var4, var5, f[a(EnumDirection.WEST)]);
      }

   }

   private static int a(EnumDirection var0) {
      return 1 << var0.b();
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      var1 = this.b(var1, var2, var3);
      return f[i(var1)];
   }

   private static int i(IBlockData var0) {
      int var1 = 0;
      if(((Boolean)var0.get(b)).booleanValue()) {
         var1 |= a(EnumDirection.NORTH);
      }

      if(((Boolean)var0.get(c)).booleanValue()) {
         var1 |= a(EnumDirection.EAST);
      }

      if(((Boolean)var0.get(d)).booleanValue()) {
         var1 |= a(EnumDirection.SOUTH);
      }

      if(((Boolean)var0.get(e)).booleanValue()) {
         var1 |= a(EnumDirection.WEST);
      }

      return var1;
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return var1.set(b, Boolean.valueOf(this.c(var2.getType(var3.c()).getBlock()))).set(d, Boolean.valueOf(this.c(var2.getType(var3.d()).getBlock()))).set(e, Boolean.valueOf(this.c(var2.getType(var3.e()).getBlock()))).set(c, Boolean.valueOf(this.c(var2.getType(var3.f()).getBlock())));
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return !this.a?null:super.a(var1, var2, var3);
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public final boolean c(Block var1) {
      return var1.u().h() || var1 == this || var1 == Blocks.w || var1 == Blocks.cG || var1 == Blocks.cH || var1 instanceof class_apc;
   }

   protected boolean o() {
      return true;
   }

   public int e(IBlockData var1) {
      return 0;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      switch(class_apc.SyntheticClass_1.a[var2.ordinal()]) {
      case 1:
         return var1.set(b, var1.get(d)).set(c, var1.get(e)).set(d, var1.get(b)).set(e, var1.get(c));
      case 2:
         return var1.set(b, var1.get(c)).set(c, var1.get(d)).set(d, var1.get(e)).set(e, var1.get(b));
      case 3:
         return var1.set(b, var1.get(e)).set(c, var1.get(b)).set(d, var1.get(c)).set(e, var1.get(d));
      default:
         return var1;
      }
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      switch(class_apc.SyntheticClass_1.b[var2.ordinal()]) {
      case 1:
         return var1.set(b, var1.get(d)).set(d, var1.get(b));
      case 2:
         return var1.set(c, var1.get(e)).set(e, var1.get(c));
      default:
         return super.a(var1, var2);
      }
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{b, c, e, d});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b = new int[class_amq.values().length];

      static {
         try {
            b[class_amq.LEFT_RIGHT.ordinal()] = 1;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            b[class_amq.FRONT_BACK.ordinal()] = 2;
         } catch (NoSuchFieldError var4) {
            ;
         }

         a = new int[class_aod.values().length];

         try {
            a[class_aod.CLOCKWISE_180.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[class_aod.CLOCKWISE_90.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
