package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Blocks;
import net.minecraft.server.Chunk;
import net.minecraft.server.Entity;
import net.minecraft.server.GameProfileSerializer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ata;
import net.minecraft.server.class_atz;
import net.minecraft.server.class_aua;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutTileEntityData;
import net.minecraft.server.class_ky;
import net.minecraft.server.MathHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_aqp extends TileEntity implements class_ky {
   private static final Logger a = LogManager.getLogger();
   private long f = 0L;
   private int g = 0;
   private BlockPosition h;
   private boolean i;

   public void a(NBTTagCompound var1) {
      super.a(var1);
      var1.a("Age", this.f);
      if(this.h != null) {
         var1.a((String)"ExitPortal", (NBTTag)GameProfileSerializer.a(this.h));
      }

      if(this.i) {
         var1.a("ExactTeleport", this.i);
      }

   }

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      this.f = var2.i("Age");
      if(var2.b("ExitPortal", 10)) {
         this.h = GameProfileSerializer.c(var2.o("ExitPortal"));
      }

      this.i = var2.p("ExactTeleport");
   }

   public void c() {
      boolean var1 = this.b();
      boolean var2 = this.d();
      ++this.f;
      if(var2) {
         --this.g;
      } else if(!this.b.E) {
         List var3 = this.b.a(Entity.class, new AxisAlignedBB(this.v()));
         if(!var3.isEmpty()) {
            this.a((Entity)var3.get(0));
         }
      }

      if(var1 != this.b() || var2 != this.d()) {
         this.v_();
      }

   }

   public boolean b() {
      return this.f < 200L;
   }

   public boolean d() {
      return this.g > 0;
   }

   public Packet D_() {
      NBTTagCompound var1 = new NBTTagCompound();
      this.a(var1);
      return new PacketPlayOutTileEntityData(this.c, 8, var1);
   }

   public void h() {
      if(!this.b.E) {
         this.g = 20;
         this.b.c(this.v(), this.w(), 1, 0);
         this.v_();
      }

   }

   public boolean c(int var1, int var2) {
      if(var1 == 1) {
         this.g = 20;
         return true;
      } else {
         return super.c(var1, var2);
      }
   }

   public void a(Entity var1) {
      if(!this.b.E && !this.d()) {
         this.g = 100;
         if(this.h == null && this.b.s instanceof class_ata) {
            this.k();
         }

         if(this.h != null) {
            BlockPosition var2 = this.i?this.h:this.j();
            var1.a((double)var2.p() + 0.5D, (double)var2.q() + 0.5D, (double)var2.r() + 0.5D);
         }

         this.h();
      }
   }

   private BlockPosition j() {
      BlockPosition var1 = a(this.b, this.h, 5, false);
      a.debug("Best exit position for portal at " + this.h + " is " + var1);
      return var1.a();
   }

   private void k() {
      Vec3D var1 = (new Vec3D((double)this.v().p(), 0.0D, (double)this.v().r())).a();
      Vec3D var2 = var1.a(1024.0D);

      int var3;
      for(var3 = 16; a(this.b, var2).g() > 0 && var3-- > 0; var2 = var2.e(var1.a(-16.0D))) {
         a.debug("Skipping backwards past nonempty chunk at " + var2);
      }

      for(var3 = 16; a(this.b, var2).g() == 0 && var3-- > 0; var2 = var2.e(var1.a(16.0D))) {
         a.debug("Skipping forward past empty chunk at " + var2);
      }

      a.debug("Found chunk at " + var2);
      Chunk var4 = a(this.b, var2);
      this.h = a(var4);
      if(this.h == null) {
         this.h = new BlockPosition(var2.b + 0.5D, 75.0D, var2.d + 0.5D);
         a.debug("Failed to find suitable block, settling on " + this.h);
         (new class_aua()).b(this.b, new Random(this.h.g()), this.h);
      } else {
         a.debug("Found block at " + this.h);
      }

      this.h = a(this.b, this.h, 16, true);
      a.debug("Creating portal at " + this.h);
      this.h = this.h.b(10);
      this.b(this.h);
      this.v_();
   }

   private static BlockPosition a(World var0, BlockPosition var1, int var2, boolean var3) {
      BlockPosition var4 = null;

      for(int var5 = -var2; var5 <= var2; ++var5) {
         for(int var6 = -var2; var6 <= var2; ++var6) {
            if(var5 != 0 || var6 != 0 || var3) {
               for(int var7 = 255; var7 > (var4 == null?0:var4.q()); --var7) {
                  BlockPosition var8 = new BlockPosition(var1.p() + var5, var7, var1.r() + var6);
                  IBlockData var9 = var0.getType(var8);
                  if(var9.k() && (var3 || var9.getBlock() != Blocks.h)) {
                     var4 = var8;
                     break;
                  }
               }
            }
         }
      }

      return var4 == null?var1:var4;
   }

   private static Chunk a(World var0, Vec3D var1) {
      return var0.a(MathHelper.c(var1.b / 16.0D), MathHelper.c(var1.d / 16.0D));
   }

   private static BlockPosition a(Chunk var0) {
      BlockPosition var1 = new BlockPosition(var0.posX * 16, 30, var0.posZ * 16);
      int var2 = var0.g() + 16 - 1;
      BlockPosition var3 = new BlockPosition(var0.posX * 16 + 16 - 1, var2, var0.posZ * 16 + 16 - 1);
      BlockPosition var4 = null;
      double var5 = 0.0D;
      Iterator var7 = BlockPosition.a(var1, var3).iterator();

      while(true) {
         BlockPosition var8;
         double var10;
         do {
            do {
               IBlockData var9;
               do {
                  do {
                     if(!var7.hasNext()) {
                        return var4;
                     }

                     var8 = (BlockPosition)var7.next();
                     var9 = var0.a(var8);
                  } while(var9.getBlock() != Blocks.bH);
               } while(var0.a(var8.b(1)).k());
            } while(var0.a(var8.b(2)).k());

            var10 = var8.f(0.0D, 0.0D, 0.0D);
         } while(var4 != null && var10 >= var5);

         var4 = var8;
         var5 = var10;
      }
   }

   private void b(BlockPosition var1) {
      (new class_atz()).b(this.b, new Random(), var1);
      TileEntity var2 = this.b.r(var1);
      if(var2 instanceof class_aqp) {
         class_aqp var3 = (class_aqp)var2;
         var3.h = new BlockPosition(this.v());
         var3.v_();
      } else {
         a.warn("Couldn\'t save exit portal at " + var1);
      }

   }
}
