package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import java.util.Random;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.Entity;
import net.minecraft.server.class_bay;
import net.minecraft.server.class_kk;

public class class_baz implements class_bay {
   private final boolean a;

   public class_baz(boolean var1) {
      this.a = var1;
   }

   public boolean a(Random var1, Entity var2) {
      return var2.aG() == this.a;
   }

   public static class class_a_in_class_baz extends class_bay.class_a_in_class_bay {
      protected class_a_in_class_baz() {
         super(new class_kk("on_fire"), class_baz.class);
      }

      public JsonElement a(class_baz var1, JsonSerializationContext var2) {
         return new JsonPrimitive(Boolean.valueOf(var1.a));
      }

      public class_baz b(JsonElement var1, JsonDeserializationContext var2) {
         return new class_baz(ChatDeserializer.c(var1, "on_fire"));
      }

      // $FF: synthetic method
      public class_bay a(JsonElement var1, JsonDeserializationContext var2) {
         return this.b(var1, var2);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public JsonElement a(class_bay var1, JsonSerializationContext var2) {
         return this.a((class_baz)var1, var2);
      }
   }
}
