package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;

public class CommandSay extends CommandAbstract {
   public String c() {
      return "say";
   }

   public int a() {
      return 1;
   }

   public String b(ICommandListener var1) {
      return "commands.say.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length > 0 && var3[0].length() > 0) {
         IChatBaseComponent var4 = b(var2, var3, 0, true);
         var1.getPlayerList().a((IChatBaseComponent)(new ChatMessage("chat.type.announcement", new Object[]{var2.i_(), var4})));
      } else {
         throw new class_cf("commands.say.usage", new Object[0]);
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length >= 1?a(var3, var1.J()):Collections.emptyList();
   }
}
