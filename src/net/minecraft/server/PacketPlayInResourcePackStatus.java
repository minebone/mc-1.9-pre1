package net.minecraft.server;

import java.io.IOException;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayInListener;

public class PacketPlayInResourcePackStatus implements Packet {
   private String a;
   private PacketPlayInResourcePackStatus.EnumResourcePackStatus b;

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.c(40);
      this.b = (PacketPlayInResourcePackStatus.EnumResourcePackStatus)var1.a(PacketPlayInResourcePackStatus.EnumResourcePackStatus.class);
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.a(this.a);
      var1.a((Enum)this.b);
   }

   public void a(PacketPlayInListener var1) {
      var1.a(this);
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketPlayInListener)var1);
   }

   public static enum EnumResourcePackStatus {
      SUCCESSFULLY_LOADED,
      DECLINED,
      FAILED_DOWNLOAD,
      ACCEPTED;
   }
}
