package net.minecraft.server;

import java.io.File;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.WorldData;
import net.minecraft.server.class_aht;
import net.minecraft.server.class_asl;
import net.minecraft.server.class_asu;
import net.minecraft.server.class_awl;
import net.minecraft.server.class_azp;

public interface class_azh {
   WorldData d();

   void c() throws class_aht;

   class_asl a(class_asu var1);

   void a(WorldData var1, NBTTagCompound var2);

   void a(WorldData var1);

   class_azp e();

   void a();

   File b();

   File a(String var1);

   class_awl h();
}
