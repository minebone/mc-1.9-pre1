package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.class_auc;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public abstract class class_ato extends class_auc {
   public class_ato(boolean var1) {
      super(var1);
   }

   protected boolean a(Block var1) {
      Material var2 = var1.u().getMaterial();
      return var2 == Material.a || var2 == Material.j || var1 == Blocks.c || var1 == Blocks.d || var1 == Blocks.r || var1 == Blocks.s || var1 == Blocks.g || var1 == Blocks.bn;
   }

   public void a(World var1, Random var2, BlockPosition var3) {
   }

   protected void a(World var1, BlockPosition var2) {
      if(var1.getType(var2).getBlock() != Blocks.d) {
         this.a(var1, var2, Blocks.d.u());
      }

   }
}
