package net.minecraft.server;

import com.mojang.authlib.GameProfile;
import java.util.Collections;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;

public class CommandPardon extends CommandAbstract {
   public String c() {
      return "pardon";
   }

   public int a() {
      return 3;
   }

   public String b(ICommandListener var1) {
      return "commands.unban.usage";
   }

   public boolean a(MinecraftServer var1, ICommandListener var2) {
      return var1.getPlayerList().h().b() && super.a(var1, var2);
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length == 1 && var3[0].length() > 0) {
         GameProfile var4 = var1.getPlayerList().h().a(var3[0]);
         if(var4 == null) {
            throw new class_bz("commands.unban.failed", new Object[]{var3[0]});
         } else {
            var1.getPlayerList().h().c(var4);
            a(var2, this, "commands.unban.success", new Object[]{var3[0]});
         }
      } else {
         throw new class_cf("commands.unban.usage", new Object[0]);
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, var1.getPlayerList().h().a()):Collections.emptyList();
   }
}
