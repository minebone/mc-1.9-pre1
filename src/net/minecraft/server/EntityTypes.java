package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityFallingBlock;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EntityLargeFireball;
import net.minecraft.server.EntityMinecartAbstract;
import net.minecraft.server.EntityMinecartChest;
import net.minecraft.server.EntityMinecartFurnace;
import net.minecraft.server.EntityMinecartHopper;
import net.minecraft.server.EntityPainting;
import net.minecraft.server.EntityWitch;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.Statistic;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.class_aaa;
import net.minecraft.server.class_aab;
import net.minecraft.server.class_aac;
import net.minecraft.server.class_aad;
import net.minecraft.server.EntityBoat;
import net.minecraft.server.class_aai;
import net.minecraft.server.class_aam;
import net.minecraft.server.class_aan;
import net.minecraft.server.class_aao;
import net.minecraft.server.class_ro;
import net.minecraft.server.class_rw;
import net.minecraft.server.class_vt;
import net.minecraft.server.class_vw;
import net.minecraft.server.class_vx;
import net.minecraft.server.class_vz;
import net.minecraft.server.class_wa;
import net.minecraft.server.class_wb;
import net.minecraft.server.class_wc;
import net.minecraft.server.class_wd;
import net.minecraft.server.class_we;
import net.minecraft.server.class_wf;
import net.minecraft.server.class_wg;
import net.minecraft.server.class_wi;
import net.minecraft.server.class_wj;
import net.minecraft.server.class_ws;
import net.minecraft.server.class_wt;
import net.minecraft.server.class_xn;
import net.minecraft.server.class_xp;
import net.minecraft.server.class_xr;
import net.minecraft.server.class_xs;
import net.minecraft.server.class_xz;
import net.minecraft.server.class_yc;
import net.minecraft.server.class_yd;
import net.minecraft.server.class_yf;
import net.minecraft.server.class_yg;
import net.minecraft.server.class_yh;
import net.minecraft.server.class_yi;
import net.minecraft.server.class_yj;
import net.minecraft.server.class_yl;
import net.minecraft.server.class_ym;
import net.minecraft.server.class_yn;
import net.minecraft.server.class_yo;
import net.minecraft.server.class_yp;
import net.minecraft.server.class_yq;
import net.minecraft.server.class_yt;
import net.minecraft.server.class_yu;
import net.minecraft.server.class_yv;
import net.minecraft.server.class_yw;
import net.minecraft.server.class_yx;
import net.minecraft.server.class_yz;
import net.minecraft.server.class_zd;
import net.minecraft.server.class_zm;
import net.minecraft.server.class_zn;
import net.minecraft.server.class_zp;
import net.minecraft.server.class_zt;
import net.minecraft.server.class_zu;
import net.minecraft.server.class_zv;
import net.minecraft.server.class_zw;
import net.minecraft.server.class_zy;
import net.minecraft.server.class_zz;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EntityTypes {
   private static final Logger b = LogManager.getLogger();
   private static final Map c = Maps.newHashMap();
   private static final Map d = Maps.newHashMap();
   private static final Map e = Maps.newHashMap();
   private static final Map f = Maps.newHashMap();
   private static final Map g = Maps.newHashMap();
   public static final Map a = Maps.newLinkedHashMap();

   private static void a(Class var0, String var1, int var2) {
      if(c.containsKey(var1)) {
         throw new IllegalArgumentException("ID is already registered: " + var1);
      } else if(e.containsKey(Integer.valueOf(var2))) {
         throw new IllegalArgumentException("ID is already registered: " + var2);
      } else if(var2 == 0) {
         throw new IllegalArgumentException("Cannot register to reserved id: " + var2);
      } else if(var0 == null) {
         throw new IllegalArgumentException("Cannot register null clazz for id: " + var2);
      } else {
         c.put(var1, var0);
         d.put(var0, var1);
         e.put(Integer.valueOf(var2), var0);
         f.put(var0, Integer.valueOf(var2));
         g.put(var1, Integer.valueOf(var2));
      }
   }

   private static void a(Class var0, String var1, int var2, int var3, int var4) {
      a(var0, var1, var2);
      a.put(var1, new EntityTypes.class_a_in_class_rs(var1, var3, var4));
   }

   public static Entity a(String var0, World var1) {
      Entity var2 = null;

      try {
         Class var3 = (Class)c.get(var0);
         if(var3 != null) {
            var2 = (Entity)var3.getConstructor(new Class[]{World.class}).newInstance(new Object[]{var1});
         }
      } catch (Exception var4) {
         var4.printStackTrace();
      }

      return var2;
   }

   public static Entity a(NBTTagCompound var0, World var1) {
      Entity var2 = null;

      try {
         Class var3 = (Class)c.get(var0.l("id"));
         if(var3 != null) {
            var2 = (Entity)var3.getConstructor(new Class[]{World.class}).newInstance(new Object[]{var1});
         }
      } catch (Exception var4) {
         var4.printStackTrace();
      }

      if(var2 != null) {
         var2.f(var0);
      } else {
         b.warn("Skipping Entity with id " + var0.l("id"));
      }

      return var2;
   }

   public static Entity a(int var0, World var1) {
      Entity var2 = null;

      try {
         Class var3 = a(var0);
         if(var3 != null) {
            var2 = (Entity)var3.getConstructor(new Class[]{World.class}).newInstance(new Object[]{var1});
         }
      } catch (Exception var4) {
         var4.printStackTrace();
      }

      if(var2 == null) {
         b.warn("Skipping Entity with id " + var0);
      }

      return var2;
   }

   public static Entity b(String var0, World var1) {
      return a(a(var0), var1);
   }

   public static int a(Entity var0) {
      Integer var1 = (Integer)f.get(var0.getClass());
      return var1 == null?0:var1.intValue();
   }

   public static Class a(int var0) {
      return (Class)e.get(Integer.valueOf(var0));
   }

   public static String b(Entity var0) {
      return a(var0.getClass());
   }

   public static String a(Class var0) {
      return (String)d.get(var0);
   }

   public static int a(String var0) {
      Integer var1 = (Integer)g.get(var0);
      return var1 == null?90:var1.intValue();
   }

   public static void a() {
   }

   public static List b() {
      Set var0 = c.keySet();
      ArrayList var1 = Lists.newArrayList();
      Iterator var2 = var0.iterator();

      while(var2.hasNext()) {
         String var3 = (String)var2.next();
         Class var4 = (Class)c.get(var3);
         if((var4.getModifiers() & 1024) != 1024) {
            var1.add(var3);
         }
      }

      var1.add("LightningBolt");
      return var1;
   }

   public static boolean a(Entity var0, String var1) {
      String var2 = b(var0);
      if(var2 == null && var0 instanceof EntityHuman) {
         var2 = "Player";
      } else if(var2 == null && var0 instanceof class_xz) {
         var2 = "LightningBolt";
      }

      return var1.equals(var2);
   }

   public static boolean b(String var0) {
      return "Player".equals(var0) || b().contains(var0);
   }

   static {
      a(class_yc.class, "Item", 1);
      a(class_rw.class, "XPOrb", 2);
      a(class_ro.class, "AreaEffectCloud", 3);
      a(class_zy.class, "ThrownEgg", 7);
      a(class_xs.class, "LeashKnot", 8);
      a(EntityPainting.class, "Painting", 9);
      a(class_aac.class, "Arrow", 10);
      a(class_zv.class, "Snowball", 11);
      a(EntityLargeFireball.class, "Fireball", 12);
      a(class_zu.class, "SmallFireball", 13);
      a(class_zz.class, "ThrownEnderpearl", 14);
      a(class_zn.class, "EyeOfEnderSignal", 15);
      a(class_aab.class, "ThrownPotion", 16);
      a(class_aaa.class, "ThrownExpBottle", 17);
      a(class_xr.class, "ItemFrame", 18);
      a(class_aad.class, "WitherSkull", 19);
      a(class_yd.class, "PrimedTnt", 20);
      a(EntityFallingBlock.class, "FallingSand", 21);
      a(class_zp.class, "FireworksRocketEntity", 22);
      a(class_zw.class, "SpectralArrow", 24);
      a(class_zt.class, "ShulkerBullet", 25);
      a(class_zm.class, "DragonFireball", 26);
      a(class_xp.class, "ArmorStand", 30);
      a(EntityBoat.class, "Boat", 41);
      a(class_aam.class, EntityMinecartAbstract.EnumMinecartType.RIDEABLE.b(), 42);
      a(EntityMinecartChest.class, EntityMinecartAbstract.EnumMinecartType.CHEST.b(), 43);
      a(EntityMinecartFurnace.class, EntityMinecartAbstract.EnumMinecartType.FURNACE.b(), 44);
      a(class_aao.class, EntityMinecartAbstract.EnumMinecartType.TNT.b(), 45);
      a(EntityMinecartHopper.class, EntityMinecartAbstract.EnumMinecartType.HOPPER.b(), 46);
      a(class_aan.class, EntityMinecartAbstract.EnumMinecartType.SPAWNER.b(), 47);
      a(class_aai.class, EntityMinecartAbstract.EnumMinecartType.COMMAND_BLOCK.b(), 40);
      a(EntityInsentient.class, "Mob", 48);
      a(class_yp.class, "Monster", 49);
      a(class_yh.class, "Creeper", 50, 894731, 0);
      a(class_yv.class, "Skeleton", 51, 12698049, 4802889);
      a(class_yx.class, "Spider", 52, 3419431, 11013646);
      a(class_ym.class, "Giant", 53);
      a(class_yz.class, "Zombie", 54, '꾯', 7969893);
      a(class_yw.class, "Slime", 55, 5349438, 8306542);
      a(class_yl.class, "Ghast", 56, 16382457, 12369084);
      a(class_yq.class, "PigZombie", 57, 15373203, 5009705);
      a(class_yi.class, "Enderman", 58, 1447446, 0);
      a(class_yg.class, "CaveSpider", 59, 803406, 11013646);
      a(class_yu.class, "Silverfish", 60, 7237230, 3158064);
      a(class_yf.class, "Blaze", 61, 16167425, 16775294);
      a(class_yo.class, "LavaSlime", 62, 3407872, 16579584);
      a(class_wt.class, "EnderDragon", 63);
      a(class_xn.class, "WitherBoss", 64);
      a(class_vt.class, "Bat", 65, 4996656, 986895);
      a(EntityWitch.class, "Witch", 66, 3407872, 5349438);
      a(class_yj.class, "Endermite", 67, 1447446, 7237230);
      a(class_yn.class, "Guardian", 68, 5931634, 15826224);
      a(class_yt.class, "Shulker", 69, 9725844, 5060690);
      a(class_wb.class, "Pig", 90, 15771042, 14377823);
      a(class_wd.class, "Sheep", 91, 15198183, 16758197);
      a(class_vx.class, "Cow", 92, 4470310, 10592673);
      a(class_vw.class, "Chicken", 93, 10592673, 16711680);
      a(class_wf.class, "Squid", 94, 2243405, 7375001);
      a(class_wi.class, "Wolf", 95, 14144467, 13545366);
      a(class_vz.class, "MushroomCow", 96, 10489616, 12040119);
      a(class_we.class, "SnowMan", 97);
      a(class_wa.class, "Ozelot", 98, 15720061, 5653556);
      a(class_wg.class, "VillagerGolem", 99);
      a(class_wj.class, "EntityHorse", 100, 12623485, 15656192);
      a(class_wc.class, "Rabbit", 101, 10051392, 7555121);
      a(class_zd.class, "Villager", 120, 5651507, 12422002);
      a(class_ws.class, "EnderCrystal", 200);
   }

   public static class class_a_in_class_rs {
      public final String a;
      public final int b;
      public final int c;
      public final Statistic d;
      public final Statistic e;

      public class_a_in_class_rs(String var1, int var2, int var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = StatisticList.a(this);
         this.e = StatisticList.b(this);
      }
   }
}
