package net.minecraft.server;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BlockPistonExtension;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.class_aqt;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_aru;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_qk;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_st;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_to;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uu;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_vy;
import net.minecraft.server.class_yk;
import net.minecraft.server.class_ys;
import net.minecraft.server.class_zl;
import net.minecraft.server.class_zt;

public class class_yt extends class_vy implements class_yk {
   private static final UUID bv = UUID.fromString("7E0292F2-9434-48D5-A29F-9583AF7DF27F");
   private static final AttributeModifier bw = (new AttributeModifier(bv, "Covered armor bonus", 20.0D, 0)).a(false);
   protected static final class_ke a = DataWatcher.a(class_yt.class, class_kg.l);
   protected static final class_ke b = DataWatcher.a(class_yt.class, class_kg.k);
   protected static final class_ke c = DataWatcher.a(class_yt.class, class_kg.a);
   private float bx;
   private float by;
   private BlockPosition bz;
   private int bA;

   public class_yt(World var1) {
      super(var1);
      this.a(1.0F, 1.0F);
      this.aN = 180.0F;
      this.aM = 180.0F;
      this.fireProof = true;
      this.bz = null;
      this.b_ = 5;
   }

   public class_sc a(class_qk var1, class_sc var2) {
      this.aM = 180.0F;
      this.aN = 180.0F;
      this.yaw = 180.0F;
      this.lastYaw = 180.0F;
      this.aO = 180.0F;
      this.aP = 180.0F;
      return super.a(var1, var2);
   }

   protected void r() {
      this.bp.a(1, new class_to(this, EntityHuman.class, 8.0F));
      this.bp.a(4, new class_yt.class_a_in_class_yt());
      this.bp.a(7, new class_yt.class_e_in_class_yt((class_yt.SyntheticClass_1)null));
      this.bp.a(8, new class_ue(this));
      this.bq.a(1, new class_uu(this, true, new Class[0]));
      this.bq.a(2, new class_yt.class_d_in_class_yt(this));
      this.bq.a(3, new class_yt.class_c_in_class_yt(this));
   }

   protected boolean ad() {
      return false;
   }

   public EnumSoundCategory by() {
      return EnumSoundCategory.HOSTILE;
   }

   protected class_nf G() {
      return class_ng.eO;
   }

   public void D() {
      if(!this.df()) {
         super.D();
      }

   }

   protected class_nf bR() {
      return class_ng.eS;
   }

   protected class_nf bQ() {
      return this.df()?class_ng.eU:class_ng.eT;
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)a, (Object)EnumDirection.DOWN);
      this.datawatcher.a((class_ke)b, (Object)Optional.absent());
      this.datawatcher.a((class_ke)c, (Object)Byte.valueOf((byte)0));
   }

   protected void bz() {
      super.bz();
      this.a((class_sk)class_ys.a).a(30.0D);
   }

   protected class_st s() {
      return new class_yt.class_b_in_class_yt(this);
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.datawatcher.b(a, EnumDirection.a(var1.f("AttachFace")));
      this.datawatcher.b(c, Byte.valueOf(var1.f("Peek")));
      if(var1.e("APX")) {
         int var2 = var1.h("APX");
         int var3 = var1.h("APY");
         int var4 = var1.h("APZ");
         this.datawatcher.b(b, Optional.of(new BlockPosition(var2, var3, var4)));
      } else {
         this.datawatcher.b(b, Optional.absent());
      }

   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("AttachFace", (byte)((EnumDirection)this.datawatcher.a(a)).a());
      var1.a("Peek", ((Byte)this.datawatcher.a(c)).byteValue());
      BlockPosition var2 = this.da();
      if(var2 != null) {
         var1.a("APX", var2.p());
         var1.a("APY", var2.q());
         var1.a("APZ", var2.r());
      }

   }

   public void m() {
      super.m();
      BlockPosition var1 = (BlockPosition)((Optional)this.datawatcher.a(b)).orNull();
      if(var1 == null && !this.world.E) {
         var1 = new BlockPosition(this);
         this.datawatcher.b(b, Optional.of(var1));
      }

      float var2;
      if(this.aH()) {
         var1 = null;
         var2 = this.bx().yaw;
         this.yaw = var2;
         this.aM = var2;
         this.aN = var2;
         this.bA = 0;
      } else if(!this.world.E) {
         IBlockData var19 = this.world.getType(var1);
         if(var19.getBlock() != Blocks.AIR) {
            EnumDirection var3;
            if(var19.getBlock() == Blocks.M) {
               var3 = (EnumDirection)var19.get(class_aqt.H);
               var1 = var1.a(var3);
               this.datawatcher.b(b, Optional.of(var1));
            } else if(var19.getBlock() == Blocks.K) {
               var3 = (EnumDirection)var19.get(BlockPistonExtension.H);
               var1 = var1.a(var3);
               this.datawatcher.b(b, Optional.of(var1));
            } else {
               this.o();
            }
         }

         BlockPosition var20 = var1.a(this.cZ());
         if(!this.world.d(var20, false)) {
            boolean var4 = false;
            EnumDirection[] var5 = EnumDirection.values();
            int var6 = var5.length;

            for(int var7 = 0; var7 < var6; ++var7) {
               EnumDirection var8 = var5[var7];
               var20 = var1.a(var8);
               if(this.world.d(var20, false)) {
                  this.datawatcher.b(a, var8);
                  var4 = true;
                  break;
               }
            }

            if(!var4) {
               this.o();
            }
         }

         BlockPosition var22 = var1.a(this.cZ().d());
         if(this.world.d(var22, false)) {
            this.o();
         }
      }

      var2 = (float)this.db() * 0.01F;
      this.bx = this.by;
      if(this.by > var2) {
         this.by = MathHelper.a(this.by - 0.05F, var2, 1.0F);
      } else if(this.by < var2) {
         this.by = MathHelper.a(this.by + 0.05F, 0.0F, var2);
      }

      if(var1 != null) {
         if(this.world.E) {
            if(this.bA > 0 && this.bz != null) {
               --this.bA;
            } else {
               this.bz = var1;
            }
         }

         this.M = this.lastX = this.locX = (double)var1.p() + 0.5D;
         this.N = this.lastY = this.locY = (double)var1.q();
         this.O = this.lastZ = this.locZ = (double)var1.r() + 0.5D;
         double var21 = 0.5D - (double)MathHelper.a((0.5F + this.by) * 3.1415927F) * 0.5D;
         double var23 = 0.5D - (double)MathHelper.a((0.5F + this.bx) * 3.1415927F) * 0.5D;
         double var24 = var21 - var23;
         double var9 = 0.0D;
         double var11 = 0.0D;
         double var13 = 0.0D;
         EnumDirection var15 = this.cZ();
         switch(class_yt.SyntheticClass_1.a[var15.ordinal()]) {
         case 1:
         default:
            this.a((AxisAlignedBB)(new AxisAlignedBB(this.locX - 0.5D, this.locY, this.locZ - 0.5D, this.locX + 0.5D, this.locY + 1.0D + var21, this.locZ + 0.5D)));
            var11 = var24;
            break;
         case 2:
            this.a((AxisAlignedBB)(new AxisAlignedBB(this.locX - 0.5D, this.locY - var21, this.locZ - 0.5D, this.locX + 0.5D, this.locY + 1.0D, this.locZ + 0.5D)));
            var11 = -var24;
            break;
         case 3:
            this.a((AxisAlignedBB)(new AxisAlignedBB(this.locX - 0.5D, this.locY, this.locZ - 0.5D, this.locX + 0.5D, this.locY + 1.0D, this.locZ + 0.5D + var21)));
            var13 = var24;
            break;
         case 4:
            this.a((AxisAlignedBB)(new AxisAlignedBB(this.locX - 0.5D, this.locY, this.locZ - 0.5D - var21, this.locX + 0.5D, this.locY + 1.0D, this.locZ + 0.5D)));
            var13 = -var24;
            break;
         case 5:
            this.a((AxisAlignedBB)(new AxisAlignedBB(this.locX - 0.5D, this.locY, this.locZ - 0.5D, this.locX + 0.5D + var21, this.locY + 1.0D, this.locZ + 0.5D)));
            var9 = var24;
            break;
         case 6:
            this.a((AxisAlignedBB)(new AxisAlignedBB(this.locX - 0.5D - var21, this.locY, this.locZ - 0.5D, this.locX + 0.5D, this.locY + 1.0D, this.locZ + 0.5D)));
            var9 = -var24;
         }

         if(var24 > 0.0D) {
            List var16 = this.world.b((Entity)this, (AxisAlignedBB)this.bk());
            if(!var16.isEmpty()) {
               Iterator var17 = var16.iterator();

               while(var17.hasNext()) {
                  Entity var18 = (Entity)var17.next();
                  if(!(var18 instanceof class_yt) && !var18.noClip) {
                     var18.d(var9, var11, var13);
                  }
               }
            }
         }
      }

   }

   public void b(double var1, double var3, double var5) {
      super.b(var1, var3, var5);
      if(this.datawatcher != null && this.ticksLived != 0) {
         Optional var7 = (Optional)this.datawatcher.a(b);
         Optional var8 = Optional.of(new BlockPosition(var1, var3, var5));
         if(!var8.equals(var7)) {
            this.datawatcher.b(b, var8);
            this.datawatcher.b(c, Byte.valueOf((byte)0));
            this.ai = true;
         }

      }
   }

   protected boolean o() {
      if(!this.cR() && this.at()) {
         BlockPosition var1 = new BlockPosition(this);

         for(int var2 = 0; var2 < 5; ++var2) {
            BlockPosition var3 = var1.a(8 - this.random.nextInt(17), 8 - this.random.nextInt(17), 8 - this.random.nextInt(17));
            if(var3.q() > 0 && this.world.d(var3) && this.world.a((class_aru)this.world.aj(), (Entity)this) && this.world.a((Entity)this, (AxisAlignedBB)(new AxisAlignedBB(var3))).isEmpty()) {
               boolean var4 = false;
               EnumDirection[] var5 = EnumDirection.values();
               int var6 = var5.length;

               for(int var7 = 0; var7 < var6; ++var7) {
                  EnumDirection var8 = var5[var7];
                  if(this.world.d(var3.a(var8), false)) {
                     this.datawatcher.b(a, var8);
                     var4 = true;
                     break;
                  }
               }

               if(var4) {
                  this.a(class_ng.eX, 1.0F, 1.0F);
                  this.datawatcher.b(b, Optional.of(var3));
                  this.datawatcher.b(c, Byte.valueOf((byte)0));
                  this.c((class_rz)null);
                  return true;
               }
            }
         }

         return false;
      } else {
         return true;
      }
   }

   public void n() {
      super.n();
      this.motX = 0.0D;
      this.motY = 0.0D;
      this.motZ = 0.0D;
      this.aN = 180.0F;
      this.aM = 180.0F;
      this.yaw = 180.0F;
   }

   public void a(class_ke var1) {
      if(b.equals(var1) && this.world.E && !this.aH()) {
         BlockPosition var2 = this.da();
         if(var2 != null) {
            if(this.bz == null) {
               this.bz = var2;
            } else {
               this.bA = 6;
            }

            this.M = this.lastX = this.locX = (double)var2.p() + 0.5D;
            this.N = this.lastY = this.locY = (double)var2.q();
            this.O = this.lastZ = this.locZ = (double)var2.r() + 0.5D;
         }
      }

      super.a(var1);
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.df()) {
         Entity var3 = var1.i();
         if(var3 instanceof class_zl) {
            return false;
         }
      }

      if(super.a(var1, var2)) {
         if((double)this.bP() < (double)this.bV() * 0.5D && this.random.nextInt(4) == 0) {
            this.o();
         }

         return true;
      } else {
         return false;
      }
   }

   private boolean df() {
      return this.db() == 0;
   }

   public AxisAlignedBB ae() {
      return this.at()?this.bk():null;
   }

   public EnumDirection cZ() {
      return (EnumDirection)this.datawatcher.a(a);
   }

   public BlockPosition da() {
      return (BlockPosition)((Optional)this.datawatcher.a(b)).orNull();
   }

   public void g(BlockPosition var1) {
      this.datawatcher.b(b, Optional.fromNullable(var1));
   }

   public int db() {
      return ((Byte)this.datawatcher.a(c)).byteValue();
   }

   public void a(int var1) {
      if(!this.world.E) {
         this.a((class_sk)class_ys.g).c(bw);
         if(var1 == 0) {
            this.a((class_sk)class_ys.g).b(bw);
            this.a(class_ng.eR, 1.0F, 1.0F);
         } else {
            this.a(class_ng.eV, 1.0F, 1.0F);
         }
      }

      this.datawatcher.b(c, Byte.valueOf((byte)var1));
   }

   public float bm() {
      return 0.5F;
   }

   public int cD() {
      return 180;
   }

   public int cE() {
      return 180;
   }

   public void i(Entity var1) {
   }

   public float az() {
      return 0.0F;
   }

   protected class_kk J() {
      return class_azs.x;
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumDirection.values().length];

      static {
         try {
            a[EnumDirection.DOWN.ordinal()] = 1;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[EnumDirection.UP.ordinal()] = 2;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[EnumDirection.NORTH.ordinal()] = 3;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.SOUTH.ordinal()] = 4;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 5;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.EAST.ordinal()] = 6;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   static class class_c_in_class_yt extends class_ux {
      public class_c_in_class_yt(class_yt var1) {
         super(var1, class_rz.class, 10, true, false, new Predicate() {
            public boolean a(class_rz var1) {
               return var1 instanceof class_yk;
            }

            // $FF: synthetic method
            public boolean apply(Object var1) {
               return this.a((class_rz)var1);
            }
         });
      }

      public boolean a() {
         return this.e.aN() == null?false:super.a();
      }

      protected AxisAlignedBB a(double var1) {
         EnumDirection var3 = ((class_yt)this.e).cZ();
         return var3.k() == EnumDirection.class_a_in_class_cq.X?this.e.bk().b(4.0D, var1, var1):(var3.k() == EnumDirection.class_a_in_class_cq.Z?this.e.bk().b(var1, var1, 4.0D):this.e.bk().b(var1, 4.0D, var1));
      }
   }

   class class_d_in_class_yt extends class_ux {
      public class_d_in_class_yt(class_yt var2) {
         super(var2, EntityHuman.class, true);
      }

      public boolean a() {
         return class_yt.this.world.ae() == EnumDifficulty.PEACEFUL?false:super.a();
      }

      protected AxisAlignedBB a(double var1) {
         EnumDirection var3 = ((class_yt)this.e).cZ();
         return var3.k() == EnumDirection.class_a_in_class_cq.X?this.e.bk().b(4.0D, var1, var1):(var3.k() == EnumDirection.class_a_in_class_cq.Z?this.e.bk().b(var1, var1, 4.0D):this.e.bk().b(var1, 4.0D, var1));
      }
   }

   class class_a_in_class_yt extends class_tj {
      private int b;

      public class_a_in_class_yt() {
         this.a(3);
      }

      public boolean a() {
         class_rz var1 = class_yt.this.A();
         return var1 != null && var1.at()?class_yt.this.world.ae() != EnumDifficulty.PEACEFUL:false;
      }

      public void c() {
         this.b = 20;
         class_yt.this.a(100);
      }

      public void d() {
         class_yt.this.a(0);
      }

      public void e() {
         if(class_yt.this.world.ae() != EnumDifficulty.PEACEFUL) {
            --this.b;
            class_rz var1 = class_yt.this.A();
            class_yt.this.t().a(var1, 180.0F, 180.0F);
            double var2 = class_yt.this.h(var1);
            if(var2 < 400.0D) {
               if(this.b <= 0) {
                  this.b = 20 + class_yt.this.random.nextInt(10) * 20 / 2;
                  class_zt var4 = new class_zt(class_yt.this.world, class_yt.this, var1, class_yt.this.cZ().k());
                  class_yt.this.world.a((Entity)var4);
                  class_yt.this.a(class_ng.eW, 2.0F, (class_yt.this.random.nextFloat() - class_yt.this.random.nextFloat()) * 0.2F + 1.0F);
               }
            } else {
               class_yt.this.c((class_rz)null);
            }

            super.e();
         }
      }
   }

   class class_e_in_class_yt extends class_tj {
      private int b;

      private class_e_in_class_yt() {
      }

      public boolean a() {
         return class_yt.this.A() == null && class_yt.this.random.nextInt(40) == 0;
      }

      public boolean b() {
         return class_yt.this.A() == null && this.b > 0;
      }

      public void c() {
         this.b = 20 * (1 + class_yt.this.random.nextInt(3));
         class_yt.this.a(30);
      }

      public void d() {
         if(class_yt.this.A() == null) {
            class_yt.this.a(0);
         }

      }

      public void e() {
         --this.b;
      }

      // $FF: synthetic method
      class_e_in_class_yt(class_yt.SyntheticClass_1 var2) {
         this();
      }
   }

   class class_b_in_class_yt extends class_st {
      public class_b_in_class_yt(class_rz var2) {
         super(var2);
      }

      public void a() {
      }
   }
}
