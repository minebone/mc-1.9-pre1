package net.minecraft.server;

import com.google.common.collect.Sets;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.util.Set;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_azx;
import net.minecraft.server.class_baa;

public class class_azy {
   private final float a;
   private final WorldServer b;
   private final class_baa c;
   private final Entity d;
   private final EntityHuman e;
   private final DamageSource f;
   private final Set g = Sets.newLinkedHashSet();

   public class_azy(float var1, WorldServer var2, class_baa var3, Entity var4, EntityHuman var5, DamageSource var6) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
      this.f = var6;
   }

   public Entity a() {
      return this.d;
   }

   public Entity b() {
      return this.e;
   }

   public Entity c() {
      return this.f == null?null:this.f.j();
   }

   public boolean a(class_azx var1) {
      return this.g.add(var1);
   }

   public void b(class_azx var1) {
      this.g.remove(var1);
   }

   public class_baa e() {
      return this.c;
   }

   public float f() {
      return this.a;
   }

   public Entity a(class_azy.class_b_in_class_azy var1) {
      switch(class_azy.SyntheticClass_1.a[var1.ordinal()]) {
      case 1:
         return this.a();
      case 2:
         return this.c();
      case 3:
         return this.b();
      default:
         return null;
      }
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[class_azy.class_b_in_class_azy.values().length];

      static {
         try {
            a[class_azy.class_b_in_class_azy.THIS.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[class_azy.class_b_in_class_azy.KILLER.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[class_azy.class_b_in_class_azy.KILLER_PLAYER.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum class_b_in_class_azy {
      THIS("this"),
      KILLER("killer"),
      KILLER_PLAYER("killer_player");

      private final String d;

      private class_b_in_class_azy(String var3) {
         this.d = var3;
      }

      public static class_azy.class_b_in_class_azy a(String var0) {
         class_azy.class_b_in_class_azy[] var1 = values();
         int var2 = var1.length;

         for(int var3 = 0; var3 < var2; ++var3) {
            class_azy.class_b_in_class_azy var4 = var1[var3];
            if(var4.d.equals(var0)) {
               return var4;
            }
         }

         throw new IllegalArgumentException("Invalid entity target " + var0);
      }

      public static class class_b_in_class_azy$class_a_in_class_b_in_class_azy extends TypeAdapter {
         public void a(JsonWriter var1, class_azy.class_b_in_class_azy var2) throws IOException {
            var1.value(var2.d);
         }

         public class_azy.class_b_in_class_azy a(JsonReader var1) throws IOException {
            return class_azy.class_b_in_class_azy.a(var1.nextString());
         }

         // $FF: synthetic method
         public Object read(JsonReader var1) throws IOException {
            return this.a(var1);
         }

         // $FF: synthetic method
         public void write(JsonWriter var1, Object var2) throws IOException {
            this.a(var1, (class_azy.class_b_in_class_azy)var2);
         }
      }
   }

   public static class class_a_in_class_azy {
      private final WorldServer a;
      private float b;
      private Entity c;
      private EntityHuman d;
      private DamageSource e;

      public class_a_in_class_azy(WorldServer var1) {
         this.a = var1;
      }

      public class_azy.class_a_in_class_azy a(float var1) {
         this.b = var1;
         return this;
      }

      public class_azy.class_a_in_class_azy a(Entity var1) {
         this.c = var1;
         return this;
      }

      public class_azy.class_a_in_class_azy a(EntityHuman var1) {
         this.d = var1;
         return this;
      }

      public class_azy.class_a_in_class_azy a(DamageSource var1) {
         this.e = var1;
         return this;
      }

      public class_azy a() {
         return new class_azy(this.b, this.a, this.a.ak(), this.c, this.d, this.e);
      }
   }
}
