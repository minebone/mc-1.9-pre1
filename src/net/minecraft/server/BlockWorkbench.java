package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.Container;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.class_abc;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_qm;

public class BlockWorkbench extends Block {
   protected BlockWorkbench() {
      super(Material.d);
      this.a(CreativeModeTab.c);
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(var1.E) {
         return true;
      } else {
         var4.a((class_qm)(new BlockWorkbench.TileEntityContainerWorkbench(var1, var2)));
         var4.b(StatisticList.ab);
         return true;
      }
   }

   public static class TileEntityContainerWorkbench implements class_qm {
      private final World a;
      private final BlockPosition b;

      public TileEntityContainerWorkbench(World var1, BlockPosition var2) {
         this.a = var1;
         this.b = var2;
      }

      public String h_() {
         return null;
      }

      public boolean o_() {
         return false;
      }

      public IChatBaseComponent i_() {
         return new ChatMessage(Blocks.ai.a() + ".name", new Object[0]);
      }

      public Container a(PlayerInventory var1, EntityHuman var2) {
         return new class_abc(var1, this.a, this.b);
      }

      public String k() {
         return "minecraft:crafting_table";
      }
   }
}
