package net.minecraft.server;

import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_auc;
import net.minecraft.server.BlockPosition;

public class class_atr extends class_auc {
   private final Block a;
   private final int b;

   public class_atr(Block var1, int var2) {
      super(false);
      this.a = var1;
      this.b = var2;
   }

   public boolean b(World var1, Random var2, BlockPosition var3) {
      while(true) {
         label50: {
            if(var3.q() > 3) {
               if(var1.d(var3.b())) {
                  break label50;
               }

               Block var4 = var1.getType(var3.b()).getBlock();
               if(var4 != Blocks.c && var4 != Blocks.d && var4 != Blocks.b) {
                  break label50;
               }
            }

            if(var3.q() <= 3) {
               return false;
            }

            int var12 = this.b;

            for(int var5 = 0; var12 >= 0 && var5 < 3; ++var5) {
               int var6 = var12 + var2.nextInt(2);
               int var7 = var12 + var2.nextInt(2);
               int var8 = var12 + var2.nextInt(2);
               float var9 = (float)(var6 + var7 + var8) * 0.333F + 0.5F;
               Iterator var10 = BlockPosition.a(var3.a(-var6, -var7, -var8), var3.a(var6, var7, var8)).iterator();

               while(var10.hasNext()) {
                  BlockPosition var11 = (BlockPosition)var10.next();
                  if(var11.k(var3) <= (double)(var9 * var9)) {
                     var1.a((BlockPosition)var11, (IBlockData)this.a.u(), 4);
                  }
               }

               var3 = var3.a(-(var12 + 1) + var2.nextInt(2 + var12 * 2), 0 - var2.nextInt(2), -(var12 + 1) + var2.nextInt(2 + var12 * 2));
            }

            return true;
         }

         var3 = var3.b();
      }
   }
}
