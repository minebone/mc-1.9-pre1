package net.minecraft.server;

import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aju;
import net.minecraft.server.class_ajx;
import net.minecraft.server.class_akm;
import net.minecraft.server.class_ape;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arn;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;

public class class_aov extends class_ajx implements class_aju {
   public static final BlockStateInteger a = BlockStateInteger.a("age", 0, 7);
   public static final class_arn c = class_ape.a;
   private final Block e;
   protected static final AxisAlignedBB[] d = new AxisAlignedBB[]{new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 0.125D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 0.25D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 0.375D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 0.5D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 0.625D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 0.75D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 0.875D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 1.0D, 0.625D)};

   protected class_aov(Block var1) {
      this.w(this.A.b().set(a, Integer.valueOf(0)).set(c, EnumDirection.UP));
      this.e = var1;
      this.a(true);
      this.a((CreativeModeTab)null);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return d[((Integer)var1.get(a)).intValue()];
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      int var4 = ((Integer)var1.get(a)).intValue();
      var1 = var1.set(c, EnumDirection.UP);
      Iterator var5 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

      while(var5.hasNext()) {
         EnumDirection var6 = (EnumDirection)var5.next();
         if(var2.getType(var3.a(var6)).getBlock() == this.e && var4 == 7) {
            var1 = var1.set(c, var6);
            break;
         }
      }

      return var1;
   }

   protected boolean i(IBlockData var1) {
      return var1.getBlock() == Blocks.ak;
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      super.b(var1, var2, var3, var4);
      if(var1.k(var2.a()) >= 9) {
         float var5 = class_akm.a((Block)this, (World)var1, (BlockPosition)var2);
         if(var4.nextInt((int)(25.0F / var5) + 1) == 0) {
            int var6 = ((Integer)var3.get(a)).intValue();
            if(var6 < 7) {
               var3 = var3.set(a, Integer.valueOf(var6 + 1));
               var1.a((BlockPosition)var2, (IBlockData)var3, 2);
            } else {
               Iterator var7 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

               while(var7.hasNext()) {
                  EnumDirection var8 = (EnumDirection)var7.next();
                  if(var1.getType(var2.a(var8)).getBlock() == this.e) {
                     return;
                  }
               }

               var2 = var2.a(EnumDirection.EnumDirectionLimit.HORIZONTAL.a(var4));
               Block var9 = var1.getType(var2.b()).getBlock();
               if(var1.getType(var2).getBlock().x == Material.a && (var9 == Blocks.ak || var9 == Blocks.d || var9 == Blocks.c)) {
                  var1.a(var2, this.e.u());
               }
            }
         }

      }
   }

   public void g(World var1, BlockPosition var2, IBlockData var3) {
      int var4 = ((Integer)var3.get(a)).intValue() + MathHelper.a((Random)var1.r, 2, 5);
      var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Integer.valueOf(Math.min(7, var4))), 2);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, float var4, int var5) {
      super.a(var1, var2, var3, var4, var5);
      if(!var1.E) {
         Item var6 = this.e();
         if(var6 != null) {
            int var7 = ((Integer)var3.get(a)).intValue();

            for(int var8 = 0; var8 < 3; ++var8) {
               if(var1.r.nextInt(15) <= var7) {
                  a((World)var1, (BlockPosition)var2, (ItemStack)(new ItemStack(var6)));
               }
            }

         }
      }
   }

   protected Item e() {
      return this.e == Blocks.aU?Items.bn:(this.e == Blocks.bk?Items.bo:null);
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return null;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      Item var4 = this.e();
      return var4 == null?null:new ItemStack(var4);
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, boolean var4) {
      return ((Integer)var3.get(a)).intValue() != 7;
   }

   public boolean a(World var1, Random var2, BlockPosition var3, IBlockData var4) {
      return true;
   }

   public void b(World var1, Random var2, BlockPosition var3, IBlockData var4) {
      this.g(var1, var3, var4);
   }

   public IBlockData a(int var1) {
      return this.u().set(a, Integer.valueOf(var1));
   }

   public int e(IBlockData var1) {
      return ((Integer)var1.get(a)).intValue();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, c});
   }
}
