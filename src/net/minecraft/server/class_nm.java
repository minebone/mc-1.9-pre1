package net.minecraft.server;

import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.Statistic;
import net.minecraft.server.StatisticList;
import net.minecraft.server.class_nq;

public class class_nm extends Statistic {
   public class_nm(String var1, IChatBaseComponent var2, class_nq var3) {
      super(var1, var2, var3);
   }

   public class_nm(String var1, IChatBaseComponent var2) {
      super(var1, var2);
   }

   public Statistic h() {
      super.h();
      StatisticList.c.add(this);
      return this;
   }
}
