package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.TileEntity;
import net.minecraft.server.class_aay;
import net.minecraft.server.class_aaz;
import net.minecraft.server.class_abs;
import net.minecraft.server.ItemStack;
import net.minecraft.server.MathHelper;
import net.minecraft.server.IInventory;

public abstract class Container {
   public List b = Lists.newArrayList();
   public List c = Lists.newArrayList();
   public int d;
   private int f = -1;
   private int g;
   private final Set h = Sets.newHashSet();
   protected List e = Lists.newArrayList();
   private Set i = Sets.newHashSet();

   protected class_abs a(class_abs var1) {
      var1.e = this.c.size();
      this.c.add(var1);
      this.b.add((Object)null);
      return var1;
   }

   public void a(class_aaz var1) {
      if(this.e.contains(var1)) {
         throw new IllegalArgumentException("Listener already listening");
      } else {
         this.e.add(var1);
         var1.a(this, this.a());
         this.b();
      }
   }

   public List a() {
      ArrayList var1 = Lists.newArrayList();

      for(int var2 = 0; var2 < this.c.size(); ++var2) {
         var1.add(((class_abs)this.c.get(var2)).d());
      }

      return var1;
   }

   public void b() {
      for(int var1 = 0; var1 < this.c.size(); ++var1) {
         ItemStack var2 = ((class_abs)this.c.get(var1)).d();
         ItemStack var3 = (ItemStack)this.b.get(var1);
         if(!ItemStack.b(var3, var2)) {
            var3 = var2 == null?null:var2.k();
            this.b.set(var1, var3);

            for(int var4 = 0; var4 < this.e.size(); ++var4) {
               ((class_aaz)this.e.get(var4)).a(this, var1, var3);
            }
         }
      }

   }

   public boolean a(EntityHuman var1, int var2) {
      return false;
   }

   public class_abs a(IInventory var1, int var2) {
      for(int var3 = 0; var3 < this.c.size(); ++var3) {
         class_abs var4 = (class_abs)this.c.get(var3);
         if(var4.a(var1, var2)) {
            return var4;
         }
      }

      return null;
   }

   public class_abs a(int var1) {
      return (class_abs)this.c.get(var1);
   }

   public ItemStack b(EntityHuman var1, int var2) {
      class_abs var3 = (class_abs)this.c.get(var2);
      return var3 != null?var3.d():null;
   }

   public ItemStack a(int var1, int var2, class_aay var3, EntityHuman var4) {
      ItemStack var5 = null;
      PlayerInventory var6 = var4.br;
      int var9;
      ItemStack var17;
      if(var3 == class_aay.QUICK_CRAFT) {
         int var7 = this.g;
         this.g = c(var2);
         if((var7 != 1 || this.g != 2) && var7 != this.g) {
            this.d();
         } else if(var6.o() == null) {
            this.d();
         } else if(this.g == 0) {
            this.f = b(var2);
            if(a(this.f, var4)) {
               this.g = 1;
               this.h.clear();
            } else {
               this.d();
            }
         } else if(this.g == 1) {
            class_abs var8 = (class_abs)this.c.get(var1);
            if(var8 != null && a(var8, var6.o(), true) && var8.a(var6.o()) && var6.o().b > this.h.size() && this.b(var8)) {
               this.h.add(var8);
            }
         } else if(this.g == 2) {
            if(!this.h.isEmpty()) {
               var17 = var6.o().k();
               var9 = var6.o().b;
               Iterator var10 = this.h.iterator();

               while(var10.hasNext()) {
                  class_abs var11 = (class_abs)var10.next();
                  if(var11 != null && a(var11, var6.o(), true) && var11.a(var6.o()) && var6.o().b >= this.h.size() && this.b(var11)) {
                     ItemStack var12 = var17.k();
                     int var13 = var11.e()?var11.d().b:0;
                     a(this.h, this.f, var12, var13);
                     if(var12.b > var12.c()) {
                        var12.b = var12.c();
                     }

                     if(var12.b > var11.b(var12)) {
                        var12.b = var11.b(var12);
                     }

                     var9 -= var12.b - var13;
                     var11.d(var12);
                  }
               }

               var17.b = var9;
               if(var17.b <= 0) {
                  var17 = null;
               }

               var6.e(var17);
            }

            this.d();
         } else {
            this.d();
         }
      } else if(this.g != 0) {
         this.d();
      } else {
         class_abs var16;
         ItemStack var18;
         int var20;
         if(var3 != class_aay.PICKUP && var3 != class_aay.QUICK_MOVE || var2 != 0 && var2 != 1) {
            if(var3 == class_aay.SWAP && var2 >= 0 && var2 < 9) {
               var16 = (class_abs)this.c.get(var1);
               var17 = var6.a(var2);
               if(var17 != null && var17.b <= 0) {
                  var17 = null;
                  var6.a(var2, (ItemStack)null);
               }

               var18 = var16.d();
               if(var17 != null || var18 != null) {
                  if(var17 == null) {
                     if(var16.a(var4)) {
                        var6.a(var2, var18);
                        var16.d((ItemStack)null);
                        var16.a(var4, var18);
                     }
                  } else if(var18 == null) {
                     if(var16.a(var17)) {
                        var20 = var16.b(var17);
                        if(var17.b > var20) {
                           var16.d(var17.a(var20));
                        } else {
                           var16.d(var17);
                           var6.a(var2, (ItemStack)null);
                        }
                     }
                  } else if(var16.a(var4) && var16.a(var17)) {
                     var20 = var16.b(var17);
                     if(var17.b > var20) {
                        var16.d(var17.a(var20));
                        var16.a(var4, var18);
                        if(!var6.c(var18)) {
                           var4.a(var18, true);
                        }
                     } else {
                        var16.d(var17);
                        var6.a(var2, var18);
                        var16.a(var4, var18);
                     }
                  }
               }
            } else if(var3 == class_aay.CLONE && var4.abilities.d && var6.o() == null && var1 >= 0) {
               var16 = (class_abs)this.c.get(var1);
               if(var16 != null && var16.e()) {
                  if(var16.d().b > 0) {
                     var17 = var16.d().k();
                     var17.b = var17.c();
                     var6.e(var17);
                  } else {
                     var16.d((ItemStack)null);
                  }
               }
            } else if(var3 == class_aay.THROW && var6.o() == null && var1 >= 0) {
               var16 = (class_abs)this.c.get(var1);
               if(var16 != null && var16.e() && var16.a(var4)) {
                  var17 = var16.a(var2 == 0?1:var16.d().b);
                  var16.a(var4, var17);
                  var4.a(var17, true);
               }
            } else if(var3 == class_aay.PICKUP_ALL && var1 >= 0) {
               var16 = (class_abs)this.c.get(var1);
               var17 = var6.o();
               if(var17 != null && (var16 == null || !var16.e() || !var16.a(var4))) {
                  var9 = var2 == 0?0:this.c.size() - 1;
                  var20 = var2 == 0?1:-1;

                  for(int var21 = 0; var21 < 2; ++var21) {
                     for(int var22 = var9; var22 >= 0 && var22 < this.c.size() && var17.b < var17.c(); var22 += var20) {
                        class_abs var23 = (class_abs)this.c.get(var22);
                        if(var23.e() && a(var23, var17, true) && var23.a(var4) && this.a(var17, var23) && (var21 != 0 || var23.d().b != var23.d().c())) {
                           int var14 = Math.min(var17.c() - var17.b, var23.d().b);
                           ItemStack var15 = var23.a(var14);
                           var17.b += var14;
                           if(var15.b <= 0) {
                              var23.d((ItemStack)null);
                           }

                           var23.a(var4, var15);
                        }
                     }
                  }
               }

               this.b();
            }
         } else if(var1 == -999) {
            if(var6.o() != null) {
               if(var2 == 0) {
                  var4.a(var6.o(), true);
                  var6.e((ItemStack)null);
               }

               if(var2 == 1) {
                  var4.a(var6.o().a(1), true);
                  if(var6.o().b == 0) {
                     var6.e((ItemStack)null);
                  }
               }
            }
         } else if(var3 == class_aay.QUICK_MOVE) {
            if(var1 < 0) {
               return null;
            }

            var16 = (class_abs)this.c.get(var1);
            if(var16 != null && var16.a(var4)) {
               var17 = var16.d();
               if(var17 != null && var17.b <= 0) {
                  var5 = var17.k();
                  var16.d((ItemStack)null);
               }

               var18 = this.b(var4, var1);
               if(var18 != null) {
                  Item var19 = var18.b();
                  var5 = var18.k();
                  if(var16.d() != null && var16.d().b() == var19) {
                     this.a(var1, var2, true, var4);
                  }
               }
            }
         } else {
            if(var1 < 0) {
               return null;
            }

            var16 = (class_abs)this.c.get(var1);
            if(var16 != null) {
               var17 = var16.d();
               var18 = var6.o();
               if(var17 != null) {
                  var5 = var17.k();
               }

               if(var17 == null) {
                  if(var18 != null && var16.a(var18)) {
                     var20 = var2 == 0?var18.b:1;
                     if(var20 > var16.b(var18)) {
                        var20 = var16.b(var18);
                     }

                     var16.d(var18.a(var20));
                     if(var18.b == 0) {
                        var6.e((ItemStack)null);
                     }
                  }
               } else if(var16.a(var4)) {
                  if(var18 == null) {
                     if(var17.b > 0) {
                        var20 = var2 == 0?var17.b:(var17.b + 1) / 2;
                        var6.e(var16.a(var20));
                        if(var17.b <= 0) {
                           var16.d((ItemStack)null);
                        }

                        var16.a(var4, var6.o());
                     } else {
                        var16.d((ItemStack)null);
                        var6.e((ItemStack)null);
                     }
                  } else if(var16.a(var18)) {
                     if(var17.b() == var18.b() && var17.i() == var18.i() && ItemStack.a(var17, var18)) {
                        var20 = var2 == 0?var18.b:1;
                        if(var20 > var16.b(var18) - var17.b) {
                           var20 = var16.b(var18) - var17.b;
                        }

                        if(var20 > var18.c() - var17.b) {
                           var20 = var18.c() - var17.b;
                        }

                        var18.a(var20);
                        if(var18.b == 0) {
                           var6.e((ItemStack)null);
                        }

                        var17.b += var20;
                     } else if(var18.b <= var16.b(var18)) {
                        var16.d(var18);
                        var6.e(var17);
                     }
                  } else if(var17.b() == var18.b() && var18.c() > 1 && (!var17.f() || var17.i() == var18.i()) && ItemStack.a(var17, var18)) {
                     var20 = var17.b;
                     if(var20 > 0 && var20 + var18.b <= var18.c()) {
                        var18.b += var20;
                        var17 = var16.a(var20);
                        if(var17.b == 0) {
                           var16.d((ItemStack)null);
                        }

                        var16.a(var4, var6.o());
                     }
                  }
               }

               var16.f();
            }
         }
      }

      return var5;
   }

   public boolean a(ItemStack var1, class_abs var2) {
      return true;
   }

   protected void a(int var1, int var2, boolean var3, EntityHuman var4) {
      this.a(var1, var2, class_aay.QUICK_MOVE, var4);
   }

   public void b(EntityHuman var1) {
      PlayerInventory var2 = var1.br;
      if(var2.o() != null) {
         var1.a(var2.o(), false);
         var2.e((ItemStack)null);
      }

   }

   public void a(IInventory var1) {
      this.b();
   }

   public void a(int var1, ItemStack var2) {
      this.a(var1).d(var2);
   }

   public boolean c(EntityHuman var1) {
      return !this.i.contains(var1);
   }

   public void a(EntityHuman var1, boolean var2) {
      if(var2) {
         this.i.remove(var1);
      } else {
         this.i.add(var1);
      }

   }

   public abstract boolean a(EntityHuman var1);

   protected boolean a(ItemStack var1, int var2, int var3, boolean var4) {
      boolean var5 = false;
      int var6 = var2;
      if(var4) {
         var6 = var3 - 1;
      }

      class_abs var7;
      ItemStack var8;
      if(var1.d()) {
         while(var1.b > 0 && (!var4 && var6 < var3 || var4 && var6 >= var2)) {
            var7 = (class_abs)this.c.get(var6);
            var8 = var7.d();
            if(var8 != null && a(var1, var8)) {
               int var9 = var8.b + var1.b;
               if(var9 <= var1.c()) {
                  var1.b = 0;
                  var8.b = var9;
                  var7.f();
                  var5 = true;
               } else if(var8.b < var1.c()) {
                  var1.b -= var1.c() - var8.b;
                  var8.b = var1.c();
                  var7.f();
                  var5 = true;
               }
            }

            if(var4) {
               --var6;
            } else {
               ++var6;
            }
         }
      }

      if(var1.b > 0) {
         if(var4) {
            var6 = var3 - 1;
         } else {
            var6 = var2;
         }

         while(!var4 && var6 < var3 || var4 && var6 >= var2) {
            var7 = (class_abs)this.c.get(var6);
            var8 = var7.d();
            if(var8 == null) {
               var7.d(var1.k());
               var7.f();
               var1.b = 0;
               var5 = true;
               break;
            }

            if(var4) {
               --var6;
            } else {
               ++var6;
            }
         }
      }

      return var5;
   }

   private static boolean a(ItemStack var0, ItemStack var1) {
      return var1.b() == var0.b() && (!var0.f() || var0.i() == var1.i()) && ItemStack.a(var0, var1);
   }

   public static int b(int var0) {
      return var0 >> 2 & 3;
   }

   public static int c(int var0) {
      return var0 & 3;
   }

   public static boolean a(int var0, EntityHuman var1) {
      return var0 == 0?true:(var0 == 1?true:var0 == 2 && var1.abilities.d);
   }

   protected void d() {
      this.g = 0;
      this.h.clear();
   }

   public static boolean a(class_abs var0, ItemStack var1, boolean var2) {
      boolean var3 = var0 == null || !var0.e();
      if(var0 != null && var0.e() && var1 != null && var1.a(var0.d()) && ItemStack.a(var0.d(), var1)) {
         var3 |= var0.d().b + (var2?0:var1.b) <= var1.c();
      }

      return var3;
   }

   public static void a(Set var0, int var1, ItemStack var2, int var3) {
      switch(var1) {
      case 0:
         var2.b = MathHelper.d((float)var2.b / (float)var0.size());
         break;
      case 1:
         var2.b = 1;
         break;
      case 2:
         var2.b = var2.b().j();
      }

      var2.b += var3;
   }

   public boolean b(class_abs var1) {
      return true;
   }

   public static int a(TileEntity var0) {
      return var0 instanceof IInventory?b((IInventory)var0):0;
   }

   public static int b(IInventory var0) {
      if(var0 == null) {
         return 0;
      } else {
         int var1 = 0;
         float var2 = 0.0F;

         for(int var3 = 0; var3 < var0.u_(); ++var3) {
            ItemStack var4 = var0.a(var3);
            if(var4 != null) {
               var2 += (float)var4.b / (float)Math.min(var0.w_(), var4.c());
               ++var1;
            }
         }

         var2 /= (float)var0.u_();
         return MathHelper.d(var2 * 14.0F) + (var1 > 0?1:0);
      }
   }
}
