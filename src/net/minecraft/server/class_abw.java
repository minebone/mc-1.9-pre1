package net.minecraft.server;

import java.util.List;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aet;
import net.minecraft.server.class_ahw;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_dc;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_xp;

public class class_abw extends Item {
   public class_abw() {
      this.a(CreativeModeTab.c);
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      if(var6 == EnumDirection.DOWN) {
         return EnumResult.FAIL;
      } else {
         boolean var10 = var3.getType(var4).getBlock().a((class_ahw)var3, (BlockPosition)var4);
         BlockPosition var11 = var10?var4:var4.a(var6);
         if(!var2.a(var11, var6, var1)) {
            return EnumResult.FAIL;
         } else {
            BlockPosition var12 = var11.a();
            boolean var13 = !var3.d(var11) && !var3.getType(var11).getBlock().a((class_ahw)var3, (BlockPosition)var11);
            var13 |= !var3.d(var12) && !var3.getType(var12).getBlock().a((class_ahw)var3, (BlockPosition)var12);
            if(var13) {
               return EnumResult.FAIL;
            } else {
               double var14 = (double)var11.p();
               double var16 = (double)var11.q();
               double var18 = (double)var11.r();
               List var20 = var3.b((Entity)null, (AxisAlignedBB)(new AxisAlignedBB(var14, var16, var18, var14 + 1.0D, var16 + 2.0D, var18 + 1.0D)));
               if(!var20.isEmpty()) {
                  return EnumResult.FAIL;
               } else {
                  if(!var3.E) {
                     var3.g(var11);
                     var3.g(var12);
                     class_xp var21 = new class_xp(var3, var14 + 0.5D, var16, var18 + 0.5D);
                     float var22 = (float)MathHelper.d((MathHelper.g(var2.yaw - 180.0F) + 22.5F) / 45.0F) * 45.0F;
                     var21.b(var14 + 0.5D, var16, var18 + 0.5D, var22, 0.0F);
                     this.a(var21, var3.r);
                     class_aet.a((World)var3, (EntityHuman)var2, (ItemStack)var1, (Entity)var21);
                     var3.a((Entity)var21);
                     var3.a((EntityHuman)null, var21.locX, var21.locY, var21.locZ, class_ng.m, EnumSoundCategory.BLOCKS, 0.75F, 0.8F);
                  }

                  --var1.b;
                  return EnumResult.SUCCESS;
               }
            }
         }
      }
   }

   private void a(class_xp var1, Random var2) {
      class_dc var3 = var1.w();
      float var5 = var2.nextFloat() * 5.0F;
      float var6 = var2.nextFloat() * 20.0F - 10.0F;
      class_dc var4 = new class_dc(var3.b() + var5, var3.c() + var6, var3.d());
      var1.a(var4);
      var3 = var1.x();
      var5 = var2.nextFloat() * 10.0F - 5.0F;
      var4 = new class_dc(var3.b(), var3.c() + var5, var3.d());
      var1.b(var4);
   }
}
