package net.minecraft.server;

import net.minecraft.server.World;
import net.minecraft.server.class_abx;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_zl;
import net.minecraft.server.class_zw;

public class class_aeu extends class_abx {
   public class_zl a(World var1, ItemStack var2, class_rz var3) {
      return new class_zw(var1, var3);
   }
}
