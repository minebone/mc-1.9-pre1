package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.EntityWitch;
import net.minecraft.server.StructureGenerator;
import net.minecraft.server.StructurePiece;
import net.minecraft.server.StructureStart;
import net.minecraft.server.World;
import net.minecraft.server.WorldGenRegistration;
import net.minecraft.server.class_aik;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;

public class WorldGenLargeFeature extends StructureGenerator {
   private static final List a = Arrays.asList(new BiomeBase[]{class_aik.d, class_aik.s, class_aik.w, class_aik.x, class_aik.h, class_aik.n, class_aik.F});
   private List b;
   private int d;
   private int h;

   public WorldGenLargeFeature() {
      this.b = Lists.newArrayList();
      this.d = 32;
      this.h = 8;
      this.b.add(new BiomeBase.BiomeMeta(EntityWitch.class, 1, 1, 1));
   }

   public WorldGenLargeFeature(Map var1) {
      this();
      Iterator var2 = var1.entrySet().iterator();

      while(var2.hasNext()) {
         Entry var3 = (Entry)var2.next();
         if(((String)var3.getKey()).equals("distance")) {
            this.d = MathHelper.a((String)var3.getValue(), this.d, this.h + 1);
         }
      }

   }

   public String a() {
      return "Temple";
   }

   protected boolean a(int var1, int var2) {
      int var3 = var1;
      int var4 = var2;
      if(var1 < 0) {
         var1 -= this.d - 1;
      }

      if(var2 < 0) {
         var2 -= this.d - 1;
      }

      int var5 = var1 / this.d;
      int var6 = var2 / this.d;
      Random var7 = this.g.a(var5, var6, 14357617);
      var5 *= this.d;
      var6 *= this.d;
      var5 += var7.nextInt(this.d - this.h);
      var6 += var7.nextInt(this.d - this.h);
      if(var3 == var5 && var4 == var6) {
         BiomeBase var8 = this.g.A().a(new BlockPosition(var3 * 16 + 8, 0, var4 * 16 + 8));
         if(var8 == null) {
            return false;
         }

         Iterator var9 = a.iterator();

         while(var9.hasNext()) {
            BiomeBase var10 = (BiomeBase)var9.next();
            if(var8 == var10) {
               return true;
            }
         }
      }

      return false;
   }

   protected StructureStart b(int var1, int var2) {
      return new WorldGenLargeFeature.class_a_in_class_avy(this.g, this.f, var1, var2);
   }

   public boolean a(BlockPosition var1) {
      StructureStart var2 = this.c(var1);
      if(var2 != null && var2 instanceof WorldGenLargeFeature.class_a_in_class_avy && !var2.a.isEmpty()) {
         StructurePiece var3 = (StructurePiece)var2.a.get(0);
         return var3 instanceof WorldGenRegistration.class_e_in_class_avz;
      } else {
         return false;
      }
   }

   public List b() {
      return this.b;
   }

   public static class class_a_in_class_avy extends StructureStart {
      public class_a_in_class_avy() {
      }

      public class_a_in_class_avy(World var1, Random var2, int var3, int var4) {
         this(var1, var2, var3, var4, var1.b(new BlockPosition(var3 * 16 + 8, 0, var4 * 16 + 8)));
      }

      public class_a_in_class_avy(World var1, Random var2, int var3, int var4, BiomeBase var5) {
         super(var3, var4);
         if(var5 != class_aik.w && var5 != class_aik.x) {
            if(var5 == class_aik.h) {
               WorldGenRegistration.class_e_in_class_avz var7 = new WorldGenRegistration.class_e_in_class_avz(var2, var3 * 16, var4 * 16);
               this.a.add(var7);
            } else if(var5 != class_aik.d && var5 != class_aik.s) {
               if(var5 == class_aik.n || var5 == class_aik.F) {
                  WorldGenRegistration.class_b_in_class_avz var9 = new WorldGenRegistration.class_b_in_class_avz(var2, var3 * 16, var4 * 16);
                  this.a.add(var9);
               }
            } else {
               WorldGenRegistration.WorldGenPyramidPiece var8 = new WorldGenRegistration.WorldGenPyramidPiece(var2, var3 * 16, var4 * 16);
               this.a.add(var8);
            }
         } else {
            WorldGenRegistration.WorldGenJungleTemple var6 = new WorldGenRegistration.WorldGenJungleTemple(var2, var3 * 16, var4 * 16);
            this.a.add(var6);
         }

         this.d();
      }
   }
}
