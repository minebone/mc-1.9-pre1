package net.minecraft.server;

import com.google.common.base.Predicates;
import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockDoubleStepAbstract;
import net.minecraft.server.BlockSand;
import net.minecraft.server.BlockStatePredicate;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.class_aly;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_auc;
import net.minecraft.server.BlockPosition;

public class class_atx extends class_auc {
   private static final BlockStatePredicate a = BlockStatePredicate.a((Block)Blocks.m).a(BlockSand.a, Predicates.equalTo(BlockSand.EnumSandVariant.SAND));
   private final IBlockData b = Blocks.U.u().set(BlockDoubleStepAbstract.e, BlockDoubleStepAbstract.EnumStoneSlabVariant.SAND).set(class_aly.a, class_aly.class_a_in_class_aly.BOTTOM);
   private final IBlockData c = Blocks.A.u();
   private final IBlockData d = Blocks.i.u();

   public boolean b(World var1, Random var2, BlockPosition var3) {
      while(var1.d(var3) && var3.q() > 2) {
         var3 = var3.b();
      }

      if(!a.a(var1.getType(var3))) {
         return false;
      } else {
         int var4;
         int var5;
         for(var4 = -2; var4 <= 2; ++var4) {
            for(var5 = -2; var5 <= 2; ++var5) {
               if(var1.d(var3.a(var4, -1, var5)) && var1.d(var3.a(var4, -2, var5))) {
                  return false;
               }
            }
         }

         for(var4 = -1; var4 <= 0; ++var4) {
            for(var5 = -2; var5 <= 2; ++var5) {
               for(int var6 = -2; var6 <= 2; ++var6) {
                  var1.a((BlockPosition)var3.a(var5, var4, var6), (IBlockData)this.c, 2);
               }
            }
         }

         var1.a((BlockPosition)var3, (IBlockData)this.d, 2);
         Iterator var7 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

         while(var7.hasNext()) {
            EnumDirection var8 = (EnumDirection)var7.next();
            var1.a((BlockPosition)var3.a(var8), (IBlockData)this.d, 2);
         }

         for(var4 = -2; var4 <= 2; ++var4) {
            for(var5 = -2; var5 <= 2; ++var5) {
               if(var4 == -2 || var4 == 2 || var5 == -2 || var5 == 2) {
                  var1.a((BlockPosition)var3.a(var4, 1, var5), (IBlockData)this.c, 2);
               }
            }
         }

         var1.a((BlockPosition)var3.a(2, 1, 0), (IBlockData)this.b, 2);
         var1.a((BlockPosition)var3.a(-2, 1, 0), (IBlockData)this.b, 2);
         var1.a((BlockPosition)var3.a(0, 1, 2), (IBlockData)this.b, 2);
         var1.a((BlockPosition)var3.a(0, 1, -2), (IBlockData)this.b, 2);

         for(var4 = -1; var4 <= 1; ++var4) {
            for(var5 = -1; var5 <= 1; ++var5) {
               if(var4 == 0 && var5 == 0) {
                  var1.a((BlockPosition)var3.a(var4, 4, var5), (IBlockData)this.c, 2);
               } else {
                  var1.a((BlockPosition)var3.a(var4, 4, var5), (IBlockData)this.b, 2);
               }
            }
         }

         for(var4 = 1; var4 <= 3; ++var4) {
            var1.a((BlockPosition)var3.a(-1, var4, -1), (IBlockData)this.c, 2);
            var1.a((BlockPosition)var3.a(-1, var4, 1), (IBlockData)this.c, 2);
            var1.a((BlockPosition)var3.a(1, var4, -1), (IBlockData)this.c, 2);
            var1.a((BlockPosition)var3.a(1, var4, 1), (IBlockData)this.c, 2);
         }

         return true;
      }
   }
}
