package net.minecraft.server;

import net.minecraft.server.BiomeBase;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.WorldGenFlatInfo;
import net.minecraft.server.WorldProviderNormal;
import net.minecraft.server.WorldType;
import net.minecraft.server.class_aij;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_ain;
import net.minecraft.server.class_aru;
import net.minecraft.server.class_arx;
import net.minecraft.server.class_atg;
import net.minecraft.server.class_ath;
import net.minecraft.server.class_atm;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EntityPlayer;

public abstract class class_asu {
   public static final float[] a = new float[]{1.0F, 0.75F, 0.5F, 0.25F, 0.0F, 0.25F, 0.5F, 0.75F};
   protected World b;
   private WorldType g;
   private String h;
   protected class_aij c;
   protected boolean d;
   protected boolean isNotOverworld;
   protected final float[] f = new float[16];
   private final float[] i = new float[4];

   public final void a(World var1) {
      this.b = var1;
      this.g = var1.T().t();
      this.h = var1.T().A();
      this.b();
      this.a();
   }

   protected void a() {
      float var1 = 0.0F;

      for(int var2 = 0; var2 <= 15; ++var2) {
         float var3 = 1.0F - (float)var2 / 15.0F;
         this.f[var2] = (1.0F - var3) / (var3 * 3.0F + 1.0F) * (1.0F - var1) + var1;
      }

   }

   protected void b() {
      WorldType var1 = this.b.T().t();
      if(var1 == WorldType.c) {
         WorldGenFlatInfo var2 = WorldGenFlatInfo.a(this.b.T().A());
         this.c = new class_ain(BiomeBase.a(var2.a(), class_aik.b));
      } else if(var1 == WorldType.g) {
         this.c = new class_ain(class_aik.c);
      } else {
         this.c = new class_aij(this.b.T());
      }

   }

   public class_arx c() {
      return (class_arx)(this.g == WorldType.c?new class_ath(this.b, this.b.O(), this.b.T().r(), this.h):(this.g == WorldType.g?new class_atg(this.b):(this.g == WorldType.f?new class_atm(this.b, this.b.O(), this.b.T().r(), this.h):new class_atm(this.b, this.b.O(), this.b.T().r(), this.h))));
   }

   public boolean a(int var1, int var2) {
      BlockPosition var3 = new BlockPosition(var1, 0, var2);
      return this.b.b(var3).i()?true:this.b.c(var3).getBlock() == Blocks.c;
   }

   public float a(long var1, float var3) {
      int var4 = (int)(var1 % 24000L);
      float var5 = ((float)var4 + var3) / 24000.0F - 0.25F;
      if(var5 < 0.0F) {
         ++var5;
      }

      if(var5 > 1.0F) {
         --var5;
      }

      float var6 = var5;
      var5 = 1.0F - (float)((Math.cos((double)var5 * 3.141592653589793D) + 1.0D) / 2.0D);
      var5 = var6 + (var5 - var6) / 3.0F;
      return var5;
   }

   public int a(long var1) {
      return (int)(var1 / 24000L % 8L + 8L) % 8;
   }

   public boolean d() {
      return true;
   }

   public boolean e() {
      return true;
   }

   public BlockPosition h() {
      return null;
   }

   public int i() {
      return this.g == WorldType.c?4:this.b.K() + 1;
   }

   public class_aij k() {
      return this.c;
   }

   public boolean l() {
      return this.d;
   }

   public boolean isNotOverworld() {
      return this.isNotOverworld;
   }

   public float[] n() {
      return this.f;
   }

   public class_aru o() {
      return new class_aru();
   }

   public void a(EntityPlayer var1) {
   }

   public void b(EntityPlayer var1) {
   }

   public abstract WorldProviderNormal p();

   public void q() {
   }

   public void r() {
   }

   public boolean c(int var1, int var2) {
      return true;
   }
}
