package net.minecraft.server;

import java.util.Arrays;
import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.BlockDirt;
import net.minecraft.server.BlockSand;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumColor;
import net.minecraft.server.World;
import net.minecraft.server.class_aki;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ate;
import net.minecraft.server.class_ato;
import net.minecraft.server.class_awu;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_ais extends BiomeBase {
   protected static final IBlockData y = Blocks.d.u().set(BlockDirt.a, BlockDirt.EnumDirtVariant.COARSE_DIRT);
   protected static final IBlockData z = Blocks.c.u();
   protected static final IBlockData A = Blocks.cz.u();
   protected static final IBlockData B = Blocks.cu.u();
   protected static final IBlockData C = B.set(class_aki.a, EnumColor.ORANGE);
   protected static final IBlockData D = Blocks.m.u().set(BlockSand.a, BlockSand.EnumSandVariant.RED_SAND);
   private IBlockData[] E;
   private long F;
   private class_awu G;
   private class_awu H;
   private class_awu I;
   private boolean J;
   private boolean K;

   public class_ais(boolean var1, boolean var2, BiomeBase.class_a_in_class_aif var3) {
      super(var3);
      this.J = var1;
      this.K = var2;
      this.v.clear();
      this.r = D;
      this.s = B;
      this.t.z = -999;
      this.t.C = 20;
      this.t.E = 3;
      this.t.F = 5;
      this.t.A = 0;
      this.v.clear();
      if(var2) {
         this.t.z = 5;
      }

   }

   public class_ato a(Random var1) {
      return n;
   }

   public void a(World var1, Random var2, BlockPosition var3) {
      super.a(var1, var2, var3);
   }

   public void a(World var1, Random var2, class_ate var3, int var4, int var5, double var6) {
      if(this.E == null || this.F != var1.O()) {
         this.a(var1.O());
      }

      if(this.G == null || this.H == null || this.F != var1.O()) {
         Random var8 = new Random(this.F);
         this.G = new class_awu(var8, 4);
         this.H = new class_awu(var8, 1);
      }

      this.F = var1.O();
      double var22 = 0.0D;
      int var10;
      int var11;
      if(this.J) {
         var10 = (var4 & -16) + (var5 & 15);
         var11 = (var5 & -16) + (var4 & 15);
         double var12 = Math.min(Math.abs(var6), this.G.a((double)var10 * 0.25D, (double)var11 * 0.25D));
         if(var12 > 0.0D) {
            double var14 = 0.001953125D;
            double var16 = Math.abs(this.H.a((double)var10 * var14, (double)var11 * var14));
            var22 = var12 * var12 * 2.5D;
            double var18 = Math.ceil(var16 * 50.0D) + 14.0D;
            if(var22 > var18) {
               var22 = var18;
            }

            var22 += 64.0D;
         }
      }

      var10 = var4 & 15;
      var11 = var5 & 15;
      int var23 = var1.K();
      IBlockData var13 = B;
      IBlockData var24 = this.s;
      int var15 = (int)(var6 / 3.0D + 3.0D + var2.nextDouble() * 0.25D);
      boolean var25 = Math.cos(var6 / 3.0D * 3.141592653589793D) > 0.0D;
      int var17 = -1;
      boolean var26 = false;

      for(int var19 = 255; var19 >= 0; --var19) {
         if(var3.a(var11, var19, var10).getMaterial() == Material.a && var19 < (int)var22) {
            var3.a(var11, var19, var10, a);
         }

         if(var19 <= var2.nextInt(5)) {
            var3.a(var11, var19, var10, c);
         } else {
            IBlockData var20 = var3.a(var11, var19, var10);
            if(var20.getMaterial() == Material.a) {
               var17 = -1;
            } else if(var20.getBlock() == Blocks.b) {
               if(var17 == -1) {
                  var26 = false;
                  if(var15 <= 0) {
                     var13 = b;
                     var24 = a;
                  } else if(var19 >= var23 - 4 && var19 <= var23 + 1) {
                     var13 = B;
                     var24 = this.s;
                  }

                  if(var19 < var23 && (var13 == null || var13.getMaterial() == Material.a)) {
                     var13 = h;
                  }

                  var17 = var15 + Math.max(0, var19 - var23);
                  if(var19 < var23 - 1) {
                     var3.a(var11, var19, var10, var24);
                     if(var24.getBlock() == Blocks.cu) {
                        var3.a(var11, var19, var10, C);
                     }
                  } else if(this.K && var19 > 86 + var15 * 2) {
                     if(var25) {
                        var3.a(var11, var19, var10, y);
                     } else {
                        var3.a(var11, var19, var10, z);
                     }
                  } else if(var19 <= var23 + 3 + var15) {
                     var3.a(var11, var19, var10, this.r);
                     var26 = true;
                  } else {
                     IBlockData var21;
                     if(var19 >= 64 && var19 <= 127) {
                        if(var25) {
                           var21 = A;
                        } else {
                           var21 = this.a(var4, var19, var5);
                        }
                     } else {
                        var21 = C;
                     }

                     var3.a(var11, var19, var10, var21);
                  }
               } else if(var17 > 0) {
                  --var17;
                  if(var26) {
                     var3.a(var11, var19, var10, C);
                  } else {
                     var3.a(var11, var19, var10, this.a(var4, var19, var5));
                  }
               }
            }
         }
      }

   }

   private void a(long var1) {
      this.E = new IBlockData[64];
      Arrays.fill(this.E, A);
      Random var3 = new Random(var1);
      this.I = new class_awu(var3, 1);

      int var4;
      for(var4 = 0; var4 < 64; ++var4) {
         var4 += var3.nextInt(5) + 1;
         if(var4 < 64) {
            this.E[var4] = C;
         }
      }

      var4 = var3.nextInt(4) + 2;

      int var5;
      int var6;
      int var7;
      int var8;
      for(var5 = 0; var5 < var4; ++var5) {
         var6 = var3.nextInt(3) + 1;
         var7 = var3.nextInt(64);

         for(var8 = 0; var7 + var8 < 64 && var8 < var6; ++var8) {
            this.E[var7 + var8] = B.set(class_aki.a, EnumColor.YELLOW);
         }
      }

      var5 = var3.nextInt(4) + 2;

      int var9;
      for(var6 = 0; var6 < var5; ++var6) {
         var7 = var3.nextInt(3) + 2;
         var8 = var3.nextInt(64);

         for(var9 = 0; var8 + var9 < 64 && var9 < var7; ++var9) {
            this.E[var8 + var9] = B.set(class_aki.a, EnumColor.BROWN);
         }
      }

      var6 = var3.nextInt(4) + 2;

      for(var7 = 0; var7 < var6; ++var7) {
         var8 = var3.nextInt(3) + 1;
         var9 = var3.nextInt(64);

         for(int var10 = 0; var9 + var10 < 64 && var10 < var8; ++var10) {
            this.E[var9 + var10] = B.set(class_aki.a, EnumColor.RED);
         }
      }

      var7 = var3.nextInt(3) + 3;
      var8 = 0;

      for(var9 = 0; var9 < var7; ++var9) {
         byte var12 = 1;
         var8 += var3.nextInt(16) + 4;

         for(int var11 = 0; var8 + var11 < 64 && var11 < var12; ++var11) {
            this.E[var8 + var11] = B.set(class_aki.a, EnumColor.WHITE);
            if(var8 + var11 > 1 && var3.nextBoolean()) {
               this.E[var8 + var11 - 1] = B.set(class_aki.a, EnumColor.SILVER);
            }

            if(var8 + var11 < 63 && var3.nextBoolean()) {
               this.E[var8 + var11 + 1] = B.set(class_aki.a, EnumColor.SILVER);
            }
         }
      }

   }

   private IBlockData a(int var1, int var2, int var3) {
      int var4 = (int)Math.round(this.I.a((double)var1 / 512.0D, (double)var1 / 512.0D) * 2.0D);
      return this.E[(var2 + var4 + 64) % 64];
   }
}
