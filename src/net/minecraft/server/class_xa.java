package net.minecraft.server;

import net.minecraft.server.Vec3D;
import net.minecraft.server.class_wt;
import net.minecraft.server.class_wv;
import net.minecraft.server.class_xj;

public class class_xa extends class_wv {
   private Vec3D b;

   public class_xa(class_wt var1) {
      super(var1);
   }

   public void c() {
      if(this.b == null) {
         this.b = new Vec3D(this.a.locX, this.a.locY, this.a.locZ);
      }

   }

   public boolean a() {
      return true;
   }

   public void d() {
      this.b = null;
   }

   public float f() {
      return 1.0F;
   }

   public Vec3D g() {
      return this.b;
   }

   public class_xj i() {
      return class_xj.k;
   }
}
