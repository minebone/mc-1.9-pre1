package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.EntityCreature;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.ScoreboardTeamBase;
import net.minecraft.server.class_aym;
import net.minecraft.server.class_ayo;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sf;
import net.minecraft.server.class_sl;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_ys;

public abstract class class_vb extends class_tj {
   protected final EntityCreature e;
   protected boolean f;
   private boolean a;
   private int b;
   private int c;
   private int d;
   protected class_rz g;
   protected int h;

   public class_vb(EntityCreature var1, boolean var2) {
      this(var1, var2, false);
   }

   public class_vb(EntityCreature var1, boolean var2, boolean var3) {
      this.h = 60;
      this.e = var1;
      this.f = var2;
      this.a = var3;
   }

   public boolean b() {
      class_rz var1 = this.e.A();
      if(var1 == null) {
         var1 = this.g;
      }

      if(var1 == null) {
         return false;
      } else if(!var1.at()) {
         return false;
      } else {
         ScoreboardTeamBase var2 = this.e.aN();
         ScoreboardTeamBase var3 = var1.aN();
         if(var2 != null && var3 == var2) {
            return false;
         } else {
            double var4 = this.f();
            if(this.e.h(var1) > var4 * var4) {
               return false;
            } else {
               if(this.f) {
                  if(this.e.y().a(var1)) {
                     this.d = 0;
                  } else if(++this.d > this.h) {
                     return false;
                  }
               }

               if(var1 instanceof EntityHuman && ((EntityHuman)var1).abilities.a) {
                  return false;
               } else {
                  this.e.c(var1);
                  return true;
               }
            }
         }
      }
   }

   protected double f() {
      class_sl var1 = this.e.a(class_ys.b);
      return var1 == null?16.0D:var1.e();
   }

   public void c() {
      this.b = 0;
      this.c = 0;
      this.d = 0;
   }

   public void d() {
      this.e.c((class_rz)null);
      this.g = null;
   }

   public static boolean a(EntityInsentient var0, class_rz var1, boolean var2, boolean var3) {
      if(var1 == null) {
         return false;
      } else if(var1 == var0) {
         return false;
      } else if(!var1.at()) {
         return false;
      } else if(!var0.d(var1.getClass())) {
         return false;
      } else if(var0.r(var1)) {
         return false;
      } else {
         if(var0 instanceof class_sf && ((class_sf)var0).b() != null) {
            if(var1 instanceof class_sf && ((class_sf)var0).b().equals(var1.getUniqueId())) {
               return false;
            }

            if(var1 == ((class_sf)var0).p_()) {
               return false;
            }
         } else if(var1 instanceof EntityHuman && !var2 && ((EntityHuman)var1).abilities.a) {
            return false;
         }

         return !var3 || var0.y().a(var1);
      }
   }

   protected boolean a(class_rz var1, boolean var2) {
      if(!a(this.e, var1, var2, this.f)) {
         return false;
      } else if(!this.e.f(new BlockPosition(var1))) {
         return false;
      } else {
         if(this.a) {
            if(--this.c <= 0) {
               this.b = 0;
            }

            if(this.b == 0) {
               this.b = this.a(var1)?1:2;
            }

            if(this.b == 2) {
               return false;
            }
         }

         return true;
      }
   }

   private boolean a(class_rz var1) {
      this.c = 10 + this.e.bE().nextInt(5);
      class_ayo var2 = this.e.x().a((Entity)var1);
      if(var2 == null) {
         return false;
      } else {
         class_aym var3 = var2.c();
         if(var3 == null) {
            return false;
         } else {
            int var4 = var3.a - MathHelper.c(var1.locX);
            int var5 = var3.c - MathHelper.c(var1.locZ);
            return (double)(var4 * var4 + var5 * var5) <= 2.25D;
         }
      }
   }
}
