package net.minecraft.server;

import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.ICommandListener;

public class CommandSaveOn extends CommandAbstract {
   public String c() {
      return "save-on";
   }

   public String b(ICommandListener var1) {
      return "commands.save-on.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      boolean var4 = false;

      for(int var5 = 0; var5 < var1.d.length; ++var5) {
         if(var1.d[var5] != null) {
            WorldServer var6 = var1.d[var5];
            if(var6.b) {
               var6.b = false;
               var4 = true;
            }
         }
      }

      if(var4) {
         a(var2, this, "commands.save.enabled", new Object[0]);
      } else {
         throw new class_bz("commands.save-on.alreadyOn", new Object[0]);
      }
   }
}
