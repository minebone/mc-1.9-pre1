package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.AchievementList;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.EnumMonsterType;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Items;
import net.minecraft.server.MobEffect;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.PathfinderGoalArrowAttack;
import net.minecraft.server.Statistic;
import net.minecraft.server.World;
import net.minecraft.server.class_aad;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_ln;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.BossBar;
import net.minecraft.server.class_ru;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_to;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_uu;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_vd;
import net.minecraft.server.class_yc;
import net.minecraft.server.class_yp;
import net.minecraft.server.class_yr;
import net.minecraft.server.class_ys;
import net.minecraft.server.class_zl;

public class class_xn extends class_yp implements class_yr {
   private static final class_ke a = DataWatcher.a(class_xn.class, class_kg.b);
   private static final class_ke b = DataWatcher.a(class_xn.class, class_kg.b);
   private static final class_ke c = DataWatcher.a(class_xn.class, class_kg.b);
   private static final class_ke[] bv = new class_ke[]{a, b, c};
   private static final class_ke bw = DataWatcher.a(class_xn.class, class_kg.b);
   private float[] bx = new float[2];
   private float[] by = new float[2];
   private float[] bz = new float[2];
   private float[] bA = new float[2];
   private int[] bB = new int[2];
   private int[] bC = new int[2];
   private int bD;
   private final class_ln bE = (class_ln)(new class_ln(this.i_(), BossBar.EnumBossBarColor.PURPLE, BossBar.EnumBossBarType.PROGRESS)).a(true);
   private static final Predicate bF = new Predicate() {
      public boolean a(Entity var1) {
         return var1 instanceof class_rz && ((class_rz)var1).bZ() != EnumMonsterType.UNDEAD;
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((Entity)var1);
      }
   };

   public class_xn(World var1) {
      super(var1);
      this.c(this.bV());
      this.a(0.9F, 3.5F);
      this.fireProof = true;
      ((class_vd)this.x()).c(true);
      this.b_ = 50;
   }

   protected void r() {
      this.bp.a(0, new class_xn.class_a_in_class_xn());
      this.bp.a(1, new class_tg(this));
      this.bp.a(2, new PathfinderGoalArrowAttack(this, 1.0D, 40, 20.0F));
      this.bp.a(5, new class_uf(this, 1.0D));
      this.bp.a(6, new class_to(this, EntityHuman.class, 8.0F));
      this.bp.a(7, new class_ue(this));
      this.bq.a(1, new class_uu(this, false, new Class[0]));
      this.bq.a(2, new class_ux(this, EntityInsentient.class, 0, false, false, bF));
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)a, (Object)Integer.valueOf(0));
      this.datawatcher.a((class_ke)b, (Object)Integer.valueOf(0));
      this.datawatcher.a((class_ke)c, (Object)Integer.valueOf(0));
      this.datawatcher.a((class_ke)bw, (Object)Integer.valueOf(0));
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("Invul", this.cZ());
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.l(var1.h("Invul"));
   }

   protected class_nf G() {
      return class_ng.gy;
   }

   protected class_nf bQ() {
      return class_ng.gB;
   }

   protected class_nf bR() {
      return class_ng.gA;
   }

   public void n() {
      this.motY *= 0.6000000238418579D;
      double var4;
      double var6;
      double var8;
      if(!this.world.E && this.m(0) > 0) {
         Entity var1 = this.world.a(this.m(0));
         if(var1 != null) {
            if(this.locY < var1.locY || !this.da() && this.locY < var1.locY + 5.0D) {
               if(this.motY < 0.0D) {
                  this.motY = 0.0D;
               }

               this.motY += (0.5D - this.motY) * 0.6000000238418579D;
            }

            double var2 = var1.locX - this.locX;
            var4 = var1.locZ - this.locZ;
            var6 = var2 * var2 + var4 * var4;
            if(var6 > 9.0D) {
               var8 = (double)MathHelper.a(var6);
               this.motX += (var2 / var8 * 0.5D - this.motX) * 0.6000000238418579D;
               this.motZ += (var4 / var8 * 0.5D - this.motZ) * 0.6000000238418579D;
            }
         }
      }

      if(this.motX * this.motX + this.motZ * this.motZ > 0.05000000074505806D) {
         this.yaw = (float)MathHelper.b(this.motZ, this.motX) * 57.295776F - 90.0F;
      }

      super.n();

      int var20;
      for(var20 = 0; var20 < 2; ++var20) {
         this.bA[var20] = this.by[var20];
         this.bz[var20] = this.bx[var20];
      }

      int var21;
      for(var20 = 0; var20 < 2; ++var20) {
         var21 = this.m(var20 + 1);
         Entity var3 = null;
         if(var21 > 0) {
            var3 = this.world.a(var21);
         }

         if(var3 != null) {
            var4 = this.n(var20 + 1);
            var6 = this.o(var20 + 1);
            var8 = this.p(var20 + 1);
            double var10 = var3.locX - var4;
            double var12 = var3.locY + (double)var3.bm() - var6;
            double var14 = var3.locZ - var8;
            double var16 = (double)MathHelper.a(var10 * var10 + var14 * var14);
            float var18 = (float)(MathHelper.b(var14, var10) * 57.2957763671875D) - 90.0F;
            float var19 = (float)(-(MathHelper.b(var12, var16) * 57.2957763671875D));
            this.bx[var20] = this.b(this.bx[var20], var19, 40.0F);
            this.by[var20] = this.b(this.by[var20], var18, 10.0F);
         } else {
            this.by[var20] = this.b(this.by[var20], this.aM, 10.0F);
         }
      }

      boolean var22 = this.da();

      for(var21 = 0; var21 < 3; ++var21) {
         double var23 = this.n(var21);
         double var5 = this.o(var21);
         double var7 = this.p(var21);
         this.world.a(EnumParticle.SMOKE_NORMAL, var23 + this.random.nextGaussian() * 0.30000001192092896D, var5 + this.random.nextGaussian() * 0.30000001192092896D, var7 + this.random.nextGaussian() * 0.30000001192092896D, 0.0D, 0.0D, 0.0D, new int[0]);
         if(var22 && this.world.r.nextInt(4) == 0) {
            this.world.a(EnumParticle.SPELL_MOB, var23 + this.random.nextGaussian() * 0.30000001192092896D, var5 + this.random.nextGaussian() * 0.30000001192092896D, var7 + this.random.nextGaussian() * 0.30000001192092896D, 0.699999988079071D, 0.699999988079071D, 0.5D, new int[0]);
         }
      }

      if(this.cZ() > 0) {
         for(var21 = 0; var21 < 3; ++var21) {
            this.world.a(EnumParticle.SPELL_MOB, this.locX + this.random.nextGaussian(), this.locY + (double)(this.random.nextFloat() * 3.3F), this.locZ + this.random.nextGaussian(), 0.699999988079071D, 0.699999988079071D, 0.8999999761581421D, new int[0]);
         }
      }

   }

   protected void M() {
      int var1;
      if(this.cZ() > 0) {
         var1 = this.cZ() - 1;
         if(var1 <= 0) {
            this.world.a(this, this.locX, this.locY + (double)this.bm(), this.locZ, 7.0F, false, this.world.U().b("mobGriefing"));
            this.world.a(1023, new BlockPosition(this), 0);
         }

         this.l(var1);
         if(this.ticksLived % 10 == 0) {
            this.b(10.0F);
         }

      } else {
         super.M();

         int var14;
         for(var1 = 1; var1 < 3; ++var1) {
            if(this.ticksLived >= this.bB[var1 - 1]) {
               this.bB[var1 - 1] = this.ticksLived + 10 + this.random.nextInt(10);
               if(this.world.ae() == EnumDifficulty.NORMAL || this.world.ae() == EnumDifficulty.HARD) {
                  int var10001 = var1 - 1;
                  int var10003 = this.bC[var1 - 1];
                  this.bC[var10001] = this.bC[var1 - 1] + 1;
                  if(var10003 > 15) {
                     float var2 = 10.0F;
                     float var3 = 5.0F;
                     double var4 = MathHelper.a(this.random, this.locX - (double)var2, this.locX + (double)var2);
                     double var6 = MathHelper.a(this.random, this.locY - (double)var3, this.locY + (double)var3);
                     double var8 = MathHelper.a(this.random, this.locZ - (double)var2, this.locZ + (double)var2);
                     this.a(var1 + 1, var4, var6, var8, true);
                     this.bC[var1 - 1] = 0;
                  }
               }

               var14 = this.m(var1);
               if(var14 > 0) {
                  Entity var16 = this.world.a(var14);
                  if(var16 != null && var16.at() && this.h(var16) <= 900.0D && this.D(var16)) {
                     if(var16 instanceof EntityHuman && ((EntityHuman)var16).abilities.a) {
                        this.a(var1, 0);
                     } else {
                        this.a(var1 + 1, (class_rz)var16);
                        this.bB[var1 - 1] = this.ticksLived + 40 + this.random.nextInt(20);
                        this.bC[var1 - 1] = 0;
                     }
                  } else {
                     this.a(var1, 0);
                  }
               } else {
                  List var15 = this.world.a(class_rz.class, this.bk().b(20.0D, 8.0D, 20.0D), Predicates.and(bF, class_ru.e));

                  for(int var18 = 0; var18 < 10 && !var15.isEmpty(); ++var18) {
                     class_rz var5 = (class_rz)var15.get(this.random.nextInt(var15.size()));
                     if(var5 != this && var5.at() && this.D(var5)) {
                        if(var5 instanceof EntityHuman) {
                           if(!((EntityHuman)var5).abilities.a) {
                              this.a(var1, var5.getId());
                           }
                        } else {
                           this.a(var1, var5.getId());
                        }
                        break;
                     }

                     var15.remove(var5);
                  }
               }
            }
         }

         if(this.A() != null) {
            this.a(0, this.A().getId());
         } else {
            this.a(0, 0);
         }

         if(this.bD > 0) {
            --this.bD;
            if(this.bD == 0 && this.world.U().b("mobGriefing")) {
               var1 = MathHelper.c(this.locY);
               var14 = MathHelper.c(this.locX);
               int var17 = MathHelper.c(this.locZ);
               boolean var19 = false;

               for(int var20 = -1; var20 <= 1; ++var20) {
                  for(int var21 = -1; var21 <= 1; ++var21) {
                     for(int var7 = 0; var7 <= 3; ++var7) {
                        int var22 = var14 + var20;
                        int var9 = var1 + var7;
                        int var10 = var17 + var21;
                        BlockPosition var11 = new BlockPosition(var22, var9, var10);
                        IBlockData var12 = this.world.getType(var11);
                        Block var13 = var12.getBlock();
                        if(var12.getMaterial() != Material.a && a(var13)) {
                           var19 = this.world.b(var11, true) || var19;
                        }
                     }
                  }
               }

               if(var19) {
                  this.world.a((EntityHuman)null, 1022, new BlockPosition(this), 0);
               }
            }
         }

         if(this.ticksLived % 20 == 0) {
            this.b(1.0F);
         }

         this.bE.a(this.bP() / this.bV());
      }
   }

   public static boolean a(Block var0) {
      return var0 != Blocks.h && var0 != Blocks.bF && var0 != Blocks.bG && var0 != Blocks.bX && var0 != Blocks.dc && var0 != Blocks.dd && var0 != Blocks.cv;
   }

   public void o() {
      this.l(220);
      this.c(this.bV() / 3.0F);
   }

   public void aP() {
   }

   public void b(EntityPlayer var1) {
      super.b(var1);
      this.bE.a(var1);
   }

   public void c(EntityPlayer var1) {
      super.c(var1);
      this.bE.b(var1);
   }

   private double n(int var1) {
      if(var1 <= 0) {
         return this.locX;
      } else {
         float var2 = (this.aM + (float)(180 * (var1 - 1))) * 0.017453292F;
         float var3 = MathHelper.b(var2);
         return this.locX + (double)var3 * 1.3D;
      }
   }

   private double o(int var1) {
      return var1 <= 0?this.locY + 3.0D:this.locY + 2.2D;
   }

   private double p(int var1) {
      if(var1 <= 0) {
         return this.locZ;
      } else {
         float var2 = (this.aM + (float)(180 * (var1 - 1))) * 0.017453292F;
         float var3 = MathHelper.a(var2);
         return this.locZ + (double)var3 * 1.3D;
      }
   }

   private float b(float var1, float var2, float var3) {
      float var4 = MathHelper.g(var2 - var1);
      if(var4 > var3) {
         var4 = var3;
      }

      if(var4 < -var3) {
         var4 = -var3;
      }

      return var1 + var4;
   }

   private void a(int var1, class_rz var2) {
      this.a(var1, var2.locX, var2.locY + (double)var2.bm() * 0.5D, var2.locZ, var1 == 0 && this.random.nextFloat() < 0.001F);
   }

   private void a(int var1, double var2, double var4, double var6, boolean var8) {
      this.world.a((EntityHuman)null, 1024, new BlockPosition(this), 0);
      double var9 = this.n(var1);
      double var11 = this.o(var1);
      double var13 = this.p(var1);
      double var15 = var2 - var9;
      double var17 = var4 - var11;
      double var19 = var6 - var13;
      class_aad var21 = new class_aad(this.world, this, var15, var17, var19);
      if(var8) {
         var21.a(true);
      }

      var21.locY = var11;
      var21.locX = var9;
      var21.locZ = var13;
      this.world.a((Entity)var21);
   }

   public void a(class_rz var1, float var2) {
      this.a(0, var1);
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b((DamageSource)var1)) {
         return false;
      } else if(var1 != DamageSource.f && !(var1.j() instanceof class_xn)) {
         if(this.cZ() > 0 && var1 != DamageSource.k) {
            return false;
         } else {
            Entity var3;
            if(this.da()) {
               var3 = var1.i();
               if(var3 instanceof class_zl) {
                  return false;
               }
            }

            var3 = var1.j();
            if(var3 != null && !(var3 instanceof EntityHuman) && var3 instanceof class_rz && ((class_rz)var3).bZ() == this.bZ()) {
               return false;
            } else {
               if(this.bD <= 0) {
                  this.bD = 20;
               }

               for(int var4 = 0; var4 < this.bC.length; ++var4) {
                  this.bC[var4] += 3;
               }

               return super.a(var1, var2);
            }
         }
      } else {
         return false;
      }
   }

   protected void b(boolean var1, int var2) {
      class_yc var3 = this.a(Items.cj, 1);
      if(var3 != null) {
         var3.v();
      }

      if(!this.world.E) {
         Iterator var4 = this.world.a(EntityHuman.class, this.bk().b(50.0D, 100.0D, 50.0D)).iterator();

         while(var4.hasNext()) {
            EntityHuman var5 = (EntityHuman)var4.next();
            var5.b((Statistic)AchievementList.J);
         }
      }

   }

   protected void L() {
      this.aU = 0;
   }

   public void e(float var1, float var2) {
   }

   public void c(MobEffect var1) {
   }

   protected void bz() {
      super.bz();
      this.a((class_sk)class_ys.a).a(300.0D);
      this.a((class_sk)class_ys.d).a(0.6000000238418579D);
      this.a((class_sk)class_ys.b).a(40.0D);
      this.a((class_sk)class_ys.g).a(4.0D);
   }

   public int cZ() {
      return ((Integer)this.datawatcher.a(bw)).intValue();
   }

   public void l(int var1) {
      this.datawatcher.b(bw, Integer.valueOf(var1));
   }

   public int m(int var1) {
      return ((Integer)this.datawatcher.a(bv[var1])).intValue();
   }

   public void a(int var1, int var2) {
      this.datawatcher.b(bv[var1], Integer.valueOf(var2));
   }

   public boolean da() {
      return this.bP() <= this.bV() / 2.0F;
   }

   public EnumMonsterType bZ() {
      return EnumMonsterType.UNDEAD;
   }

   protected boolean n(Entity var1) {
      return false;
   }

   public boolean aU() {
      return false;
   }

   class class_a_in_class_xn extends class_tj {
      public class_a_in_class_xn() {
         this.a(7);
      }

      public boolean a() {
         return class_xn.this.cZ() > 0;
      }
   }
}
