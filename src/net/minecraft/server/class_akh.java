package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumColor;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aju;
import net.minecraft.server.class_amf;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_anf;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_rz;

public class class_akh extends class_amf implements class_aju {
   public static final BlockStateInteger a = BlockStateInteger.a("age", 0, 2);
   protected static final AxisAlignedBB[] b = new AxisAlignedBB[]{new AxisAlignedBB(0.6875D, 0.4375D, 0.375D, 0.9375D, 0.75D, 0.625D), new AxisAlignedBB(0.5625D, 0.3125D, 0.3125D, 0.9375D, 0.75D, 0.6875D), new AxisAlignedBB(0.5625D, 0.3125D, 0.3125D, 0.9375D, 0.75D, 0.6875D)};
   protected static final AxisAlignedBB[] c = new AxisAlignedBB[]{new AxisAlignedBB(0.0625D, 0.4375D, 0.375D, 0.3125D, 0.75D, 0.625D), new AxisAlignedBB(0.0625D, 0.3125D, 0.3125D, 0.4375D, 0.75D, 0.6875D), new AxisAlignedBB(0.0625D, 0.3125D, 0.3125D, 0.4375D, 0.75D, 0.6875D)};
   protected static final AxisAlignedBB[] d = new AxisAlignedBB[]{new AxisAlignedBB(0.375D, 0.4375D, 0.0625D, 0.625D, 0.75D, 0.3125D), new AxisAlignedBB(0.3125D, 0.3125D, 0.0625D, 0.6875D, 0.75D, 0.4375D), new AxisAlignedBB(0.3125D, 0.3125D, 0.0625D, 0.6875D, 0.75D, 0.4375D)};
   protected static final AxisAlignedBB[] e = new AxisAlignedBB[]{new AxisAlignedBB(0.375D, 0.4375D, 0.6875D, 0.625D, 0.75D, 0.9375D), new AxisAlignedBB(0.3125D, 0.3125D, 0.5625D, 0.6875D, 0.75D, 0.9375D), new AxisAlignedBB(0.3125D, 0.3125D, 0.5625D, 0.6875D, 0.75D, 0.9375D)};

   public class_akh() {
      super(Material.k);
      this.w(this.A.b().set(D, EnumDirection.NORTH).set(a, Integer.valueOf(0)));
      this.a(true);
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(!this.e(var1, var2, var3)) {
         this.f(var1, var2, var3);
      } else if(var1.r.nextInt(5) == 0) {
         int var5 = ((Integer)var3.get(a)).intValue();
         if(var5 < 2) {
            var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Integer.valueOf(var5 + 1)), 2);
         }
      }

   }

   public boolean e(World var1, BlockPosition var2, IBlockData var3) {
      var2 = var2.a((EnumDirection)var3.get(D));
      IBlockData var4 = var1.getType(var2);
      return var4.getBlock() == Blocks.r && var4.get(class_anf.b) == BlockWood.EnumLogVariant.JUNGLE;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      int var4 = ((Integer)var1.get(a)).intValue();
      switch(class_akh.SyntheticClass_1.a[((EnumDirection)var1.get(D)).ordinal()]) {
      case 1:
         return e[var4];
      case 2:
      default:
         return d[var4];
      case 3:
         return c[var4];
      case 4:
         return b[var4];
      }
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(D, var2.a((EnumDirection)var1.get(D)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(D)));
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, class_rz var4, ItemStack var5) {
      EnumDirection var6 = EnumDirection.a((double)var4.yaw);
      var1.a((BlockPosition)var2, (IBlockData)var3.set(D, var6), 2);
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      if(!var3.k().c()) {
         var3 = EnumDirection.NORTH;
      }

      return this.u().set(D, var3.d()).set(a, Integer.valueOf(0));
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!this.e(var1, var2, var3)) {
         this.f(var1, var2, var3);
      }

   }

   private void f(World var1, BlockPosition var2, IBlockData var3) {
      var1.a((BlockPosition)var2, (IBlockData)Blocks.AIR.u(), 3);
      this.b(var1, var2, var3, 0);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, float var4, int var5) {
      int var6 = ((Integer)var3.get(a)).intValue();
      byte var7 = 1;
      if(var6 >= 2) {
         var7 = 3;
      }

      for(int var8 = 0; var8 < var7; ++var8) {
         a((World)var1, (BlockPosition)var2, (ItemStack)(new ItemStack(Items.bd, 1, EnumColor.BROWN.b())));
      }

   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Items.bd, 1, EnumColor.BROWN.b());
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, boolean var4) {
      return ((Integer)var3.get(a)).intValue() < 2;
   }

   public boolean a(World var1, Random var2, BlockPosition var3, IBlockData var4) {
      return true;
   }

   public void b(World var1, Random var2, BlockPosition var3, IBlockData var4) {
      var1.a((BlockPosition)var3, (IBlockData)var4.set(a, Integer.valueOf(((Integer)var4.get(a)).intValue() + 1)), 2);
   }

   public IBlockData a(int var1) {
      return this.u().set(D, EnumDirection.b(var1)).set(a, Integer.valueOf((var1 & 15) >> 2));
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((EnumDirection)var1.get(D)).b();
      var3 |= ((Integer)var1.get(a)).intValue() << 2;
      return var3;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{D, a});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumDirection.values().length];

      static {
         try {
            a[EnumDirection.SOUTH.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.NORTH.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.EAST.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
