package net.minecraft.server;

import com.google.common.collect.Sets;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import net.minecraft.server.Block;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityFallingBlock;
import net.minecraft.server.EntityFireball;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityMinecartAbstract;
import net.minecraft.server.EntityPainting;
import net.minecraft.server.EntityTracker;
import net.minecraft.server.Items;
import net.minecraft.server.MobEffect;
import net.minecraft.server.PacketPlayOutEntity;
import net.minecraft.server.PacketPlayOutUpdateAttributes;
import net.minecraft.server.World;
import net.minecraft.server.class_aaa;
import net.minecraft.server.class_aab;
import net.minecraft.server.class_aac;
import net.minecraft.server.class_aad;
import net.minecraft.server.EntityBoat;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_adv;
import net.minecraft.server.class_ayy;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutSpawnEntity;
import net.minecraft.server.PacketPlayOutSpawnEntityExperienceOrb;
import net.minecraft.server.PacketPlayOutEntityLiving;
import net.minecraft.server.PacketPlayOutSpawnEntityPainting;
import net.minecraft.server.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.PacketPlayOutBed;
import net.minecraft.server.PacketPlayOutEntityHeadRotation;
import net.minecraft.server.PacketPlayOutEntityMetadata;
import net.minecraft.server.PacketPlayOutEntityVelocity;
import net.minecraft.server.PacketPlayOutEntityEquipment;
import net.minecraft.server.PacketPlayOutPlayerRefresh;
import net.minecraft.server.PacketPlayOutEntityTeleport;
import net.minecraft.server.PacketPlayOutEntityEffect;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_ro;
import net.minecraft.server.class_rp;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rw;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sq;
import net.minecraft.server.class_ws;
import net.minecraft.server.class_xp;
import net.minecraft.server.class_xr;
import net.minecraft.server.class_xs;
import net.minecraft.server.class_xv;
import net.minecraft.server.class_yc;
import net.minecraft.server.class_yd;
import net.minecraft.server.class_zl;
import net.minecraft.server.class_zm;
import net.minecraft.server.class_zn;
import net.minecraft.server.class_zp;
import net.minecraft.server.class_zt;
import net.minecraft.server.class_zu;
import net.minecraft.server.class_zv;
import net.minecraft.server.class_zw;
import net.minecraft.server.class_zy;
import net.minecraft.server.class_zz;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_lt {
   private static final Logger c = LogManager.getLogger();
   private Entity d;
   private int e;
   private int f;
   private int g;
   private long h;
   private long i;
   private long j;
   private int k;
   private int l;
   private int m;
   private double n;
   private double o;
   private double p;
   public int a;
   private double q;
   private double r;
   private double s;
   private boolean t;
   private boolean u;
   private int v;
   private List w = Collections.emptyList();
   private boolean x;
   private boolean y;
   public boolean b;
   private Set z = Sets.newHashSet();

   public class_lt(Entity var1, int var2, int var3, int var4, boolean var5) {
      this.d = var1;
      this.e = var2;
      this.f = var3;
      this.g = var4;
      this.u = var5;
      this.h = EntityTracker.a(var1.locX);
      this.i = EntityTracker.a(var1.locY);
      this.j = EntityTracker.a(var1.locZ);
      this.k = MathHelper.d(var1.yaw * 256.0F / 360.0F);
      this.l = MathHelper.d(var1.pitch * 256.0F / 360.0F);
      this.m = MathHelper.d(var1.aR() * 256.0F / 360.0F);
      this.y = var1.onGround;
   }

   public boolean equals(Object var1) {
      return var1 instanceof class_lt?((class_lt)var1).d.getId() == this.d.getId():false;
   }

   public int hashCode() {
      return this.d.getId();
   }

   public void a(List var1) {
      this.b = false;
      if(!this.t || this.d.e(this.q, this.r, this.s) > 16.0D) {
         this.q = this.d.locX;
         this.r = this.d.locY;
         this.s = this.d.locZ;
         this.t = true;
         this.b = true;
         this.b(var1);
      }

      List var2 = this.d.bt();
      if(!var2.equals(this.w)) {
         this.w = var2;
         this.a((Packet)(new PacketPlayOutPlayerRefresh(this.d)));
      }

      if(this.d instanceof class_xr && this.a % 10 == 0) {
         class_xr var3 = (class_xr)this.d;
         ItemStack var4 = var3.r();
         if(var4 != null && var4.b() instanceof class_adv) {
            class_ayy var5 = Items.bk.a(var4, this.d.world);
            Iterator var6 = var1.iterator();

            while(var6.hasNext()) {
               EntityHuman var7 = (EntityHuman)var6.next();
               EntityPlayer var8 = (EntityPlayer)var7;
               var5.a(var8, var4);
               Packet var9 = Items.bk.a((ItemStack)var4, (World)this.d.world, (EntityHuman)var8);
               if(var9 != null) {
                  var8.a.a(var9);
               }
            }
         }

         this.d();
      }

      if(this.a % this.g == 0 || this.d.ai || this.d.Q().a()) {
         int var31;
         if(!this.d.aH()) {
            ++this.v;
            long var32 = EntityTracker.a(this.d.locX);
            long var35 = EntityTracker.a(this.d.locY);
            long var36 = EntityTracker.a(this.d.locZ);
            int var37 = MathHelper.d(this.d.yaw * 256.0F / 360.0F);
            int var10 = MathHelper.d(this.d.pitch * 256.0F / 360.0F);
            long var11 = var32 - this.h;
            long var13 = var35 - this.i;
            long var15 = var36 - this.j;
            Object var17 = null;
            boolean var18 = var11 * var11 + var13 * var13 + var15 * var15 >= 128L || this.a % 60 == 0;
            boolean var19 = Math.abs(var37 - this.k) >= 1 || Math.abs(var10 - this.l) >= 1;
            if(this.a > 0 || this.d instanceof class_zl) {
               if(var11 >= -32768L && var11 < 32768L && var13 >= -32768L && var13 < 32768L && var15 >= -32768L && var15 < 32768L && this.v <= 400 && !this.x && this.y == this.d.onGround) {
                  if((!var18 || !var19) && !(this.d instanceof class_zl)) {
                     if(var18) {
                        var17 = new PacketPlayOutEntity.class_a_in_class_gu(this.d.getId(), var11, var13, var15, this.d.onGround);
                     } else if(var19) {
                        var17 = new PacketPlayOutEntity.class_c_in_class_gu(this.d.getId(), (byte)var37, (byte)var10, this.d.onGround);
                     }
                  } else {
                     var17 = new PacketPlayOutEntity.class_b_in_class_gu(this.d.getId(), var11, var13, var15, (byte)var37, (byte)var10, this.d.onGround);
                  }
               } else {
                  this.y = this.d.onGround;
                  this.v = 0;
                  this.c();
                  var17 = new PacketPlayOutEntityTeleport(this.d);
               }
            }

            boolean var20 = this.u;
            if(this.d instanceof class_rz && ((class_rz)this.d).cA()) {
               var20 = true;
            }

            if(var20) {
               double var21 = this.d.motX - this.n;
               double var23 = this.d.motY - this.o;
               double var25 = this.d.motZ - this.p;
               double var27 = 0.02D;
               double var29 = var21 * var21 + var23 * var23 + var25 * var25;
               if(var29 > 4.0E-4D || var29 > 0.0D && this.d.motX == 0.0D && this.d.motY == 0.0D && this.d.motZ == 0.0D) {
                  this.n = this.d.motX;
                  this.o = this.d.motY;
                  this.p = this.d.motZ;
                  this.a((Packet)(new PacketPlayOutEntityVelocity(this.d.getId(), this.n, this.o, this.p)));
               }
            }

            if(var17 != null) {
               this.a((Packet)var17);
            }

            this.d();
            if(var18) {
               this.h = var32;
               this.i = var35;
               this.j = var36;
            }

            if(var19) {
               this.k = var37;
               this.l = var10;
            }

            this.x = false;
         } else {
            var31 = MathHelper.d(this.d.yaw * 256.0F / 360.0F);
            int var33 = MathHelper.d(this.d.pitch * 256.0F / 360.0F);
            boolean var34 = Math.abs(var31 - this.k) >= 1 || Math.abs(var33 - this.l) >= 1;
            if(var34) {
               this.a((Packet)(new PacketPlayOutEntity.class_c_in_class_gu(this.d.getId(), (byte)var31, (byte)var33, this.d.onGround)));
               this.k = var31;
               this.l = var33;
            }

            this.h = EntityTracker.a(this.d.locX);
            this.i = EntityTracker.a(this.d.locY);
            this.j = EntityTracker.a(this.d.locZ);
            this.d();
            this.x = true;
         }

         var31 = MathHelper.d(this.d.aR() * 256.0F / 360.0F);
         if(Math.abs(var31 - this.m) >= 1) {
            this.a((Packet)(new PacketPlayOutEntityHeadRotation(this.d, (byte)var31)));
            this.m = var31;
         }

         this.d.ai = false;
      }

      ++this.a;
      if(this.d.velocityChanged) {
         this.b((Packet)(new PacketPlayOutEntityVelocity(this.d)));
         this.d.velocityChanged = false;
      }

   }

   private void d() {
      DataWatcher var1 = this.d.Q();
      if(var1.a()) {
         this.b((Packet)(new PacketPlayOutEntityMetadata(this.d.getId(), var1, false)));
      }

      if(this.d instanceof class_rz) {
         class_sq var2 = (class_sq)((class_rz)this.d).bY();
         Set var3 = var2.b();
         if(!var3.isEmpty()) {
            this.b((Packet)(new PacketPlayOutUpdateAttributes(this.d.getId(), var3)));
         }

         var3.clear();
      }

   }

   public void a(Packet var1) {
      Iterator var2 = this.z.iterator();

      while(var2.hasNext()) {
         EntityPlayer var3 = (EntityPlayer)var2.next();
         var3.a.a(var1);
      }

   }

   public void b(Packet var1) {
      this.a(var1);
      if(this.d instanceof EntityPlayer) {
         ((EntityPlayer)this.d).a.a(var1);
      }

   }

   public void a() {
      Iterator var1 = this.z.iterator();

      while(var1.hasNext()) {
         EntityPlayer var2 = (EntityPlayer)var1.next();
         this.d.c(var2);
         var2.c(this.d);
      }

   }

   public void a(EntityPlayer var1) {
      if(this.z.contains(var1)) {
         this.d.c(var1);
         var1.c(this.d);
         this.z.remove(var1);
      }

   }

   public void b(EntityPlayer var1) {
      if(var1 != this.d) {
         if(this.c(var1)) {
            if(!this.z.contains(var1) && (this.e(var1) || this.d.attachedToPlayer)) {
               this.z.add(var1);
               Packet var2 = this.e();
               var1.a.a(var2);
               if(!this.d.Q().d()) {
                  var1.a.a((Packet)(new PacketPlayOutEntityMetadata(this.d.getId(), this.d.Q(), true)));
               }

               boolean var3 = this.u;
               if(this.d instanceof class_rz) {
                  class_sq var4 = (class_sq)((class_rz)this.d).bY();
                  Collection var5 = var4.c();
                  if(!var5.isEmpty()) {
                     var1.a.a((Packet)(new PacketPlayOutUpdateAttributes(this.d.getId(), var5)));
                  }

                  if(((class_rz)this.d).cA()) {
                     var3 = true;
                  }
               }

               this.n = this.d.motX;
               this.o = this.d.motY;
               this.p = this.d.motZ;
               if(var3 && !(var2 instanceof PacketPlayOutEntityLiving)) {
                  var1.a.a((Packet)(new PacketPlayOutEntityVelocity(this.d.getId(), this.d.motX, this.d.motY, this.d.motZ)));
               }

               if(this.d instanceof class_rz) {
                  EnumInventorySlot[] var9 = EnumInventorySlot.values();
                  int var12 = var9.length;

                  for(int var6 = 0; var6 < var12; ++var6) {
                     EnumInventorySlot var7 = var9[var6];
                     ItemStack var8 = ((class_rz)this.d).a(var7);
                     if(var8 != null) {
                        var1.a.a((Packet)(new PacketPlayOutEntityEquipment(this.d.getId(), var7, var8)));
                     }
                  }
               }

               if(this.d instanceof EntityHuman) {
                  EntityHuman var10 = (EntityHuman)this.d;
                  if(var10.ck()) {
                     var1.a.a((Packet)(new PacketPlayOutBed(var10, new BlockPosition(this.d))));
                  }
               }

               if(this.d instanceof class_rz) {
                  class_rz var11 = (class_rz)this.d;
                  Iterator var13 = var11.bN().iterator();

                  while(var13.hasNext()) {
                     MobEffect var14 = (MobEffect)var13.next();
                     var1.a.a((Packet)(new PacketPlayOutEntityEffect(this.d.getId(), var14)));
                  }
               }

               this.d.b(var1);
               var1.d(this.d);
            }
         } else if(this.z.contains(var1)) {
            this.z.remove(var1);
            this.d.c(var1);
            var1.c(this.d);
         }

      }
   }

   public boolean c(EntityPlayer var1) {
      double var2 = var1.locX - (double)this.h / 4096.0D;
      double var4 = var1.locZ - (double)this.j / 4096.0D;
      int var6 = Math.min(this.e, this.f);
      return var2 >= (double)(-var6) && var2 <= (double)var6 && var4 >= (double)(-var6) && var4 <= (double)var6 && this.d.a(var1);
   }

   private boolean e(EntityPlayer var1) {
      return var1.x().w().a(var1, this.d.ab, this.d.ad);
   }

   public void b(List var1) {
      for(int var2 = 0; var2 < var1.size(); ++var2) {
         this.b((EntityPlayer)var1.get(var2));
      }

   }

   private Packet e() {
      if(this.d.dead) {
         c.warn("Fetching addPacket for removed entity");
      }

      if(this.d instanceof class_yc) {
         return new PacketPlayOutSpawnEntity(this.d, 2, 1);
      } else if(this.d instanceof EntityPlayer) {
         return new PacketPlayOutNamedEntitySpawn((EntityHuman)this.d);
      } else if(this.d instanceof EntityMinecartAbstract) {
         EntityMinecartAbstract var10 = (EntityMinecartAbstract)this.d;
         return new PacketPlayOutSpawnEntity(this.d, 10, var10.v().a());
      } else if(this.d instanceof EntityBoat) {
         return new PacketPlayOutSpawnEntity(this.d, 1);
      } else if(this.d instanceof class_rp) {
         this.m = MathHelper.d(this.d.aR() * 256.0F / 360.0F);
         return new PacketPlayOutEntityLiving((class_rz)this.d);
      } else if(this.d instanceof class_xv) {
         EntityHuman var9 = ((class_xv)this.d).a;
         return new PacketPlayOutSpawnEntity(this.d, 90, var9 != null?var9.getId():this.d.getId());
      } else {
         Entity var8;
         if(this.d instanceof class_zw) {
            var8 = ((class_zw)this.d).e;
            return new PacketPlayOutSpawnEntity(this.d, 91, 1 + (var8 != null?var8.getId():this.d.getId()));
         } else if(this.d instanceof class_aac) {
            var8 = ((class_zl)this.d).e;
            return new PacketPlayOutSpawnEntity(this.d, 60, 1 + (var8 != null?var8.getId():this.d.getId()));
         } else if(this.d instanceof class_zv) {
            return new PacketPlayOutSpawnEntity(this.d, 61);
         } else if(this.d instanceof class_aab) {
            return new PacketPlayOutSpawnEntity(this.d, 73);
         } else if(this.d instanceof class_aaa) {
            return new PacketPlayOutSpawnEntity(this.d, 75);
         } else if(this.d instanceof class_zz) {
            return new PacketPlayOutSpawnEntity(this.d, 65);
         } else if(this.d instanceof class_zn) {
            return new PacketPlayOutSpawnEntity(this.d, 72);
         } else if(this.d instanceof class_zp) {
            return new PacketPlayOutSpawnEntity(this.d, 76);
         } else {
            PacketPlayOutSpawnEntity var2;
            if(this.d instanceof EntityFireball) {
               EntityFireball var7 = (EntityFireball)this.d;
               var2 = null;
               byte var3 = 63;
               if(this.d instanceof class_zu) {
                  var3 = 64;
               } else if(this.d instanceof class_zm) {
                  var3 = 93;
               } else if(this.d instanceof class_aad) {
                  var3 = 66;
               }

               if(var7.a != null) {
                  var2 = new PacketPlayOutSpawnEntity(this.d, var3, ((EntityFireball)this.d).a.getId());
               } else {
                  var2 = new PacketPlayOutSpawnEntity(this.d, var3, 0);
               }

               var2.a((int)(var7.b * 8000.0D));
               var2.b((int)(var7.c * 8000.0D));
               var2.c((int)(var7.d * 8000.0D));
               return var2;
            } else if(this.d instanceof class_zt) {
               PacketPlayOutSpawnEntity var6 = new PacketPlayOutSpawnEntity(this.d, 67, 0);
               var6.a((int)(this.d.motX * 8000.0D));
               var6.b((int)(this.d.motY * 8000.0D));
               var6.c((int)(this.d.motZ * 8000.0D));
               return var6;
            } else if(this.d instanceof class_zy) {
               return new PacketPlayOutSpawnEntity(this.d, 62);
            } else if(this.d instanceof class_yd) {
               return new PacketPlayOutSpawnEntity(this.d, 50);
            } else if(this.d instanceof class_ws) {
               return new PacketPlayOutSpawnEntity(this.d, 51);
            } else if(this.d instanceof EntityFallingBlock) {
               EntityFallingBlock var5 = (EntityFallingBlock)this.d;
               return new PacketPlayOutSpawnEntity(this.d, 70, Block.j(var5.l()));
            } else if(this.d instanceof class_xp) {
               return new PacketPlayOutSpawnEntity(this.d, 78);
            } else if(this.d instanceof EntityPainting) {
               return new PacketPlayOutSpawnEntityPainting((EntityPainting)this.d);
            } else if(this.d instanceof class_xr) {
               class_xr var4 = (class_xr)this.d;
               var2 = new PacketPlayOutSpawnEntity(this.d, 71, var4.b.b(), var4.q());
               return var2;
            } else if(this.d instanceof class_xs) {
               class_xs var1 = (class_xs)this.d;
               var2 = new PacketPlayOutSpawnEntity(this.d, 77, 0, var1.q());
               return var2;
            } else if(this.d instanceof class_rw) {
               return new PacketPlayOutSpawnEntityExperienceOrb((class_rw)this.d);
            } else if(this.d instanceof class_ro) {
               return new PacketPlayOutSpawnEntity(this.d, 3);
            } else {
               throw new IllegalArgumentException("Don\'t know how to add " + this.d.getClass() + "!");
            }
         }
      }
   }

   public void d(EntityPlayer var1) {
      if(this.z.contains(var1)) {
         this.z.remove(var1);
         this.d.c(var1);
         var1.c(this.d);
      }

   }

   public Entity b() {
      return this.d;
   }

   public void a(int var1) {
      this.f = var1;
   }

   public void c() {
      this.t = false;
   }
}
