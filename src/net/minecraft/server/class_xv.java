package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Items;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agn;
import net.minecraft.server.Material;
import net.minecraft.server.class_azs;
import net.minecraft.server.class_azy;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rw;
import net.minecraft.server.class_yc;

public class class_xv extends Entity {
   private static final class_ke c = DataWatcher.a(class_xv.class, class_kg.b);
   private int d = -1;
   private int e = -1;
   private int f = -1;
   private Block g;
   private boolean h;
   public EntityHuman a;
   private int as;
   private int at;
   private int au;
   private int av;
   private int aw;
   private float ax;
   public Entity b;
   private int ay;
   private double az;
   private double aA;
   private double aB;
   private double aC;
   private double aD;

   public class_xv(World var1) {
      super(var1);
      this.a(0.25F, 0.25F);
      this.ah = true;
   }

   public class_xv(World var1, EntityHuman var2) {
      super(var1);
      this.ah = true;
      this.a = var2;
      this.a.bP = this;
      this.a(0.25F, 0.25F);
      this.b(var2.locX, var2.locY + (double)var2.bm(), var2.locZ, var2.yaw, var2.pitch);
      this.locX -= (double)(MathHelper.b(this.yaw * 0.017453292F) * 0.16F);
      this.locY -= 0.10000000149011612D;
      this.locZ -= (double)(MathHelper.a(this.yaw * 0.017453292F) * 0.16F);
      this.b(this.locX, this.locY, this.locZ);
      float var3 = 0.4F;
      this.motX = (double)(-MathHelper.a(this.yaw * 0.017453292F) * MathHelper.b(this.pitch * 0.017453292F) * var3);
      this.motZ = (double)(MathHelper.b(this.yaw * 0.017453292F) * MathHelper.b(this.pitch * 0.017453292F) * var3);
      this.motY = (double)(-MathHelper.a(this.pitch * 0.017453292F) * var3);
      this.c(this.motX, this.motY, this.motZ, 1.5F, 1.0F);
   }

   protected void i() {
      this.Q().a((class_ke)c, (Object)Integer.valueOf(0));
   }

   public void a(class_ke var1) {
      if(c.equals(var1)) {
         int var2 = ((Integer)this.Q().a(c)).intValue();
         if(var2 > 0 && this.b != null) {
            this.b = null;
         }
      }

      super.a(var1);
   }

   public void c(double var1, double var3, double var5, float var7, float var8) {
      float var9 = MathHelper.a(var1 * var1 + var3 * var3 + var5 * var5);
      var1 /= (double)var9;
      var3 /= (double)var9;
      var5 /= (double)var9;
      var1 += this.random.nextGaussian() * 0.007499999832361937D * (double)var8;
      var3 += this.random.nextGaussian() * 0.007499999832361937D * (double)var8;
      var5 += this.random.nextGaussian() * 0.007499999832361937D * (double)var8;
      var1 *= (double)var7;
      var3 *= (double)var7;
      var5 *= (double)var7;
      this.motX = var1;
      this.motY = var3;
      this.motZ = var5;
      float var10 = MathHelper.a(var1 * var1 + var5 * var5);
      this.lastYaw = this.yaw = (float)(MathHelper.b(var1, var5) * 57.2957763671875D);
      this.lastPitch = this.pitch = (float)(MathHelper.b(var3, (double)var10) * 57.2957763671875D);
      this.as = 0;
   }

   public void m() {
      super.m();
      if(this.world.E) {
         int var1 = ((Integer)this.Q().a(c)).intValue();
         if(var1 > 0 && this.b == null) {
            this.b = this.world.a(var1 - 1);
         }
      } else {
         ItemStack var21 = this.a.ca();
         if(this.a.dead || !this.a.at() || var21 == null || var21.b() != Items.aY || this.h(this.a) > 1024.0D) {
            this.S();
            this.a.bP = null;
            return;
         }
      }

      if(this.b != null) {
         if(!this.b.dead) {
            this.locX = this.b.locX;
            double var10002 = (double)this.b.length;
            this.locY = this.b.bk().b + var10002 * 0.8D;
            this.locZ = this.b.locZ;
            return;
         }

         this.b = null;
      }

      if(this.ay > 0) {
         double var24 = this.locX + (this.az - this.locX) / (double)this.ay;
         double var27 = this.locY + (this.aA - this.locY) / (double)this.ay;
         double var29 = this.locZ + (this.aB - this.locZ) / (double)this.ay;
         double var35 = MathHelper.g(this.aC - (double)this.yaw);
         this.yaw = (float)((double)this.yaw + var35 / (double)this.ay);
         this.pitch = (float)((double)this.pitch + (this.aD - (double)this.pitch) / (double)this.ay);
         --this.ay;
         this.b(var24, var27, var29);
         this.b(this.yaw, this.pitch);
      } else {
         if(this.h) {
            if(this.world.getType(new BlockPosition(this.d, this.e, this.f)).getBlock() == this.g) {
               ++this.as;
               if(this.as == 1200) {
                  this.S();
               }

               return;
            }

            this.h = false;
            this.motX *= (double)(this.random.nextFloat() * 0.2F);
            this.motY *= (double)(this.random.nextFloat() * 0.2F);
            this.motZ *= (double)(this.random.nextFloat() * 0.2F);
            this.as = 0;
            this.at = 0;
         } else {
            ++this.at;
         }

         double var6;
         double var12;
         if(!this.world.E) {
            Vec3D var22 = new Vec3D(this.locX, this.locY, this.locZ);
            Vec3D var2 = new Vec3D(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
            MovingObjectPosition var3 = this.world.a(var22, var2);
            var22 = new Vec3D(this.locX, this.locY, this.locZ);
            var2 = new Vec3D(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
            if(var3 != null) {
               var2 = new Vec3D(var3.c.b, var3.c.c, var3.c.d);
            }

            Entity var4 = null;
            List var5 = this.world.b((Entity)this, (AxisAlignedBB)this.bk().a(this.motX, this.motY, this.motZ).g(1.0D));
            var6 = 0.0D;

            for(int var8 = 0; var8 < var5.size(); ++var8) {
               Entity var9 = (Entity)var5.get(var8);
               if(var9.ao() && (var9 != this.a || this.at >= 5)) {
                  AxisAlignedBB var10 = var9.bk().g(0.30000001192092896D);
                  MovingObjectPosition var11 = var10.a(var22, var2);
                  if(var11 != null) {
                     var12 = var22.g(var11.c);
                     if(var12 < var6 || var6 == 0.0D) {
                        var4 = var9;
                        var6 = var12;
                     }
                  }
               }
            }

            if(var4 != null) {
               var3 = new MovingObjectPosition(var4);
            }

            if(var3 != null) {
               if(var3.d != null) {
                  this.b = var3.d;
                  this.Q().b(c, Integer.valueOf(this.b.getId() + 1));
               } else {
                  this.h = true;
               }
            }
         }

         if(!this.h) {
            this.d(this.motX, this.motY, this.motZ);
            float var23 = MathHelper.a(this.motX * this.motX + this.motZ * this.motZ);
            this.yaw = (float)(MathHelper.b(this.motX, this.motZ) * 57.2957763671875D);

            for(this.pitch = (float)(MathHelper.b(this.motY, (double)var23) * 57.2957763671875D); this.pitch - this.lastPitch < -180.0F; this.lastPitch -= 360.0F) {
               ;
            }

            while(this.pitch - this.lastPitch >= 180.0F) {
               this.lastPitch += 360.0F;
            }

            while(this.yaw - this.lastYaw < -180.0F) {
               this.lastYaw -= 360.0F;
            }

            while(this.yaw - this.lastYaw >= 180.0F) {
               this.lastYaw += 360.0F;
            }

            this.pitch = this.lastPitch + (this.pitch - this.lastPitch) * 0.2F;
            this.yaw = this.lastYaw + (this.yaw - this.lastYaw) * 0.2F;
            float var25 = 0.92F;
            if(this.onGround || this.positionChanged) {
               var25 = 0.5F;
            }

            byte var26 = 5;
            double var28 = 0.0D;

            for(int var30 = 0; var30 < var26; ++var30) {
               AxisAlignedBB var7 = this.bk();
               double var33 = var7.e - var7.b;
               double var37 = var7.b + var33 * (double)var30 / (double)var26;
               var12 = var7.b + var33 * (double)(var30 + 1) / (double)var26;
               AxisAlignedBB var14 = new AxisAlignedBB(var7.a, var37, var7.c, var7.d, var12, var7.f);
               if(this.world.b(var14, Material.h)) {
                  var28 += 1.0D / (double)var26;
               }
            }

            if(!this.world.E && var28 > 0.0D) {
               WorldServer var31 = (WorldServer)this.world;
               int var32 = 1;
               BlockPosition var34 = (new BlockPosition(this)).a();
               if(this.random.nextFloat() < 0.25F && this.world.B(var34)) {
                  var32 = 2;
               }

               if(this.random.nextFloat() < 0.5F && !this.world.h(var34)) {
                  --var32;
               }

               if(this.au > 0) {
                  --this.au;
                  if(this.au <= 0) {
                     this.av = 0;
                     this.aw = 0;
                  }
               } else {
                  double var16;
                  Block var18;
                  float var36;
                  float var38;
                  float var39;
                  double var40;
                  if(this.aw > 0) {
                     this.aw -= var32;
                     if(this.aw <= 0) {
                        this.motY -= 0.20000000298023224D;
                        this.a(class_ng.F, 0.25F, 1.0F + (this.random.nextFloat() - this.random.nextFloat()) * 0.4F);
                        var36 = (float)MathHelper.c(this.bk().b);
                        var31.a(EnumParticle.WATER_BUBBLE, this.locX, (double)(var36 + 1.0F), this.locZ, (int)(1.0F + this.width * 20.0F), (double)this.width, 0.0D, (double)this.width, 0.20000000298023224D, new int[0]);
                        var31.a(EnumParticle.WATER_WAKE, this.locX, (double)(var36 + 1.0F), this.locZ, (int)(1.0F + this.width * 20.0F), (double)this.width, 0.0D, (double)this.width, 0.20000000298023224D, new int[0]);
                        this.au = MathHelper.a((Random)this.random, 10, 30);
                     } else {
                        this.ax = (float)((double)this.ax + this.random.nextGaussian() * 4.0D);
                        var36 = this.ax * 0.017453292F;
                        var38 = MathHelper.a(var36);
                        var39 = MathHelper.b(var36);
                        var12 = this.locX + (double)(var38 * (float)this.aw * 0.1F);
                        var40 = (double)((float)MathHelper.c(this.bk().b) + 1.0F);
                        var16 = this.locZ + (double)(var39 * (float)this.aw * 0.1F);
                        var18 = var31.getType(new BlockPosition((int)var12, (int)var40 - 1, (int)var16)).getBlock();
                        if(var18 == Blocks.j || var18 == Blocks.i) {
                           if(this.random.nextFloat() < 0.15F) {
                              var31.a(EnumParticle.WATER_BUBBLE, var12, var40 - 0.10000000149011612D, var16, 1, (double)var38, 0.1D, (double)var39, 0.0D, new int[0]);
                           }

                           float var19 = var38 * 0.04F;
                           float var20 = var39 * 0.04F;
                           var31.a(EnumParticle.WATER_WAKE, var12, var40, var16, 0, (double)var20, 0.01D, (double)(-var19), 1.0D, new int[0]);
                           var31.a(EnumParticle.WATER_WAKE, var12, var40, var16, 0, (double)(-var20), 0.01D, (double)var19, 1.0D, new int[0]);
                        }
                     }
                  } else if(this.av > 0) {
                     this.av -= var32;
                     var36 = 0.15F;
                     if(this.av < 20) {
                        var36 = (float)((double)var36 + (double)(20 - this.av) * 0.05D);
                     } else if(this.av < 40) {
                        var36 = (float)((double)var36 + (double)(40 - this.av) * 0.02D);
                     } else if(this.av < 60) {
                        var36 = (float)((double)var36 + (double)(60 - this.av) * 0.01D);
                     }

                     if(this.random.nextFloat() < var36) {
                        var38 = MathHelper.a(this.random, 0.0F, 360.0F) * 0.017453292F;
                        var39 = MathHelper.a(this.random, 25.0F, 60.0F);
                        var12 = this.locX + (double)(MathHelper.a(var38) * var39 * 0.1F);
                        var40 = (double)((float)MathHelper.c(this.bk().b) + 1.0F);
                        var16 = this.locZ + (double)(MathHelper.b(var38) * var39 * 0.1F);
                        var18 = var31.getType(new BlockPosition((int)var12, (int)var40 - 1, (int)var16)).getBlock();
                        if(var18 == Blocks.j || var18 == Blocks.i) {
                           var31.a(EnumParticle.WATER_SPLASH, var12, var40, var16, 2 + this.random.nextInt(2), 0.10000000149011612D, 0.0D, 0.10000000149011612D, 0.0D, new int[0]);
                        }
                     }

                     if(this.av <= 0) {
                        this.ax = MathHelper.a(this.random, 0.0F, 360.0F);
                        this.aw = MathHelper.a((Random)this.random, 20, 80);
                     }
                  } else {
                     this.av = MathHelper.a((Random)this.random, 100, 900);
                     this.av -= class_agn.g(this.a) * 20 * 5;
                  }
               }

               if(this.au > 0) {
                  this.motY -= (double)(this.random.nextFloat() * this.random.nextFloat() * this.random.nextFloat()) * 0.2D;
               }
            }

            var6 = var28 * 2.0D - 1.0D;
            this.motY += 0.03999999910593033D * var6;
            if(var28 > 0.0D) {
               var25 = (float)((double)var25 * 0.9D);
               this.motY *= 0.8D;
            }

            this.motX *= (double)var25;
            this.motY *= (double)var25;
            this.motZ *= (double)var25;
            this.b(this.locX, this.locY, this.locZ);
         }
      }
   }

   public void b(NBTTagCompound var1) {
      var1.a("xTile", this.d);
      var1.a("yTile", this.e);
      var1.a("zTile", this.f);
      class_kk var2 = (class_kk)Block.h.b(this.g);
      var1.a("inTile", var2 == null?"":var2.toString());
      var1.a("inGround", (byte)(this.h?1:0));
   }

   public void a(NBTTagCompound var1) {
      this.d = var1.h("xTile");
      this.e = var1.h("yTile");
      this.f = var1.h("zTile");
      if(var1.b("inTile", 8)) {
         this.g = Block.b(var1.l("inTile"));
      } else {
         this.g = Block.b(var1.f("inTile") & 255);
      }

      this.h = var1.f("inGround") == 1;
   }

   public int j() {
      if(this.world.E) {
         return 0;
      } else {
         int var1 = 0;
         if(this.b != null) {
            this.k();
            this.world.a((Entity)this, (byte)31);
            var1 = this.b instanceof class_yc?3:5;
         } else if(this.au > 0) {
            class_azy.class_a_in_class_azy var2 = new class_azy.class_a_in_class_azy((WorldServer)this.world);
            var2.a((float)class_agn.f(this.a) + this.a.da());
            Iterator var3 = this.world.ak().a(class_azs.al).a(this.random, var2.a()).iterator();

            while(var3.hasNext()) {
               ItemStack var4 = (ItemStack)var3.next();
               class_yc var5 = new class_yc(this.world, this.locX, this.locY, this.locZ, var4);
               double var6 = this.a.locX - this.locX;
               double var8 = this.a.locY - this.locY;
               double var10 = this.a.locZ - this.locZ;
               double var12 = (double)MathHelper.a(var6 * var6 + var8 * var8 + var10 * var10);
               double var14 = 0.1D;
               var5.motX = var6 * var14;
               var5.motY = var8 * var14 + (double)MathHelper.a(var12) * 0.08D;
               var5.motZ = var10 * var14;
               this.world.a((Entity)var5);
               this.a.world.a((Entity)(new class_rw(this.a.world, this.a.locX, this.a.locY + 0.5D, this.a.locZ + 0.5D, this.random.nextInt(6) + 1)));
            }

            var1 = 1;
         }

         if(this.h) {
            var1 = 2;
         }

         this.S();
         this.a.bP = null;
         return var1;
      }
   }

   protected void k() {
      double var1 = this.a.locX - this.locX;
      double var3 = this.a.locY - this.locY;
      double var5 = this.a.locZ - this.locZ;
      double var7 = (double)MathHelper.a(var1 * var1 + var3 * var3 + var5 * var5);
      double var9 = 0.1D;
      this.b.motX += var1 * var9;
      this.b.motY += var3 * var9 + (double)MathHelper.a(var7) * 0.08D;
      this.b.motZ += var5 * var9;
   }

   public void S() {
      super.S();
      if(this.a != null) {
         this.a.bP = null;
      }

   }
}
