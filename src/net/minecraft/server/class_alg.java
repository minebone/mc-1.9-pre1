package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityFallingBlock;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_alg extends Block {
   public static boolean f;

   public class_alg() {
      super(Material.p);
      this.a((CreativeModeTab)CreativeModeTab.b);
   }

   public class_alg(Material var1) {
      super(var1);
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      var1.a((BlockPosition)var2, (Block)this, this.a(var1));
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      var1.a((BlockPosition)var2, (Block)this, this.a(var1));
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(!var1.E) {
         this.b(var1, var2);
      }

   }

   private void b(World var1, BlockPosition var2) {
      if(i(var1.getType(var2.b())) && var2.q() >= 0) {
         byte var3 = 32;
         if(!f && var1.a(var2.a(-var3, -var3, -var3), var2.a(var3, var3, var3))) {
            if(!var1.E) {
               EntityFallingBlock var5 = new EntityFallingBlock(var1, (double)var2.p() + 0.5D, (double)var2.q(), (double)var2.r() + 0.5D, var1.getType(var2));
               this.a(var5);
               var1.a((Entity)var5);
            }
         } else {
            var1.g(var2);

            BlockPosition var4;
            for(var4 = var2.b(); i(var1.getType(var4)) && var4.q() > 0; var4 = var4.b()) {
               ;
            }

            if(var4.q() > 0) {
               var1.a(var4.a(), this.u());
            }
         }

      }
   }

   protected void a(EntityFallingBlock var1) {
   }

   public int a(World var1) {
      return 2;
   }

   public static boolean i(IBlockData var0) {
      Block var1 = var0.getBlock();
      Material var2 = var0.getMaterial();
      return var1 == Blocks.ab || var2 == Material.a || var2 == Material.h || var2 == Material.i;
   }

   public void a_(World var1, BlockPosition var2) {
   }
}
