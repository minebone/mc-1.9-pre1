package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.TileEntity;
import net.minecraft.server.class_ky;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;

public class class_aqd extends TileEntity implements class_ky {
   public float a;
   public float f;
   public int g;
   private int h;

   public void c() {
      if(++this.h % 20 * 4 == 0) {
         this.b.c(this.c, Blocks.bQ, 1, this.g);
      }

      this.f = this.a;
      int var1 = this.c.p();
      int var2 = this.c.q();
      int var3 = this.c.r();
      float var4 = 0.1F;
      double var7;
      if(this.g > 0 && this.a == 0.0F) {
         double var5 = (double)var1 + 0.5D;
         var7 = (double)var3 + 0.5D;
         this.b.a((EntityHuman)null, var5, (double)var2 + 0.5D, var7, class_ng.aJ, EnumSoundCategory.BLOCKS, 0.5F, this.b.r.nextFloat() * 0.1F + 0.9F);
      }

      if(this.g == 0 && this.a > 0.0F || this.g > 0 && this.a < 1.0F) {
         float var11 = this.a;
         if(this.g > 0) {
            this.a += var4;
         } else {
            this.a -= var4;
         }

         if(this.a > 1.0F) {
            this.a = 1.0F;
         }

         float var6 = 0.5F;
         if(this.a < var6 && var11 >= var6) {
            var7 = (double)var1 + 0.5D;
            double var9 = (double)var3 + 0.5D;
            this.b.a((EntityHuman)null, var7, (double)var2 + 0.5D, var9, class_ng.aI, EnumSoundCategory.BLOCKS, 0.5F, this.b.r.nextFloat() * 0.1F + 0.9F);
         }

         if(this.a < 0.0F) {
            this.a = 0.0F;
         }
      }

   }

   public boolean c(int var1, int var2) {
      if(var1 == 1) {
         this.g = var2;
         return true;
      } else {
         return super.c(var1, var2);
      }
   }

   public void y() {
      this.A();
      super.y();
   }

   public void b() {
      ++this.g;
      this.b.c(this.c, Blocks.bQ, 1, this.g);
   }

   public void d() {
      --this.g;
      this.b.c(this.c, Blocks.bQ, 1, this.g);
   }

   public boolean a(EntityHuman var1) {
      return this.b.r(this.c) != this?false:var1.e((double)this.c.p() + 0.5D, (double)this.c.q() + 0.5D, (double)this.c.r() + 0.5D) <= 64.0D;
   }
}
