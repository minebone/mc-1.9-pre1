package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mojang.authlib.GameProfile;
import io.netty.buffer.Unpooled;
import java.io.File;
import java.net.SocketAddress;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.DemoPlayerInteractManager;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumChatFormat;
import net.minecraft.server.EnumMainHandOption;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.IpBanEntry;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MobEffect;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NetworkManager;
import net.minecraft.server.OpListEntry;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketPlayOutCustomPayload;
import net.minecraft.server.PacketPlayOutGameStateChange;
import net.minecraft.server.PacketPlayOutPlayerInfo;
import net.minecraft.server.PacketPlayOutScoreboardTeam;
import net.minecraft.server.PacketPlayOutWorldBorder;
import net.minecraft.server.PlayerConnection;
import net.minecraft.server.ScoreboardTeam;
import net.minecraft.server.ScoreboardTeamBase;
import net.minecraft.server.ServerStatisticManager;
import net.minecraft.server.Statistic;
import net.minecraft.server.StatisticList;
import net.minecraft.server.UserCache;
import net.minecraft.server.World;
import net.minecraft.server.WorldData;
import net.minecraft.server.WorldServer;
import net.minecraft.server.WorldSettings;
import net.minecraft.server.class_ars;
import net.minecraft.server.class_aru;
import net.minecraft.server.class_asr;
import net.minecraft.server.class_azp;
import net.minecraft.server.class_bbk;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutServerDifficulty;
import net.minecraft.server.PacketPlayOutChat;
import net.minecraft.server.PacketPlayOutEntityStatus;
import net.minecraft.server.PacketPlayOutLogin;
import net.minecraft.server.PacketPlayoutAbilities;
import net.minecraft.server.PacketPlayOutRespawn;
import net.minecraft.server.PacketPlayOutHeldItemSlot;
import net.minecraft.server.PacketPlayOutExperience;
import net.minecraft.server.PacketPlayOutSpawnPosition;
import net.minecraft.server.PacketPlayOutUpdateTime;
import net.minecraft.server.PacketPlayOutEntityEffect;
import net.minecraft.server.class_kw;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.PlayerInteractManager;
import net.minecraft.server.class_lv;
import net.minecraft.server.class_mj;
import net.minecraft.server.class_mn;
import net.minecraft.server.class_mp;
import net.minecraft.server.class_mr;
import net.minecraft.server.class_ms;
import net.minecraft.server.class_mt;
import net.minecraft.server.class_mu;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_oy;
import net.minecraft.server.class_oz;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class PlayerList {
   public static final File a = new File("banned-players.json");
   public static final File b = new File("banned-ips.json");
   public static final File c = new File("ops.json");
   public static final File d = new File("whitelist.json");
   private static final Logger f = LogManager.getLogger();
   private static final SimpleDateFormat g = new SimpleDateFormat("yyyy-MM-dd \'at\' HH:mm:ss z");
   private final MinecraftServer h;
   private final List i = Lists.newArrayList();
   private final Map j = Maps.newHashMap();
   private final class_mr k;
   private final class_mj l;
   private final class_mn m;
   private final class_mt n;
   private final Map o;
   private class_azp p;
   private boolean q;
   protected int e;
   private int r;
   private WorldSettings.EnumGamemode s;
   private boolean t;
   private int u;

   public PlayerList(MinecraftServer var1) {
      this.k = new class_mr(a);
      this.l = new class_mj(b);
      this.m = new class_mn(c);
      this.n = new class_mt(d);
      this.o = Maps.newHashMap();
      this.h = var1;
      this.k.a(false);
      this.l.a(false);
      this.e = 8;
   }

   public void a(NetworkManager var1, EntityPlayer var2) {
      GameProfile var3 = var2.getProfile();
      UserCache var4 = this.h.aA();
      GameProfile var5 = var4.a(var3.getId());
      String var6 = var5 == null?var3.getName():var5.getName();
      var4.a(var3);
      NBTTagCompound var7 = this.a(var2);
      var2.a((World)this.h.a(var2.dimension));
      var2.c.a((WorldServer)var2.world);
      String var8 = "local";
      if(var1.b() != null) {
         var8 = var1.b().toString();
      }

      f.info(var2.h_() + "[" + var8 + "] logged in with entity id " + var2.getId() + " at (" + var2.locX + ", " + var2.locY + ", " + var2.locZ + ")");
      WorldServer var9 = this.h.a(var2.dimension);
      WorldData var10 = var9.T();
      BlockPosition var11 = var9.R();
      this.a(var2, (EntityPlayer)null, var9);
      PlayerConnection var12 = new PlayerConnection(this.h, var1, var2);
      var12.a((Packet)(new PacketPlayOutLogin(var2.getId(), var2.c.b(), var10.s(), var9.s.p().a(), var9.ae(), this.p(), var10.t(), var9.U().b("reducedDebugInfo"))));
      var12.a((Packet)(new PacketPlayOutCustomPayload("MC|Brand", (new PacketDataSerializer(Unpooled.buffer())).a(this.c().getServerModName()))));
      var12.a((Packet)(new PacketPlayOutServerDifficulty(var10.x(), var10.y())));
      var12.a((Packet)(new PacketPlayOutSpawnPosition(var11)));
      var12.a((Packet)(new PacketPlayoutAbilities(var2.abilities)));
      var12.a((Packet)(new PacketPlayOutHeldItemSlot(var2.br.d)));
      this.f(var2);
      var2.E().d();
      var2.E().b(var2);
      this.a((class_kw)var9.ad(), var2);
      this.h.aC();
      ChatMessage var13;
      if(!var2.h_().equalsIgnoreCase(var6)) {
         var13 = new ChatMessage("multiplayer.player.joined.renamed", new Object[]{var2.i_(), var6});
      } else {
         var13 = new ChatMessage("multiplayer.player.joined", new Object[]{var2.i_()});
      }

      var13.b().a(EnumChatFormat.YELLOW);
      this.a((IChatBaseComponent)var13);
      this.c(var2);
      var12.a(var2.locX, var2.locY, var2.locZ, var2.yaw, var2.pitch);
      this.b(var2, var9);
      if(!this.h.X().isEmpty()) {
         var2.a(this.h.X(), this.h.Y());
      }

      Iterator var14 = var2.bN().iterator();

      while(var14.hasNext()) {
         MobEffect var15 = (MobEffect)var14.next();
         var12.a((Packet)(new PacketPlayOutEntityEffect(var2.getId(), var15)));
      }

      if(var7 != null) {
         if(var7.b("RootVehicle", 10)) {
            NBTTagCompound var19 = var7.o("RootVehicle");
            Entity var21 = class_asr.a(var19.o("Entity"), var9, true);
            if(var21 != null) {
               UUID var16 = var19.a("Attach");
               Iterator var17;
               Entity var18;
               if(var21.getUniqueId().equals(var16)) {
                  var2.a(var21, true);
               } else {
                  var17 = var21.bu().iterator();

                  while(var17.hasNext()) {
                     var18 = (Entity)var17.next();
                     if(var18.getUniqueId().equals(var16)) {
                        var2.a(var18, true);
                        break;
                     }
                  }
               }

               if(!var2.aH()) {
                  f.warn("Couldn\'t reattach entity to player");
                  var9.f(var21);
                  var17 = var21.bu().iterator();

                  while(var17.hasNext()) {
                     var18 = (Entity)var17.next();
                     var9.f(var18);
                  }
               }
            }
         } else if(var7.b("Riding", 10)) {
            Entity var20 = class_asr.a(var7.o("Riding"), var9, true);
            if(var20 != null) {
               var2.a(var20, true);
            }
         }
      }

      var2.j_();
   }

   protected void a(class_kw var1, EntityPlayer var2) {
      HashSet var3 = Sets.newHashSet();
      Iterator var4 = var1.g().iterator();

      while(var4.hasNext()) {
         ScoreboardTeam var5 = (ScoreboardTeam)var4.next();
         var2.a.a((Packet)(new PacketPlayOutScoreboardTeam(var5, 0)));
      }

      for(int var9 = 0; var9 < 19; ++var9) {
         class_bbk var10 = var1.a(var9);
         if(var10 != null && !var3.contains(var10)) {
            List var6 = var1.d(var10);
            Iterator var7 = var6.iterator();

            while(var7.hasNext()) {
               Packet var8 = (Packet)var7.next();
               var2.a.a(var8);
            }

            var3.add(var10);
         }
      }

   }

   public void a(WorldServer[] var1) {
      this.p = var1[0].S().e();
      var1[0].aj().a(new class_ars() {
         public void a(class_aru var1, double var2) {
            PlayerList.this.sendAll((Packet)(new PacketPlayOutWorldBorder(var1, PacketPlayOutWorldBorder.EnumWorldBorderAction.SET_SIZE)));
         }

         public void a(class_aru var1, double var2, double var4, long var6) {
            PlayerList.this.sendAll((Packet)(new PacketPlayOutWorldBorder(var1, PacketPlayOutWorldBorder.EnumWorldBorderAction.LERP_SIZE)));
         }

         public void a(class_aru var1, double var2, double var4) {
            PlayerList.this.sendAll((Packet)(new PacketPlayOutWorldBorder(var1, PacketPlayOutWorldBorder.EnumWorldBorderAction.SET_CENTER)));
         }

         public void a(class_aru var1, int var2) {
            PlayerList.this.sendAll((Packet)(new PacketPlayOutWorldBorder(var1, PacketPlayOutWorldBorder.EnumWorldBorderAction.SET_WARNING_TIME)));
         }

         public void b(class_aru var1, int var2) {
            PlayerList.this.sendAll((Packet)(new PacketPlayOutWorldBorder(var1, PacketPlayOutWorldBorder.EnumWorldBorderAction.SET_WARNING_BLOCKS)));
         }

         public void b(class_aru var1, double var2) {
         }

         public void c(class_aru var1, double var2) {
         }
      });
   }

   public void a(EntityPlayer var1, WorldServer var2) {
      WorldServer var3 = var1.x();
      if(var2 != null) {
         var2.w().b(var1);
      }

      var3.w().a(var1);
      var3.r().d((int)var1.locX >> 4, (int)var1.locZ >> 4);
   }

   public int d() {
      return class_lv.b(this.s());
   }

   public NBTTagCompound a(EntityPlayer var1) {
      NBTTagCompound var2 = this.h.d[0].T().h();
      NBTTagCompound var3;
      if(var1.h_().equals(this.h.Q()) && var2 != null) {
         var3 = this.h.aI().a((class_oy)class_oz.PLAYER, (NBTTagCompound)var2);
         var1.f(var3);
         f.debug("loading single player");
      } else {
         var3 = this.p.b(var1);
      }

      return var3;
   }

   protected void b(EntityPlayer var1) {
      this.p.a(var1);
      ServerStatisticManager var2 = (ServerStatisticManager)this.o.get(var1.getUniqueId());
      if(var2 != null) {
         var2.b();
      }

   }

   public void c(EntityPlayer var1) {
      this.i.add(var1);
      this.j.put(var1.getUniqueId(), var1);
      this.sendAll((Packet)(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, new EntityPlayer[]{var1})));
      WorldServer var2 = this.h.a(var1.dimension);

      for(int var3 = 0; var3 < this.i.size(); ++var3) {
         var1.a.a((Packet)(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, new EntityPlayer[]{(EntityPlayer)this.i.get(var3)})));
      }

      var2.a((Entity)var1);
      this.a((EntityPlayer)var1, (WorldServer)null);
   }

   public void d(EntityPlayer var1) {
      var1.x().w().c(var1);
   }

   public void e(EntityPlayer var1) {
      WorldServer var2 = var1.x();
      var1.b((Statistic)StatisticList.f);
      this.b(var1);
      if(var1.aH()) {
         Entity var3 = var1.bv();
         if(var3.b(EntityPlayer.class).size() == 1) {
            f.debug("Removing player mount");
            var1.p();
            var2.f(var3);
            Iterator var4 = var3.bu().iterator();

            while(var4.hasNext()) {
               Entity var5 = (Entity)var4.next();
               var2.f(var5);
            }

            var2.a(var1.ab, var1.ad).e();
         }
      }

      var2.e(var1);
      var2.w().b(var1);
      this.i.remove(var1);
      UUID var6 = var1.getUniqueId();
      EntityPlayer var7 = (EntityPlayer)this.j.get(var6);
      if(var7 == var1) {
         this.j.remove(var6);
         this.o.remove(var6);
      }

      this.sendAll((Packet)(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, new EntityPlayer[]{var1})));
   }

   public String a(SocketAddress var1, GameProfile var2) {
      String var4;
      if(this.k.a(var2)) {
         class_ms var5 = new class_ms(var2);
         var4 = "You are banned from this server!\nReason: " + var5.d();
         if(var5.c() != null) {
            var4 = var4 + "\nYour ban will be removed on " + g.format(var5.c());
         }

         return var4;
      } else if(!this.e(var2)) {
         return "You are not white-listed on this server!";
      } else if(this.l.a(var1)) {
         IpBanEntry var3 = this.l.b(var1);
         var4 = "Your IP address is banned from this server!\nReason: " + var3.d();
         if(var3.c() != null) {
            var4 = var4 + "\nYour ban will be removed on " + g.format(var3.c());
         }

         return var4;
      } else {
         return this.i.size() >= this.e && !this.f(var2)?"The server is full!":null;
      }
   }

   public EntityPlayer g(GameProfile var1) {
      UUID var2 = EntityHuman.a(var1);
      ArrayList var3 = Lists.newArrayList();

      for(int var4 = 0; var4 < this.i.size(); ++var4) {
         EntityPlayer var5 = (EntityPlayer)this.i.get(var4);
         if(var5.getUniqueId().equals(var2)) {
            var3.add(var5);
         }
      }

      EntityPlayer var7 = (EntityPlayer)this.j.get(var1.getId());
      if(var7 != null && !var3.contains(var7)) {
         var3.add(var7);
      }

      Iterator var8 = var3.iterator();

      while(var8.hasNext()) {
         EntityPlayer var6 = (EntityPlayer)var8.next();
         var6.a.c("You logged in from another location");
      }

      Object var9;
      if(this.h.V()) {
         var9 = new DemoPlayerInteractManager(this.h.a(0));
      } else {
         var9 = new PlayerInteractManager(this.h.a(0));
      }

      return new EntityPlayer(this.h, this.h.a(0), var1, (PlayerInteractManager)var9);
   }

   public EntityPlayer a(EntityPlayer var1, int var2, boolean var3) {
      var1.x().v().b(var1);
      var1.x().v().b((Entity)var1);
      var1.x().w().b(var1);
      this.i.remove(var1);
      this.h.a(var1.dimension).f(var1);
      BlockPosition var4 = var1.cN();
      boolean var5 = var1.cO();
      var1.dimension = var2;
      Object var6;
      if(this.h.V()) {
         var6 = new DemoPlayerInteractManager(this.h.a(var1.dimension));
      } else {
         var6 = new PlayerInteractManager(this.h.a(var1.dimension));
      }

      EntityPlayer var7 = new EntityPlayer(this.h, this.h.a(var1.dimension), var1.getProfile(), (PlayerInteractManager)var6);
      var7.a = var1.a;
      var7.a((EntityHuman)var1, var3);
      var7.f(var1.getId());
      var7.v(var1);
      var7.a((EnumMainHandOption)var1.cq());
      Iterator var8 = var1.O().iterator();

      while(var8.hasNext()) {
         String var9 = (String)var8.next();
         var7.a((String)var9);
      }

      WorldServer var10 = this.h.a(var1.dimension);
      this.a(var7, var1, var10);
      BlockPosition var11;
      if(var4 != null) {
         var11 = EntityHuman.a(this.h.a(var1.dimension), var4, var5);
         if(var11 != null) {
            var7.b((double)((float)var11.p() + 0.5F), (double)((float)var11.q() + 0.1F), (double)((float)var11.r() + 0.5F), 0.0F, 0.0F);
            var7.a((BlockPosition)var4, var5);
         } else {
            var7.a.a((Packet)(new PacketPlayOutGameStateChange(0, 0.0F)));
         }
      }

      var10.r().d((int)var7.locX >> 4, (int)var7.locZ >> 4);

      while(!var10.a((Entity)var7, (AxisAlignedBB)var7.bk()).isEmpty() && var7.locY < 256.0D) {
         var7.b(var7.locX, var7.locY + 1.0D, var7.locZ);
      }

      var7.a.a((Packet)(new PacketPlayOutRespawn(var7.dimension, var7.world.ae(), var7.world.T().t(), var7.c.b())));
      var11 = var10.R();
      var7.a.a(var7.locX, var7.locY, var7.locZ, var7.yaw, var7.pitch);
      var7.a.a((Packet)(new PacketPlayOutSpawnPosition(var11)));
      var7.a.a((Packet)(new PacketPlayOutExperience(var7.bM, var7.bL, var7.bK)));
      this.b(var7, var10);
      this.f(var7);
      var10.w().a(var7);
      var10.a((Entity)var7);
      this.i.add(var7);
      this.j.put(var7.getUniqueId(), var7);
      var7.j_();
      var7.c(var7.bP());
      return var7;
   }

   public void f(EntityPlayer var1) {
      GameProfile var2 = var1.getProfile();
      int var3 = this.h(var2)?this.m.a(var2):0;
      var3 = this.h.R() && this.h.d[0].T().u()?4:var3;
      var3 = this.t?4:var3;
      this.b(var1, var3);
   }

   public void a(EntityPlayer var1, int var2) {
      int var3 = var1.dimension;
      WorldServer var4 = this.h.a(var1.dimension);
      var1.dimension = var2;
      WorldServer var5 = this.h.a(var1.dimension);
      var1.a.a((Packet)(new PacketPlayOutRespawn(var1.dimension, var1.world.ae(), var1.world.T().t(), var1.c.b())));
      this.f(var1);
      var4.f(var1);
      var1.dead = false;
      this.a(var1, var3, var4, var5);
      this.a(var1, var4);
      var1.a.a(var1.locX, var1.locY, var1.locZ, var1.yaw, var1.pitch);
      var1.c.a(var5);
      var1.a.a((Packet)(new PacketPlayoutAbilities(var1.abilities)));
      this.b(var1, var5);
      this.g(var1);
      Iterator var6 = var1.bN().iterator();

      while(var6.hasNext()) {
         MobEffect var7 = (MobEffect)var6.next();
         var1.a.a((Packet)(new PacketPlayOutEntityEffect(var1.getId(), var7)));
      }

   }

   public void a(Entity var1, int var2, WorldServer var3, WorldServer var4) {
      double var5 = var1.locX;
      double var7 = var1.locZ;
      double var9 = 8.0D;
      float var11 = var1.yaw;
      var3.C.a("moving");
      if(var1.dimension == -1) {
         var5 = MathHelper.a(var5 / var9, var4.aj().b() + 16.0D, var4.aj().d() - 16.0D);
         var7 = MathHelper.a(var7 / var9, var4.aj().c() + 16.0D, var4.aj().e() - 16.0D);
         var1.b(var5, var1.locY, var7, var1.yaw, var1.pitch);
         if(var1.at()) {
            var3.a(var1, false);
         }
      } else if(var1.dimension == 0) {
         var5 = MathHelper.a(var5 * var9, var4.aj().b() + 16.0D, var4.aj().d() - 16.0D);
         var7 = MathHelper.a(var7 * var9, var4.aj().c() + 16.0D, var4.aj().e() - 16.0D);
         var1.b(var5, var1.locY, var7, var1.yaw, var1.pitch);
         if(var1.at()) {
            var3.a(var1, false);
         }
      } else {
         BlockPosition var12;
         if(var2 == 1) {
            var12 = var4.R();
         } else {
            var12 = var4.p();
         }

         var5 = (double)var12.p();
         var1.locY = (double)var12.q();
         var7 = (double)var12.r();
         var1.b(var5, var1.locY, var7, 90.0F, 0.0F);
         if(var1.at()) {
            var3.a(var1, false);
         }
      }

      var3.C.b();
      if(var2 != 1) {
         var3.C.a("placing");
         var5 = (double)MathHelper.a((int)var5, -29999872, 29999872);
         var7 = (double)MathHelper.a((int)var7, -29999872, 29999872);
         if(var1.at()) {
            var1.b(var5, var1.locY, var7, var1.yaw, var1.pitch);
            var4.x().a(var1, var11);
            var4.a(var1);
            var4.a(var1, false);
         }

         var3.C.b();
      }

      var1.a((World)var4);
   }

   public void e() {
      if(++this.u > 600) {
         this.sendAll((Packet)(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.UPDATE_LATENCY, this.i)));
         this.u = 0;
      }

   }

   public void sendAll(Packet var1) {
      for(int var2 = 0; var2 < this.i.size(); ++var2) {
         ((EntityPlayer)this.i.get(var2)).a.a(var1);
      }

   }

   public void a(Packet var1, int var2) {
      for(int var3 = 0; var3 < this.i.size(); ++var3) {
         EntityPlayer var4 = (EntityPlayer)this.i.get(var3);
         if(var4.dimension == var2) {
            var4.a.a(var1);
         }
      }

   }

   public void a(EntityHuman var1, IChatBaseComponent var2) {
      ScoreboardTeamBase var3 = var1.aN();
      if(var3 != null) {
         Collection var4 = var3.d();
         Iterator var5 = var4.iterator();

         while(var5.hasNext()) {
            String var6 = (String)var5.next();
            EntityPlayer var7 = this.a(var6);
            if(var7 != null && var7 != var1) {
               var7.a(var2);
            }
         }

      }
   }

   public void b(EntityHuman var1, IChatBaseComponent var2) {
      ScoreboardTeamBase var3 = var1.aN();
      if(var3 == null) {
         this.a(var2);
      } else {
         for(int var4 = 0; var4 < this.i.size(); ++var4) {
            EntityPlayer var5 = (EntityPlayer)this.i.get(var4);
            if(var5.aN() != var3) {
               var5.a(var2);
            }
         }

      }
   }

   public String b(boolean var1) {
      String var2 = "";
      ArrayList var3 = Lists.newArrayList((Iterable)this.i);

      for(int var4 = 0; var4 < var3.size(); ++var4) {
         if(var4 > 0) {
            var2 = var2 + ", ";
         }

         var2 = var2 + ((EntityPlayer)var3.get(var4)).h_();
         if(var1) {
            var2 = var2 + " (" + ((EntityPlayer)var3.get(var4)).getUniqueId().toString() + ")";
         }
      }

      return var2;
   }

   public String[] f() {
      String[] var1 = new String[this.i.size()];

      for(int var2 = 0; var2 < this.i.size(); ++var2) {
         var1[var2] = ((EntityPlayer)this.i.get(var2)).h_();
      }

      return var1;
   }

   public GameProfile[] g() {
      GameProfile[] var1 = new GameProfile[this.i.size()];

      for(int var2 = 0; var2 < this.i.size(); ++var2) {
         var1[var2] = ((EntityPlayer)this.i.get(var2)).getProfile();
      }

      return var1;
   }

   public class_mr h() {
      return this.k;
   }

   public class_mj i() {
      return this.l;
   }

   public void a(GameProfile var1) {
      int var2 = this.h.q();
      this.m.a((class_mp)(new OpListEntry(var1, this.h.q(), this.m.b(var1))));
      this.b(this.a(var1.getId()), var2);
   }

   public void b(GameProfile var1) {
      this.m.c(var1);
      this.b(this.a(var1.getId()), 0);
   }

   private void b(EntityPlayer var1, int var2) {
      if(var1 != null && var1.a != null) {
         byte var3;
         if(var2 <= 0) {
            var3 = 24;
         } else if(var2 >= 4) {
            var3 = 28;
         } else {
            var3 = (byte)(24 + var2);
         }

         var1.a.a((Packet)(new PacketPlayOutEntityStatus(var1, var3)));
      }

   }

   public boolean e(GameProfile var1) {
      return !this.q || this.m.d(var1) || this.n.d(var1);
   }

   public boolean h(GameProfile var1) {
      return this.m.d(var1) || this.h.R() && this.h.d[0].T().u() && this.h.Q().equalsIgnoreCase(var1.getName()) || this.t;
   }

   public EntityPlayer a(String var1) {
      Iterator var2 = this.i.iterator();

      EntityPlayer var3;
      do {
         if(!var2.hasNext()) {
            return null;
         }

         var3 = (EntityPlayer)var2.next();
      } while(!var3.h_().equalsIgnoreCase(var1));

      return var3;
   }

   public void a(EntityHuman var1, double var2, double var4, double var6, double var8, int var10, Packet var11) {
      for(int var12 = 0; var12 < this.i.size(); ++var12) {
         EntityPlayer var13 = (EntityPlayer)this.i.get(var12);
         if(var13 != var1 && var13.dimension == var10) {
            double var14 = var2 - var13.locX;
            double var16 = var4 - var13.locY;
            double var18 = var6 - var13.locZ;
            if(var14 * var14 + var16 * var16 + var18 * var18 < var8 * var8) {
               var13.a.a(var11);
            }
         }
      }

   }

   public void j() {
      for(int var1 = 0; var1 < this.i.size(); ++var1) {
         this.b((EntityPlayer)this.i.get(var1));
      }

   }

   public void d(GameProfile var1) {
      this.n.a((class_mp)(new class_mu(var1)));
   }

   public void c(GameProfile var1) {
      this.n.c(var1);
   }

   public class_mt k() {
      return this.n;
   }

   public String[] l() {
      return this.n.a();
   }

   public class_mn m() {
      return this.m;
   }

   public String[] n() {
      return this.m.a();
   }

   public void a() {
   }

   public void b(EntityPlayer var1, WorldServer var2) {
      class_aru var3 = this.h.d[0].aj();
      var1.a.a((Packet)(new PacketPlayOutWorldBorder(var3, PacketPlayOutWorldBorder.EnumWorldBorderAction.INITIALIZE)));
      var1.a.a((Packet)(new PacketPlayOutUpdateTime(var2.P(), var2.Q(), var2.U().b("doDaylightCycle"))));
      if(var2.W()) {
         var1.a.a((Packet)(new PacketPlayOutGameStateChange(1, 0.0F)));
         var1.a.a((Packet)(new PacketPlayOutGameStateChange(7, var2.j(1.0F))));
         var1.a.a((Packet)(new PacketPlayOutGameStateChange(8, var2.h(1.0F))));
      }

   }

   public void g(EntityPlayer var1) {
      var1.a(var1.bs);
      var1.u();
      var1.a.a((Packet)(new PacketPlayOutHeldItemSlot(var1.br.d)));
   }

   public int o() {
      return this.i.size();
   }

   public int p() {
      return this.e;
   }

   public String[] q() {
      return this.h.d[0].S().e().f();
   }

   public boolean r() {
      return this.q;
   }

   public void a(boolean var1) {
      this.q = var1;
   }

   public List b(String var1) {
      ArrayList var2 = Lists.newArrayList();
      Iterator var3 = this.i.iterator();

      while(var3.hasNext()) {
         EntityPlayer var4 = (EntityPlayer)var3.next();
         if(var4.A().equals(var1)) {
            var2.add(var4);
         }
      }

      return var2;
   }

   public int s() {
      return this.r;
   }

   public MinecraftServer c() {
      return this.h;
   }

   public NBTTagCompound t() {
      return null;
   }

   private void a(EntityPlayer var1, EntityPlayer var2, World var3) {
      if(var2 != null) {
         var1.c.a(var2.c.b());
      } else if(this.s != null) {
         var1.c.a(this.s);
      }

      var1.c.b(var3.T().q());
   }

   public void u() {
      for(int var1 = 0; var1 < this.i.size(); ++var1) {
         ((EntityPlayer)this.i.get(var1)).a.c("Server closed");
      }

   }

   public void a(IChatBaseComponent var1, boolean var2) {
      this.h.a(var1);
      int var3 = var2?1:0;
      this.sendAll((Packet)(new PacketPlayOutChat(var1, (byte)var3)));
   }

   public void a(IChatBaseComponent var1) {
      this.a(var1, true);
   }

   public ServerStatisticManager a(EntityHuman var1) {
      UUID var2 = var1.getUniqueId();
      ServerStatisticManager var3 = var2 == null?null:(ServerStatisticManager)this.o.get(var2);
      if(var3 == null) {
         File var4 = new File(this.h.a(0).S().b(), "stats");
         File var5 = new File(var4, var2.toString() + ".json");
         if(!var5.exists()) {
            File var6 = new File(var4, var1.h_() + ".json");
            if(var6.exists() && var6.isFile()) {
               var6.renameTo(var5);
            }
         }

         var3 = new ServerStatisticManager(this.h, var5);
         var3.a();
         this.o.put(var2, var3);
      }

      return var3;
   }

   public void a(int var1) {
      this.r = var1;
      if(this.h.d != null) {
         WorldServer[] var2 = this.h.d;
         int var3 = var2.length;

         for(int var4 = 0; var4 < var3; ++var4) {
            WorldServer var5 = var2[var4];
            if(var5 != null) {
               var5.w().a(var1);
               var5.v().a(var1);
            }
         }

      }
   }

   public List v() {
      return this.i;
   }

   public EntityPlayer a(UUID var1) {
      return (EntityPlayer)this.j.get(var1);
   }

   public boolean f(GameProfile var1) {
      return false;
   }
}
