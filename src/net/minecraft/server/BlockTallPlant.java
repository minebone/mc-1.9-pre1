package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockLongGrass;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aju;
import net.minecraft.server.class_ajx;
import net.minecraft.server.class_amf;
import net.minecraft.server.class_aoo;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_or;
import net.minecraft.server.class_rz;

public class BlockTallPlant extends class_ajx implements class_aju {
   public static final BlockStateEnum a = BlockStateEnum.a("variant", BlockTallPlant.EnumTallFlowerVariants.class);
   public static final BlockStateEnum c = BlockStateEnum.a("half", BlockTallPlant.class_a_in_class_akv.class);
   public static final BlockStateEnum d = class_amf.D;

   public BlockTallPlant() {
      super(Material.l);
      this.w(this.A.b().set(a, BlockTallPlant.EnumTallFlowerVariants.SUNFLOWER).set(c, BlockTallPlant.class_a_in_class_akv.LOWER).set(d, EnumDirection.NORTH));
      this.c(0.0F);
      this.a(class_aoo.c);
      this.c("doublePlant");
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return j;
   }

   private BlockTallPlant.EnumTallFlowerVariants a(class_ahw var1, BlockPosition var2, IBlockData var3) {
      if(var3.getBlock() == this) {
         var3 = var3.b(var1, var2);
         return (BlockTallPlant.EnumTallFlowerVariants)var3.get(a);
      } else {
         return BlockTallPlant.EnumTallFlowerVariants.FERN;
      }
   }

   public boolean a(World var1, BlockPosition var2) {
      return super.a(var1, var2) && var1.d(var2.a());
   }

   public boolean a(class_ahw var1, BlockPosition var2) {
      IBlockData var3 = var1.getType(var2);
      if(var3.getBlock() != this) {
         return true;
      } else {
         BlockTallPlant.EnumTallFlowerVariants var4 = (BlockTallPlant.EnumTallFlowerVariants)var3.b(var1, var2).get(a);
         return var4 == BlockTallPlant.EnumTallFlowerVariants.FERN || var4 == BlockTallPlant.EnumTallFlowerVariants.GRASS;
      }
   }

   protected void e(World var1, BlockPosition var2, IBlockData var3) {
      if(!this.f(var1, var2, var3)) {
         boolean var4 = var3.get(c) == BlockTallPlant.class_a_in_class_akv.UPPER;
         BlockPosition var5 = var4?var2:var2.a();
         BlockPosition var6 = var4?var2.b():var2;
         Object var7 = var4?this:var1.getType(var5).getBlock();
         Object var8 = var4?var1.getType(var6).getBlock():this;
         if(var7 == this) {
            var1.a((BlockPosition)var5, (IBlockData)Blocks.AIR.u(), 2);
         }

         if(var8 == this) {
            var1.a((BlockPosition)var6, (IBlockData)Blocks.AIR.u(), 3);
            if(!var4) {
               this.b(var1, var6, var3, 0);
            }
         }

      }
   }

   public boolean f(World var1, BlockPosition var2, IBlockData var3) {
      if(var3.get(c) == BlockTallPlant.class_a_in_class_akv.UPPER) {
         return var1.getType(var2.b()).getBlock() == this;
      } else {
         IBlockData var4 = var1.getType(var2.a());
         return var4.getBlock() == this && super.f(var1, var2, var4);
      }
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      if(var1.get(c) == BlockTallPlant.class_a_in_class_akv.UPPER) {
         return null;
      } else {
         BlockTallPlant.EnumTallFlowerVariants var4 = (BlockTallPlant.EnumTallFlowerVariants)var1.get(a);
         return var4 == BlockTallPlant.EnumTallFlowerVariants.FERN?null:(var4 == BlockTallPlant.EnumTallFlowerVariants.GRASS?(var2.nextInt(8) == 0?Items.P:null):Item.a((Block)this));
      }
   }

   public int d(IBlockData var1) {
      return var1.get(c) != BlockTallPlant.class_a_in_class_akv.UPPER && var1.get(a) != BlockTallPlant.EnumTallFlowerVariants.GRASS?((BlockTallPlant.EnumTallFlowerVariants)var1.get(a)).a():0;
   }

   public void a(World var1, BlockPosition var2, BlockTallPlant.EnumTallFlowerVariants var3, int var4) {
      var1.a(var2, this.u().set(c, BlockTallPlant.class_a_in_class_akv.LOWER).set(a, var3), var4);
      var1.a(var2.a(), this.u().set(c, BlockTallPlant.class_a_in_class_akv.UPPER), var4);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, class_rz var4, ItemStack var5) {
      var1.a((BlockPosition)var2.a(), (IBlockData)this.u().set(c, BlockTallPlant.class_a_in_class_akv.UPPER), 2);
   }

   public void a(World var1, EntityHuman var2, BlockPosition var3, IBlockData var4, TileEntity var5, ItemStack var6) {
      if(var1.E || var6 == null || var6.b() != Items.bl || var4.get(c) != BlockTallPlant.class_a_in_class_akv.LOWER || !this.b(var1, var3, var4, var2)) {
         super.a(var1, var2, var3, var4, var5, var6);
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4) {
      if(var3.get(c) == BlockTallPlant.class_a_in_class_akv.UPPER) {
         if(var1.getType(var2.b()).getBlock() == this) {
            if(!var4.abilities.d) {
               IBlockData var5 = var1.getType(var2.b());
               BlockTallPlant.EnumTallFlowerVariants var6 = (BlockTallPlant.EnumTallFlowerVariants)var5.get(a);
               if(var6 != BlockTallPlant.EnumTallFlowerVariants.FERN && var6 != BlockTallPlant.EnumTallFlowerVariants.GRASS) {
                  var1.b(var2.b(), true);
               } else if(!var1.E) {
                  if(var4.ca() != null && var4.ca().b() == Items.bl) {
                     this.b(var1, var2, var5, var4);
                     var1.g(var2.b());
                  } else {
                     var1.b(var2.b(), true);
                  }
               } else {
                  var1.g(var2.b());
               }
            } else {
               var1.g(var2.b());
            }
         }
      } else if(var1.getType(var2.a()).getBlock() == this) {
         var1.a((BlockPosition)var2.a(), (IBlockData)Blocks.AIR.u(), 2);
      }

      super.a(var1, var2, var3, var4);
   }

   private boolean b(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4) {
      BlockTallPlant.EnumTallFlowerVariants var5 = (BlockTallPlant.EnumTallFlowerVariants)var3.get(a);
      if(var5 != BlockTallPlant.EnumTallFlowerVariants.FERN && var5 != BlockTallPlant.EnumTallFlowerVariants.GRASS) {
         return false;
      } else {
         var4.b(StatisticList.a((Block)this));
         int var6 = (var5 == BlockTallPlant.EnumTallFlowerVariants.GRASS?BlockLongGrass.EnumTallGrassType.GRASS:BlockLongGrass.EnumTallGrassType.FERN).a();
         a((World)var1, (BlockPosition)var2, (ItemStack)(new ItemStack(Blocks.H, 2, var6)));
         return true;
      }
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(this, 1, this.a((class_ahw)var1, (BlockPosition)var2, (IBlockData)var3).a());
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, boolean var4) {
      BlockTallPlant.EnumTallFlowerVariants var5 = this.a((class_ahw)var1, (BlockPosition)var2, (IBlockData)var3);
      return var5 != BlockTallPlant.EnumTallFlowerVariants.GRASS && var5 != BlockTallPlant.EnumTallFlowerVariants.FERN;
   }

   public boolean a(World var1, Random var2, BlockPosition var3, IBlockData var4) {
      return true;
   }

   public void b(World var1, Random var2, BlockPosition var3, IBlockData var4) {
      a((World)var1, (BlockPosition)var3, (ItemStack)(new ItemStack(this, 1, this.a((class_ahw)var1, (BlockPosition)var3, (IBlockData)var4).a())));
   }

   public IBlockData a(int var1) {
      return (var1 & 8) > 0?this.u().set(c, BlockTallPlant.class_a_in_class_akv.UPPER):this.u().set(c, BlockTallPlant.class_a_in_class_akv.LOWER).set(a, BlockTallPlant.EnumTallFlowerVariants.a(var1 & 7));
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      if(var1.get(c) == BlockTallPlant.class_a_in_class_akv.UPPER) {
         IBlockData var4 = var2.getType(var3.b());
         if(var4.getBlock() == this) {
            var1 = var1.set(a, var4.get(a));
         }
      }

      return var1;
   }

   public int e(IBlockData var1) {
      return var1.get(c) == BlockTallPlant.class_a_in_class_akv.UPPER?8 | ((EnumDirection)var1.get(d)).b():((BlockTallPlant.EnumTallFlowerVariants)var1.get(a)).a();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{c, a, d});
   }

   public static enum class_a_in_class_akv implements class_or {
      UPPER,
      LOWER;

      public String toString() {
         return this.m();
      }

      public String m() {
         return this == UPPER?"upper":"lower";
      }
   }

   public static enum EnumTallFlowerVariants implements class_or {
      SUNFLOWER(0, "sunflower"),
      SYRINGA(1, "syringa"),
      GRASS(2, "double_grass", "grass"),
      FERN(3, "double_fern", "fern"),
      ROSE(4, "double_rose", "rose"),
      PAEONIA(5, "paeonia");

      private static final BlockTallPlant.EnumTallFlowerVariants[] g = new BlockTallPlant.EnumTallFlowerVariants[values().length];
      private final int h;
      private final String i;
      private final String j;

      private EnumTallFlowerVariants(int var3, String var4) {
         this(var3, var4, var4);
      }

      private EnumTallFlowerVariants(int var3, String var4, String var5) {
         this.h = var3;
         this.i = var4;
         this.j = var5;
      }

      public int a() {
         return this.h;
      }

      public String toString() {
         return this.i;
      }

      public static BlockTallPlant.EnumTallFlowerVariants a(int var0) {
         if(var0 < 0 || var0 >= g.length) {
            var0 = 0;
         }

         return g[var0];
      }

      public String m() {
         return this.i;
      }

      public String c() {
         return this.j;
      }

      static {
         BlockTallPlant.EnumTallFlowerVariants[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            BlockTallPlant.EnumTallFlowerVariants var3 = var0[var2];
            g[var3.a()] = var3;
         }

      }
   }
}
