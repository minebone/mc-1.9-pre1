package net.minecraft.server;

import java.util.List;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;

public class CommandMe extends CommandAbstract {
   public String c() {
      return "me";
   }

   public int a() {
      return 0;
   }

   public String b(ICommandListener var1) {
      return "commands.me.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length <= 0) {
         throw new class_cf("commands.me.usage", new Object[0]);
      } else {
         IChatBaseComponent var4 = b(var2, var3, 0, !(var2 instanceof EntityHuman));
         var1.getPlayerList().a((IChatBaseComponent)(new ChatMessage("chat.type.emote", new Object[]{var2.i_(), var4})));
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return a(var3, var1.J());
   }
}
