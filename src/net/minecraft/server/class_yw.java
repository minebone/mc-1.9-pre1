package net.minecraft.server;

import net.minecraft.server.BiomeBase;
import net.minecraft.server.Chunk;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.MobEffectType;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.WorldType;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_qk;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sx;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_uv;
import net.minecraft.server.class_uw;
import net.minecraft.server.class_vd;
import net.minecraft.server.class_wg;
import net.minecraft.server.class_yk;
import net.minecraft.server.class_ys;

public class class_yw extends EntityInsentient implements class_yk {
   private static final class_ke bt = DataWatcher.a(class_yw.class, class_kg.b);
   public float a;
   public float b;
   public float c;
   private boolean bu;

   public class_yw(World var1) {
      super(var1);
      this.f = new class_yw.class_d_in_class_yw(this);
   }

   protected void r() {
      this.bp.a(1, new class_yw.class_b_in_class_yw(this));
      this.bp.a(2, new class_yw.class_a_in_class_yw(this));
      this.bp.a(3, new class_yw.class_e_in_class_yw(this));
      this.bp.a(5, new class_yw.class_c_in_class_yw(this));
      this.bq.a(1, new class_uw(this));
      this.bq.a(3, new class_uv(this, class_wg.class));
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)bt, (Object)Integer.valueOf(1));
   }

   protected void a(int var1) {
      this.datawatcher.b(bt, Integer.valueOf(var1));
      this.a(0.51000005F * (float)var1, 0.51000005F * (float)var1);
      this.b(this.locX, this.locY, this.locZ);
      this.a((class_sk)class_ys.a).a((double)(var1 * var1));
      this.a((class_sk)class_ys.d).a((double)(0.2F + 0.1F * (float)var1));
      this.c(this.bV());
      this.b_ = var1;
   }

   public int db() {
      return ((Integer)this.datawatcher.a(bt)).intValue();
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("Size", this.db() - 1);
      var1.a("wasOnGround", this.bu);
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      int var2 = var1.h("Size");
      if(var2 < 0) {
         var2 = 0;
      }

      this.a(var2 + 1);
      this.bu = var1.p("wasOnGround");
   }

   protected EnumParticle o() {
      return EnumParticle.SLIME;
   }

   public void m() {
      if(!this.world.E && this.world.ae() == EnumDifficulty.PEACEFUL && this.db() > 0) {
         this.dead = true;
      }

      this.b += (this.a - this.b) * 0.5F;
      this.c = this.b;
      super.m();
      if(this.onGround && !this.bu) {
         int var1 = this.db();

         for(int var2 = 0; var2 < var1 * 8; ++var2) {
            float var3 = this.random.nextFloat() * 6.2831855F;
            float var4 = this.random.nextFloat() * 0.5F + 0.5F;
            float var5 = MathHelper.a(var3) * (float)var1 * 0.5F * var4;
            float var6 = MathHelper.b(var3) * (float)var1 * 0.5F * var4;
            World var10000 = this.world;
            EnumParticle var10001 = this.o();
            double var10002 = this.locX + (double)var5;
            double var10004 = this.locZ + (double)var6;
            var10000.a(var10001, var10002, this.bk().b, var10004, 0.0D, 0.0D, 0.0D, new int[0]);
         }

         if(this.da()) {
            this.a(this.cY(), this.cc(), ((this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F) / 0.8F);
         }

         this.a = -0.5F;
      } else if(!this.onGround && this.bu) {
         this.a = 1.0F;
      }

      this.bu = this.onGround;
      this.cV();
   }

   protected void cV() {
      this.a *= 0.6F;
   }

   protected int cU() {
      return this.random.nextInt(20) + 10;
   }

   protected class_yw cT() {
      return new class_yw(this.world);
   }

   public void a(class_ke var1) {
      if(bt.equals(var1)) {
         int var2 = this.db();
         this.a(0.51000005F * (float)var2, 0.51000005F * (float)var2);
         this.yaw = this.aO;
         this.aM = this.aO;
         if(this.ah() && this.random.nextInt(20) == 0) {
            this.aj();
         }
      }

      super.a((class_ke)var1);
   }

   public void S() {
      int var1 = this.db();
      if(!this.world.E && var1 > 1 && this.bP() <= 0.0F) {
         int var2 = 2 + this.random.nextInt(3);

         for(int var3 = 0; var3 < var2; ++var3) {
            float var4 = ((float)(var3 % 2) - 0.5F) * (float)var1 / 4.0F;
            float var5 = ((float)(var3 / 2) - 0.5F) * (float)var1 / 4.0F;
            class_yw var6 = this.cT();
            if(this.o_()) {
               var6.c(this.be());
            }

            if(this.cN()) {
               var6.cL();
            }

            var6.a(var1 / 2);
            var6.b(this.locX + (double)var4, this.locY + 0.5D, this.locZ + (double)var5, this.random.nextFloat() * 360.0F, 0.0F);
            this.world.a((Entity)var6);
         }
      }

      super.S();
   }

   public void i(Entity var1) {
      super.i(var1);
      if(var1 instanceof class_wg && this.cW()) {
         this.d((class_rz)var1);
      }

   }

   public void d(EntityHuman var1) {
      if(this.cW()) {
         this.d((class_rz)var1);
      }

   }

   protected void d(class_rz var1) {
      int var2 = this.db();
      if(this.D(var1) && this.h(var1) < 0.6D * (double)var2 * 0.6D * (double)var2 && var1.a(DamageSource.a((class_rz)this), (float)this.cX())) {
         this.a(class_ng.fk, 1.0F, (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
         this.a(this, var1);
      }

   }

   public float bm() {
      return 0.625F * this.length;
   }

   protected boolean cW() {
      return this.db() > 1;
   }

   protected int cX() {
      return this.db();
   }

   protected class_nf bQ() {
      return this.db() > 1?class_ng.fp:class_ng.fy;
   }

   protected class_nf bR() {
      return this.db() > 1?class_ng.fm:class_ng.fx;
   }

   protected class_nf cY() {
      return this.db() > 1?class_ng.fs:class_ng.fz;
   }

   protected Item I() {
      return this.db() == 1?Items.aT:null;
   }

   protected class_kk J() {
      return this.db() == 1?class_azs.ac:class_azs.a;
   }

   public boolean cF() {
      BlockPosition var1 = new BlockPosition(MathHelper.c(this.locX), 0, MathHelper.c(this.locZ));
      Chunk var2 = this.world.f(var1);
      if(this.world.T().t() == WorldType.c && this.random.nextInt(4) != 1) {
         return false;
      } else {
         if(this.world.ae() != EnumDifficulty.PEACEFUL) {
            BiomeBase var3 = this.world.b(var1);
            if(var3 == class_aik.h && this.locY > 50.0D && this.locY < 70.0D && this.random.nextFloat() < 0.5F && this.random.nextFloat() < this.world.E() && this.world.k(new BlockPosition(this)) <= this.random.nextInt(8)) {
               return super.cF();
            }

            if(this.random.nextInt(10) == 0 && var2.a(987234911L).nextInt(10) == 0 && this.locY < 40.0D) {
               return super.cF();
            }
         }

         return false;
      }
   }

   protected float cc() {
      return 0.4F * (float)this.db();
   }

   public int cD() {
      return 0;
   }

   protected boolean dc() {
      return this.db() > 0;
   }

   protected boolean da() {
      return this.db() > 2;
   }

   protected void cg() {
      this.motY = 0.41999998688697815D;
      this.ai = true;
   }

   public class_sc a(class_qk var1, class_sc var2) {
      int var3 = this.random.nextInt(3);
      if(var3 < 2 && this.random.nextFloat() < 0.5F * var1.c()) {
         ++var3;
      }

      int var4 = 1 << var3;
      this.a(var4);
      return super.a(var1, var2);
   }

   protected class_nf cZ() {
      return class_ng.fq;
   }

   static class class_c_in_class_yw extends class_tj {
      private class_yw a;

      public class_c_in_class_yw(class_yw var1) {
         this.a = var1;
         this.a(5);
      }

      public boolean a() {
         return true;
      }

      public void e() {
         ((class_yw.class_d_in_class_yw)this.a.u()).a(1.0D);
      }
   }

   static class class_b_in_class_yw extends class_tj {
      private class_yw a;

      public class_b_in_class_yw(class_yw var1) {
         this.a = var1;
         this.a(5);
         ((class_vd)var1.x()).c(true);
      }

      public boolean a() {
         return this.a.ah() || this.a.am();
      }

      public void e() {
         if(this.a.bE().nextFloat() < 0.8F) {
            this.a.w().a();
         }

         ((class_yw.class_d_in_class_yw)this.a.u()).a(1.2D);
      }
   }

   static class class_e_in_class_yw extends class_tj {
      private class_yw a;
      private float b;
      private int c;

      public class_e_in_class_yw(class_yw var1) {
         this.a = var1;
         this.a(2);
      }

      public boolean a() {
         return this.a.A() == null && (this.a.onGround || this.a.ah() || this.a.am() || this.a.a((MobEffectType)MobEffectList.y));
      }

      public void e() {
         if(--this.c <= 0) {
            this.c = 40 + this.a.bE().nextInt(60);
            this.b = (float)this.a.bE().nextInt(360);
         }

         ((class_yw.class_d_in_class_yw)this.a.u()).a(this.b, false);
      }
   }

   static class class_a_in_class_yw extends class_tj {
      private class_yw a;
      private int b;

      public class_a_in_class_yw(class_yw var1) {
         this.a = var1;
         this.a(2);
      }

      public boolean a() {
         class_rz var1 = this.a.A();
         return var1 == null?false:(!var1.at()?false:!(var1 instanceof EntityHuman) || !((EntityHuman)var1).abilities.a);
      }

      public void c() {
         this.b = 300;
         super.c();
      }

      public boolean b() {
         class_rz var1 = this.a.A();
         return var1 == null?false:(!var1.at()?false:(var1 instanceof EntityHuman && ((EntityHuman)var1).abilities.a?false:--this.b > 0));
      }

      public void e() {
         this.a.a(this.a.A(), 10.0F, 10.0F);
         ((class_yw.class_d_in_class_yw)this.a.u()).a(this.a.yaw, this.a.cW());
      }
   }

   static class class_d_in_class_yw extends class_sx {
      private float i;
      private int j;
      private class_yw k;
      private boolean l;

      public class_d_in_class_yw(class_yw var1) {
         super(var1);
         this.k = var1;
         this.i = 180.0F * var1.yaw / 3.1415927F;
      }

      public void a(float var1, boolean var2) {
         this.i = var1;
         this.l = var2;
      }

      public void a(double var1) {
         this.e = var1;
         this.h = class_sx.class_a_in_class_sx.MOVE_TO;
      }

      public void c() {
         this.a.yaw = this.a(this.a.yaw, this.i, 90.0F);
         this.a.aO = this.a.yaw;
         this.a.aM = this.a.yaw;
         if(this.h != class_sx.class_a_in_class_sx.MOVE_TO) {
            this.a.o(0.0F);
         } else {
            this.h = class_sx.class_a_in_class_sx.WAIT;
            if(this.a.onGround) {
               this.a.l((float)(this.e * this.a.a((class_sk)class_ys.d).e()));
               if(this.j-- <= 0) {
                  this.j = this.k.cU();
                  if(this.l) {
                     this.j /= 3;
                  }

                  this.k.w().a();
                  if(this.k.dc()) {
                     this.k.a(this.k.cZ(), this.k.cc(), ((this.k.bE().nextFloat() - this.k.bE().nextFloat()) * 0.2F + 1.0F) * 0.8F);
                  }
               } else {
                  this.k.bd = this.k.be = 0.0F;
                  this.a.l(0.0F);
               }
            } else {
               this.a.l((float)(this.e * this.a.a((class_sk)class_ys.d).e()));
            }

         }
      }
   }
}
