package net.minecraft.server;

import java.io.IOException;
import net.minecraft.server.Entity;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketListenerPlayOut;
import net.minecraft.server.class_xz;
/*TODO: Packet changed*/
public class PacketPlayOutEntityWeather implements Packet {
   private int a;
   private double b;
   private double c;
   private double d;
   private int e;

   public PacketPlayOutEntityWeather() {
   }

   public PacketPlayOutEntityWeather(Entity var1) {
      this.a = var1.getId();
      this.b = var1.locX;
      this.c = var1.locY;
      this.d = var1.locZ;
      if(var1 instanceof class_xz) {
         this.e = 1;
      }

   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.readVarInt();
      this.e = var1.readByte();
      this.b = var1.readDouble();
      this.c = var1.readDouble();
      this.d = var1.readDouble();
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.writeVarInt(this.a);
      var1.writeByte(this.e);
      var1.writeDouble(this.b);
      var1.writeDouble(this.c);
      var1.writeDouble(this.d);
   }

   public void a(PacketListenerPlayOut var1) {
      var1.a(this);
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketListenerPlayOut)var1);
   }
}
