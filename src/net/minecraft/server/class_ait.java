package net.minecraft.server;

import net.minecraft.server.BiomeBase;
import net.minecraft.server.Blocks;
import net.minecraft.server.class_vz;

public class class_ait extends BiomeBase {
   public class_ait(BiomeBase.class_a_in_class_aif var1) {
      super(var1);
      this.t.z = -100;
      this.t.A = -100;
      this.t.B = -100;
      this.t.D = 1;
      this.t.J = 1;
      this.r = Blocks.bw.u();
      this.u.clear();
      this.v.clear();
      this.w.clear();
      this.v.add(new BiomeBase.BiomeMeta(class_vz.class, 8, 4, 8));
   }
}
