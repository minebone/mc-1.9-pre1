package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Enchantment;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agn;
import net.minecraft.server.class_agp;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rz;

public class class_rw extends Entity {
   public int a;
   public int b;
   public int c;
   private int d = 5;
   private int e;
   private EntityHuman f;
   private int g;

   public class_rw(World var1, double var2, double var4, double var6, int var8) {
      super(var1);
      this.a(0.5F, 0.5F);
      this.b(var2, var4, var6);
      this.yaw = (float)(Math.random() * 360.0D);
      this.motX = (double)((float)(Math.random() * 0.20000000298023224D - 0.10000000149011612D) * 2.0F);
      this.motY = (double)((float)(Math.random() * 0.2D) * 2.0F);
      this.motZ = (double)((float)(Math.random() * 0.20000000298023224D - 0.10000000149011612D) * 2.0F);
      this.e = var8;
   }

   protected boolean ad() {
      return false;
   }

   public class_rw(World var1) {
      super(var1);
      this.a(0.25F, 0.25F);
   }

   protected void i() {
   }

   public void m() {
      super.m();
      if(this.c > 0) {
         --this.c;
      }

      this.lastX = this.locX;
      this.lastY = this.locY;
      this.lastZ = this.locZ;
      this.motY -= 0.029999999329447746D;
      if(this.world.getType(new BlockPosition(this)).getMaterial() == Material.i) {
         this.motY = 0.20000000298023224D;
         this.motX = (double)((this.random.nextFloat() - this.random.nextFloat()) * 0.2F);
         this.motZ = (double)((this.random.nextFloat() - this.random.nextFloat()) * 0.2F);
         this.a(class_ng.bx, 0.4F, 2.0F + this.random.nextFloat() * 0.4F);
      }

      this.j(this.locX, (this.bk().b + this.bk().e) / 2.0D, this.locZ);
      double var1 = 8.0D;
      if(this.g < this.a - 20 + this.getId() % 100) {
         if(this.f == null || this.f.h(this) > var1 * var1) {
            this.f = this.world.a(this, var1);
         }

         this.g = this.a;
      }

      if(this.f != null && this.f.y()) {
         this.f = null;
      }

      if(this.f != null) {
         double var3 = (this.f.locX - this.locX) / var1;
         double var5 = (this.f.locY + (double)this.f.bm() / 2.0D - this.locY) / var1;
         double var7 = (this.f.locZ - this.locZ) / var1;
         double var9 = Math.sqrt(var3 * var3 + var5 * var5 + var7 * var7);
         double var11 = 1.0D - var9;
         if(var11 > 0.0D) {
            var11 *= var11;
            this.motX += var3 / var9 * var11 * 0.1D;
            this.motY += var5 / var9 * var11 * 0.1D;
            this.motZ += var7 / var9 * var11 * 0.1D;
         }
      }

      this.d(this.motX, this.motY, this.motZ);
      float var13 = 0.98F;
      if(this.onGround) {
         var13 = this.world.getType(new BlockPosition(MathHelper.c(this.locX), MathHelper.c(this.bk().b) - 1, MathHelper.c(this.locZ))).getBlock().z * 0.98F;
      }

      this.motX *= (double)var13;
      this.motY *= 0.9800000190734863D;
      this.motZ *= (double)var13;
      if(this.onGround) {
         this.motY *= -0.8999999761581421D;
      }

      ++this.a;
      ++this.b;
      if(this.b >= 6000) {
         this.S();
      }

   }

   public boolean ai() {
      return this.world.a((AxisAlignedBB)this.bk(), (Material)Material.h, (Entity)this);
   }

   protected void h(int var1) {
      this.a(DamageSource.a, (float)var1);
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b(var1)) {
         return false;
      } else {
         this.an();
         this.d = (int)((float)this.d - var2);
         if(this.d <= 0) {
            this.S();
         }

         return false;
      }
   }

   public void b(NBTTagCompound var1) {
      var1.a("Health", (short)this.d);
      var1.a("Age", (short)this.b);
      var1.a("Value", (short)this.e);
   }

   public void a(NBTTagCompound var1) {
      this.d = var1.g("Health");
      this.b = var1.g("Age");
      this.e = var1.g("Value");
   }

   public void d(EntityHuman var1) {
      if(!this.world.E) {
         if(this.c == 0 && var1.by == 0) {
            var1.by = 2;
            this.world.a((EntityHuman)null, var1.locX, var1.locY, var1.locZ, class_ng.bg, EnumSoundCategory.PLAYERS, 0.1F, 0.5F * ((this.random.nextFloat() - this.random.nextFloat()) * 0.7F + 1.8F));
            var1.a(this, 1);
            ItemStack var2 = class_agn.b((Enchantment)class_agp.A, (class_rz)var1);
            if(var2 != null && var2.g()) {
               int var3 = Math.min(this.d(this.e), var2.h());
               this.e -= this.b(var3);
               var2.b(var2.h() - var3);
            }

            if(this.e > 0) {
               var1.n(this.e);
            }

            this.S();
         }

      }
   }

   private int b(int var1) {
      return var1 / 2;
   }

   private int d(int var1) {
      return var1 * 2;
   }

   public int j() {
      return this.e;
   }

   public static int a(int var0) {
      return var0 >= 2477?2477:(var0 >= 1237?1237:(var0 >= 617?617:(var0 >= 307?307:(var0 >= 149?149:(var0 >= 73?73:(var0 >= 37?37:(var0 >= 17?17:(var0 >= 7?7:(var0 >= 3?3:1)))))))));
   }

   public boolean aS() {
      return false;
   }
}
