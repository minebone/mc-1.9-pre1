package net.minecraft.server;

import com.google.common.collect.Sets;
import java.util.Set;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Item;
import net.minecraft.server.ItemTool;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;

public class class_aea extends ItemTool {
   private static final Set e = Sets.newHashSet((Object[])(new Block[]{Blocks.cs, Blocks.q, Blocks.e, Blocks.E, Blocks.ah, Blocks.ag, Blocks.T, Blocks.D, Blocks.R, Blocks.o, Blocks.aI, Blocks.S, Blocks.p, Blocks.y, Blocks.x, Blocks.aD, Blocks.Y, Blocks.aV, Blocks.cB, Blocks.av, Blocks.aC, Blocks.A, Blocks.cM, Blocks.b, Blocks.U, Blocks.aG, Blocks.az}));

   protected class_aea(Item.class_a_in_class_adn var1) {
      super(1.0F, -2.8F, var1, e);
   }

   public boolean a(IBlockData var1) {
      Block var2 = var1.getBlock();
      if(var2 == Blocks.Z) {
         return this.d.d() == 3;
      } else if(var2 != Blocks.ah && var2 != Blocks.ag) {
         if(var2 != Blocks.bP && var2 != Blocks.bT) {
            if(var2 != Blocks.R && var2 != Blocks.o) {
               if(var2 != Blocks.S && var2 != Blocks.p) {
                  if(var2 != Blocks.y && var2 != Blocks.x) {
                     if(var2 != Blocks.aC && var2 != Blocks.aD) {
                        Material var3 = var1.getMaterial();
                        return var3 == Material.e?true:(var3 == Material.f?true:var3 == Material.g);
                     } else {
                        return this.d.d() >= 2;
                     }
                  } else {
                     return this.d.d() >= 1;
                  }
               } else {
                  return this.d.d() >= 1;
               }
            } else {
               return this.d.d() >= 2;
            }
         } else {
            return this.d.d() >= 2;
         }
      } else {
         return this.d.d() >= 2;
      }
   }

   public float a(ItemStack var1, IBlockData var2) {
      Material var3 = var2.getMaterial();
      return var3 != Material.f && var3 != Material.g && var3 != Material.e?super.a(var1, var2):this.a;
   }
}
