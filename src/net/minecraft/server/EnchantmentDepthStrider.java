package net.minecraft.server;

import net.minecraft.server.Enchantment;
import net.minecraft.server.class_agm;
import net.minecraft.server.class_agp;
import net.minecraft.server.EnumInventorySlot;

public class EnchantmentDepthStrider extends Enchantment {
   public EnchantmentDepthStrider(Enchantment.class_a_in_class_agl var1, EnumInventorySlot... var2) {
      super(var1, class_agm.ARMOR_FEET, var2);
      this.c("waterWalker");
   }

   public int a(int var1) {
      return var1 * 10;
   }

   public int b(int var1) {
      return this.a(var1) + 15;
   }

   public int b() {
      return 3;
   }

   public boolean a(Enchantment var1) {
      return super.a(var1) && var1 != class_agp.j;
   }
}
