package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.World;
import net.minecraft.server.class_aoa;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_ajl extends Block {
   protected class_ajl() {
      super(Material.I);
      this.k();
      this.b(6000001.0F);
      this.q();
      this.n = true;
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.INVISIBLE;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, float var4, int var5) {
   }
}
