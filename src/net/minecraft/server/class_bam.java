package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import java.util.Random;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_afp;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_bae;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_kk;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_bam extends class_bae {
   private static final Logger a = LogManager.getLogger();

   public class_bam(class_baq[] var1) {
      super(var1);
   }

   public ItemStack a(ItemStack var1, Random var2, class_azy var3) {
      ItemStack var4 = class_afp.a().a(var1);
      if(var4 == null) {
         a.warn("Couldn\'t smelt " + var1 + " because there is no smelting recipe");
         return var1;
      } else {
         ItemStack var5 = var4.k();
         var5.b = var1.b;
         return var5;
      }
   }

   public static class class_a_in_class_bam extends class_bae.class_a_in_class_bae {
      protected class_a_in_class_bam() {
         super(new class_kk("furnace_smelt"), class_bam.class);
      }

      public void a(JsonObject var1, class_bam var2, JsonSerializationContext var3) {
      }

      public class_bam a(JsonObject var1, JsonDeserializationContext var2, class_baq[] var3) {
         return new class_bam(var3);
      }

      // $FF: synthetic method
      public class_bae b(JsonObject var1, JsonDeserializationContext var2, class_baq[] var3) {
         return this.a(var1, var2, var3);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(JsonObject var1, class_bae var2, JsonSerializationContext var3) {
         this.a(var1, (class_bam)var2, var3);
      }
   }
}
