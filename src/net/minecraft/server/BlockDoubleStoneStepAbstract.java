package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockSand;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Item;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aly;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;
import net.minecraft.server.class_or;

public abstract class BlockDoubleStoneStepAbstract extends class_aly {
   public static final class_arm d = class_arm.a("seamless");
   public static final BlockStateEnum e = BlockStateEnum.a("variant", BlockDoubleStoneStepAbstract.EnumStoneSlab2Variant.class);

   public BlockDoubleStoneStepAbstract() {
      super(Material.e);
      IBlockData var1 = this.A.b();
      if(this.e()) {
         var1 = var1.set(d, Boolean.valueOf(false));
      } else {
         var1 = var1.set(a, class_aly.class_a_in_class_aly.BOTTOM);
      }

      this.w(var1.set(e, BlockDoubleStoneStepAbstract.EnumStoneSlab2Variant.RED_SANDSTONE));
      this.a(CreativeModeTab.b);
   }

   public String c() {
      return class_di.a(this.a() + ".red_sandstone.name");
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Item.a((Block)Blocks.cP);
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Blocks.cP, 1, ((BlockDoubleStoneStepAbstract.EnumStoneSlab2Variant)var3.get(e)).a());
   }

   public String e(int var1) {
      return super.a() + "." + BlockDoubleStoneStepAbstract.EnumStoneSlab2Variant.a(var1).d();
   }

   public IBlockState g() {
      return e;
   }

   public Comparable a(ItemStack var1) {
      return BlockDoubleStoneStepAbstract.EnumStoneSlab2Variant.a(var1.i() & 7);
   }

   public IBlockData a(int var1) {
      IBlockData var2 = this.u().set(e, BlockDoubleStoneStepAbstract.EnumStoneSlab2Variant.a(var1 & 7));
      if(this.e()) {
         var2 = var2.set(d, Boolean.valueOf((var1 & 8) != 0));
      } else {
         var2 = var2.set(a, (var1 & 8) == 0?class_aly.class_a_in_class_aly.BOTTOM:class_aly.class_a_in_class_aly.TOP);
      }

      return var2;
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((BlockDoubleStoneStepAbstract.EnumStoneSlab2Variant)var1.get(e)).a();
      if(this.e()) {
         if(((Boolean)var1.get(d)).booleanValue()) {
            var3 |= 8;
         }
      } else if(var1.get(a) == class_aly.class_a_in_class_aly.TOP) {
         var3 |= 8;
      }

      return var3;
   }

   protected BlockStateList b() {
      return this.e()?new BlockStateList(this, new IBlockState[]{d, e}):new BlockStateList(this, new IBlockState[]{a, e});
   }

   public MaterialMapColor r(IBlockData var1) {
      return ((BlockDoubleStoneStepAbstract.EnumStoneSlab2Variant)var1.get(e)).c();
   }

   public int d(IBlockData var1) {
      return ((BlockDoubleStoneStepAbstract.EnumStoneSlab2Variant)var1.get(e)).a();
   }

   public static enum EnumStoneSlab2Variant implements class_or {
      RED_SANDSTONE(0, "red_sandstone", BlockSand.EnumSandVariant.RED_SAND.c());

      private static final BlockDoubleStoneStepAbstract.EnumStoneSlab2Variant[] b = new BlockDoubleStoneStepAbstract.EnumStoneSlab2Variant[values().length];
      private final int c;
      private final String d;
      private final MaterialMapColor e;

      private EnumStoneSlab2Variant(int var3, String var4, MaterialMapColor var5) {
         this.c = var3;
         this.d = var4;
         this.e = var5;
      }

      public int a() {
         return this.c;
      }

      public MaterialMapColor c() {
         return this.e;
      }

      public String toString() {
         return this.d;
      }

      public static BlockDoubleStoneStepAbstract.EnumStoneSlab2Variant a(int var0) {
         if(var0 < 0 || var0 >= b.length) {
            var0 = 0;
         }

         return b[var0];
      }

      public String m() {
         return this.d;
      }

      public String d() {
         return this.d;
      }

      static {
         BlockDoubleStoneStepAbstract.EnumStoneSlab2Variant[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            BlockDoubleStoneStepAbstract.EnumStoneSlab2Variant var3 = var0[var2];
            b[var3.a()] = var3;
         }

      }
   }
}
