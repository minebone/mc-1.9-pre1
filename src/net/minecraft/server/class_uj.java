package net.minecraft.server;

import net.minecraft.server.EntityCreature;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_vd;

public class class_uj extends class_tj {
   private EntityCreature a;

   public class_uj(EntityCreature var1) {
      this.a = var1;
   }

   public boolean a() {
      return this.a.world.B() && this.a.a(EnumInventorySlot.HEAD) == null;
   }

   public void c() {
      ((class_vd)this.a.x()).d(true);
   }

   public void d() {
      ((class_vd)this.a.x()).d(false);
   }
}
