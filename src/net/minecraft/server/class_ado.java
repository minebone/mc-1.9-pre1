package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import net.minecraft.server.Item;
import net.minecraft.server.MathHelper;

public class class_ado {
   private final Map a = Maps.newHashMap();
   private int b;

   public boolean a(Item var1) {
      return this.a(var1, 0.0F) > 0.0F;
   }

   public float a(Item var1, float var2) {
      class_ado.class_a_in_class_ado var3 = (class_ado.class_a_in_class_ado)this.a.get(var1);
      if(var3 != null) {
         float var4 = (float)(var3.b - var3.a);
         float var5 = (float)var3.b - ((float)this.b + var2);
         return MathHelper.a(var5 / var4, 0.0F, 1.0F);
      } else {
         return 0.0F;
      }
   }

   public void a() {
      ++this.b;
      if(!this.a.isEmpty()) {
         Iterator var1 = this.a.entrySet().iterator();

         while(var1.hasNext()) {
            Entry var2 = (Entry)var1.next();
            if(((class_ado.class_a_in_class_ado)var2.getValue()).b <= this.b) {
               var1.remove();
               this.c((Item)var2.getKey());
            }
         }
      }

   }

   public void a(Item var1, int var2) {
      this.a.put(var1, new class_ado.class_a_in_class_ado(this.b, this.b + var2));
      this.b(var1, var2);
   }

   protected void b(Item var1, int var2) {
   }

   protected void c(Item var1) {
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
   }

   class class_a_in_class_ado {
      final int a;
      final int b;

      private class_a_in_class_ado(int var2, int var3) {
         this.a = var2;
         this.b = var3;
      }

      // $FF: synthetic method
      class_a_in_class_ado(int var2, int var3, class_ado.SyntheticClass_1 var4) {
         this(var2, var3);
      }
   }
}
