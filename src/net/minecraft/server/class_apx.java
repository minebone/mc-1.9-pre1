package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.Entity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.class_ahi;
import net.minecraft.server.class_akj;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutTileEntityData;

public class class_apx extends TileEntity {
   private boolean a;
   private boolean f;
   private boolean g;
   private boolean h;
   private final class_ahi i = new class_ahi() {
      public BlockPosition c() {
         return class_apx.this.c;
      }

      public Vec3D d() {
         return new Vec3D((double)class_apx.this.c.p() + 0.5D, (double)class_apx.this.c.q() + 0.5D, (double)class_apx.this.c.r() + 0.5D);
      }

      public World e() {
         return class_apx.this.D();
      }

      public void a(String var1) {
         super.a(var1);
         class_apx.this.v_();
      }

      public void i() {
         IBlockData var1 = class_apx.this.b.getType(class_apx.this.c);
         class_apx.this.D().a(class_apx.this.c, var1, var1, 3);
      }

      public Entity f() {
         return null;
      }

      public MinecraftServer h() {
         return class_apx.this.b.u();
      }
   };

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.i.a(var1);
      var1.a("powered", this.d());
      var1.a("conditionMet", this.g());
      var1.a("auto", this.e());
   }

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      this.i.b(var2);
      this.a(var2.p("powered"));
      this.c(var2.p("conditionMet"));
      this.b(var2.p("auto"));
   }

   public Packet D_() {
      if(this.h()) {
         this.d(false);
         NBTTagCompound var1 = new NBTTagCompound();
         this.a(var1);
         return new PacketPlayOutTileEntityData(this.c, 2, var1);
      } else {
         return null;
      }
   }

   public boolean B() {
      return true;
   }

   public class_ahi b() {
      return this.i;
   }

   public CommandObjectiveExecutor c() {
      return this.i.o();
   }

   public void a(boolean var1) {
      this.a = var1;
   }

   public boolean d() {
      return this.a;
   }

   public boolean e() {
      return this.f;
   }

   public void b(boolean var1) {
      boolean var2 = this.f;
      this.f = var1;
      if(!var2 && var1 && !this.a && this.b != null && this.i() != class_apx.class_a_in_class_apx.SEQUENCE) {
         Block var3 = this.w();
         if(var3 instanceof class_akj) {
            BlockPosition var4 = this.v();
            class_akj var5 = (class_akj)var3;
            this.g = !this.j() || var5.e(this.b, var4, this.b.getType(var4));
            this.b.a(var4, var3, var3.a(this.b));
            if(this.g) {
               var5.c(this.b, var4);
            }
         }
      }

   }

   public boolean g() {
      return this.g;
   }

   public void c(boolean var1) {
      this.g = var1;
   }

   public boolean h() {
      return this.h;
   }

   public void d(boolean var1) {
      this.h = var1;
   }

   public class_apx.class_a_in_class_apx i() {
      Block var1 = this.w();
      return var1 == Blocks.bX?class_apx.class_a_in_class_apx.REDSTONE:(var1 == Blocks.dc?class_apx.class_a_in_class_apx.AUTO:(var1 == Blocks.dd?class_apx.class_a_in_class_apx.SEQUENCE:class_apx.class_a_in_class_apx.REDSTONE));
   }

   public boolean j() {
      IBlockData var1 = this.b.getType(this.v());
      return var1.getBlock() instanceof class_akj?((Boolean)var1.get(class_akj.b)).booleanValue():false;
   }

   public void z() {
      this.e = null;
      super.z();
   }

   public static enum class_a_in_class_apx {
      SEQUENCE,
      AUTO,
      REDSTONE;
   }
}
