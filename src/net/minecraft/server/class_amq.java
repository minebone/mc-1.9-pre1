package net.minecraft.server;

import net.minecraft.server.EnumDirection;
import net.minecraft.server.class_aod;

public enum class_amq {
   NONE("no_mirror"),
   LEFT_RIGHT("mirror_left_right"),
   FRONT_BACK("mirror_front_back");

   private final String d;
   private static String[] e = new String[values().length];

   private class_amq(String var3) {
      this.d = var3;
   }

   public int a(int var1, int var2) {
      int var3 = var2 / 2;
      int var4 = var1 > var3?var1 - var2:var1;
      switch(class_amq.SyntheticClass_1.a[this.ordinal()]) {
      case 1:
         return (var2 - var4) % var2;
      case 2:
         return (var3 - var4 + var2) % var2;
      default:
         return var1;
      }
   }

   public class_aod a(EnumDirection var1) {
      EnumDirection.class_a_in_class_cq var2 = var1.k();
      return (this != LEFT_RIGHT || var2 != EnumDirection.class_a_in_class_cq.Z) && (this != FRONT_BACK || var2 != EnumDirection.class_a_in_class_cq.X)?class_aod.NONE:class_aod.CLOCKWISE_180;
   }

   public EnumDirection b(EnumDirection var1) {
      switch(class_amq.SyntheticClass_1.a[this.ordinal()]) {
      case 1:
         if(var1 == EnumDirection.WEST) {
            return EnumDirection.EAST;
         } else {
            if(var1 == EnumDirection.EAST) {
               return EnumDirection.WEST;
            }

            return var1;
         }
      case 2:
         if(var1 == EnumDirection.NORTH) {
            return EnumDirection.SOUTH;
         } else {
            if(var1 == EnumDirection.SOUTH) {
               return EnumDirection.NORTH;
            }

            return var1;
         }
      default:
         return var1;
      }
   }

   static {
      int var0 = 0;
      class_amq[] var1 = values();
      int var2 = var1.length;

      for(int var3 = 0; var3 < var2; ++var3) {
         class_amq var4 = var1[var3];
         e[var0++] = var4.d;
      }

   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[class_amq.values().length];

      static {
         try {
            a[class_amq.LEFT_RIGHT.ordinal()] = 1;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[class_amq.FRONT_BACK.ordinal()] = 2;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
