package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.InventoryEnderChest;
import net.minecraft.server.Item;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_amf;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aoa;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_aqd;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arn;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.IInventory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;

public class class_ale extends class_ajm {
   public static final class_arn a = class_amf.D;
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.0625D, 0.0D, 0.0625D, 0.9375D, 0.875D, 0.9375D);

   protected class_ale() {
      super(Material.e);
      this.w(this.A.b().set(a, EnumDirection.NORTH));
      this.a((CreativeModeTab)CreativeModeTab.c);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return b;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.ENTITYBLOCK_ANIMATED;
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Item.a(Blocks.Z);
   }

   public int a(Random var1) {
      return 8;
   }

   protected boolean o() {
      return true;
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return this.u().set(a, var8.bh().d());
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, class_rz var4, ItemStack var5) {
      var1.a((BlockPosition)var2, (IBlockData)var3.set(a, var4.bh().d()), 2);
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      InventoryEnderChest var11 = var4.cU();
      TileEntity var12 = var1.r(var2);
      if(var11 != null && var12 instanceof class_aqd) {
         if(var1.getType(var2.a()).l()) {
            return true;
         } else if(var1.E) {
            return true;
         } else {
            var11.a((class_aqd)var12);
            var4.openContainer((IInventory)var11);
            var4.b(StatisticList.X);
            return true;
         }
      } else {
         return true;
      }
   }

   public TileEntity a(World var1, int var2) {
      return new class_aqd();
   }

   public IBlockData a(int var1) {
      EnumDirection var2 = EnumDirection.a(var1);
      if(var2.k() == EnumDirection.class_a_in_class_cq.Y) {
         var2 = EnumDirection.NORTH;
      }

      return this.u().set(a, var2);
   }

   public int e(IBlockData var1) {
      return ((EnumDirection)var1.get(a)).a();
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(a, var2.a((EnumDirection)var1.get(a)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(a)));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }
}
