package net.minecraft.server;

import net.minecraft.server.ChatComponentText;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.Item;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.NBTTagString;
import net.minecraft.server.StatisticList;
import net.minecraft.server.UtilColor;
import net.minecraft.server.World;
import net.minecraft.server.class_abs;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_afb;
import net.minecraft.server.class_ev;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutSetSlot;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.IInventory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;

public class ItemWrittenBook extends Item {
   public ItemWrittenBook() {
      this.d(1);
   }

   public static boolean b(NBTTagCompound var0) {
      if(!class_afb.b(var0)) {
         return false;
      } else if(!var0.b("title", 8)) {
         return false;
      } else {
         String var1 = var0.l("title");
         return var1 != null && var1.length() <= 32?var0.b("author", 8):false;
      }
   }

   public static int h(ItemStack var0) {
      return var0.o().h("generation");
   }

   public String a(ItemStack var1) {
      if(var1.n()) {
         NBTTagCompound var2 = var1.o();
         String var3 = var2.l("title");
         if(!UtilColor.b(var3)) {
            return var3;
         }
      }

      return super.a(var1);
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      if(!var2.E) {
         this.a(var1, var3);
      }

      var3.a(var1, var4);
      var3.b(StatisticList.b((Item)this));
      return new class_qo(EnumResult.SUCCESS, var1);
   }

   private void a(ItemStack var1, EntityHuman var2) {
      if(var1 != null && var1.o() != null) {
         NBTTagCompound var3 = var1.o();
         if(!var3.p("resolved")) {
            var3.a("resolved", true);
            if(b(var3)) {
               NBTTagList var4 = var3.c("pages", 8);

               for(int var5 = 0; var5 < var4.c(); ++var5) {
                  String var6 = var4.g(var5);

                  Object var7;
                  try {
                     IChatBaseComponent var11 = IChatBaseComponent.ChatSerializer.b(var6);
                     var7 = class_ev.a(var2, var11, var2);
                  } catch (Exception var9) {
                     var7 = new ChatComponentText(var6);
                  }

                  var4.a(var5, new NBTTagString(IChatBaseComponent.ChatSerializer.a((IChatBaseComponent)var7)));
               }

               var3.a((String)"pages", (NBTTag)var4);
               if(var2 instanceof EntityPlayer && var2.ca() == var1) {
                  class_abs var10 = var2.bt.a((IInventory)var2.br, var2.br.d);
                  ((EntityPlayer)var2).a.a((Packet)(new PacketPlayOutSetSlot(0, var10.e, var1)));
               }

            }
         }
      }
   }
}
