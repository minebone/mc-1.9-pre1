package net.minecraft.server;

import java.util.List;
import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.Blocks;
import net.minecraft.server.Chunk;
import net.minecraft.server.EnumCreatureType;
import net.minecraft.server.World;
import net.minecraft.server.class_ahm;
import net.minecraft.server.class_alg;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ari;
import net.minecraft.server.class_arx;
import net.minecraft.server.class_ate;
import net.minecraft.server.class_atj;
import net.minecraft.server.class_atk;
import net.minecraft.server.class_att;
import net.minecraft.server.class_auc;
import net.minecraft.server.class_auf;
import net.minecraft.server.class_aug;
import net.minecraft.server.class_auh;
import net.minecraft.server.class_aum;
import net.minecraft.server.class_aus;
import net.minecraft.server.class_avu;
import net.minecraft.server.class_awt;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;

public class class_atl implements class_arx {
   protected static final IBlockData a = Blocks.AIR.u();
   protected static final IBlockData b = Blocks.aV.u();
   protected static final IBlockData c = Blocks.h.u();
   protected static final IBlockData d = Blocks.l.u();
   protected static final IBlockData e = Blocks.n.u();
   protected static final IBlockData f = Blocks.aW.u();
   private final World n;
   private final boolean o;
   private final Random p;
   private double[] q = new double[256];
   private double[] r = new double[256];
   private double[] s = new double[256];
   private double[] t;
   private final class_awt u;
   private final class_awt v;
   private final class_awt w;
   private final class_awt x;
   private final class_awt y;
   public final class_awt g;
   public final class_awt h;
   private final class_auf z = new class_auf();
   private final class_aum A = new class_aum();
   private final class_aug B = new class_aug();
   private final class_auc C = new class_aus(Blocks.co.u(), 14, class_ari.a(Blocks.aV));
   private final class_auh D = new class_auh(Blocks.k, true);
   private final class_auh E = new class_auh(Blocks.k, false);
   private final class_att F = new class_att(Blocks.P);
   private final class_att G = new class_att(Blocks.Q);
   private final class_avu H = new class_avu();
   private final class_atj I = new class_atk();
   double[] i;
   double[] j;
   double[] k;
   double[] l;
   double[] m;

   public class_atl(World var1, boolean var2, long var3) {
      this.n = var1;
      this.o = var2;
      this.p = new Random(var3);
      this.u = new class_awt(this.p, 16);
      this.v = new class_awt(this.p, 16);
      this.w = new class_awt(this.p, 8);
      this.x = new class_awt(this.p, 4);
      this.y = new class_awt(this.p, 4);
      this.g = new class_awt(this.p, 10);
      this.h = new class_awt(this.p, 16);
      var1.b(63);
   }

   public void a(int var1, int var2, class_ate var3) {
      byte var4 = 4;
      int var5 = this.n.K() / 2 + 1;
      int var6 = var4 + 1;
      byte var7 = 17;
      int var8 = var4 + 1;
      this.t = this.a(this.t, var1 * var4, 0, var2 * var4, var6, var7, var8);

      for(int var9 = 0; var9 < var4; ++var9) {
         for(int var10 = 0; var10 < var4; ++var10) {
            for(int var11 = 0; var11 < 16; ++var11) {
               double var12 = 0.125D;
               double var14 = this.t[((var9 + 0) * var8 + var10 + 0) * var7 + var11 + 0];
               double var16 = this.t[((var9 + 0) * var8 + var10 + 1) * var7 + var11 + 0];
               double var18 = this.t[((var9 + 1) * var8 + var10 + 0) * var7 + var11 + 0];
               double var20 = this.t[((var9 + 1) * var8 + var10 + 1) * var7 + var11 + 0];
               double var22 = (this.t[((var9 + 0) * var8 + var10 + 0) * var7 + var11 + 1] - var14) * var12;
               double var24 = (this.t[((var9 + 0) * var8 + var10 + 1) * var7 + var11 + 1] - var16) * var12;
               double var26 = (this.t[((var9 + 1) * var8 + var10 + 0) * var7 + var11 + 1] - var18) * var12;
               double var28 = (this.t[((var9 + 1) * var8 + var10 + 1) * var7 + var11 + 1] - var20) * var12;

               for(int var30 = 0; var30 < 8; ++var30) {
                  double var31 = 0.25D;
                  double var33 = var14;
                  double var35 = var16;
                  double var37 = (var18 - var14) * var31;
                  double var39 = (var20 - var16) * var31;

                  for(int var41 = 0; var41 < 4; ++var41) {
                     double var42 = 0.25D;
                     double var44 = var33;
                     double var46 = (var35 - var33) * var42;

                     for(int var48 = 0; var48 < 4; ++var48) {
                        IBlockData var49 = null;
                        if(var11 * 8 + var30 < var5) {
                           var49 = d;
                        }

                        if(var44 > 0.0D) {
                           var49 = b;
                        }

                        int var50 = var41 + var9 * 4;
                        int var51 = var30 + var11 * 8;
                        int var52 = var48 + var10 * 4;
                        var3.a(var50, var51, var52, var49);
                        var44 += var46;
                     }

                     var33 += var37;
                     var35 += var39;
                  }

                  var14 += var22;
                  var16 += var24;
                  var18 += var26;
                  var20 += var28;
               }
            }
         }
      }

   }

   public void b(int var1, int var2, class_ate var3) {
      int var4 = this.n.K() + 1;
      double var5 = 0.03125D;
      this.q = this.x.a(this.q, var1 * 16, var2 * 16, 0, 16, 16, 1, var5, var5, 1.0D);
      this.r = this.x.a(this.r, var1 * 16, 109, var2 * 16, 16, 1, 16, var5, 1.0D, var5);
      this.s = this.y.a(this.s, var1 * 16, var2 * 16, 0, 16, 16, 1, var5 * 2.0D, var5 * 2.0D, var5 * 2.0D);

      for(int var7 = 0; var7 < 16; ++var7) {
         for(int var8 = 0; var8 < 16; ++var8) {
            boolean var9 = this.q[var7 + var8 * 16] + this.p.nextDouble() * 0.2D > 0.0D;
            boolean var10 = this.r[var7 + var8 * 16] + this.p.nextDouble() * 0.2D > 0.0D;
            int var11 = (int)(this.s[var7 + var8 * 16] / 3.0D + 3.0D + this.p.nextDouble() * 0.25D);
            int var12 = -1;
            IBlockData var13 = b;
            IBlockData var14 = b;

            for(int var15 = 127; var15 >= 0; --var15) {
               if(var15 < 127 - this.p.nextInt(5) && var15 > this.p.nextInt(5)) {
                  IBlockData var16 = var3.a(var8, var15, var7);
                  if(var16.getBlock() != null && var16.getMaterial() != Material.a) {
                     if(var16.getBlock() == Blocks.aV) {
                        if(var12 == -1) {
                           if(var11 <= 0) {
                              var13 = a;
                              var14 = b;
                           } else if(var15 >= var4 - 4 && var15 <= var4 + 1) {
                              var13 = b;
                              var14 = b;
                              if(var10) {
                                 var13 = e;
                                 var14 = b;
                              }

                              if(var9) {
                                 var13 = f;
                                 var14 = f;
                              }
                           }

                           if(var15 < var4 && (var13 == null || var13.getMaterial() == Material.a)) {
                              var13 = d;
                           }

                           var12 = var11;
                           if(var15 >= var4 - 1) {
                              var3.a(var8, var15, var7, var13);
                           } else {
                              var3.a(var8, var15, var7, var14);
                           }
                        } else if(var12 > 0) {
                           --var12;
                           var3.a(var8, var15, var7, var14);
                        }
                     }
                  } else {
                     var12 = -1;
                  }
               } else {
                  var3.a(var8, var15, var7, c);
               }
            }
         }
      }

   }

   public Chunk a(int var1, int var2) {
      this.p.setSeed((long)var1 * 341873128712L + (long)var2 * 132897987541L);
      class_ate var3 = new class_ate();
      this.a(var1, var2, var3);
      this.b(var1, var2, var3);
      this.I.a(this.n, var1, var2, var3);
      if(this.o) {
         this.H.a(this.n, var1, var2, var3);
      }

      Chunk var4 = new Chunk(this.n, var3, var1, var2);
      BiomeBase[] var5 = this.n.A().b((BiomeBase[])null, var1 * 16, var2 * 16, 16, 16);
      byte[] var6 = var4.l();

      for(int var7 = 0; var7 < var6.length; ++var7) {
         var6[var7] = (byte)BiomeBase.a(var5[var7]);
      }

      var4.m();
      return var4;
   }

   private double[] a(double[] var1, int var2, int var3, int var4, int var5, int var6, int var7) {
      if(var1 == null) {
         var1 = new double[var5 * var6 * var7];
      }

      double var8 = 684.412D;
      double var10 = 2053.236D;
      this.l = this.g.a(this.l, var2, var3, var4, var5, 1, var7, 1.0D, 0.0D, 1.0D);
      this.m = this.h.a(this.m, var2, var3, var4, var5, 1, var7, 100.0D, 0.0D, 100.0D);
      this.i = this.w.a(this.i, var2, var3, var4, var5, var6, var7, var8 / 80.0D, var10 / 60.0D, var8 / 80.0D);
      this.j = this.u.a(this.j, var2, var3, var4, var5, var6, var7, var8, var10, var8);
      this.k = this.v.a(this.k, var2, var3, var4, var5, var6, var7, var8, var10, var8);
      int var12 = 0;
      double[] var13 = new double[var6];

      int var14;
      for(var14 = 0; var14 < var6; ++var14) {
         var13[var14] = Math.cos((double)var14 * 3.141592653589793D * 6.0D / (double)var6) * 2.0D;
         double var15 = (double)var14;
         if(var14 > var6 / 2) {
            var15 = (double)(var6 - 1 - var14);
         }

         if(var15 < 4.0D) {
            var15 = 4.0D - var15;
            var13[var14] -= var15 * var15 * var15 * 10.0D;
         }
      }

      for(var14 = 0; var14 < var5; ++var14) {
         for(int var31 = 0; var31 < var7; ++var31) {
            double var16 = 0.0D;

            for(int var18 = 0; var18 < var6; ++var18) {
               double var19 = 0.0D;
               double var21 = var13[var18];
               double var23 = this.j[var12] / 512.0D;
               double var25 = this.k[var12] / 512.0D;
               double var27 = (this.i[var12] / 10.0D + 1.0D) / 2.0D;
               if(var27 < 0.0D) {
                  var19 = var23;
               } else if(var27 > 1.0D) {
                  var19 = var25;
               } else {
                  var19 = var23 + (var25 - var23) * var27;
               }

               var19 -= var21;
               double var29;
               if(var18 > var6 - 4) {
                  var29 = (double)((float)(var18 - (var6 - 4)) / 3.0F);
                  var19 = var19 * (1.0D - var29) + -10.0D * var29;
               }

               if((double)var18 < var16) {
                  var29 = (var16 - (double)var18) / 4.0D;
                  var29 = MathHelper.a(var29, 0.0D, 1.0D);
                  var19 = var19 * (1.0D - var29) + -10.0D * var29;
               }

               var1[var12] = var19;
               ++var12;
            }
         }
      }

      return var1;
   }

   public void b(int var1, int var2) {
      class_alg.f = true;
      BlockPosition var3 = new BlockPosition(var1 * 16, 0, var2 * 16);
      class_ahm var4 = new class_ahm(var1, var2);
      this.H.a(this.n, this.p, var4);

      int var5;
      for(var5 = 0; var5 < 8; ++var5) {
         this.E.b(this.n, this.p, var3.a(this.p.nextInt(16) + 8, this.p.nextInt(120) + 4, this.p.nextInt(16) + 8));
      }

      for(var5 = 0; var5 < this.p.nextInt(this.p.nextInt(10) + 1) + 1; ++var5) {
         this.z.b(this.n, this.p, var3.a(this.p.nextInt(16) + 8, this.p.nextInt(120) + 4, this.p.nextInt(16) + 8));
      }

      for(var5 = 0; var5 < this.p.nextInt(this.p.nextInt(10) + 1); ++var5) {
         this.A.b(this.n, this.p, var3.a(this.p.nextInt(16) + 8, this.p.nextInt(120) + 4, this.p.nextInt(16) + 8));
      }

      for(var5 = 0; var5 < 10; ++var5) {
         this.B.b(this.n, this.p, var3.a(this.p.nextInt(16) + 8, this.p.nextInt(128), this.p.nextInt(16) + 8));
      }

      if(this.p.nextBoolean()) {
         this.F.b(this.n, this.p, var3.a(this.p.nextInt(16) + 8, this.p.nextInt(128), this.p.nextInt(16) + 8));
      }

      if(this.p.nextBoolean()) {
         this.G.b(this.n, this.p, var3.a(this.p.nextInt(16) + 8, this.p.nextInt(128), this.p.nextInt(16) + 8));
      }

      for(var5 = 0; var5 < 16; ++var5) {
         this.C.b(this.n, this.p, var3.a(this.p.nextInt(16), this.p.nextInt(108) + 10, this.p.nextInt(16)));
      }

      for(var5 = 0; var5 < 16; ++var5) {
         this.D.b(this.n, this.p, var3.a(this.p.nextInt(16), this.p.nextInt(108) + 10, this.p.nextInt(16)));
      }

      class_alg.f = false;
   }

   public boolean a(Chunk var1, int var2, int var3) {
      return false;
   }

   public List a(EnumCreatureType var1, BlockPosition var2) {
      if(var1 == EnumCreatureType.MONSTER) {
         if(this.H.b(var2)) {
            return this.H.b();
         }

         if(this.H.a(this.n, var2) && this.n.getType(var2.b()).getBlock() == Blocks.by) {
            return this.H.b();
         }
      }

      BiomeBase var3 = this.n.b(var2);
      return var3.a(var1);
   }

   public BlockPosition a(World var1, String var2, BlockPosition var3) {
      return null;
   }

   public void b(Chunk var1, int var2, int var3) {
      this.H.a(this.n, var2, var3, (class_ate)null);
   }
}
