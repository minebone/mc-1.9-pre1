package net.minecraft.server;

import java.util.List;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_akr;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_aoo;
import net.minecraft.server.class_aqt;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_or;

public class BlockPistonExtension extends class_akr {
   public static final BlockStateEnum a = BlockStateEnum.a("type", BlockPistonExtension.EnumPistonType.class);
   public static final class_arm b = class_arm.a("short");
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.75D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
   protected static final AxisAlignedBB d = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.25D, 1.0D, 1.0D);
   protected static final AxisAlignedBB e = new AxisAlignedBB(0.0D, 0.0D, 0.75D, 1.0D, 1.0D, 1.0D);
   protected static final AxisAlignedBB f = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.25D);
   protected static final AxisAlignedBB g = new AxisAlignedBB(0.0D, 0.75D, 0.0D, 1.0D, 1.0D, 1.0D);
   protected static final AxisAlignedBB B = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.25D, 1.0D);
   protected static final AxisAlignedBB C = new AxisAlignedBB(0.375D, -0.25D, 0.375D, 0.625D, 0.75D, 0.625D);
   protected static final AxisAlignedBB D = new AxisAlignedBB(0.375D, 0.25D, 0.375D, 0.625D, 1.25D, 0.625D);
   protected static final AxisAlignedBB E = new AxisAlignedBB(0.375D, 0.375D, -0.25D, 0.625D, 0.625D, 0.75D);
   protected static final AxisAlignedBB F = new AxisAlignedBB(0.375D, 0.375D, 0.25D, 0.625D, 0.625D, 1.25D);
   protected static final AxisAlignedBB G = new AxisAlignedBB(-0.25D, 0.375D, 0.375D, 0.75D, 0.625D, 0.625D);
   protected static final AxisAlignedBB I = new AxisAlignedBB(0.25D, 0.375D, 0.375D, 1.25D, 0.625D, 0.625D);

   public BlockPistonExtension() {
      super(Material.H);
      this.w(this.A.b().set(H, EnumDirection.NORTH).set(a, BlockPistonExtension.EnumPistonType.DEFAULT).set(b, Boolean.valueOf(false)));
      this.a(class_aoo.d);
      this.c(0.5F);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      switch(BlockPistonExtension.SyntheticClass_1.a[((EnumDirection)var1.get(H)).ordinal()]) {
      case 1:
      default:
         return B;
      case 2:
         return g;
      case 3:
         return f;
      case 4:
         return e;
      case 5:
         return d;
      case 6:
         return c;
      }
   }

   public void a(IBlockData var1, World var2, BlockPosition var3, AxisAlignedBB var4, List var5, Entity var6) {
      a((BlockPosition)var3, (AxisAlignedBB)var4, (List)var5, (AxisAlignedBB)var1.c(var2, var3));
      a((BlockPosition)var3, (AxisAlignedBB)var4, (List)var5, (AxisAlignedBB)this.i(var1));
   }

   private AxisAlignedBB i(IBlockData var1) {
      switch(BlockPistonExtension.SyntheticClass_1.a[((EnumDirection)var1.get(H)).ordinal()]) {
      case 1:
      default:
         return D;
      case 2:
         return C;
      case 3:
         return F;
      case 4:
         return E;
      case 5:
         return I;
      case 6:
         return G;
      }
   }

   public boolean k(IBlockData var1) {
      return var1.get(H) == EnumDirection.UP;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4) {
      if(var4.abilities.d) {
         BlockPosition var5 = var2.a(((EnumDirection)var3.get(H)).d());
         Block var6 = var1.getType(var5).getBlock();
         if(var6 == Blocks.J || var6 == Blocks.F) {
            var1.g(var5);
         }
      }

      super.a(var1, var2, var3, var4);
   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      super.b(var1, var2, var3);
      EnumDirection var4 = ((EnumDirection)var3.get(H)).d();
      var2 = var2.a(var4);
      IBlockData var5 = var1.getType(var2);
      if((var5.getBlock() == Blocks.J || var5.getBlock() == Blocks.F) && ((Boolean)var5.get(class_aqt.a)).booleanValue()) {
         var5.getBlock().b(var1, var2, var5, 0);
         var1.g(var2);
      }

   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean a(World var1, BlockPosition var2) {
      return false;
   }

   public boolean b(World var1, BlockPosition var2, EnumDirection var3) {
      return false;
   }

   public int a(Random var1) {
      return 0;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      EnumDirection var5 = (EnumDirection)var3.get(H);
      BlockPosition var6 = var2.a(var5.d());
      IBlockData var7 = var1.getType(var6);
      if(var7.getBlock() != Blocks.J && var7.getBlock() != Blocks.F) {
         var1.g(var2);
      } else {
         var7.getBlock().a(var1, var6, var7, var4);
      }

   }

   public static EnumDirection e(int var0) {
      int var1 = var0 & 7;
      return var1 > 5?null:EnumDirection.a(var1);
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(var3.get(a) == BlockPistonExtension.EnumPistonType.STICKY?Blocks.F:Blocks.J);
   }

   public IBlockData a(int var1) {
      return this.u().set(H, e(var1)).set(a, (var1 & 8) > 0?BlockPistonExtension.EnumPistonType.STICKY:BlockPistonExtension.EnumPistonType.DEFAULT);
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((EnumDirection)var1.get(H)).a();
      if(var1.get(a) == BlockPistonExtension.EnumPistonType.STICKY) {
         var3 |= 8;
      }

      return var3;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(H, var2.a((EnumDirection)var1.get(H)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(H)));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{H, a, b});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumDirection.values().length];

      static {
         try {
            a[EnumDirection.DOWN.ordinal()] = 1;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[EnumDirection.UP.ordinal()] = 2;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[EnumDirection.NORTH.ordinal()] = 3;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.SOUTH.ordinal()] = 4;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 5;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.EAST.ordinal()] = 6;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum EnumPistonType implements class_or {
      DEFAULT("normal"),
      STICKY("sticky");

      private final String c;

      private EnumPistonType(String var3) {
         this.c = var3;
      }

      public String toString() {
         return this.c;
      }

      public String m() {
         return this.c;
      }
   }
}
