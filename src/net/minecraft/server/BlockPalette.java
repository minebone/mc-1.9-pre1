package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.NibbleArray;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_asb;
import net.minecraft.server.class_asc;
import net.minecraft.server.class_asf;
import net.minecraft.server.class_ash;
import net.minecraft.server.class_asi;
import net.minecraft.server.class_nx;
import net.minecraft.server.MathHelper;

public class BlockPalette implements class_asi {
   private static final class_ash d = new class_asb();
   protected static final IBlockData a = Blocks.AIR.u();
   protected class_nx b;
   protected class_ash c;
   private int e = 0;

   public BlockPalette() {
      this.b(4);
   }

   private static int b(int var0, int var1, int var2) {
      return var1 << 8 | var2 << 4 | var0;
   }

   private void b(int var1) {
      if(var1 != this.e) {
         this.e = var1;
         if(this.e <= 4) {
            this.e = 4;
            this.c = new class_asf(this.e, this);
         } else if(this.e <= 8) {
            this.c = new class_asc(this.e, this);
         } else {
            this.c = d;
            this.e = MathHelper.d(Block.i.a());
         }

         this.c.a(a);
         this.b = new class_nx(this.e, 4096);
      }
   }

   public int a(int var1, IBlockData var2) {
      class_nx var3 = this.b;
      class_ash var4 = this.c;
      this.b(var1);

      for(int var5 = 0; var5 < var3.b(); ++var5) {
         IBlockData var6 = var4.a(var3.a(var5));
         if(var6 != null) {
            this.b(var5, var6);
         }
      }

      return this.c.a(var2);
   }

   public void a(int var1, int var2, int var3, IBlockData var4) {
      this.b(b(var1, var2, var3), var4);
   }

   protected void b(int var1, IBlockData var2) {
      int var3 = this.c.a(var2);
      this.b.a(var1, var3);
   }

   public IBlockData a(int var1, int var2, int var3) {
      return this.a(b(var1, var2, var3));
   }

   protected IBlockData a(int var1) {
      IBlockData var2 = this.c.a(this.b.a(var1));
      return var2 == null?a:var2;
   }

   public void b(PacketDataSerializer var1) {
      var1.writeByte(this.e);
      this.c.b(var1);
      var1.a(this.b.a());
   }

   public NibbleArray a(byte[] var1, NibbleArray var2) {
      NibbleArray var3 = null;

      for(int var4 = 0; var4 < 4096; ++var4) {
         int var5 = Block.i.a(this.a(var4));
         int var6 = var4 & 15;
         int var7 = var4 >> 8 & 15;
         int var8 = var4 >> 4 & 15;
         if((var5 >> 12 & 15) != 0) {
            if(var3 == null) {
               var3 = new NibbleArray();
            }

            var3.a(var6, var7, var8, var5 >> 12 & 15);
         }

         var1[var4] = (byte)(var5 >> 4 & 255);
         var2.a(var6, var7, var8, var5 & 15);
      }

      return var3;
   }

   public void a(byte[] var1, NibbleArray var2, NibbleArray var3) {
      for(int var4 = 0; var4 < 4096; ++var4) {
         int var5 = var4 & 15;
         int var6 = var4 >> 8 & 15;
         int var7 = var4 >> 4 & 15;
         int var8 = var3 == null?0:var3.a(var5, var6, var7);
         int var9 = var8 << 12 | (var1[var4] & 255) << 4 | var2.a(var5, var6, var7);
         this.b(var4, (IBlockData)Block.i.a(var9));
      }

   }

   public int a() {
      int var1 = this.b.b();
      return 1 + this.c.a() + PacketDataSerializer.a(var1) + var1 * 8;
   }
}
