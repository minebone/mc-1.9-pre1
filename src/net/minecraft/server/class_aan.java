package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityMinecartAbstract;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.class_ahj;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;

public class class_aan extends EntityMinecartAbstract {
   private final class_ahj a = new class_ahj() {
      public void a(int var1) {
         class_aan.this.world.a((Entity)class_aan.this, (byte)((byte)var1));
      }

      public World a() {
         return class_aan.this.world;
      }

      public BlockPosition b() {
         return new BlockPosition(class_aan.this);
      }
   };

   public class_aan(World var1) {
      super(var1);
   }

   public class_aan(World var1, double var2, double var4, double var6) {
      super(var1, var2, var4, var6);
   }

   public EntityMinecartAbstract.EnumMinecartType v() {
      return EntityMinecartAbstract.EnumMinecartType.SPAWNER;
   }

   public IBlockData x() {
      return Blocks.ac.u();
   }

   protected void a(NBTTagCompound var1) {
      super.a(var1);
      this.a.a(var1);
   }

   protected void b(NBTTagCompound var1) {
      super.b(var1);
      this.a.b(var1);
   }

   public void m() {
      super.m();
      this.a.c();
   }
}
