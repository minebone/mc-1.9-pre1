package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.class_azw;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_bab;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_bar;
import net.minecraft.server.MathHelper;
import org.apache.commons.lang3.ArrayUtils;

public class class_azv {
   private final class_azw[] a;
   private final class_baq[] b;
   private final class_bab c;
   private final class_bab d;

   public class_azv(class_azw[] var1, class_baq[] var2, class_bab var3, class_bab var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   protected void a(Collection var1, Random var2, class_azy var3) {
      ArrayList var4 = Lists.newArrayList();
      int var5 = 0;
      class_azw[] var6 = this.a;
      int var7 = var6.length;

      for(int var8 = 0; var8 < var7; ++var8) {
         class_azw var9 = var6[var8];
         if(class_bar.a(var9.e, var2, var3)) {
            int var10 = var9.a(var3.f());
            if(var10 > 0) {
               var4.add(var9);
               var5 += var10;
            }
         }
      }

      if(var5 != 0 && !var4.isEmpty()) {
         int var11 = var2.nextInt(var5);
         Iterator var12 = var4.iterator();

         class_azw var13;
         do {
            if(!var12.hasNext()) {
               return;
            }

            var13 = (class_azw)var12.next();
            var11 -= var13.a(var3.f());
         } while(var11 >= 0);

         var13.a(var1, var2, var3);
      }
   }

   public void b(Collection var1, Random var2, class_azy var3) {
      if(class_bar.a(this.b, var2, var3)) {
         int var4 = this.c.a(var2) + MathHelper.d(this.d.b(var2) * var3.f());

         for(int var5 = 0; var5 < var4; ++var5) {
            this.a(var1, var2, var3);
         }

      }
   }

   public static class class_a_in_class_azv implements JsonDeserializer, JsonSerializer {
      public class_azv a(JsonElement var1, Type var2, JsonDeserializationContext var3) throws JsonParseException {
         JsonObject var4 = ChatDeserializer.m(var1, "loot pool");
         class_azw[] var5 = (class_azw[])ChatDeserializer.a(var4, "entries", var3, class_azw[].class);
         class_baq[] var6 = (class_baq[])ChatDeserializer.a(var4, "conditions", new class_baq[0], var3, class_baq[].class);
         class_bab var7 = (class_bab)ChatDeserializer.a(var4, "rolls", var3, class_bab.class);
         class_bab var8 = (class_bab)ChatDeserializer.a(var4, "bonus_rolls", new class_bab(0.0F, 0.0F), var3, class_bab.class);
         return new class_azv(var5, var6, var7, var8);
      }

      public JsonElement a(class_azv var1, Type var2, JsonSerializationContext var3) {
         JsonObject var4 = new JsonObject();
         var4.add("entries", var3.serialize(var1.a));
         var4.add("rolls", var3.serialize(var1.c));
         if(var1.d.a() != 0.0F && var1.d.b() != 0.0F) {
            var4.add("bonus_rolls", var3.serialize(var1.d));
         }

         if(!ArrayUtils.isEmpty((Object[])var1.b)) {
            var4.add("conditions", var3.serialize(var1.b));
         }

         return var4;
      }

      // $FF: synthetic method
      public JsonElement serialize(Object var1, Type var2, JsonSerializationContext var3) {
         return this.a((class_azv)var1, var2, var3);
      }

      // $FF: synthetic method
      public Object deserialize(JsonElement var1, Type var2, JsonDeserializationContext var3) throws JsonParseException {
         return this.a(var1, var2, var3);
      }
   }
}
