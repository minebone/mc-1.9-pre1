package net.minecraft.server;

import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.WorldData;
import net.minecraft.server.class_bz;
import net.minecraft.server.ICommandListener;

public class CommandToggleDownfall extends CommandAbstract {
   public String c() {
      return "toggledownfall";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.downfall.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      this.a(var1);
      a(var2, this, "commands.downfall.success", new Object[0]);
   }

   protected void a(MinecraftServer var1) {
      WorldData var2 = var1.d[0].T();
      var2.b(!var2.o());
   }
}
