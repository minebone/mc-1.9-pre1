package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSyntaxException;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Random;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.class_azt;
import net.minecraft.server.class_azu;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_azz;
import net.minecraft.server.class_baq;
import net.minecraft.server.MathHelper;

public abstract class class_azw {
   protected final int c;
   protected final int d;
   protected final class_baq[] e;

   protected class_azw(int var1, int var2, class_baq[] var3) {
      this.c = var1;
      this.d = var2;
      this.e = var3;
   }

   public int a(float var1) {
      return MathHelper.d((float)this.c + (float)this.d * var1);
   }

   public abstract void a(Collection var1, Random var2, class_azy var3);

   protected abstract void a(JsonObject var1, JsonSerializationContext var2);

   public static class class_a_in_class_azw implements JsonDeserializer, JsonSerializer {
      public class_azw a(JsonElement var1, Type var2, JsonDeserializationContext var3) throws JsonParseException {
         JsonObject var4 = ChatDeserializer.m(var1, "loot item");
         String var5 = ChatDeserializer.h(var4, "type");
         int var6 = ChatDeserializer.a(var4, "weight", 1);
         int var7 = ChatDeserializer.a(var4, "quality", 0);
         class_baq[] var8;
         if(var4.has("conditions")) {
            var8 = (class_baq[])ChatDeserializer.a(var4, "conditions", var3, class_baq[].class);
         } else {
            var8 = new class_baq[0];
         }

         if(var5.equals("item")) {
            return class_azu.a(var4, var3, var6, var7, var8);
         } else if(var5.equals("loot_table")) {
            return class_azz.a(var4, var3, var6, var7, var8);
         } else if(var5.equals("empty")) {
            return class_azt.a(var4, var3, var6, var7, var8);
         } else {
            throw new JsonSyntaxException("Unknown loot entry type \'" + var5 + "\'");
         }
      }

      public JsonElement a(class_azw var1, Type var2, JsonSerializationContext var3) {
         JsonObject var4 = new JsonObject();
         var4.addProperty("weight", (Number)Integer.valueOf(var1.c));
         var4.addProperty("quality", (Number)Integer.valueOf(var1.d));
         if(var1.e.length > 0) {
            var4.add("conditions", var3.serialize(var1.e));
         }

         if(var1 instanceof class_azu) {
            var4.addProperty("type", "item");
         } else if(var1 instanceof class_azz) {
            var4.addProperty("type", "item");
         } else {
            if(!(var1 instanceof class_azt)) {
               throw new IllegalArgumentException("Don\'t know how to serialize " + var1);
            }

            var4.addProperty("type", "empty");
         }

         var1.a(var4, var3);
         return var4;
      }

      // $FF: synthetic method
      public JsonElement serialize(Object var1, Type var2, JsonSerializationContext var3) {
         return this.a((class_azw)var1, var2, var3);
      }

      // $FF: synthetic method
      public Object deserialize(JsonElement var1, Type var2, JsonDeserializationContext var3) throws JsonParseException {
         return this.a(var1, var2, var3);
      }
   }
}
