package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.UtilColor;
import net.minecraft.server.World;
import net.minecraft.server.class_aic;
import net.minecraft.server.class_asr;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ov;
import net.minecraft.server.class_qk;
import net.minecraft.server.class_sc;

public abstract class class_ahj {
   private int a = 20;
   private final List b = Lists.newArrayList();
   private class_aic c = new class_aic();
   private double d;
   private double e;
   private int f = 200;
   private int g = 800;
   private int h = 4;
   private Entity i;
   private int j = 6;
   private int k = 16;
   private int l = 4;

   private String g() {
      return this.c.b().l("id");
   }

   public void a(String var1) {
      this.c.b().a("id", var1);
   }

   private boolean h() {
      BlockPosition var1 = this.b();
      return this.a().a((double)var1.p() + 0.5D, (double)var1.q() + 0.5D, (double)var1.r() + 0.5D, (double)this.k);
   }

   public void c() {
      if(!this.h()) {
         this.e = this.d;
      } else {
         BlockPosition var1 = this.b();
         double var6;
         if(this.a().E) {
            double var15 = (double)((float)var1.p() + this.a().r.nextFloat());
            double var16 = (double)((float)var1.q() + this.a().r.nextFloat());
            var6 = (double)((float)var1.r() + this.a().r.nextFloat());
            this.a().a(EnumParticle.SMOKE_NORMAL, var15, var16, var6, 0.0D, 0.0D, 0.0D, new int[0]);
            this.a().a(EnumParticle.FLAME, var15, var16, var6, 0.0D, 0.0D, 0.0D, new int[0]);
            if(this.a > 0) {
               --this.a;
            }

            this.e = this.d;
            this.d = (this.d + (double)(1000.0F / ((float)this.a + 200.0F))) % 360.0D;
         } else {
            if(this.a == -1) {
               this.i();
            }

            if(this.a > 0) {
               --this.a;
               return;
            }

            boolean var2 = false;
            int var3 = 0;

            while(true) {
               if(var3 >= this.h) {
                  if(var2) {
                     this.i();
                  }
                  break;
               }

               NBTTagCompound var4 = this.c.b();
               NBTTagList var5 = var4.c("Pos", 6);
               var6 = var4.d() >= 1?var5.e(0):(double)var1.p() + (this.a().r.nextDouble() - this.a().r.nextDouble()) * (double)this.l + 0.5D;
               double var8 = var4.d() >= 2?var5.e(1):(double)(var1.q() + this.a().r.nextInt(3) - 1);
               double var10 = var4.d() >= 3?var5.e(2):(double)var1.r() + (this.a().r.nextDouble() - this.a().r.nextDouble()) * (double)this.l + 0.5D;
               Entity var12 = class_asr.a(var4, this.a(), var6, var8, var10, false);
               if(var12 == null) {
                  return;
               }

               int var13 = this.a().a(var12.getClass(), (new AxisAlignedBB((double)var1.p(), (double)var1.q(), (double)var1.r(), (double)(var1.p() + 1), (double)(var1.q() + 1), (double)(var1.r() + 1))).g((double)this.l)).size();
               if(var13 >= this.j) {
                  this.i();
                  return;
               }

               EntityInsentient var14 = var12 instanceof EntityInsentient?(EntityInsentient)var12:null;
               var12.b(var12.locX, var12.locY, var12.locZ, this.a().r.nextFloat() * 360.0F, 0.0F);
               if(var14 == null || var14.cF() && var14.cG()) {
                  if(this.c.b().d() == 1 && this.c.b().b("id", 8) && var12 instanceof EntityInsentient) {
                     ((EntityInsentient)var12).a((class_qk)this.a().D(new BlockPosition(var12)), (class_sc)null);
                  }

                  class_asr.a(var12, this.a());
                  this.a().b(2004, var1, 0);
                  if(var14 != null) {
                     var14.E();
                  }

                  var2 = true;
               }

               ++var3;
            }
         }

      }
   }

   private void i() {
      if(this.g <= this.f) {
         this.a = this.f;
      } else {
         int var10003 = this.g - this.f;
         this.a = this.f + this.a().r.nextInt(var10003);
      }

      if(!this.b.isEmpty()) {
         this.a((class_aic)class_ov.a(this.a().r, this.b));
      }

      this.a(1);
   }

   public void a(NBTTagCompound var1) {
      this.a = var1.g("Delay");
      this.b.clear();
      if(var1.b("SpawnPotentials", 9)) {
         NBTTagList var2 = var1.c("SpawnPotentials", 10);

         for(int var3 = 0; var3 < var2.c(); ++var3) {
            this.b.add(new class_aic(var2.b(var3)));
         }
      }

      NBTTagCompound var4 = var1.o("SpawnData");
      if(!var4.b("id", 8)) {
         var4.a("id", "Pig");
      }

      this.a(new class_aic(1, var4));
      if(var1.b("MinSpawnDelay", 99)) {
         this.f = var1.g("MinSpawnDelay");
         this.g = var1.g("MaxSpawnDelay");
         this.h = var1.g("SpawnCount");
      }

      if(var1.b("MaxNearbyEntities", 99)) {
         this.j = var1.g("MaxNearbyEntities");
         this.k = var1.g("RequiredPlayerRange");
      }

      if(var1.b("SpawnRange", 99)) {
         this.l = var1.g("SpawnRange");
      }

      if(this.a() != null) {
         this.i = null;
      }

   }

   public void b(NBTTagCompound var1) {
      String var2 = this.g();
      if(!UtilColor.b(var2)) {
         var1.a("Delay", (short)this.a);
         var1.a("MinSpawnDelay", (short)this.f);
         var1.a("MaxSpawnDelay", (short)this.g);
         var1.a("SpawnCount", (short)this.h);
         var1.a("MaxNearbyEntities", (short)this.j);
         var1.a("RequiredPlayerRange", (short)this.k);
         var1.a("SpawnRange", (short)this.l);
         var1.a("SpawnData", this.c.b().b());
         NBTTagList var3 = new NBTTagList();
         if(!this.b.isEmpty()) {
            Iterator var4 = this.b.iterator();

            while(var4.hasNext()) {
               class_aic var5 = (class_aic)var4.next();
               var3.a((NBTTag)var5.a());
            }
         } else {
            var3.a((NBTTag)this.c.a());
         }

         var1.a((String)"SpawnPotentials", (NBTTag)var3);
      }
   }

   public boolean b(int var1) {
      if(var1 == 1 && this.a().E) {
         this.a = this.f;
         return true;
      } else {
         return false;
      }
   }

   public void a(class_aic var1) {
      this.c = var1;
   }

   public abstract void a(int var1);

   public abstract World a();

   public abstract BlockPosition b();
}
