package net.minecraft.server;

import com.google.common.collect.ImmutableMap;
import java.util.Collection;
import net.minecraft.server.Block;
import net.minecraft.server.class_ara;
import net.minecraft.server.IBlockState;

public interface IBlockData extends class_ara {
   Collection r();

   Comparable get(IBlockState var1);

   IBlockData set(IBlockState var1, Comparable var2);

   IBlockData a(IBlockState var1);

   ImmutableMap s();

   Block getBlock();
}
