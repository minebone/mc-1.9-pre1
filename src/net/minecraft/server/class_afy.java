package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.CraftingManager;
import net.minecraft.server.EnumColor;
import net.minecraft.server.InventoryCrafting;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntityBanner;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aft;

public class class_afy {
   public void a(CraftingManager var1) {
      var1.a(new ItemStack(Items.cQ), new Object[]{"WoW", "WWW", " W ", Character.valueOf('W'), Blocks.f, Character.valueOf('o'), Items.l});
      var1.a(new class_afy.class_a_in_class_afy());
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
   }

   static class class_a_in_class_afy implements class_aft {
      private class_a_in_class_afy() {
      }

      public boolean a(InventoryCrafting var1, World var2) {
         ItemStack var3 = null;
         ItemStack var4 = null;

         for(int var5 = 0; var5 < var1.u_(); ++var5) {
            ItemStack var6 = var1.a(var5);
            if(var6 != null) {
               if(var6.b() == Items.cO) {
                  if(var4 != null) {
                     return false;
                  }

                  var4 = var6;
               } else {
                  if(var6.b() != Items.cQ) {
                     return false;
                  }

                  if(var3 != null) {
                     return false;
                  }

                  if(var6.a("BlockEntityTag", false) != null) {
                     return false;
                  }

                  var3 = var6;
               }
            }
         }

         if(var3 != null && var4 != null) {
            return true;
         } else {
            return false;
         }
      }

      public ItemStack a(InventoryCrafting var1) {
         ItemStack var2 = null;

         for(int var3 = 0; var3 < var1.u_(); ++var3) {
            ItemStack var4 = var1.a(var3);
            if(var4 != null && var4.b() == Items.cO) {
               var2 = var4;
            }
         }

         ItemStack var6 = new ItemStack(Items.cQ, 1, 0);
         EnumColor var5;
         NBTTagCompound var7;
         if(var2.n()) {
            var7 = (NBTTagCompound)var2.o().b();
            var5 = EnumColor.a(TileEntityBanner.b(var2));
         } else {
            var7 = new NBTTagCompound();
            var5 = EnumColor.a(var2.h());
         }

         var6.d(var7);
         TileEntityBanner.a(var6, var5);
         return var6;
      }

      public int a() {
         return 2;
      }

      public ItemStack b() {
         return null;
      }

      public ItemStack[] b(InventoryCrafting var1) {
         ItemStack[] var2 = new ItemStack[var1.u_()];

         for(int var3 = 0; var3 < var2.length; ++var3) {
            ItemStack var4 = var1.a(var3);
            if(var4 != null && var4.b().r()) {
               var2[var3] = new ItemStack(var4.b().q());
            }
         }

         return var2;
      }

      // $FF: synthetic method
      class_a_in_class_afy(class_afy.SyntheticClass_1 var1) {
         this();
      }
   }
}
