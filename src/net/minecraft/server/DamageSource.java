package net.minecraft.server;

import net.minecraft.server.ChatMessage;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityFireball;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.Vec3D;
import net.minecraft.server.class_aho;
import net.minecraft.server.class_di;
import net.minecraft.server.class_rc;
import net.minecraft.server.class_rd;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_zl;

public class DamageSource {
   public static DamageSource a = (new DamageSource("inFire")).n();
   public static DamageSource b = new DamageSource("lightningBolt");
   public static DamageSource c = (new DamageSource("onFire")).k().n();
   public static DamageSource d = (new DamageSource("lava")).n();
   public static DamageSource e = (new DamageSource("inWall")).k();
   public static DamageSource f = (new DamageSource("drown")).k();
   public static DamageSource g = (new DamageSource("starve")).k().m();
   public static DamageSource h = new DamageSource("cactus");
   public static DamageSource i = (new DamageSource("fall")).k();
   public static DamageSource j = (new DamageSource("flyIntoWall")).k();
   public static DamageSource k = (new DamageSource("outOfWorld")).k().l();
   public static DamageSource l = (new DamageSource("generic")).k();
   public static DamageSource m = (new DamageSource("magic")).k().t();
   public static DamageSource n = (new DamageSource("wither")).k();
   public static DamageSource o = new DamageSource("anvil");
   public static DamageSource p = new DamageSource("fallingBlock");
   public static DamageSource q = (new DamageSource("dragonBreath")).k();
   private boolean s;
   private boolean t;
   private boolean u;
   private float v = 0.3F;
   private boolean w;
   private boolean x;
   private boolean y;
   private boolean z;
   private boolean A;
   public String r;

   public static DamageSource a(class_rz var0) {
      return new class_rc("mob", var0);
   }

   public static DamageSource a(Entity var0, class_rz var1) {
      return new class_rd("mob", var0, var1);
   }

   public static DamageSource a(EntityHuman var0) {
      return new class_rc("player", var0);
   }

   public static DamageSource a(class_zl var0, Entity var1) {
      return (new class_rd("arrow", var0, var1)).b();
   }

   public static DamageSource a(EntityFireball var0, Entity var1) {
      return var1 == null?(new class_rd("onFire", var0, var0)).n().b():(new class_rd("fireball", var0, var1)).n().b();
   }

   public static DamageSource a(Entity var0, Entity var1) {
      return (new class_rd("thrown", var0, var1)).b();
   }

   public static DamageSource b(Entity var0, Entity var1) {
      return (new class_rd("indirectMagic", var0, var1)).k().t();
   }

   public static DamageSource a(Entity var0) {
      return (new class_rc("thorns", var0)).w().t();
   }

   public static DamageSource a(class_aho var0) {
      return var0 != null && var0.c() != null?(new class_rc("explosion.player", var0.c())).q().d():(new DamageSource("explosion")).q().d();
   }

   public static DamageSource b(class_rz var0) {
      return var0 != null?(new class_rc("explosion.player", var0)).q().d():(new DamageSource("explosion")).q().d();
   }

   public boolean a() {
      return this.x;
   }

   public DamageSource b() {
      this.x = true;
      return this;
   }

   public boolean c() {
      return this.A;
   }

   public DamageSource d() {
      this.A = true;
      return this;
   }

   public boolean e() {
      return this.s;
   }

   public float f() {
      return this.v;
   }

   public boolean g() {
      return this.t;
   }

   public boolean h() {
      return this.u;
   }

   protected DamageSource(String var1) {
      this.r = var1;
   }

   public Entity i() {
      return this.j();
   }

   public Entity j() {
      return null;
   }

   protected DamageSource k() {
      this.s = true;
      this.v = 0.0F;
      return this;
   }

   protected DamageSource l() {
      this.t = true;
      return this;
   }

   protected DamageSource m() {
      this.u = true;
      this.v = 0.0F;
      return this;
   }

   protected DamageSource n() {
      this.w = true;
      return this;
   }

   public IChatBaseComponent c(class_rz var1) {
      class_rz var2 = var1.bU();
      String var3 = "death.attack." + this.r;
      String var4 = var3 + ".player";
      return var2 != null && class_di.c(var4)?new ChatMessage(var4, new Object[]{var1.i_(), var2.i_()}):new ChatMessage(var3, new Object[]{var1.i_()});
   }

   public boolean o() {
      return this.w;
   }

   public String p() {
      return this.r;
   }

   public DamageSource q() {
      this.y = true;
      return this;
   }

   public boolean r() {
      return this.y;
   }

   public boolean s() {
      return this.z;
   }

   public DamageSource t() {
      this.z = true;
      return this;
   }

   public boolean u() {
      Entity var1 = this.j();
      return var1 instanceof EntityHuman && ((EntityHuman)var1).abilities.d;
   }

   public Vec3D v() {
      return null;
   }
}
