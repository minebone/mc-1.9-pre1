package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.util.Random;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.MathHelper;

public class class_bab {
   private final float a;
   private final float b;

   public class_bab(float var1, float var2) {
      this.a = var1;
      this.b = var2;
   }

   public class_bab(float var1) {
      this.a = var1;
      this.b = var1;
   }

   public float a() {
      return this.a;
   }

   public float b() {
      return this.b;
   }

   public int a(Random var1) {
      return MathHelper.a(var1, MathHelper.d(this.a), MathHelper.d(this.b));
   }

   public float b(Random var1) {
      return MathHelper.a(var1, this.a, this.b);
   }

   public boolean a(int var1) {
      return (float)var1 <= this.b && (float)var1 >= this.a;
   }

   public static class class_a_in_class_bab implements JsonDeserializer, JsonSerializer {
      public class_bab a(JsonElement var1, Type var2, JsonDeserializationContext var3) throws JsonParseException {
         if(ChatDeserializer.b(var1)) {
            return new class_bab(ChatDeserializer.e(var1, "value"));
         } else {
            JsonObject var4 = ChatDeserializer.m(var1, "value");
            float var5 = ChatDeserializer.l(var4, "min");
            float var6 = ChatDeserializer.l(var4, "max");
            return new class_bab(var5, var6);
         }
      }

      public JsonElement a(class_bab var1, Type var2, JsonSerializationContext var3) {
         if(var1.a == var1.b) {
            return new JsonPrimitive(Float.valueOf(var1.a));
         } else {
            JsonObject var4 = new JsonObject();
            var4.addProperty("min", (Number)Float.valueOf(var1.a));
            var4.addProperty("max", (Number)Float.valueOf(var1.b));
            return var4;
         }
      }

      // $FF: synthetic method
      public JsonElement serialize(Object var1, Type var2, JsonSerializationContext var3) {
         return this.a((class_bab)var1, var2, var3);
      }

      // $FF: synthetic method
      public Object deserialize(JsonElement var1, Type var2, JsonDeserializationContext var3) throws JsonParseException {
         return this.a(var1, var2, var3);
      }
   }
}
