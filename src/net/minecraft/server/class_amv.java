package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;

public class class_amv extends Block {
   public class_amv() {
      super(Material.e);
      this.a(CreativeModeTab.b);
   }

   public MaterialMapColor r(IBlockData var1) {
      return MaterialMapColor.K;
   }
}
