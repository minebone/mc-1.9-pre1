package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aoa;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_aji extends Block {
   protected class_aji() {
      super(Material.a);
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.INVISIBLE;
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return null;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean a(IBlockData var1, boolean var2) {
      return false;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, float var4, int var5) {
   }

   public boolean a(class_ahw var1, BlockPosition var2) {
      return true;
   }

   public boolean c(IBlockData var1) {
      return false;
   }
}
