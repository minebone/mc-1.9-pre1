package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.WorldData;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;

public class CommandWeather extends CommandAbstract {
   public String c() {
      return "weather";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.weather.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length >= 1 && var3.length <= 2) {
         int var4 = (300 + (new Random()).nextInt(600)) * 20;
         if(var3.length >= 2) {
            var4 = a(var3[1], 1, 1000000) * 20;
         }

         WorldServer var5 = var1.d[0];
         WorldData var6 = var5.T();
         if("clear".equalsIgnoreCase(var3[0])) {
            var6.i(var4);
            var6.g(0);
            var6.f(0);
            var6.b(false);
            var6.a(false);
            a(var2, this, "commands.weather.clear", new Object[0]);
         } else if("rain".equalsIgnoreCase(var3[0])) {
            var6.i(0);
            var6.g(var4);
            var6.f(var4);
            var6.b(true);
            var6.a(false);
            a(var2, this, "commands.weather.rain", new Object[0]);
         } else {
            if(!"thunder".equalsIgnoreCase(var3[0])) {
               throw new class_cf("commands.weather.usage", new Object[0]);
            }

            var6.i(0);
            var6.g(var4);
            var6.f(var4);
            var6.b(true);
            var6.a(true);
            a(var2, this, "commands.weather.thunder", new Object[0]);
         }

      } else {
         throw new class_cf("commands.weather.usage", new Object[0]);
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, new String[]{"clear", "rain", "thunder"}):Collections.emptyList();
   }
}
