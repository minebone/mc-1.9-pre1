package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockSoil;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aju;
import net.minecraft.server.class_ajx;
import net.minecraft.server.class_aoo;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;

public class class_akm extends class_ajx implements class_aju {
   public static final BlockStateInteger c = BlockStateInteger.a("age", 0, 7);
   private static final AxisAlignedBB[] a = new AxisAlignedBB[]{new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.125D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.25D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.375D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.5D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.625D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.75D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.875D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D)};

   protected class_akm() {
      this.w(this.A.b().set(this.e(), Integer.valueOf(0)));
      this.a(true);
      this.a((CreativeModeTab)null);
      this.c(0.0F);
      this.a(class_aoo.c);
      this.q();
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return a[((Integer)var1.get(this.e())).intValue()];
   }

   protected boolean i(IBlockData var1) {
      return var1.getBlock() == Blocks.ak;
   }

   protected BlockStateInteger e() {
      return c;
   }

   public int g() {
      return 7;
   }

   protected int x(IBlockData var1) {
      return ((Integer)var1.get(this.e())).intValue();
   }

   public IBlockData e(int var1) {
      return this.u().set(this.e(), Integer.valueOf(var1));
   }

   public boolean y(IBlockData var1) {
      return ((Integer)var1.get(this.e())).intValue() >= this.g();
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      super.b(var1, var2, var3, var4);
      if(var1.k(var2.a()) >= 9) {
         int var5 = this.x(var3);
         if(var5 < this.g()) {
            float var6 = a((Block)this, (World)var1, (BlockPosition)var2);
            if(var4.nextInt((int)(25.0F / var6) + 1) == 0) {
               var1.a((BlockPosition)var2, (IBlockData)this.e(var5 + 1), 2);
            }
         }
      }

   }

   public void g(World var1, BlockPosition var2, IBlockData var3) {
      int var4 = this.x(var3) + this.b(var1);
      int var5 = this.g();
      if(var4 > var5) {
         var4 = var5;
      }

      var1.a((BlockPosition)var2, (IBlockData)this.e(var4), 2);
   }

   protected int b(World var1) {
      return MathHelper.a((Random)var1.r, 2, 5);
   }

   protected static float a(Block var0, World var1, BlockPosition var2) {
      float var3 = 1.0F;
      BlockPosition var4 = var2.b();

      for(int var5 = -1; var5 <= 1; ++var5) {
         for(int var6 = -1; var6 <= 1; ++var6) {
            float var7 = 0.0F;
            IBlockData var8 = var1.getType(var4.a(var5, 0, var6));
            if(var8.getBlock() == Blocks.ak) {
               var7 = 1.0F;
               if(((Integer)var8.get(BlockSoil.a)).intValue() > 0) {
                  var7 = 3.0F;
               }
            }

            if(var5 != 0 || var6 != 0) {
               var7 /= 4.0F;
            }

            var3 += var7;
         }
      }

      BlockPosition var12 = var2.c();
      BlockPosition var13 = var2.d();
      BlockPosition var15 = var2.e();
      BlockPosition var14 = var2.f();
      boolean var9 = var0 == var1.getType(var15).getBlock() || var0 == var1.getType(var14).getBlock();
      boolean var10 = var0 == var1.getType(var12).getBlock() || var0 == var1.getType(var13).getBlock();
      if(var9 && var10) {
         var3 /= 2.0F;
      } else {
         boolean var11 = var0 == var1.getType(var15.c()).getBlock() || var0 == var1.getType(var14.c()).getBlock() || var0 == var1.getType(var14.d()).getBlock() || var0 == var1.getType(var15.d()).getBlock();
         if(var11) {
            var3 /= 2.0F;
         }
      }

      return var3;
   }

   public boolean f(World var1, BlockPosition var2, IBlockData var3) {
      return (var1.j(var2) >= 8 || var1.h(var2)) && this.i(var1.getType(var2.b()));
   }

   protected Item h() {
      return Items.P;
   }

   protected Item i() {
      return Items.Q;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, float var4, int var5) {
      super.a(var1, var2, var3, var4, 0);
      if(!var1.E) {
         int var6 = this.x(var3);
         if(var6 >= this.g()) {
            int var7 = 3 + var5;

            for(int var8 = 0; var8 < var7; ++var8) {
               if(var1.r.nextInt(2 * this.g()) <= var6) {
                  a((World)var1, (BlockPosition)var2, (ItemStack)(new ItemStack(this.h())));
               }
            }
         }

      }
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return this.y(var1)?this.i():this.h();
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(this.h());
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, boolean var4) {
      return !this.y(var3);
   }

   public boolean a(World var1, Random var2, BlockPosition var3, IBlockData var4) {
      return true;
   }

   public void b(World var1, Random var2, BlockPosition var3, IBlockData var4) {
      this.g(var1, var3, var4);
   }

   public IBlockData a(int var1) {
      return this.e(var1);
   }

   public int e(IBlockData var1) {
      return this.x(var1);
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{c});
   }
}
