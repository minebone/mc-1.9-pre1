package net.minecraft.server;

import net.minecraft.server.EnumParticle;
import net.minecraft.server.Vec3D;
import net.minecraft.server.class_aub;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_wt;
import net.minecraft.server.class_wv;
import net.minecraft.server.class_xf;
import net.minecraft.server.class_xj;

public class class_xc extends class_wv {
   private Vec3D b;

   public class_xc(class_wt var1) {
      super(var1);
   }

   public void b() {
      Vec3D var1 = this.a.a(1.0F).a();
      var1.b(-0.7853982F);
      double var2 = this.a.bu.locX;
      double var4 = this.a.bu.locY + (double)(this.a.bu.length / 2.0F);
      double var6 = this.a.bu.locZ;

      for(int var8 = 0; var8 < 8; ++var8) {
         double var9 = var2 + this.a.bE().nextGaussian() / 2.0D;
         double var11 = var4 + this.a.bE().nextGaussian() / 2.0D;
         double var13 = var6 + this.a.bE().nextGaussian() / 2.0D;
         this.a.world.a(EnumParticle.DRAGON_BREATH, var9, var11, var13, -var1.b * 0.07999999821186066D + this.a.motX, -var1.c * 0.30000001192092896D + this.a.motY, -var1.d * 0.07999999821186066D + this.a.motZ, new int[0]);
         var1.b(0.19634955F);
      }

   }

   public void c() {
      if(this.b == null) {
         this.b = new Vec3D(this.a.world.q(class_aub.a));
      }

      if(this.b.c(this.a.locX, this.a.locY, this.a.locZ) < 1.0D) {
         ((class_xf)this.a.cT().b(class_xj.f)).j();
         this.a.cT().a(class_xj.g);
      }

   }

   public float f() {
      return 1.5F;
   }

   public float h() {
      float var1 = MathHelper.a(this.a.motX * this.a.motX + this.a.motZ * this.a.motZ) + 1.0F;
      float var2 = Math.min(var1, 40.0F);
      return var2 / var1;
   }

   public void d() {
      this.b = null;
   }

   public Vec3D g() {
      return this.b;
   }

   public class_xj i() {
      return class_xj.d;
   }
}
