package net.minecraft.server;

import com.google.common.base.Predicate;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.MobEffectType;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NavigationAbstract;
import net.minecraft.server.PacketPlayOutGameStateChange;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.Material;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sw;
import net.minecraft.server.class_sx;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_to;
import net.minecraft.server.class_tv;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_vh;
import net.minecraft.server.class_wf;
import net.minecraft.server.class_yp;
import net.minecraft.server.class_ys;

public class class_yn extends class_yp {
   private static final class_ke a = DataWatcher.a(class_yn.class, class_kg.a);
   private static final class_ke b = DataWatcher.a(class_yn.class, class_kg.b);
   private float c;
   private float bv;
   private float bw;
   private float bx;
   private float by;
   private class_rz bz;
   private int bA;
   private boolean bB;
   private class_uf bC;

   public class_yn(World var1) {
      super(var1);
      this.b_ = 10;
      this.a(0.85F, 0.85F);
      this.f = new class_yn.class_c_in_class_yn(this);
      this.bv = this.c = this.random.nextFloat();
   }

   protected void r() {
      this.bp.a(4, new class_yn.class_a_in_class_yn(this));
      class_tv var1;
      this.bp.a(5, var1 = new class_tv(this, 1.0D));
      this.bp.a(7, this.bC = new class_uf(this, 1.0D, 80));
      this.bp.a(8, new class_to(this, EntityHuman.class, 8.0F));
      this.bp.a(8, new class_to(this, class_yn.class, 12.0F, 0.01F));
      this.bp.a(9, new class_ue(this));
      this.bC.a(3);
      var1.a(3);
      this.bq.a(1, new class_ux(this, class_rz.class, 10, true, false, new class_yn.class_b_in_class_yn(this)));
   }

   protected void bz() {
      super.bz();
      this.a((class_sk)class_ys.e).a(6.0D);
      this.a((class_sk)class_ys.d).a(0.5D);
      this.a((class_sk)class_ys.b).a(16.0D);
      this.a((class_sk)class_ys.a).a(30.0D);
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.a(var1.p("Elder"));
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("Elder", this.db());
   }

   protected NavigationAbstract b(World var1) {
      return new class_vh(this, var1);
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)a, (Object)Byte.valueOf((byte)0));
      this.datawatcher.a((class_ke)b, (Object)Integer.valueOf(0));
   }

   private boolean a(int var1) {
      return (((Byte)this.datawatcher.a(a)).byteValue() & var1) != 0;
   }

   private void a(int var1, boolean var2) {
      byte var3 = ((Byte)this.datawatcher.a(a)).byteValue();
      if(var2) {
         this.datawatcher.b(a, Byte.valueOf((byte)(var3 | var1)));
      } else {
         this.datawatcher.b(a, Byte.valueOf((byte)(var3 & ~var1)));
      }

   }

   public boolean o() {
      return this.a(2);
   }

   private void o(boolean var1) {
      this.a(2, var1);
   }

   public int da() {
      return this.db()?60:80;
   }

   public boolean db() {
      return this.a(4);
   }

   public void a(boolean var1) {
      this.a(4, var1);
      if(var1) {
         this.a(1.9975F, 1.9975F);
         this.a((class_sk)class_ys.d).a(0.30000001192092896D);
         this.a((class_sk)class_ys.e).a(8.0D);
         this.a((class_sk)class_ys.a).a(80.0D);
         this.cL();
         if(this.bC != null) {
            this.bC.b(400);
         }
      }

   }

   private void b(int var1) {
      this.datawatcher.b(b, Integer.valueOf(var1));
   }

   public boolean dd() {
      return ((Integer)this.datawatcher.a(b)).intValue() != 0;
   }

   public class_rz de() {
      if(!this.dd()) {
         return null;
      } else if(this.world.E) {
         if(this.bz != null) {
            return this.bz;
         } else {
            Entity var1 = this.world.a(((Integer)this.datawatcher.a(b)).intValue());
            if(var1 instanceof class_rz) {
               this.bz = (class_rz)var1;
               return this.bz;
            } else {
               return null;
            }
         }
      } else {
         return this.A();
      }
   }

   public void a(class_ke var1) {
      super.a(var1);
      if(a.equals(var1)) {
         if(this.db() && this.width < 1.0F) {
            this.a(1.9975F, 1.9975F);
         }
      } else if(b.equals(var1)) {
         this.bA = 0;
         this.bz = null;
      }

   }

   public int C() {
      return 160;
   }

   protected class_nf G() {
      return this.db()?(this.ah()?class_ng.aB:class_ng.aC):(this.ah()?class_ng.cb:class_ng.cc);
   }

   protected class_nf bQ() {
      return this.db()?(this.ah()?class_ng.aG:class_ng.aH):(this.ah()?class_ng.ch:class_ng.ci);
   }

   protected class_nf bR() {
      return this.db()?(this.ah()?class_ng.aE:class_ng.aF):(this.ah()?class_ng.ce:class_ng.cf);
   }

   protected boolean ad() {
      return false;
   }

   public float bm() {
      return this.length * 0.5F;
   }

   public float a(BlockPosition var1) {
      return this.world.getType(var1).getMaterial() == Material.h?10.0F + this.world.n(var1) - 0.5F:super.a(var1);
   }

   public void n() {
      if(this.world.E) {
         this.bv = this.c;
         if(!this.ah()) {
            this.bw = 2.0F;
            if(this.motY > 0.0D && this.bB && !this.ac()) {
               this.world.a(this.locX, this.locY, this.locZ, class_ng.cg, this.by(), 1.0F, 1.0F, false);
            }

            this.bB = this.motY < 0.0D && this.world.d((new BlockPosition(this)).b(), false);
         } else if(this.o()) {
            if(this.bw < 0.5F) {
               this.bw = 4.0F;
            } else {
               this.bw += (0.5F - this.bw) * 0.1F;
            }
         } else {
            this.bw += (0.125F - this.bw) * 0.2F;
         }

         this.c += this.bw;
         this.by = this.bx;
         if(!this.ah()) {
            this.bx = this.random.nextFloat();
         } else if(this.o()) {
            this.bx += (0.0F - this.bx) * 0.25F;
         } else {
            this.bx += (1.0F - this.bx) * 0.06F;
         }

         if(this.o() && this.ah()) {
            Vec3D var1 = this.f(0.0F);

            for(int var2 = 0; var2 < 2; ++var2) {
               this.world.a(EnumParticle.WATER_BUBBLE, this.locX + (this.random.nextDouble() - 0.5D) * (double)this.width - var1.b * 1.5D, this.locY + this.random.nextDouble() * (double)this.length - var1.c * 1.5D, this.locZ + (this.random.nextDouble() - 0.5D) * (double)this.width - var1.d * 1.5D, 0.0D, 0.0D, 0.0D, new int[0]);
            }
         }

         if(this.dd()) {
            if(this.bA < this.da()) {
               ++this.bA;
            }

            class_rz var14 = this.de();
            if(var14 != null) {
               this.t().a(var14, 90.0F, 90.0F);
               this.t().a();
               double var15 = (double)this.s(0.0F);
               double var4 = var14.locX - this.locX;
               double var6 = var14.locY + (double)(var14.length * 0.5F) - (this.locY + (double)this.bm());
               double var8 = var14.locZ - this.locZ;
               double var10 = Math.sqrt(var4 * var4 + var6 * var6 + var8 * var8);
               var4 /= var10;
               var6 /= var10;
               var8 /= var10;
               double var12 = this.random.nextDouble();

               while(var12 < var10) {
                  var12 += 1.8D - var15 + this.random.nextDouble() * (1.7D - var15);
                  this.world.a(EnumParticle.WATER_BUBBLE, this.locX + var4 * var12, this.locY + var6 * var12 + (double)this.bm(), this.locZ + var8 * var12, 0.0D, 0.0D, 0.0D, new int[0]);
               }
            }
         }
      }

      if(this.inWater) {
         this.j(300);
      } else if(this.onGround) {
         this.motY += 0.5D;
         this.motX += (double)((this.random.nextFloat() * 2.0F - 1.0F) * 0.4F);
         this.motZ += (double)((this.random.nextFloat() * 2.0F - 1.0F) * 0.4F);
         this.yaw = this.random.nextFloat() * 360.0F;
         this.onGround = false;
         this.ai = true;
      }

      if(this.dd()) {
         this.yaw = this.aO;
      }

      super.n();
   }

   public float s(float var1) {
      return ((float)this.bA + var1) / (float)this.da();
   }

   protected void M() {
      super.M();
      if(this.db()) {
         boolean var1 = true;
         boolean var2 = true;
         boolean var3 = true;
         boolean var4 = true;
         if((this.ticksLived + this.getId()) % 1200 == 0) {
            MobEffectType var5 = MobEffectList.d;
            List var6 = this.world.b(EntityPlayer.class, new Predicate() {
               public boolean a(EntityPlayer var1) {
                  return class_yn.this.h(var1) < 2500.0D && var1.c.c();
               }

               // $FF: synthetic method
               public boolean apply(Object var1) {
                  return this.a((EntityPlayer)var1);
               }
            });
            Iterator var7 = var6.iterator();

            label30:
            while(true) {
               EntityPlayer var8;
               do {
                  if(!var7.hasNext()) {
                     break label30;
                  }

                  var8 = (EntityPlayer)var7.next();
               } while(var8.a((MobEffectType)var5) && var8.b((MobEffectType)var5).c() >= 2 && var8.b((MobEffectType)var5).b() >= 1200);

               var8.a.a((Packet)(new PacketPlayOutGameStateChange(10, 0.0F)));
               var8.c(new MobEffect(var5, 6000, 2));
            }
         }

         if(!this.cY()) {
            this.a(new BlockPosition(this), 16);
         }
      }

   }

   protected class_kk J() {
      return this.db()?class_azs.w:class_azs.v;
   }

   protected boolean s_() {
      return true;
   }

   public boolean cG() {
      return this.world.a((AxisAlignedBB)this.bk(), (Entity)this) && this.world.a((Entity)this, (AxisAlignedBB)this.bk()).isEmpty();
   }

   public boolean cF() {
      return (this.random.nextInt(20) == 0 || !this.world.i(new BlockPosition(this))) && super.cF();
   }

   public boolean a(DamageSource var1, float var2) {
      if(!this.o() && !var1.s() && var1.i() instanceof class_rz) {
         class_rz var3 = (class_rz)var1.i();
         if(!var1.c()) {
            var3.a(DamageSource.a((Entity)this), 2.0F);
         }
      }

      if(this.bC != null) {
         this.bC.f();
      }

      return super.a(var1, var2);
   }

   public int cD() {
      return 180;
   }

   public void g(float var1, float var2) {
      if(this.cn()) {
         if(this.ah()) {
            this.a(var1, var2, 0.1F);
            this.d(this.motX, this.motY, this.motZ);
            this.motX *= 0.8999999761581421D;
            this.motY *= 0.8999999761581421D;
            this.motZ *= 0.8999999761581421D;
            if(!this.o() && this.A() == null) {
               this.motY -= 0.005D;
            }
         } else {
            super.g(var1, var2);
         }
      } else {
         super.g(var1, var2);
      }

   }

   static class class_c_in_class_yn extends class_sx {
      private class_yn i;

      public class_c_in_class_yn(class_yn var1) {
         super(var1);
         this.i = var1;
      }

      public void c() {
         if(this.h == class_sx.class_a_in_class_sx.MOVE_TO && !this.i.x().n()) {
            double var1 = this.b - this.i.locX;
            double var3 = this.c - this.i.locY;
            double var5 = this.d - this.i.locZ;
            double var7 = var1 * var1 + var3 * var3 + var5 * var5;
            var7 = (double)MathHelper.a(var7);
            var3 /= var7;
            float var9 = (float)(MathHelper.b(var5, var1) * 57.2957763671875D) - 90.0F;
            this.i.yaw = this.a(this.i.yaw, var9, 90.0F);
            this.i.aM = this.i.yaw;
            float var10 = (float)(this.e * this.i.a((class_sk)class_ys.d).e());
            this.i.l(this.i.cj() + (var10 - this.i.cj()) * 0.125F);
            double var11 = Math.sin((double)(this.i.ticksLived + this.i.getId()) * 0.5D) * 0.05D;
            double var13 = Math.cos((double)(this.i.yaw * 0.017453292F));
            double var15 = Math.sin((double)(this.i.yaw * 0.017453292F));
            this.i.motX += var11 * var13;
            this.i.motZ += var11 * var15;
            var11 = Math.sin((double)(this.i.ticksLived + this.i.getId()) * 0.75D) * 0.05D;
            this.i.motY += var11 * (var15 + var13) * 0.25D;
            this.i.motY += (double)this.i.cj() * var3 * 0.1D;
            class_sw var17 = this.i.t();
            double var18 = this.i.locX + var1 / var7 * 2.0D;
            double var20 = (double)this.i.bm() + this.i.locY + var3 / var7;
            double var22 = this.i.locZ + var5 / var7 * 2.0D;
            double var24 = var17.e();
            double var26 = var17.f();
            double var28 = var17.g();
            if(!var17.b()) {
               var24 = var18;
               var26 = var20;
               var28 = var22;
            }

            this.i.t().a(var24 + (var18 - var24) * 0.125D, var26 + (var20 - var26) * 0.125D, var28 + (var22 - var28) * 0.125D, 10.0F, 40.0F);
            this.i.o(true);
         } else {
            this.i.l(0.0F);
            this.i.o(false);
         }
      }
   }

   static class class_a_in_class_yn extends class_tj {
      private class_yn a;
      private int b;

      public class_a_in_class_yn(class_yn var1) {
         this.a = var1;
         this.a(3);
      }

      public boolean a() {
         class_rz var1 = this.a.A();
         return var1 != null && var1.at();
      }

      public boolean b() {
         return super.b() && (this.a.db() || this.a.h(this.a.A()) > 9.0D);
      }

      public void c() {
         this.b = -10;
         this.a.x().o();
         this.a.t().a(this.a.A(), 90.0F, 90.0F);
         this.a.ai = true;
      }

      public void d() {
         this.a.b(0);
         this.a.c((class_rz)null);
         this.a.bC.f();
      }

      public void e() {
         class_rz var1 = this.a.A();
         this.a.x().o();
         this.a.t().a(var1, 90.0F, 90.0F);
         if(!this.a.D(var1)) {
            this.a.c((class_rz)null);
         } else {
            ++this.b;
            if(this.b == 0) {
               this.a.b(this.a.A().getId());
               this.a.world.a((Entity)this.a, (byte)21);
            } else if(this.b >= this.a.da()) {
               float var2 = 1.0F;
               if(this.a.world.ae() == EnumDifficulty.HARD) {
                  var2 += 2.0F;
               }

               if(this.a.db()) {
                  var2 += 2.0F;
               }

               var1.a(DamageSource.b(this.a, this.a), var2);
               var1.a(DamageSource.a((class_rz)this.a), (float)this.a.a((class_sk)class_ys.e).e());
               this.a.c((class_rz)null);
            }

            super.e();
         }
      }
   }

   static class class_b_in_class_yn implements Predicate {
      private class_yn a;

      public class_b_in_class_yn(class_yn var1) {
         this.a = var1;
      }

      public boolean a(class_rz var1) {
         return (var1 instanceof EntityHuman || var1 instanceof class_wf) && var1.h(this.a) > 9.0D;
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((class_rz)var1);
      }
   }
}
