package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockMinecartTrackAbstract;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.World;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.BlockPosition;

public class class_ans extends BlockMinecartTrackAbstract {
   public static final BlockStateEnum d = BlockStateEnum.a("shape", BlockMinecartTrackAbstract.EnumTrackPosition.class);

   protected class_ans() {
      super(false);
      this.w(this.A.b().set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH));
   }

   protected void b(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(var4.u().m() && (new BlockMinecartTrackAbstract.class_a_in_class_ajo(var1, var2, var3)).b() == 3) {
         this.a(var1, var2, var3, false);
      }

   }

   public IBlockState g() {
      return d;
   }

   public IBlockData a(int var1) {
      return this.u().set(d, BlockMinecartTrackAbstract.EnumTrackPosition.a(var1));
   }

   public int e(IBlockData var1) {
      return ((BlockMinecartTrackAbstract.EnumTrackPosition)var1.get(d)).a();
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      switch(class_ans.SyntheticClass_1.b[var2.ordinal()]) {
      case 1:
         switch(class_ans.SyntheticClass_1.a[((BlockMinecartTrackAbstract.EnumTrackPosition)var1.get(d)).ordinal()]) {
         case 1:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST);
         case 2:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST);
         case 3:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH);
         case 4:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH);
         case 5:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST);
         case 6:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST);
         case 7:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST);
         case 8:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST);
         }
      case 2:
         switch(class_ans.SyntheticClass_1.a[((BlockMinecartTrackAbstract.EnumTrackPosition)var1.get(d)).ordinal()]) {
         case 1:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH);
         case 2:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH);
         case 3:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST);
         case 4:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST);
         case 5:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST);
         case 6:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST);
         case 7:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST);
         case 8:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST);
         case 9:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST);
         case 10:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH);
         }
      case 3:
         switch(class_ans.SyntheticClass_1.a[((BlockMinecartTrackAbstract.EnumTrackPosition)var1.get(d)).ordinal()]) {
         case 1:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH);
         case 2:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH);
         case 3:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST);
         case 4:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST);
         case 5:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST);
         case 6:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST);
         case 7:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST);
         case 8:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST);
         case 9:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST);
         case 10:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH);
         }
      default:
         return var1;
      }
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      BlockMinecartTrackAbstract.EnumTrackPosition var3 = (BlockMinecartTrackAbstract.EnumTrackPosition)var1.get(d);
      switch(class_ans.SyntheticClass_1.c[var2.ordinal()]) {
      case 1:
         switch(class_ans.SyntheticClass_1.a[var3.ordinal()]) {
         case 3:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH);
         case 4:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH);
         case 5:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST);
         case 6:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST);
         case 7:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST);
         case 8:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST);
         default:
            return super.a(var1, var2);
         }
      case 2:
         switch(class_ans.SyntheticClass_1.a[var3.ordinal()]) {
         case 1:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST);
         case 2:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST);
         case 3:
         case 4:
         default:
            break;
         case 5:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST);
         case 6:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST);
         case 7:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST);
         case 8:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST);
         }
      }

      return super.a(var1, var2);
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{d});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b;
      // $FF: synthetic field
      static final int[] c = new int[class_amq.values().length];

      static {
         try {
            c[class_amq.LEFT_RIGHT.ordinal()] = 1;
         } catch (NoSuchFieldError var15) {
            ;
         }

         try {
            c[class_amq.FRONT_BACK.ordinal()] = 2;
         } catch (NoSuchFieldError var14) {
            ;
         }

         b = new int[class_aod.values().length];

         try {
            b[class_aod.CLOCKWISE_180.ordinal()] = 1;
         } catch (NoSuchFieldError var13) {
            ;
         }

         try {
            b[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var12) {
            ;
         }

         try {
            b[class_aod.CLOCKWISE_90.ordinal()] = 3;
         } catch (NoSuchFieldError var11) {
            ;
         }

         a = new int[BlockMinecartTrackAbstract.EnumTrackPosition.values().length];

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST.ordinal()] = 1;
         } catch (NoSuchFieldError var10) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST.ordinal()] = 2;
         } catch (NoSuchFieldError var9) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH.ordinal()] = 3;
         } catch (NoSuchFieldError var8) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH.ordinal()] = 4;
         } catch (NoSuchFieldError var7) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST.ordinal()] = 5;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST.ordinal()] = 6;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST.ordinal()] = 7;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST.ordinal()] = 8;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH.ordinal()] = 9;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST.ordinal()] = 10;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
