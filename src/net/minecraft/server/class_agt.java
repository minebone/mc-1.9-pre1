package net.minecraft.server;

import net.minecraft.server.Enchantment;
import net.minecraft.server.class_agm;
import net.minecraft.server.EnumInventorySlot;

public class class_agt extends Enchantment {
   protected class_agt(Enchantment.class_a_in_class_agl var1, EnumInventorySlot... var2) {
      super(var1, class_agm.WEAPON, var2);
      this.c("knockback");
   }

   public int a(int var1) {
      return 5 + 20 * (var1 - 1);
   }

   public int b(int var1) {
      return super.a(var1) + 50;
   }

   public int b() {
      return 2;
   }
}
