package net.minecraft.server;

import com.mojang.authlib.GameProfile;
import java.util.Collections;
import java.util.List;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;

public class CommandWhitelist extends CommandAbstract {
   public String c() {
      return "whitelist";
   }

   public int a() {
      return 3;
   }

   public String b(ICommandListener var1) {
      return "commands.whitelist.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 1) {
         throw new class_cf("commands.whitelist.usage", new Object[0]);
      } else {
         if(var3[0].equals("on")) {
            var1.getPlayerList().a(true);
            a(var2, this, "commands.whitelist.enabled", new Object[0]);
         } else if(var3[0].equals("off")) {
            var1.getPlayerList().a(false);
            a(var2, this, "commands.whitelist.disabled", new Object[0]);
         } else if(var3[0].equals("list")) {
            var2.a(new ChatMessage("commands.whitelist.list", new Object[]{Integer.valueOf(var1.getPlayerList().l().length), Integer.valueOf(var1.getPlayerList().q().length)}));
            String[] var4 = var1.getPlayerList().l();
            var2.a(new ChatComponentText(a(var4)));
         } else {
            GameProfile var5;
            if(var3[0].equals("add")) {
               if(var3.length < 2) {
                  throw new class_cf("commands.whitelist.add.usage", new Object[0]);
               }

               var5 = var1.aA().a(var3[1]);
               if(var5 == null) {
                  throw new class_bz("commands.whitelist.add.failed", new Object[]{var3[1]});
               }

               var1.getPlayerList().d(var5);
               a(var2, this, "commands.whitelist.add.success", new Object[]{var3[1]});
            } else if(var3[0].equals("remove")) {
               if(var3.length < 2) {
                  throw new class_cf("commands.whitelist.remove.usage", new Object[0]);
               }

               var5 = var1.getPlayerList().k().a(var3[1]);
               if(var5 == null) {
                  throw new class_bz("commands.whitelist.remove.failed", new Object[]{var3[1]});
               }

               var1.getPlayerList().c(var5);
               a(var2, this, "commands.whitelist.remove.success", new Object[]{var3[1]});
            } else if(var3[0].equals("reload")) {
               var1.getPlayerList().a();
               a(var2, this, "commands.whitelist.reloaded", new Object[0]);
            }
         }

      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      if(var3.length == 1) {
         return a(var3, new String[]{"on", "off", "list", "add", "remove", "reload"});
      } else {
         if(var3.length == 2) {
            if(var3[0].equals("remove")) {
               return a(var3, var1.getPlayerList().l());
            }

            if(var3[0].equals("add")) {
               return a(var3, var1.aA().a());
            }
         }

         return Collections.emptyList();
      }
   }
}
