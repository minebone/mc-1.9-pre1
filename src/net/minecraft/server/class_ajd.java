package net.minecraft.server;

import net.minecraft.server.BiomeBase;
import net.minecraft.server.Blocks;
import net.minecraft.server.class_aje;
import net.minecraft.server.class_yi;

public class class_ajd extends BiomeBase {
   public class_ajd(BiomeBase.class_a_in_class_aif var1) {
      super(var1);
      this.u.clear();
      this.v.clear();
      this.w.clear();
      this.x.clear();
      this.u.add(new BiomeBase.BiomeMeta(class_yi.class, 10, 4, 4));
      this.r = Blocks.d.u();
      this.s = Blocks.d.u();
      this.t = new class_aje();
   }
}
