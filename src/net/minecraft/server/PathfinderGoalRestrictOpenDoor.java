package net.minecraft.server;

import net.minecraft.server.EntityCreature;
import net.minecraft.server.Village;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_vd;
import net.minecraft.server.class_vn;

public class PathfinderGoalRestrictOpenDoor extends class_tj {
   private EntityCreature a;
   private class_vn b;

   public PathfinderGoalRestrictOpenDoor(EntityCreature var1) {
      this.a = var1;
      if(!(var1.x() instanceof class_vd)) {
         throw new IllegalArgumentException("Unsupported mob type for RestrictOpenDoorGoal");
      }
   }

   public boolean a() {
      if(this.a.world.B()) {
         return false;
      } else {
         BlockPosition var1 = new BlockPosition(this.a);
         Village var2 = this.a.world.ai().a(var1, 16);
         if(var2 == null) {
            return false;
         } else {
            this.b = var2.b(var1);
            return this.b == null?false:(double)this.b.b(var1) < 2.25D;
         }
      }
   }

   public boolean b() {
      return this.a.world.B()?false:!this.b.i() && this.b.c(new BlockPosition(this.a));
   }

   public void c() {
      ((class_vd)this.a.x()).a(false);
      ((class_vd)this.a.x()).b(false);
   }

   public void d() {
      ((class_vd)this.a.x()).a(true);
      ((class_vd)this.a.x()).b(true);
      this.b = null;
   }

   public void e() {
      this.b.b();
   }
}
