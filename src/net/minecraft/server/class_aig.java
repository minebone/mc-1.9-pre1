package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.List;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_aij;
import net.minecraft.server.class_ol;

public class class_aig {
   private final class_aij a;
   private long b;
   private final class_ol c = new class_ol();
   private final List d = Lists.newArrayList();

   public class_aig(class_aij var1) {
      this.a = var1;
   }

   public class_aig.class_a_in_class_aig a(int var1, int var2) {
      var1 >>= 4;
      var2 >>= 4;
      long var3 = (long)var1 & 4294967295L | ((long)var2 & 4294967295L) << 32;
      class_aig.class_a_in_class_aig var5 = (class_aig.class_a_in_class_aig)this.c.a(var3);
      if(var5 == null) {
         var5 = new class_aig.class_a_in_class_aig(var1, var2);
         this.c.a(var3, var5);
         this.d.add(var5);
      }

      var5.d = MinecraftServer.av();
      return var5;
   }

   public BiomeBase a(int var1, int var2, BiomeBase var3) {
      BiomeBase var4 = this.a(var1, var2).a(var1, var2);
      return var4 == null?var3:var4;
   }

   public void a() {
      long var1 = MinecraftServer.av();
      long var3 = var1 - this.b;
      if(var3 > 7500L || var3 < 0L) {
         this.b = var1;

         for(int var5 = 0; var5 < this.d.size(); ++var5) {
            class_aig.class_a_in_class_aig var6 = (class_aig.class_a_in_class_aig)this.d.get(var5);
            long var7 = var1 - var6.d;
            if(var7 > 30000L || var7 < 0L) {
               this.d.remove(var5--);
               long var9 = (long)var6.b & 4294967295L | ((long)var6.c & 4294967295L) << 32;
               this.c.d(var9);
            }
         }
      }

   }

   public BiomeBase[] b(int var1, int var2) {
      return this.a(var1, var2).a;
   }

   public class class_a_in_class_aig {
      public BiomeBase[] a = new BiomeBase[256];
      public int b;
      public int c;
      public long d;

      public class_a_in_class_aig(int var2, int var3) {
         this.b = var2;
         this.c = var3;
         class_aig.this.a.a(this.a, var2 << 4, var3 << 4, 16, 16, false);
      }

      public BiomeBase a(int var1, int var2) {
         return this.a[var1 & 15 | (var2 & 15) << 4];
      }
   }
}
