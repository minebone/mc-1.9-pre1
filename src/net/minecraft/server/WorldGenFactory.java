package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Map;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.StructurePiece;
import net.minecraft.server.StructureStart;
import net.minecraft.server.World;
import net.minecraft.server.WorldGenLargeFeature;
import net.minecraft.server.WorldGenMineshaftPieces;
import net.minecraft.server.WorldGenMonument;
import net.minecraft.server.WorldGenMonumentPieces;
import net.minecraft.server.WorldGenNetherPieces;
import net.minecraft.server.WorldGenRegistration;
import net.minecraft.server.WorldGenStronghold;
import net.minecraft.server.WorldGenStrongholdPieces;
import net.minecraft.server.WorldGenVillage;
import net.minecraft.server.WorldGenVillagePieces;
import net.minecraft.server.class_avp;
import net.minecraft.server.class_avq;
import net.minecraft.server.class_avt;
import net.minecraft.server.class_avu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WorldGenFactory {
   private static final Logger a = LogManager.getLogger();
   private static Map b = Maps.newHashMap();
   private static Map c = Maps.newHashMap();
   private static Map d = Maps.newHashMap();
   private static Map e = Maps.newHashMap();

   private static void b(Class var0, String var1) {
      b.put(var1, var0);
      c.put(var0, var1);
   }

   static void a(Class var0, String var1) {
      d.put(var1, var0);
      e.put(var0, var1);
   }

   public static String a(StructureStart var0) {
      return (String)c.get(var0.getClass());
   }

   public static String a(StructurePiece var0) {
      return (String)e.get(var0.getClass());
   }

   public static StructureStart a(NBTTagCompound var0, World var1) {
      StructureStart var2 = null;

      try {
         Class var3 = (Class)b.get(var0.l("id"));
         if(var3 != null) {
            var2 = (StructureStart)var3.newInstance();
         }
      } catch (Exception var4) {
         a.warn("Failed Start with id " + var0.l("id"));
         var4.printStackTrace();
      }

      if(var2 != null) {
         var2.a(var1, var0);
      } else {
         a.warn("Skipping Structure with id " + var0.l("id"));
      }

      return var2;
   }

   public static StructurePiece b(NBTTagCompound var0, World var1) {
      StructurePiece var2 = null;

      try {
         Class var3 = (Class)d.get(var0.l("id"));
         if(var3 != null) {
            var2 = (StructurePiece)var3.newInstance();
         }
      } catch (Exception var4) {
         a.warn("Failed Piece with id " + var0.l("id"));
         var4.printStackTrace();
      }

      if(var2 != null) {
         var2.a(var1, var0);
      } else {
         a.warn("Skipping Piece with id " + var0.l("id"));
      }

      return var2;
   }

   static {
      b(class_avt.class, "Mineshaft");
      b(WorldGenVillage.WorldGenVillageStart.class, "Village");
      b(class_avu.class_a_in_class_avu.class, "Fortress");
      b(WorldGenStronghold.class_a_in_class_awa.class, "Stronghold");
      b(WorldGenLargeFeature.class_a_in_class_avy.class, "Temple");
      b(WorldGenMonument.WorldGenMonumentStart.class, "Monument");
      b(class_avp.class_a_in_class_avp.class, "EndCity");
      WorldGenMineshaftPieces.a();
      WorldGenVillagePieces.a();
      WorldGenNetherPieces.a();
      WorldGenStrongholdPieces.a();
      WorldGenRegistration.a();
      WorldGenMonumentPieces.a();
      class_avq.a();
   }
}
