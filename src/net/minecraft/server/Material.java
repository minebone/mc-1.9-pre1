package net.minecraft.server;

import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.class_axa;
import net.minecraft.server.class_axb;
import net.minecraft.server.class_axc;
import net.minecraft.server.class_axf;
import net.minecraft.server.class_axg;

public class Material {
   public static final Material a = new class_axb(MaterialMapColor.b);
   public static final Material b = new Material(MaterialMapColor.c);
   public static final Material c = new Material(MaterialMapColor.l);
   public static final Material d = (new Material(MaterialMapColor.o)).g();
   public static final Material e = (new Material(MaterialMapColor.m)).f();
   public static final Material f = (new Material(MaterialMapColor.h)).f();
   public static final Material g = (new Material(MaterialMapColor.h)).f().o();
   public static final Material h = (new class_axc(MaterialMapColor.n)).n();
   public static final Material i = (new class_axc(MaterialMapColor.f)).n();
   public static final Material j = (new Material(MaterialMapColor.i)).g().s().n();
   public static final Material k = (new class_axa(MaterialMapColor.i)).n();
   public static final Material l = (new class_axa(MaterialMapColor.i)).g().n().i();
   public static final Material m = new Material(MaterialMapColor.t);
   public static final Material n = (new Material(MaterialMapColor.e)).g();
   public static final Material o = (new class_axb(MaterialMapColor.b)).n();
   public static final Material p = new Material(MaterialMapColor.d);
   public static final Material q = (new class_axa(MaterialMapColor.b)).n();
   public static final Material r = (new class_axa(MaterialMapColor.e)).g();
   public static final Material s = (new Material(MaterialMapColor.b)).s().p();
   public static final Material t = (new Material(MaterialMapColor.b)).p();
   public static final Material u = (new Material(MaterialMapColor.f)).g().s();
   public static final Material v = (new Material(MaterialMapColor.i)).n();
   public static final Material w = (new Material(MaterialMapColor.g)).s().p();
   public static final Material x = (new Material(MaterialMapColor.g)).p();
   public static final Material y = (new class_axa(MaterialMapColor.j)).i().s().f().n();
   public static final Material z = (new Material(MaterialMapColor.j)).f();
   public static final Material A = (new Material(MaterialMapColor.i)).s().n();
   public static final Material B = new Material(MaterialMapColor.k);
   public static final Material C = (new Material(MaterialMapColor.i)).n();
   public static final Material D = (new Material(MaterialMapColor.i)).n();
   public static final Material E = (new class_axf(MaterialMapColor.b)).o();
   public static final Material F = (new Material(MaterialMapColor.b)).n();
   public static final Material G = (new Material(MaterialMapColor.e) {
      public boolean c() {
         return false;
      }
   }).f().n();
   public static final Material H = (new Material(MaterialMapColor.m)).o();
   public static final Material I = (new Material(MaterialMapColor.b)).f().o();
   private boolean J;
   private boolean K;
   private boolean L;
   private final MaterialMapColor M;
   private boolean N = true;
   private class_axg O = class_axg.NORMAL;
   private boolean P;

   public Material(MaterialMapColor var1) {
      this.M = var1;
   }

   public boolean d() {
      return false;
   }

   public boolean a() {
      return true;
   }

   public boolean b() {
      return true;
   }

   public boolean c() {
      return true;
   }

   private Material s() {
      this.L = true;
      return this;
   }

   protected Material f() {
      this.N = false;
      return this;
   }

   protected Material g() {
      this.J = true;
      return this;
   }

   public boolean h() {
      return this.J;
   }

   public Material i() {
      this.K = true;
      return this;
   }

   public boolean j() {
      return this.K;
   }

   public boolean k() {
      return this.L?false:this.c();
   }

   public boolean l() {
      return this.N;
   }

   public class_axg m() {
      return this.O;
   }

   protected Material n() {
      this.O = class_axg.DESTROY;
      return this;
   }

   protected Material o() {
      this.O = class_axg.BLOCK;
      return this;
   }

   protected Material p() {
      this.P = true;
      return this;
   }

   public MaterialMapColor r() {
      return this.M;
   }
}
