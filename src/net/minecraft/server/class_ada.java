package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Item;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.ShapeDetector;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_alc;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;
import net.minecraft.server.class_zn;

public class class_ada extends Item {
   public class_ada() {
      this.a(CreativeModeTab.f);
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      IBlockData var10 = var3.getType(var4);
      if(var2.a(var4.a(var6), var6, var1) && var10.getBlock() == Blocks.bG && !((Boolean)var10.get(class_alc.b)).booleanValue()) {
         if(var3.E) {
            return EnumResult.SUCCESS;
         } else {
            var3.a((BlockPosition)var4, (IBlockData)var10.set(class_alc.b, Boolean.valueOf(true)), 2);
            var3.f(var4, Blocks.bG);
            --var1.b;

            for(int var11 = 0; var11 < 16; ++var11) {
               double var12 = (double)((float)var4.p() + (5.0F + i.nextFloat() * 6.0F) / 16.0F);
               double var14 = (double)((float)var4.q() + 0.8125F);
               double var16 = (double)((float)var4.r() + (5.0F + i.nextFloat() * 6.0F) / 16.0F);
               double var18 = 0.0D;
               double var20 = 0.0D;
               double var22 = 0.0D;
               var3.a(EnumParticle.SMOKE_NORMAL, var12, var14, var16, 0.0D, 0.0D, 0.0D, new int[0]);
            }

            EnumDirection var24 = (EnumDirection)var10.get(class_alc.a);
            ShapeDetector.ShapeDetectorCollection var25 = class_alc.e().a(var3, var4);
            if(var25 != null) {
               BlockPosition var13 = var25.a().a(-3, 0, -3);

               for(int var26 = 0; var26 < 3; ++var26) {
                  for(int var15 = 0; var15 < 3; ++var15) {
                     var3.a((BlockPosition)var13.a(var26, 0, var15), (IBlockData)Blocks.bF.u(), 2);
                  }
               }
            }

            return EnumResult.SUCCESS;
         }
      } else {
         return EnumResult.FAIL;
      }
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      MovingObjectPosition var5 = this.a(var2, var3, false);
      if(var5 != null && var5.a == MovingObjectPosition.EnumMovingObjectType.BLOCK && var2.getType(var5.a()).getBlock() == Blocks.bG) {
         return new class_qo(EnumResult.PASS, var1);
      } else {
         if(!var2.E) {
            BlockPosition var6 = ((WorldServer)var2).r().a(var2, "Stronghold", new BlockPosition(var3));
            if(var6 != null) {
               class_zn var7 = new class_zn(var2, var3.locX, var3.locY + (double)(var3.length / 2.0F), var3.locZ);
               var7.a(var6);
               var2.a((Entity)var7);
               var2.a((EntityHuman)null, var3.locX, var3.locY, var3.locZ, class_ng.aR, EnumSoundCategory.NEUTRAL, 0.5F, 0.4F / (i.nextFloat() * 0.4F + 0.8F));
               var2.a((EntityHuman)null, 1003, new BlockPosition(var3), 0);
               if(!var3.abilities.d) {
                  --var1.b;
               }

               var3.b(StatisticList.b((Item)this));
               return new class_qo(EnumResult.SUCCESS, var1);
            }
         }

         return new class_qo(EnumResult.FAIL, var1);
      }
   }
}
