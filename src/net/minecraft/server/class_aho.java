package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.class_agx;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_yd;

public class class_aho {
   private final boolean a;
   private final boolean b;
   private final Random c = new Random();
   private final World d;
   private final double e;
   private final double f;
   private final double g;
   private final Entity h;
   private final float i;
   private final List j = Lists.newArrayList();
   private final Map k = Maps.newHashMap();

   public class_aho(World var1, Entity var2, double var3, double var5, double var7, float var9, boolean var10, boolean var11) {
      this.d = var1;
      this.h = var2;
      this.i = var9;
      this.e = var3;
      this.f = var5;
      this.g = var7;
      this.a = var10;
      this.b = var11;
   }

   public void a() {
      HashSet var1 = Sets.newHashSet();
      boolean var2 = true;

      int var4;
      int var5;
      for(int var3 = 0; var3 < 16; ++var3) {
         for(var4 = 0; var4 < 16; ++var4) {
            for(var5 = 0; var5 < 16; ++var5) {
               if(var3 == 0 || var3 == 15 || var4 == 0 || var4 == 15 || var5 == 0 || var5 == 15) {
                  double var6 = (double)((float)var3 / 15.0F * 2.0F - 1.0F);
                  double var8 = (double)((float)var4 / 15.0F * 2.0F - 1.0F);
                  double var10 = (double)((float)var5 / 15.0F * 2.0F - 1.0F);
                  double var12 = Math.sqrt(var6 * var6 + var8 * var8 + var10 * var10);
                  var6 /= var12;
                  var8 /= var12;
                  var10 /= var12;
                  float var14 = this.i * (0.7F + this.d.r.nextFloat() * 0.6F);
                  double var15 = this.e;
                  double var17 = this.f;
                  double var19 = this.g;

                  for(float var21 = 0.3F; var14 > 0.0F; var14 -= 0.22500001F) {
                     BlockPosition var22 = new BlockPosition(var15, var17, var19);
                     IBlockData var23 = this.d.getType(var22);
                     if(var23.getMaterial() != Material.a) {
                        float var24 = this.h != null?this.h.a(this, this.d, var22, var23):var23.getBlock().a((Entity)null);
                        var14 -= (var24 + 0.3F) * 0.3F;
                     }

                     if(var14 > 0.0F && (this.h == null || this.h.a(this, this.d, var22, var23, var14))) {
                        var1.add(var22);
                     }

                     var15 += var6 * 0.30000001192092896D;
                     var17 += var8 * 0.30000001192092896D;
                     var19 += var10 * 0.30000001192092896D;
                  }
               }
            }
         }
      }

      this.j.addAll(var1);
      float var31 = this.i * 2.0F;
      var4 = MathHelper.c(this.e - (double)var31 - 1.0D);
      var5 = MathHelper.c(this.e + (double)var31 + 1.0D);
      int var32 = MathHelper.c(this.f - (double)var31 - 1.0D);
      int var7 = MathHelper.c(this.f + (double)var31 + 1.0D);
      int var33 = MathHelper.c(this.g - (double)var31 - 1.0D);
      int var9 = MathHelper.c(this.g + (double)var31 + 1.0D);
      List var34 = this.d.b(this.h, new AxisAlignedBB((double)var4, (double)var32, (double)var33, (double)var5, (double)var7, (double)var9));
      Vec3D var11 = new Vec3D(this.e, this.f, this.g);

      for(int var35 = 0; var35 < var34.size(); ++var35) {
         Entity var13 = (Entity)var34.get(var35);
         if(!var13.bp()) {
            double var36 = var13.f(this.e, this.f, this.g) / (double)var31;
            if(var36 <= 1.0D) {
               double var16 = var13.locX - this.e;
               double var18 = var13.locY + (double)var13.bm() - this.f;
               double var20 = var13.locZ - this.g;
               double var37 = (double)MathHelper.a(var16 * var16 + var18 * var18 + var20 * var20);
               if(var37 != 0.0D) {
                  var16 /= var37;
                  var18 /= var37;
                  var20 /= var37;
                  double var38 = (double)this.d.a(var11, var13.bk());
                  double var26 = (1.0D - var36) * var38;
                  var13.a(DamageSource.a(this), (float)((int)((var26 * var26 + var26) / 2.0D * 7.0D * (double)var31 + 1.0D)));
                  double var28 = 1.0D;
                  if(var13 instanceof class_rz) {
                     var28 = class_agx.a((class_rz)var13, var26);
                  }

                  var13.motX += var16 * var28;
                  var13.motY += var18 * var28;
                  var13.motZ += var20 * var28;
                  if(var13 instanceof EntityHuman) {
                     EntityHuman var30 = (EntityHuman)var13;
                     if(!var30.y() && (!var30.l_() || !var30.abilities.b)) {
                        this.k.put(var30, new Vec3D(var16 * var26, var18 * var26, var20 * var26));
                     }
                  }
               }
            }
         }
      }

   }

   public void a(boolean var1) {
      this.d.a((EntityHuman)null, this.e, this.f, this.g, class_ng.bB, EnumSoundCategory.BLOCKS, 4.0F, (1.0F + (this.d.r.nextFloat() - this.d.r.nextFloat()) * 0.2F) * 0.7F);
      if(this.i >= 2.0F && this.b) {
         this.d.a(EnumParticle.EXPLOSION_HUGE, this.e, this.f, this.g, 1.0D, 0.0D, 0.0D, new int[0]);
      } else {
         this.d.a(EnumParticle.EXPLOSION_LARGE, this.e, this.f, this.g, 1.0D, 0.0D, 0.0D, new int[0]);
      }

      Iterator var2;
      BlockPosition var3;
      if(this.b) {
         var2 = this.j.iterator();

         while(var2.hasNext()) {
            var3 = (BlockPosition)var2.next();
            IBlockData var4 = this.d.getType(var3);
            Block var5 = var4.getBlock();
            if(var1) {
               double var6 = (double)((float)var3.p() + this.d.r.nextFloat());
               double var8 = (double)((float)var3.q() + this.d.r.nextFloat());
               double var10 = (double)((float)var3.r() + this.d.r.nextFloat());
               double var12 = var6 - this.e;
               double var14 = var8 - this.f;
               double var16 = var10 - this.g;
               double var18 = (double)MathHelper.a(var12 * var12 + var14 * var14 + var16 * var16);
               var12 /= var18;
               var14 /= var18;
               var16 /= var18;
               double var20 = 0.5D / (var18 / (double)this.i + 0.1D);
               var20 *= (double)(this.d.r.nextFloat() * this.d.r.nextFloat() + 0.3F);
               var12 *= var20;
               var14 *= var20;
               var16 *= var20;
               this.d.a(EnumParticle.EXPLOSION_NORMAL, (var6 + this.e) / 2.0D, (var8 + this.f) / 2.0D, (var10 + this.g) / 2.0D, var12, var14, var16, new int[0]);
               this.d.a(EnumParticle.SMOKE_NORMAL, var6, var8, var10, var12, var14, var16, new int[0]);
            }

            if(var4.getMaterial() != Material.a) {
               if(var5.a(this)) {
                  var5.a(this.d, var3, this.d.getType(var3), 1.0F / this.i, 0);
               }

               this.d.a((BlockPosition)var3, (IBlockData)Blocks.AIR.u(), 3);
               var5.a(this.d, var3, this);
            }
         }
      }

      if(this.a) {
         var2 = this.j.iterator();

         while(var2.hasNext()) {
            var3 = (BlockPosition)var2.next();
            if(this.d.getType(var3).getMaterial() == Material.a && this.d.getType(var3.b()).b() && this.c.nextInt(3) == 0) {
               this.d.a(var3, Blocks.ab.u());
            }
         }
      }

   }

   public Map b() {
      return this.k;
   }

   public class_rz c() {
      return this.h == null?null:(this.h instanceof class_yd?((class_yd)this.h).j():(this.h instanceof class_rz?(class_rz)this.h:null));
   }

   public void d() {
      this.j.clear();
   }

   public List e() {
      return this.j;
   }
}
