package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.World;
import net.minecraft.server.class_ayo;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_vd;

public class class_vg extends class_vd {
   private BlockPosition f;

   public class_vg(EntityInsentient var1, World var2) {
      super(var1, var2);
   }

   public class_ayo a(BlockPosition var1) {
      this.f = var1;
      return super.a(var1);
   }

   public class_ayo a(Entity var1) {
      this.f = new BlockPosition(var1);
      return super.a(var1);
   }

   public boolean a(Entity var1, double var2) {
      class_ayo var4 = this.a(var1);
      if(var4 != null) {
         return this.a(var4, var2);
      } else {
         this.f = new BlockPosition(var1);
         this.d = var2;
         return true;
      }
   }

   public void l() {
      if(!this.n()) {
         super.l();
      } else {
         if(this.f != null) {
            double var1 = (double)(this.a.width * this.a.width);
            if(this.a.d((BlockPosition)this.f) >= var1 && (this.a.locY <= (double)this.f.q() || this.a.d((BlockPosition)(new BlockPosition(this.f.p(), MathHelper.c(this.a.locY), this.f.r()))) >= var1)) {
               this.a.u().a((double)this.f.p(), (double)this.f.q(), (double)this.f.r(), this.d);
            } else {
               this.f = null;
            }
         }

      }
   }
}
