package net.minecraft.server;

import net.minecraft.server.class_kf;

public class class_ke {
   private final int a;
   private final class_kf b;

   public class_ke(int var1, class_kf var2) {
      this.a = var1;
      this.b = var2;
   }

   public int a() {
      return this.a;
   }

   public class_kf b() {
      return this.b;
   }

   public boolean equals(Object var1) {
      if(this == var1) {
         return true;
      } else if(var1 != null && this.getClass() == var1.getClass()) {
         class_ke var2 = (class_ke)var1;
         return this.a == var2.a;
      } else {
         return false;
      }
   }

   public int hashCode() {
      return this.a;
   }
}
