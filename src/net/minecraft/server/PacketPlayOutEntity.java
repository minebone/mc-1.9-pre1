package net.minecraft.server;

import java.io.IOException;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketListenerPlayOut;

/*TODO: Packet Changed*/
public class PacketPlayOutEntity implements Packet {
   protected int a;
   protected int b;
   protected int c;
   protected int d;
   protected byte e;
   protected byte f;
   protected boolean g;
   protected boolean h;

   public PacketPlayOutEntity() {
   }

   public PacketPlayOutEntity(int var1) {
      this.a = var1;
   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.readVarInt();
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.writeVarInt(this.a);
   }

   public void a(PacketListenerPlayOut var1) {
      var1.a(this);
   }

   public String toString() {
      return "Entity_" + super.toString();
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketListenerPlayOut)var1);
   }

   public static class class_c_in_class_gu extends PacketPlayOutEntity {
      public class_c_in_class_gu() {
         this.h = true;
      }

      public class_c_in_class_gu(int var1, byte var2, byte var3, boolean var4) {
         super(var1);
         this.e = var2;
         this.f = var3;
         this.h = true;
         this.g = var4;
      }

      public void decode(PacketDataSerializer var1) throws IOException {
         super.decode(var1);
         this.e = var1.readByte();
         this.f = var1.readByte();
         this.g = var1.readBoolean();
      }

      public void encode(PacketDataSerializer var1) throws IOException {
         super.encode(var1);
         var1.writeByte(this.e);
         var1.writeByte(this.f);
         var1.writeBoolean(this.g);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void handle(PacketListener var1) {
         super.a((PacketListenerPlayOut)var1);
      }
   }

   public static class class_a_in_class_gu extends PacketPlayOutEntity {
      public class_a_in_class_gu() {
      }

      public class_a_in_class_gu(int var1, long var2, long var4, long var6, boolean var8) {
         super(var1);
         this.b = (int)var2;
         this.c = (int)var4;
         this.d = (int)var6;
         this.g = var8;
      }

      public void decode(PacketDataSerializer var1) throws IOException {
         super.decode(var1);
         this.b = var1.readShort();
         this.c = var1.readShort();
         this.d = var1.readShort();
         this.g = var1.readBoolean();
      }

      public void encode(PacketDataSerializer var1) throws IOException {
         super.encode(var1);
         var1.writeShort(this.b);
         var1.writeShort(this.c);
         var1.writeShort(this.d);
         var1.writeBoolean(this.g);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void handle(PacketListener var1) {
         super.a((PacketListenerPlayOut)var1);
      }
   }

   public static class class_b_in_class_gu extends PacketPlayOutEntity {
      public class_b_in_class_gu() {
         this.h = true;
      }

      public class_b_in_class_gu(int var1, long var2, long var4, long var6, byte var8, byte var9, boolean var10) {
         super(var1);
         this.b = (int)var2;
         this.c = (int)var4;
         this.d = (int)var6;
         this.e = var8;
         this.f = var9;
         this.g = var10;
         this.h = true;
      }

      public void decode(PacketDataSerializer var1) throws IOException {
         super.decode(var1);
         this.b = var1.readShort();
         this.c = var1.readShort();
         this.d = var1.readShort();
         this.e = var1.readByte();
         this.f = var1.readByte();
         this.g = var1.readBoolean();
      }

      public void encode(PacketDataSerializer var1) throws IOException {
         super.encode(var1);
         var1.writeShort(this.b);
         var1.writeShort(this.c);
         var1.writeShort(this.d);
         var1.writeByte(this.e);
         var1.writeByte(this.f);
         var1.writeBoolean(this.g);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void handle(PacketListener var1) {
         super.a((PacketListenerPlayOut)var1);
      }
   }
}
