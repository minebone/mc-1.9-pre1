package net.minecraft.server;

import com.google.common.base.Predicate;
import net.minecraft.server.Block;
import net.minecraft.server.BlockLeaves;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.BlockWood;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.BlockPosition;

public class class_amy extends BlockLeaves {
   public static final BlockStateEnum e = BlockStateEnum.a("variant", BlockWood.EnumLogVariant.class, new Predicate() {
      public boolean a(BlockWood.EnumLogVariant var1) {
         return var1.a() >= 4;
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((BlockWood.EnumLogVariant)var1);
      }
   });

   public class_amy() {
      this.w(this.A.b().set(e, BlockWood.EnumLogVariant.ACACIA).set(b, Boolean.valueOf(true)).set(a, Boolean.valueOf(true)));
   }

   protected void a(World var1, BlockPosition var2, IBlockData var3, int var4) {
      if(var3.get(e) == BlockWood.EnumLogVariant.DARK_OAK && var1.r.nextInt(var4) == 0) {
         a(var1, var2, new ItemStack(Items.e));
      }

   }

   public int d(IBlockData var1) {
      return ((BlockWood.EnumLogVariant)var1.get(e)).a();
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(this, 1, var3.getBlock().e(var3) & 3);
   }

   protected ItemStack u(IBlockData var1) {
      return new ItemStack(Item.a((Block)this), 1, ((BlockWood.EnumLogVariant)var1.get(e)).a() - 4);
   }

   public IBlockData a(int var1) {
      return this.u().set(e, this.e(var1)).set(a, Boolean.valueOf((var1 & 4) == 0)).set(b, Boolean.valueOf((var1 & 8) > 0));
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((BlockWood.EnumLogVariant)var1.get(e)).a() - 4;
      if(!((Boolean)var1.get(a)).booleanValue()) {
         var3 |= 4;
      }

      if(((Boolean)var1.get(b)).booleanValue()) {
         var3 |= 8;
      }

      return var3;
   }

   public BlockWood.EnumLogVariant e(int var1) {
      return BlockWood.EnumLogVariant.a((var1 & 3) + 4);
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{e, b, a});
   }

   public void a(World var1, EntityHuman var2, BlockPosition var3, IBlockData var4, TileEntity var5, ItemStack var6) {
      if(!var1.E && var6 != null && var6.b() == Items.bl) {
         var2.b(StatisticList.a((Block)this));
         a(var1, var3, new ItemStack(Item.a((Block)this), 1, ((BlockWood.EnumLogVariant)var4.get(e)).a() - 4));
      } else {
         super.a(var1, var2, var3, var4, var5, var6);
      }
   }
}
