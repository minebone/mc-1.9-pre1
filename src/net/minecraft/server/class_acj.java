package net.minecraft.server;

import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_wb;

public class class_acj extends Item {
   public class_acj() {
      this.a(CreativeModeTab.e);
      this.d(1);
      this.e(25);
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      if(var3.aH() && var3.bx() instanceof class_wb) {
         class_wb var5 = (class_wb)var3.bx();
         if(var1.j() - var1.i() >= 7 && var5.da()) {
            var1.a(7, (class_rz)var3);
            if(var1.b == 0) {
               ItemStack var6 = new ItemStack(Items.aY);
               var6.d(var1.o());
               return new class_qo(EnumResult.SUCCESS, var6);
            }

            return new class_qo(EnumResult.SUCCESS, var1);
         }
      }

      var3.b(StatisticList.b((Item)this));
      return new class_qo(EnumResult.PASS, var1);
   }
}
