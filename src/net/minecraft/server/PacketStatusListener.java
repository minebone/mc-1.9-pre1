package net.minecraft.server;

import net.minecraft.server.ChatComponentText;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NetworkManager;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketStatusOutPong;
import net.minecraft.server.PacketStatusOutServerInfo;
import net.minecraft.server.PacketStatusInListener;
import net.minecraft.server.PacketStatusInPing;
import net.minecraft.server.PacketStatusInStart;

public class PacketStatusListener implements PacketStatusInListener {
	
   private static final IChatBaseComponent a = new ChatComponentText("Status request has been handled.");
   private final MinecraftServer b;
   private final NetworkManager c;
   private boolean d;

   public PacketStatusListener(MinecraftServer var1, NetworkManager var2) {
      this.b = var1;
      this.c = var2;
   }

   public void disconnect(IChatBaseComponent var1) {
   }

   public void a(PacketStatusInStart var1) {
      if(this.d) {
         this.c.a(a);
      } else {
         this.d = true;
         this.c.a((Packet)(new PacketStatusOutServerInfo(this.b.aB())));
      }
   }

   public void a(PacketStatusInPing var1) {
      this.c.a((Packet)(new PacketStatusOutPong(var1.a())));
      this.c.a(a);
   }
}
