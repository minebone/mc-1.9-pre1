package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.World;
import net.minecraft.server.class_azs;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_wh;
import net.minecraft.server.class_ys;

public class class_wf extends class_wh {
   public float a;
   public float b;
   public float c;
   public float bt;
   public float bu;
   public float bv;
   public float bw;
   public float bx;
   private float by;
   private float bz;
   private float bA;
   private float bB;
   private float bC;
   private float bD;

   public class_wf(World var1) {
      super(var1);
      this.a(0.8F, 0.8F);
      this.random.setSeed((long)(1 + this.getId()));
      this.bz = 1.0F / (this.random.nextFloat() + 1.0F) * 0.2F;
   }

   protected void r() {
      this.bp.a(0, new class_wf.class_a_in_class_wf(this));
   }

   protected void bz() {
      super.bz();
      this.a(class_ys.a).a(10.0D);
   }

   public float bm() {
      return this.length * 0.5F;
   }

   protected class_nf G() {
      return class_ng.fQ;
   }

   protected class_nf bQ() {
      return class_ng.fS;
   }

   protected class_nf bR() {
      return class_ng.fR;
   }

   protected float cc() {
      return 0.4F;
   }

   protected boolean ad() {
      return false;
   }

   protected class_kk J() {
      return class_azs.af;
   }

   public boolean ah() {
      return super.ah();
   }

   public void n() {
      super.n();
      this.b = this.a;
      this.bt = this.c;
      this.bv = this.bu;
      this.bx = this.bw;
      this.bu += this.bz;
      if((double)this.bu > 6.283185307179586D) {
         if(this.world.E) {
            this.bu = 6.2831855F;
         } else {
            this.bu = (float)((double)this.bu - 6.283185307179586D);
            if(this.random.nextInt(10) == 0) {
               this.bz = 1.0F / (this.random.nextFloat() + 1.0F) * 0.2F;
            }

            this.world.a((Entity)this, (byte)19);
         }
      }

      if(this.inWater) {
         float var1;
         if(this.bu < 3.1415927F) {
            var1 = this.bu / 3.1415927F;
            this.bw = MathHelper.a(var1 * var1 * 3.1415927F) * 3.1415927F * 0.25F;
            if((double)var1 > 0.75D) {
               this.by = 1.0F;
               this.bA = 1.0F;
            } else {
               this.bA *= 0.8F;
            }
         } else {
            this.bw = 0.0F;
            this.by *= 0.9F;
            this.bA *= 0.99F;
         }

         if(!this.world.E) {
            this.motX = (double)(this.bB * this.by);
            this.motY = (double)(this.bC * this.by);
            this.motZ = (double)(this.bD * this.by);
         }

         var1 = MathHelper.a(this.motX * this.motX + this.motZ * this.motZ);
         this.aM += (-((float)MathHelper.b(this.motX, this.motZ)) * 57.295776F - this.aM) * 0.1F;
         this.yaw = this.aM;
         this.c = (float)((double)this.c + 3.141592653589793D * (double)this.bA * 1.5D);
         this.a += (-((float)MathHelper.b((double)var1, this.motY)) * 57.295776F - this.a) * 0.1F;
      } else {
         this.bw = MathHelper.e(MathHelper.a(this.bu)) * 3.1415927F * 0.25F;
         if(!this.world.E) {
            this.motX = 0.0D;
            this.motZ = 0.0D;
            if(this.a(MobEffectList.y)) {
               this.motY += 0.05D * (double)(this.b(MobEffectList.y).c() + 1) - this.motY;
            } else {
               this.motY -= 0.08D;
            }

            this.motY *= 0.9800000190734863D;
         }

         this.a = (float)((double)this.a + (double)(-90.0F - this.a) * 0.02D);
      }

   }

   public void g(float var1, float var2) {
      this.d(this.motX, this.motY, this.motZ);
   }

   public boolean cF() {
      return this.locY > 45.0D && this.locY < (double)this.world.K() && super.cF();
   }

   public void b(float var1, float var2, float var3) {
      this.bB = var1;
      this.bC = var2;
      this.bD = var3;
   }

   public boolean o() {
      return this.bB != 0.0F || this.bC != 0.0F || this.bD != 0.0F;
   }

   static class class_a_in_class_wf extends class_tj {
      private class_wf a;

      public class_a_in_class_wf(class_wf var1) {
         this.a = var1;
      }

      public boolean a() {
         return true;
      }

      public void e() {
         int var1 = this.a.bJ();
         if(var1 > 100) {
            this.a.b(0.0F, 0.0F, 0.0F);
         } else if(this.a.bE().nextInt(50) == 0 || !this.a.inWater || !this.a.o()) {
            float var2 = this.a.bE().nextFloat() * 6.2831855F;
            float var3 = MathHelper.b(var2) * 0.2F;
            float var4 = -0.1F + this.a.bE().nextFloat() * 0.2F;
            float var5 = MathHelper.a(var2) * 0.2F;
            this.a.b(var3, var4, var5);
         }

      }
   }
}
