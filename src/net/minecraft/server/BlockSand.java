package net.minecraft.server;

import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.class_alg;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.class_or;

public class BlockSand extends class_alg {
   public static final BlockStateEnum a = BlockStateEnum.a("variant", BlockSand.EnumSandVariant.class);

   public BlockSand() {
      this.w(this.A.b().set(a, BlockSand.EnumSandVariant.SAND));
   }

   public int d(IBlockData var1) {
      return ((BlockSand.EnumSandVariant)var1.get(a)).a();
   }

   public MaterialMapColor r(IBlockData var1) {
      return ((BlockSand.EnumSandVariant)var1.get(a)).c();
   }

   public IBlockData a(int var1) {
      return this.u().set(a, BlockSand.EnumSandVariant.a(var1));
   }

   public int e(IBlockData var1) {
      return ((BlockSand.EnumSandVariant)var1.get(a)).a();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   public static enum EnumSandVariant implements class_or {
      SAND(0, "sand", "default", MaterialMapColor.d),
      RED_SAND(1, "red_sand", "red", MaterialMapColor.q);

      private static final BlockSand.EnumSandVariant[] c = new BlockSand.EnumSandVariant[values().length];
      private final int d;
      private final String e;
      private final MaterialMapColor f;
      private final String g;

      private EnumSandVariant(int var3, String var4, String var5, MaterialMapColor var6) {
         this.d = var3;
         this.e = var4;
         this.f = var6;
         this.g = var5;
      }

      public int a() {
         return this.d;
      }

      public String toString() {
         return this.e;
      }

      public MaterialMapColor c() {
         return this.f;
      }

      public static BlockSand.EnumSandVariant a(int var0) {
         if(var0 < 0 || var0 >= c.length) {
            var0 = 0;
         }

         return c[var0];
      }

      public String m() {
         return this.e;
      }

      public String d() {
         return this.g;
      }

      static {
         BlockSand.EnumSandVariant[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            BlockSand.EnumSandVariant var3 = var0[var2];
            c[var3.a()] = var3;
         }

      }
   }
}
