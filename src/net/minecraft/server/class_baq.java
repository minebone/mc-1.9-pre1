package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import java.util.Random;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_kk;

public interface class_baq {
   boolean a(Random var1, class_azy var2);

   public abstract static class class_a_in_class_baq {
      private final class_kk a;
      private final Class b;

      protected class_a_in_class_baq(class_kk var1, Class var2) {
         this.a = var1;
         this.b = var2;
      }

      public class_kk a() {
         return this.a;
      }

      public Class b() {
         return this.b;
      }

      public abstract void a(JsonObject var1, class_baq var2, JsonSerializationContext var3);

      public abstract class_baq b(JsonObject var1, JsonDeserializationContext var2);
   }
}
