package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockLeaves;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.class_ane;
import net.minecraft.server.class_anf;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ato;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_avc extends class_ato {
   private static final IBlockData a = Blocks.r.u().set(class_anf.b, BlockWood.EnumLogVariant.SPRUCE);
   private static final IBlockData b = Blocks.t.u().set(class_ane.e, BlockWood.EnumLogVariant.SPRUCE).set(BlockLeaves.b, Boolean.valueOf(false));

   public class_avc(boolean var1) {
      super(var1);
   }

   public boolean b(World var1, Random var2, BlockPosition var3) {
      int var4 = var2.nextInt(4) + 6;
      int var5 = 1 + var2.nextInt(2);
      int var6 = var4 - var5;
      int var7 = 2 + var2.nextInt(2);
      boolean var8 = true;
      if(var3.q() >= 1 && var3.q() + var4 + 1 <= 256) {
         int var13;
         int var21;
         for(int var9 = var3.q(); var9 <= var3.q() + 1 + var4 && var8; ++var9) {
            boolean var10 = true;
            if(var9 - var3.q() < var5) {
               var21 = 0;
            } else {
               var21 = var7;
            }

            BlockPosition.class_a_in_class_cj var11 = new BlockPosition.class_a_in_class_cj();

            for(int var12 = var3.p() - var21; var12 <= var3.p() + var21 && var8; ++var12) {
               for(var13 = var3.r() - var21; var13 <= var3.r() + var21 && var8; ++var13) {
                  if(var9 >= 0 && var9 < 256) {
                     Material var14 = var1.getType(var11.c(var12, var9, var13)).getMaterial();
                     if(var14 != Material.a && var14 != Material.j) {
                        var8 = false;
                     }
                  } else {
                     var8 = false;
                  }
               }
            }
         }

         if(!var8) {
            return false;
         } else {
            Block var20 = var1.getType(var3.b()).getBlock();
            if((var20 == Blocks.c || var20 == Blocks.d || var20 == Blocks.ak) && var3.q() < 256 - var4 - 1) {
               this.a(var1, var3.b());
               var21 = var2.nextInt(2);
               int var22 = 1;
               byte var23 = 0;

               int var24;
               for(var13 = 0; var13 <= var6; ++var13) {
                  var24 = var3.q() + var4 - var13;

                  for(int var15 = var3.p() - var21; var15 <= var3.p() + var21; ++var15) {
                     int var16 = var15 - var3.p();

                     for(int var17 = var3.r() - var21; var17 <= var3.r() + var21; ++var17) {
                        int var18 = var17 - var3.r();
                        if(Math.abs(var16) != var21 || Math.abs(var18) != var21 || var21 <= 0) {
                           BlockPosition var19 = new BlockPosition(var15, var24, var17);
                           if(!var1.getType(var19).b()) {
                              this.a(var1, var19, b);
                           }
                        }
                     }
                  }

                  if(var21 >= var22) {
                     var21 = var23;
                     var23 = 1;
                     ++var22;
                     if(var22 > var7) {
                        var22 = var7;
                     }
                  } else {
                     ++var21;
                  }
               }

               var13 = var2.nextInt(3);

               for(var24 = 0; var24 < var4 - var13; ++var24) {
                  Material var25 = var1.getType(var3.b(var24)).getMaterial();
                  if(var25 == Material.a || var25 == Material.j) {
                     this.a(var1, var3.b(var24), a);
                  }
               }

               return true;
            } else {
               return false;
            }
         }
      } else {
         return false;
      }
   }
}
