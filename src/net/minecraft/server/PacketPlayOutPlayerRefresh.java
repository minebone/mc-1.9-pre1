package net.minecraft.server;

import java.io.IOException;
import java.util.List;
import net.minecraft.server.Entity;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketListenerPlayOut;

public class PacketPlayOutPlayerRefresh implements Packet {
	
   private int a;
   private int[] b;

   public PacketPlayOutPlayerRefresh() {
   }

   public PacketPlayOutPlayerRefresh(Entity var1) {
      this.a = var1.getId();
      List var2 = var1.bt();
      this.b = new int[var2.size()];

      for(int var3 = 0; var3 < var2.size(); ++var3) {
         this.b[var3] = ((Entity)var2.get(var3)).getId();
      }

   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.readVarInt();
      this.b = var1.b();
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.writeVarInt(this.a);
      var1.a(this.b);
   }

   public void a(PacketListenerPlayOut var1) {
      var1.a(this);
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketListenerPlayOut)var1);
   }
}
