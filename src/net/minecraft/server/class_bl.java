package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EntityTypes;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MojangsonParser;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.class_asr;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ec;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.class_qk;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_xz;

public class class_bl extends CommandAbstract {
   public String c() {
      return "summon";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.summon.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 1) {
         throw new class_cf("commands.summon.usage", new Object[0]);
      } else {
         String var4 = var3[0];
         BlockPosition var5 = var2.c();
         Vec3D var6 = var2.d();
         double var7 = var6.b;
         double var9 = var6.c;
         double var11 = var6.d;
         if(var3.length >= 4) {
            var7 = b(var7, var3[1], true);
            var9 = b(var9, var3[2], false);
            var11 = b(var11, var3[3], true);
            var5 = new BlockPosition(var7, var9, var11);
         }

         World var13 = var2.e();
         if(!var13.e(var5)) {
            throw new class_bz("commands.summon.outOfWorld", new Object[0]);
         } else if("LightningBolt".equals(var4)) {
            var13.d((Entity)(new class_xz(var13, var7, var9, var11, false)));
            a(var2, this, "commands.summon.success", new Object[0]);
         } else {
            NBTTagCompound var14 = new NBTTagCompound();
            boolean var15 = false;
            if(var3.length >= 5) {
               IChatBaseComponent var16 = a(var2, var3, 4);

               try {
                  var14 = MojangsonParser.a(var16.c());
                  var15 = true;
               } catch (class_ec var18) {
                  throw new class_bz("commands.summon.tagError", new Object[]{var18.getMessage()});
               }
            }

            var14.a("id", var4);
            Entity var19 = class_asr.a(var14, var13, var7, var9, var11, true);
            if(var19 == null) {
               throw new class_bz("commands.summon.failed", new Object[0]);
            } else {
               var19.b(var7, var9, var11, var19.yaw, var19.pitch);
               if(!var15 && var19 instanceof EntityInsentient) {
                  ((EntityInsentient)var19).a((class_qk)var13.D(new BlockPosition(var19)), (class_sc)null);
               }

               a(var2, this, "commands.summon.success", new Object[0]);
            }
         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, EntityTypes.b()):(var3.length > 1 && var3.length <= 4?a(var3, 1, var4):Collections.emptyList());
   }
}
