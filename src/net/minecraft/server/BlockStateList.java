package net.minecraft.server;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockDataAbstract;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aoa;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.class_axg;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_cm;
import net.minecraft.server.class_cw;

public class BlockStateList {
   private static final Pattern a = Pattern.compile("^[a-z0-9_]+$");
   private static final Function b = new Function() {
      public String a(IBlockState var1) {
         return var1 == null?"<NULL>":var1.a();
      }

      // $FF: synthetic method
      public Object apply(Object var1) {
         return this.a((IBlockState)var1);
      }
   };
   private final Block c;
   private final ImmutableSortedMap d;
   private final ImmutableList e;

   public BlockStateList(Block var1, IBlockState... var2) {
      this.c = var1;
      HashMap var3 = Maps.newHashMap();
      IBlockState[] var4 = var2;
      int var5 = var2.length;

      for(int var6 = 0; var6 < var5; ++var6) {
         IBlockState var7 = var4[var6];
         a(var1, var7);
         var3.put(var7.a(), var7);
      }

      this.d = ImmutableSortedMap.copyOf(var3);
      LinkedHashMap var11 = Maps.newLinkedHashMap();
      ArrayList var12 = Lists.newArrayList();
      Iterable var13 = class_cm.a(this.e());
      Iterator var14 = var13.iterator();

      while(var14.hasNext()) {
         List var8 = (List)var14.next();
         Map var9 = class_cw.b(this.d.values(), var8);
         BlockStateList.BlockData var10 = new BlockStateList.BlockData(var1, ImmutableMap.copyOf(var9), null);
         var11.put(var9, var10);
         var12.add(var10);
      }

      var14 = var12.iterator();

      while(var14.hasNext()) {
         BlockStateList.BlockData var15 = (BlockStateList.BlockData)var14.next();
         var15.a((Map)var11);
      }

      this.e = ImmutableList.copyOf((Collection)var12);
   }

   public static String a(Block var0, IBlockState var1) {
      String var2 = var1.a();
      if(!a.matcher(var2).matches()) {
         throw new IllegalArgumentException("Block: " + var0.getClass() + " has invalidly named property: " + var2);
      } else {
         Iterator var3 = var1.c().iterator();

         String var5;
         do {
            if(!var3.hasNext()) {
               return var2;
            }

            Comparable var4 = (Comparable)var3.next();
            var5 = var1.a(var4);
         } while(a.matcher(var5).matches());

         throw new IllegalArgumentException("Block: " + var0.getClass() + " has property: " + var2 + " with invalidly named value: " + var5);
      }
   }

   public ImmutableList a() {
      return this.e;
   }

   private List e() {
      ArrayList var1 = Lists.newArrayList();
      ImmutableCollection var2 = this.d.values();
      Iterator var3 = var2.iterator();

      while(var3.hasNext()) {
         IBlockState var4 = (IBlockState)var3.next();
         var1.add(var4.c());
      }

      return var1;
   }

   public IBlockData b() {
      return (IBlockData)this.e.get(0);
   }

   public Block c() {
      return this.c;
   }

   public Collection d() {
      return this.d.values();
   }

   public String toString() {
      return Objects.toStringHelper((Object)this).add("block", Block.h.b(this.c)).add("properties", Iterables.transform(this.d.values(), b)).toString();
   }

   static class BlockData extends BlockDataAbstract {
      private final Block a;
      private final ImmutableMap b;
      private ImmutableTable c;

      private BlockData(Block var1, ImmutableMap var2) {
         this.a = var1;
         this.b = var2;
      }

      public Collection r() {
         return Collections.unmodifiableCollection(this.b.keySet());
      }

      public Comparable get(IBlockState var1) {
         if(!this.b.containsKey(var1)) {
            throw new IllegalArgumentException("Cannot get property " + var1 + " as it does not exist in " + this.a.t());
         } else {
            return (Comparable)var1.b().cast(this.b.get(var1));
         }
      }

      public IBlockData set(IBlockState var1, Comparable var2) {
         if(!this.b.containsKey(var1)) {
            throw new IllegalArgumentException("Cannot set property " + var1 + " as it does not exist in " + this.a.t());
         } else if(!var1.c().contains(var2)) {
            throw new IllegalArgumentException("Cannot set property " + var1 + " to " + var2 + " on block " + Block.h.b(this.a) + ", it is not an allowed value");
         } else {
            return (IBlockData)(this.b.get(var1) == var2?this:(IBlockData)this.c.get(var1, var2));
         }
      }

      public ImmutableMap s() {
         return this.b;
      }

      public Block getBlock() {
         return this.a;
      }

      public boolean equals(Object var1) {
         return this == var1;
      }

      public int hashCode() {
         return this.b.hashCode();
      }

      public void a(Map var1) {
         if(this.c != null) {
            throw new IllegalStateException();
         } else {
            HashBasedTable var2 = HashBasedTable.create();
            Iterator var3 = this.b.entrySet().iterator();

            while(var3.hasNext()) {
               Entry var4 = (Entry)var3.next();
               IBlockState var5 = (IBlockState)var4.getKey();
               Iterator var6 = var5.c().iterator();

               while(var6.hasNext()) {
                  Comparable var7 = (Comparable)var6.next();
                  if(var7 != var4.getValue()) {
                     var2.put(var5, var7, var1.get(this.b(var5, var7)));
                  }
               }
            }

            this.c = ImmutableTable.copyOf(var2);
         }
      }

      private Map b(IBlockState var1, Comparable var2) {
         HashMap var3 = Maps.newHashMap(this.b);
         var3.put(var1, var2);
         return var3;
      }

      public Material getMaterial() {
         return this.a.q(this);
      }

      public boolean b() {
         return this.a.l(this);
      }

      public int c() {
         return this.a.m(this);
      }

      public int d() {
         return this.a.o(this);
      }

      public boolean f() {
         return this.a.p(this);
      }

      public MaterialMapColor g() {
         return this.a.r(this);
      }

      public IBlockData a(class_aod var1) {
         return this.a.a((IBlockData)this, (class_aod)var1);
      }

      public IBlockData a(class_amq var1) {
         return this.a.a((IBlockData)this, (class_amq)var1);
      }

      public boolean h() {
         return this.a.c((IBlockData)this);
      }

      public class_aoa i() {
         return this.a.a((IBlockData)this);
      }

      public boolean k() {
         return this.a.s(this);
      }

      public boolean l() {
         return this.a.t(this);
      }

      public boolean m() {
         return this.a.g(this);
      }

      public int a(class_ahw var1, BlockPosition var2, EnumDirection var3) {
         return this.a.b((IBlockData)this, (class_ahw)var1, (BlockPosition)var2, (EnumDirection)var3);
      }

      public boolean n() {
         return this.a.v(this);
      }

      public int a(World var1, BlockPosition var2) {
         return this.a.d((IBlockData)this, (World)var1, (BlockPosition)var2);
      }

      public float b(World var1, BlockPosition var2) {
         return this.a.b((IBlockData)this, (World)var1, (BlockPosition)var2);
      }

      public float a(EntityHuman var1, World var2, BlockPosition var3) {
         return this.a.a((IBlockData)this, (EntityHuman)var1, (World)var2, (BlockPosition)var3);
      }

      public int b(class_ahw var1, BlockPosition var2, EnumDirection var3) {
         return this.a.c(this, var1, var2, var3);
      }

      public class_axg o() {
         return this.a.h(this);
      }

      public IBlockData b(class_ahw var1, BlockPosition var2) {
         return this.a.b((IBlockData)this, (class_ahw)var1, (BlockPosition)var2);
      }

      public boolean p() {
         return this.a.b((IBlockData)this);
      }

      public AxisAlignedBB d(World var1, BlockPosition var2) {
         return this.a.a((IBlockData)this, (World)var1, (BlockPosition)var2);
      }

      public void a(World var1, BlockPosition var2, AxisAlignedBB var3, List var4, Entity var5) {
         this.a.a((IBlockData)this, (World)var1, var2, (AxisAlignedBB)var3, (List)var4, (Entity)var5);
      }

      public AxisAlignedBB c(class_ahw var1, BlockPosition var2) {
         return this.a.a((IBlockData)this, (class_ahw)var1, (BlockPosition)var2);
      }

      public MovingObjectPosition a(World var1, BlockPosition var2, Vec3D var3, Vec3D var4) {
         return this.a.a((IBlockData)this, (World)var1, (BlockPosition)var2, (Vec3D)var3, (Vec3D)var4);
      }

      public boolean q() {
         return this.a.k(this);
      }

      // $FF: synthetic method
      BlockData(Block var1, ImmutableMap var2, Object var3) {
         this(var1, var2);
      }
   }
}
