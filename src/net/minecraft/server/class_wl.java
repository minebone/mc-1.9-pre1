package net.minecraft.server;

import net.minecraft.server.ChatMessage;
import net.minecraft.server.class_azs;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;

public enum class_wl {
   HORSE("horse", "horse_white", class_ng.ck, class_ng.cr, class_ng.co, class_azs.D),
   DONKEY("donkey", "donkey", class_ng.av, class_ng.az, class_ng.ay, class_azs.D),
   MULE("mule", "mule", class_ng.du, class_ng.dw, class_ng.dv, class_azs.D),
   ZOMBIE("zombiehorse", "horse_zombie", class_ng.he, class_ng.hg, class_ng.hf, class_azs.E),
   SKELETON("skeletonhorse", "horse_skeleton", class_ng.fe, class_ng.fg, class_ng.ff, class_azs.F);

   private final ChatMessage f;
   private final class_kk g;
   private final class_nf h;
   private final class_nf i;
   private final class_nf j;
   private class_kk k;

   private class_wl(String var3, String var4, class_nf var5, class_nf var6, class_nf var7, class_kk var8) {
      this.k = var8;
      this.f = new ChatMessage("entity." + var3 + ".name", new Object[0]);
      this.g = new class_kk("textures/entity/horse/" + var4 + ".png");
      this.h = var6;
      this.i = var5;
      this.j = var7;
   }

   public class_nf a() {
      return this.i;
   }

   public class_nf b() {
      return this.h;
   }

   public class_nf c() {
      return this.j;
   }

   public ChatMessage d() {
      return this.f;
   }

   public boolean f() {
      return this == DONKEY || this == MULE;
   }

   public boolean g() {
      return this == DONKEY || this == MULE;
   }

   public boolean h() {
      return this == ZOMBIE || this == SKELETON;
   }

   public boolean i() {
      return !this.h() && this != MULE;
   }

   public boolean j() {
      return this == HORSE;
   }

   public int k() {
      return this.ordinal();
   }

   public static class_wl a(int var0) {
      return values()[var0];
   }

   public class_kk l() {
      return this.k;
   }
}
