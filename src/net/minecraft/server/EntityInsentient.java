package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import net.minecraft.server.AchievementList;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntitySenses;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Item;
import net.minecraft.server.ItemArmor;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagFloat;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.NavigationAbstract;
import net.minecraft.server.PathfinderGoalSelector;
import net.minecraft.server.Statistic;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.EntityBoat;
import net.minecraft.server.class_acg;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aew;
import net.minecraft.server.class_agn;
import net.minecraft.server.class_ayl;
import net.minecraft.server.class_azx;
import net.minecraft.server.class_azy;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutAttachEntity;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_qk;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_st;
import net.minecraft.server.class_sv;
import net.minecraft.server.class_sw;
import net.minecraft.server.class_sx;
import net.minecraft.server.class_vd;
import net.minecraft.server.class_xq;
import net.minecraft.server.class_xs;
import net.minecraft.server.class_yc;
import net.minecraft.server.class_yk;
import net.minecraft.server.class_yl;
import net.minecraft.server.class_ys;

public abstract class EntityInsentient extends class_rz {
   private static final class_ke a = DataWatcher.a(EntityInsentient.class, class_kg.a);
   public int a_;
   protected int b_;
   private class_sw b;
   protected class_sx f;
   protected class_sv g;
   private class_st c;
   protected NavigationAbstract h;
   protected final PathfinderGoalSelector bp;
   protected final PathfinderGoalSelector bq;
   private class_rz bt;
   private EntitySenses bu;
   private ItemStack[] bv = new ItemStack[2];
   protected float[] br = new float[2];
   private ItemStack[] bw = new ItemStack[4];
   protected float[] bs = new float[4];
   private boolean bx;
   private boolean by;
   private Map bz = Maps.newEnumMap(class_ayl.class);
   private class_kk bA;
   private long bB;
   private boolean bC;
   private Entity bD;
   private NBTTagCompound bE;

   public EntityInsentient(World var1) {
      super(var1);
      this.bp = new PathfinderGoalSelector(var1 != null && var1.C != null?var1.C:null);
      this.bq = new PathfinderGoalSelector(var1 != null && var1.C != null?var1.C:null);
      this.b = new class_sw(this);
      this.f = new class_sx(this);
      this.g = new class_sv(this);
      this.c = this.s();
      this.h = this.b(var1);
      this.bu = new EntitySenses(this);

      int var2;
      for(var2 = 0; var2 < this.bs.length; ++var2) {
         this.bs[var2] = 0.085F;
      }

      for(var2 = 0; var2 < this.br.length; ++var2) {
         this.br[var2] = 0.085F;
      }

      if(var1 != null && !var1.E) {
         this.r();
      }

   }

   protected void r() {
   }

   protected void bz() {
      super.bz();
      this.bY().b(class_ys.b).a(16.0D);
   }

   protected NavigationAbstract b(World var1) {
      return new class_vd(this, var1);
   }

   public float a(class_ayl var1) {
      return this.bz.containsKey(var1)?((Float)this.bz.get(var1)).floatValue():var1.a();
   }

   public void a(class_ayl var1, float var2) {
      this.bz.put(var1, Float.valueOf(var2));
   }

   protected class_st s() {
      return new class_st(this);
   }

   public class_sw t() {
      return this.b;
   }

   public class_sx u() {
      return this.f;
   }

   public class_sv w() {
      return this.g;
   }

   public NavigationAbstract x() {
      return this.h;
   }

   public EntitySenses y() {
      return this.bu;
   }

   public class_rz A() {
      return this.bt;
   }

   public void c(class_rz var1) {
      this.bt = var1;
   }

   public boolean d(Class var1) {
      return var1 != class_yl.class;
   }

   public void B() {
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)a, (Object)Byte.valueOf((byte)0));
   }

   public int C() {
      return 80;
   }

   public void D() {
      class_nf var1 = this.G();
      if(var1 != null) {
         this.a(var1, this.cc(), this.cd());
      }

   }

   public void T() {
      super.T();
      this.world.C.a("mobBaseTick");
      if(this.at() && this.random.nextInt(1000) < this.a_++) {
         this.o();
         this.D();
      }

      this.world.C.b();
   }

   protected void c(DamageSource var1) {
      this.o();
      super.c(var1);
   }

   private void o() {
      this.a_ = -this.C();
   }

   protected int b(EntityHuman var1) {
      if(this.b_ > 0) {
         int var2 = this.b_;

         int var3;
         for(var3 = 0; var3 < this.bw.length; ++var3) {
            if(this.bw[var3] != null && this.bs[var3] <= 1.0F) {
               var2 += 1 + this.random.nextInt(3);
            }
         }

         for(var3 = 0; var3 < this.bv.length; ++var3) {
            if(this.bv[var3] != null && this.br[var3] <= 1.0F) {
               var2 += 1 + this.random.nextInt(3);
            }
         }

         return var2;
      } else {
         return this.b_;
      }
   }

   public void E() {
      if(this.world.E) {
         for(int var1 = 0; var1 < 20; ++var1) {
            double var2 = this.random.nextGaussian() * 0.02D;
            double var4 = this.random.nextGaussian() * 0.02D;
            double var6 = this.random.nextGaussian() * 0.02D;
            double var8 = 10.0D;
            this.world.a(EnumParticle.EXPLOSION_NORMAL, this.locX + (double)(this.random.nextFloat() * this.width * 2.0F) - (double)this.width - var2 * var8, this.locY + (double)(this.random.nextFloat() * this.length) - var4 * var8, this.locZ + (double)(this.random.nextFloat() * this.width * 2.0F) - (double)this.width - var6 * var8, var2, var4, var6, new int[0]);
         }
      } else {
         this.world.a((Entity)this, (byte)20);
      }

   }

   public void m() {
      super.m();
      if(!this.world.E) {
         this.cO();
         if(this.ticksLived % 5 == 0) {
            boolean var1 = !(this.bs() instanceof EntityInsentient);
            boolean var2 = !(this.bx() instanceof EntityBoat);
            this.bp.a(5, var1 && var2);
            this.bp.a(2, var1);
         }
      }

   }

   protected float h(float var1, float var2) {
      this.c.a();
      return var2;
   }

   protected class_nf G() {
      return null;
   }

   protected Item I() {
      return null;
   }

   protected void b(boolean var1, int var2) {
      Item var3 = this.I();
      if(var3 != null) {
         int var4 = this.random.nextInt(3);
         if(var2 > 0) {
            var4 += this.random.nextInt(var2 + 1);
         }

         for(int var5 = 0; var5 < var4; ++var5) {
            this.a(var3, 1);
         }
      }

   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("CanPickUpLoot", this.cM());
      var1.a("PersistenceRequired", this.by);
      NBTTagList var2 = new NBTTagList();

      for(int var3 = 0; var3 < this.bw.length; ++var3) {
         NBTTagCompound var4 = new NBTTagCompound();
         if(this.bw[var3] != null) {
            this.bw[var3].b(var4);
         }

         var2.a((NBTTag)var4);
      }

      var1.a((String)"ArmorItems", (NBTTag)var2);
      NBTTagList var8 = new NBTTagList();

      for(int var9 = 0; var9 < this.bv.length; ++var9) {
         NBTTagCompound var5 = new NBTTagCompound();
         if(this.bv[var9] != null) {
            this.bv[var9].b(var5);
         }

         var8.a((NBTTag)var5);
      }

      var1.a((String)"HandItems", (NBTTag)var8);
      NBTTagList var10 = new NBTTagList();

      for(int var11 = 0; var11 < this.bs.length; ++var11) {
         var10.a((NBTTag)(new NBTTagFloat(this.bs[var11])));
      }

      var1.a((String)"ArmorDropChances", (NBTTag)var10);
      NBTTagList var12 = new NBTTagList();

      for(int var6 = 0; var6 < this.br.length; ++var6) {
         var12.a((NBTTag)(new NBTTagFloat(this.br[var6])));
      }

      var1.a((String)"HandDropChances", (NBTTag)var12);
      var1.a("Leashed", this.bC);
      if(this.bD != null) {
         NBTTagCompound var13 = new NBTTagCompound();
         if(this.bD instanceof class_rz) {
            UUID var7 = this.bD.getUniqueId();
            var13.a("UUID", var7);
         } else if(this.bD instanceof class_xq) {
            BlockPosition var14 = ((class_xq)this.bD).q();
            var13.a("X", var14.p());
            var13.a("Y", var14.q());
            var13.a("Z", var14.r());
         }

         var1.a((String)"Leash", (NBTTag)var13);
      }

      var1.a("LeftHanded", this.cS());
      if(this.bA != null) {
         var1.a("DeathLootTable", this.bA.toString());
         if(this.bB != 0L) {
            var1.a("DeathLootTableSeed", this.bB);
         }
      }

      if(this.cR()) {
         var1.a("NoAI", this.cR());
      }

   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      if(var1.b("CanPickUpLoot", 1)) {
         this.l(var1.p("CanPickUpLoot"));
      }

      this.by = var1.p("PersistenceRequired");
      NBTTagList var2;
      int var3;
      if(var1.b("ArmorItems", 9)) {
         var2 = var1.c("ArmorItems", 10);

         for(var3 = 0; var3 < this.bw.length; ++var3) {
            this.bw[var3] = ItemStack.a(var2.b(var3));
         }
      }

      if(var1.b("HandItems", 9)) {
         var2 = var1.c("HandItems", 10);

         for(var3 = 0; var3 < this.bv.length; ++var3) {
            this.bv[var3] = ItemStack.a(var2.b(var3));
         }
      }

      if(var1.b("ArmorDropChances", 9)) {
         var2 = var1.c("ArmorDropChances", 5);

         for(var3 = 0; var3 < var2.c(); ++var3) {
            this.bs[var3] = var2.f(var3);
         }
      }

      if(var1.b("HandDropChances", 9)) {
         var2 = var1.c("HandDropChances", 5);

         for(var3 = 0; var3 < var2.c(); ++var3) {
            this.br[var3] = var2.f(var3);
         }
      }

      this.bC = var1.p("Leashed");
      if(this.bC && var1.b("Leash", 10)) {
         this.bE = var1.o("Leash");
      }

      this.n(var1.p("LeftHanded"));
      if(var1.b("DeathLootTable", 8)) {
         this.bA = new class_kk(var1.l("DeathLootTable"));
         this.bB = var1.i("DeathLootTableSeed");
      }

      this.m(var1.p("NoAI"));
   }

   protected class_kk J() {
      return null;
   }

   protected void a(boolean var1, int var2, DamageSource var3) {
      class_kk var4 = this.bA;
      if(var4 == null) {
         var4 = this.J();
      }

      if(var4 != null) {
         class_azx var5 = this.world.ak().a(var4);
         this.bA = null;
         class_azy.class_a_in_class_azy var6 = (new class_azy.class_a_in_class_azy((WorldServer)this.world)).a((Entity)this).a(var3);
         if(var1 && this.aR != null) {
            var6 = var6.a(this.aR).a(this.aR.da());
         }

         List var7 = var5.a(this.bB == 0L?this.random:new Random(this.bB), var6.a());
         Iterator var8 = var7.iterator();

         while(var8.hasNext()) {
            ItemStack var9 = (ItemStack)var8.next();
            this.a((ItemStack)var9, 0.0F);
         }

         this.a(var1, var2);
      } else {
         super.a(var1, var2, var3);
      }

   }

   public void o(float var1) {
      this.be = var1;
   }

   public void p(float var1) {
      this.bd = var1;
   }

   public void l(float var1) {
      super.l(var1);
      this.o(var1);
   }

   public void n() {
      super.n();
      this.world.C.a("looting");
      if(!this.world.E && this.cM() && !this.aT && this.world.U().b("mobGriefing")) {
         List var1 = this.world.a(class_yc.class, this.bk().b(1.0D, 0.0D, 1.0D));
         Iterator var2 = var1.iterator();

         while(var2.hasNext()) {
            class_yc var3 = (class_yc)var2.next();
            if(!var3.dead && var3.k() != null && !var3.t()) {
               this.a(var3);
            }
         }
      }

      this.world.C.b();
   }

   protected void a(class_yc var1) {
      ItemStack var2 = var1.k();
      EnumInventorySlot var3 = d(var2);
      boolean var4 = true;
      ItemStack var5 = this.a(var3);
      if(var5 != null) {
         if(var3.a() == EnumInventorySlot.EnumSlotType.HAND) {
            if(var2.b() instanceof class_aew && !(var5.b() instanceof class_aew)) {
               var4 = true;
            } else if(var2.b() instanceof class_aew && var5.b() instanceof class_aew) {
               class_aew var6 = (class_aew)var2.b();
               class_aew var7 = (class_aew)var5.b();
               if(var6.g() == var7.g()) {
                  var4 = var2.i() > var5.i() || var2.n() && !var5.n();
               } else {
                  var4 = var6.g() > var7.g();
               }
            } else if(var2.b() instanceof class_acg && var5.b() instanceof class_acg) {
               var4 = var2.n() && !var5.n();
            } else {
               var4 = false;
            }
         } else if(var2.b() instanceof ItemArmor && !(var5.b() instanceof ItemArmor)) {
            var4 = true;
         } else if(var2.b() instanceof ItemArmor && var5.b() instanceof ItemArmor) {
            ItemArmor var9 = (ItemArmor)var2.b();
            ItemArmor var11 = (ItemArmor)var5.b();
            if(var9.d == var11.d) {
               var4 = var2.i() > var5.i() || var2.n() && !var5.n();
            } else {
               var4 = var9.d > var11.d;
            }
         } else {
            var4 = false;
         }
      }

      if(var4 && this.c(var2)) {
         double var10;
         switch(EntityInsentient.SyntheticClass_1.a[var3.a().ordinal()]) {
         case 1:
            var10 = (double)this.br[var3.b()];
            break;
         case 2:
            var10 = (double)this.bs[var3.b()];
            break;
         default:
            var10 = 0.0D;
         }

         if(var5 != null && (double)(this.random.nextFloat() - 0.1F) < var10) {
            this.a((ItemStack)var5, 0.0F);
         }

         if(var2.b() == Items.k && var1.n() != null) {
            EntityHuman var8 = this.world.a(var1.n());
            if(var8 != null) {
               var8.b((Statistic)AchievementList.x);
            }
         }

         this.a(var3, var2);
         switch(EntityInsentient.SyntheticClass_1.a[var3.a().ordinal()]) {
         case 1:
            this.br[var3.b()] = 2.0F;
            break;
         case 2:
            this.bs[var3.b()] = 2.0F;
         }

         this.by = true;
         this.a(var1, 1);
         var1.S();
      }

   }

   protected boolean c(ItemStack var1) {
      return true;
   }

   protected boolean K() {
      return true;
   }

   protected void L() {
      if(this.by) {
         this.aU = 0;
      } else {
         EntityHuman var1 = this.world.a(this, -1.0D);
         if(var1 != null) {
            double var2 = var1.locX - this.locX;
            double var4 = var1.locY - this.locY;
            double var6 = var1.locZ - this.locZ;
            double var8 = var2 * var2 + var4 * var4 + var6 * var6;
            if(this.K() && var8 > 16384.0D) {
               this.S();
            }

            if(this.aU > 600 && this.random.nextInt(800) == 0 && var8 > 1024.0D && this.K()) {
               this.S();
            } else if(var8 < 1024.0D) {
               this.aU = 0;
            }
         }

      }
   }

   protected final void cl() {
      ++this.aU;
      this.world.C.a("checkDespawn");
      this.L();
      this.world.C.b();
      this.world.C.a("sensing");
      this.bu.a();
      this.world.C.b();
      this.world.C.a("targetSelector");
      this.bq.a();
      this.world.C.b();
      this.world.C.a("goalSelector");
      this.bp.a();
      this.world.C.b();
      this.world.C.a("navigation");
      this.h.l();
      this.world.C.b();
      this.world.C.a("mob tick");
      this.M();
      this.world.C.b();
      if(this.aH() && this.bx() instanceof EntityInsentient) {
         EntityInsentient var1 = (EntityInsentient)this.bx();
         var1.x().a(this.x().k(), 1.5D);
         var1.u().a(this.u());
      }

      this.world.C.a("controls");
      this.world.C.a("move");
      this.f.c();
      this.world.C.c("look");
      this.b.a();
      this.world.C.c("jump");
      this.g.b();
      this.world.C.b();
      this.world.C.b();
   }

   protected void M() {
   }

   public int cD() {
      return 40;
   }

   public int cE() {
      return 10;
   }

   public void a(Entity var1, float var2, float var3) {
      double var4 = var1.locX - this.locX;
      double var8 = var1.locZ - this.locZ;
      double var6;
      if(var1 instanceof class_rz) {
         class_rz var10 = (class_rz)var1;
         var6 = var10.locY + (double)var10.bm() - (this.locY + (double)this.bm());
      } else {
         var6 = (var1.bk().b + var1.bk().e) / 2.0D - (this.locY + (double)this.bm());
      }

      double var14 = (double)MathHelper.a(var4 * var4 + var8 * var8);
      float var12 = (float)(MathHelper.b(var8, var4) * 57.2957763671875D) - 90.0F;
      float var13 = (float)(-(MathHelper.b(var6, var14) * 57.2957763671875D));
      this.pitch = this.b(this.pitch, var13, var3);
      this.yaw = this.b(this.yaw, var12, var2);
   }

   private float b(float var1, float var2, float var3) {
      float var4 = MathHelper.g(var2 - var1);
      if(var4 > var3) {
         var4 = var3;
      }

      if(var4 < -var3) {
         var4 = -var3;
      }

      return var1 + var4;
   }

   public boolean cF() {
      return true;
   }

   public boolean cG() {
      return this.world.a((AxisAlignedBB)this.bk(), (Entity)this) && this.world.a((Entity)this, (AxisAlignedBB)this.bk()).isEmpty() && !this.world.e(this.bk());
   }

   public int cJ() {
      return 4;
   }

   public int aV() {
      if(this.A() == null) {
         return 3;
      } else {
         int var1 = (int)(this.bP() - this.bV() * 0.33F);
         var1 -= (3 - this.world.ae().a()) * 4;
         if(var1 < 0) {
            var1 = 0;
         }

         return var1 + 3;
      }
   }

   public Iterable aD() {
      return Arrays.asList(this.bv);
   }

   public Iterable aE() {
      return Arrays.asList(this.bw);
   }

   public ItemStack a(EnumInventorySlot var1) {
      ItemStack var2 = null;
      switch(EntityInsentient.SyntheticClass_1.a[var1.a().ordinal()]) {
      case 1:
         var2 = this.bv[var1.b()];
         break;
      case 2:
         var2 = this.bw[var1.b()];
      }

      return var2;
   }

   public void a(EnumInventorySlot var1, ItemStack var2) {
      switch(EntityInsentient.SyntheticClass_1.a[var1.a().ordinal()]) {
      case 1:
         this.bv[var1.b()] = var2;
         break;
      case 2:
         this.bw[var1.b()] = var2;
      }

   }

   protected void a(boolean var1, int var2) {
      EnumInventorySlot[] var3 = EnumInventorySlot.values();
      int var4 = var3.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         EnumInventorySlot var6 = var3[var5];
         ItemStack var7 = this.a(var6);
         double var8;
         switch(EntityInsentient.SyntheticClass_1.a[var6.a().ordinal()]) {
         case 1:
            var8 = (double)this.br[var6.b()];
            break;
         case 2:
            var8 = (double)this.bs[var6.b()];
            break;
         default:
            var8 = 0.0D;
         }

         boolean var10 = var8 > 1.0D;
         if(var7 != null && (var1 || var10) && (double)(this.random.nextFloat() - (float)var2 * 0.01F) < var8) {
            if(!var10 && var7.e()) {
               int var11 = Math.max(var7.j() - 25, 1);
               int var12 = var7.j() - this.random.nextInt(this.random.nextInt(var11) + 1);
               if(var12 > var11) {
                  var12 = var11;
               }

               if(var12 < 1) {
                  var12 = 1;
               }

               var7.b(var12);
            }

            this.a((ItemStack)var7, 0.0F);
         }
      }

   }

   protected void a(class_qk var1) {
      if(this.random.nextFloat() < 0.15F * var1.c()) {
         int var2 = this.random.nextInt(2);
         float var3 = this.world.ae() == EnumDifficulty.HARD?0.1F:0.25F;
         if(this.random.nextFloat() < 0.095F) {
            ++var2;
         }

         if(this.random.nextFloat() < 0.095F) {
            ++var2;
         }

         if(this.random.nextFloat() < 0.095F) {
            ++var2;
         }

         boolean var4 = true;
         EnumInventorySlot[] var5 = EnumInventorySlot.values();
         int var6 = var5.length;

         for(int var7 = 0; var7 < var6; ++var7) {
            EnumInventorySlot var8 = var5[var7];
            if(var8.a() == EnumInventorySlot.EnumSlotType.ARMOR) {
               ItemStack var9 = this.a(var8);
               if(!var4 && this.random.nextFloat() < var3) {
                  break;
               }

               var4 = false;
               if(var9 == null) {
                  Item var10 = a(var8, var2);
                  if(var10 != null) {
                     this.a(var8, new ItemStack(var10));
                  }
               }
            }
         }
      }

   }

   public static EnumInventorySlot d(ItemStack var0) {
      return var0.b() != Item.a(Blocks.aU) && var0.b() != Items.ch?(var0.b() == Items.cR?EnumInventorySlot.CHEST:(var0.b() instanceof ItemArmor?((ItemArmor)var0.b()).c:(var0.b() == Items.cR?EnumInventorySlot.CHEST:EnumInventorySlot.MAINHAND))):EnumInventorySlot.HEAD;
   }

   public static Item a(EnumInventorySlot var0, int var1) {
      switch(EntityInsentient.SyntheticClass_1.b[var0.ordinal()]) {
      case 1:
         if(var1 == 0) {
            return Items.S;
         } else if(var1 == 1) {
            return Items.ai;
         } else if(var1 == 2) {
            return Items.W;
         } else if(var1 == 3) {
            return Items.aa;
         } else if(var1 == 4) {
            return Items.ae;
         }
      case 2:
         if(var1 == 0) {
            return Items.T;
         } else if(var1 == 1) {
            return Items.aj;
         } else if(var1 == 2) {
            return Items.X;
         } else if(var1 == 3) {
            return Items.ab;
         } else if(var1 == 4) {
            return Items.af;
         }
      case 3:
         if(var1 == 0) {
            return Items.U;
         } else if(var1 == 1) {
            return Items.ak;
         } else if(var1 == 2) {
            return Items.Y;
         } else if(var1 == 3) {
            return Items.ac;
         } else if(var1 == 4) {
            return Items.ag;
         }
      case 4:
         if(var1 == 0) {
            return Items.V;
         } else if(var1 == 1) {
            return Items.al;
         } else if(var1 == 2) {
            return Items.Z;
         } else if(var1 == 3) {
            return Items.ad;
         } else if(var1 == 4) {
            return Items.ah;
         }
      default:
         return null;
      }
   }

   protected void b(class_qk var1) {
      float var2 = var1.c();
      if(this.ca() != null && this.random.nextFloat() < 0.25F * var2) {
         class_agn.a(this.random, this.ca(), (int)(5.0F + var2 * (float)this.random.nextInt(18)), false);
      }

      EnumInventorySlot[] var3 = EnumInventorySlot.values();
      int var4 = var3.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         EnumInventorySlot var6 = var3[var5];
         if(var6.a() == EnumInventorySlot.EnumSlotType.ARMOR) {
            ItemStack var7 = this.a(var6);
            if(var7 != null && this.random.nextFloat() < 0.5F * var2) {
               class_agn.a(this.random, var7, (int)(5.0F + var2 * (float)this.random.nextInt(18)), false);
            }
         }
      }

   }

   public class_sc a(class_qk var1, class_sc var2) {
      this.a((class_sk)class_ys.b).b(new AttributeModifier("Random spawn bonus", this.random.nextGaussian() * 0.05D, 1));
      if(this.random.nextFloat() < 0.05F) {
         this.n(true);
      } else {
         this.n(false);
      }

      return var2;
   }

   public boolean cK() {
      return false;
   }

   public void cL() {
      this.by = true;
   }

   public void a(EnumInventorySlot var1, float var2) {
      switch(EntityInsentient.SyntheticClass_1.a[var1.a().ordinal()]) {
      case 1:
         this.br[var1.b()] = var2;
         break;
      case 2:
         this.bs[var1.b()] = var2;
      }

   }

   public boolean cM() {
      return this.bx;
   }

   public void l(boolean var1) {
      this.bx = var1;
   }

   public boolean cN() {
      return this.by;
   }

   public final boolean a(EntityHuman var1, ItemStack var2, EnumHand var3) {
      if(this.cP() && this.cQ() == var1) {
         this.a(true, !var1.abilities.d);
         return true;
      } else if(var2 != null && var2.b() == Items.cx && this.a(var1)) {
         this.b(var1, true);
         --var2.b;
         return true;
      } else {
         return this.a(var1, var3, var2)?true:super.a(var1, var2, var3);
      }
   }

   protected boolean a(EntityHuman var1, EnumHand var2, ItemStack var3) {
      return false;
   }

   protected void cO() {
      if(this.bE != null) {
         this.cT();
      }

      if(this.bC) {
         if(!this.at()) {
            this.a(true, true);
         }

         if(this.bD == null || this.bD.dead) {
            this.a(true, true);
         }
      }
   }

   public void a(boolean var1, boolean var2) {
      if(this.bC) {
         this.bC = false;
         this.bD = null;
         if(!this.world.E && var2) {
            this.a(Items.cx, 1);
         }

         if(!this.world.E && var1 && this.world instanceof WorldServer) {
            ((WorldServer)this.world).v().a((Entity)this, (Packet)(new PacketPlayOutAttachEntity(this, (Entity)null)));
         }
      }

   }

   public boolean a(EntityHuman var1) {
      return !this.cP() && !(this instanceof class_yk);
   }

   public boolean cP() {
      return this.bC;
   }

   public Entity cQ() {
      return this.bD;
   }

   public void b(Entity var1, boolean var2) {
      this.bC = true;
      this.bD = var1;
      if(!this.world.E && var2 && this.world instanceof WorldServer) {
         ((WorldServer)this.world).v().a((Entity)this, (Packet)(new PacketPlayOutAttachEntity(this, this.bD)));
      }

      if(this.aH()) {
         this.p();
      }

   }

   public boolean a(Entity var1, boolean var2) {
      boolean var3 = super.a(var1, var2);
      if(var3 && this.cP()) {
         this.a(true, true);
      }

      return var3;
   }

   private void cT() {
      if(this.bC && this.bE != null) {
         if(this.bE.b("UUID")) {
            UUID var1 = this.bE.a("UUID");
            List var2 = this.world.a(class_rz.class, this.bk().g(10.0D));
            Iterator var3 = var2.iterator();

            while(var3.hasNext()) {
               class_rz var4 = (class_rz)var3.next();
               if(var4.getUniqueId().equals(var1)) {
                  this.bD = var4;
                  break;
               }
            }
         } else if(this.bE.b("X", 99) && this.bE.b("Y", 99) && this.bE.b("Z", 99)) {
            BlockPosition var5 = new BlockPosition(this.bE.h("X"), this.bE.h("Y"), this.bE.h("Z"));
            class_xs var6 = class_xs.b(this.world, var5);
            if(var6 == null) {
               var6 = class_xs.a(this.world, var5);
            }

            this.bD = var6;
         } else {
            this.a(false, true);
         }
      }

      this.bE = null;
   }

   public boolean c(int var1, ItemStack var2) {
      EnumInventorySlot var3;
      if(var1 == 98) {
         var3 = EnumInventorySlot.MAINHAND;
      } else if(var1 == 99) {
         var3 = EnumInventorySlot.OFFHAND;
      } else if(var1 == 100 + EnumInventorySlot.HEAD.b()) {
         var3 = EnumInventorySlot.HEAD;
      } else if(var1 == 100 + EnumInventorySlot.CHEST.b()) {
         var3 = EnumInventorySlot.CHEST;
      } else if(var1 == 100 + EnumInventorySlot.LEGS.b()) {
         var3 = EnumInventorySlot.LEGS;
      } else {
         if(var1 != 100 + EnumInventorySlot.FEET.b()) {
            return false;
         }

         var3 = EnumInventorySlot.FEET;
      }

      if(var2 != null && !b(var3, var2) && var3 != EnumInventorySlot.HEAD) {
         return false;
      } else {
         this.a(var3, var2);
         return true;
      }
   }

   public static boolean b(EnumInventorySlot var0, ItemStack var1) {
      EnumInventorySlot var2 = d(var1);
      return var2 == var0 || var2 == EnumInventorySlot.MAINHAND && var0 == EnumInventorySlot.OFFHAND;
   }

   public boolean cn() {
      return super.cn() && !this.cR();
   }

   public void m(boolean var1) {
      byte var2 = ((Byte)this.datawatcher.a(a)).byteValue();
      this.datawatcher.b(a, Byte.valueOf(var1?(byte)(var2 | 1):(byte)(var2 & -2)));
   }

   public void n(boolean var1) {
      byte var2 = ((Byte)this.datawatcher.a(a)).byteValue();
      this.datawatcher.b(a, Byte.valueOf(var1?(byte)(var2 | 2):(byte)(var2 & -3)));
   }

   public boolean cR() {
      return (((Byte)this.datawatcher.a(a)).byteValue() & 1) != 0;
   }

   public boolean cS() {
      return (((Byte)this.datawatcher.a(a)).byteValue() & 2) != 0;
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b = new int[EnumInventorySlot.values().length];

      static {
         try {
            b[EnumInventorySlot.HEAD.ordinal()] = 1;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            b[EnumInventorySlot.CHEST.ordinal()] = 2;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            b[EnumInventorySlot.LEGS.ordinal()] = 3;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            b[EnumInventorySlot.FEET.ordinal()] = 4;
         } catch (NoSuchFieldError var3) {
            ;
         }

         a = new int[EnumInventorySlot.EnumSlotType.values().length];

         try {
            a[EnumInventorySlot.EnumSlotType.HAND.ordinal()] = 1;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumInventorySlot.EnumSlotType.ARMOR.ordinal()] = 2;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum EnumEntityPositionType {
      ON_GROUND,
      IN_AIR,
      IN_WATER;
   }
}
