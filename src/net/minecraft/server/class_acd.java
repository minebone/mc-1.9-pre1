package net.minecraft.server;

import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.StatisticList;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.EntityBoat;
import net.minecraft.server.ItemStack;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;

public class class_acd extends Item {
   private final EntityBoat.EnumBoatVariant a;

   public class_acd(EntityBoat.EnumBoatVariant var1) {
      this.a = var1;
      this.j = 1;
      this.a(CreativeModeTab.e);
      this.c("boat." + var1.a());
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      float var5 = 1.0F;
      float var6 = var3.lastPitch + (var3.pitch - var3.lastPitch) * var5;
      float var7 = var3.lastYaw + (var3.yaw - var3.lastYaw) * var5;
      double var8 = var3.lastX + (var3.locX - var3.lastX) * (double)var5;
      double var10 = var3.lastY + (var3.locY - var3.lastY) * (double)var5 + (double)var3.bm();
      double var12 = var3.lastZ + (var3.locZ - var3.lastZ) * (double)var5;
      Vec3D var14 = new Vec3D(var8, var10, var12);
      float var15 = MathHelper.b(-var7 * 0.017453292F - 3.1415927F);
      float var16 = MathHelper.a(-var7 * 0.017453292F - 3.1415927F);
      float var17 = -MathHelper.b(-var6 * 0.017453292F);
      float var18 = MathHelper.a(-var6 * 0.017453292F);
      float var19 = var16 * var17;
      float var21 = var15 * var17;
      double var22 = 5.0D;
      Vec3D var24 = var14.b((double)var19 * var22, (double)var18 * var22, (double)var21 * var22);
      MovingObjectPosition var25 = var2.a(var14, var24, true);
      if(var25 == null) {
         return new class_qo(EnumResult.PASS, var1);
      } else {
         Vec3D var26 = var3.f(var5);
         boolean var27 = false;
         List var28 = var2.b((Entity)var3, (AxisAlignedBB)var3.bk().a(var26.b * var22, var26.c * var22, var26.d * var22).g(1.0D));

         for(int var29 = 0; var29 < var28.size(); ++var29) {
            Entity var30 = (Entity)var28.get(var29);
            if(var30.ao()) {
               AxisAlignedBB var31 = var30.bk().g((double)var30.az());
               if(var31.a(var14)) {
                  var27 = true;
               }
            }
         }

         if(var27) {
            return new class_qo(EnumResult.PASS, var1);
         } else if(var25.a != MovingObjectPosition.EnumMovingObjectType.BLOCK) {
            return new class_qo(EnumResult.PASS, var1);
         } else {
            Block var32 = var2.getType(var25.a()).getBlock();
            boolean var33 = var32 == Blocks.j || var32 == Blocks.i;
            EntityBoat var34 = new EntityBoat(var2, var25.c.b, var33?var25.c.c - 0.12D:var25.c.c, var25.c.d);
            var34.a(this.a);
            var34.yaw = var3.yaw;
            if(!var2.a((Entity)var34, (AxisAlignedBB)var34.bk().g(-0.1D)).isEmpty()) {
               return new class_qo(EnumResult.FAIL, var1);
            } else {
               if(!var2.E) {
                  var2.a((Entity)var34);
               }

               if(!var3.abilities.d) {
                  --var1.b;
               }

               var3.b(StatisticList.b((Item)this));
               return new class_qo(EnumResult.SUCCESS, var1);
            }
         }
      }
   }
}
