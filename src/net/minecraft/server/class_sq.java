package net.minecraft.server;

import com.google.common.collect.Sets;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import net.minecraft.server.AttributeMapBase;
import net.minecraft.server.AttributeModifiable;
import net.minecraft.server.AttributeRanged;
import net.minecraft.server.class_of;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sl;

public class class_sq extends AttributeMapBase {
   private final Set e = Sets.newHashSet();
   protected final Map d = new class_of();

   public AttributeModifiable e(class_sk var1) {
      return (AttributeModifiable)super.a(var1);
   }

   public AttributeModifiable b(String var1) {
      class_sl var2 = super.a(var1);
      if(var2 == null) {
         var2 = (class_sl)this.d.get(var1);
      }

      return (AttributeModifiable)var2;
   }

   public class_sl b(class_sk var1) {
      class_sl var2 = super.b(var1);
      if(var1 instanceof AttributeRanged && ((AttributeRanged)var1).g() != null) {
         this.d.put(((AttributeRanged)var1).g(), var2);
      }

      return var2;
   }

   protected class_sl c(class_sk var1) {
      return new AttributeModifiable(this, var1);
   }

   public void a(class_sl var1) {
      if(var1.a().c()) {
         this.e.add(var1);
      }

      Iterator var2 = this.c.get(var1.a()).iterator();

      while(var2.hasNext()) {
         class_sk var3 = (class_sk)var2.next();
         AttributeModifiable var4 = this.e(var3);
         if(var4 != null) {
            var4.f();
         }
      }

   }

   public Set b() {
      return this.e;
   }

   public Collection c() {
      HashSet var1 = Sets.newHashSet();
      Iterator var2 = this.a().iterator();

      while(var2.hasNext()) {
         class_sl var3 = (class_sl)var2.next();
         if(var3.a().c()) {
            var1.add(var3);
         }
      }

      return var1;
   }

   // $FF: synthetic method
   public class_sl a(String var1) {
      return this.b(var1);
   }

   // $FF: synthetic method
   public class_sl a(class_sk var1) {
      return this.e(var1);
   }
}
