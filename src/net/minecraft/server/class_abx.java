package net.minecraft.server;

import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.class_aac;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_zl;

public class class_abx extends Item {
   public class_abx() {
      this.a(CreativeModeTab.j);
   }

   public class_zl a(World var1, ItemStack var2, class_rz var3) {
      class_aac var4 = new class_aac(var1, var3);
      var4.a(var2);
      return var4;
   }
}
