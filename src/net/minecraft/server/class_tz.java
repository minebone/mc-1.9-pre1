package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Entity;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_wg;
import net.minecraft.server.class_zd;

public class class_tz extends class_tj {
   private class_wg a;
   private class_zd b;
   private int c;

   public class_tz(class_wg var1) {
      this.a = var1;
      this.a(3);
   }

   public boolean a() {
      if(!this.a.world.B()) {
         return false;
      } else if(this.a.bE().nextInt(8000) != 0) {
         return false;
      } else {
         this.b = (class_zd)this.a.world.a((Class)class_zd.class, (AxisAlignedBB)this.a.bk().b(6.0D, 2.0D, 6.0D), (Entity)this.a);
         return this.b != null;
      }
   }

   public boolean b() {
      return this.c > 0;
   }

   public void c() {
      this.c = 400;
      this.a.a(true);
   }

   public void d() {
      this.a.a(false);
      this.b = null;
   }

   public void e() {
      this.a.t().a(this.b, 30.0F, 30.0F);
      --this.c;
   }
}
