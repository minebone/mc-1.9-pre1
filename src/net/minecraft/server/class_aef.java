package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;

public class class_aef extends Item {
   public class_aef() {
      this.a(CreativeModeTab.d);
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      boolean var10 = var3.getType(var4).getBlock().a((class_ahw)var3, (BlockPosition)var4);
      BlockPosition var11 = var10?var4:var4.a(var6);
      if(var2.a(var11, var6, var1) && var3.a(var3.getType(var11).getBlock(), var11, false, var6, (Entity)null, var1) && Blocks.af.a(var3, var11)) {
         --var1.b;
         var3.a(var11, Blocks.af.u());
         return EnumResult.SUCCESS;
      } else {
         return EnumResult.FAIL;
      }
   }
}
