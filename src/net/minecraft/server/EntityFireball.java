package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_kk;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_zs;

public abstract class EntityFireball extends Entity {
   private int e = -1;
   private int f = -1;
   private int g = -1;
   private Block h;
   private boolean as;
   public class_rz a;
   private int at;
   private int au;
   public double b;
   public double c;
   public double d;

   public EntityFireball(World var1) {
      super(var1);
      this.a(1.0F, 1.0F);
   }

   protected void i() {
   }

   public EntityFireball(World var1, double var2, double var4, double var6, double var8, double var10, double var12) {
      super(var1);
      this.a(1.0F, 1.0F);
      this.b(var2, var4, var6, this.yaw, this.pitch);
      this.b(var2, var4, var6);
      double var14 = (double)MathHelper.a(var8 * var8 + var10 * var10 + var12 * var12);
      this.b = var8 / var14 * 0.1D;
      this.c = var10 / var14 * 0.1D;
      this.d = var12 / var14 * 0.1D;
   }

   public EntityFireball(World var1, class_rz var2, double var3, double var5, double var7) {
      super(var1);
      this.a = var2;
      this.a(1.0F, 1.0F);
      this.b(var2.locX, var2.locY, var2.locZ, var2.yaw, var2.pitch);
      this.b(this.locX, this.locY, this.locZ);
      this.motX = this.motY = this.motZ = 0.0D;
      var3 += this.random.nextGaussian() * 0.4D;
      var5 += this.random.nextGaussian() * 0.4D;
      var7 += this.random.nextGaussian() * 0.4D;
      double var9 = (double)MathHelper.a(var3 * var3 + var5 * var5 + var7 * var7);
      this.b = var3 / var9 * 0.1D;
      this.c = var5 / var9 * 0.1D;
      this.d = var7 / var9 * 0.1D;
   }

   public void m() {
      if(!this.world.E && (this.a != null && this.a.dead || !this.world.e(new BlockPosition(this)))) {
         this.S();
      } else {
         super.m();
         if(this.k()) {
            this.g(1);
         }

         if(this.as) {
            if(this.world.getType(new BlockPosition(this.e, this.f, this.g)).getBlock() == this.h) {
               ++this.at;
               if(this.at == 600) {
                  this.S();
               }

               return;
            }

            this.as = false;
            this.motX *= (double)(this.random.nextFloat() * 0.2F);
            this.motY *= (double)(this.random.nextFloat() * 0.2F);
            this.motZ *= (double)(this.random.nextFloat() * 0.2F);
            this.at = 0;
            this.au = 0;
         } else {
            ++this.au;
         }

         MovingObjectPosition var1 = class_zs.a(this, true, this.au >= 25, this.a);
         if(var1 != null) {
            this.a(var1);
         }

         this.locX += this.motX;
         this.locY += this.motY;
         this.locZ += this.motZ;
         class_zs.a(this, 0.2F);
         float var2 = this.l();
         if(this.ah()) {
            for(int var3 = 0; var3 < 4; ++var3) {
               float var4 = 0.25F;
               this.world.a(EnumParticle.WATER_BUBBLE, this.locX - this.motX * (double)var4, this.locY - this.motY * (double)var4, this.locZ - this.motZ * (double)var4, this.motX, this.motY, this.motZ, new int[0]);
            }

            var2 = 0.8F;
         }

         this.motX += this.b;
         this.motY += this.c;
         this.motZ += this.d;
         this.motX *= (double)var2;
         this.motY *= (double)var2;
         this.motZ *= (double)var2;
         this.world.a(this.j(), this.locX, this.locY + 0.5D, this.locZ, 0.0D, 0.0D, 0.0D, new int[0]);
         this.b(this.locX, this.locY, this.locZ);
      }
   }

   protected boolean k() {
      return true;
   }

   protected EnumParticle j() {
      return EnumParticle.SMOKE_NORMAL;
   }

   protected float l() {
      return 0.95F;
   }

   protected abstract void a(MovingObjectPosition var1);

   public void b(NBTTagCompound var1) {
      var1.a("xTile", this.e);
      var1.a("yTile", this.f);
      var1.a("zTile", this.g);
      class_kk var2 = (class_kk)Block.h.b(this.h);
      var1.a("inTile", var2 == null?"":var2.toString());
      var1.a("inGround", (byte)(this.as?1:0));
      var1.a((String)"direction", (NBTTag)this.a((double[])(new double[]{this.motX, this.motY, this.motZ})));
      var1.a((String)"power", (NBTTag)this.a((double[])(new double[]{this.b, this.c, this.d})));
      var1.a("life", this.at);
   }

   public void a(NBTTagCompound var1) {
      this.e = var1.h("xTile");
      this.f = var1.h("yTile");
      this.g = var1.h("zTile");
      if(var1.b("inTile", 8)) {
         this.h = Block.b(var1.l("inTile"));
      } else {
         this.h = Block.b(var1.f("inTile") & 255);
      }

      this.as = var1.f("inGround") == 1;
      NBTTagList var2;
      if(var1.b("power", 9)) {
         var2 = var1.c("power", 6);
         if(var2.c() == 3) {
            this.b = var2.e(0);
            this.c = var2.e(1);
            this.d = var2.e(2);
         }
      }

      this.at = var1.h("life");
      if(var1.b("direction", 9) && var1.c("direction", 6).c() == 3) {
         var2 = var1.c("direction", 6);
         this.motX = var2.e(0);
         this.motY = var2.e(1);
         this.motZ = var2.e(2);
      } else {
         this.S();
      }

   }

   public boolean ao() {
      return true;
   }

   public float az() {
      return 1.0F;
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b(var1)) {
         return false;
      } else {
         this.an();
         if(var1.j() != null) {
            Vec3D var3 = var1.j().aA();
            if(var3 != null) {
               this.motX = var3.b;
               this.motY = var3.c;
               this.motZ = var3.d;
               this.b = this.motX * 0.1D;
               this.c = this.motY * 0.1D;
               this.d = this.motZ * 0.1D;
            }

            if(var1.j() instanceof class_rz) {
               this.a = (class_rz)var1.j();
            }

            return true;
         } else {
            return false;
         }
      }
   }

   public float e(float var1) {
      return 1.0F;
   }
}
