package net.minecraft.server;

import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityFireball;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.class_rz;

public class EntityLargeFireball extends EntityFireball {
   public int e = 1;

   public EntityLargeFireball(World var1) {
      super(var1);
   }

   public EntityLargeFireball(World var1, class_rz var2, double var3, double var5, double var7) {
      super(var1, var2, var3, var5, var7);
   }

   protected void a(MovingObjectPosition var1) {
      if(!this.world.E) {
         if(var1.d != null) {
            var1.d.a(DamageSource.a((EntityFireball)this, (Entity)this.a), 6.0F);
            this.a(this.a, var1.d);
         }

         boolean var2 = this.world.U().b("mobGriefing");
         this.world.a((Entity)null, this.locX, this.locY, this.locZ, (float)this.e, var2, var2);
         this.S();
      }

   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("ExplosionPower", this.e);
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      if(var1.b("ExplosionPower", 99)) {
         this.e = var1.h("ExplosionPower");
      }

   }
}
