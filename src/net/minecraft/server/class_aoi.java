package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_aqm;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;

public class class_aoi extends class_ajm {
   protected static final AxisAlignedBB a = new AxisAlignedBB(0.25D, 0.0D, 0.25D, 0.75D, 1.0D, 0.75D);

   protected class_aoi() {
      super(Material.d);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return a;
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(class_ahw var1, BlockPosition var2) {
      return true;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean d() {
      return true;
   }

   public TileEntity a(World var1, int var2) {
      return new class_aqm();
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Items.ar;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Items.ar);
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(var1.E) {
         return true;
      } else {
         TileEntity var11 = var1.r(var2);
         return var11 instanceof class_aqm?((class_aqm)var11).b(var4):false;
      }
   }

   public boolean a(World var1, BlockPosition var2) {
      return !this.b(var1, var2) && super.a(var1, var2);
   }
}
