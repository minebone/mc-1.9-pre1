package net.minecraft.server;

import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagFloat;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.class_ox;

public class class_pf implements class_ox {
   public int a() {
      return 100;
   }

   public NBTTagCompound a(NBTTagCompound var1) {
      NBTTagList var2 = var1.c("Equipment", 10);
      NBTTagList var3;
      if(var2.c() > 0 && !var1.b("HandItems", 10)) {
         var3 = new NBTTagList();
         var3.a(var2.h(0));
         var3.a((NBTTag)(new NBTTagCompound()));
         var1.a((String)"HandItems", (NBTTag)var3);
      }

      if(var2.c() > 1 && !var1.b("ArmorItem", 10)) {
         var3 = new NBTTagList();
         var3.a((NBTTag)var2.b(1));
         var3.a((NBTTag)var2.b(2));
         var3.a((NBTTag)var2.b(3));
         var3.a((NBTTag)var2.b(4));
         var1.a((String)"ArmorItems", (NBTTag)var3);
      }

      var1.q("Equipment");
      if(var1.b("DropChances", 9)) {
         var3 = var1.c("DropChances", 5);
         NBTTagList var4;
         if(!var1.b("HandDropChances", 10)) {
            var4 = new NBTTagList();
            var4.a((NBTTag)(new NBTTagFloat(var3.f(0))));
            var4.a((NBTTag)(new NBTTagFloat(0.0F)));
            var1.a((String)"HandDropChances", (NBTTag)var4);
         }

         if(!var1.b("ArmorDropChances", 10)) {
            var4 = new NBTTagList();
            var4.a((NBTTag)(new NBTTagFloat(var3.f(1))));
            var4.a((NBTTag)(new NBTTagFloat(var3.f(2))));
            var4.a((NBTTag)(new NBTTagFloat(var3.f(3))));
            var4.a((NBTTag)(new NBTTagFloat(var3.f(4))));
            var1.a((String)"ArmorDropChances", (NBTTag)var4);
         }

         var1.q("DropChances");
      }

      return var1;
   }
}
