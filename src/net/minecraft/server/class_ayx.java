package net.minecraft.server;

public class class_ayx {
   private byte a;
   private byte b;
   private byte c;
   private byte d;

   public class_ayx(byte var1, byte var2, byte var3, byte var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }

   public class_ayx(class_ayx var1) {
      this.a = var1.a;
      this.b = var1.b;
      this.c = var1.c;
      this.d = var1.d;
   }

   public byte a() {
      return this.a;
   }

   public byte b() {
      return this.b;
   }

   public byte c() {
      return this.c;
   }

   public byte d() {
      return this.d;
   }

   public boolean equals(Object var1) {
      if(this == var1) {
         return true;
      } else if(!(var1 instanceof class_ayx)) {
         return false;
      } else {
         class_ayx var2 = (class_ayx)var1;
         return this.a != var2.a?false:(this.d != var2.d?false:(this.b != var2.b?false:this.c == var2.c));
      }
   }

   public int hashCode() {
      byte var1 = this.a;
      int var2 = 31 * var1 + this.b;
      var2 = 31 * var2 + this.c;
      var2 = 31 * var2 + this.d;
      return var2;
   }
}
