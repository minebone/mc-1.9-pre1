package net.minecraft.server;

import com.google.common.collect.Lists;
import com.mojang.authlib.GameProfile;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.UUID;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockDoor;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.UserCache;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.BaseBlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_vn;
import net.minecraft.server.class_wg;
import net.minecraft.server.class_zd;

public class Village {
   private World a;
   private final List b = Lists.newArrayList();
   private BlockPosition c = BlockPosition.a;
   private BlockPosition d = BlockPosition.a;
   private int e;
   private int f;
   private int g;
   private int h;
   private int i;
   private TreeMap j = new TreeMap();
   private List k = Lists.newArrayList();
   private int l;

   public Village() {
   }

   public Village(World var1) {
      this.a = var1;
   }

   public void a(World var1) {
      this.a = var1;
   }

   public void a(int var1) {
      this.g = var1;
      this.m();
      this.l();
      if(var1 % 20 == 0) {
         this.k();
      }

      if(var1 % 30 == 0) {
         this.j();
      }

      int var2 = this.h / 10;
      if(this.l < var2 && this.b.size() > 20 && this.a.r.nextInt(7000) == 0) {
         Vec3D var3 = this.a(this.d, 2, 4, 2);
         if(var3 != null) {
            class_wg var4 = new class_wg(this.a);
            var4.b(var3.b, var3.c, var3.d);
            this.a.a((Entity)var4);
            ++this.l;
         }
      }

   }

   private Vec3D a(BlockPosition var1, int var2, int var3, int var4) {
      for(int var5 = 0; var5 < 10; ++var5) {
         BlockPosition var6 = var1.a(this.a.r.nextInt(16) - 8, this.a.r.nextInt(6) - 3, this.a.r.nextInt(16) - 8);
         if(this.a(var6) && this.a(new BlockPosition(var2, var3, var4), var6)) {
            return new Vec3D((double)var6.p(), (double)var6.q(), (double)var6.r());
         }
      }

      return null;
   }

   private boolean a(BlockPosition var1, BlockPosition var2) {
      if(!this.a.getType(var2.b()).q()) {
         return false;
      } else {
         int var3 = var2.p() - var1.p() / 2;
         int var4 = var2.r() - var1.r() / 2;

         for(int var5 = var3; var5 < var3 + var1.p(); ++var5) {
            for(int var6 = var2.q(); var6 < var2.q() + var1.q(); ++var6) {
               for(int var7 = var4; var7 < var4 + var1.r(); ++var7) {
                  if(this.a.getType(new BlockPosition(var5, var6, var7)).l()) {
                     return false;
                  }
               }
            }
         }

         return true;
      }
   }

   private void j() {
      List var1 = this.a.a(class_wg.class, new AxisAlignedBB((double)(this.d.p() - this.e), (double)(this.d.q() - 4), (double)(this.d.r() - this.e), (double)(this.d.p() + this.e), (double)(this.d.q() + 4), (double)(this.d.r() + this.e)));
      this.l = var1.size();
   }

   private void k() {
      List var1 = this.a.a(class_zd.class, new AxisAlignedBB((double)(this.d.p() - this.e), (double)(this.d.q() - 4), (double)(this.d.r() - this.e), (double)(this.d.p() + this.e), (double)(this.d.q() + 4), (double)(this.d.r() + this.e)));
      this.h = var1.size();
      if(this.h == 0) {
         this.j.clear();
      }

   }

   public BlockPosition a() {
      return this.d;
   }

   public int b() {
      return this.e;
   }

   public int c() {
      return this.b.size();
   }

   public int d() {
      return this.g - this.f;
   }

   public int e() {
      return this.h;
   }

   public boolean a(BlockPosition var1) {
      return this.d.k(var1) < (double)(this.e * this.e);
   }

   public List f() {
      return this.b;
   }

   public class_vn b(BlockPosition var1) {
      class_vn var2 = null;
      int var3 = Integer.MAX_VALUE;
      Iterator var4 = this.b.iterator();

      while(var4.hasNext()) {
         class_vn var5 = (class_vn)var4.next();
         int var6 = var5.a(var1);
         if(var6 < var3) {
            var2 = var5;
            var3 = var6;
         }
      }

      return var2;
   }

   public class_vn c(BlockPosition var1) {
      class_vn var2 = null;
      int var3 = Integer.MAX_VALUE;
      Iterator var4 = this.b.iterator();

      while(var4.hasNext()) {
         class_vn var5 = (class_vn)var4.next();
         int var6 = var5.a(var1);
         if(var6 > 256) {
            var6 *= 1000;
         } else {
            var6 = var5.c();
         }

         if(var6 < var3) {
            BlockPosition var7 = var5.d();
            EnumDirection var8 = var5.j();
            if(this.a.getType(var7.a(var8, 1)).getBlock().b(this.a, var7.a(var8, 1)) && this.a.getType(var7.a(var8, -1)).getBlock().b(this.a, var7.a(var8, -1)) && this.a.getType(var7.a().a(var8, 1)).getBlock().b(this.a, var7.a().a(var8, 1)) && this.a.getType(var7.a().a(var8, -1)).getBlock().b(this.a, var7.a().a(var8, -1))) {
               var2 = var5;
               var3 = var6;
            }
         }
      }

      return var2;
   }

   public class_vn e(BlockPosition var1) {
      if(this.d.k(var1) > (double)(this.e * this.e)) {
         return null;
      } else {
         Iterator var2 = this.b.iterator();

         class_vn var3;
         do {
            if(!var2.hasNext()) {
               return null;
            }

            var3 = (class_vn)var2.next();
         } while(var3.d().p() != var1.p() || var3.d().r() != var1.r() || Math.abs(var3.d().q() - var1.q()) > 1);

         return var3;
      }
   }

   public void a(class_vn var1) {
      this.b.add(var1);
      this.c = this.c.a((BaseBlockPosition)var1.d());
      this.n();
      this.f = var1.h();
   }

   public boolean g() {
      return this.b.isEmpty();
   }

   public void a(class_rz var1) {
      Iterator var2 = this.k.iterator();

      Village.class_a_in_class_vo var3;
      do {
         if(!var2.hasNext()) {
            this.k.add(new Village.class_a_in_class_vo(var1, this.g));
            return;
         }

         var3 = (Village.class_a_in_class_vo)var2.next();
      } while(var3.a != var1);

      var3.b = this.g;
   }

   public class_rz b(class_rz var1) {
      double var2 = Double.MAX_VALUE;
      Village.class_a_in_class_vo var4 = null;

      for(int var5 = 0; var5 < this.k.size(); ++var5) {
         Village.class_a_in_class_vo var6 = (Village.class_a_in_class_vo)this.k.get(var5);
         double var7 = var6.a.h(var1);
         if(var7 <= var2) {
            var4 = var6;
            var2 = var7;
         }
      }

      return var4 != null?var4.a:null;
   }

   public EntityHuman c(class_rz var1) {
      double var2 = Double.MAX_VALUE;
      EntityHuman var4 = null;
      Iterator var5 = this.j.keySet().iterator();

      while(var5.hasNext()) {
         String var6 = (String)var5.next();
         if(this.d(var6)) {
            EntityHuman var7 = this.a.a(var6);
            if(var7 != null) {
               double var8 = var7.h(var1);
               if(var8 <= var2) {
                  var4 = var7;
                  var2 = var8;
               }
            }
         }
      }

      return var4;
   }

   private void l() {
      Iterator var1 = this.k.iterator();

      while(true) {
         Village.class_a_in_class_vo var2;
         do {
            if(!var1.hasNext()) {
               return;
            }

            var2 = (Village.class_a_in_class_vo)var1.next();
         } while(var2.a.at() && Math.abs(this.g - var2.b) <= 300);

         var1.remove();
      }
   }

   private void m() {
      boolean var1 = false;
      boolean var2 = this.a.r.nextInt(50) == 0;
      Iterator var3 = this.b.iterator();

      while(true) {
         class_vn var4;
         do {
            if(!var3.hasNext()) {
               if(var1) {
                  this.n();
               }

               return;
            }

            var4 = (class_vn)var3.next();
            if(var2) {
               var4.a();
            }
         } while(this.f(var4.d()) && Math.abs(this.g - var4.h()) <= 1200);

         this.c = this.c.b(var4.d());
         var1 = true;
         var4.a(true);
         var3.remove();
      }
   }

   private boolean f(BlockPosition var1) {
      IBlockData var2 = this.a.getType(var1);
      Block var3 = var2.getBlock();
      return var3 instanceof BlockDoor?var2.getMaterial() == Material.d:false;
   }

   private void n() {
      int var1 = this.b.size();
      if(var1 == 0) {
         this.d = BlockPosition.a;
         this.e = 0;
      } else {
         this.d = new BlockPosition(this.c.p() / var1, this.c.q() / var1, this.c.r() / var1);
         int var2 = 0;

         class_vn var4;
         for(Iterator var3 = this.b.iterator(); var3.hasNext(); var2 = Math.max(var4.a(this.d), var2)) {
            var4 = (class_vn)var3.next();
         }

         this.e = Math.max(32, (int)Math.sqrt((double)var2) + 1);
      }
   }

   public int a(String var1) {
      Integer var2 = (Integer)this.j.get(var1);
      return var2 != null?var2.intValue():0;
   }

   public int a(String var1, int var2) {
      int var3 = this.a(var1);
      int var4 = MathHelper.a(var3 + var2, -30, 10);
      this.j.put(var1, Integer.valueOf(var4));
      return var4;
   }

   public boolean d(String var1) {
      return this.a(var1) <= -15;
   }

   public void a(NBTTagCompound var1) {
      this.h = var1.h("PopSize");
      this.e = var1.h("Radius");
      this.l = var1.h("Golems");
      this.f = var1.h("Stable");
      this.g = var1.h("Tick");
      this.i = var1.h("MTick");
      this.d = new BlockPosition(var1.h("CX"), var1.h("CY"), var1.h("CZ"));
      this.c = new BlockPosition(var1.h("ACX"), var1.h("ACY"), var1.h("ACZ"));
      NBTTagList var2 = var1.c("Doors", 10);

      for(int var3 = 0; var3 < var2.c(); ++var3) {
         NBTTagCompound var4 = var2.b(var3);
         class_vn var5 = new class_vn(new BlockPosition(var4.h("X"), var4.h("Y"), var4.h("Z")), var4.h("IDX"), var4.h("IDZ"), var4.h("TS"));
         this.b.add(var5);
      }

      NBTTagList var8 = var1.c("Players", 10);

      for(int var9 = 0; var9 < var8.c(); ++var9) {
         NBTTagCompound var10 = var8.b(var9);
         if(var10.e("UUID")) {
            UserCache var6 = this.a.u().aA();
            GameProfile var7 = var6.a(UUID.fromString(var10.l("UUID")));
            if(var7 != null) {
               this.j.put(var7.getName(), Integer.valueOf(var10.h("S")));
            }
         } else {
            this.j.put(var10.l("Name"), Integer.valueOf(var10.h("S")));
         }
      }

   }

   public void b(NBTTagCompound var1) {
      var1.a("PopSize", this.h);
      var1.a("Radius", this.e);
      var1.a("Golems", this.l);
      var1.a("Stable", this.f);
      var1.a("Tick", this.g);
      var1.a("MTick", this.i);
      var1.a("CX", this.d.p());
      var1.a("CY", this.d.q());
      var1.a("CZ", this.d.r());
      var1.a("ACX", this.c.p());
      var1.a("ACY", this.c.q());
      var1.a("ACZ", this.c.r());
      NBTTagList var2 = new NBTTagList();
      Iterator var3 = this.b.iterator();

      while(var3.hasNext()) {
         class_vn var4 = (class_vn)var3.next();
         NBTTagCompound var5 = new NBTTagCompound();
         var5.a("X", var4.d().p());
         var5.a("Y", var4.d().q());
         var5.a("Z", var4.d().r());
         var5.a("IDX", var4.f());
         var5.a("IDZ", var4.g());
         var5.a("TS", var4.h());
         var2.a((NBTTag)var5);
      }

      var1.a((String)"Doors", (NBTTag)var2);
      NBTTagList var9 = new NBTTagList();
      Iterator var10 = this.j.keySet().iterator();

      while(var10.hasNext()) {
         String var11 = (String)var10.next();
         NBTTagCompound var6 = new NBTTagCompound();
         UserCache var7 = this.a.u().aA();
         GameProfile var8 = var7.a(var11);
         if(var8 != null) {
            var6.a("UUID", var8.getId().toString());
            var6.a("S", ((Integer)this.j.get(var11)).intValue());
            var9.a((NBTTag)var6);
         }
      }

      var1.a((String)"Players", (NBTTag)var9);
   }

   public void h() {
      this.i = this.g;
   }

   public boolean i() {
      return this.i == 0 || this.g - this.i >= 3600;
   }

   public void b(int var1) {
      Iterator var2 = this.j.keySet().iterator();

      while(var2.hasNext()) {
         String var3 = (String)var2.next();
         this.a(var3, var1);
      }

   }

   class class_a_in_class_vo {
      public class_rz a;
      public int b;

      class_a_in_class_vo(class_rz var2, int var3) {
         this.a = var2;
         this.b = var3;
      }
   }
}
