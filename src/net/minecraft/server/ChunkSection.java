package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.NibbleArray;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPalette;

public class ChunkSection {
   private int yPos;
   private int nonEmptyBlockCount;
   private int tickingBlockCount;
   private BlockPalette blockPalette;
   private NibbleArray emittedLight;
   private NibbleArray skyLight;

   public ChunkSection(int var1, boolean flag) {
      this.yPos = var1;
      this.blockPalette = new BlockPalette();
      this.emittedLight = new NibbleArray();
      if(flag) {
         this.skyLight = new NibbleArray();
      }

   }

   public IBlockData a(int var1, int var2, int var3) {
      return this.blockPalette.a(var1, var2, var3);
   }

   public void a(int var1, int var2, int var3, IBlockData var4) {
      IBlockData var5 = this.a(var1, var2, var3);
      Block var6 = var5.getBlock();
      Block var7 = var4.getBlock();
      if(var6 != Blocks.AIR) {
         --this.nonEmptyBlockCount;
         if(var6.l()) {
            --this.tickingBlockCount;
         }
      }

      if(var7 != Blocks.AIR) {
         ++this.nonEmptyBlockCount;
         if(var7.l()) {
            ++this.tickingBlockCount;
         }
      }

      this.blockPalette.a(var1, var2, var3, var4);
   }

   public boolean isEmpty() {
      return this.nonEmptyBlockCount == 0;
   }

   public boolean shouldTick() {
      return this.tickingBlockCount > 0;
   }

   public int getYPosition() {
      return this.yPos;
   }

   public void setSkyLight(int var1, int var2, int var3, int var4) {
      this.skyLight.a(var1, var2, var3, var4);
   }

   public int getSkyLight(int var1, int var2, int var3) {
      return this.skyLight.a(var1, var2, var3);
   }

   public void setEmmitedLight(int var1, int var2, int var3, int var4) {
      this.emittedLight.a(var1, var2, var3, var4);
   }

   public int getEmittedLight(int var1, int var2, int var3) {
      return this.emittedLight.a(var1, var2, var3);
   }

   public void recalcBlockCount() {
      this.nonEmptyBlockCount = 0;
      this.tickingBlockCount = 0;

      for(int var1 = 0; var1 < 16; ++var1) {
         for(int var2 = 0; var2 < 16; ++var2) {
            for(int var3 = 0; var3 < 16; ++var3) {
            	
               Block var4 = this.a(var1, var2, var3).getBlock();
               
               if(var4 != Blocks.AIR) {
                  ++this.nonEmptyBlockCount;
                  if(var4.l()) {
                     ++this.tickingBlockCount;
                  }
               }
            }
         }
      }

   }

   public BlockPalette getBlockPalette() {
      return this.blockPalette;
   }

   public NibbleArray getEmittedLight() {
      return this.emittedLight;
   }

   public NibbleArray getSkyLight() {
      return this.skyLight;
   }

   public void setEmittedLight(NibbleArray var1) {
      this.emittedLight = var1;
   }

   public void setSkyLight(NibbleArray var1) {
      this.skyLight = var1;
   }
}
