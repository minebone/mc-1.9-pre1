package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.Vec3D;
import net.minecraft.server.Village;
import net.minecraft.server.World;
import net.minecraft.server.class_ahz;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_qk;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_yz;

public class class_vp {
   private World a;
   private boolean b;
   private int c = -1;
   private int d;
   private int e;
   private Village f;
   private int g;
   private int h;
   private int i;

   public class_vp(World var1) {
      this.a = var1;
   }

   public void a() {
      if(this.a.B()) {
         this.c = 0;
      } else if(this.c != 2) {
         if(this.c == 0) {
            float var1 = this.a.c(0.0F);
            if((double)var1 < 0.5D || (double)var1 > 0.501D) {
               return;
            }

            this.c = this.a.r.nextInt(10) == 0?1:2;
            this.b = false;
            if(this.c == 2) {
               return;
            }
         }

         if(this.c != -1) {
            if(!this.b) {
               if(!this.b()) {
                  return;
               }

               this.b = true;
            }

            if(this.e > 0) {
               --this.e;
            } else {
               this.e = 2;
               if(this.d > 0) {
                  this.c();
                  --this.d;
               } else {
                  this.c = 2;
               }

            }
         }
      }
   }

   private boolean b() {
      List var1 = this.a.i;
      Iterator var2 = var1.iterator();

      Vec3D var11;
      do {
         do {
            do {
               do {
                  do {
                     EntityHuman var3;
                     do {
                        if(!var2.hasNext()) {
                           return false;
                        }

                        var3 = (EntityHuman)var2.next();
                     } while(var3.y());

                     this.f = this.a.ai().a(new BlockPosition(var3), 1);
                  } while(this.f == null);
               } while(this.f.c() < 10);
            } while(this.f.d() < 20);
         } while(this.f.e() < 20);

         BlockPosition var4 = this.f.a();
         float var5 = (float)this.f.b();
         boolean var6 = false;

         for(int var7 = 0; var7 < 10; ++var7) {
            float var8 = this.a.r.nextFloat() * 6.2831855F;
            this.g = var4.p() + (int)((double)(MathHelper.b(var8) * var5) * 0.9D);
            this.h = var4.q();
            this.i = var4.r() + (int)((double)(MathHelper.a(var8) * var5) * 0.9D);
            var6 = false;
            Iterator var9 = this.a.ai().b().iterator();

            while(var9.hasNext()) {
               Village var10 = (Village)var9.next();
               if(var10 != this.f && var10.a(new BlockPosition(this.g, this.h, this.i))) {
                  var6 = true;
                  break;
               }
            }

            if(!var6) {
               break;
            }
         }

         if(var6) {
            return false;
         }

         var11 = this.a(new BlockPosition(this.g, this.h, this.i));
      } while(var11 == null);

      this.e = 0;
      this.d = 20;
      return true;
   }

   private boolean c() {
      Vec3D var1 = this.a(new BlockPosition(this.g, this.h, this.i));
      if(var1 == null) {
         return false;
      } else {
         class_yz var2;
         try {
            var2 = new class_yz(this.a);
            var2.a((class_qk)this.a.D(new BlockPosition(var2)), (class_sc)null);
            var2.df();
         } catch (Exception var4) {
            var4.printStackTrace();
            return false;
         }

         var2.b(var1.b, var1.c, var1.d, this.a.r.nextFloat() * 360.0F, 0.0F);
         this.a.a((Entity)var2);
         BlockPosition var3 = this.f.a();
         var2.a(var3, this.f.b());
         return true;
      }
   }

   private Vec3D a(BlockPosition var1) {
      for(int var2 = 0; var2 < 10; ++var2) {
         BlockPosition var3 = var1.a(this.a.r.nextInt(16) - 8, this.a.r.nextInt(6) - 3, this.a.r.nextInt(16) - 8);
         if(this.f.a(var3) && class_ahz.a(EntityInsentient.EnumEntityPositionType.ON_GROUND, this.a, var3)) {
            return new Vec3D((double)var3.p(), (double)var3.q(), (double)var3.r());
         }
      }

      return null;
   }
}
