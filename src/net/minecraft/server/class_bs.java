package net.minecraft.server;

import com.google.gson.JsonParseException;
import java.util.Collections;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.PacketPlayOutTitle;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ev;
import net.minecraft.server.Packet;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_bs extends CommandAbstract {
   private static final Logger a = LogManager.getLogger();

   public String c() {
      return "title";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.title.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 2) {
         throw new class_cf("commands.title.usage", new Object[0]);
      } else {
         if(var3.length < 3) {
            if("title".equals(var3[1]) || "subtitle".equals(var3[1])) {
               throw new class_cf("commands.title.usage.title", new Object[0]);
            }

            if("times".equals(var3[1])) {
               throw new class_cf("commands.title.usage.times", new Object[0]);
            }
         }

         EntityPlayer var4 = a(var1, var2, var3[0]);
         PacketPlayOutTitle.EnumTitleAction var5 = PacketPlayOutTitle.EnumTitleAction.a(var3[1]);
         if(var5 != PacketPlayOutTitle.EnumTitleAction.CLEAR && var5 != PacketPlayOutTitle.EnumTitleAction.RESET) {
            if(var5 == PacketPlayOutTitle.EnumTitleAction.TIMES) {
               if(var3.length != 5) {
                  throw new class_cf("commands.title.usage", new Object[0]);
               } else {
                  int var12 = a(var3[2]);
                  int var13 = a(var3[3]);
                  int var14 = a(var3[4]);
                  PacketPlayOutTitle var9 = new PacketPlayOutTitle(var12, var13, var14);
                  var4.a.a((Packet)var9);
                  a(var2, this, "commands.title.success", new Object[0]);
               }
            } else if(var3.length < 3) {
               throw new class_cf("commands.title.usage", new Object[0]);
            } else {
               String var11 = a(var3, 2);

               IChatBaseComponent var7;
               try {
                  var7 = IChatBaseComponent.ChatSerializer.a(var11);
               } catch (JsonParseException var10) {
                  throw a(var10);
               }

               PacketPlayOutTitle var8 = new PacketPlayOutTitle(var5, class_ev.a(var2, var7, var4));
               var4.a.a((Packet)var8);
               a(var2, this, "commands.title.success", new Object[0]);
            }
         } else if(var3.length != 2) {
            throw new class_cf("commands.title.usage", new Object[0]);
         } else {
            PacketPlayOutTitle var6 = new PacketPlayOutTitle(var5, (IChatBaseComponent)null);
            var4.a.a((Packet)var6);
            a(var2, this, "commands.title.success", new Object[0]);
         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, var1.J()):(var3.length == 2?a(var3, PacketPlayOutTitle.EnumTitleAction.a()):Collections.emptyList());
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 0;
   }
}
