package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockSmoothBrick;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.BlockStone;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_or;
import net.minecraft.server.class_yu;

public class BlockMonsterEggs extends Block {
   public static final BlockStateEnum a = BlockStateEnum.a("variant", BlockMonsterEggs.EnumMonsterEggVarient.class);

   public BlockMonsterEggs() {
      super(Material.B);
      this.w(this.A.b().set(a, BlockMonsterEggs.EnumMonsterEggVarient.a));
      this.c(0.0F);
      this.a(CreativeModeTab.c);
   }

   public int a(Random var1) {
      return 0;
   }

   public static boolean i(IBlockData var0) {
      Block var1 = var0.getBlock();
      return var0 == Blocks.b.u().set(BlockStone.a, BlockStone.EnumStoneVariant.STONE) || var1 == Blocks.e || var1 == Blocks.bf;
   }

   protected ItemStack u(IBlockData var1) {
      switch(BlockMonsterEggs.SyntheticClass_1.a[((BlockMonsterEggs.EnumMonsterEggVarient)var1.get(a)).ordinal()]) {
      case 1:
         return new ItemStack(Blocks.e);
      case 2:
         return new ItemStack(Blocks.bf);
      case 3:
         return new ItemStack(Blocks.bf, 1, BlockSmoothBrick.EnumStonebrickType.MOSSY.a());
      case 4:
         return new ItemStack(Blocks.bf, 1, BlockSmoothBrick.EnumStonebrickType.CRACKED.a());
      case 5:
         return new ItemStack(Blocks.bf, 1, BlockSmoothBrick.EnumStonebrickType.CHISELED.a());
      default:
         return new ItemStack(Blocks.b);
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, float var4, int var5) {
      if(!var1.E && var1.U().b("doTileDrops")) {
         class_yu var6 = new class_yu(var1);
         var6.b((double)var2.p() + 0.5D, (double)var2.q(), (double)var2.r() + 0.5D, 0.0F, 0.0F);
         var1.a((Entity)var6);
         var6.E();
      }

   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(this, 1, var3.getBlock().e(var3));
   }

   public IBlockData a(int var1) {
      return this.u().set(a, BlockMonsterEggs.EnumMonsterEggVarient.a(var1));
   }

   public int e(IBlockData var1) {
      return ((BlockMonsterEggs.EnumMonsterEggVarient)var1.get(a)).a();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[BlockMonsterEggs.EnumMonsterEggVarient.values().length];

      static {
         try {
            a[BlockMonsterEggs.EnumMonsterEggVarient.b.ordinal()] = 1;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[BlockMonsterEggs.EnumMonsterEggVarient.c.ordinal()] = 2;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[BlockMonsterEggs.EnumMonsterEggVarient.d.ordinal()] = 3;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[BlockMonsterEggs.EnumMonsterEggVarient.e.ordinal()] = 4;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[BlockMonsterEggs.EnumMonsterEggVarient.f.ordinal()] = 5;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum EnumMonsterEggVarient implements class_or {
      a(0, "stone") {
         public IBlockData d() {
            return Blocks.b.u().set(BlockStone.a, BlockStone.EnumStoneVariant.STONE);
         }
      },
      b(1, "cobblestone", "cobble") {
         public IBlockData d() {
            return Blocks.e.u();
         }
      },
      c(2, "stone_brick", "brick") {
         public IBlockData d() {
            return Blocks.bf.u().set(BlockSmoothBrick.a, BlockSmoothBrick.EnumStonebrickType.DEFAULT);
         }
      },
      d(3, "mossy_brick", "mossybrick") {
         public IBlockData d() {
            return Blocks.bf.u().set(BlockSmoothBrick.a, BlockSmoothBrick.EnumStonebrickType.MOSSY);
         }
      },
      e(4, "cracked_brick", "crackedbrick") {
         public IBlockData d() {
            return Blocks.bf.u().set(BlockSmoothBrick.a, BlockSmoothBrick.EnumStonebrickType.CRACKED);
         }
      },
      f(5, "chiseled_brick", "chiseledbrick") {
         public IBlockData d() {
            return Blocks.bf.u().set(BlockSmoothBrick.a, BlockSmoothBrick.EnumStonebrickType.CHISELED);
         }
      };

      private static final BlockMonsterEggs.EnumMonsterEggVarient[] g = new BlockMonsterEggs.EnumMonsterEggVarient[values().length];
      private final int h;
      private final String i;
      private final String j;

      private EnumMonsterEggVarient(int var3, String var4) {
         this(var3, var4, var4);
      }

      private EnumMonsterEggVarient(int var3, String var4, String var5) {
         this.h = var3;
         this.i = var4;
         this.j = var5;
      }

      public int a() {
         return this.h;
      }

      public String toString() {
         return this.i;
      }

      public static BlockMonsterEggs.EnumMonsterEggVarient a(int var0) {
         if(var0 < 0 || var0 >= g.length) {
            var0 = 0;
         }

         return g[var0];
      }

      public String m() {
         return this.i;
      }

      public String c() {
         return this.j;
      }

      public abstract IBlockData d();

      public static BlockMonsterEggs.EnumMonsterEggVarient a(IBlockData var0) {
         BlockMonsterEggs.EnumMonsterEggVarient[] var1 = values();
         int var2 = var1.length;

         for(int var3 = 0; var3 < var2; ++var3) {
            BlockMonsterEggs.EnumMonsterEggVarient var4 = var1[var3];
            if(var0 == var4.d()) {
               return var4;
            }
         }

         return a;
      }

      // $FF: synthetic method
      EnumMonsterEggVarient(int var3, String var4, BlockMonsterEggs.SyntheticClass_1 var5) {
         this(var3, var4);
      }

      // $FF: synthetic method
      EnumMonsterEggVarient(int var3, String var4, String var5, BlockMonsterEggs.SyntheticClass_1 var6) {
         this(var3, var4, var5);
      }

      static {
         BlockMonsterEggs.EnumMonsterEggVarient[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            BlockMonsterEggs.EnumMonsterEggVarient var3 = var0[var2];
            g[var3.a()] = var3;
         }

      }
   }
}
