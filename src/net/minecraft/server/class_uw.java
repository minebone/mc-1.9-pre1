package net.minecraft.server;

import com.google.common.base.Predicate;
import java.util.Collections;
import java.util.List;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityCreature;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.ScoreboardTeamBase;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sl;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_vb;
import net.minecraft.server.class_ys;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_uw extends class_tj {
   private static final Logger a = LogManager.getLogger();
   private EntityInsentient b;
   private final Predicate c;
   private final class_ux.class_a_in_class_ux d;
   private class_rz e;

   public class_uw(EntityInsentient var1) {
      this.b = var1;
      if(var1 instanceof EntityCreature) {
         a.warn("Use NearestAttackableTargetGoal.class for PathfinerMob mobs!");
      }

      this.c = new Predicate() {
         public boolean a(Entity var1) {
            if(!(var1 instanceof EntityHuman)) {
               return false;
            } else if(((EntityHuman)var1).abilities.a) {
               return false;
            } else {
               double var2 = class_uw.this.f();
               if(var1.aJ()) {
                  var2 *= 0.800000011920929D;
               }

               if(var1.aM()) {
                  float var4 = ((EntityHuman)var1).cF();
                  if(var4 < 0.1F) {
                     var4 = 0.1F;
                  }

                  var2 *= (double)(0.7F * var4);
               }

               return (double)var1.g(class_uw.this.b) > var2?false:class_vb.a(class_uw.this.b, (class_rz)var1, false, true);
            }
         }

         // $FF: synthetic method
         public boolean apply(Object var1) {
            return this.a((Entity)var1);
         }
      };
      this.d = new class_ux.class_a_in_class_ux(var1);
   }

   public boolean a() {
      double var1 = this.f();
      List var3 = this.b.world.a(EntityHuman.class, this.b.bk().b(var1, 4.0D, var1), this.c);
      Collections.sort(var3, this.d);
      if(var3.isEmpty()) {
         return false;
      } else {
         this.e = (class_rz)var3.get(0);
         return true;
      }
   }

   public boolean b() {
      class_rz var1 = this.b.A();
      if(var1 == null) {
         return false;
      } else if(!var1.at()) {
         return false;
      } else if(var1 instanceof EntityHuman && ((EntityHuman)var1).abilities.a) {
         return false;
      } else {
         ScoreboardTeamBase var2 = this.b.aN();
         ScoreboardTeamBase var3 = var1.aN();
         if(var2 != null && var3 == var2) {
            return false;
         } else {
            double var4 = this.f();
            return this.b.h(var1) > var4 * var4?false:!(var1 instanceof EntityPlayer) || !((EntityPlayer)var1).c.isCreative();
         }
      }
   }

   public void c() {
      this.b.c(this.e);
      super.c();
   }

   public void d() {
      this.b.c((class_rz)null);
      super.c();
   }

   protected double f() {
      class_sl var1 = this.b.a((class_sk)class_ys.b);
      return var1 == null?16.0D:var1.e();
   }
}
