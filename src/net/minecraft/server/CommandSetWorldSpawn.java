package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutSpawnPosition;
import net.minecraft.server.ICommandListener;

public class CommandSetWorldSpawn extends CommandAbstract {
   public String c() {
      return "setworldspawn";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.setworldspawn.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      BlockPosition var4;
      if(var3.length == 0) {
         var4 = a(var2).c();
      } else {
         if(var3.length != 3 || var2.e() == null) {
            throw new class_cf("commands.setworldspawn.usage", new Object[0]);
         }

         var4 = a(var2, var3, 0, true);
      }

      var2.e().A(var4);
      var1.getPlayerList().sendAll((Packet)(new PacketPlayOutSpawnPosition(var4)));
      a(var2, this, "commands.setworldspawn.success", new Object[]{Integer.valueOf(var4.p()), Integer.valueOf(var4.q()), Integer.valueOf(var4.r())});
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length > 0 && var3.length <= 3?a(var3, 0, var4):Collections.emptyList();
   }
}
