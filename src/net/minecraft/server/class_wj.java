package net.minecraft.server;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.AttributeRanged;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityAnimal;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EnumMonsterType;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.MobEffectType;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.NameReferencingFileConverter;
import net.minecraft.server.World;
import net.minecraft.server.class_aau;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aoo;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_qh;
import net.minecraft.server.class_qk;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_qu;
import net.minecraft.server.class_rn;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_si;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sl;
import net.minecraft.server.class_tc;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_ti;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_to;
import net.minecraft.server.class_ub;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_uk;
import net.minecraft.server.class_wk;
import net.minecraft.server.class_wl;
import net.minecraft.server.class_wm;
import net.minecraft.server.class_ys;

public class class_wj extends EntityAnimal implements class_qh, class_si {
   private static final Predicate bB = new Predicate() {
      public boolean a(Entity var1) {
         return var1 instanceof class_wj && ((class_wj)var1).do1();
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((Entity)var1);
      }
   };
   private static final class_sk bC = (new AttributeRanged((class_sk)null, "horse.jumpStrength", 0.7D, 0.0D, 2.0D)).a("Jump Strength").a(true);
   private static final UUID bD = UUID.fromString("556E1665-8B10-40C8-8F9D-CF9B1667F295");
   private static final class_ke bE = DataWatcher.a(class_wj.class, class_kg.a);
   private static final class_ke bF = DataWatcher.a(class_wj.class, class_kg.b);
   private static final class_ke bG = DataWatcher.a(class_wj.class, class_kg.b);
   private static final class_ke bH = DataWatcher.a(class_wj.class, class_kg.m);
   private static final class_ke bI = DataWatcher.a(class_wj.class, class_kg.b);
   private static final String[] bJ = new String[]{"textures/entity/horse/horse_white.png", "textures/entity/horse/horse_creamy.png", "textures/entity/horse/horse_chestnut.png", "textures/entity/horse/horse_brown.png", "textures/entity/horse/horse_black.png", "textures/entity/horse/horse_gray.png", "textures/entity/horse/horse_darkbrown.png"};
   private static final String[] bK = new String[]{"hwh", "hcr", "hch", "hbr", "hbl", "hgr", "hdb"};
   private static final String[] bL = new String[]{null, "textures/entity/horse/horse_markings_white.png", "textures/entity/horse/horse_markings_whitefield.png", "textures/entity/horse/horse_markings_whitedots.png", "textures/entity/horse/horse_markings_blackdots.png"};
   private static final String[] bM = new String[]{"", "wo_", "wmo", "wdo", "bdo"};
   private final class_wm bN = new class_wm(this);
   private int bO;
   private int bP;
   private int bQ;
   public int bv;
   public int bw;
   protected boolean bx;
   private class_aau bR;
   private boolean bS;
   protected int bz;
   protected float bA;
   private boolean bT;
   private boolean bU;
   private int bV = 0;
   private float bW;
   private float bX;
   private float bY;
   private float bZ;
   private float ca;
   private float cb;
   private int cc;
   private String cd;
   private String[] ce = new String[3];
   private boolean cf = false;

   public class_wj(World var1) {
      super(var1);
      this.a(1.4F, 1.6F);
      this.fireProof = false;
      this.r(false);
      this.dJ();
   }

   protected void r() {
      this.bp.a(0, new class_tg(this));
      this.bp.a(1, new class_ub(this, 1.2D));
      this.bp.a(1, new class_uk(this, 1.2D));
      this.bp.a(2, new class_tc(this, 1.0D));
      this.bp.a(4, new class_ti(this, 1.0D));
      this.bp.a(6, new class_uf(this, 0.7D));
      this.bp.a(7, new class_to(this, EntityHuman.class, 6.0F));
      this.bp.a(8, new class_ue(this));
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)bE, (Object)Byte.valueOf((byte)0));
      this.datawatcher.a((class_ke)bF, (Object)Integer.valueOf(class_wl.HORSE.k()));
      this.datawatcher.a((class_ke)bG, (Object)Integer.valueOf(0));
      this.datawatcher.a((class_ke)bH, (Object)Optional.absent());
      this.datawatcher.a((class_ke)bI, (Object)Integer.valueOf(class_wk.NONE.a()));
   }

   public void a(class_wl var1) {
      this.datawatcher.b(bF, Integer.valueOf(var1.k()));
      this.dL();
   }

   public class_wl cZ() {
      return class_wl.a(((Integer)this.datawatcher.a(bF)).intValue());
   }

   public void l(int var1) {
      this.datawatcher.b(bG, Integer.valueOf(var1));
      this.dL();
   }

   public int da() {
      return ((Integer)this.datawatcher.a(bG)).intValue();
   }

   public String h_() {
      return this.o_()?this.be():this.cZ().d().c();
   }

   private boolean o(int var1) {
      return (((Byte)this.datawatcher.a(bE)).byteValue() & var1) != 0;
   }

   private void c(int var1, boolean var2) {
      byte var3 = ((Byte)this.datawatcher.a(bE)).byteValue();
      if(var2) {
         this.datawatcher.b(bE, Byte.valueOf((byte)(var3 | var1)));
      } else {
         this.datawatcher.b(bE, Byte.valueOf((byte)(var3 & ~var1)));
      }

   }

   public boolean db() {
      return !this.m_();
   }

   public boolean dc() {
      return this.o(2);
   }

   public boolean dd() {
      return this.db();
   }

   public UUID dh() {
      return (UUID)((Optional)this.datawatcher.a(bH)).orNull();
   }

   public void b(UUID var1) {
      this.datawatcher.b(bH, Optional.fromNullable(var1));
   }

   public float di() {
      return 0.5F;
   }

   public void a(boolean var1) {
      if(var1) {
         this.a(this.di());
      } else {
         this.a(1.0F);
      }

   }

   public boolean dj() {
      return this.bx;
   }

   public void o(boolean var1) {
      this.c(2, var1);
   }

   public void p(boolean var1) {
      this.bx = var1;
   }

   public boolean a(EntityHuman var1) {
      return !this.cZ().h() && super.a((EntityHuman)var1);
   }

   protected void q(float var1) {
      if(var1 > 6.0F && this.dm()) {
         this.u(false);
      }

   }

   public boolean dk() {
      return this.cZ().f() && this.o(8);
   }

   public class_wk dl() {
      return class_wk.a(((Integer)this.datawatcher.a(bI)).intValue());
   }

   public boolean dm() {
      return this.o(32);
   }

   public boolean dn() {
      return this.o(64);
   }

   public boolean do1() {
      return this.o(16);
   }

   public boolean dp() {
      return this.bS;
   }

   public void f(ItemStack var1) {
      class_wk var2 = class_wk.a(var1);
      this.datawatcher.b(bI, Integer.valueOf(var2.a()));
      this.dL();
      if(!this.world.E) {
         this.a((class_sk)class_ys.g).b(bD);
         int var3 = var2.c();
         if(var3 != 0) {
            this.a((class_sk)class_ys.g).b((new AttributeModifier(bD, "Horse armor bonus", (double)var3, 0)).a(false));
         }
      }

   }

   public void q(boolean var1) {
      this.c(16, var1);
   }

   public void r(boolean var1) {
      this.c(8, var1);
   }

   public void s(boolean var1) {
      this.bS = var1;
   }

   public void t(boolean var1) {
      this.c(4, var1);
   }

   public int dq() {
      return this.bz;
   }

   public void m(int var1) {
      this.bz = var1;
   }

   public int n(int var1) {
      int var2 = MathHelper.a(this.dq() + var1, 0, this.dw());
      this.m(var2);
      return var2;
   }

   public boolean a(DamageSource var1, float var2) {
      Entity var3 = var1.j();
      return this.aI() && var3 != null && this.y(var3)?false:super.a(var1, var2);
   }

   public boolean ap() {
      return !this.aI();
   }

   public boolean dr() {
      int var1 = MathHelper.c(this.locX);
      int var2 = MathHelper.c(this.locZ);
      this.world.b(new BlockPosition(var1, 0, var2));
      return true;
   }

   public void ds() {
      if(!this.world.E && this.dk()) {
         this.a(Item.a((Block)Blocks.ae), 1);
         this.r(false);
      }
   }

   private void dH() {
      this.dO();
      if(!this.ac()) {
         this.world.a((EntityHuman)null, this.locX, this.locY, this.locZ, class_ng.cp, this.by(), 1.0F, 1.0F + (this.random.nextFloat() - this.random.nextFloat()) * 0.2F);
      }

   }

   public void e(float var1, float var2) {
      if(var1 > 1.0F) {
         this.a(class_ng.ct, 0.4F, 1.0F);
      }

      int var3 = MathHelper.f((var1 * 0.5F - 3.0F) * var2);
      if(var3 > 0) {
         this.a(DamageSource.i, (float)var3);
         if(this.aI()) {
            Iterator var4 = this.bu().iterator();

            while(var4.hasNext()) {
               Entity var5 = (Entity)var4.next();
               var5.a(DamageSource.i, (float)var3);
            }
         }

         IBlockData var7 = this.world.getType(new BlockPosition(this.locX, this.locY - 0.2D - (double)this.lastYaw, this.locZ));
         Block var8 = var7.getBlock();
         if(var7.getMaterial() != Material.a && !this.ac()) {
            class_aoo var6 = var8.w();
            this.world.a((EntityHuman)null, this.locX, this.locY, this.locZ, var6.d(), this.by(), var6.a() * 0.5F, var6.b() * 0.75F);
         }

      }
   }

   private int dI() {
      class_wl var1 = this.cZ();
      return this.dk() && var1.f()?17:2;
   }

   private void dJ() {
      class_aau var1 = this.bR;
      this.bR = new class_aau("HorseChest", this.dI());
      this.bR.a(this.h_());
      if(var1 != null) {
         var1.b(this);
         int var2 = Math.min(var1.u_(), this.bR.u_());

         for(int var3 = 0; var3 < var2; ++var3) {
            ItemStack var4 = var1.a(var3);
            if(var4 != null) {
               this.bR.a(var3, var4.k());
            }
         }
      }

      this.bR.a(this);
      this.dK();
   }

   private void dK() {
      if(!this.world.E) {
         this.t(this.bR.a(0) != null);
         if(this.cZ().j()) {
            this.f(this.bR.a(1));
         }
      }

   }

   public void a(class_qu var1) {
      class_wk var2 = this.dl();
      boolean var3 = this.du();
      this.dK();
      if(this.ticksLived > 20) {
         if(var2 == class_wk.NONE && var2 != this.dl()) {
            this.a(class_ng.cm, 0.5F, 1.0F);
         } else if(var2 != this.dl()) {
            this.a(class_ng.cm, 0.5F, 1.0F);
         }

         if(!var3 && this.du()) {
            this.a(class_ng.cu, 0.5F, 1.0F);
         }
      }

   }

   public boolean cF() {
      this.dr();
      return super.cF();
   }

   protected class_wj a(Entity var1, double var2) {
      double var4 = Double.MAX_VALUE;
      Entity var6 = null;
      List var7 = this.world.a(var1, var1.bk().a(var2, var2, var2), bB);
      Iterator var8 = var7.iterator();

      while(var8.hasNext()) {
         Entity var9 = (Entity)var8.next();
         double var10 = var9.e(var1.locX, var1.locY, var1.locZ);
         if(var10 < var4) {
            var6 = var9;
            var4 = var10;
         }
      }

      return (class_wj)var6;
   }

   public double dt() {
      return this.a((class_sk)bC).e();
   }

   protected class_nf bR() {
      this.dO();
      return this.cZ().c();
   }

   protected class_nf bQ() {
      this.dO();
      if(this.random.nextInt(3) == 0) {
         this.dQ();
      }

      return this.cZ().b();
   }

   public boolean du() {
      return this.o(4);
   }

   protected class_nf G() {
      this.dO();
      if(this.random.nextInt(10) == 0 && !this.ce()) {
         this.dQ();
      }

      return this.cZ().a();
   }

   protected class_nf dv() {
      this.dO();
      this.dQ();
      class_wl var1 = this.cZ();
      return var1.h()?null:(var1.g()?class_ng.aw:class_ng.cl);
   }

   protected void a(BlockPosition var1, Block var2) {
      class_aoo var3 = var2.w();
      if(this.world.getType(var1.a()).getBlock() == Blocks.aH) {
         var3 = Blocks.aH.w();
      }

      if(!var2.u().getMaterial().d()) {
         class_wl var4 = this.cZ();
         if(this.aI() && !var4.g()) {
            ++this.cc;
            if(this.cc > 5 && this.cc % 3 == 0) {
               this.a(class_ng.cq, var3.a() * 0.15F, var3.b());
               if(var4 == class_wl.HORSE && this.random.nextInt(10) == 0) {
                  this.a(class_ng.cn, var3.a() * 0.6F, var3.b());
               }
            } else if(this.cc <= 5) {
               this.a(class_ng.cw, var3.a() * 0.15F, var3.b());
            }
         } else if(var3 == class_aoo.a) {
            this.a(class_ng.cw, var3.a() * 0.15F, var3.b());
         } else {
            this.a(class_ng.cv, var3.a() * 0.15F, var3.b());
         }
      }

   }

   protected void bz() {
      super.bz();
      this.bY().b(bC);
      this.a((class_sk)class_ys.a).a(53.0D);
      this.a((class_sk)class_ys.d).a(0.22499999403953552D);
   }

   public int cJ() {
      return 6;
   }

   public int dw() {
      return 100;
   }

   protected float cc() {
      return 0.8F;
   }

   public int C() {
      return 400;
   }

   private void dL() {
      this.cd = null;
   }

   public void f(EntityHuman var1) {
      if(!this.world.E && (!this.aI() || this.w(var1)) && this.dc()) {
         this.bR.a(this.h_());
         var1.a((class_wj)this, (IInventory)this.bR);
      }

   }

   public boolean a(EntityHuman var1, EnumHand var2, ItemStack var3) {
      if(var3 != null && var3.b() == Items.bT) {
         return super.a(var1, var2, var3);
      } else if(!this.dc() && this.cZ().h()) {
         return false;
      } else if(this.dc() && this.db() && var1.aJ()) {
         this.f(var1);
         return true;
      } else if(this.dd() && this.aI()) {
         return super.a(var1, var2, var3);
      } else {
         if(var3 != null) {
            if(this.cZ().j()) {
               class_wk var4 = class_wk.a(var3);
               if(var4 != class_wk.NONE) {
                  if(!this.dc()) {
                     this.dE();
                     return true;
                  }

                  this.f(var1);
                  return true;
               }
            }

            boolean var8 = false;
            if(!this.cZ().h()) {
               float var5 = 0.0F;
               short var6 = 0;
               byte var7 = 0;
               if(var3.b() == Items.Q) {
                  var5 = 2.0F;
                  var6 = 20;
                  var7 = 3;
               } else if(var3.b() == Items.bf) {
                  var5 = 1.0F;
                  var6 = 30;
                  var7 = 3;
               } else if(Block.a(var3.b()) == Blocks.cx) {
                  var5 = 20.0F;
                  var6 = 180;
               } else if(var3.b() == Items.e) {
                  var5 = 3.0F;
                  var6 = 60;
                  var7 = 3;
               } else if(var3.b() == Items.cg) {
                  var5 = 4.0F;
                  var6 = 60;
                  var7 = 5;
                  if(this.dc() && this.l() == 0) {
                     var8 = true;
                     this.c(var1);
                  }
               } else if(var3.b() == Items.aq) {
                  var5 = 10.0F;
                  var6 = 240;
                  var7 = 10;
                  if(this.dc() && this.l() == 0 && !this.df()) {
                     var8 = true;
                     this.c(var1);
                  }
               }

               if(this.bP() < this.bV() && var5 > 0.0F) {
                  this.b(var5);
                  var8 = true;
               }

               if(!this.db() && var6 > 0) {
                  if(!this.world.E) {
                     this.a(var6);
                  }

                  var8 = true;
               }

               if(var7 > 0 && (var8 || !this.dc()) && this.dq() < this.dw()) {
                  var8 = true;
                  if(!this.world.E) {
                     this.n(var7);
                  }
               }

               if(var8) {
                  this.dH();
               }
            }

            if(!this.dc() && !var8) {
               if(var3.a((EntityHuman)var1, (class_rz)this, (EnumHand)var2)) {
                  return true;
               }

               this.dE();
               return true;
            }

            if(!var8 && this.cZ().f() && !this.dk() && var3.b() == Item.a((Block)Blocks.ae)) {
               this.r(true);
               this.a(class_ng.ax, 1.0F, (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
               var8 = true;
               this.dJ();
            }

            if(!var8 && this.dd() && !this.du() && var3.b() == Items.aC) {
               this.f(var1);
               return true;
            }

            if(var8) {
               if(!var1.abilities.d) {
                  --var3.b;
               }

               return true;
            }
         }

         if(this.dd() && !this.aI()) {
            if(var3 != null && var3.a((EntityHuman)var1, (class_rz)this, (EnumHand)var2)) {
               return true;
            } else {
               this.h(var1);
               return true;
            }
         } else {
            return super.a(var1, var2, var3);
         }
      }
   }

   private void h(EntityHuman var1) {
      var1.yaw = this.yaw;
      var1.pitch = this.pitch;
      this.u(false);
      this.v(false);
      if(!this.world.E) {
         var1.m(this);
      }

   }

   protected boolean ce() {
      return this.aI() && this.du()?true:this.dm() || this.dn();
   }

   public boolean e(ItemStack var1) {
      return false;
   }

   private void dN() {
      this.bv = 1;
   }

   public void a(DamageSource var1) {
      super.a((DamageSource)var1);
      if(!this.world.E) {
         this.dF();
      }

   }

   public void n() {
      if(this.random.nextInt(200) == 0) {
         this.dN();
      }

      super.n();
      if(!this.world.E) {
         if(this.random.nextInt(900) == 0 && this.aA == 0) {
            this.b(1.0F);
         }

         if(!this.dm() && !this.aI() && this.random.nextInt(300) == 0 && this.world.getType(new BlockPosition(MathHelper.c(this.locX), MathHelper.c(this.locY) - 1, MathHelper.c(this.locZ))).getBlock() == Blocks.c) {
            this.u(true);
         }

         if(this.dm() && ++this.bO > 50) {
            this.bO = 0;
            this.u(false);
         }

         if(this.do1() && !this.db() && !this.dm()) {
            class_wj var1 = this.a(this, 16.0D);
            if(var1 != null && this.h(var1) > 4.0D) {
               this.h.a((Entity)var1);
            }
         }

         if(this.dG() && this.bV++ >= 18000) {
            this.S();
         }
      }

   }

   public void m() {
      super.m();
      if(this.world.E && this.datawatcher.a()) {
         this.datawatcher.e();
         this.dL();
      }

      if(this.bP > 0 && ++this.bP > 30) {
         this.bP = 0;
         this.c(128, false);
      }

      if(this.bw() && this.bQ > 0 && ++this.bQ > 20) {
         this.bQ = 0;
         this.v(false);
      }

      if(this.bv > 0 && ++this.bv > 8) {
         this.bv = 0;
      }

      if(this.bw > 0) {
         ++this.bw;
         if(this.bw > 300) {
            this.bw = 0;
         }
      }

      this.bX = this.bW;
      if(this.dm()) {
         this.bW += (1.0F - this.bW) * 0.4F + 0.05F;
         if(this.bW > 1.0F) {
            this.bW = 1.0F;
         }
      } else {
         this.bW += (0.0F - this.bW) * 0.4F - 0.05F;
         if(this.bW < 0.0F) {
            this.bW = 0.0F;
         }
      }

      this.bZ = this.bY;
      if(this.dn()) {
         this.bX = this.bW = 0.0F;
         this.bY += (1.0F - this.bY) * 0.4F + 0.05F;
         if(this.bY > 1.0F) {
            this.bY = 1.0F;
         }
      } else {
         this.bT = false;
         this.bY += (0.8F * this.bY * this.bY * this.bY - this.bY) * 0.6F - 0.05F;
         if(this.bY < 0.0F) {
            this.bY = 0.0F;
         }
      }

      this.cb = this.ca;
      if(this.o(128)) {
         this.ca += (1.0F - this.ca) * 0.7F + 0.05F;
         if(this.ca > 1.0F) {
            this.ca = 1.0F;
         }
      } else {
         this.ca += (0.0F - this.ca) * 0.7F - 0.05F;
         if(this.ca < 0.0F) {
            this.ca = 0.0F;
         }
      }

   }

   private void dO() {
      if(!this.world.E) {
         this.bP = 1;
         this.c(128, true);
      }

   }

   private boolean dP() {
      return !this.aI() && !this.aH() && this.dc() && this.db() && this.cZ().i() && this.bP() >= this.bV() && this.df();
   }

   public void u(boolean var1) {
      this.c(32, var1);
   }

   public void v(boolean var1) {
      if(var1) {
         this.u(false);
      }

      this.c(64, var1);
   }

   private void dQ() {
      if(this.bw()) {
         this.bQ = 1;
         this.v(true);
      }

   }

   public void dE() {
      this.dQ();
      class_nf var1 = this.dv();
      if(var1 != null) {
         this.a(var1, this.cc(), this.cd());
      }

   }

   public void dF() {
      this.a((Entity)this, (class_aau)this.bR);
      this.ds();
   }

   private void a(Entity var1, class_aau var2) {
      if(var2 != null && !this.world.E) {
         for(int var3 = 0; var3 < var2.u_(); ++var3) {
            ItemStack var4 = var2.a(var3);
            if(var4 != null) {
               this.a(var4, 0.0F);
            }
         }

      }
   }

   public boolean g(EntityHuman var1) {
      this.b(var1.getUniqueId());
      this.o(true);
      return true;
   }

   public void g(float var1, float var2) {
      if(this.aI() && this.cK() && this.du()) {
         class_rz var3 = (class_rz)this.bs();
         this.lastYaw = this.yaw = var3.yaw;
         this.pitch = var3.pitch * 0.5F;
         this.b(this.yaw, this.pitch);
         this.aO = this.aM = this.yaw;
         var1 = var3.bd * 0.5F;
         var2 = var3.be;
         if(var2 <= 0.0F) {
            var2 *= 0.25F;
            this.cc = 0;
         }

         if(this.onGround && this.bA == 0.0F && this.dn() && !this.bT) {
            var1 = 0.0F;
            var2 = 0.0F;
         }

         if(this.bA > 0.0F && !this.dj() && this.onGround) {
            this.motY = this.dt() * (double)this.bA;
            if(this.a((MobEffectType)MobEffectList.h)) {
               this.motY += (double)((float)(this.b((MobEffectType)MobEffectList.h).c() + 1) * 0.1F);
            }

            this.p(true);
            this.ai = true;
            if(var2 > 0.0F) {
               float var4 = MathHelper.a(this.yaw * 0.017453292F);
               float var5 = MathHelper.b(this.yaw * 0.017453292F);
               this.motX += (double)(-0.4F * var4 * this.bA);
               this.motZ += (double)(0.4F * var5 * this.bA);
               this.a(class_ng.cs, 0.4F, 1.0F);
            }

            this.bA = 0.0F;
         }

         this.P = 1.0F;
         this.aQ = this.cj() * 0.1F;
         if(this.bw()) {
            this.l((float)this.a((class_sk)class_ys.d).e());
            super.g(var1, var2);
         } else if(var3 instanceof EntityHuman) {
            this.motX = 0.0D;
            this.motY = 0.0D;
            this.motZ = 0.0D;
         }

         if(this.onGround) {
            this.bA = 0.0F;
            this.p(false);
         }

         this.aE = this.aF;
         double var9 = this.locX - this.lastX;
         double var6 = this.locZ - this.lastZ;
         float var8 = MathHelper.a(var9 * var9 + var6 * var6) * 4.0F;
         if(var8 > 1.0F) {
            var8 = 1.0F;
         }

         this.aF += (var8 - this.aF) * 0.4F;
         this.aG += this.aF;
      } else {
         this.P = 0.5F;
         this.aQ = 0.02F;
         super.g(var1, var2);
      }
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("EatingHaystack", this.dm());
      var1.a("ChestedHorse", this.dk());
      var1.a("HasReproduced", this.dp());
      var1.a("Bred", this.do1());
      var1.a("Type", this.cZ().k());
      var1.a("Variant", this.da());
      var1.a("Temper", this.dq());
      var1.a("Tame", this.dc());
      var1.a("SkeletonTrap", this.dG());
      var1.a("SkeletonTrapTime", this.bV);
      if(this.dh() != null) {
         var1.a("OwnerUUID", this.dh().toString());
      }

      if(this.dk()) {
         NBTTagList var2 = new NBTTagList();

         for(int var3 = 2; var3 < this.bR.u_(); ++var3) {
            ItemStack var4 = this.bR.a(var3);
            if(var4 != null) {
               NBTTagCompound var5 = new NBTTagCompound();
               var5.a("Slot", (byte)var3);
               var4.b(var5);
               var2.a((NBTTag)var5);
            }
         }

         var1.a((String)"Items", (NBTTag)var2);
      }

      if(this.bR.a(1) != null) {
         var1.a((String)"ArmorItem", (NBTTag)this.bR.a(1).b(new NBTTagCompound()));
      }

      if(this.bR.a(0) != null) {
         var1.a((String)"SaddleItem", (NBTTag)this.bR.a(0).b(new NBTTagCompound()));
      }

   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.u(var1.p("EatingHaystack"));
      this.q(var1.p("Bred"));
      this.r(var1.p("ChestedHorse"));
      this.s(var1.p("HasReproduced"));
      this.a(class_wl.a(var1.h("Type")));
      this.l(var1.h("Variant"));
      this.m(var1.h("Temper"));
      this.o(var1.p("Tame"));
      this.x(var1.p("SkeletonTrap"));
      this.bV = var1.h("SkeletonTrapTime");
      String var2 = "";
      if(var1.b("OwnerUUID", 8)) {
         var2 = var1.l("OwnerUUID");
      } else {
         String var3 = var1.l("Owner");
         var2 = NameReferencingFileConverter.a(this.h(), var3);
      }

      if(!var2.isEmpty()) {
         this.b(UUID.fromString(var2));
      }

      class_sl var8 = this.bY().a("Speed");
      if(var8 != null) {
         this.a((class_sk)class_ys.d).a(var8.b() * 0.25D);
      }

      if(this.dk()) {
         NBTTagList var4 = var1.c("Items", 10);
         this.dJ();

         for(int var5 = 0; var5 < var4.c(); ++var5) {
            NBTTagCompound var6 = var4.b(var5);
            int var7 = var6.f("Slot") & 255;
            if(var7 >= 2 && var7 < this.bR.u_()) {
               this.bR.a(var7, ItemStack.a(var6));
            }
         }
      }

      ItemStack var9;
      if(var1.b("ArmorItem", 10)) {
         var9 = ItemStack.a(var1.o("ArmorItem"));
         if(var9 != null && class_wk.b(var9.b())) {
            this.bR.a(1, var9);
         }
      }

      if(var1.b("SaddleItem", 10)) {
         var9 = ItemStack.a(var1.o("SaddleItem"));
         if(var9 != null && var9.b() == Items.aC) {
            this.bR.a(0, var9);
         }
      }

      this.dK();
   }

   public boolean a(EntityAnimal var1) {
      if(var1 == this) {
         return false;
      } else if(var1.getClass() != this.getClass()) {
         return false;
      } else {
         class_wj var2 = (class_wj)var1;
         if(this.dP() && var2.dP()) {
            class_wl var3 = this.cZ();
            class_wl var4 = var2.cZ();
            return var3 == var4 || var3 == class_wl.HORSE && var4 == class_wl.DONKEY || var3 == class_wl.DONKEY && var4 == class_wl.HORSE;
         } else {
            return false;
         }
      }
   }

   public class_rn a(class_rn var1) {
      class_wj var2 = (class_wj)var1;
      class_wj var3 = new class_wj(this.world);
      class_wl var4 = this.cZ();
      class_wl var5 = var2.cZ();
      class_wl var6 = class_wl.HORSE;
      if(var4 == var5) {
         var6 = var4;
      } else if(var4 == class_wl.HORSE && var5 == class_wl.DONKEY || var4 == class_wl.DONKEY && var5 == class_wl.HORSE) {
         var6 = class_wl.MULE;
      }

      if(var6 == class_wl.HORSE) {
         int var8 = this.random.nextInt(9);
         int var7;
         if(var8 < 4) {
            var7 = this.da() & 255;
         } else if(var8 < 8) {
            var7 = var2.da() & 255;
         } else {
            var7 = this.random.nextInt(7);
         }

         int var9 = this.random.nextInt(5);
         if(var9 < 2) {
            var7 |= this.da() & '\uff00';
         } else if(var9 < 4) {
            var7 |= var2.da() & '\uff00';
         } else {
            var7 |= this.random.nextInt(5) << 8 & '\uff00';
         }

         var3.l(var7);
      }

      var3.a(var6);
      double var14 = this.a((class_sk)class_ys.a).b() + var1.a((class_sk)class_ys.a).b() + (double)this.dR();
      var3.a((class_sk)class_ys.a).a(var14 / 3.0D);
      double var13 = this.a((class_sk)bC).b() + var1.a((class_sk)bC).b() + this.dS();
      var3.a((class_sk)bC).a(var13 / 3.0D);
      double var11 = this.a((class_sk)class_ys.d).b() + var1.a((class_sk)class_ys.d).b() + this.dT();
      var3.a((class_sk)class_ys.d).a(var11 / 3.0D);
      return var3;
   }

   public class_sc a(class_qk var1, class_sc var2) {
      Object var7 = super.a(var1, var2);
      class_wl var3 = class_wl.HORSE;
      int var4 = 0;
      if(var7 instanceof class_wj.class_a_in_class_wj) {
         var3 = ((class_wj.class_a_in_class_wj)var7).a;
         var4 = ((class_wj.class_a_in_class_wj)var7).b & 255 | this.random.nextInt(5) << 8;
      } else {
         if(this.random.nextInt(10) == 0) {
            var3 = class_wl.DONKEY;
         } else {
            int var5 = this.random.nextInt(7);
            int var6 = this.random.nextInt(5);
            var3 = class_wl.HORSE;
            var4 = var5 | var6 << 8;
         }

         var7 = new class_wj.class_a_in_class_wj(var3, var4);
      }

      this.a(var3);
      this.l(var4);
      if(this.random.nextInt(5) == 0) {
         this.b_(-24000);
      }

      if(var3.h()) {
         this.a((class_sk)class_ys.a).a(15.0D);
         this.a((class_sk)class_ys.d).a(0.20000000298023224D);
      } else {
         this.a((class_sk)class_ys.a).a((double)this.dR());
         if(var3 == class_wl.HORSE) {
            this.a((class_sk)class_ys.d).a(this.dT());
         } else {
            this.a((class_sk)class_ys.d).a(0.17499999701976776D);
         }
      }

      if(var3.g()) {
         this.a((class_sk)bC).a(0.5D);
      } else {
         this.a((class_sk)bC).a(this.dS());
      }

      this.c(this.bV());
      return (class_sc)var7;
   }

   public boolean cK() {
      Entity var1 = this.bs();
      return var1 instanceof class_rz;
   }

   public boolean b() {
      return this.du();
   }

   public void b(int var1) {
      this.bT = true;
      this.dQ();
   }

   public void r_() {
   }

   public void k(Entity var1) {
      super.k(var1);
      if(var1 instanceof EntityInsentient) {
         EntityInsentient var2 = (EntityInsentient)var1;
         this.aM = var2.aM;
      }

      if(this.bZ > 0.0F) {
         float var6 = MathHelper.a(this.aM * 0.017453292F);
         float var3 = MathHelper.b(this.aM * 0.017453292F);
         float var4 = 0.7F * this.bZ;
         float var5 = 0.15F * this.bZ;
         var1.b(this.locX + (double)(var4 * var6), this.locY + this.ax() + var1.aw() + (double)var5, this.locZ - (double)(var4 * var3));
         if(var1 instanceof class_rz) {
            ((class_rz)var1).aM = this.aM;
         }
      }

   }

   public double ax() {
      double var1 = super.ax();
      if(this.cZ() == class_wl.SKELETON) {
         var1 -= 0.1875D;
      } else if(this.cZ() == class_wl.DONKEY) {
         var1 -= 0.25D;
      }

      return var1;
   }

   private float dR() {
      return 15.0F + (float)this.random.nextInt(8) + (float)this.random.nextInt(9);
   }

   private double dS() {
      return 0.4000000059604645D + this.random.nextDouble() * 0.2D + this.random.nextDouble() * 0.2D + this.random.nextDouble() * 0.2D;
   }

   private double dT() {
      return (0.44999998807907104D + this.random.nextDouble() * 0.3D + this.random.nextDouble() * 0.3D + this.random.nextDouble() * 0.3D) * 0.25D;
   }

   public boolean dG() {
      return this.bU;
   }

   public void x(boolean var1) {
      if(var1 != this.bU) {
         this.bU = var1;
         if(var1) {
            this.bp.a(1, this.bN);
         } else {
            this.bp.a((class_tj)this.bN);
         }
      }

   }

   public boolean n_() {
      return false;
   }

   public float bm() {
      return this.length;
   }

   public boolean c(int var1, ItemStack var2) {
      if(var1 == 499 && this.cZ().f()) {
         if(var2 == null && this.dk()) {
            this.r(false);
            this.dJ();
            return true;
         }

         if(var2 != null && var2.b() == Item.a((Block)Blocks.ae) && !this.dk()) {
            this.r(true);
            this.dJ();
            return true;
         }
      }

      int var3 = var1 - 400;
      if(var3 >= 0 && var3 < 2 && var3 < this.bR.u_()) {
         if(var3 == 0 && var2 != null && var2.b() != Items.aC) {
            return false;
         } else if(var3 != 1 || (var2 == null || class_wk.b(var2.b())) && this.cZ().j()) {
            this.bR.a(var3, var2);
            this.dK();
            return true;
         } else {
            return false;
         }
      } else {
         int var4 = var1 - 500 + 2;
         if(var4 >= 2 && var4 < this.bR.u_()) {
            this.bR.a(var4, var2);
            return true;
         } else {
            return false;
         }
      }
   }

   public Entity bs() {
      return this.bt().isEmpty()?null:(Entity)this.bt().get(0);
   }

   public EnumMonsterType bZ() {
      return this.cZ().h()?EnumMonsterType.UNDEAD:EnumMonsterType.UNDEFINED;
   }

   protected class_kk J() {
      return this.cZ().l();
   }

   public static class class_a_in_class_wj implements class_sc {
      public class_wl a;
      public int b;

      public class_a_in_class_wj(class_wl var1, int var2) {
         this.a = var1;
         this.b = var2;
      }
   }
}
