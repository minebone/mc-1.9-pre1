package net.minecraft.server;

import com.google.common.collect.Sets;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Items;
import net.minecraft.server.MobEffect;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_afd;
import net.minecraft.server.class_aff;
import net.minecraft.server.class_afg;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_zl;

public class class_aac extends class_zl {
   private static final class_ke f = DataWatcher.a(class_aac.class, class_kg.b);
   private class_afd g = class_afg.a;
   private final Set h = Sets.newHashSet();

   public class_aac(World var1) {
      super(var1);
   }

   public class_aac(World var1, double var2, double var4, double var6) {
      super(var1, var2, var4, var6);
   }

   public class_aac(World var1, class_rz var2) {
      super(var1, var2);
   }

   public void a(ItemStack var1) {
      if(var1.b() == Items.i) {
         this.g = class_aff.c(var1.o());
         List var2 = class_aff.b(var1);
         if(!var2.isEmpty()) {
            Iterator var3 = var2.iterator();

            while(var3.hasNext()) {
               MobEffect var4 = (MobEffect)var3.next();
               this.h.add(new MobEffect(var4));
            }
         }

         this.datawatcher.b(f, Integer.valueOf(class_aff.a((Collection)class_aff.a((class_afd)this.g, (Collection)var2))));
      } else if(var1.b() == Items.g) {
         this.g = class_afg.a;
         this.h.clear();
         this.datawatcher.b(f, Integer.valueOf(0));
      }

   }

   public void a(MobEffect var1) {
      this.h.add(var1);
      this.Q().b(f, Integer.valueOf(class_aff.a((Collection)class_aff.a((class_afd)this.g, (Collection)this.h))));
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)f, (Object)Integer.valueOf(0));
   }

   public void m() {
      super.m();
      if(this.world.E) {
         if(this.a) {
            if(this.b % 5 == 0) {
               this.b(1);
            }
         } else {
            this.b(2);
         }
      } else if(this.a && this.b != 0 && !this.h.isEmpty() && this.b >= 600) {
         this.world.a((Entity)this, (byte)0);
         this.g = class_afg.a;
         this.h.clear();
         this.datawatcher.b(f, Integer.valueOf(0));
      }

   }

   private void b(int var1) {
      int var2 = this.n();
      if(var2 != 0 && var1 > 0) {
         double var3 = (double)(var2 >> 16 & 255) / 255.0D;
         double var5 = (double)(var2 >> 8 & 255) / 255.0D;
         double var7 = (double)(var2 >> 0 & 255) / 255.0D;

         for(int var9 = 0; var9 < var1; ++var9) {
            this.world.a(EnumParticle.SPELL_MOB, this.locX + (this.random.nextDouble() - 0.5D) * (double)this.width, this.locY + this.random.nextDouble() * (double)this.length, this.locZ + (this.random.nextDouble() - 0.5D) * (double)this.width, var3, var5, var7, new int[0]);
         }

      }
   }

   public int n() {
      return ((Integer)this.datawatcher.a(f)).intValue();
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      if(this.g != class_afg.a && this.g != null) {
         var1.a("Potion", ((class_kk)class_afd.a.b(this.g)).toString());
      }

      if(!this.h.isEmpty()) {
         NBTTagList var2 = new NBTTagList();
         Iterator var3 = this.h.iterator();

         while(var3.hasNext()) {
            MobEffect var4 = (MobEffect)var3.next();
            var2.a((NBTTag)var4.a(new NBTTagCompound()));
         }

         var1.a((String)"CustomPotionEffects", (NBTTag)var2);
      }

   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      if(var1.b("Potion", 8)) {
         this.g = class_aff.c(var1);
      }

      Iterator var2 = class_aff.b(var1).iterator();

      while(var2.hasNext()) {
         MobEffect var3 = (MobEffect)var2.next();
         this.a(var3);
      }

      if(this.g != class_afg.a || !this.h.isEmpty()) {
         this.datawatcher.b(f, Integer.valueOf(class_aff.a((Collection)class_aff.a((class_afd)this.g, (Collection)this.h))));
      }

   }

   protected void a(class_rz var1) {
      super.a(var1);
      Iterator var2 = this.g.a().iterator();

      MobEffect var3;
      while(var2.hasNext()) {
         var3 = (MobEffect)var2.next();
         var1.c(new MobEffect(var3.a(), var3.b() / 8, var3.c(), var3.d(), var3.e()));
      }

      if(!this.h.isEmpty()) {
         var2 = this.h.iterator();

         while(var2.hasNext()) {
            var3 = (MobEffect)var2.next();
            var1.c(var3);
         }
      }

   }

   protected ItemStack j() {
      if(this.h.isEmpty() && this.g == class_afg.a) {
         return new ItemStack(Items.g);
      } else {
         ItemStack var1 = new ItemStack(Items.i);
         class_aff.a(var1, this.g);
         class_aff.a((ItemStack)var1, (Collection)this.h);
         return var1;
      }
   }
}
