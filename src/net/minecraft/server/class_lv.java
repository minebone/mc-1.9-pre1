package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.collect.AbstractIterator;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import net.minecraft.server.Chunk;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_ahm;
import net.minecraft.server.class_asu;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.class_lu;
import net.minecraft.server.class_ol;
import net.minecraft.server.MathHelper;

public class class_lv {
   private static final Predicate a = new Predicate() {
      public boolean a(EntityPlayer var1) {
         return var1 != null && !var1.y();
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((EntityPlayer)var1);
      }
   };
   private static final Predicate b = new Predicate() {
      public boolean a(EntityPlayer var1) {
         return var1 != null && (!var1.y() || var1.x().U().b("spectatorsGenerateChunks"));
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((EntityPlayer)var1);
      }
   };
   private final WorldServer c;
   private final List d = Lists.newArrayList();
   private final class_ol e = new class_ol();
   private final Set f = Sets.newHashSet();
   private final List g = Lists.newLinkedList();
   private final List h = Lists.newLinkedList();
   private final List i = Lists.newArrayList();
   private int j;
   private long k;
   private boolean l = true;
   private boolean m = true;

   public class_lv(WorldServer var1) {
      this.c = var1;
      this.a(var1.u().getPlayerList().s());
   }

   public WorldServer a() {
      return this.c;
   }

   public Iterator b() {
      final Iterator var1 = this.i.iterator();
      return new AbstractIterator() {
         protected Chunk a() {
            while(true) {
               if(var1.hasNext()) {
                  class_lu var1x = (class_lu)var1.next();
                  Chunk var2 = var1x.f();
                  if(var2 == null) {
                     continue;
                  }

                  if(!var2.v() && var2.u()) {
                     return var2;
                  }

                  if(!var2.j()) {
                     return var2;
                  }

                  if(!var1x.a(128.0D, class_lv.a)) {
                     continue;
                  }

                  return var2;
               }

               return (Chunk)this.endOfData();
            }
         }

         // $FF: synthetic method
         protected Object computeNext() {
            return this.a();
         }
      };
   }

   public void c() {
      long var1 = this.c.P();
      int var3;
      class_lu var4;
      if(var1 - this.k > 8000L) {
         this.k = var1;

         for(var3 = 0; var3 < this.i.size(); ++var3) {
            var4 = (class_lu)this.i.get(var3);
            var4.d();
            var4.c();
         }
      }

      if(!this.f.isEmpty()) {
         Iterator var9 = this.f.iterator();

         while(var9.hasNext()) {
            var4 = (class_lu)var9.next();
            var4.d();
         }

         this.f.clear();
      }

      if(this.l && var1 % 4L == 0L) {
         this.l = false;
         Collections.sort(this.h, new Comparator() {
            public int a(class_lu var1, class_lu var2) {
               return ComparisonChain.start().compare(var1.g(), var2.g()).result();
            }

            // $FF: synthetic method
            public int compare(Object var1, Object var2) {
               return this.a((class_lu)var1, (class_lu)var2);
            }
         });
      }

      if(this.m && var1 % 4L == 2L) {
         this.m = false;
         Collections.sort(this.g, new Comparator() {
            public int a(class_lu var1, class_lu var2) {
               return ComparisonChain.start().compare(var1.g(), var2.g()).result();
            }

            // $FF: synthetic method
            public int compare(Object var1, Object var2) {
               return this.a((class_lu)var1, (class_lu)var2);
            }
         });
      }

      if(!this.h.isEmpty()) {
         long var10 = System.nanoTime() + 50000000L;
         int var5 = 49;
         Iterator var6 = this.h.iterator();

         while(var6.hasNext()) {
            class_lu var7 = (class_lu)var6.next();
            if(var7.f() == null) {
               boolean var8 = var7.a(b);
               if(var7.a(var8)) {
                  var6.remove();
                  if(var7.b()) {
                     this.g.remove(var7);
                  }

                  --var5;
                  if(var5 < 0 || System.nanoTime() > var10) {
                     break;
                  }
               }
            }
         }
      }

      if(!this.g.isEmpty()) {
         var3 = 81;
         Iterator var11 = this.g.iterator();

         while(var11.hasNext()) {
            class_lu var13 = (class_lu)var11.next();
            if(var13.b()) {
               var11.remove();
               --var3;
               if(var3 < 0) {
                  break;
               }
            }
         }
      }

      if(this.d.isEmpty()) {
         class_asu var12 = this.c.s;
         if(!var12.e()) {
            this.c.r().b();
         }
      }

   }

   public boolean a(int var1, int var2) {
      long var3 = d(var1, var2);
      return this.e.a(var3) != null;
   }

   public class_lu b(int var1, int var2) {
      return (class_lu)this.e.a(d(var1, var2));
   }

   private class_lu c(int var1, int var2) {
      long var3 = d(var1, var2);
      class_lu var5 = (class_lu)this.e.a(var3);
      if(var5 == null) {
         var5 = new class_lu(this, var1, var2);
         this.e.a(var3, var5);
         this.i.add(var5);
         if(var5.f() == null) {
            this.h.add(var5);
         }

         if(!var5.b()) {
            this.g.add(var5);
         }
      }

      return var5;
   }

   public void a(BlockPosition var1) {
      int var2 = var1.p() >> 4;
      int var3 = var1.r() >> 4;
      class_lu var4 = this.b(var2, var3);
      if(var4 != null) {
         var4.a(var1.p() & 15, var1.q(), var1.r() & 15);
      }

   }

   public void a(EntityPlayer var1) {
      int var2 = (int)var1.locX >> 4;
      int var3 = (int)var1.locZ >> 4;
      var1.d = var1.locX;
      var1.e = var1.locZ;

      for(int var4 = var2 - this.j; var4 <= var2 + this.j; ++var4) {
         for(int var5 = var3 - this.j; var5 <= var3 + this.j; ++var5) {
            this.c(var4, var5).a(var1);
         }
      }

      this.d.add(var1);
      this.e();
   }

   public void b(EntityPlayer var1) {
      int var2 = (int)var1.d >> 4;
      int var3 = (int)var1.e >> 4;

      for(int var4 = var2 - this.j; var4 <= var2 + this.j; ++var4) {
         for(int var5 = var3 - this.j; var5 <= var3 + this.j; ++var5) {
            class_lu var6 = this.b(var4, var5);
            if(var6 != null) {
               var6.b(var1);
            }
         }
      }

      this.d.remove(var1);
      this.e();
   }

   private boolean a(int var1, int var2, int var3, int var4, int var5) {
      int var6 = var1 - var3;
      int var7 = var2 - var4;
      return var6 >= -var5 && var6 <= var5?var7 >= -var5 && var7 <= var5:false;
   }

   public void c(EntityPlayer var1) {
      int var2 = (int)var1.locX >> 4;
      int var3 = (int)var1.locZ >> 4;
      double var4 = var1.d - var1.locX;
      double var6 = var1.e - var1.locZ;
      double var8 = var4 * var4 + var6 * var6;
      if(var8 >= 64.0D) {
         int var10 = (int)var1.d >> 4;
         int var11 = (int)var1.e >> 4;
         int var12 = this.j;
         int var13 = var2 - var10;
         int var14 = var3 - var11;
         if(var13 != 0 || var14 != 0) {
            for(int var15 = var2 - var12; var15 <= var2 + var12; ++var15) {
               for(int var16 = var3 - var12; var16 <= var3 + var12; ++var16) {
                  if(!this.a(var15, var16, var10, var11, var12)) {
                     this.c(var15, var16).a(var1);
                  }

                  if(!this.a(var15 - var13, var16 - var14, var2, var3, var12)) {
                     class_lu var17 = this.b(var15 - var13, var16 - var14);
                     if(var17 != null) {
                        var17.b(var1);
                     }
                  }
               }
            }

            var1.d = var1.locX;
            var1.e = var1.locZ;
            this.e();
         }
      }
   }

   public boolean a(EntityPlayer var1, int var2, int var3) {
      class_lu var4 = this.b(var2, var3);
      return var4 != null && var4.d(var1) && var4.e();
   }

   public void a(int var1) {
      var1 = MathHelper.a(var1, 3, 32);
      if(var1 != this.j) {
         int var2 = var1 - this.j;
         ArrayList var3 = Lists.newArrayList((Iterable)this.d);
         Iterator var4 = var3.iterator();

         while(true) {
            while(var4.hasNext()) {
               EntityPlayer var5 = (EntityPlayer)var4.next();
               int var6 = (int)var5.locX >> 4;
               int var7 = (int)var5.locZ >> 4;
               int var8;
               int var9;
               if(var2 > 0) {
                  for(var8 = var6 - var1; var8 <= var6 + var1; ++var8) {
                     for(var9 = var7 - var1; var9 <= var7 + var1; ++var9) {
                        class_lu var10 = this.c(var8, var9);
                        if(!var10.d(var5)) {
                           var10.a(var5);
                        }
                     }
                  }
               } else {
                  for(var8 = var6 - this.j; var8 <= var6 + this.j; ++var8) {
                     for(var9 = var7 - this.j; var9 <= var7 + this.j; ++var9) {
                        if(!this.a(var8, var9, var6, var7, var1)) {
                           this.c(var8, var9).b(var5);
                        }
                     }
                  }
               }
            }

            this.j = var1;
            this.e();
            return;
         }
      }
   }

   private void e() {
      this.l = true;
      this.m = true;
   }

   public static int b(int var0) {
      return var0 * 16 - 16;
   }

   private static long d(int var0, int var1) {
      return (long)var0 + 2147483647L | (long)var1 + 2147483647L << 32;
   }

   public void a(class_lu var1) {
      this.f.add(var1);
   }

   public void b(class_lu var1) {
      class_ahm var2 = var1.a();
      long var3 = d(var2.a, var2.b);
      var1.c();
      this.e.d(var3);
      this.i.remove(var1);
      this.f.remove(var1);
      this.g.remove(var1);
      this.h.remove(var1);
      this.a().r().a(var2.a, var2.b);
   }
}
