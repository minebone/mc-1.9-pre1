package net.minecraft.server;

import com.google.common.collect.Sets;
import java.util.Iterator;
import java.util.Set;
import net.minecraft.server.AchievementList;
import net.minecraft.server.Block;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityAnimal;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.PathfinderGoalTempt;
import net.minecraft.server.Statistic;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rn;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_tc;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_ti;
import net.minecraft.server.class_to;
import net.minecraft.server.class_ub;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_xz;
import net.minecraft.server.class_yq;
import net.minecraft.server.class_ys;

public class class_wb extends EntityAnimal {
   private static final class_ke bv = DataWatcher.a(class_wb.class, class_kg.h);
   private static final Set bw = Sets.newHashSet((Object[])(new Item[]{Items.cb, Items.cc, Items.cV}));
   private boolean bx;
   private int bz;
   private int bA;

   public class_wb(World var1) {
      super(var1);
      this.a(0.9F, 0.9F);
   }

   protected void r() {
      this.bp.a(0, new class_tg(this));
      this.bp.a(1, new class_ub(this, 1.25D));
      this.bp.a(3, new class_tc(this, 1.0D));
      this.bp.a(4, new PathfinderGoalTempt(this, 1.2D, Items.ci, false));
      this.bp.a(4, new PathfinderGoalTempt(this, 1.2D, false, bw));
      this.bp.a(5, new class_ti(this, 1.1D));
      this.bp.a(6, new class_uf(this, 1.0D));
      this.bp.a(7, new class_to(this, EntityHuman.class, 6.0F));
      this.bp.a(8, new class_ue(this));
   }

   protected void bz() {
      super.bz();
      this.a((class_sk)class_ys.a).a(10.0D);
      this.a((class_sk)class_ys.d).a(0.25D);
   }

   public Entity bs() {
      return this.bt().isEmpty()?null:(Entity)this.bt().get(0);
   }

   public boolean cK() {
      Entity var1 = this.bs();
      if(!(var1 instanceof EntityHuman)) {
         return false;
      } else {
         EntityHuman var2 = (EntityHuman)var1;
         ItemStack var3 = var2.ca();
         if(var3 != null && var3.b() == Items.ci) {
            return true;
         } else {
            var3 = var2.cb();
            return var3 != null && var3.b() == Items.ci;
         }
      }
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)bv, (Object)Boolean.valueOf(false));
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("Saddle", this.cZ());
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.o(var1.p("Saddle"));
   }

   protected class_nf G() {
      return class_ng.dL;
   }

   protected class_nf bQ() {
      return class_ng.dN;
   }

   protected class_nf bR() {
      return class_ng.dM;
   }

   protected void a(BlockPosition var1, Block var2) {
      this.a(class_ng.dP, 0.15F, 1.0F);
   }

   public boolean a(EntityHuman var1, EnumHand var2, ItemStack var3) {
      if(!super.a(var1, var2, var3)) {
         if(this.cZ() && !this.world.E && !this.aI()) {
            var1.m(this);
            return true;
         } else {
            return false;
         }
      } else {
         return true;
      }
   }

   protected void a(boolean var1, int var2) {
      super.a(var1, var2);
      if(this.cZ()) {
         this.a(Items.aC, 1);
      }

   }

   protected class_kk J() {
      return class_azs.C;
   }

   public boolean cZ() {
      return ((Boolean)this.datawatcher.a(bv)).booleanValue();
   }

   public void o(boolean var1) {
      if(var1) {
         this.datawatcher.b(bv, Boolean.valueOf(true));
      } else {
         this.datawatcher.b(bv, Boolean.valueOf(false));
      }

   }

   public void a(class_xz var1) {
      if(!this.world.E && !this.dead) {
         class_yq var2 = new class_yq(this.world);
         var2.a((EnumInventorySlot)EnumInventorySlot.MAINHAND, (ItemStack)(new ItemStack(Items.D)));
         var2.b(this.locX, this.locY, this.locZ, this.yaw, this.pitch);
         var2.m(this.cR());
         if(this.o_()) {
            var2.c(this.be());
            var2.i(this.bf());
         }

         this.world.a((Entity)var2);
         this.S();
      }
   }

   public void e(float var1, float var2) {
      super.e(var1, var2);
      if(var1 > 5.0F) {
         Iterator var3 = this.b((Class)EntityHuman.class).iterator();

         while(var3.hasNext()) {
            EntityHuman var4 = (EntityHuman)var3.next();
            var4.b((Statistic)AchievementList.u);
         }
      }

   }

   public void g(float var1, float var2) {
      Entity var3 = this.bt().isEmpty()?null:(Entity)this.bt().get(0);
      if(this.aI() && this.cK()) {
         this.lastYaw = this.yaw = var3.yaw;
         this.pitch = var3.pitch * 0.5F;
         this.b(this.yaw, this.pitch);
         this.aO = this.aM = this.yaw;
         this.P = 1.0F;
         this.aQ = this.cj() * 0.1F;
         if(this.bw()) {
            float var4 = (float)this.a((class_sk)class_ys.d).e() * 0.225F;
            if(this.bx) {
               if(this.bz++ > this.bA) {
                  this.bx = false;
               }

               var4 += var4 * 1.15F * MathHelper.a((float)this.bz / (float)this.bA * 3.1415927F);
            }

            this.l(var4);
            super.g(0.0F, 1.0F);
         } else {
            this.motX = 0.0D;
            this.motY = 0.0D;
            this.motZ = 0.0D;
         }

         this.aE = this.aF;
         double var9 = this.locX - this.lastX;
         double var6 = this.locZ - this.lastZ;
         float var8 = MathHelper.a(var9 * var9 + var6 * var6) * 4.0F;
         if(var8 > 1.0F) {
            var8 = 1.0F;
         }

         this.aF += (var8 - this.aF) * 0.4F;
         this.aG += this.aF;
      } else {
         this.P = 0.5F;
         this.aQ = 0.02F;
         super.g(var1, var2);
      }
   }

   public boolean da() {
      if(this.bx) {
         return false;
      } else {
         this.bx = true;
         this.bz = 0;
         this.bA = this.bE().nextInt(841) + 140;
         return true;
      }
   }

   public class_wb b(class_rn var1) {
      return new class_wb(this.world);
   }

   public boolean e(ItemStack var1) {
      return var1 != null && bw.contains(var1.b());
   }

   // $FF: synthetic method
   public class_rn a(class_rn var1) {
      return this.b(var1);
   }
}
