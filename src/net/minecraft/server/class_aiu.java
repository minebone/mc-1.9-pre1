package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.class_aio;
import net.minecraft.server.class_ato;

public class class_aiu extends class_aio {
   public class_aiu(BiomeBase.class_a_in_class_aif var1) {
      super(class_aio.class_a_in_class_aio.BIRCH, var1);
   }

   public class_ato a(Random var1) {
      return var1.nextBoolean()?class_aio.y:class_aio.z;
   }
}
