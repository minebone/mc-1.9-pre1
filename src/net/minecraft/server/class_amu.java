package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockDirt;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Item;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_amu extends Block {
   public static final class_arm a = class_arm.a("snowy");

   protected class_amu() {
      super(Material.b, MaterialMapColor.z);
      this.w(this.A.b().set(a, Boolean.valueOf(false)));
      this.a(true);
      this.a(CreativeModeTab.b);
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      Block var4 = var2.getType(var3.a()).getBlock();
      return var1.set(a, Boolean.valueOf(var4 == Blocks.aJ || var4 == Blocks.aH));
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(!var1.E) {
         if(var1.k(var2.a()) < 4 && var1.getType(var2.a()).c() > 2) {
            var1.a(var2, Blocks.d.u().set(BlockDirt.a, BlockDirt.EnumDirtVariant.DIRT));
         } else {
            if(var1.k(var2.a()) >= 9) {
               for(int var5 = 0; var5 < 4; ++var5) {
                  BlockPosition var6 = var2.a(var4.nextInt(3) - 1, var4.nextInt(5) - 3, var4.nextInt(3) - 1);
                  IBlockData var7 = var1.getType(var6);
                  IBlockData var8 = var1.getType(var6.a());
                  if(var7.getBlock() == Blocks.d && var7.get(BlockDirt.a) == BlockDirt.EnumDirtVariant.DIRT && var1.k(var6.a()) >= 4 && var8.c() <= 2) {
                     var1.a(var6, this.u());
                  }
               }
            }

         }
      }
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Blocks.d.a(Blocks.d.u().set(BlockDirt.a, BlockDirt.EnumDirtVariant.DIRT), var2, var3);
   }

   public int e(IBlockData var1) {
      return 0;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }
}
