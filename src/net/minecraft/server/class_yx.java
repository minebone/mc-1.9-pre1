package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.EnumMonsterType;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.MobEffectType;
import net.minecraft.server.NavigationAbstract;
import net.minecraft.server.World;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.class_qk;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_tn;
import net.minecraft.server.class_to;
import net.minecraft.server.class_tr;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_uu;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_vg;
import net.minecraft.server.class_wg;
import net.minecraft.server.class_yp;
import net.minecraft.server.class_ys;
import net.minecraft.server.class_yv;

public class class_yx extends class_yp {
   private static final class_ke a = DataWatcher.a(class_yx.class, class_kg.a);

   public class_yx(World var1) {
      super(var1);
      this.a(1.4F, 0.9F);
   }

   protected void r() {
      this.bp.a(1, new class_tg(this));
      this.bp.a(3, new class_tn(this, 0.4F));
      this.bp.a(4, new class_yx.class_a_in_class_yx(this));
      this.bp.a(5, new class_uf(this, 0.8D));
      this.bp.a(6, new class_to(this, EntityHuman.class, 8.0F));
      this.bp.a(6, new class_ue(this));
      this.bq.a(1, new class_uu(this, false, new Class[0]));
      this.bq.a(2, new class_yx.class_c_in_class_yx(this, EntityHuman.class));
      this.bq.a(3, new class_yx.class_c_in_class_yx(this, class_wg.class));
   }

   public double ax() {
      return (double)(this.length * 0.5F);
   }

   protected NavigationAbstract b(World var1) {
      return new class_vg(this, var1);
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)a, (Object)Byte.valueOf((byte)0));
   }

   public void m() {
      super.m();
      if(!this.world.E) {
         this.a(this.positionChanged);
      }

   }

   protected void bz() {
      super.bz();
      this.a(class_ys.a).a(16.0D);
      this.a(class_ys.d).a(0.30000001192092896D);
   }

   protected class_nf G() {
      return class_ng.fK;
   }

   protected class_nf bQ() {
      return class_ng.fM;
   }

   protected class_nf bR() {
      return class_ng.fL;
   }

   protected void a(BlockPosition var1, Block var2) {
      this.a(class_ng.fN, 0.15F, 1.0F);
   }

   protected class_kk J() {
      return class_azs.q;
   }

   public boolean n_() {
      return this.o();
   }

   public void aP() {
   }

   public EnumMonsterType bZ() {
      return EnumMonsterType.ARTHROPOD;
   }

   public boolean d(MobEffect var1) {
      return var1.a() == MobEffectList.s?false:super.d(var1);
   }

   public boolean o() {
      return (((Byte)this.datawatcher.a(a)).byteValue() & 1) != 0;
   }

   public void a(boolean var1) {
      byte var2 = ((Byte)this.datawatcher.a(a)).byteValue();
      if(var1) {
         var2 = (byte)(var2 | 1);
      } else {
         var2 &= -2;
      }

      this.datawatcher.b(a, Byte.valueOf(var2));
   }

   public class_sc a(class_qk var1, class_sc var2) {
      Object var4 = super.a(var1, var2);
      if(this.world.r.nextInt(100) == 0) {
         class_yv var3 = new class_yv(this.world);
         var3.b(this.locX, this.locY, this.locZ, this.yaw, 0.0F);
         var3.a((class_qk)var1, (class_sc)null);
         this.world.a((Entity)var3);
         var3.m(this);
      }

      if(var4 == null) {
         var4 = new class_yx.class_b_in_class_yx();
         if(this.world.ae() == EnumDifficulty.HARD && this.world.r.nextFloat() < 0.1F * var1.c()) {
            ((class_yx.class_b_in_class_yx)var4).a(this.world.r);
         }
      }

      if(var4 instanceof class_yx.class_b_in_class_yx) {
         MobEffectType var5 = ((class_yx.class_b_in_class_yx)var4).a;
         if(var5 != null) {
            this.c(new MobEffect(var5, Integer.MAX_VALUE));
         }
      }

      return (class_sc)var4;
   }

   public float bm() {
      return 0.65F;
   }

   static class class_c_in_class_yx extends class_ux {
      public class_c_in_class_yx(class_yx var1, Class var2) {
         super(var1, var2, true);
      }

      public boolean a() {
         float var1 = this.e.e(1.0F);
         return var1 >= 0.5F?false:super.a();
      }
   }

   static class class_a_in_class_yx extends class_tr {
      public class_a_in_class_yx(class_yx var1) {
         super(var1, 1.0D, true);
      }

      public boolean b() {
         float var1 = this.b.e(1.0F);
         if(var1 >= 0.5F && this.b.bE().nextInt(100) == 0) {
            this.b.c((class_rz)null);
            return false;
         } else {
            return super.b();
         }
      }

      protected double a(class_rz var1) {
         return (double)(4.0F + var1.width);
      }
   }

   public static class class_b_in_class_yx implements class_sc {
      public MobEffectType a;

      public void a(Random var1) {
         int var2 = var1.nextInt(5);
         if(var2 <= 1) {
            this.a = MobEffectList.a;
         } else if(var2 <= 2) {
            this.a = MobEffectList.e;
         } else if(var2 <= 3) {
            this.a = MobEffectList.j;
         } else if(var2 <= 4) {
            this.a = MobEffectList.n;
         }

      }
   }
}
