package net.minecraft.server;

import com.google.common.base.Optional;
import java.util.Iterator;
import net.minecraft.server.AchievementList;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.Statistic;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_yc extends Entity {
   private static final Logger b = LogManager.getLogger();
   private static final class_ke c = DataWatcher.a(class_yc.class, class_kg.f);
   private int d;
   private int e;
   private int f;
   private String g;
   private String h;
   public float a;

   public class_yc(World var1, double var2, double var4, double var6) {
      super(var1);
      this.f = 5;
      this.a = (float)(Math.random() * 3.141592653589793D * 2.0D);
      this.a(0.25F, 0.25F);
      this.b(var2, var4, var6);
      this.yaw = (float)(Math.random() * 360.0D);
      this.motX = (double)((float)(Math.random() * 0.20000000298023224D - 0.10000000149011612D));
      this.motY = 0.20000000298023224D;
      this.motZ = (double)((float)(Math.random() * 0.20000000298023224D - 0.10000000149011612D));
   }

   public class_yc(World var1, double var2, double var4, double var6, ItemStack var8) {
      this(var1, var2, var4, var6);
      this.a(var8);
   }

   protected boolean ad() {
      return false;
   }

   public class_yc(World var1) {
      super(var1);
      this.f = 5;
      this.a = (float)(Math.random() * 3.141592653589793D * 2.0D);
      this.a(0.25F, 0.25F);
      this.a(new ItemStack(Blocks.AIR, 0));
   }

   protected void i() {
      this.Q().a((class_ke)c, (Object)Optional.absent());
   }

   public void m() {
      if(this.k() == null) {
         this.S();
      } else {
         super.m();
         if(this.e > 0 && this.e != 32767) {
            --this.e;
         }

         this.lastX = this.locX;
         this.lastY = this.locY;
         this.lastZ = this.locZ;
         this.motY -= 0.03999999910593033D;
         this.noClip = this.j(this.locX, (this.bk().b + this.bk().e) / 2.0D, this.locZ);
         this.d(this.motX, this.motY, this.motZ);
         boolean var1 = (int)this.lastX != (int)this.locX || (int)this.lastY != (int)this.locY || (int)this.lastZ != (int)this.locZ;
         if(var1 || this.ticksLived % 25 == 0) {
            if(this.world.getType(new BlockPosition(this)).getMaterial() == Material.i) {
               this.motY = 0.20000000298023224D;
               this.motX = (double)((this.random.nextFloat() - this.random.nextFloat()) * 0.2F);
               this.motZ = (double)((this.random.nextFloat() - this.random.nextFloat()) * 0.2F);
               this.a(class_ng.bx, 0.4F, 2.0F + this.random.nextFloat() * 0.4F);
            }

            if(!this.world.E) {
               this.x();
            }
         }

         float var2 = 0.98F;
         if(this.onGround) {
            var2 = this.world.getType(new BlockPosition(MathHelper.c(this.locX), MathHelper.c(this.bk().b) - 1, MathHelper.c(this.locZ))).getBlock().z * 0.98F;
         }

         this.motX *= (double)var2;
         this.motY *= 0.9800000190734863D;
         this.motZ *= (double)var2;
         if(this.onGround) {
            this.motY *= -0.5D;
         }

         if(this.d != -32768) {
            ++this.d;
         }

         this.ai();
         if(!this.world.E && this.d >= 6000) {
            this.S();
         }

      }
   }

   private void x() {
      Iterator var1 = this.world.a(class_yc.class, this.bk().b(0.5D, 0.0D, 0.5D)).iterator();

      while(var1.hasNext()) {
         class_yc var2 = (class_yc)var1.next();
         this.a(var2);
      }

   }

   private boolean a(class_yc var1) {
      if(var1 == this) {
         return false;
      } else if(var1.at() && this.at()) {
         ItemStack var2 = this.k();
         ItemStack var3 = var1.k();
         if(this.e != 32767 && var1.e != 32767) {
            if(this.d != -32768 && var1.d != -32768) {
               if(var3.b() != var2.b()) {
                  return false;
               } else if(var3.n() ^ var2.n()) {
                  return false;
               } else if(var3.n() && !var3.o().equals(var2.o())) {
                  return false;
               } else if(var3.b() == null) {
                  return false;
               } else if(var3.b().k() && var3.i() != var2.i()) {
                  return false;
               } else if(var3.b < var2.b) {
                  return var1.a(this);
               } else if(var3.b + var2.b > var3.c()) {
                  return false;
               } else {
                  var3.b += var2.b;
                  var1.e = Math.max(var1.e, this.e);
                  var1.d = Math.min(var1.d, this.d);
                  var1.a(var3);
                  this.S();
                  return true;
               }
            } else {
               return false;
            }
         } else {
            return false;
         }
      } else {
         return false;
      }
   }

   public void j() {
      this.d = 4800;
   }

   public boolean ai() {
      if(this.world.a((AxisAlignedBB)this.bk(), (Material)Material.h, (Entity)this)) {
         if(!this.inWater && !this.justCreated) {
            this.aj();
         }

         this.inWater = true;
      } else {
         this.inWater = false;
      }

      return this.inWater;
   }

   protected void h(int var1) {
      this.a(DamageSource.a, (float)var1);
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b(var1)) {
         return false;
      } else if(this.k() != null && this.k().b() == Items.cj && var1.c()) {
         return false;
      } else {
         this.an();
         this.f = (int)((float)this.f - var2);
         if(this.f <= 0) {
            this.S();
         }

         return false;
      }
   }

   public void b(NBTTagCompound var1) {
      var1.a("Health", (short)this.f);
      var1.a("Age", (short)this.d);
      var1.a("PickupDelay", (short)this.e);
      if(this.n() != null) {
         var1.a("Thrower", this.g);
      }

      if(this.l() != null) {
         var1.a("Owner", this.h);
      }

      if(this.k() != null) {
         var1.a((String)"Item", (NBTTag)this.k().b(new NBTTagCompound()));
      }

   }

   public void a(NBTTagCompound var1) {
      this.f = var1.g("Health");
      this.d = var1.g("Age");
      if(var1.e("PickupDelay")) {
         this.e = var1.g("PickupDelay");
      }

      if(var1.e("Owner")) {
         this.h = var1.l("Owner");
      }

      if(var1.e("Thrower")) {
         this.g = var1.l("Thrower");
      }

      NBTTagCompound var2 = var1.o("Item");
      this.a(ItemStack.a(var2));
      if(this.k() == null) {
         this.S();
      }

   }

   public void d(EntityHuman var1) {
      if(!this.world.E) {
         ItemStack var2 = this.k();
         int var3 = var2.b;
         if(this.e == 0 && (this.h == null || 6000 - this.d <= 200 || this.h.equals(var1.h_())) && var1.br.c(var2)) {
            if(var2.b() == Item.a(Blocks.r)) {
               var1.b((Statistic)AchievementList.g);
            }

            if(var2.b() == Item.a(Blocks.s)) {
               var1.b((Statistic)AchievementList.g);
            }

            if(var2.b() == Items.aM) {
               var1.b((Statistic)AchievementList.t);
            }

            if(var2.b() == Items.k) {
               var1.b((Statistic)AchievementList.w);
            }

            if(var2.b() == Items.bC) {
               var1.b((Statistic)AchievementList.A);
            }

            if(var2.b() == Items.k && this.n() != null) {
               EntityHuman var4 = this.world.a(this.n());
               if(var4 != null && var4 != var1) {
                  var4.b((Statistic)AchievementList.x);
               }
            }

            if(!this.ac()) {
               this.world.a((EntityHuman)null, var1.locX, var1.locY, var1.locZ, class_ng.cR, EnumSoundCategory.PLAYERS, 0.2F, ((this.random.nextFloat() - this.random.nextFloat()) * 0.7F + 1.0F) * 2.0F);
            }

            var1.a(this, var3);
            if(var2.b <= 0) {
               this.S();
            }

            var1.a(StatisticList.d(var2.b()), var3);
         }

      }
   }

   public String h_() {
      return this.o_()?this.be():class_di.a("item." + this.k().a());
   }

   public boolean aS() {
      return false;
   }

   public Entity c(int var1) {
      Entity var2 = super.c(var1);
      if(!this.world.E && var2 instanceof class_yc) {
         ((class_yc)var2).x();
      }

      return var2;
   }

   public ItemStack k() {
      ItemStack var1 = (ItemStack)((Optional)this.Q().a(c)).orNull();
      if(var1 == null) {
         if(this.world != null) {
            b.error("Item entity " + this.getId() + " has no item?!");
         }

         return new ItemStack(Blocks.b);
      } else {
         return var1;
      }
   }

   public void a(ItemStack var1) {
      this.Q().b(c, Optional.fromNullable(var1));
      this.Q().b(c);
   }

   public String l() {
      return this.h;
   }

   public void d(String var1) {
      this.h = var1;
   }

   public String n() {
      return this.g;
   }

   public void e(String var1) {
      this.g = var1;
   }

   public void q() {
      this.e = 10;
   }

   public void r() {
      this.e = 0;
   }

   public void s() {
      this.e = 32767;
   }

   public void a(int var1) {
      this.e = var1;
   }

   public boolean t() {
      return this.e > 0;
   }

   public void v() {
      this.d = -6000;
   }

   public void w() {
      this.s();
      this.d = 5999;
   }
}
