package net.minecraft.server;

import com.google.common.base.Predicate;
import java.util.List;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.TileEntity;
import net.minecraft.server.TileEntityComparator;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_akq;
import net.minecraft.server.class_alf;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.class_or;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_xr;

public class BlockRedstoneComparator extends class_akq implements class_alf {
   public static final class_arm a = class_arm.a("powered");
   public static final BlockStateEnum b = BlockStateEnum.a("mode", BlockRedstoneComparator.EnumComparatorMode.class);

   public BlockRedstoneComparator(boolean var1) {
      super(var1);
      this.w(this.A.b().set(D, EnumDirection.NORTH).set(a, Boolean.valueOf(false)).set(b, BlockRedstoneComparator.EnumComparatorMode.COMPARE));
      this.u = true;
   }

   public String c() {
      return class_di.a("item.comparator.name");
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Items.co;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Items.co);
   }

   protected int i(IBlockData var1) {
      return 2;
   }

   protected IBlockData x(IBlockData var1) {
      Boolean var2 = (Boolean)var1.get(a);
      BlockRedstoneComparator.EnumComparatorMode var3 = (BlockRedstoneComparator.EnumComparatorMode)var1.get(b);
      EnumDirection var4 = (EnumDirection)var1.get(D);
      return Blocks.ck.u().set(D, var4).set(a, var2).set(b, var3);
   }

   protected IBlockData y(IBlockData var1) {
      Boolean var2 = (Boolean)var1.get(a);
      BlockRedstoneComparator.EnumComparatorMode var3 = (BlockRedstoneComparator.EnumComparatorMode)var1.get(b);
      EnumDirection var4 = (EnumDirection)var1.get(D);
      return Blocks.cj.u().set(D, var4).set(a, var2).set(b, var3);
   }

   protected boolean z(IBlockData var1) {
      return this.d || ((Boolean)var1.get(a)).booleanValue();
   }

   protected int a(class_ahw var1, BlockPosition var2, IBlockData var3) {
      TileEntity var4 = var1.r(var2);
      return var4 instanceof TileEntityComparator?((TileEntityComparator)var4).b():0;
   }

   private int j(World var1, BlockPosition var2, IBlockData var3) {
      return var3.get(b) == BlockRedstoneComparator.EnumComparatorMode.SUBTRACT?Math.max(this.f(var1, var2, var3) - this.c(var1, var2, var3), 0):this.f(var1, var2, var3);
   }

   protected boolean e(World var1, BlockPosition var2, IBlockData var3) {
      int var4 = this.f(var1, var2, var3);
      if(var4 >= 15) {
         return true;
      } else if(var4 == 0) {
         return false;
      } else {
         int var5 = this.c(var1, var2, var3);
         return var5 == 0?true:var4 >= var5;
      }
   }

   protected int f(World var1, BlockPosition var2, IBlockData var3) {
      int var4 = super.f(var1, var2, var3);
      EnumDirection var5 = (EnumDirection)var3.get(D);
      BlockPosition var6 = var2.a(var5);
      IBlockData var7 = var1.getType(var6);
      Block var8 = var7.getBlock();
      if(var7.n()) {
         var4 = var7.a(var1, var6);
      } else if(var4 < 15 && var7.l()) {
         var6 = var6.a(var5);
         var7 = var1.getType(var6);
         if(var7.n()) {
            var4 = var7.a(var1, var6);
         } else if(var7.getMaterial() == Material.a) {
            class_xr var9 = this.a(var1, var5, var6);
            if(var9 != null) {
               var4 = var9.t();
            }
         }
      }

      return var4;
   }

   private class_xr a(World var1, final EnumDirection var2, BlockPosition var3) {
      List var4 = var1.a(class_xr.class, new AxisAlignedBB((double)var3.p(), (double)var3.q(), (double)var3.r(), (double)(var3.p() + 1), (double)(var3.q() + 1), (double)(var3.r() + 1)), new Predicate() {
         public boolean a(Entity var1) {
            return var1 != null && var1.bh() == var2;
         }

         // $FF: synthetic method
         public boolean apply(Object var1) {
            return this.a((Entity)var1);
         }
      });
      return var4.size() == 1?(class_xr)var4.get(0):null;
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(!var4.abilities.e) {
         return false;
      } else {
         var3 = var3.a(b);
         float var11 = var3.get(b) == BlockRedstoneComparator.EnumComparatorMode.SUBTRACT?0.55F:0.5F;
         var1.a(var4, var2, class_ng.aj, EnumSoundCategory.BLOCKS, 0.3F, var11);
         var1.a((BlockPosition)var2, (IBlockData)var3, 2);
         this.k(var1, var2, var3);
         return true;
      }
   }

   protected void g(World var1, BlockPosition var2, IBlockData var3) {
      if(!var1.a((BlockPosition)var2, (Block)this)) {
         int var4 = this.j(var1, var2, var3);
         TileEntity var5 = var1.r(var2);
         int var6 = var5 instanceof TileEntityComparator?((TileEntityComparator)var5).b():0;
         if(var4 != var6 || this.z(var3) != this.e(var1, var2, var3)) {
            if(this.i(var1, var2, var3)) {
               var1.a(var2, this, 2, -1);
            } else {
               var1.a(var2, this, 2, 0);
            }
         }

      }
   }

   private void k(World var1, BlockPosition var2, IBlockData var3) {
      int var4 = this.j(var1, var2, var3);
      TileEntity var5 = var1.r(var2);
      int var6 = 0;
      if(var5 instanceof TileEntityComparator) {
         TileEntityComparator var7 = (TileEntityComparator)var5;
         var6 = var7.b();
         var7.a(var4);
      }

      if(var6 != var4 || var3.get(b) == BlockRedstoneComparator.EnumComparatorMode.COMPARE) {
         boolean var9 = this.e(var1, var2, var3);
         boolean var8 = this.z(var3);
         if(var8 && !var9) {
            var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Boolean.valueOf(false)), 2);
         } else if(!var8 && var9) {
            var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Boolean.valueOf(true)), 2);
         }

         this.h(var1, var2, var3);
      }

   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(this.d) {
         var1.a((BlockPosition)var2, (IBlockData)this.y(var3).set(a, Boolean.valueOf(true)), 4);
      }

      this.k(var1, var2, var3);
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      super.c(var1, var2, var3);
      var1.a(var2, this.a(var1, 0));
   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      super.b((World)var1, var2, (IBlockData)var3);
      var1.s(var2);
      this.h(var1, var2, var3);
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, int var4, int var5) {
      super.a(var1, var2, var3, var4, var5);
      TileEntity var6 = var1.r(var2);
      return var6 == null?false:var6.c(var4, var5);
   }

   public TileEntity a(World var1, int var2) {
      return new TileEntityComparator();
   }

   public IBlockData a(int var1) {
      return this.u().set(D, EnumDirection.b(var1)).set(a, Boolean.valueOf((var1 & 8) > 0)).set(b, (var1 & 4) > 0?BlockRedstoneComparator.EnumComparatorMode.SUBTRACT:BlockRedstoneComparator.EnumComparatorMode.COMPARE);
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((EnumDirection)var1.get(D)).b();
      if(((Boolean)var1.get(a)).booleanValue()) {
         var3 |= 8;
      }

      if(var1.get(b) == BlockRedstoneComparator.EnumComparatorMode.SUBTRACT) {
         var3 |= 4;
      }

      return var3;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(D, var2.a((EnumDirection)var1.get(D)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(D)));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{D, b, a});
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return this.u().set(D, var8.bh().d()).set(a, Boolean.valueOf(false)).set(b, BlockRedstoneComparator.EnumComparatorMode.COMPARE);
   }

   public static enum EnumComparatorMode implements class_or {
      COMPARE("compare"),
      SUBTRACT("subtract");

      private final String c;

      private EnumComparatorMode(String var3) {
         this.c = var3;
      }

      public String toString() {
         return this.c;
      }

      public String m() {
         return this.c;
      }
   }
}
