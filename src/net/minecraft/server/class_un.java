package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import net.minecraft.server.Entity;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_wg;
import net.minecraft.server.class_zd;

public class class_un extends class_tj {
   private class_zd a;
   private class_wg b;
   private int c;
   private boolean d;

   public class_un(class_zd var1) {
      this.a = var1;
      this.a(3);
   }

   public boolean a() {
      if(this.a.l() >= 0) {
         return false;
      } else if(!this.a.world.B()) {
         return false;
      } else {
         List var1 = this.a.world.a(class_wg.class, this.a.bk().b(6.0D, 2.0D, 6.0D));
         if(var1.isEmpty()) {
            return false;
         } else {
            Iterator var2 = var1.iterator();

            while(var2.hasNext()) {
               class_wg var3 = (class_wg)var2.next();
               if(var3.da() > 0) {
                  this.b = var3;
                  break;
               }
            }

            return this.b != null;
         }
      }
   }

   public boolean b() {
      return this.b.da() > 0;
   }

   public void c() {
      this.c = this.a.bE().nextInt(320);
      this.d = false;
      this.b.x().o();
   }

   public void d() {
      this.b = null;
      this.a.x().o();
   }

   public void e() {
      this.a.t().a(this.b, 30.0F, 30.0F);
      if(this.b.da() == this.c) {
         this.a.x().a((Entity)this.b, 0.5D);
         this.d = true;
      }

      if(this.d && this.a.h(this.b) < 4.0D) {
         this.b.a(false);
         this.a.x().o();
      }

   }
}
