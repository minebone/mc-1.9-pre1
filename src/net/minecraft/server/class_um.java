package net.minecraft.server;

import net.minecraft.server.class_rz;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_yh;

public class class_um extends class_tj {
   class_yh a;
   class_rz b;

   public class_um(class_yh var1) {
      this.a = var1;
      this.a(1);
   }

   public boolean a() {
      class_rz var1 = this.a.A();
      return this.a.da() > 0 || var1 != null && this.a.h(var1) < 9.0D;
   }

   public void c() {
      this.a.x().o();
      this.b = this.a.A();
   }

   public void d() {
      this.b = null;
   }

   public void e() {
      if(this.b == null) {
         this.a.a(-1);
      } else if(this.a.h(this.b) > 49.0D) {
         this.a.a(-1);
      } else if(!this.a.y().a(this.b)) {
         this.a.a(-1);
      } else {
         this.a.a(1);
      }
   }
}
