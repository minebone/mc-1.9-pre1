package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import net.minecraft.server.CrashReportSystemDetails;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.GameRules;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.WorldProviderNormal;
import net.minecraft.server.WorldSettings;
import net.minecraft.server.WorldType;
import net.minecraft.server.BlockPosition;

public class WorldData {
   private String b;
   private int c;
   private boolean d;
   public static final EnumDifficulty a = EnumDifficulty.NORMAL;
   private long e;
   private WorldType f = WorldType.b;
   private String g = "";
   private int h;
   private int i;
   private int j;
   private long k;
   private long l;
   private long m;
   private long n;
   private NBTTagCompound o;
   private int p;
   private String q;
   private int r;
   private int s;
   private boolean t;
   private int u;
   private boolean v;
   private int w;
   private WorldSettings.EnumGamemode x;
   private boolean y;
   private boolean z;
   private boolean A;
   private boolean B;
   private EnumDifficulty C;
   private boolean D;
   private double E = 0.0D;
   private double F = 0.0D;
   private double G = 6.0E7D;
   private long H = 0L;
   private double I = 0.0D;
   private double J = 5.0D;
   private double K = 0.2D;
   private int L = 5;
   private int M = 15;
   private final Map N = Maps.newEnumMap(WorldProviderNormal.class);
   private GameRules O = new GameRules();

   protected WorldData() {
   }

   public WorldData(NBTTagCompound var1) {
      NBTTagCompound var2;
      if(var1.b("Version", 10)) {
         var2 = var1.o("Version");
         this.b = var2.l("Name");
         this.c = var2.h("Id");
         this.d = var2.p("Snapshot");
      }

      this.e = var1.i("RandomSeed");
      if(var1.b("generatorName", 8)) {
         String var5 = var1.l("generatorName");
         this.f = WorldType.a(var5);
         if(this.f == null) {
            this.f = WorldType.b;
         } else if(this.f.f()) {
            int var3 = 0;
            if(var1.b("generatorVersion", 99)) {
               var3 = var1.h("generatorVersion");
            }

            this.f = this.f.a(var3);
         }

         if(var1.b("generatorOptions", 8)) {
            this.g = var1.l("generatorOptions");
         }
      }

      this.x = WorldSettings.EnumGamemode.a(var1.h("GameType"));
      if(var1.b("MapFeatures", 99)) {
         this.y = var1.p("MapFeatures");
      } else {
         this.y = true;
      }

      this.h = var1.h("SpawnX");
      this.i = var1.h("SpawnY");
      this.j = var1.h("SpawnZ");
      this.k = var1.i("Time");
      if(var1.b("DayTime", 99)) {
         this.l = var1.i("DayTime");
      } else {
         this.l = this.k;
      }

      this.m = var1.i("LastPlayed");
      this.n = var1.i("SizeOnDisk");
      this.q = var1.l("LevelName");
      this.r = var1.h("version");
      this.s = var1.h("clearWeatherTime");
      this.u = var1.h("rainTime");
      this.t = var1.p("raining");
      this.w = var1.h("thunderTime");
      this.v = var1.p("thundering");
      this.z = var1.p("hardcore");
      if(var1.b("initialized", 99)) {
         this.B = var1.p("initialized");
      } else {
         this.B = true;
      }

      if(var1.b("allowCommands", 99)) {
         this.A = var1.p("allowCommands");
      } else {
         this.A = this.x == WorldSettings.EnumGamemode.CREATIVE;
      }

      if(var1.b("Player", 10)) {
         this.o = var1.o("Player");
         this.p = this.o.h("Dimension");
      }

      if(var1.b("GameRules", 10)) {
         this.O.a(var1.o("GameRules"));
      }

      if(var1.b("Difficulty", 99)) {
         this.C = EnumDifficulty.a(var1.f("Difficulty"));
      }

      if(var1.b("DifficultyLocked", 1)) {
         this.D = var1.p("DifficultyLocked");
      }

      if(var1.b("BorderCenterX", 99)) {
         this.E = var1.k("BorderCenterX");
      }

      if(var1.b("BorderCenterZ", 99)) {
         this.F = var1.k("BorderCenterZ");
      }

      if(var1.b("BorderSize", 99)) {
         this.G = var1.k("BorderSize");
      }

      if(var1.b("BorderSizeLerpTime", 99)) {
         this.H = var1.i("BorderSizeLerpTime");
      }

      if(var1.b("BorderSizeLerpTarget", 99)) {
         this.I = var1.k("BorderSizeLerpTarget");
      }

      if(var1.b("BorderSafeZone", 99)) {
         this.J = var1.k("BorderSafeZone");
      }

      if(var1.b("BorderDamagePerBlock", 99)) {
         this.K = var1.k("BorderDamagePerBlock");
      }

      if(var1.b("BorderWarningBlocks", 99)) {
         this.L = var1.h("BorderWarningBlocks");
      }

      if(var1.b("BorderWarningTime", 99)) {
         this.M = var1.h("BorderWarningTime");
      }

      if(var1.b("DimensionData", 10)) {
         var2 = var1.o("DimensionData");
         Iterator var6 = var2.c().iterator();

         while(var6.hasNext()) {
            String var4 = (String)var6.next();
            this.N.put(WorldProviderNormal.a(Integer.parseInt(var4)), var2.o(var4));
         }
      }

   }

   public WorldData(WorldSettings var1, String var2) {
      this.a(var1);
      this.q = var2;
      this.C = a;
      this.B = false;
   }

   public void a(WorldSettings var1) {
      this.e = var1.d();
      this.x = var1.e();
      this.y = var1.g();
      this.z = var1.f();
      this.f = var1.h();
      this.g = var1.j();
      this.A = var1.i();
   }

   public WorldData(WorldData var1) {
      this.e = var1.e;
      this.f = var1.f;
      this.g = var1.g;
      this.x = var1.x;
      this.y = var1.y;
      this.h = var1.h;
      this.i = var1.i;
      this.j = var1.j;
      this.k = var1.k;
      this.l = var1.l;
      this.m = var1.m;
      this.n = var1.n;
      this.o = var1.o;
      this.p = var1.p;
      this.q = var1.q;
      this.r = var1.r;
      this.u = var1.u;
      this.t = var1.t;
      this.w = var1.w;
      this.v = var1.v;
      this.z = var1.z;
      this.A = var1.A;
      this.B = var1.B;
      this.O = var1.O;
      this.C = var1.C;
      this.D = var1.D;
      this.E = var1.E;
      this.F = var1.F;
      this.G = var1.G;
      this.H = var1.H;
      this.I = var1.I;
      this.J = var1.J;
      this.K = var1.K;
      this.M = var1.M;
      this.L = var1.L;
   }

   public NBTTagCompound a(NBTTagCompound var1) {
      if(var1 == null) {
         var1 = this.o;
      }

      NBTTagCompound var2 = new NBTTagCompound();
      this.a(var2, var1);
      return var2;
   }

   private void a(NBTTagCompound var1, NBTTagCompound var2) {
      NBTTagCompound var3 = new NBTTagCompound();
      var3.a("Name", "1.9-pre2");
      var3.a("Id", (int)165);
      var3.a("Snapshot", true);
      var1.a((String)"Version", (NBTTag)var3);
      var1.a("DataVersion", (int)165);
      var1.a("RandomSeed", this.e);
      var1.a("generatorName", this.f.a());
      var1.a("generatorVersion", this.f.d());
      var1.a("generatorOptions", this.g);
      var1.a("GameType", this.x.a());
      var1.a("MapFeatures", this.y);
      var1.a("SpawnX", this.h);
      var1.a("SpawnY", this.i);
      var1.a("SpawnZ", this.j);
      var1.a("Time", this.k);
      var1.a("DayTime", this.l);
      var1.a("SizeOnDisk", this.n);
      var1.a("LastPlayed", MinecraftServer.av());
      var1.a("LevelName", this.q);
      var1.a("version", this.r);
      var1.a("clearWeatherTime", this.s);
      var1.a("rainTime", this.u);
      var1.a("raining", this.t);
      var1.a("thunderTime", this.w);
      var1.a("thundering", this.v);
      var1.a("hardcore", this.z);
      var1.a("allowCommands", this.A);
      var1.a("initialized", this.B);
      var1.a("BorderCenterX", this.E);
      var1.a("BorderCenterZ", this.F);
      var1.a("BorderSize", this.G);
      var1.a("BorderSizeLerpTime", this.H);
      var1.a("BorderSafeZone", this.J);
      var1.a("BorderDamagePerBlock", this.K);
      var1.a("BorderSizeLerpTarget", this.I);
      var1.a("BorderWarningBlocks", (double)this.L);
      var1.a("BorderWarningTime", (double)this.M);
      if(this.C != null) {
         var1.a("Difficulty", (byte)this.C.a());
      }

      var1.a("DifficultyLocked", this.D);
      var1.a((String)"GameRules", (NBTTag)this.O.a());
      NBTTagCompound var4 = new NBTTagCompound();
      Iterator var5 = this.N.entrySet().iterator();

      while(var5.hasNext()) {
         Entry var6 = (Entry)var5.next();
         var4.a(String.valueOf(((WorldProviderNormal)var6.getKey()).a()), (NBTTag)var6.getValue());
      }

      var1.a((String)"DimensionData", (NBTTag)var4);
      if(var2 != null) {
         var1.a((String)"Player", (NBTTag)var2);
      }

   }

   public long a() {
      return this.e;
   }

   public int b() {
      return this.h;
   }

   public int c() {
      return this.i;
   }

   public int d() {
      return this.j;
   }

   public long e() {
      return this.k;
   }

   public long f() {
      return this.l;
   }

   public NBTTagCompound h() {
      return this.o;
   }

   public void b(long var1) {
      this.k = var1;
   }

   public void c(long var1) {
      this.l = var1;
   }

   public void a(BlockPosition var1) {
      this.h = var1.p();
      this.i = var1.q();
      this.j = var1.r();
   }

   public String j() {
      return this.q;
   }

   public void a(String var1) {
      this.q = var1;
   }

   public int k() {
      return this.r;
   }

   public void e(int var1) {
      this.r = var1;
   }

   public int z() {
      return this.s;
   }

   public void i(int var1) {
      this.s = var1;
   }

   public boolean m() {
      return this.v;
   }

   public void a(boolean var1) {
      this.v = var1;
   }

   public int n() {
      return this.w;
   }

   public void f(int var1) {
      this.w = var1;
   }

   public boolean o() {
      return this.t;
   }

   public void b(boolean var1) {
      this.t = var1;
   }

   public int p() {
      return this.u;
   }

   public void g(int var1) {
      this.u = var1;
   }

   public WorldSettings.EnumGamemode q() {
      return this.x;
   }

   public boolean r() {
      return this.y;
   }

   public void f(boolean var1) {
      this.y = var1;
   }

   public void a(WorldSettings.EnumGamemode var1) {
      this.x = var1;
   }

   public boolean s() {
      return this.z;
   }

   public void g(boolean var1) {
      this.z = var1;
   }

   public WorldType t() {
      return this.f;
   }

   public void a(WorldType var1) {
      this.f = var1;
   }

   public String A() {
      return this.g == null?"":this.g;
   }

   public boolean u() {
      return this.A;
   }

   public void c(boolean var1) {
      this.A = var1;
   }

   public boolean v() {
      return this.B;
   }

   public void d(boolean var1) {
      this.B = var1;
   }

   public GameRules w() {
      return this.O;
   }

   public double B() {
      return this.E;
   }

   public double C() {
      return this.F;
   }

   public double D() {
      return this.G;
   }

   public void a(double var1) {
      this.G = var1;
   }

   public long E() {
      return this.H;
   }

   public void e(long var1) {
      this.H = var1;
   }

   public double F() {
      return this.I;
   }

   public void b(double var1) {
      this.I = var1;
   }

   public void c(double var1) {
      this.F = var1;
   }

   public void d(double var1) {
      this.E = var1;
   }

   public double G() {
      return this.J;
   }

   public void e(double var1) {
      this.J = var1;
   }

   public double H() {
      return this.K;
   }

   public void f(double var1) {
      this.K = var1;
   }

   public int I() {
      return this.L;
   }

   public int J() {
      return this.M;
   }

   public void j(int var1) {
      this.L = var1;
   }

   public void k(int var1) {
      this.M = var1;
   }

   public EnumDifficulty x() {
      return this.C;
   }

   public void a(EnumDifficulty var1) {
      this.C = var1;
   }

   public boolean y() {
      return this.D;
   }

   public void e(boolean var1) {
      this.D = var1;
   }

   public void a(CrashReportSystemDetails var1) {
      var1.a("Level seed", new Callable() {
         public String a() throws Exception {
            return String.valueOf(WorldData.this.a());
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
      var1.a("Level generator", new Callable() {
         public String a() throws Exception {
            return String.format("ID %02d - %s, ver %d. Features enabled: %b", new Object[]{Integer.valueOf(WorldData.this.f.g()), WorldData.this.f.a(), Integer.valueOf(WorldData.this.f.d()), Boolean.valueOf(WorldData.this.y)});
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
      var1.a("Level generator options", new Callable() {
         public String a() throws Exception {
            return WorldData.this.g;
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
      var1.a("Level spawn location", new Callable() {
         public String a() throws Exception {
            return CrashReportSystemDetails.a(WorldData.this.h, WorldData.this.i, WorldData.this.j);
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
      var1.a("Level time", new Callable() {
         public String a() throws Exception {
            return String.format("%d game time, %d day time", new Object[]{Long.valueOf(WorldData.this.k), Long.valueOf(WorldData.this.l)});
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
      var1.a("Level dimension", new Callable() {
         public String a() throws Exception {
            return String.valueOf(WorldData.this.p);
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
      var1.a("Level storage version", new Callable() {
         public String a() throws Exception {
            String var1 = "Unknown?";

            try {
               switch(WorldData.this.r) {
               case 19132:
                  var1 = "McRegion";
                  break;
               case 19133:
                  var1 = "Anvil";
               }
            } catch (Throwable var3) {
               ;
            }

            return String.format("0x%05X - %s", new Object[]{Integer.valueOf(WorldData.this.r), var1});
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
      var1.a("Level weather", new Callable() {
         public String a() throws Exception {
            return String.format("Rain time: %d (now: %b), thunder time: %d (now: %b)", new Object[]{Integer.valueOf(WorldData.this.u), Boolean.valueOf(WorldData.this.t), Integer.valueOf(WorldData.this.w), Boolean.valueOf(WorldData.this.v)});
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
      var1.a("Level game mode", new Callable() {
         public String a() throws Exception {
            return String.format("Game mode: %s (ID %d). Hardcore: %b. Cheats: %b", new Object[]{WorldData.this.x.b(), Integer.valueOf(WorldData.this.x.a()), Boolean.valueOf(WorldData.this.z), Boolean.valueOf(WorldData.this.A)});
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
   }

   public NBTTagCompound a(WorldProviderNormal var1) {
      NBTTagCompound var2 = (NBTTagCompound)this.N.get(var1);
      return var2 == null?new NBTTagCompound():var2;
   }

   public void a(WorldProviderNormal var1, NBTTagCompound var2) {
      this.N.put(var1, var2);
   }
}
