package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSyntaxException;
import java.util.Random;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.MojangsonParser;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_bae;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_ec;
import net.minecraft.server.class_kk;

public class class_bal extends class_bae {
   private final NBTTagCompound a;

   public class_bal(class_baq[] var1, NBTTagCompound var2) {
      super(var1);
      this.a = var2;
   }

   public ItemStack a(ItemStack var1, Random var2, class_azy var3) {
      NBTTagCompound var4 = var1.o();
      if(var4 == null) {
         var4 = (NBTTagCompound)this.a.b();
      } else {
         var4.a(this.a);
      }

      var1.d(var4);
      return var1;
   }

   public static class class_a_in_class_bal extends class_bae.class_a_in_class_bae {
      public class_a_in_class_bal() {
         super(new class_kk("set_nbt"), class_bal.class);
      }

      public void a(JsonObject var1, class_bal var2, JsonSerializationContext var3) {
         var1.addProperty("tag", var2.a.toString());
      }

      public class_bal a(JsonObject var1, JsonDeserializationContext var2, class_baq[] var3) {
         try {
            NBTTagCompound var4 = MojangsonParser.a(ChatDeserializer.h(var1, "tag"));
            return new class_bal(var3, var4);
         } catch (class_ec var5) {
            throw new JsonSyntaxException(var5);
         }
      }

      // $FF: synthetic method
      public class_bae b(JsonObject var1, JsonDeserializationContext var2, class_baq[] var3) {
         return this.a(var1, var2, var3);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(JsonObject var1, class_bae var2, JsonSerializationContext var3) {
         this.a(var1, (class_bal)var2, var3);
      }
   }
}
