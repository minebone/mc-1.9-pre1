package net.minecraft.server;

import com.google.common.collect.Sets;
import java.util.Set;
import net.minecraft.server.Block;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityAnimal;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.PathfinderGoalTempt;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ayl;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rn;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_tc;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_ti;
import net.minecraft.server.class_to;
import net.minecraft.server.class_ub;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_ys;

public class class_vw extends EntityAnimal {
   private static final Set bD = Sets.newHashSet((Object[])(new Item[]{Items.P, Items.bo, Items.bn, Items.cU}));
   public float bv;
   public float bw;
   public float bx;
   public float bz;
   public float bA = 1.0F;
   public int bB;
   public boolean bC;

   public class_vw(World var1) {
      super(var1);
      this.a(0.4F, 0.7F);
      this.bB = this.random.nextInt(6000) + 6000;
      this.a(class_ayl.WATER, 0.0F);
   }

   protected void r() {
      this.bp.a(0, new class_tg(this));
      this.bp.a(1, new class_ub(this, 1.4D));
      this.bp.a(2, new class_tc(this, 1.0D));
      this.bp.a(3, new PathfinderGoalTempt(this, 1.0D, false, bD));
      this.bp.a(4, new class_ti(this, 1.1D));
      this.bp.a(5, new class_uf(this, 1.0D));
      this.bp.a(6, new class_to(this, EntityHuman.class, 6.0F));
      this.bp.a(7, new class_ue(this));
   }

   public float bm() {
      return this.length;
   }

   protected void bz() {
      super.bz();
      this.a((class_sk)class_ys.a).a(4.0D);
      this.a((class_sk)class_ys.d).a(0.25D);
   }

   public void n() {
      super.n();
      this.bz = this.bv;
      this.bx = this.bw;
      this.bw = (float)((double)this.bw + (double)(this.onGround?-1:4) * 0.3D);
      this.bw = MathHelper.a(this.bw, 0.0F, 1.0F);
      if(!this.onGround && this.bA < 1.0F) {
         this.bA = 1.0F;
      }

      this.bA = (float)((double)this.bA * 0.9D);
      if(!this.onGround && this.motY < 0.0D) {
         this.motY *= 0.6D;
      }

      this.bv += this.bA * 2.0F;
      if(!this.world.E && !this.m_() && !this.cZ() && --this.bB <= 0) {
         this.a(class_ng.Y, 1.0F, (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
         this.a(Items.aW, 1);
         this.bB = this.random.nextInt(6000) + 6000;
      }

   }

   public void e(float var1, float var2) {
   }

   protected class_nf G() {
      return class_ng.W;
   }

   protected class_nf bQ() {
      return class_ng.Z;
   }

   protected class_nf bR() {
      return class_ng.X;
   }

   protected void a(BlockPosition var1, Block var2) {
      this.a(class_ng.aa, 0.15F, 1.0F);
   }

   protected class_kk J() {
      return class_azs.B;
   }

   public class_vw b(class_rn var1) {
      return new class_vw(this.world);
   }

   public boolean e(ItemStack var1) {
      return var1 != null && bD.contains(var1.b());
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.bC = var1.p("IsChickenJockey");
      if(var1.e("EggLayTime")) {
         this.bB = var1.h("EggLayTime");
      }

   }

   protected int b(EntityHuman var1) {
      return this.cZ()?10:super.b(var1);
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("IsChickenJockey", this.bC);
      var1.a("EggLayTime", this.bB);
   }

   protected boolean K() {
      return this.cZ() && !this.aI();
   }

   public void k(Entity var1) {
      super.k(var1);
      float var2 = MathHelper.a(this.aM * 0.017453292F);
      float var3 = MathHelper.b(this.aM * 0.017453292F);
      float var4 = 0.1F;
      float var5 = 0.0F;
      var1.b(this.locX + (double)(var4 * var2), this.locY + (double)(this.length * 0.5F) + var1.aw() + (double)var5, this.locZ - (double)(var4 * var3));
      if(var1 instanceof class_rz) {
         ((class_rz)var1).aM = this.aM;
      }

   }

   public boolean cZ() {
      return this.bC;
   }

   public void o(boolean var1) {
      this.bC = var1;
   }

   // $FF: synthetic method
   public class_rn a(class_rn var1) {
      return this.b(var1);
   }
}
