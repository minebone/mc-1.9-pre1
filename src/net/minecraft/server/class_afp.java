package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import net.minecraft.server.Block;
import net.minecraft.server.BlockSmoothBrick;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumColor;
import net.minecraft.server.Item;
import net.minecraft.server.ItemFish;
import net.minecraft.server.Items;
import net.minecraft.server.ItemStack;

public class class_afp {
   private static final class_afp a = new class_afp();
   private Map b = Maps.newHashMap();
   private Map c = Maps.newHashMap();

   public static class_afp a() {
      return a;
   }

   private class_afp() {
      this.a(Blocks.p, new ItemStack(Items.l), 0.7F);
      this.a(Blocks.o, new ItemStack(Items.m), 1.0F);
      this.a(Blocks.ag, new ItemStack(Items.k), 1.0F);
      this.a((Block)Blocks.m, new ItemStack(Blocks.w), 0.1F);
      this.a(Items.an, new ItemStack(Items.ao), 0.35F);
      this.a(Items.bp, new ItemStack(Items.bq), 0.35F);
      this.a(Items.br, new ItemStack(Items.bs), 0.35F);
      this.a(Items.bv, new ItemStack(Items.bw), 0.35F);
      this.a(Items.bt, new ItemStack(Items.bu), 0.35F);
      this.a(Blocks.e, new ItemStack(Blocks.b), 0.1F);
      this.a(new ItemStack(Blocks.bf, 1, BlockSmoothBrick.b), new ItemStack(Blocks.bf, 1, BlockSmoothBrick.d), 0.1F);
      this.a(Items.aP, new ItemStack(Items.aO), 0.3F);
      this.a(Blocks.aL, new ItemStack(Blocks.cz), 0.35F);
      this.a((Block)Blocks.aK, new ItemStack(Items.bd, 1, EnumColor.GREEN.b()), 0.2F);
      this.a(Blocks.r, new ItemStack(Items.j, 1, 1), 0.15F);
      this.a(Blocks.s, new ItemStack(Items.j, 1, 1), 0.15F);
      this.a(Blocks.bP, new ItemStack(Items.bY), 1.0F);
      this.a(Items.cc, new ItemStack(Items.cd), 0.35F);
      this.a(Blocks.aV, new ItemStack(Items.cp), 0.1F);
      this.a(new ItemStack(Blocks.v, 1, 1), new ItemStack(Blocks.v, 1, 0), 0.15F);
      this.a(Items.cS, new ItemStack(Items.cT), 0.1F);
      ItemFish.EnumFish[] var1 = ItemFish.EnumFish.values();
      int var2 = var1.length;

      for(int var3 = 0; var3 < var2; ++var3) {
         ItemFish.EnumFish var4 = var1[var3];
         if(var4.g()) {
            this.a(new ItemStack(Items.bb, 1, var4.a()), new ItemStack(Items.bc, 1, var4.a()), 0.35F);
         }
      }

      this.a(Blocks.q, new ItemStack(Items.j), 0.1F);
      this.a(Blocks.aC, new ItemStack(Items.aE), 0.7F);
      this.a(Blocks.x, new ItemStack(Items.bd, 1, EnumColor.BLUE.b()), 0.2F);
      this.a(Blocks.co, new ItemStack(Items.cq), 0.2F);
   }

   public void a(Block var1, ItemStack var2, float var3) {
      this.a(Item.a(var1), var2, var3);
   }

   public void a(Item var1, ItemStack var2, float var3) {
      this.a(new ItemStack(var1, 1, 32767), var2, var3);
   }

   public void a(ItemStack var1, ItemStack var2, float var3) {
      this.b.put(var1, var2);
      this.c.put(var2, Float.valueOf(var3));
   }

   public ItemStack a(ItemStack var1) {
      Iterator var2 = this.b.entrySet().iterator();

      Entry var3;
      do {
         if(!var2.hasNext()) {
            return null;
         }

         var3 = (Entry)var2.next();
      } while(!this.a(var1, (ItemStack)var3.getKey()));

      return (ItemStack)var3.getValue();
   }

   private boolean a(ItemStack var1, ItemStack var2) {
      return var2.b() == var1.b() && (var2.i() == 32767 || var2.i() == var1.i());
   }

   public Map b() {
      return this.b;
   }

   public float b(ItemStack var1) {
      Iterator var2 = this.c.entrySet().iterator();

      Entry var3;
      do {
         if(!var2.hasNext()) {
            return 0.0F;
         }

         var3 = (Entry)var2.next();
      } while(!this.a(var1, (ItemStack)var3.getKey()));

      return ((Float)var3.getValue()).floatValue();
   }
}
