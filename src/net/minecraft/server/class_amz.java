package net.minecraft.server;

import com.google.common.base.Predicate;
import net.minecraft.server.Block;
import net.minecraft.server.BlockLogAbstract;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Item;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;

public class class_amz extends BlockLogAbstract {
   public static final BlockStateEnum b = BlockStateEnum.a("variant", BlockWood.EnumLogVariant.class, new Predicate() {
      public boolean a(BlockWood.EnumLogVariant var1) {
         return var1.a() >= 4;
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((BlockWood.EnumLogVariant)var1);
      }
   });

   public class_amz() {
      this.w(this.A.b().set(b, BlockWood.EnumLogVariant.ACACIA).set(a, BlockLogAbstract.EnumLogRotation.Y));
   }

   public MaterialMapColor r(IBlockData var1) {
      BlockWood.EnumLogVariant var2 = (BlockWood.EnumLogVariant)var1.get(b);
      switch(class_amz.SyntheticClass_1.b[((BlockLogAbstract.EnumLogRotation)var1.get(a)).ordinal()]) {
      case 1:
      case 2:
      case 3:
      default:
         switch(class_amz.SyntheticClass_1.a[var2.ordinal()]) {
         case 1:
         default:
            return MaterialMapColor.m;
         case 2:
            return BlockWood.EnumLogVariant.DARK_OAK.c();
         }
      case 4:
         return var2.c();
      }
   }

   public IBlockData a(int var1) {
      IBlockData var2 = this.u().set(b, BlockWood.EnumLogVariant.a((var1 & 3) + 4));
      switch(var1 & 12) {
      case 0:
         var2 = var2.set(a, BlockLogAbstract.EnumLogRotation.Y);
         break;
      case 4:
         var2 = var2.set(a, BlockLogAbstract.EnumLogRotation.X);
         break;
      case 8:
         var2 = var2.set(a, BlockLogAbstract.EnumLogRotation.Z);
         break;
      default:
         var2 = var2.set(a, BlockLogAbstract.EnumLogRotation.NONE);
      }

      return var2;
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((BlockWood.EnumLogVariant)var1.get(b)).a() - 4;
      switch(class_amz.SyntheticClass_1.b[((BlockLogAbstract.EnumLogRotation)var1.get(a)).ordinal()]) {
      case 1:
         var3 |= 4;
         break;
      case 2:
         var3 |= 8;
         break;
      case 3:
         var3 |= 12;
      }

      return var3;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{b, a});
   }

   protected ItemStack u(IBlockData var1) {
      return new ItemStack(Item.a((Block)this), 1, ((BlockWood.EnumLogVariant)var1.get(b)).a() - 4);
   }

   public int d(IBlockData var1) {
      return ((BlockWood.EnumLogVariant)var1.get(b)).a() - 4;
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b = new int[BlockLogAbstract.EnumLogRotation.values().length];

      static {
         try {
            b[BlockLogAbstract.EnumLogRotation.X.ordinal()] = 1;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            b[BlockLogAbstract.EnumLogRotation.Z.ordinal()] = 2;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            b[BlockLogAbstract.EnumLogRotation.NONE.ordinal()] = 3;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            b[BlockLogAbstract.EnumLogRotation.Y.ordinal()] = 4;
         } catch (NoSuchFieldError var3) {
            ;
         }

         a = new int[BlockWood.EnumLogVariant.values().length];

         try {
            a[BlockWood.EnumLogVariant.ACACIA.ordinal()] = 1;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[BlockWood.EnumLogVariant.DARK_OAK.ordinal()] = 2;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
