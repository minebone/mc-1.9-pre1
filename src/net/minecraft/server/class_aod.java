package net.minecraft.server;

import net.minecraft.server.EnumDirection;

public enum class_aod {
   NONE("rotate_0"),
   CLOCKWISE_90("rotate_90"),
   CLOCKWISE_180("rotate_180"),
   COUNTERCLOCKWISE_90("rotate_270");

   private final String e;
   private static String[] f = new String[values().length];

   private class_aod(String var3) {
      this.e = var3;
   }

   public class_aod a(class_aod var1) {
      switch(class_aod.SyntheticClass_1.a[var1.ordinal()]) {
      case 3:
         switch(class_aod.SyntheticClass_1.a[this.ordinal()]) {
         case 1:
            return CLOCKWISE_180;
         case 2:
            return COUNTERCLOCKWISE_90;
         case 3:
            return NONE;
         case 4:
            return CLOCKWISE_90;
         }
      case 4:
         switch(class_aod.SyntheticClass_1.a[this.ordinal()]) {
         case 1:
            return COUNTERCLOCKWISE_90;
         case 2:
            return NONE;
         case 3:
            return CLOCKWISE_90;
         case 4:
            return CLOCKWISE_180;
         }
      case 2:
         switch(class_aod.SyntheticClass_1.a[this.ordinal()]) {
         case 1:
            return CLOCKWISE_90;
         case 2:
            return CLOCKWISE_180;
         case 3:
            return COUNTERCLOCKWISE_90;
         case 4:
            return NONE;
         }
      default:
         return this;
      }
   }

   public EnumDirection a(EnumDirection var1) {
      if(var1.k() == EnumDirection.class_a_in_class_cq.Y) {
         return var1;
      } else {
         switch(class_aod.SyntheticClass_1.a[this.ordinal()]) {
         case 2:
            return var1.e();
         case 3:
            return var1.d();
         case 4:
            return var1.f();
         default:
            return var1;
         }
      }
   }

   public int a(int var1, int var2) {
      switch(class_aod.SyntheticClass_1.a[this.ordinal()]) {
      case 2:
         return (var1 + var2 / 4) % var2;
      case 3:
         return (var1 + var2 / 2) % var2;
      case 4:
         return (var1 + var2 * 3 / 4) % var2;
      default:
         return var1;
      }
   }

   static {
      int var0 = 0;
      class_aod[] var1 = values();
      int var2 = var1.length;

      for(int var3 = 0; var3 < var2; ++var3) {
         class_aod var4 = var1[var3];
         f[var0++] = var4.e;
      }

   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[class_aod.values().length];

      static {
         try {
            a[class_aod.NONE.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[class_aod.CLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[class_aod.CLOCKWISE_180.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
