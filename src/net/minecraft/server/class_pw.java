package net.minecraft.server;

import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.class_oz;
import net.minecraft.server.class_pa;
import net.minecraft.server.class_pd;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_pw implements class_pd {
   private static final Logger a = LogManager.getLogger();

   public NBTTagCompound a(class_pa var1, NBTTagCompound var2, int var3) {
      NBTTagCompound var4 = var2.o("tag");
      if(var4.b("EntityTag", 10)) {
         NBTTagCompound var5 = var4.o("EntityTag");
         String var6 = var2.l("id");
         String var7;
         if("minecraft:armor_stand".equals(var6)) {
            var7 = "ArmorStand";
         } else {
            if(!"minecraft:spawn_egg".equals(var6)) {
               return var2;
            }

            var7 = var5.l("id");
         }

         boolean var8;
         if(var7 == null) {
            a.warn("Unable to resolve Entity for ItemInstance: {}", new Object[]{var6});
            var8 = false;
         } else {
            var8 = !var5.b("id", 8);
            var5.a("id", var7);
         }

         var1.a(class_oz.ENTITY, var5, var3);
         if(var8) {
            var5.q("id");
         }
      }

      return var2;
   }
}
