package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockHugeMushroom;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_auc;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_aui extends class_auc {
   private final Block a;

   public class_aui(Block var1) {
      super(true);
      this.a = var1;
   }

   public class_aui() {
      super(false);
      this.a = null;
   }

   public boolean b(World var1, Random var2, BlockPosition var3) {
      Block var4 = this.a;
      if(var4 == null) {
         var4 = var2.nextBoolean()?Blocks.bg:Blocks.bh;
      }

      int var5 = var2.nextInt(3) + 4;
      boolean var6 = true;
      if(var3.q() >= 1 && var3.q() + var5 + 1 < 256) {
         int var10;
         int var11;
         for(int var7 = var3.q(); var7 <= var3.q() + 1 + var5; ++var7) {
            byte var8 = 3;
            if(var7 <= var3.q() + 3) {
               var8 = 0;
            }

            BlockPosition.class_a_in_class_cj var9 = new BlockPosition.class_a_in_class_cj();

            for(var10 = var3.p() - var8; var10 <= var3.p() + var8 && var6; ++var10) {
               for(var11 = var3.r() - var8; var11 <= var3.r() + var8 && var6; ++var11) {
                  if(var7 >= 0 && var7 < 256) {
                     Material var12 = var1.getType(var9.c(var10, var7, var11)).getMaterial();
                     if(var12 != Material.a && var12 != Material.j) {
                        var6 = false;
                     }
                  } else {
                     var6 = false;
                  }
               }
            }
         }

         if(!var6) {
            return false;
         } else {
            Block var20 = var1.getType(var3.b()).getBlock();
            if(var20 != Blocks.d && var20 != Blocks.c && var20 != Blocks.bw) {
               return false;
            } else {
               int var21 = var3.q() + var5;
               if(var4 == Blocks.bh) {
                  var21 = var3.q() + var5 - 3;
               }

               int var22;
               for(var22 = var21; var22 <= var3.q() + var5; ++var22) {
                  var10 = 1;
                  if(var22 < var3.q() + var5) {
                     ++var10;
                  }

                  if(var4 == Blocks.bg) {
                     var10 = 3;
                  }

                  var11 = var3.p() - var10;
                  int var24 = var3.p() + var10;
                  int var13 = var3.r() - var10;
                  int var14 = var3.r() + var10;

                  for(int var15 = var11; var15 <= var24; ++var15) {
                     for(int var16 = var13; var16 <= var14; ++var16) {
                        int var17 = 5;
                        if(var15 == var11) {
                           --var17;
                        } else if(var15 == var24) {
                           ++var17;
                        }

                        if(var16 == var13) {
                           var17 -= 3;
                        } else if(var16 == var14) {
                           var17 += 3;
                        }

                        BlockHugeMushroom.EnumHugeMushroomVariant var18 = BlockHugeMushroom.EnumHugeMushroomVariant.a(var17);
                        if(var4 == Blocks.bg || var22 < var3.q() + var5) {
                           if((var15 == var11 || var15 == var24) && (var16 == var13 || var16 == var14)) {
                              continue;
                           }

                           if(var15 == var3.p() - (var10 - 1) && var16 == var13) {
                              var18 = BlockHugeMushroom.EnumHugeMushroomVariant.NORTH_WEST;
                           }

                           if(var15 == var11 && var16 == var3.r() - (var10 - 1)) {
                              var18 = BlockHugeMushroom.EnumHugeMushroomVariant.NORTH_WEST;
                           }

                           if(var15 == var3.p() + (var10 - 1) && var16 == var13) {
                              var18 = BlockHugeMushroom.EnumHugeMushroomVariant.NORTH_EAST;
                           }

                           if(var15 == var24 && var16 == var3.r() - (var10 - 1)) {
                              var18 = BlockHugeMushroom.EnumHugeMushroomVariant.NORTH_EAST;
                           }

                           if(var15 == var3.p() - (var10 - 1) && var16 == var14) {
                              var18 = BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH_WEST;
                           }

                           if(var15 == var11 && var16 == var3.r() + (var10 - 1)) {
                              var18 = BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH_WEST;
                           }

                           if(var15 == var3.p() + (var10 - 1) && var16 == var14) {
                              var18 = BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH_EAST;
                           }

                           if(var15 == var24 && var16 == var3.r() + (var10 - 1)) {
                              var18 = BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH_EAST;
                           }
                        }

                        if(var18 == BlockHugeMushroom.EnumHugeMushroomVariant.CENTER && var22 < var3.q() + var5) {
                           var18 = BlockHugeMushroom.EnumHugeMushroomVariant.ALL_INSIDE;
                        }

                        if(var3.q() >= var3.q() + var5 - 1 || var18 != BlockHugeMushroom.EnumHugeMushroomVariant.ALL_INSIDE) {
                           BlockPosition var19 = new BlockPosition(var15, var22, var16);
                           if(!var1.getType(var19).b()) {
                              this.a(var1, var19, var4.u().set(BlockHugeMushroom.a, var18));
                           }
                        }
                     }
                  }
               }

               for(var22 = 0; var22 < var5; ++var22) {
                  IBlockData var23 = var1.getType(var3.b(var22));
                  if(!var23.b()) {
                     this.a(var1, var3.b(var22), var4.u().set(BlockHugeMushroom.a, BlockHugeMushroom.EnumHugeMushroomVariant.STEM));
                  }
               }

               return true;
            }
         }
      } else {
         return false;
      }
   }
}
