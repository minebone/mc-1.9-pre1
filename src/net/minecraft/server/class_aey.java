package net.minecraft.server;

import net.minecraft.server.World;
import net.minecraft.server.class_aac;
import net.minecraft.server.class_abx;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aff;
import net.minecraft.server.class_di;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_zl;

public class class_aey extends class_abx {
   public class_zl a(World var1, ItemStack var2, class_rz var3) {
      class_aac var4 = new class_aac(var1, var3);
      var4.a(var2);
      return var4;
   }

   public String a(ItemStack var1) {
      return class_di.a(class_aff.c(var1).b("tipped_arrow.effect."));
   }
}
