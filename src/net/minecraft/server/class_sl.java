package net.minecraft.server;

import java.util.Collection;
import java.util.UUID;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.class_sk;

public interface class_sl {
   class_sk a();

   double b();

   void a(double var1);

   Collection a(int var1);

   Collection c();

   boolean a(AttributeModifier var1);

   AttributeModifier a(UUID var1);

   void b(AttributeModifier var1);

   void c(AttributeModifier var1);

   void b(UUID var1);

   double e();
}
