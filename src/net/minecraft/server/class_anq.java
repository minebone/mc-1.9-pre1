package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aly;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_or;

public abstract class class_anq extends class_aly {
   public static final BlockStateEnum d = BlockStateEnum.a("variant", class_anq.class_c_in_class_anq.class);

   public class_anq() {
      super(Material.e);
      IBlockData var1 = this.A.b();
      if(!this.e()) {
         var1 = var1.set(a, class_aly.class_a_in_class_aly.BOTTOM);
      }

      this.w(var1.set(d, class_anq.class_c_in_class_anq.DEFAULT));
      this.a(CreativeModeTab.b);
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Item.a((Block)Blocks.cX);
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Blocks.cX);
   }

   public IBlockData a(int var1) {
      IBlockData var2 = this.u().set(d, class_anq.class_c_in_class_anq.DEFAULT);
      if(!this.e()) {
         var2 = var2.set(a, (var1 & 8) == 0?class_aly.class_a_in_class_aly.BOTTOM:class_aly.class_a_in_class_aly.TOP);
      }

      return var2;
   }

   public int e(IBlockData var1) {
      int var2 = 0;
      if(!this.e() && var1.get(a) == class_aly.class_a_in_class_aly.TOP) {
         var2 |= 8;
      }

      return var2;
   }

   protected BlockStateList b() {
      return this.e()?new BlockStateList(this, new IBlockState[]{d}):new BlockStateList(this, new IBlockState[]{a, d});
   }

   public String e(int var1) {
      return super.a();
   }

   public IBlockState g() {
      return d;
   }

   public Comparable a(ItemStack var1) {
      return class_anq.class_c_in_class_anq.DEFAULT;
   }

   public static enum class_c_in_class_anq implements class_or {
      DEFAULT;

      public String m() {
         return "default";
      }
   }

   public static class class_a_in_class_anq extends class_anq {
      public boolean e() {
         return true;
      }
   }

   public static class class_b_in_class_anq extends class_anq {
      public boolean e() {
         return false;
      }
   }
}
