package net.minecraft.server;

import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.WorldProviderNormal;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_ain;
import net.minecraft.server.class_arx;
import net.minecraft.server.class_asu;
import net.minecraft.server.class_asz;
import net.minecraft.server.class_atn;
import net.minecraft.server.BlockPosition;

public class class_ata extends class_asu {
   private class_asz g = null;

   public void b() {
      this.c = new class_ain(class_aik.k);
      this.isNotOverworld = true;
      NBTTagCompound var1 = this.b.T().a(WorldProviderNormal.THE_END);
      this.g = this.b instanceof WorldServer?new class_asz((WorldServer)this.b, var1.o("DragonFight")):null;
   }

   public class_arx c() {
      return new class_atn(this.b, this.b.T().r(), this.b.O());
   }

   public float a(long var1, float var3) {
      return 0.0F;
   }

   public boolean e() {
      return false;
   }

   public boolean d() {
      return false;
   }

   public boolean a(int var1, int var2) {
      return this.b.c(new BlockPosition(var1, 0, var2)).getMaterial().c();
   }

   public BlockPosition h() {
      return new BlockPosition(100, 50, 0);
   }

   public int i() {
      return 50;
   }

   public WorldProviderNormal p() {
      return WorldProviderNormal.THE_END;
   }

   public void q() {
      NBTTagCompound var1 = new NBTTagCompound();
      if(this.g != null) {
         var1.a((String)"DragonFight", (NBTTag)this.g.a());
      }

      this.b.T().a(WorldProviderNormal.THE_END, var1);
   }

   public void r() {
      if(this.g != null) {
         this.g.b();
      }

   }

   public class_asz s() {
      return this.g;
   }
}
