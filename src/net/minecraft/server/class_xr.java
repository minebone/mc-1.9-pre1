package net.minecraft.server;

import com.google.common.base.Optional;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_adv;
import net.minecraft.server.class_ayy;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_xq;

public class class_xr extends class_xq {
   private static final class_ke c = DataWatcher.a(class_xr.class, class_kg.f);
   private static final class_ke d = DataWatcher.a(class_xr.class, class_kg.b);
   private float e = 1.0F;

   public class_xr(World var1) {
      super(var1);
   }

   public class_xr(World var1, BlockPosition var2, EnumDirection var3) {
      super(var1, var2);
      this.a((EnumDirection)var3);
   }

   protected void i() {
      this.Q().a((class_ke)c, (Object)Optional.absent());
      this.Q().a((class_ke)d, (Object)Integer.valueOf(0));
   }

   public float az() {
      return 0.0F;
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b((DamageSource)var1)) {
         return false;
      } else if(!var1.c() && this.r() != null) {
         if(!this.world.E) {
            this.b(var1.j(), false);
            this.a(class_ng.cO, 1.0F, 1.0F);
            this.a((ItemStack)null);
         }

         return true;
      } else {
         return super.a(var1, var2);
      }
   }

   public int l() {
      return 12;
   }

   public int n() {
      return 12;
   }

   public void a(Entity var1) {
      this.a(class_ng.cM, 1.0F, 1.0F);
      this.b(var1, true);
   }

   public void o() {
      this.a(class_ng.cN, 1.0F, 1.0F);
   }

   public void b(Entity var1, boolean var2) {
      if(this.world.U().b("doEntityDrops")) {
         ItemStack var3 = this.r();
         if(var1 instanceof EntityHuman) {
            EntityHuman var4 = (EntityHuman)var1;
            if(var4.abilities.d) {
               this.b(var3);
               return;
            }
         }

         if(var2) {
            this.a(new ItemStack(Items.bZ), 0.0F);
         }

         if(var3 != null && this.random.nextFloat() < this.e) {
            var3 = var3.k();
            this.b(var3);
            this.a(var3, 0.0F);
         }

      }
   }

   private void b(ItemStack var1) {
      if(var1 != null) {
         if(var1.b() == Items.bk) {
            class_ayy var2 = ((class_adv)var1.b()).a(var1, this.world);
            var2.i.remove("frame-" + this.getId());
         }

         var1.a((class_xr)null);
      }
   }

   public ItemStack r() {
      return (ItemStack)((Optional)this.Q().a(c)).orNull();
   }

   public void a(ItemStack var1) {
      this.a(var1, true);
   }

   private void a(ItemStack var1, boolean var2) {
      if(var1 != null) {
         var1 = var1.k();
         var1.b = 1;
         var1.a(this);
      }

      this.Q().b(c, Optional.fromNullable(var1));
      this.Q().b(c);
      if(var1 != null) {
         this.a(class_ng.cL, 1.0F, 1.0F);
      }

      if(var2 && this.a != null) {
         this.world.f(this.a, Blocks.AIR);
      }

   }

   public void a(class_ke var1) {
      if(var1.equals(c)) {
         ItemStack var2 = this.r();
         if(var2 != null && var2.z() != this) {
            var2.a(this);
         }
      }

   }

   public int s() {
      return ((Integer)this.Q().a(d)).intValue();
   }

   public void a(int var1) {
      this.a(var1, true);
   }

   private void a(int var1, boolean var2) {
      this.Q().b(d, Integer.valueOf(var1 % 8));
      if(var2 && this.a != null) {
         this.world.f(this.a, Blocks.AIR);
      }

   }

   public void b(NBTTagCompound var1) {
      if(this.r() != null) {
         var1.a((String)"Item", (NBTTag)this.r().b(new NBTTagCompound()));
         var1.a("ItemRotation", (byte)this.s());
         var1.a("ItemDropChance", this.e);
      }

      super.b(var1);
   }

   public void a(NBTTagCompound var1) {
      NBTTagCompound var2 = var1.o("Item");
      if(var2 != null && !var2.c_()) {
         this.a(ItemStack.a(var2), false);
         this.a(var1.f("ItemRotation"), false);
         if(var1.b("ItemDropChance", 99)) {
            this.e = var1.j("ItemDropChance");
         }
      }

      super.a(var1);
   }

   public boolean a(EntityHuman var1, ItemStack var2, EnumHand var3) {
      if(this.r() == null) {
         if(var2 != null && !this.world.E) {
            this.a(var2);
            if(!var1.abilities.d) {
               --var2.b;
            }
         }
      } else if(!this.world.E) {
         this.a(class_ng.cP, 1.0F, 1.0F);
         this.a(this.s() + 1);
      }

      return true;
   }

   public int t() {
      return this.r() == null?0:this.s() % 8 + 1;
   }
}
