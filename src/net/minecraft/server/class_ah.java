package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.WorldSettings;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cb;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;

public class class_ah extends CommandAbstract {
   public String c() {
      return "gamemode";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.gamemode.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length <= 0) {
         throw new class_cf("commands.gamemode.usage", new Object[0]);
      } else {
         WorldSettings.EnumGamemode var4 = this.c(var2, var3[0]);
         EntityPlayer var5 = var3.length >= 2?a(var1, var2, var3[1]):a(var2);
         var5.a(var4);
         ChatMessage var6 = new ChatMessage("gameMode." + var4.b(), new Object[0]);
         if(var2.e().U().b("sendCommandFeedback")) {
            var5.a((IChatBaseComponent)(new ChatMessage("gameMode.changed", new Object[]{var6})));
         }

         if(var5 != var2) {
            a(var2, this, 1, "commands.gamemode.success.other", new Object[]{var5.h_(), var6});
         } else {
            a(var2, this, 1, "commands.gamemode.success.self", new Object[]{var6});
         }

      }
   }

   protected WorldSettings.EnumGamemode c(ICommandListener var1, String var2) throws class_cb {
      WorldSettings.EnumGamemode var3 = WorldSettings.EnumGamemode.a(var2, WorldSettings.EnumGamemode.NOT_SET);
      return var3 == WorldSettings.EnumGamemode.NOT_SET?WorldSettings.a(a(var2, 0, WorldSettings.EnumGamemode.values().length - 2)):var3;
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, new String[]{"survival", "creative", "adventure", "spectator"}):(var3.length == 2?a(var3, var1.J()):Collections.emptyList());
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 1;
   }
}
