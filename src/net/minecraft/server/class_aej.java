package net.minecraft.server;

import net.minecraft.server.Item;
import net.minecraft.server.class_ado;
import net.minecraft.server.Packet;
import net.minecraft.server.class_gg;
import net.minecraft.server.EntityPlayer;

public class class_aej extends class_ado {
   private final EntityPlayer a;

   public class_aej(EntityPlayer var1) {
      this.a = var1;
   }

   protected void b(Item var1, int var2) {
      super.b(var1, var2);
      this.a.a.a((Packet)(new class_gg(var1, var2)));
   }

   protected void c(Item var1) {
      super.c(var1);
      this.a.a.a((Packet)(new class_gg(var1, 0)));
   }
}
