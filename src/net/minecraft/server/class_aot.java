package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.World;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_aoi;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.BlockPosition;

public class class_aot extends class_aoi {
   public static final BlockStateInteger b = BlockStateInteger.a("rotation", 0, 15);

   public class_aot() {
      this.w(this.A.b().set(b, Integer.valueOf(0)));
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!var1.getType(var2.b()).getMaterial().a()) {
         this.b(var1, var2, var3, 0);
         var1.g(var2);
      }

      super.a(var1, var2, var3, var4);
   }

   public IBlockData a(int var1) {
      return this.u().set(b, Integer.valueOf(var1));
   }

   public int e(IBlockData var1) {
      return ((Integer)var1.get(b)).intValue();
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(b, Integer.valueOf(var2.a(((Integer)var1.get(b)).intValue(), 16)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.set(b, Integer.valueOf(var2.a(((Integer)var1.get(b)).intValue(), 16)));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{b});
   }
}
