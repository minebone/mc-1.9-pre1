package net.minecraft.server;

import net.minecraft.server.EntityTameableAnimal;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_tj;

public class class_ul extends class_tj {
   private EntityTameableAnimal a;
   private boolean b;

   public class_ul(EntityTameableAnimal var1) {
      this.a = var1;
      this.a(5);
   }

   public boolean a() {
      if(!this.a.cZ()) {
         return false;
      } else if(this.a.ah()) {
         return false;
      } else if(!this.a.onGround) {
         return false;
      } else {
         class_rz var1 = this.a.dc();
         return var1 == null?true:(this.a.h(var1) < 144.0D && var1.bF() != null?false:this.b);
      }
   }

   public void c() {
      this.a.x().o();
      this.a.q(true);
   }

   public void d() {
      this.a.q(false);
   }

   public void a(boolean var1) {
      this.b = var1;
   }
}
