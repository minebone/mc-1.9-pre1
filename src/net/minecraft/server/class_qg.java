package net.minecraft.server;

import net.minecraft.server.ItemStack;

public class class_qg {
   public static ItemStack a(ItemStack[] var0, int var1, int var2) {
      if(var1 >= 0 && var1 < var0.length && var0[var1] != null && var2 > 0) {
         ItemStack var3 = var0[var1].a(var2);
         if(var0[var1].b == 0) {
            var0[var1] = null;
         }

         return var3;
      } else {
         return null;
      }
   }

   public static ItemStack a(ItemStack[] var0, int var1) {
      if(var1 >= 0 && var1 < var0.length) {
         ItemStack var2 = var0[var1];
         var0[var1] = null;
         return var2;
      } else {
         return null;
      }
   }
}
