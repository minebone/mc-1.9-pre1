package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Iterator;
import java.util.Map;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockMinecartTrackAbstract;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityMinecartChest;
import net.minecraft.server.EntityMinecartFurnace;
import net.minecraft.server.EntityMinecartHopper;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Items;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_aai;
import net.minecraft.server.class_aam;
import net.minecraft.server.class_aan;
import net.minecraft.server.class_aao;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_anm;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_qs;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_wg;

public abstract class EntityMinecartAbstract extends Entity implements class_qs {
   private static final class_ke a = DataWatcher.a(EntityMinecartAbstract.class, class_kg.b);
   private static final class_ke b = DataWatcher.a(EntityMinecartAbstract.class, class_kg.b);
   private static final class_ke c = DataWatcher.a(EntityMinecartAbstract.class, class_kg.c);
   private static final class_ke d = DataWatcher.a(EntityMinecartAbstract.class, class_kg.b);
   private static final class_ke e = DataWatcher.a(EntityMinecartAbstract.class, class_kg.b);
   private static final class_ke f = DataWatcher.a(EntityMinecartAbstract.class, class_kg.h);
   private boolean g;
   private static final int[][][] h = new int[][][]{{{0, 0, -1}, {0, 0, 1}}, {{-1, 0, 0}, {1, 0, 0}}, {{-1, -1, 0}, {1, 0, 0}}, {{-1, 0, 0}, {1, -1, 0}}, {{0, 0, -1}, {0, -1, 1}}, {{0, -1, -1}, {0, 0, 1}}, {{0, 0, 1}, {1, 0, 0}}, {{0, 0, 1}, {-1, 0, 0}}, {{0, 0, -1}, {-1, 0, 0}}, {{0, 0, -1}, {1, 0, 0}}};
   private int as;
   private double at;
   private double au;
   private double av;
   private double aw;
   private double ax;

   public EntityMinecartAbstract(World var1) {
      super(var1);
      this.i = true;
      this.a(0.98F, 0.7F);
   }

   public static EntityMinecartAbstract a(World var0, double var1, double var3, double var5, EntityMinecartAbstract.EnumMinecartType var7) {
      switch(EntityMinecartAbstract.SyntheticClass_1.a[var7.ordinal()]) {
      case 1:
         return new EntityMinecartChest(var0, var1, var3, var5);
      case 2:
         return new EntityMinecartFurnace(var0, var1, var3, var5);
      case 3:
         return new class_aao(var0, var1, var3, var5);
      case 4:
         return new class_aan(var0, var1, var3, var5);
      case 5:
         return new EntityMinecartHopper(var0, var1, var3, var5);
      case 6:
         return new class_aai(var0, var1, var3, var5);
      default:
         return new class_aam(var0, var1, var3, var5);
      }
   }

   protected boolean ad() {
      return false;
   }

   protected void i() {
      this.datawatcher.a((class_ke)a, (Object)Integer.valueOf(0));
      this.datawatcher.a((class_ke)b, (Object)Integer.valueOf(1));
      this.datawatcher.a((class_ke)c, (Object)Float.valueOf(0.0F));
      this.datawatcher.a((class_ke)d, (Object)Integer.valueOf(0));
      this.datawatcher.a((class_ke)e, (Object)Integer.valueOf(6));
      this.datawatcher.a((class_ke)f, (Object)Boolean.valueOf(false));
   }

   public AxisAlignedBB j(Entity var1) {
      return var1.ap()?var1.bk():null;
   }

   public AxisAlignedBB ae() {
      return null;
   }

   public boolean ap() {
      return true;
   }

   public EntityMinecartAbstract(World var1, double var2, double var4, double var6) {
      this(var1);
      this.b(var2, var4, var6);
      this.motX = 0.0D;
      this.motY = 0.0D;
      this.motZ = 0.0D;
      this.lastX = var2;
      this.lastY = var4;
      this.lastZ = var6;
   }

   public double ax() {
      return 0.0D;
   }

   public boolean a(DamageSource var1, float var2) {
      if(!this.world.E && !this.dead) {
         if(this.b(var1)) {
            return false;
         } else {
            this.e(-this.u());
            this.d(10);
            this.an();
            this.a(this.s() + var2 * 10.0F);
            boolean var3 = var1.j() instanceof EntityHuman && ((EntityHuman)var1.j()).abilities.d;
            if(var3 || this.s() > 40.0F) {
               this.ay();
               if(var3 && !this.o_()) {
                  this.S();
               } else {
                  this.a(var1);
               }
            }

            return true;
         }
      } else {
         return true;
      }
   }

   public void a(DamageSource var1) {
      this.S();
      if(this.world.U().b("doEntityDrops")) {
         ItemStack var2 = new ItemStack(Items.aB, 1);
         if(this.h_() != null) {
            var2.c(this.h_());
         }

         this.a(var2, 0.0F);
      }

   }

   public boolean ao() {
      return !this.dead;
   }

   public void S() {
      super.S();
   }

   public EnumDirection bi() {
      return this.g?this.bh().d().e():this.bh().e();
   }

   public void m() {
      if(this.t() > 0) {
         this.d(this.t() - 1);
      }

      if(this.s() > 0.0F) {
         this.a(this.s() - 1.0F);
      }

      if(this.locY < -64.0D) {
         this.X();
      }

      int var2;
      if(!this.world.E && this.world instanceof WorldServer) {
         this.world.C.a("portal");
         MinecraftServer var1 = this.world.u();
         var2 = this.U();
         if(this.ak) {
            if(var1.E()) {
               if(!this.aH() && this.al++ >= var2) {
                  this.al = var2;
                  this.portalCooldown = this.aB();
                  byte var3;
                  if(this.world.s.p().a() == -1) {
                     var3 = 0;
                  } else {
                     var3 = -1;
                  }

                  this.c(var3);
               }

               this.ak = false;
            }
         } else {
            if(this.al > 0) {
               this.al -= 4;
            }

            if(this.al < 0) {
               this.al = 0;
            }
         }

         if(this.portalCooldown > 0) {
            --this.portalCooldown;
         }

         this.world.C.b();
      }

      if(this.world.E) {
         if(this.as > 0) {
            double var15 = this.locX + (this.at - this.locX) / (double)this.as;
            double var17 = this.locY + (this.au - this.locY) / (double)this.as;
            double var18 = this.locZ + (this.av - this.locZ) / (double)this.as;
            double var7 = MathHelper.g(this.aw - (double)this.yaw);
            this.yaw = (float)((double)this.yaw + var7 / (double)this.as);
            this.pitch = (float)((double)this.pitch + (this.ax - (double)this.pitch) / (double)this.as);
            --this.as;
            this.b(var15, var17, var18);
            this.b(this.yaw, this.pitch);
         } else {
            this.b(this.locX, this.locY, this.locZ);
            this.b(this.yaw, this.pitch);
         }

      } else {
         this.lastX = this.locX;
         this.lastY = this.locY;
         this.lastZ = this.locZ;
         this.motY -= 0.03999999910593033D;
         int var14 = MathHelper.c(this.locX);
         var2 = MathHelper.c(this.locY);
         int var16 = MathHelper.c(this.locZ);
         if(BlockMinecartTrackAbstract.b(this.world, new BlockPosition(var14, var2 - 1, var16))) {
            --var2;
         }

         BlockPosition var4 = new BlockPosition(var14, var2, var16);
         IBlockData var5 = this.world.getType(var4);
         if(BlockMinecartTrackAbstract.i(var5)) {
            this.a(var4, var5);
            if(var5.getBlock() == Blocks.cs) {
               this.a(var14, var2, var16, ((Boolean)var5.get(class_anm.e)).booleanValue());
            }
         } else {
            this.q();
         }

         this.ab();
         this.pitch = 0.0F;
         double var6 = this.lastX - this.locX;
         double var8 = this.lastZ - this.locZ;
         if(var6 * var6 + var8 * var8 > 0.001D) {
            this.yaw = (float)(MathHelper.b(var8, var6) * 180.0D / 3.141592653589793D);
            if(this.g) {
               this.yaw += 180.0F;
            }
         }

         double var10 = (double)MathHelper.g(this.yaw - this.lastYaw);
         if(var10 < -170.0D || var10 >= 170.0D) {
            this.yaw += 180.0F;
            this.g = !this.g;
         }

         this.b(this.yaw, this.pitch);
         Iterator var12 = this.world.b((Entity)this, (AxisAlignedBB)this.bk().b(0.20000000298023224D, 0.0D, 0.20000000298023224D)).iterator();

         while(var12.hasNext()) {
            Entity var13 = (Entity)var12.next();
            if(!this.w(var13) && var13.ap() && var13 instanceof EntityMinecartAbstract) {
               var13.i(this);
            }
         }

         this.ai();
      }
   }

   protected double o() {
      return 0.4D;
   }

   public void a(int var1, int var2, int var3, boolean var4) {
   }

   protected void q() {
      double var1 = this.o();
      this.motX = MathHelper.a(this.motX, -var1, var1);
      this.motZ = MathHelper.a(this.motZ, -var1, var1);
      if(this.onGround) {
         this.motX *= 0.5D;
         this.motY *= 0.5D;
         this.motZ *= 0.5D;
      }

      this.d(this.motX, this.motY, this.motZ);
      if(!this.onGround) {
         this.motX *= 0.949999988079071D;
         this.motY *= 0.949999988079071D;
         this.motZ *= 0.949999988079071D;
      }

   }

   protected void a(BlockPosition var1, IBlockData var2) {
      this.fallDistance = 0.0F;
      Vec3D var3 = this.k(this.locX, this.locY, this.locZ);
      this.locY = (double)var1.q();
      boolean var4 = false;
      boolean var5 = false;
      BlockMinecartTrackAbstract var6 = (BlockMinecartTrackAbstract)var2.getBlock();
      if(var6 == Blocks.D) {
         var4 = ((Boolean)var2.get(class_anm.e)).booleanValue();
         var5 = !var4;
      }

      double var7 = 0.0078125D;
      BlockMinecartTrackAbstract.EnumTrackPosition var9 = (BlockMinecartTrackAbstract.EnumTrackPosition)var2.get(var6.g());
      switch(EntityMinecartAbstract.SyntheticClass_1.b[var9.ordinal()]) {
      case 1:
         this.motX -= 0.0078125D;
         ++this.locY;
         break;
      case 2:
         this.motX += 0.0078125D;
         ++this.locY;
         break;
      case 3:
         this.motZ += 0.0078125D;
         ++this.locY;
         break;
      case 4:
         this.motZ -= 0.0078125D;
         ++this.locY;
      }

      int[][] var10 = h[var9.a()];
      double var11 = (double)(var10[1][0] - var10[0][0]);
      double var13 = (double)(var10[1][2] - var10[0][2]);
      double var15 = Math.sqrt(var11 * var11 + var13 * var13);
      double var17 = this.motX * var11 + this.motZ * var13;
      if(var17 < 0.0D) {
         var11 = -var11;
         var13 = -var13;
      }

      double var19 = Math.sqrt(this.motX * this.motX + this.motZ * this.motZ);
      if(var19 > 2.0D) {
         var19 = 2.0D;
      }

      this.motX = var19 * var11 / var15;
      this.motZ = var19 * var13 / var15;
      Entity var21 = this.bt().isEmpty()?null:(Entity)this.bt().get(0);
      double var22;
      double var24;
      double var26;
      double var28;
      if(var21 instanceof class_rz) {
         var22 = (double)((class_rz)var21).be;
         if(var22 > 0.0D) {
            var24 = -Math.sin((double)(var21.yaw * 0.017453292F));
            var26 = Math.cos((double)(var21.yaw * 0.017453292F));
            var28 = this.motX * this.motX + this.motZ * this.motZ;
            if(var28 < 0.01D) {
               this.motX += var24 * 0.1D;
               this.motZ += var26 * 0.1D;
               var5 = false;
            }
         }
      }

      if(var5) {
         var22 = Math.sqrt(this.motX * this.motX + this.motZ * this.motZ);
         if(var22 < 0.03D) {
            this.motX *= 0.0D;
            this.motY *= 0.0D;
            this.motZ *= 0.0D;
         } else {
            this.motX *= 0.5D;
            this.motY *= 0.0D;
            this.motZ *= 0.5D;
         }
      }

      var22 = 0.0D;
      var24 = (double)var1.p() + 0.5D + (double)var10[0][0] * 0.5D;
      var26 = (double)var1.r() + 0.5D + (double)var10[0][2] * 0.5D;
      var28 = (double)var1.p() + 0.5D + (double)var10[1][0] * 0.5D;
      double var30 = (double)var1.r() + 0.5D + (double)var10[1][2] * 0.5D;
      var11 = var28 - var24;
      var13 = var30 - var26;
      double var32;
      double var34;
      if(var11 == 0.0D) {
         this.locX = (double)var1.p() + 0.5D;
         var22 = this.locZ - (double)var1.r();
      } else if(var13 == 0.0D) {
         this.locZ = (double)var1.r() + 0.5D;
         var22 = this.locX - (double)var1.p();
      } else {
         var32 = this.locX - var24;
         var34 = this.locZ - var26;
         var22 = (var32 * var11 + var34 * var13) * 2.0D;
      }

      this.locX = var24 + var11 * var22;
      this.locZ = var26 + var13 * var22;
      this.b(this.locX, this.locY, this.locZ);
      var32 = this.motX;
      var34 = this.motZ;
      if(this.aI()) {
         var32 *= 0.75D;
         var34 *= 0.75D;
      }

      double var36 = this.o();
      var32 = MathHelper.a(var32, -var36, var36);
      var34 = MathHelper.a(var34, -var36, var36);
      this.d(var32, 0.0D, var34);
      if(var10[0][1] != 0 && MathHelper.c(this.locX) - var1.p() == var10[0][0] && MathHelper.c(this.locZ) - var1.r() == var10[0][2]) {
         this.b(this.locX, this.locY + (double)var10[0][1], this.locZ);
      } else if(var10[1][1] != 0 && MathHelper.c(this.locX) - var1.p() == var10[1][0] && MathHelper.c(this.locZ) - var1.r() == var10[1][2]) {
         this.b(this.locX, this.locY + (double)var10[1][1], this.locZ);
      }

      this.r();
      Vec3D var38 = this.k(this.locX, this.locY, this.locZ);
      if(var38 != null && var3 != null) {
         double var39 = (var3.c - var38.c) * 0.05D;
         var19 = Math.sqrt(this.motX * this.motX + this.motZ * this.motZ);
         if(var19 > 0.0D) {
            this.motX = this.motX / var19 * (var19 + var39);
            this.motZ = this.motZ / var19 * (var19 + var39);
         }

         this.b(this.locX, var38.c, this.locZ);
      }

      int var45 = MathHelper.c(this.locX);
      int var40 = MathHelper.c(this.locZ);
      if(var45 != var1.p() || var40 != var1.r()) {
         var19 = Math.sqrt(this.motX * this.motX + this.motZ * this.motZ);
         this.motX = var19 * (double)(var45 - var1.p());
         this.motZ = var19 * (double)(var40 - var1.r());
      }

      if(var4) {
         double var41 = Math.sqrt(this.motX * this.motX + this.motZ * this.motZ);
         if(var41 > 0.01D) {
            double var43 = 0.06D;
            this.motX += this.motX / var41 * var43;
            this.motZ += this.motZ / var41 * var43;
         } else if(var9 == BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST) {
            if(this.world.getType(var1.e()).l()) {
               this.motX = 0.02D;
            } else if(this.world.getType(var1.f()).l()) {
               this.motX = -0.02D;
            }
         } else if(var9 == BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH) {
            if(this.world.getType(var1.c()).l()) {
               this.motZ = 0.02D;
            } else if(this.world.getType(var1.d()).l()) {
               this.motZ = -0.02D;
            }
         }
      }

   }

   protected void r() {
      if(this.aI()) {
         this.motX *= 0.996999979019165D;
         this.motY *= 0.0D;
         this.motZ *= 0.996999979019165D;
      } else {
         this.motX *= 0.9599999785423279D;
         this.motY *= 0.0D;
         this.motZ *= 0.9599999785423279D;
      }

   }

   public void b(double var1, double var3, double var5) {
      this.locX = var1;
      this.locY = var3;
      this.locZ = var5;
      float var7 = this.width / 2.0F;
      float var8 = this.length;
      this.a((AxisAlignedBB)(new AxisAlignedBB(var1 - (double)var7, var3, var5 - (double)var7, var1 + (double)var7, var3 + (double)var8, var5 + (double)var7)));
   }

   public Vec3D k(double var1, double var3, double var5) {
      int var7 = MathHelper.c(var1);
      int var8 = MathHelper.c(var3);
      int var9 = MathHelper.c(var5);
      if(BlockMinecartTrackAbstract.b(this.world, new BlockPosition(var7, var8 - 1, var9))) {
         --var8;
      }

      IBlockData var10 = this.world.getType(new BlockPosition(var7, var8, var9));
      if(BlockMinecartTrackAbstract.i(var10)) {
         BlockMinecartTrackAbstract.EnumTrackPosition var11 = (BlockMinecartTrackAbstract.EnumTrackPosition)var10.get(((BlockMinecartTrackAbstract)var10.getBlock()).g());
         int[][] var12 = h[var11.a()];
         double var13 = 0.0D;
         double var15 = (double)var7 + 0.5D + (double)var12[0][0] * 0.5D;
         double var17 = (double)var8 + 0.0625D + (double)var12[0][1] * 0.5D;
         double var19 = (double)var9 + 0.5D + (double)var12[0][2] * 0.5D;
         double var21 = (double)var7 + 0.5D + (double)var12[1][0] * 0.5D;
         double var23 = (double)var8 + 0.0625D + (double)var12[1][1] * 0.5D;
         double var25 = (double)var9 + 0.5D + (double)var12[1][2] * 0.5D;
         double var27 = var21 - var15;
         double var29 = (var23 - var17) * 2.0D;
         double var31 = var25 - var19;
         if(var27 == 0.0D) {
            var1 = (double)var7 + 0.5D;
            var13 = var5 - (double)var9;
         } else if(var31 == 0.0D) {
            var5 = (double)var9 + 0.5D;
            var13 = var1 - (double)var7;
         } else {
            double var33 = var1 - var15;
            double var35 = var5 - var19;
            var13 = (var33 * var27 + var35 * var31) * 2.0D;
         }

         var1 = var15 + var27 * var13;
         var3 = var17 + var29 * var13;
         var5 = var19 + var31 * var13;
         if(var29 < 0.0D) {
            ++var3;
         }

         if(var29 > 0.0D) {
            var3 += 0.5D;
         }

         return new Vec3D(var1, var3, var5);
      } else {
         return null;
      }
   }

   protected void a(NBTTagCompound var1) {
      if(var1.p("CustomDisplayTile")) {
         Block var2;
         if(var1.b("DisplayTile", 8)) {
            var2 = Block.b(var1.l("DisplayTile"));
         } else {
            var2 = Block.b(var1.h("DisplayTile"));
         }

         int var3 = var1.h("DisplayData");
         this.a(var2 == null?Blocks.AIR.u():var2.a(var3));
         this.k(var1.h("DisplayOffset"));
      }

   }

   protected void b(NBTTagCompound var1) {
      if(this.B()) {
         var1.a("CustomDisplayTile", true);
         IBlockData var2 = this.w();
         class_kk var3 = (class_kk)Block.h.b(var2.getBlock());
         var1.a("DisplayTile", var3 == null?"":var3.toString());
         var1.a("DisplayData", var2.getBlock().e(var2));
         var1.a("DisplayOffset", this.y());
      }

   }

   public void i(Entity var1) {
      if(!this.world.E) {
         if(!var1.noClip && !this.noClip) {
            if(!this.w(var1)) {
               if(var1 instanceof class_rz && this.v() == EntityMinecartAbstract.EnumMinecartType.RIDEABLE && this.motX * this.motX + this.motZ * this.motZ > 0.01D && !(var1 instanceof EntityHuman) && !(var1 instanceof class_wg) && !this.aI() && !var1.aH()) {
                  var1.m(this);
               }

               double var2 = var1.locX - this.locX;
               double var4 = var1.locZ - this.locZ;
               double var6 = var2 * var2 + var4 * var4;
               if(var6 >= 9.999999747378752E-5D) {
                  var6 = (double)MathHelper.a(var6);
                  var2 /= var6;
                  var4 /= var6;
                  double var8 = 1.0D / var6;
                  if(var8 > 1.0D) {
                     var8 = 1.0D;
                  }

                  var2 *= var8;
                  var4 *= var8;
                  var2 *= 0.10000000149011612D;
                  var4 *= 0.10000000149011612D;
                  var2 *= (double)(1.0F - this.R);
                  var4 *= (double)(1.0F - this.R);
                  var2 *= 0.5D;
                  var4 *= 0.5D;
                  if(var1 instanceof EntityMinecartAbstract) {
                     double var10 = var1.locX - this.locX;
                     double var12 = var1.locZ - this.locZ;
                     Vec3D var14 = (new Vec3D(var10, 0.0D, var12)).a();
                     Vec3D var15 = (new Vec3D((double)MathHelper.b(this.yaw * 0.017453292F), 0.0D, (double)MathHelper.a(this.yaw * 0.017453292F))).a();
                     double var16 = Math.abs(var14.b(var15));
                     if(var16 < 0.800000011920929D) {
                        return;
                     }

                     double var18 = var1.motX + this.motX;
                     double var20 = var1.motZ + this.motZ;
                     if(((EntityMinecartAbstract)var1).v() == EntityMinecartAbstract.EnumMinecartType.FURNACE && this.v() != EntityMinecartAbstract.EnumMinecartType.FURNACE) {
                        this.motX *= 0.20000000298023224D;
                        this.motZ *= 0.20000000298023224D;
                        this.g(var1.motX - var2, 0.0D, var1.motZ - var4);
                        var1.motX *= 0.949999988079071D;
                        var1.motZ *= 0.949999988079071D;
                     } else if(((EntityMinecartAbstract)var1).v() != EntityMinecartAbstract.EnumMinecartType.FURNACE && this.v() == EntityMinecartAbstract.EnumMinecartType.FURNACE) {
                        var1.motX *= 0.20000000298023224D;
                        var1.motZ *= 0.20000000298023224D;
                        var1.g(this.motX + var2, 0.0D, this.motZ + var4);
                        this.motX *= 0.949999988079071D;
                        this.motZ *= 0.949999988079071D;
                     } else {
                        var18 /= 2.0D;
                        var20 /= 2.0D;
                        this.motX *= 0.20000000298023224D;
                        this.motZ *= 0.20000000298023224D;
                        this.g(var18 - var2, 0.0D, var20 - var4);
                        var1.motX *= 0.20000000298023224D;
                        var1.motZ *= 0.20000000298023224D;
                        var1.g(var18 + var2, 0.0D, var20 + var4);
                     }
                  } else {
                     this.g(-var2, 0.0D, -var4);
                     var1.g(var2 / 4.0D, 0.0D, var4 / 4.0D);
                  }
               }

            }
         }
      }
   }

   public void a(float var1) {
      this.datawatcher.b(c, Float.valueOf(var1));
   }

   public float s() {
      return ((Float)this.datawatcher.a(c)).floatValue();
   }

   public void d(int var1) {
      this.datawatcher.b(a, Integer.valueOf(var1));
   }

   public int t() {
      return ((Integer)this.datawatcher.a(a)).intValue();
   }

   public void e(int var1) {
      this.datawatcher.b(b, Integer.valueOf(var1));
   }

   public int u() {
      return ((Integer)this.datawatcher.a(b)).intValue();
   }

   public abstract EntityMinecartAbstract.EnumMinecartType v();

   public IBlockData w() {
      return !this.B()?this.x():Block.c(((Integer)this.Q().a(d)).intValue());
   }

   public IBlockData x() {
      return Blocks.AIR.u();
   }

   public int y() {
      return !this.B()?this.A():((Integer)this.Q().a(e)).intValue();
   }

   public int A() {
      return 6;
   }

   public void a(IBlockData var1) {
      this.Q().b(d, Integer.valueOf(Block.j(var1)));
      this.a(true);
   }

   public void k(int var1) {
      this.Q().b(e, Integer.valueOf(var1));
      this.a(true);
   }

   public boolean B() {
      return ((Boolean)this.Q().a(f)).booleanValue();
   }

   public void a(boolean var1) {
      this.Q().b(f, Boolean.valueOf(var1));
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b = new int[BlockMinecartTrackAbstract.EnumTrackPosition.values().length];

      static {
         try {
            b[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST.ordinal()] = 1;
         } catch (NoSuchFieldError var10) {
            ;
         }

         try {
            b[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST.ordinal()] = 2;
         } catch (NoSuchFieldError var9) {
            ;
         }

         try {
            b[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH.ordinal()] = 3;
         } catch (NoSuchFieldError var8) {
            ;
         }

         try {
            b[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH.ordinal()] = 4;
         } catch (NoSuchFieldError var7) {
            ;
         }

         a = new int[EntityMinecartAbstract.EnumMinecartType.values().length];

         try {
            a[EntityMinecartAbstract.EnumMinecartType.CHEST.ordinal()] = 1;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[EntityMinecartAbstract.EnumMinecartType.FURNACE.ordinal()] = 2;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[EntityMinecartAbstract.EnumMinecartType.TNT.ordinal()] = 3;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EntityMinecartAbstract.EnumMinecartType.SPAWNER.ordinal()] = 4;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EntityMinecartAbstract.EnumMinecartType.HOPPER.ordinal()] = 5;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EntityMinecartAbstract.EnumMinecartType.COMMAND_BLOCK.ordinal()] = 6;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum EnumMinecartType {
      RIDEABLE(0, "MinecartRideable"),
      CHEST(1, "MinecartChest"),
      FURNACE(2, "MinecartFurnace"),
      TNT(3, "MinecartTNT"),
      SPAWNER(4, "MinecartSpawner"),
      HOPPER(5, "MinecartHopper"),
      COMMAND_BLOCK(6, "MinecartCommandBlock");

      private static final Map h = Maps.newHashMap();
      private final int i;
      private final String j;

      private EnumMinecartType(int var3, String var4) {
         this.i = var3;
         this.j = var4;
      }

      public int a() {
         return this.i;
      }

      public String b() {
         return this.j;
      }

      static {
         EntityMinecartAbstract.EnumMinecartType[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            EntityMinecartAbstract.EnumMinecartType var3 = var0[var2];
            h.put(Integer.valueOf(var3.a()), var3);
         }

      }
   }
}
