package net.minecraft.server;

import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aoa;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.class_axg;
import net.minecraft.server.BlockPosition;

public interface class_ara {
   Material getMaterial();

   boolean b();

   int c();

   int d();

   boolean f();

   MaterialMapColor g();

   IBlockData a(class_aod var1);

   IBlockData a(class_amq var1);

   boolean h();

   class_aoa i();

   boolean k();

   boolean l();

   boolean m();

   int a(class_ahw var1, BlockPosition var2, EnumDirection var3);

   boolean n();

   int a(World var1, BlockPosition var2);

   float b(World var1, BlockPosition var2);

   float a(EntityHuman var1, World var2, BlockPosition var3);

   int b(class_ahw var1, BlockPosition var2, EnumDirection var3);

   class_axg o();

   IBlockData b(class_ahw var1, BlockPosition var2);

   boolean p();

   AxisAlignedBB d(World var1, BlockPosition var2);

   void a(World var1, BlockPosition var2, AxisAlignedBB var3, List var4, Entity var5);

   AxisAlignedBB c(class_ahw var1, BlockPosition var2);

   MovingObjectPosition a(World var1, BlockPosition var2, Vec3D var3, Vec3D var4);

   boolean q();
}
