package net.minecraft.server;

import com.mojang.authlib.GameProfile;
import java.io.PrintStream;
import java.util.Random;
import java.util.UUID;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.Block;
import net.minecraft.server.BlockPumpkin;
import net.minecraft.server.Blocks;
import net.minecraft.server.Enchantment;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumColor;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.GameProfileSerializer;
import net.minecraft.server.Item;
import net.minecraft.server.ItemArmor;
import net.minecraft.server.Items;
import net.minecraft.server.MobEffectType;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.RedirectStream;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.TileEntityDispenser;
import net.minecraft.server.UtilColor;
import net.minecraft.server.World;
import net.minecraft.server.class_aaa;
import net.minecraft.server.class_aab;
import net.minecraft.server.class_aac;
import net.minecraft.server.EntityBoat;
import net.minecraft.server.class_aci;
import net.minecraft.server.class_act;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aet;
import net.minecraft.server.class_afd;
import net.minecraft.server.class_afe;
import net.minecraft.server.class_akt;
import net.minecraft.server.class_alk;
import net.minecraft.server.class_amn;
import net.minecraft.server.class_aoj;
import net.minecraft.server.class_apd;
import net.minecraft.server.class_aqn;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ck;
import net.minecraft.server.class_cn;
import net.minecraft.server.class_cr;
import net.minecraft.server.class_cz;
import net.minecraft.server.class_km;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_yd;
import net.minecraft.server.class_zl;
import net.minecraft.server.class_zp;
import net.minecraft.server.class_zr;
import net.minecraft.server.class_zu;
import net.minecraft.server.class_zv;
import net.minecraft.server.class_zw;
import net.minecraft.server.class_zy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Bootstrap {
   public static final PrintStream a = System.out;
   private static boolean b = false;
   private static final Logger c = LogManager.getLogger();

   public static boolean a() {
      return b;
   }

   static void b() {
      class_akt.c.a(Items.g, new class_km() {
         protected class_zr a(World var1, class_cz var2, ItemStack var3) {
            class_aac var4 = new class_aac(var1, var2.a(), var2.b(), var2.c());
            var4.c = class_zl.class_a_in_class_zl.ALLOWED;
            return var4;
         }
      });
      class_akt.c.a(Items.i, new class_km() {
         protected class_zr a(World var1, class_cz var2, ItemStack var3) {
            class_aac var4 = new class_aac(var1, var2.a(), var2.b(), var2.c());
            var4.a(var3);
            var4.c = class_zl.class_a_in_class_zl.ALLOWED;
            return var4;
         }
      });
      class_akt.c.a(Items.h, new class_km() {
         protected class_zr a(World var1, class_cz var2, ItemStack var3) {
            class_zw var4 = new class_zw(var1, var2.a(), var2.b(), var2.c());
            var4.c = class_zl.class_a_in_class_zl.ALLOWED;
            return var4;
         }
      });
      class_akt.c.a(Items.aW, new class_km() {
         protected class_zr a(World var1, class_cz var2, ItemStack var3) {
            return new class_zy(var1, var2.a(), var2.b(), var2.c());
         }
      });
      class_akt.c.a(Items.aF, new class_km() {
         protected class_zr a(World var1, class_cz var2, ItemStack var3) {
            return new class_zv(var1, var2.a(), var2.b(), var2.c());
         }
      });
      class_akt.c.a(Items.bU, new class_km() {
         protected class_zr a(World var1, class_cz var2, ItemStack var3) {
            return new class_aaa(var1, var2.a(), var2.b(), var2.c());
         }

         protected float a() {
            return super.a() * 0.5F;
         }

         protected float b() {
            return super.b() * 1.25F;
         }
      });
      class_akt.c.a(Items.bH, new class_cr() {
         public ItemStack a(class_ck var1, final ItemStack var2) {
            return (new class_km() {
               protected class_zr a(World var1, class_cz var2x, ItemStack var3) {
                  return new class_aab(var1, var2x.a(), var2x.b(), var2x.c(), var2.k());
               }

               protected float a() {
                  return super.a() * 0.5F;
               }

               protected float b() {
                  return super.b() * 1.25F;
               }
            }).a(var1, var2);
         }
      });
      class_akt.c.a(Items.bI, new class_cr() {
         public ItemStack a(class_ck var1, final ItemStack var2) {
            return (new class_km() {
               protected class_zr a(World var1, class_cz var2x, ItemStack var3) {
                  return new class_aab(var1, var2x.a(), var2x.b(), var2x.c(), var2.k());
               }

               protected float a() {
                  return super.a() * 0.5F;
               }

               protected float b() {
                  return super.b() * 1.25F;
               }
            }).a(var1, var2);
         }
      });
      class_akt.c.a(Items.bT, new class_cn() {
         public ItemStack b(class_ck var1, ItemStack var2) {
            EnumDirection var3 = class_akt.e(var1.f());
            double var4 = var1.a() + (double)var3.g();
            double var6 = (double)((float)var1.d().q() + 0.2F);
            double var8 = var1.c() + (double)var3.i();
            Entity var10 = class_aet.a(var1.i(), class_aet.h(var2), var4, var6, var8);
            if(var10 instanceof class_rz && var2.s()) {
               var10.c(var2.q());
            }

            class_aet.a((World)var1.i(), (EntityHuman)null, (ItemStack)var2, (Entity)var10);
            var2.a(1);
            return var2;
         }
      });
      class_akt.c.a(Items.cl, new class_cn() {
         public ItemStack b(class_ck var1, ItemStack var2) {
            EnumDirection var3 = class_akt.e(var1.f());
            double var4 = var1.a() + (double)var3.g();
            double var6 = (double)((float)var1.d().q() + 0.2F);
            double var8 = var1.c() + (double)var3.i();
            class_zp var10 = new class_zp(var1.i(), var4, var6, var8, var2);
            var1.i().a((Entity)var10);
            var2.a(1);
            return var2;
         }

         protected void a(class_ck var1) {
            var1.i().b(1004, var1.d(), 0);
         }
      });
      class_akt.c.a(Items.bV, new class_cn() {
         public ItemStack b(class_ck var1, ItemStack var2) {
            EnumDirection var3 = class_akt.e(var1.f());
            class_cz var4 = class_akt.a(var1);
            double var5 = var4.a() + (double)((float)var3.g() * 0.3F);
            double var7 = var4.b() + (double)((float)var3.h() * 0.3F);
            double var9 = var4.c() + (double)((float)var3.i() * 0.3F);
            World var11 = var1.i();
            Random var12 = var11.r;
            double var13 = var12.nextGaussian() * 0.05D + (double)var3.g();
            double var15 = var12.nextGaussian() * 0.05D + (double)var3.h();
            double var17 = var12.nextGaussian() * 0.05D + (double)var3.i();
            var11.a((Entity)(new class_zu(var11, var5, var7, var9, var13, var15, var17)));
            var2.a(1);
            return var2;
         }

         protected void a(class_ck var1) {
            var1.i().b(1018, var1.d(), 0);
         }
      });
      class_akt.c.a(Items.aG, new Bootstrap.class_a_in_class_kn(EntityBoat.EnumBoatVariant.OAK));
      class_akt.c.a(Items.aH, new Bootstrap.class_a_in_class_kn(EntityBoat.EnumBoatVariant.SPRUCE));
      class_akt.c.a(Items.aI, new Bootstrap.class_a_in_class_kn(EntityBoat.EnumBoatVariant.BIRCH));
      class_akt.c.a(Items.aJ, new Bootstrap.class_a_in_class_kn(EntityBoat.EnumBoatVariant.JUNGLE));
      class_akt.c.a(Items.aL, new Bootstrap.class_a_in_class_kn(EntityBoat.EnumBoatVariant.DARK_OAK));
      class_akt.c.a(Items.aK, new Bootstrap.class_a_in_class_kn(EntityBoat.EnumBoatVariant.ACACIA));
      class_cn var0 = new class_cn() {
         private final class_cn b = new class_cn();

         public ItemStack b(class_ck var1, ItemStack var2) {
            class_aci var3 = (class_aci)var2.b();
            BlockPosition var4 = var1.d().a(class_akt.e(var1.f()));
            if(var3.a((EntityHuman)null, (World)var1.i(), (BlockPosition)var4)) {
               var2.a(Items.ay);
               var2.b = 1;
               return var2;
            } else {
               return this.b.a(var1, var2);
            }
         }
      };
      class_akt.c.a(Items.aA, var0);
      class_akt.c.a(Items.az, var0);
      class_akt.c.a(Items.ay, new class_cn() {
         private final class_cn b = new class_cn();

         public ItemStack b(class_ck var1, ItemStack var2) {
            World var3 = var1.i();
            BlockPosition var4 = var1.d().a(class_akt.e(var1.f()));
            IBlockData var5 = var3.getType(var4);
            Block var6 = var5.getBlock();
            Material var7 = var5.getMaterial();
            Item var8;
            if(Material.h.equals(var7) && var6 instanceof class_amn && ((Integer)var5.get(class_amn.b)).intValue() == 0) {
               var8 = Items.az;
            } else {
               if(!Material.i.equals(var7) || !(var6 instanceof class_amn) || ((Integer)var5.get(class_amn.b)).intValue() != 0) {
                  return super.b(var1, var2);
               }

               var8 = Items.aA;
            }

            var3.g(var4);
            if(--var2.b == 0) {
               var2.a(var8);
               var2.b = 1;
            } else if(((TileEntityDispenser)var1.h()).a(new ItemStack(var8)) < 0) {
               this.b.a(var1, new ItemStack(var8));
            }

            return var2;
         }
      });
      class_akt.c.a(Items.d, new class_cn() {
         private boolean b = true;

         protected ItemStack b(class_ck var1, ItemStack var2) {
            World var3 = var1.i();
            BlockPosition var4 = var1.d().a(class_akt.e(var1.f()));
            if(var3.d(var4)) {
               var3.a(var4, Blocks.ab.u());
               if(var2.a(1, (Random)var3.r)) {
                  var2.b = 0;
               }
            } else if(var3.getType(var4).getBlock() == Blocks.W) {
               Blocks.W.d(var3, var4, Blocks.W.u().set(class_apd.a, Boolean.valueOf(true)));
               var3.g(var4);
            } else {
               this.b = false;
            }

            return var2;
         }

         protected void a(class_ck var1) {
            if(this.b) {
               var1.i().b(1000, var1.d(), 0);
            } else {
               var1.i().b(1001, var1.d(), 0);
            }

         }
      });
      class_akt.c.a(Items.bd, new class_cn() {
         private boolean b = true;

         protected ItemStack b(class_ck var1, ItemStack var2) {
            if(EnumColor.WHITE == EnumColor.a(var2.i())) {
               World var3 = var1.i();
               BlockPosition var4 = var1.d().a(class_akt.e(var1.f()));
               if(class_act.a(var2, var3, var4)) {
                  if(!var3.E) {
                     var3.b(2005, var4, 0);
                  }
               } else {
                  this.b = false;
               }

               return var2;
            } else {
               return super.b(var1, var2);
            }
         }

         protected void a(class_ck var1) {
            if(this.b) {
               var1.i().b(1000, var1.d(), 0);
            } else {
               var1.i().b(1001, var1.d(), 0);
            }

         }
      });
      class_akt.c.a(Item.a(Blocks.W), new class_cn() {
         protected ItemStack b(class_ck var1, ItemStack var2) {
            World var3 = var1.i();
            BlockPosition var4 = var1.d().a(class_akt.e(var1.f()));
            class_yd var5 = new class_yd(var3, (double)var4.p() + 0.5D, (double)var4.q(), (double)var4.r() + 0.5D, (class_rz)null);
            var3.a((Entity)var5);
            var3.a((EntityHuman)null, var5.locX, var5.locY, var5.locZ, class_ng.gd, EnumSoundCategory.BLOCKS, 1.0F, 1.0F);
            --var2.b;
            return var2;
         }
      });
      class_akt.c.a(Items.ch, new class_cn() {
         private boolean b = true;

         protected ItemStack b(class_ck var1, ItemStack var2) {
            World var3 = var1.i();
            EnumDirection var4 = class_akt.e(var1.f());
            BlockPosition var5 = var1.d().a(var4);
            class_aoj var6 = Blocks.ce;
            if(var3.d(var5) && var6.b(var3, var5, var2)) {
               if(!var3.E) {
                  var3.a((BlockPosition)var5, (IBlockData)var6.u().set(class_aoj.a, EnumDirection.UP), 3);
                  TileEntity var7 = var3.r(var5);
                  if(var7 instanceof class_aqn) {
                     if(var2.i() == 3) {
                        GameProfile var8 = null;
                        if(var2.n()) {
                           NBTTagCompound var9 = var2.o();
                           if(var9.b("SkullOwner", 10)) {
                              var8 = GameProfileSerializer.a(var9.o("SkullOwner"));
                           } else if(var9.b("SkullOwner", 8)) {
                              String var10 = var9.l("SkullOwner");
                              if(!UtilColor.b(var10)) {
                                 var8 = new GameProfile((UUID)null, var10);
                              }
                           }
                        }

                        ((class_aqn)var7).a(var8);
                     } else {
                        ((class_aqn)var7).a(var2.i());
                     }

                     ((class_aqn)var7).b(var4.d().b() * 4);
                     Blocks.ce.a(var3, var5, (class_aqn)var7);
                  }

                  --var2.b;
               }
            } else if(ItemArmor.a(var1, var2) == null) {
               this.b = false;
            }

            return var2;
         }

         protected void a(class_ck var1) {
            if(this.b) {
               var1.i().b(1000, var1.d(), 0);
            } else {
               var1.i().b(1001, var1.d(), 0);
            }

         }
      });
      class_akt.c.a(Item.a(Blocks.aU), new class_cn() {
         private boolean b = true;

         protected ItemStack b(class_ck var1, ItemStack var2) {
            World var3 = var1.i();
            BlockPosition var4 = var1.d().a(class_akt.e(var1.f()));
            BlockPumpkin var5 = (BlockPumpkin)Blocks.aU;
            if(var3.d(var4) && var5.b(var3, var4)) {
               if(!var3.E) {
                  var3.a((BlockPosition)var4, (IBlockData)var5.u(), 3);
               }

               --var2.b;
            } else {
               ItemStack var6 = ItemArmor.a(var1, var2);
               if(var6 == null) {
                  this.b = false;
               }
            }

            return var2;
         }

         protected void a(class_ck var1) {
            if(this.b) {
               var1.i().b(1000, var1.d(), 0);
            } else {
               var1.i().b(1001, var1.d(), 0);
            }

         }
      });
   }

   public static void c() {
      if(!b) {
         b = true;
         if(c.isDebugEnabled()) {
            d();
         }

         class_nf.b();
         Block.x();
         class_alk.e();
         MobEffectType.k();
         Enchantment.f();
         Item.t();
         class_afd.b();
         class_afe.a();
         StatisticList.a();
         BiomeBase.q();
         b();
      }
   }

   private static void d() {
      System.setErr(new RedirectStream("STDERR", System.err));
      System.setOut(new RedirectStream("STDOUT", a));
   }

   public static class class_a_in_class_kn extends class_cn {
      private final class_cn b = new class_cn();
      private final EntityBoat.EnumBoatVariant c;

      public class_a_in_class_kn(EntityBoat.EnumBoatVariant var1) {
         this.c = var1;
      }

      public ItemStack b(class_ck var1, ItemStack var2) {
         EnumDirection var3 = class_akt.e(var1.f());
         World var4 = var1.i();
         double var5 = var1.a() + (double)((float)var3.g() * 1.125F);
         double var7 = var1.b() + (double)((float)var3.h() * 1.125F);
         double var9 = var1.c() + (double)((float)var3.i() * 1.125F);
         BlockPosition var11 = var1.d().a(var3);
         Material var12 = var4.getType(var11).getMaterial();
         double var13;
         if(Material.h.equals(var12)) {
            var13 = 1.0D;
         } else {
            if(!Material.a.equals(var12) || !Material.h.equals(var4.getType(var11.b()).getMaterial())) {
               return this.b.a(var1, var2);
            }

            var13 = 0.0D;
         }

         EntityBoat var15 = new EntityBoat(var4, var5, var7 + var13, var9);
         var15.a(this.c);
         var15.yaw = var3.d().l();
         var4.a((Entity)var15);
         var2.a(1);
         return var2;
      }

      protected void a(class_ck var1) {
         var1.i().b(1000, var1.d(), 0);
      }
   }
}
