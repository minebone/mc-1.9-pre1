package net.minecraft.server;

import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.Blocks;
import net.minecraft.server.Enchantment;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agm;
import net.minecraft.server.class_agp;
import net.minecraft.server.class_amn;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;

public class class_ags extends Enchantment {
   public class_ags(Enchantment.class_a_in_class_agl var1, EnumInventorySlot... var2) {
      super(var1, class_agm.ARMOR_FEET, var2);
      this.c("frostWalker");
   }

   public int a(int var1) {
      return var1 * 10;
   }

   public int b(int var1) {
      return this.a(var1) + 15;
   }

   public boolean e() {
      return true;
   }

   public int b() {
      return 2;
   }

   public static void a(class_rz var0, World var1, BlockPosition var2, int var3) {
      if(var0.onGround) {
         float var4 = (float)Math.min(16, 2 + var3);
         BlockPosition.class_a_in_class_cj var5 = new BlockPosition.class_a_in_class_cj(0, 0, 0);
         Iterator var6 = BlockPosition.b(var2.a((double)(-var4), -1.0D, (double)(-var4)), var2.a((double)var4, -1.0D, (double)var4)).iterator();

         while(var6.hasNext()) {
            BlockPosition.class_a_in_class_cj var7 = (BlockPosition.class_a_in_class_cj)var6.next();
            if(var7.f(var0.locX, var0.locY, var0.locZ) <= (double)(var4 * var4)) {
               var5.c(var7.p(), var7.q() + 1, var7.r());
               IBlockData var8 = var1.getType(var5);
               if(var8.getBlock() == Blocks.AIR) {
                  IBlockData var9 = var1.getType(var7);
                  if(var9.getMaterial() == Material.h && ((Integer)var9.get(class_amn.b)).intValue() == 0 && var1.a(Blocks.de, var7, false, EnumDirection.DOWN, (Entity)null, (ItemStack)null)) {
                     var1.a((BlockPosition)var7, (IBlockData)Blocks.de.u());
                     var1.a(var7.h(), Blocks.de, MathHelper.a((Random)var0.bE(), 60, 120));
                  }
               }
            }
         }

      }
   }

   public boolean a(Enchantment var1) {
      return super.a(var1) && var1 != class_agp.i;
   }
}
