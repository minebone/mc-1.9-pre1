package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.World;
import net.minecraft.server.class_azs;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_qk;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_ys;
import net.minecraft.server.class_yx;

public class class_yg extends class_yx {
   public class_yg(World var1) {
      super(var1);
      this.a(0.7F, 0.5F);
   }

   protected void bz() {
      super.bz();
      this.a(class_ys.a).a(12.0D);
   }

   public boolean B(Entity var1) {
      if(super.B(var1)) {
         if(var1 instanceof class_rz) {
            byte var2 = 0;
            if(this.world.ae() == EnumDifficulty.NORMAL) {
               var2 = 7;
            } else if(this.world.ae() == EnumDifficulty.HARD) {
               var2 = 15;
            }

            if(var2 > 0) {
               ((class_rz)var1).c(new MobEffect(MobEffectList.s, var2 * 20, 0));
            }
         }

         return true;
      } else {
         return false;
      }
   }

   public class_sc a(class_qk var1, class_sc var2) {
      return var2;
   }

   public float bm() {
      return 0.45F;
   }

   protected class_kk J() {
      return class_azs.r;
   }
}
