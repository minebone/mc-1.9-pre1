package net.minecraft.server;

import java.io.IOException;
import net.minecraft.server.Entity;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketListenerPlayOut;

/*TODO: Packet changed*/
public class PacketPlayOutAttachEntity implements Packet {
   private int a;
   private int b;

   public PacketPlayOutAttachEntity() {
   }

   public PacketPlayOutAttachEntity(Entity var1, Entity var2) {
      this.a = var1.getId();
      this.b = var2 != null?var2.getId():-1;
   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.readInt();
      this.b = var1.readInt();
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.writeInt(this.a);
      var1.writeInt(this.b);
   }

   public void a(PacketListenerPlayOut var1) {
      var1.a(this);
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketListenerPlayOut)var1);
   }
}
