package net.minecraft.server;

import net.minecraft.server.BlockLeaves;
import net.minecraft.server.class_acb;
import net.minecraft.server.ItemStack;

public class class_adt extends class_acb {
   private final BlockLeaves b;

   public class_adt(BlockLeaves var1) {
      super(var1);
      this.b = var1;
      this.e(0);
      this.a(true);
   }

   public int a(int var1) {
      return var1 | 4;
   }

   public String f_(ItemStack var1) {
      return super.a() + "." + this.b.e(var1.i()).d();
   }
}
