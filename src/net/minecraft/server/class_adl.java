package net.minecraft.server;

import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityPainting;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_xq;
import net.minecraft.server.class_xr;

public class class_adl extends Item {
   private final Class a;

   public class_adl(Class var1) {
      this.a = var1;
      this.a(CreativeModeTab.c);
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      BlockPosition var10 = var4.a(var6);
      if(var6 != EnumDirection.DOWN && var6 != EnumDirection.UP && var2.a(var10, var6, var1)) {
         class_xq var11 = this.a(var3, var10, var6);
         if(var11 != null && var11.k()) {
            if(!var3.E) {
               var11.o();
               var3.a((Entity)var11);
            }

            --var1.b;
         }

         return EnumResult.SUCCESS;
      } else {
         return EnumResult.FAIL;
      }
   }

   private class_xq a(World var1, BlockPosition var2, EnumDirection var3) {
      return (class_xq)(this.a == EntityPainting.class?new EntityPainting(var1, var2, var3):(this.a == class_xr.class?new class_xr(var1, var2, var3):null));
   }
}
