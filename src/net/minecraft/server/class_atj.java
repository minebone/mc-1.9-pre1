package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.World;
import net.minecraft.server.class_ate;

public class class_atj {
   protected int e = 8;
   protected Random f = new Random();
   protected World g;

   public void a(World var1, int var2, int var3, class_ate var4) {
      int var5 = this.e;
      this.g = var1;
      this.f.setSeed(var1.O());
      long var6 = this.f.nextLong();
      long var8 = this.f.nextLong();

      for(int var10 = var2 - var5; var10 <= var2 + var5; ++var10) {
         for(int var11 = var3 - var5; var11 <= var3 + var5; ++var11) {
            long var12 = (long)var10 * var6;
            long var14 = (long)var11 * var8;
            this.f.setSeed(var12 ^ var14 ^ var1.O());
            this.a(var1, var10, var11, var2, var3, var4);
         }
      }

   }

   protected void a(World var1, int var2, int var3, int var4, int var5, class_ate var6) {
   }
}
