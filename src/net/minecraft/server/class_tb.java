package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockDoor;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.PathfinderGoalDoorInteract;
import net.minecraft.server.BlockPosition;

public class class_tb extends PathfinderGoalDoorInteract {
   private int g;
   private int h = -1;

   public class_tb(EntityInsentient var1) {
      super(var1);
   }

   public boolean a() {
      if(!super.a()) {
         return false;
      } else if(!this.a.world.U().b("mobGriefing")) {
         return false;
      } else {
         BlockDoor var10000 = this.c;
         return !BlockDoor.d(this.a.world, this.b);
      }
   }

   public void c() {
      super.c();
      this.g = 0;
   }

   public boolean b() {
      double var1 = this.a.c((BlockPosition)this.b);
      boolean var3;
      if(this.g <= 240) {
         BlockDoor var10000 = this.c;
         if(!BlockDoor.d(this.a.world, this.b) && var1 < 4.0D) {
            var3 = true;
            return var3;
         }
      }

      var3 = false;
      return var3;
   }

   public void d() {
      super.d();
      this.a.world.c(this.a.getId(), this.b, -1);
   }

   public void e() {
      super.e();
      if(this.a.bE().nextInt(20) == 0) {
         this.a.world.b(1019, this.b, 0);
      }

      ++this.g;
      int var1 = (int)((float)this.g / 240.0F * 10.0F);
      if(var1 != this.h) {
         this.a.world.c(this.a.getId(), this.b, var1);
         this.h = var1;
      }

      if(this.g == 240 && this.a.world.ae() == EnumDifficulty.HARD) {
         this.a.world.g(this.b);
         this.a.world.b(1021, this.b, 0);
         this.a.world.b(2001, this.b, Block.a((Block)this.c));
      }

   }
}
