package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.StructureGenerator;
import net.minecraft.server.StructureStart;
import net.minecraft.server.World;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_ate;
import net.minecraft.server.class_atn;
import net.minecraft.server.class_avq;
import net.minecraft.server.BlockPosition;

public class class_avp extends StructureGenerator {
   private int a = 20;
   private int b = 11;
   private final class_atn d;

   public class_avp(class_atn var1) {
      this.d = var1;
   }

   public String a() {
      return "EndCity";
   }

   protected boolean a(int var1, int var2) {
      int var3 = var1;
      int var4 = var2;
      if(var1 < 0) {
         var1 -= this.a - 1;
      }

      if(var2 < 0) {
         var2 -= this.a - 1;
      }

      int var5 = var1 / this.a;
      int var6 = var2 / this.a;
      Random var7 = this.g.a(var5, var6, 10387313);
      var5 *= this.a;
      var6 *= this.a;
      var5 += (var7.nextInt(this.a - this.b) + var7.nextInt(this.a - this.b)) / 2;
      var6 += (var7.nextInt(this.a - this.b) + var7.nextInt(this.a - this.b)) / 2;
      return var3 == var5 && var4 == var6 && this.d.c(var3, var4);
   }

   protected StructureStart b(int var1, int var2) {
      return new class_avp.class_a_in_class_avp(this.g, this.d, this.f, var1, var2);
   }

   public static class class_a_in_class_avp extends StructureStart {
      private boolean c;

      public class_a_in_class_avp() {
      }

      public class_a_in_class_avp(World var1, class_atn var2, Random var3, int var4, int var5) {
         super(var4, var5);
         this.a(var1, var2, var3, var4, var5);
      }

      private void a(World var1, class_atn var2, Random var3, int var4, int var5) {
         class_aod var6 = class_aod.values()[var3.nextInt(class_aod.values().length)];
         class_ate var7 = new class_ate();
         var2.a(var4, var5, var7);
         byte var8 = 5;
         byte var9 = 5;
         if(var6 == class_aod.CLOCKWISE_90) {
            var8 = -5;
         } else if(var6 == class_aod.CLOCKWISE_180) {
            var8 = -5;
            var9 = -5;
         } else if(var6 == class_aod.COUNTERCLOCKWISE_90) {
            var9 = -5;
         }

         int var10 = var7.a(7, 7);
         int var11 = var7.a(7, 7 + var9);
         int var12 = var7.a(7 + var8, 7);
         int var13 = var7.a(7 + var8, 7 + var9);
         int var14 = Math.min(Math.min(var10, var11), Math.min(var12, var13));
         if(var14 < 60) {
            this.c = false;
         } else {
            BlockPosition var15 = new BlockPosition(var4 * 16 + 8, var14, var5 * 16 + 8);
            class_avq.a(var15, var6, this.a, var3);
            this.d();
            this.c = true;
         }
      }

      public boolean a() {
         return this.c;
      }

      public void a(NBTTagCompound var1) {
         super.a(var1);
      }

      public void b(NBTTagCompound var1) {
         super.b(var1);
      }
   }
}
