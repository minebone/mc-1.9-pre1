package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.class_aqt;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.class_axg;
import net.minecraft.server.BlockPosition;

public class class_aqx {
   private final World a;
   private final BlockPosition b;
   private final BlockPosition c;
   private final EnumDirection d;
   private final List e = Lists.newArrayList();
   private final List f = Lists.newArrayList();

   public class_aqx(World var1, BlockPosition var2, EnumDirection var3, boolean var4) {
      this.a = var1;
      this.b = var2;
      if(var4) {
         this.d = var3;
         this.c = var2.a(var3);
      } else {
         this.d = var3.d();
         this.c = var2.a(var3, 2);
      }

   }

   public boolean a() {
      this.e.clear();
      this.f.clear();
      IBlockData var1 = this.a.getType(this.c);
      if(!class_aqt.a(var1, this.a, this.c, this.d, false)) {
         if(var1.o() != class_axg.DESTROY) {
            return false;
         } else {
            this.f.add(this.c);
            return true;
         }
      } else if(!this.a(this.c)) {
         return false;
      } else {
         for(int var2 = 0; var2 < this.e.size(); ++var2) {
            BlockPosition var3 = (BlockPosition)this.e.get(var2);
            if(this.a.getType(var3).getBlock() == Blocks.cE && !this.b(var3)) {
               return false;
            }
         }

         return true;
      }
   }

   private boolean a(BlockPosition var1) {
      IBlockData var2 = this.a.getType(var1);
      Block var3 = var2.getBlock();
      if(var2.getMaterial() == Material.a) {
         return true;
      } else if(!class_aqt.a(var2, this.a, var1, this.d, false)) {
         return true;
      } else if(var1.equals(this.b)) {
         return true;
      } else if(this.e.contains(var1)) {
         return true;
      } else {
         int var4 = 1;
         if(var4 + this.e.size() > 12) {
            return false;
         } else {
            while(var3 == Blocks.cE) {
               BlockPosition var5 = var1.a(this.d.d(), var4);
               var2 = this.a.getType(var5);
               var3 = var2.getBlock();
               if(var2.getMaterial() == Material.a || !class_aqt.a(var2, this.a, var5, this.d, false) || var5.equals(this.b)) {
                  break;
               }

               ++var4;
               if(var4 + this.e.size() > 12) {
                  return false;
               }
            }

            int var11 = 0;

            int var6;
            for(var6 = var4 - 1; var6 >= 0; --var6) {
               this.e.add(var1.a(this.d.d(), var6));
               ++var11;
            }

            var6 = 1;

            while(true) {
               BlockPosition var7 = var1.a(this.d, var6);
               int var8 = this.e.indexOf(var7);
               if(var8 > -1) {
                  this.a(var11, var8);

                  for(int var9 = 0; var9 <= var8 + var11; ++var9) {
                     BlockPosition var10 = (BlockPosition)this.e.get(var9);
                     if(this.a.getType(var10).getBlock() == Blocks.cE && !this.b(var10)) {
                        return false;
                     }
                  }

                  return true;
               }

               var2 = this.a.getType(var7);
               if(var2.getMaterial() == Material.a) {
                  return true;
               }

               if(!class_aqt.a(var2, this.a, var7, this.d, true) || var7.equals(this.b)) {
                  return false;
               }

               if(var2.o() == class_axg.DESTROY) {
                  this.f.add(var7);
                  return true;
               }

               if(this.e.size() >= 12) {
                  return false;
               }

               this.e.add(var7);
               ++var11;
               ++var6;
            }
         }
      }
   }

   private void a(int var1, int var2) {
      ArrayList var3 = Lists.newArrayList();
      ArrayList var4 = Lists.newArrayList();
      ArrayList var5 = Lists.newArrayList();
      var3.addAll(this.e.subList(0, var2));
      var4.addAll(this.e.subList(this.e.size() - var1, this.e.size()));
      var5.addAll(this.e.subList(var2, this.e.size() - var1));
      this.e.clear();
      this.e.addAll(var3);
      this.e.addAll(var4);
      this.e.addAll(var5);
   }

   private boolean b(BlockPosition var1) {
      EnumDirection[] var2 = EnumDirection.values();
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         EnumDirection var5 = var2[var4];
         if(var5.k() != this.d.k() && !this.a(var1.a(var5))) {
            return false;
         }
      }

      return true;
   }

   public List c() {
      return this.e;
   }

   public List d() {
      return this.f;
   }
}
