package net.minecraft.server;

import net.minecraft.server.BiomeBase;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityAnimal;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.PathfinderGoalTempt;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ail;
import net.minecraft.server.class_akb;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ayo;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_qk;
import net.minecraft.server.class_rn;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sv;
import net.minecraft.server.class_sx;
import net.minecraft.server.class_sz;
import net.minecraft.server.class_tc;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_to;
import net.minecraft.server.class_tr;
import net.minecraft.server.class_tu;
import net.minecraft.server.class_ub;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_uu;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_wi;
import net.minecraft.server.class_yp;
import net.minecraft.server.class_ys;

public class class_wc extends EntityAnimal {
   private static final class_ke bv = DataWatcher.a(class_wc.class, class_kg.b);
   private int bw = 0;
   private int bx = 0;
   private boolean bz = false;
   private int bA = 0;
   private int bB = 0;

   public class_wc(World var1) {
      super(var1);
      this.a(0.4F, 0.5F);
      this.g = new class_wc.class_d_in_class_wc(this);
      this.f = new class_wc.class_e_in_class_wc(this);
      this.c(0.0D);
   }

   protected void r() {
      this.bp.a(1, new class_tg(this));
      this.bp.a(1, new class_wc.class_f_in_class_wc(this, 2.2D));
      this.bp.a(2, new class_tc(this, 0.8D));
      this.bp.a(3, new PathfinderGoalTempt(this, 1.0D, Items.cb, false));
      this.bp.a(3, new PathfinderGoalTempt(this, 1.0D, Items.cg, false));
      this.bp.a(3, new PathfinderGoalTempt(this, 1.0D, Item.a((Block)Blocks.N), false));
      this.bp.a(4, new class_wc.class_b_in_class_wc(this, EntityHuman.class, 8.0F, 2.2D, 2.2D));
      this.bp.a(4, new class_wc.class_b_in_class_wc(this, class_wi.class, 10.0F, 2.2D, 2.2D));
      this.bp.a(4, new class_wc.class_b_in_class_wc(this, class_yp.class, 4.0F, 2.2D, 2.2D));
      this.bp.a(5, new class_wc.class_g_in_class_wc(this));
      this.bp.a(6, new class_uf(this, 0.6D));
      this.bp.a(11, new class_to(this, EntityHuman.class, 10.0F));
   }

   protected float cf() {
      if(!this.positionChanged && (!this.f.a() || this.f.e() <= this.locY + 0.5D)) {
         class_ayo var1 = this.h.k();
         if(var1 != null && var1.e() < var1.d()) {
            Vec3D var2 = var1.a((Entity)this);
            if(var2.c > this.locY) {
               return 0.5F;
            }
         }

         return this.f.b() <= 0.6D?0.2F:0.3F;
      } else {
         return 0.5F;
      }
   }

   protected void cg() {
      super.cg();
      double var1 = this.f.b();
      if(var1 > 0.0D) {
         double var3 = this.motX * this.motX + this.motZ * this.motZ;
         if(var3 < 0.010000000000000002D) {
            this.a(0.0F, 1.0F, 0.1F);
         }
      }

      if(!this.world.E) {
         this.world.a((Entity)this, (byte)1);
      }

   }

   public void c(double var1) {
      this.x().a(var1);
      this.f.a(this.f.d(), this.f.e(), this.f.f(), var1);
   }

   public void k(boolean var1) {
      super.k(var1);
      if(var1) {
         this.a(this.da(), this.cc(), ((this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F) * 0.8F);
      }

   }

   public void cZ() {
      this.k(true);
      this.bx = 10;
      this.bw = 0;
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)bv, (Object)Integer.valueOf(0));
   }

   public void M() {
      if(this.bA > 0) {
         --this.bA;
      }

      if(this.bB > 0) {
         this.bB -= this.random.nextInt(3);
         if(this.bB < 0) {
            this.bB = 0;
         }
      }

      if(this.onGround) {
         if(!this.bz) {
            this.k(false);
            this.dj();
         }

         if(this.db() == 99 && this.bA == 0) {
            class_rz var1 = this.A();
            if(var1 != null && this.h(var1) < 16.0D) {
               this.a(var1.locX, var1.locZ);
               this.f.a(var1.locX, var1.locY, var1.locZ, this.f.b());
               this.cZ();
               this.bz = true;
            }
         }

         class_wc.class_d_in_class_wc var4 = (class_wc.class_d_in_class_wc)this.g;
         if(!var4.c()) {
            if(this.f.a() && this.bA == 0) {
               class_ayo var2 = this.h.k();
               Vec3D var3 = new Vec3D(this.f.d(), this.f.e(), this.f.f());
               if(var2 != null && var2.e() < var2.d()) {
                  var3 = var2.a((Entity)this);
               }

               this.a(var3.b, var3.d);
               this.cZ();
            }
         } else if(!var4.d()) {
            this.dd();
         }
      }

      this.bz = this.onGround;
   }

   public void ak() {
   }

   private void a(double var1, double var3) {
      this.yaw = (float)(MathHelper.b(var3 - this.locZ, var1 - this.locX) * 57.2957763671875D) - 90.0F;
   }

   private void dd() {
      ((class_wc.class_d_in_class_wc)this.g).a(true);
   }

   private void dh() {
      ((class_wc.class_d_in_class_wc)this.g).a(false);
   }

   private void di() {
      if(this.f.b() < 2.2D) {
         this.bA = 10;
      } else {
         this.bA = 1;
      }

   }

   private void dj() {
      this.di();
      this.dh();
   }

   public void n() {
      super.n();
      if(this.bw != this.bx) {
         ++this.bw;
      } else if(this.bx != 0) {
         this.bw = 0;
         this.bx = 0;
         this.k(false);
      }

   }

   protected void bz() {
      super.bz();
      this.a((class_sk)class_ys.a).a(3.0D);
      this.a((class_sk)class_ys.d).a(0.30000001192092896D);
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("RabbitType", this.db());
      var1.a("MoreCarrotTicks", this.bB);
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.l(var1.h("RabbitType"));
      this.bB = var1.h("MoreCarrotTicks");
   }

   protected class_nf da() {
      return class_ng.en;
   }

   protected class_nf G() {
      return class_ng.ej;
   }

   protected class_nf bQ() {
      return class_ng.em;
   }

   protected class_nf bR() {
      return class_ng.el;
   }

   public boolean B(Entity var1) {
      if(this.db() == 99) {
         this.a(class_ng.ek, 1.0F, (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
         return var1.a(DamageSource.a((class_rz)this), 8.0F);
      } else {
         return var1.a(DamageSource.a((class_rz)this), 3.0F);
      }
   }

   public EnumSoundCategory by() {
      return this.db() == 99?EnumSoundCategory.HOSTILE:EnumSoundCategory.NEUTRAL;
   }

   public boolean a(DamageSource var1, float var2) {
      return this.b((DamageSource)var1)?false:super.a(var1, var2);
   }

   protected class_kk J() {
      return class_azs.A;
   }

   private boolean a(Item var1) {
      return var1 == Items.cb || var1 == Items.cg || var1 == Item.a((Block)Blocks.N);
   }

   public class_wc b(class_rn var1) {
      class_wc var2 = new class_wc(this.world);
      int var3 = this.dk();
      if(this.random.nextInt(20) != 0) {
         if(var1 instanceof class_wc && this.random.nextBoolean()) {
            var3 = ((class_wc)var1).db();
         } else {
            var3 = this.db();
         }
      }

      var2.l(var3);
      return var2;
   }

   public boolean e(ItemStack var1) {
      return var1 != null && this.a(var1.b());
   }

   public int db() {
      return ((Integer)this.datawatcher.a(bv)).intValue();
   }

   public void l(int var1) {
      if(var1 == 99) {
         this.a((class_sk)class_ys.g).a(8.0D);
         this.bp.a(4, new class_wc.class_a_in_class_wc(this));
         this.bq.a(1, new class_uu(this, false, new Class[0]));
         this.bq.a(2, new class_ux(this, EntityHuman.class, true));
         this.bq.a(2, new class_ux(this, class_wi.class, true));
         if(!this.o_()) {
            this.c(class_di.a("entity.KillerBunny.name"));
         }
      }

      this.datawatcher.b(bv, Integer.valueOf(var1));
   }

   public class_sc a(class_qk var1, class_sc var2) {
      Object var5 = super.a(var1, var2);
      int var3 = this.dk();
      boolean var4 = false;
      if(var5 instanceof class_wc.class_c_in_class_wc) {
         var3 = ((class_wc.class_c_in_class_wc)var5).a;
         var4 = true;
      } else {
         var5 = new class_wc.class_c_in_class_wc(var3);
      }

      this.l(var3);
      if(var4) {
         this.b_(-24000);
      }

      return (class_sc)var5;
   }

   private int dk() {
      BiomeBase var1 = this.world.b(new BlockPosition(this));
      int var2 = this.random.nextInt(100);
      return var1.p()?(var2 < 80?1:3):(var1 instanceof class_ail?4:(var2 < 50?0:(var2 < 90?5:2)));
   }

   private boolean dl() {
      return this.bB == 0;
   }

   protected void dc() {
      class_akb var1 = (class_akb)Blocks.cb;
      IBlockData var2 = var1.e(var1.g());
      this.world.a(EnumParticle.BLOCK_DUST, this.locX + (double)(this.random.nextFloat() * this.width * 2.0F) - (double)this.width, this.locY + 0.5D + (double)(this.random.nextFloat() * this.length), this.locZ + (double)(this.random.nextFloat() * this.width * 2.0F) - (double)this.width, 0.0D, 0.0D, 0.0D, new int[]{Block.j(var2)});
      this.bB = 40;
   }

   public void a(class_ke var1) {
      super.a((class_ke)var1);
   }

   // $FF: synthetic method
   public class_rn a(class_rn var1) {
      return this.b(var1);
   }

   static class class_a_in_class_wc extends class_tr {
      public class_a_in_class_wc(class_wc var1) {
         super(var1, 1.4D, true);
      }

      protected double a(class_rz var1) {
         return (double)(4.0F + var1.width);
      }
   }

   static class class_f_in_class_wc extends class_ub {
      private class_wc b;

      public class_f_in_class_wc(class_wc var1, double var2) {
         super(var1, var2);
         this.b = var1;
      }

      public void e() {
         super.e();
         this.b.c(this.a);
      }
   }

   static class class_g_in_class_wc extends class_tu {
      private final class_wc c;
      private boolean d;
      private boolean e = false;

      public class_g_in_class_wc(class_wc var1) {
         super(var1, 0.699999988079071D, 16);
         this.c = var1;
      }

      public boolean a() {
         if(this.a <= 0) {
            if(!this.c.world.U().b("mobGriefing")) {
               return false;
            }

            this.e = false;
            this.d = this.c.dl();
            this.d = true;
         }

         return super.a();
      }

      public boolean b() {
         return this.e && super.b();
      }

      public void c() {
         super.c();
      }

      public void d() {
         super.d();
      }

      public void e() {
         super.e();
         this.c.t().a((double)this.b.p() + 0.5D, (double)(this.b.q() + 1), (double)this.b.r() + 0.5D, 10.0F, (float)this.c.cD());
         if(this.f()) {
            World var1 = this.c.world;
            BlockPosition var2 = this.b.a();
            IBlockData var3 = var1.getType(var2);
            Block var4 = var3.getBlock();
            if(this.e && var4 instanceof class_akb) {
               Integer var5 = (Integer)var3.get(class_akb.c);
               if(var5.intValue() == 0) {
                  var1.a((BlockPosition)var2, (IBlockData)Blocks.AIR.u(), 2);
                  var1.b(var2, true);
               } else {
                  var1.a((BlockPosition)var2, (IBlockData)var3.set(class_akb.c, Integer.valueOf(var5.intValue() - 1)), 2);
                  var1.b(2001, var2, Block.j(var3));
               }

               this.c.dc();
            }

            this.e = false;
            this.a = 10;
         }

      }

      protected boolean a(World var1, BlockPosition var2) {
         Block var3 = var1.getType(var2).getBlock();
         if(var3 == Blocks.ak && this.d && !this.e) {
            var2 = var2.a();
            IBlockData var4 = var1.getType(var2);
            var3 = var4.getBlock();
            if(var3 instanceof class_akb && ((class_akb)var3).y(var4)) {
               this.e = true;
               return true;
            }
         }

         return false;
      }
   }

   static class class_b_in_class_wc extends class_sz {
      private class_wc c;

      public class_b_in_class_wc(class_wc var1, Class var2, float var3, double var4, double var6) {
         super(var1, var2, var3, var4, var6);
         this.c = var1;
      }

      public boolean a() {
         return this.c.db() != 99 && super.a();
      }
   }

   static class class_e_in_class_wc extends class_sx {
      private class_wc i;
      private double j;

      public class_e_in_class_wc(class_wc var1) {
         super(var1);
         this.i = var1;
      }

      public void c() {
         if(this.i.onGround && !this.i.bc && !((class_wc.class_d_in_class_wc)this.i.g).c()) {
            this.i.c(0.0D);
         } else if(this.a()) {
            this.i.c(this.j);
         }

         super.c();
      }

      public void a(double var1, double var3, double var5, double var7) {
         if(this.i.ah()) {
            var7 = 1.5D;
         }

         super.a(var1, var3, var5, var7);
         if(var7 > 0.0D) {
            this.j = var7;
         }

      }
   }

   public class class_d_in_class_wc extends class_sv {
      private class_wc c;
      private boolean d = false;

      public class_d_in_class_wc(class_wc var2) {
         super(var2);
         this.c = var2;
      }

      public boolean c() {
         return this.a;
      }

      public boolean d() {
         return this.d;
      }

      public void a(boolean var1) {
         this.d = var1;
      }

      public void b() {
         if(this.a) {
            this.c.cZ();
            this.a = false;
         }

      }
   }

   public static class class_c_in_class_wc implements class_sc {
      public int a;

      public class_c_in_class_wc(int var1) {
         this.a = var1;
      }
   }
}
