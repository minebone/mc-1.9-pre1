package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumSkyBlock;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agn;
import net.minecraft.server.class_agp;
import net.minecraft.server.class_ama;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.class_axg;
import net.minecraft.server.BlockPosition;

public class class_amh extends class_ama {
   public class_amh() {
      super(Material.w, false);
      this.z = 0.98F;
      this.a(true);
      this.a(CreativeModeTab.b);
   }

   public void a(World var1, EntityHuman var2, BlockPosition var3, IBlockData var4, TileEntity var5, ItemStack var6) {
      var2.b(StatisticList.a((Block)this));
      var2.a(0.025F);
      if(this.o() && class_agn.a(class_agp.r, var6) > 0) {
         ItemStack var9 = this.u(var4);
         if(var9 != null) {
            a(var1, var3, var9);
         }
      } else {
         if(var1.s.l()) {
            var1.g(var3);
            return;
         }

         int var7 = class_agn.a(class_agp.t, var6);
         this.b(var1, var3, var4, var7);
         Material var8 = var1.getType(var3.b()).getMaterial();
         if(var8.c() || var8.d()) {
            var1.a(var3, Blocks.i.u());
         }
      }

   }

   public int a(Random var1) {
      return 0;
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(var1.b(EnumSkyBlock.BLOCK, var2) > 11 - this.u().c()) {
         this.b(var1, var2);
      }

   }

   protected void b(World var1, BlockPosition var2) {
      if(var1.s.l()) {
         var1.g(var2);
      } else {
         this.b(var1, var2, var1.getType(var2), 0);
         var1.a(var2, Blocks.j.u());
         var1.e(var2, Blocks.j);
      }
   }

   public class_axg h(IBlockData var1) {
      return class_axg.NORMAL;
   }
}
