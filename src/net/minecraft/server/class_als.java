package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.class_ama;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;

public class class_als extends class_ama {
   public class_als(Material var1, boolean var2) {
      super(var1, var2);
      this.a(CreativeModeTab.b);
   }

   public int a(Random var1) {
      return 0;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   protected boolean o() {
      return true;
   }
}
