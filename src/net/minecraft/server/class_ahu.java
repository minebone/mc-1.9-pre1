package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_nf;
import net.minecraft.server.EnumSoundCategory;

public interface class_ahu {
   void a(World var1, BlockPosition var2, IBlockData var3, IBlockData var4, int var5);

   void a(BlockPosition var1);

   void a(int var1, int var2, int var3, int var4, int var5, int var6);

   void a(EntityHuman var1, class_nf var2, EnumSoundCategory var3, double var4, double var6, double var8, float var10, float var11);

   void a(class_nf var1, BlockPosition var2);

   void a(int var1, boolean var2, double var3, double var5, double var7, double var9, double var11, double var13, int... var15);

   void a(Entity var1);

   void b(Entity var1);

   void a(int var1, BlockPosition var2, int var3);

   void a(EntityHuman var1, int var2, BlockPosition var3, int var4);

   void b(int var1, BlockPosition var2, int var3);
}
