package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumChatFormat;
import net.minecraft.server.ExceptionInvalidSyntax;
import net.minecraft.server.GameProfileSerializer;
import net.minecraft.server.IScoreboardCriteria;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MojangsonParser;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.Scoreboard;
import net.minecraft.server.ScoreboardScore;
import net.minecraft.server.ScoreboardTeam;
import net.minecraft.server.ScoreboardTeamBase;
import net.minecraft.server.class_bbk;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ec;
import net.minecraft.server.ICommandListener;

public class CommandScoreboard extends CommandAbstract {
   public String c() {
      return "scoreboard";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.scoreboard.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(!this.b(var1, var2, var3)) {
         if(var3.length < 1) {
            throw new class_cf("commands.scoreboard.usage", new Object[0]);
         } else {
            if(var3[0].equalsIgnoreCase("objectives")) {
               if(var3.length == 1) {
                  throw new class_cf("commands.scoreboard.objectives.usage", new Object[0]);
               }

               if(var3[1].equalsIgnoreCase("list")) {
                  this.a(var2, var1);
               } else if(var3[1].equalsIgnoreCase("add")) {
                  if(var3.length < 4) {
                     throw new class_cf("commands.scoreboard.objectives.add.usage", new Object[0]);
                  }

                  this.a(var2, var3, 2, var1);
               } else if(var3[1].equalsIgnoreCase("remove")) {
                  if(var3.length != 3) {
                     throw new class_cf("commands.scoreboard.objectives.remove.usage", new Object[0]);
                  }

                  this.a(var2, var3[2], var1);
               } else {
                  if(!var3[1].equalsIgnoreCase("setdisplay")) {
                     throw new class_cf("commands.scoreboard.objectives.usage", new Object[0]);
                  }

                  if(var3.length != 3 && var3.length != 4) {
                     throw new class_cf("commands.scoreboard.objectives.setdisplay.usage", new Object[0]);
                  }

                  this.i(var2, var3, 2, var1);
               }
            } else if(var3[0].equalsIgnoreCase("players")) {
               if(var3.length == 1) {
                  throw new class_cf("commands.scoreboard.players.usage", new Object[0]);
               }

               if(var3[1].equalsIgnoreCase("list")) {
                  if(var3.length > 3) {
                     throw new class_cf("commands.scoreboard.players.list.usage", new Object[0]);
                  }

                  this.j(var2, var3, 2, var1);
               } else if(var3[1].equalsIgnoreCase("add")) {
                  if(var3.length < 5) {
                     throw new class_cf("commands.scoreboard.players.add.usage", new Object[0]);
                  }

                  this.k(var2, var3, 2, var1);
               } else if(var3[1].equalsIgnoreCase("remove")) {
                  if(var3.length < 5) {
                     throw new class_cf("commands.scoreboard.players.remove.usage", new Object[0]);
                  }

                  this.k(var2, var3, 2, var1);
               } else if(var3[1].equalsIgnoreCase("set")) {
                  if(var3.length < 5) {
                     throw new class_cf("commands.scoreboard.players.set.usage", new Object[0]);
                  }

                  this.k(var2, var3, 2, var1);
               } else if(var3[1].equalsIgnoreCase("reset")) {
                  if(var3.length != 3 && var3.length != 4) {
                     throw new class_cf("commands.scoreboard.players.reset.usage", new Object[0]);
                  }

                  this.l(var2, var3, 2, var1);
               } else if(var3[1].equalsIgnoreCase("enable")) {
                  if(var3.length != 4) {
                     throw new class_cf("commands.scoreboard.players.enable.usage", new Object[0]);
                  }

                  this.m(var2, var3, 2, var1);
               } else if(var3[1].equalsIgnoreCase("test")) {
                  if(var3.length != 5 && var3.length != 6) {
                     throw new class_cf("commands.scoreboard.players.test.usage", new Object[0]);
                  }

                  this.n(var2, var3, 2, var1);
               } else if(var3[1].equalsIgnoreCase("operation")) {
                  if(var3.length != 7) {
                     throw new class_cf("commands.scoreboard.players.operation.usage", new Object[0]);
                  }

                  this.o(var2, var3, 2, var1);
               } else {
                  if(!var3[1].equalsIgnoreCase("tag")) {
                     throw new class_cf("commands.scoreboard.players.usage", new Object[0]);
                  }

                  if(var3.length < 4) {
                     throw new class_cf("commands.scoreboard.players.tag.usage", new Object[0]);
                  }

                  this.a(var1, var2, var3, 2);
               }
            } else {
               if(!var3[0].equalsIgnoreCase("teams")) {
                  throw new class_cf("commands.scoreboard.usage", new Object[0]);
               }

               if(var3.length == 1) {
                  throw new class_cf("commands.scoreboard.teams.usage", new Object[0]);
               }

               if(var3[1].equalsIgnoreCase("list")) {
                  if(var3.length > 3) {
                     throw new class_cf("commands.scoreboard.teams.list.usage", new Object[0]);
                  }

                  this.e(var2, var3, 2, var1);
               } else if(var3[1].equalsIgnoreCase("add")) {
                  if(var3.length < 3) {
                     throw new class_cf("commands.scoreboard.teams.add.usage", new Object[0]);
                  }

                  this.b(var2, var3, 2, var1);
               } else if(var3[1].equalsIgnoreCase("remove")) {
                  if(var3.length != 3) {
                     throw new class_cf("commands.scoreboard.teams.remove.usage", new Object[0]);
                  }

                  this.d(var2, var3, 2, var1);
               } else if(var3[1].equalsIgnoreCase("empty")) {
                  if(var3.length != 3) {
                     throw new class_cf("commands.scoreboard.teams.empty.usage", new Object[0]);
                  }

                  this.h(var2, var3, 2, var1);
               } else if(!var3[1].equalsIgnoreCase("join")) {
                  if(var3[1].equalsIgnoreCase("leave")) {
                     if(var3.length < 3 && !(var2 instanceof EntityHuman)) {
                        throw new class_cf("commands.scoreboard.teams.leave.usage", new Object[0]);
                     }

                     this.g(var2, var3, 2, var1);
                  } else {
                     if(!var3[1].equalsIgnoreCase("option")) {
                        throw new class_cf("commands.scoreboard.teams.usage", new Object[0]);
                     }

                     if(var3.length != 4 && var3.length != 5) {
                        throw new class_cf("commands.scoreboard.teams.option.usage", new Object[0]);
                     }

                     this.c(var2, var3, 2, var1);
                  }
               } else {
                  if(var3.length < 4 && (var3.length != 3 || !(var2 instanceof EntityHuman))) {
                     throw new class_cf("commands.scoreboard.teams.join.usage", new Object[0]);
                  }

                  this.f(var2, var3, 2, var1);
               }
            }

         }
      }
   }

   private boolean b(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      int var4 = -1;

      for(int var5 = 0; var5 < var3.length; ++var5) {
         if(this.b(var3, var5) && "*".equals(var3[var5])) {
            if(var4 >= 0) {
               throw new class_bz("commands.scoreboard.noMultiWildcard", new Object[0]);
            }

            var4 = var5;
         }
      }

      if(var4 < 0) {
         return false;
      } else {
         ArrayList var13 = Lists.newArrayList((Iterable)this.a(var1).d());
         String var6 = var3[var4];
         ArrayList var7 = Lists.newArrayList();
         Iterator var8 = var13.iterator();

         while(var8.hasNext()) {
            String var9 = (String)var8.next();
            var3[var4] = var9;

            try {
               this.a(var1, var2, var3);
               var7.add(var9);
            } catch (class_bz var12) {
               ChatMessage var11 = new ChatMessage(var12.getMessage(), var12.a());
               var11.b().a(EnumChatFormat.RED);
               var2.a(var11);
            }
         }

         var3[var4] = var6;
         var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_ENTITIES, var7.size());
         if(var7.isEmpty()) {
            throw new class_cf("commands.scoreboard.allMatchesFailed", new Object[0]);
         } else {
            return true;
         }
      }
   }

   protected Scoreboard a(MinecraftServer var1) {
      return var1.a(0).ad();
   }

   protected class_bbk a(String var1, boolean var2, MinecraftServer var3) throws class_bz {
      Scoreboard var4 = this.a(var3);
      class_bbk var5 = var4.b(var1);
      if(var5 == null) {
         throw new class_bz("commands.scoreboard.objectiveNotFound", new Object[]{var1});
      } else if(var2 && var5.c().b()) {
         throw new class_bz("commands.scoreboard.objectiveReadOnly", new Object[]{var1});
      } else {
         return var5;
      }
   }

   protected ScoreboardTeam a(String var1, MinecraftServer var2) throws class_bz {
      Scoreboard var3 = this.a(var2);
      ScoreboardTeam var4 = var3.d(var1);
      if(var4 == null) {
         throw new class_bz("commands.scoreboard.teamNotFound", new Object[]{var1});
      } else {
         return var4;
      }
   }

   protected void a(ICommandListener var1, String[] var2, int var3, MinecraftServer var4) throws class_bz {
      String var5 = var2[var3++];
      String var6 = var2[var3++];
      Scoreboard var7 = this.a(var4);
      IScoreboardCriteria var8 = (IScoreboardCriteria)IScoreboardCriteria.a.get(var6);
      if(var8 == null) {
         throw new class_cf("commands.scoreboard.objectives.add.wrongType", new Object[]{var6});
      } else if(var7.b(var5) != null) {
         throw new class_bz("commands.scoreboard.objectives.add.alreadyExists", new Object[]{var5});
      } else if(var5.length() > 16) {
         throw new ExceptionInvalidSyntax("commands.scoreboard.objectives.add.tooLong", new Object[]{var5, Integer.valueOf(16)});
      } else if(var5.isEmpty()) {
         throw new class_cf("commands.scoreboard.objectives.add.usage", new Object[0]);
      } else {
         if(var2.length > var3) {
            String var9 = a(var1, var2, var3).c();
            if(var9.length() > 32) {
               throw new ExceptionInvalidSyntax("commands.scoreboard.objectives.add.displayTooLong", new Object[]{var9, Integer.valueOf(32)});
            }

            if(!var9.isEmpty()) {
               var7.a(var5, var8).a(var9);
            } else {
               var7.a(var5, var8);
            }
         } else {
            var7.a(var5, var8);
         }

         a(var1, this, "commands.scoreboard.objectives.add.success", new Object[]{var5});
      }
   }

   protected void b(ICommandListener var1, String[] var2, int var3, MinecraftServer var4) throws class_bz {
      String var5 = var2[var3++];
      Scoreboard var6 = this.a(var4);
      if(var6.d(var5) != null) {
         throw new class_bz("commands.scoreboard.teams.add.alreadyExists", new Object[]{var5});
      } else if(var5.length() > 16) {
         throw new ExceptionInvalidSyntax("commands.scoreboard.teams.add.tooLong", new Object[]{var5, Integer.valueOf(16)});
      } else if(var5.isEmpty()) {
         throw new class_cf("commands.scoreboard.teams.add.usage", new Object[0]);
      } else {
         if(var2.length > var3) {
            String var7 = a(var1, var2, var3).c();
            if(var7.length() > 32) {
               throw new ExceptionInvalidSyntax("commands.scoreboard.teams.add.displayTooLong", new Object[]{var7, Integer.valueOf(32)});
            }

            if(!var7.isEmpty()) {
               var6.e(var5).a(var7);
            } else {
               var6.e(var5);
            }
         } else {
            var6.e(var5);
         }

         a(var1, this, "commands.scoreboard.teams.add.success", new Object[]{var5});
      }
   }

   protected void c(ICommandListener var1, String[] var2, int var3, MinecraftServer var4) throws class_bz {
      ScoreboardTeam var5 = this.a(var2[var3++], var4);
      if(var5 != null) {
         String var6 = var2[var3++].toLowerCase();
         if(!var6.equalsIgnoreCase("color") && !var6.equalsIgnoreCase("friendlyfire") && !var6.equalsIgnoreCase("seeFriendlyInvisibles") && !var6.equalsIgnoreCase("nametagVisibility") && !var6.equalsIgnoreCase("deathMessageVisibility") && !var6.equalsIgnoreCase("collisionRule")) {
            throw new class_cf("commands.scoreboard.teams.option.usage", new Object[0]);
         } else if(var2.length == 4) {
            if(var6.equalsIgnoreCase("color")) {
               throw new class_cf("commands.scoreboard.teams.option.noValue", new Object[]{var6, a(EnumChatFormat.a(true, false))});
            } else if(!var6.equalsIgnoreCase("friendlyfire") && !var6.equalsIgnoreCase("seeFriendlyInvisibles")) {
               if(!var6.equalsIgnoreCase("nametagVisibility") && !var6.equalsIgnoreCase("deathMessageVisibility")) {
                  if(var6.equalsIgnoreCase("collisionRule")) {
                     throw new class_cf("commands.scoreboard.teams.option.noValue", new Object[]{var6, a(ScoreboardTeamBase.class_a_in_class_bbq.a())});
                  } else {
                     throw new class_cf("commands.scoreboard.teams.option.usage", new Object[0]);
                  }
               } else {
                  throw new class_cf("commands.scoreboard.teams.option.noValue", new Object[]{var6, a(ScoreboardTeamBase.EnumNameTagVisibility.a())});
               }
            } else {
               throw new class_cf("commands.scoreboard.teams.option.noValue", new Object[]{var6, a(Arrays.asList(new String[]{"true", "false"}))});
            }
         } else {
            String var7 = var2[var3];
            if(var6.equalsIgnoreCase("color")) {
               EnumChatFormat var10 = EnumChatFormat.b(var7);
               if(var10 == null || var10.c()) {
                  throw new class_cf("commands.scoreboard.teams.option.noValue", new Object[]{var6, a(EnumChatFormat.a(true, false))});
               }

               var5.a(var10);
               var5.b(var10.toString());
               var5.c(EnumChatFormat.RESET.toString());
            } else if(var6.equalsIgnoreCase("friendlyfire")) {
               if(!var7.equalsIgnoreCase("true") && !var7.equalsIgnoreCase("false")) {
                  throw new class_cf("commands.scoreboard.teams.option.noValue", new Object[]{var6, a(Arrays.asList(new String[]{"true", "false"}))});
               }

               var5.a(var7.equalsIgnoreCase("true"));
            } else if(var6.equalsIgnoreCase("seeFriendlyInvisibles")) {
               if(!var7.equalsIgnoreCase("true") && !var7.equalsIgnoreCase("false")) {
                  throw new class_cf("commands.scoreboard.teams.option.noValue", new Object[]{var6, a(Arrays.asList(new String[]{"true", "false"}))});
               }

               var5.b(var7.equalsIgnoreCase("true"));
            } else {
               ScoreboardTeamBase.EnumNameTagVisibility var8;
               if(var6.equalsIgnoreCase("nametagVisibility")) {
                  var8 = ScoreboardTeamBase.EnumNameTagVisibility.a(var7);
                  if(var8 == null) {
                     throw new class_cf("commands.scoreboard.teams.option.noValue", new Object[]{var6, a(ScoreboardTeamBase.EnumNameTagVisibility.a())});
                  }

                  var5.a(var8);
               } else if(var6.equalsIgnoreCase("deathMessageVisibility")) {
                  var8 = ScoreboardTeamBase.EnumNameTagVisibility.a(var7);
                  if(var8 == null) {
                     throw new class_cf("commands.scoreboard.teams.option.noValue", new Object[]{var6, a(ScoreboardTeamBase.EnumNameTagVisibility.a())});
                  }

                  var5.b(var8);
               } else if(var6.equalsIgnoreCase("collisionRule")) {
                  ScoreboardTeamBase.class_a_in_class_bbq var9 = ScoreboardTeamBase.class_a_in_class_bbq.a(var7);
                  if(var9 == null) {
                     throw new class_cf("commands.scoreboard.teams.option.noValue", new Object[]{var6, a(ScoreboardTeamBase.class_a_in_class_bbq.a())});
                  }

                  var5.a(var9);
               }
            }

            a(var1, this, "commands.scoreboard.teams.option.success", new Object[]{var6, var5.b(), var7});
         }
      }
   }

   protected void d(ICommandListener var1, String[] var2, int var3, MinecraftServer var4) throws class_bz {
      Scoreboard var5 = this.a(var4);
      ScoreboardTeam var6 = this.a(var2[var3], var4);
      if(var6 != null) {
         var5.d(var6);
         a(var1, this, "commands.scoreboard.teams.remove.success", new Object[]{var6.b()});
      }
   }

   protected void e(ICommandListener var1, String[] var2, int var3, MinecraftServer var4) throws class_bz {
      Scoreboard var5 = this.a(var4);
      if(var2.length > var3) {
         ScoreboardTeam var6 = this.a(var2[var3], var4);
         if(var6 == null) {
            return;
         }

         Collection var7 = var6.d();
         var1.a(CommandObjectiveExecutor.EnumCommandResult.QUERY_RESULT, var7.size());
         if(var7.isEmpty()) {
            throw new class_bz("commands.scoreboard.teams.list.player.empty", new Object[]{var6.b()});
         }

         ChatMessage var8 = new ChatMessage("commands.scoreboard.teams.list.player.count", new Object[]{Integer.valueOf(var7.size()), var6.b()});
         var8.b().a(EnumChatFormat.DARK_GREEN);
         var1.a(var8);
         var1.a(new ChatComponentText(a(var7.toArray())));
      } else {
         Collection var10 = var5.g();
         var1.a(CommandObjectiveExecutor.EnumCommandResult.QUERY_RESULT, var10.size());
         if(var10.isEmpty()) {
            throw new class_bz("commands.scoreboard.teams.list.empty", new Object[0]);
         }

         ChatMessage var11 = new ChatMessage("commands.scoreboard.teams.list.count", new Object[]{Integer.valueOf(var10.size())});
         var11.b().a(EnumChatFormat.DARK_GREEN);
         var1.a(var11);
         Iterator var12 = var10.iterator();

         while(var12.hasNext()) {
            ScoreboardTeam var9 = (ScoreboardTeam)var12.next();
            var1.a(new ChatMessage("commands.scoreboard.teams.list.entry", new Object[]{var9.b(), var9.c(), Integer.valueOf(var9.d().size())}));
         }
      }

   }

   protected void f(ICommandListener var1, String[] var2, int var3, MinecraftServer var4) throws class_bz {
      Scoreboard var5 = this.a(var4);
      String var6 = var2[var3++];
      HashSet var7 = Sets.newHashSet();
      HashSet var8 = Sets.newHashSet();
      String var9;
      if(var1 instanceof EntityHuman && var3 == var2.length) {
         var9 = a(var1).h_();
         if(var5.a(var9, var6)) {
            var7.add(var9);
         } else {
            var8.add(var9);
         }
      } else {
         label50:
         while(true) {
            while(true) {
               if(var3 >= var2.length) {
                  break label50;
               }

               var9 = var2[var3++];
               if(var9.startsWith("@")) {
                  List var14 = c(var4, var1, var9);
                  Iterator var11 = var14.iterator();

                  while(var11.hasNext()) {
                     Entity var12 = (Entity)var11.next();
                     String var13 = e(var4, var1, var12.getUniqueId().toString());
                     if(var5.a(var13, var6)) {
                        var7.add(var13);
                     } else {
                        var8.add(var13);
                     }
                  }
               } else {
                  String var10 = e(var4, var1, var9);
                  if(var5.a(var10, var6)) {
                     var7.add(var10);
                  } else {
                     var8.add(var10);
                  }
               }
            }
         }
      }

      if(!var7.isEmpty()) {
         var1.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_ENTITIES, var7.size());
         a(var1, this, "commands.scoreboard.teams.join.success", new Object[]{Integer.valueOf(var7.size()), var6, a(var7.toArray(new String[var7.size()]))});
      }

      if(!var8.isEmpty()) {
         throw new class_bz("commands.scoreboard.teams.join.failure", new Object[]{Integer.valueOf(var8.size()), var6, a(var8.toArray(new String[var8.size()]))});
      }
   }

   protected void g(ICommandListener var1, String[] var2, int var3, MinecraftServer var4) throws class_bz {
      Scoreboard var5 = this.a(var4);
      HashSet var6 = Sets.newHashSet();
      HashSet var7 = Sets.newHashSet();
      String var8;
      if(var1 instanceof EntityHuman && var3 == var2.length) {
         var8 = a(var1).h_();
         if(var5.f(var8)) {
            var6.add(var8);
         } else {
            var7.add(var8);
         }
      } else {
         label50:
         while(true) {
            while(true) {
               if(var3 >= var2.length) {
                  break label50;
               }

               var8 = var2[var3++];
               if(var8.startsWith("@")) {
                  List var13 = c(var4, var1, var8);
                  Iterator var10 = var13.iterator();

                  while(var10.hasNext()) {
                     Entity var11 = (Entity)var10.next();
                     String var12 = e(var4, var1, var11.getUniqueId().toString());
                     if(var5.f(var12)) {
                        var6.add(var12);
                     } else {
                        var7.add(var12);
                     }
                  }
               } else {
                  String var9 = e(var4, var1, var8);
                  if(var5.f(var9)) {
                     var6.add(var9);
                  } else {
                     var7.add(var9);
                  }
               }
            }
         }
      }

      if(!var6.isEmpty()) {
         var1.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_ENTITIES, var6.size());
         a(var1, this, "commands.scoreboard.teams.leave.success", new Object[]{Integer.valueOf(var6.size()), a(var6.toArray(new String[var6.size()]))});
      }

      if(!var7.isEmpty()) {
         throw new class_bz("commands.scoreboard.teams.leave.failure", new Object[]{Integer.valueOf(var7.size()), a(var7.toArray(new String[var7.size()]))});
      }
   }

   protected void h(ICommandListener var1, String[] var2, int var3, MinecraftServer var4) throws class_bz {
      Scoreboard var5 = this.a(var4);
      ScoreboardTeam var6 = this.a(var2[var3], var4);
      if(var6 != null) {
         ArrayList var7 = Lists.newArrayList((Iterable)var6.d());
         var1.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_ENTITIES, var7.size());
         if(var7.isEmpty()) {
            throw new class_bz("commands.scoreboard.teams.empty.alreadyEmpty", new Object[]{var6.b()});
         } else {
            Iterator var8 = var7.iterator();

            while(var8.hasNext()) {
               String var9 = (String)var8.next();
               var5.a(var9, var6);
            }

            a(var1, this, "commands.scoreboard.teams.empty.success", new Object[]{Integer.valueOf(var7.size()), var6.b()});
         }
      }
   }

   protected void a(ICommandListener var1, String var2, MinecraftServer var3) throws class_bz {
      Scoreboard var4 = this.a(var3);
      class_bbk var5 = this.a(var2, false, var3);
      var4.k(var5);
      a(var1, this, "commands.scoreboard.objectives.remove.success", new Object[]{var2});
   }

   protected void a(ICommandListener var1, MinecraftServer var2) throws class_bz {
      Scoreboard var3 = this.a(var2);
      Collection var4 = var3.c();
      if(var4.isEmpty()) {
         throw new class_bz("commands.scoreboard.objectives.list.empty", new Object[0]);
      } else {
         ChatMessage var5 = new ChatMessage("commands.scoreboard.objectives.list.count", new Object[]{Integer.valueOf(var4.size())});
         var5.b().a(EnumChatFormat.DARK_GREEN);
         var1.a(var5);
         Iterator var6 = var4.iterator();

         while(var6.hasNext()) {
            class_bbk var7 = (class_bbk)var6.next();
            var1.a(new ChatMessage("commands.scoreboard.objectives.list.entry", new Object[]{var7.b(), var7.d(), var7.c().a()}));
         }

      }
   }

   protected void i(ICommandListener var1, String[] var2, int var3, MinecraftServer var4) throws class_bz {
      Scoreboard var5 = this.a(var4);
      String var6 = var2[var3++];
      int var7 = Scoreboard.h(var6);
      class_bbk var8 = null;
      if(var2.length == 4) {
         var8 = this.a(var2[var3], false, var4);
      }

      if(var7 < 0) {
         throw new class_bz("commands.scoreboard.objectives.setdisplay.invalidSlot", new Object[]{var6});
      } else {
         var5.a(var7, var8);
         if(var8 != null) {
            a(var1, this, "commands.scoreboard.objectives.setdisplay.successSet", new Object[]{Scoreboard.b(var7), var8.b()});
         } else {
            a(var1, this, "commands.scoreboard.objectives.setdisplay.successCleared", new Object[]{Scoreboard.b(var7)});
         }

      }
   }

   protected void j(ICommandListener var1, String[] var2, int var3, MinecraftServer var4) throws class_bz {
      Scoreboard var5 = this.a(var4);
      if(var2.length > var3) {
         String var6 = e(var4, var1, var2[var3]);
         Map var7 = var5.c(var6);
         var1.a(CommandObjectiveExecutor.EnumCommandResult.QUERY_RESULT, var7.size());
         if(var7.isEmpty()) {
            throw new class_bz("commands.scoreboard.players.list.player.empty", new Object[]{var6});
         }

         ChatMessage var8 = new ChatMessage("commands.scoreboard.players.list.player.count", new Object[]{Integer.valueOf(var7.size()), var6});
         var8.b().a(EnumChatFormat.DARK_GREEN);
         var1.a(var8);
         Iterator var9 = var7.values().iterator();

         while(var9.hasNext()) {
            ScoreboardScore var10 = (ScoreboardScore)var9.next();
            var1.a(new ChatMessage("commands.scoreboard.players.list.player.entry", new Object[]{Integer.valueOf(var10.c()), var10.d().d(), var10.d().b()}));
         }
      } else {
         Collection var11 = var5.d();
         var1.a(CommandObjectiveExecutor.EnumCommandResult.QUERY_RESULT, var11.size());
         if(var11.isEmpty()) {
            throw new class_bz("commands.scoreboard.players.list.empty", new Object[0]);
         }

         ChatMessage var12 = new ChatMessage("commands.scoreboard.players.list.count", new Object[]{Integer.valueOf(var11.size())});
         var12.b().a(EnumChatFormat.DARK_GREEN);
         var1.a(var12);
         var1.a(new ChatComponentText(a(var11.toArray())));
      }

   }

   protected void k(ICommandListener var1, String[] var2, int var3, MinecraftServer var4) throws class_bz {
      String var5 = var2[var3 - 1];
      int var6 = var3;
      String var7 = e(var4, var1, var2[var3++]);
      if(var7.length() > 40) {
         throw new ExceptionInvalidSyntax("commands.scoreboard.players.name.tooLong", new Object[]{var7, Integer.valueOf(40)});
      } else {
         class_bbk var8 = this.a(var2[var3++], true, var4);
         int var9 = var5.equalsIgnoreCase("set")?a(var2[var3++]):a(var2[var3++], 0);
         if(var2.length > var3) {
            Entity var10 = b(var4, var1, var2[var6]);

            try {
               NBTTagCompound var11 = MojangsonParser.a(a(var2, var3));
               NBTTagCompound var12 = a(var10);
               if(!GameProfileSerializer.a(var11, var12, true)) {
                  throw new class_bz("commands.scoreboard.players.set.tagMismatch", new Object[]{var7});
               }
            } catch (class_ec var13) {
               throw new class_bz("commands.scoreboard.players.set.tagError", new Object[]{var13.getMessage()});
            }
         }

         Scoreboard var14 = this.a(var4);
         ScoreboardScore var15 = var14.c(var7, var8);
         if(var5.equalsIgnoreCase("set")) {
            var15.c(var9);
         } else if(var5.equalsIgnoreCase("add")) {
            var15.a(var9);
         } else {
            var15.b(var9);
         }

         a(var1, this, "commands.scoreboard.players.set.success", new Object[]{var8.b(), var7, Integer.valueOf(var15.c())});
      }
   }

   protected void l(ICommandListener var1, String[] var2, int var3, MinecraftServer var4) throws class_bz {
      Scoreboard var5 = this.a(var4);
      String var6 = e(var4, var1, var2[var3++]);
      if(var2.length > var3) {
         class_bbk var7 = this.a(var2[var3++], false, var4);
         var5.d(var6, var7);
         a(var1, this, "commands.scoreboard.players.resetscore.success", new Object[]{var7.b(), var6});
      } else {
         var5.d(var6, (class_bbk)null);
         a(var1, this, "commands.scoreboard.players.reset.success", new Object[]{var6});
      }

   }

   protected void m(ICommandListener var1, String[] var2, int var3, MinecraftServer var4) throws class_bz {
      Scoreboard var5 = this.a(var4);
      String var6 = d(var4, var1, var2[var3++]);
      if(var6.length() > 40) {
         throw new ExceptionInvalidSyntax("commands.scoreboard.players.name.tooLong", new Object[]{var6, Integer.valueOf(40)});
      } else {
         class_bbk var7 = this.a(var2[var3], false, var4);
         if(var7.c() != IScoreboardCriteria.c) {
            throw new class_bz("commands.scoreboard.players.enable.noTrigger", new Object[]{var7.b()});
         } else {
            ScoreboardScore var8 = var5.c(var6, var7);
            var8.a(false);
            a(var1, this, "commands.scoreboard.players.enable.success", new Object[]{var7.b(), var6});
         }
      }
   }

   protected void n(ICommandListener var1, String[] var2, int var3, MinecraftServer var4) throws class_bz {
      Scoreboard var5 = this.a(var4);
      String var6 = e(var4, var1, var2[var3++]);
      if(var6.length() > 40) {
         throw new ExceptionInvalidSyntax("commands.scoreboard.players.name.tooLong", new Object[]{var6, Integer.valueOf(40)});
      } else {
         class_bbk var7 = this.a(var2[var3++], false, var4);
         if(!var5.b(var6, var7)) {
            throw new class_bz("commands.scoreboard.players.test.notFound", new Object[]{var7.b(), var6});
         } else {
            int var8 = var2[var3].equals("*")?Integer.MIN_VALUE:a(var2[var3]);
            ++var3;
            int var9 = var3 < var2.length && !var2[var3].equals("*")?a(var2[var3], var8):Integer.MAX_VALUE;
            ScoreboardScore var10 = var5.c(var6, var7);
            if(var10.c() >= var8 && var10.c() <= var9) {
               a(var1, this, "commands.scoreboard.players.test.success", new Object[]{Integer.valueOf(var10.c()), Integer.valueOf(var8), Integer.valueOf(var9)});
            } else {
               throw new class_bz("commands.scoreboard.players.test.failed", new Object[]{Integer.valueOf(var10.c()), Integer.valueOf(var8), Integer.valueOf(var9)});
            }
         }
      }
   }

   protected void o(ICommandListener var1, String[] var2, int var3, MinecraftServer var4) throws class_bz {
      Scoreboard var5 = this.a(var4);
      String var6 = e(var4, var1, var2[var3++]);
      class_bbk var7 = this.a(var2[var3++], true, var4);
      String var8 = var2[var3++];
      String var9 = e(var4, var1, var2[var3++]);
      class_bbk var10 = this.a(var2[var3], false, var4);
      if(var6.length() > 40) {
         throw new ExceptionInvalidSyntax("commands.scoreboard.players.name.tooLong", new Object[]{var6, Integer.valueOf(40)});
      } else if(var9.length() > 40) {
         throw new ExceptionInvalidSyntax("commands.scoreboard.players.name.tooLong", new Object[]{var9, Integer.valueOf(40)});
      } else {
         ScoreboardScore var11 = var5.c(var6, var7);
         if(!var5.b(var9, var10)) {
            throw new class_bz("commands.scoreboard.players.operation.notFound", new Object[]{var10.b(), var9});
         } else {
            ScoreboardScore var12 = var5.c(var9, var10);
            if(var8.equals("+=")) {
               var11.c(var11.c() + var12.c());
            } else if(var8.equals("-=")) {
               var11.c(var11.c() - var12.c());
            } else if(var8.equals("*=")) {
               var11.c(var11.c() * var12.c());
            } else if(var8.equals("/=")) {
               if(var12.c() != 0) {
                  var11.c(var11.c() / var12.c());
               }
            } else if(var8.equals("%=")) {
               if(var12.c() != 0) {
                  var11.c(var11.c() % var12.c());
               }
            } else if(var8.equals("=")) {
               var11.c(var12.c());
            } else if(var8.equals("<")) {
               var11.c(Math.min(var11.c(), var12.c()));
            } else if(var8.equals(">")) {
               var11.c(Math.max(var11.c(), var12.c()));
            } else {
               if(!var8.equals("><")) {
                  throw new class_bz("commands.scoreboard.players.operation.invalidOperation", new Object[]{var8});
               }

               int var13 = var11.c();
               var11.c(var12.c());
               var12.c(var13);
            }

            a(var1, this, "commands.scoreboard.players.operation.success", new Object[0]);
         }
      }
   }

   protected void a(MinecraftServer var1, ICommandListener var2, String[] var3, int var4) throws class_bz {
      String var5 = e(var1, var2, var3[var4]);
      Entity var6 = b(var1, var2, var3[var4++]);
      String var7 = var3[var4++];
      Set var8 = var6.O();
      if("list".equals(var7)) {
         if(!var8.isEmpty()) {
            ChatMessage var13 = new ChatMessage("commands.scoreboard.players.tag.list", new Object[]{var5});
            var13.b().a(EnumChatFormat.DARK_GREEN);
            var2.a(var13);
            var2.a(new ChatComponentText(a(var8.toArray())));
         }

         var2.a(CommandObjectiveExecutor.EnumCommandResult.QUERY_RESULT, var8.size());
      } else if(var3.length < 5) {
         throw new class_cf("commands.scoreboard.players.tag.usage", new Object[0]);
      } else {
         String var9 = var3[var4++];
         if(var3.length > var4) {
            try {
               NBTTagCompound var10 = MojangsonParser.a(a(var3, var4));
               NBTTagCompound var11 = a(var6);
               if(!GameProfileSerializer.a(var10, var11, true)) {
                  throw new class_bz("commands.scoreboard.players.tag.tagMismatch", new Object[]{var5});
               }
            } catch (class_ec var12) {
               throw new class_bz("commands.scoreboard.players.tag.tagError", new Object[]{var12.getMessage()});
            }
         }

         if("add".equals(var7)) {
            if(!var6.a(var9)) {
               throw new class_bz("commands.scoreboard.players.tag.tooMany", new Object[]{Integer.valueOf(1024)});
            }

            a(var2, this, "commands.scoreboard.players.tag.success.add", new Object[]{var9});
         } else {
            if(!"remove".equals(var7)) {
               throw new class_cf("commands.scoreboard.players.tag.usage", new Object[0]);
            }

            if(!var6.b(var9)) {
               throw new class_bz("commands.scoreboard.players.tag.notFound", new Object[]{var9});
            }

            a(var2, this, "commands.scoreboard.players.tag.success.remove", new Object[]{var9});
         }

      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      if(var3.length == 1) {
         return a((String[])var3, (String[])(new String[]{"objectives", "players", "teams"}));
      } else {
         if(var3[0].equalsIgnoreCase("objectives")) {
            if(var3.length == 2) {
               return a((String[])var3, (String[])(new String[]{"list", "add", "remove", "setdisplay"}));
            }

            if(var3[1].equalsIgnoreCase("add")) {
               if(var3.length == 4) {
                  Set var5 = IScoreboardCriteria.a.keySet();
                  return a((String[])var3, (Collection)var5);
               }
            } else if(var3[1].equalsIgnoreCase("remove")) {
               if(var3.length == 3) {
                  return a((String[])var3, (Collection)this.a(false, var1));
               }
            } else if(var3[1].equalsIgnoreCase("setdisplay")) {
               if(var3.length == 3) {
                  return a((String[])var3, (String[])Scoreboard.h());
               }

               if(var3.length == 4) {
                  return a((String[])var3, (Collection)this.a(false, var1));
               }
            }
         } else if(var3[0].equalsIgnoreCase("players")) {
            if(var3.length == 2) {
               return a((String[])var3, (String[])(new String[]{"set", "add", "remove", "reset", "list", "enable", "test", "operation", "tag"}));
            }

            if(!var3[1].equalsIgnoreCase("set") && !var3[1].equalsIgnoreCase("add") && !var3[1].equalsIgnoreCase("remove") && !var3[1].equalsIgnoreCase("reset")) {
               if(var3[1].equalsIgnoreCase("enable")) {
                  if(var3.length == 3) {
                     return a((String[])var3, (String[])var1.J());
                  }

                  if(var3.length == 4) {
                     return a((String[])var3, (Collection)this.b(var1));
                  }
               } else if(!var3[1].equalsIgnoreCase("list") && !var3[1].equalsIgnoreCase("test")) {
                  if(var3[1].equalsIgnoreCase("operation")) {
                     if(var3.length == 3) {
                        return a((String[])var3, (Collection)this.a(var1).d());
                     }

                     if(var3.length == 4) {
                        return a((String[])var3, (Collection)this.a(true, var1));
                     }

                     if(var3.length == 5) {
                        return a((String[])var3, (String[])(new String[]{"+=", "-=", "*=", "/=", "%=", "=", "<", ">", "><"}));
                     }

                     if(var3.length == 6) {
                        return a((String[])var3, (String[])var1.J());
                     }

                     if(var3.length == 7) {
                        return a((String[])var3, (Collection)this.a(false, var1));
                     }
                  } else if(var3[1].equalsIgnoreCase("tag")) {
                     if(var3.length == 3) {
                        return a((String[])var3, (Collection)this.a(var1).d());
                     }

                     if(var3.length == 4) {
                        return a((String[])var3, (String[])(new String[]{"add", "remove", "list"}));
                     }
                  }
               } else {
                  if(var3.length == 3) {
                     return a((String[])var3, (Collection)this.a(var1).d());
                  }

                  if(var3.length == 4 && var3[1].equalsIgnoreCase("test")) {
                     return a((String[])var3, (Collection)this.a(false, var1));
                  }
               }
            } else {
               if(var3.length == 3) {
                  return a((String[])var3, (String[])var1.J());
               }

               if(var3.length == 4) {
                  return a((String[])var3, (Collection)this.a(true, var1));
               }
            }
         } else if(var3[0].equalsIgnoreCase("teams")) {
            if(var3.length == 2) {
               return a((String[])var3, (String[])(new String[]{"add", "remove", "join", "leave", "empty", "list", "option"}));
            }

            if(var3[1].equalsIgnoreCase("join")) {
               if(var3.length == 3) {
                  return a((String[])var3, (Collection)this.a(var1).f());
               }

               if(var3.length >= 4) {
                  return a((String[])var3, (String[])var1.J());
               }
            } else {
               if(var3[1].equalsIgnoreCase("leave")) {
                  return a((String[])var3, (String[])var1.J());
               }

               if(!var3[1].equalsIgnoreCase("empty") && !var3[1].equalsIgnoreCase("list") && !var3[1].equalsIgnoreCase("remove")) {
                  if(var3[1].equalsIgnoreCase("option")) {
                     if(var3.length == 3) {
                        return a((String[])var3, (Collection)this.a(var1).f());
                     }

                     if(var3.length == 4) {
                        return a((String[])var3, (String[])(new String[]{"color", "friendlyfire", "seeFriendlyInvisibles", "nametagVisibility", "deathMessageVisibility", "collisionRule"}));
                     }

                     if(var3.length == 5) {
                        if(var3[3].equalsIgnoreCase("color")) {
                           return a((String[])var3, (Collection)EnumChatFormat.a(true, false));
                        }

                        if(var3[3].equalsIgnoreCase("nametagVisibility") || var3[3].equalsIgnoreCase("deathMessageVisibility")) {
                           return a((String[])var3, (String[])ScoreboardTeamBase.EnumNameTagVisibility.a());
                        }

                        if(var3[3].equalsIgnoreCase("collisionRule")) {
                           return a((String[])var3, (String[])ScoreboardTeamBase.class_a_in_class_bbq.a());
                        }

                        if(var3[3].equalsIgnoreCase("friendlyfire") || var3[3].equalsIgnoreCase("seeFriendlyInvisibles")) {
                           return a((String[])var3, (String[])(new String[]{"true", "false"}));
                        }
                     }
                  }
               } else if(var3.length == 3) {
                  return a((String[])var3, (Collection)this.a(var1).f());
               }
            }
         }

         return Collections.emptyList();
      }
   }

   protected List a(boolean var1, MinecraftServer var2) {
      Collection var3 = this.a(var2).c();
      ArrayList var4 = Lists.newArrayList();
      Iterator var5 = var3.iterator();

      while(true) {
         class_bbk var6;
         do {
            if(!var5.hasNext()) {
               return var4;
            }

            var6 = (class_bbk)var5.next();
         } while(var1 && var6.c().b());

         var4.add(var6.b());
      }
   }

   protected List b(MinecraftServer var1) {
      Collection var2 = this.a(var1).c();
      ArrayList var3 = Lists.newArrayList();
      Iterator var4 = var2.iterator();

      while(var4.hasNext()) {
         class_bbk var5 = (class_bbk)var4.next();
         if(var5.c() == IScoreboardCriteria.c) {
            var3.add(var5.b());
         }
      }

      return var3;
   }

   public boolean b(String[] var1, int var2) {
      return !var1[0].equalsIgnoreCase("players")?(var1[0].equalsIgnoreCase("teams")?var2 == 2:false):(var1.length > 1 && var1[1].equalsIgnoreCase("operation")?var2 == 2 || var2 == 5:var2 == 2);
   }
}
