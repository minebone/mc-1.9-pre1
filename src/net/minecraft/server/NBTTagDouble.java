package net.minecraft.server;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import net.minecraft.server.NBTReadLimiter;
import net.minecraft.server.NBTTag;
import net.minecraft.server.MathHelper;

public class NBTTagDouble extends NBTTag.class_a_in_class_eb {
   private double b;

   NBTTagDouble() {
   }

   public NBTTagDouble(double var1) {
      this.b = var1;
   }

   void a(DataOutput var1) throws IOException {
      var1.writeDouble(this.b);
   }

   void a(DataInput var1, int var2, NBTReadLimiter var3) throws IOException {
      var3.a(128L);
      this.b = var1.readDouble();
   }

   public byte a() {
      return (byte)6;
   }

   public String toString() {
      return "" + this.b + "d";
   }

   public NBTTag b() {
      return new NBTTagDouble(this.b);
   }

   public boolean equals(Object var1) {
      if(super.equals(var1)) {
         NBTTagDouble var2 = (NBTTagDouble)var1;
         return this.b == var2.b;
      } else {
         return false;
      }
   }

   public int hashCode() {
      long var1 = Double.doubleToLongBits(this.b);
      return super.hashCode() ^ (int)(var1 ^ var1 >>> 32);
   }

   public long c() {
      return (long)Math.floor(this.b);
   }

   public int d() {
      return MathHelper.c(this.b);
   }

   public short e() {
      return (short)(MathHelper.c(this.b) & '\uffff');
   }

   public byte f() {
      return (byte)(MathHelper.c(this.b) & 255);
   }

   public double g() {
      return this.b;
   }

   public float h() {
      return (float)this.b;
   }
}
