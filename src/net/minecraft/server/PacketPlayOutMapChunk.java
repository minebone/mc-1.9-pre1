package net.minecraft.server;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import java.io.IOException;
import net.minecraft.server.Chunk;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.ChunkSection;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketListenerPlayOut;

/*TODO: Packet Changed*/
public class PacketPlayOutMapChunk implements Packet {
   private int chunkX;
   private int chunkZ;
   private int primaryBitMask;
   private byte[] data;
   private boolean groundUp;

   public PacketPlayOutMapChunk() {
   }

   public PacketPlayOutMapChunk(Chunk chunk, boolean groundUp, int var3) {
      this.chunkX = chunk.posX;
      this.chunkZ = chunk.posZ;
      this.groundUp = groundUp;
      boolean overworld = !chunk.q().s.isNotOverworld();
      this.data = new byte[a(chunk, groundUp, overworld, var3)];
      this.primaryBitMask = a(new PacketDataSerializer(this.prepareByteBuf()), chunk, groundUp, overworld, var3);
   }

   public void decode(PacketDataSerializer pacetserializer) throws IOException {
      this.chunkX = pacetserializer.readInt();
      this.chunkZ = pacetserializer.readInt();
      this.groundUp = pacetserializer.readBoolean();
      this.primaryBitMask = pacetserializer.readVarInt();
      int size = pacetserializer.readVarInt();
      if(size > 2097152) {
         throw new RuntimeException("Chunk Packet trying to allocate too much memory on read.");
      } else {
         this.data = new byte[size];
         pacetserializer.readBytes(this.data);
      }
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.writeInt(this.chunkX);
      var1.writeInt(this.chunkZ);
      var1.writeBoolean(this.groundUp);
      var1.writeVarInt(this.primaryBitMask);
      var1.writeVarInt(this.data.length);
      var1.writeBytes(this.data);
   }

   public void a(PacketListenerPlayOut var1) {
      var1.a(this);
   }

   private ByteBuf prepareByteBuf() {
      ByteBuf byteBuf = Unpooled.wrappedBuffer(this.data);
      byteBuf.writerIndex(0);
      return byteBuf;
   }

   public static int a(PacketDataSerializer packetDataSerlializer, Chunk chunk, boolean groundUp, boolean overworld, int var4) {
      int finalData = 0;
      ChunkSection[] chunkSections = chunk.getChunkSections();
      int count = 0;

      for(int i = chunkSections.length; count < i; ++count) {
         ChunkSection chunkSection = chunkSections[count];
         if(chunkSection != Chunk.a && (!groundUp || !chunkSection.isEmpty()) && (var4 & 1 << count) != 0) {
            finalData |= 1 << count;
            chunkSection.getBlockPalette().b(packetDataSerlializer);
            packetDataSerlializer.writeBytes(chunkSection.getEmittedLight().a());
            if(overworld) {
               packetDataSerlializer.writeBytes(chunkSection.getSkyLight().a());
            }
         }
      }

      if(groundUp) {
         packetDataSerlializer.writeBytes(chunk.l());
      }

      return finalData;
   }

   protected static int a(Chunk chunk, boolean var1, boolean var2, int var3) {
      int finalData = 0;
      ChunkSection[] chunkSections = chunk.getChunkSections();
      int count = 0;

      for(int i = chunkSections.length; count < i; ++count) {
         ChunkSection chunkSection = chunkSections[count];
         if(chunkSection != Chunk.a && (!var1 || !chunkSection.isEmpty()) && (var3 & 1 << count) != 0) {
            finalData += chunkSection.getBlockPalette().a();
            finalData += chunkSection.getEmittedLight().a().length;
            if(var2) {
               finalData += chunkSection.getSkyLight().a().length;
            }
         }
      }

      if(var1) {
         finalData += chunk.l().length;
      }

      return finalData;
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketListenerPlayOut)var1);
   }
}
