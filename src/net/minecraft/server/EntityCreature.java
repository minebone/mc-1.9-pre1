package net.minecraft.server;

import java.util.UUID;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EntityTameableAnimal;
import net.minecraft.server.World;
import net.minecraft.server.class_ayl;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_tv;
import net.minecraft.server.class_vd;

public abstract class EntityCreature extends EntityInsentient {
   public static final UUID bt = UUID.fromString("E199AD21-BA8A-4C53-8D13-6182D5C69D3A");
   public static final AttributeModifier bu = (new AttributeModifier(bt, "Fleeing speed bonus", 2.0D, 2)).a(false);
   private BlockPosition a = BlockPosition.a;
   private float b = -1.0F;
   private class_tj c = new class_tv(this, 1.0D);
   private boolean bv;
   private float bw = class_ayl.WATER.a();

   public EntityCreature(World var1) {
      super(var1);
   }

   public float a(BlockPosition var1) {
      return 0.0F;
   }

   public boolean cF() {
      return super.cF() && this.a(new BlockPosition(this.locX, this.bk().b, this.locZ)) >= 0.0F;
   }

   public boolean cT() {
      return !this.h.n();
   }

   public boolean cU() {
      return this.f(new BlockPosition(this));
   }

   public boolean f(BlockPosition var1) {
      return this.b == -1.0F?true:this.a.k(var1) < (double)(this.b * this.b);
   }

   public void a(BlockPosition var1, int var2) {
      this.a = var1;
      this.b = (float)var2;
   }

   public BlockPosition cV() {
      return this.a;
   }

   public float cW() {
      return this.b;
   }

   public void cX() {
      this.b = -1.0F;
   }

   public boolean cY() {
      return this.b != -1.0F;
   }

   protected void cO() {
      super.cO();
      if(this.cP() && this.cQ() != null && this.cQ().world == this.world) {
         Entity var1 = this.cQ();
         this.a(new BlockPosition((int)var1.locX, (int)var1.locY, (int)var1.locZ), 5);
         float var2 = this.g(var1);
         if(this instanceof EntityTameableAnimal && ((EntityTameableAnimal)this).db()) {
            if(var2 > 10.0F) {
               this.a(true, true);
            }

            return;
         }

         if(!this.bv) {
            this.bp.a(2, this.c);
            if(this.x() instanceof class_vd) {
               this.bw = this.a(class_ayl.WATER);
               this.a(class_ayl.WATER, 0.0F);
            }

            this.bv = true;
         }

         this.q(var2);
         if(var2 > 4.0F) {
            this.x().a(var1, 1.0D);
         }

         if(var2 > 6.0F) {
            double var3 = (var1.locX - this.locX) / (double)var2;
            double var5 = (var1.locY - this.locY) / (double)var2;
            double var7 = (var1.locZ - this.locZ) / (double)var2;
            this.motX += var3 * Math.abs(var3) * 0.4D;
            this.motY += var5 * Math.abs(var5) * 0.4D;
            this.motZ += var7 * Math.abs(var7) * 0.4D;
         }

         if(var2 > 10.0F) {
            this.a(true, true);
         }
      } else if(!this.cP() && this.bv) {
         this.bv = false;
         this.bp.a(this.c);
         if(this.x() instanceof class_vd) {
            this.a(class_ayl.WATER, this.bw);
         }

         this.cX();
      }

   }

   protected void q(float var1) {
   }
}
