package net.minecraft.server;

import com.google.common.base.Optional;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.GameProfileSerializer;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.class_asz;
import net.minecraft.server.class_ata;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_wt;

public class class_ws extends Entity {
   private static final class_ke b = DataWatcher.a(class_ws.class, class_kg.k);
   private static final class_ke c = DataWatcher.a(class_ws.class, class_kg.h);
   public int a;

   public class_ws(World var1) {
      super(var1);
      this.i = true;
      this.a(2.0F, 2.0F);
      this.a = this.random.nextInt(100000);
   }

   public class_ws(World var1, double var2, double var4, double var6) {
      this(var1);
      this.b(var2, var4, var6);
   }

   protected boolean ad() {
      return false;
   }

   protected void i() {
      this.Q().a((class_ke)b, (Object)Optional.absent());
      this.Q().a((class_ke)c, (Object)Boolean.valueOf(true));
   }

   public void m() {
      this.lastX = this.locX;
      this.lastY = this.locY;
      this.lastZ = this.locZ;
      ++this.a;
      if(!this.world.E) {
         BlockPosition var1 = new BlockPosition(this);
         if(this.world.s instanceof class_ata && this.world.getType(var1).getBlock() != Blocks.ab) {
            this.world.a(var1, Blocks.ab.u());
         }
      }

   }

   protected void b(NBTTagCompound var1) {
      if(this.j() != null) {
         var1.a((String)"BeamTarget", (NBTTag)GameProfileSerializer.a(this.j()));
      }

      var1.a("ShowBottom", this.k());
   }

   protected void a(NBTTagCompound var1) {
      if(var1.b("BeamTarget", 10)) {
         this.a(GameProfileSerializer.c(var1.o("BeamTarget")));
      }

      if(var1.b("ShowBottom", 1)) {
         this.a(var1.p("ShowBottom"));
      }

   }

   public boolean ao() {
      return true;
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b(var1)) {
         return false;
      } else if(var1.j() instanceof class_wt) {
         return false;
      } else {
         if(!this.dead && !this.world.E) {
            this.S();
            if(!this.world.E) {
               this.world.a((Entity)null, this.locX, this.locY, this.locZ, 6.0F, true);
               this.a(var1);
            }
         }

         return true;
      }
   }

   public void P() {
      this.a(DamageSource.l);
      super.P();
   }

   private void a(DamageSource var1) {
      if(this.world.s instanceof class_ata) {
         class_ata var2 = (class_ata)this.world.s;
         class_asz var3 = var2.s();
         if(var3 != null) {
            var3.a(this, var1);
         }
      }

   }

   public void a(BlockPosition var1) {
      this.Q().b(b, Optional.fromNullable(var1));
   }

   public BlockPosition j() {
      return (BlockPosition)((Optional)this.Q().a(b)).orNull();
   }

   public void a(boolean var1) {
      this.Q().b(c, Boolean.valueOf(var1));
   }

   public boolean k() {
      return ((Boolean)this.Q().a(c)).booleanValue();
   }
}
