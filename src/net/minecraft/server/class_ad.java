package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.Block;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.Entity;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_l;
import net.minecraft.server.ICommandListener;

public class class_ad extends CommandAbstract {
   public String c() {
      return "execute";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.execute.usage";
   }

   public void a(final MinecraftServer var1, final ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 5) {
         throw new class_cf("commands.execute.usage", new Object[0]);
      } else {
         final Entity var4 = a(var1, var2, var3[0], Entity.class);
         final double var5 = b(var4.locX, var3[1], false);
         final double var7 = b(var4.locY, var3[2], false);
         final double var9 = b(var4.locZ, var3[3], false);
         final BlockPosition var11 = new BlockPosition(var5, var7, var9);
         byte var12 = 4;
         if("detect".equals(var3[4]) && var3.length > 10) {
            World var13 = var4.e();
            double var14 = b(var5, var3[5], false);
            double var16 = b(var7, var3[6], false);
            double var18 = b(var9, var3[7], false);
            Block var20 = b(var2, var3[8]);
            int var21 = a(var3[9], -1, 15);
            BlockPosition var22 = new BlockPosition(var14, var16, var18);
            IBlockData var23 = var13.getType(var22);
            if(var23.getBlock() != var20 || var21 >= 0 && var23.getBlock().e(var23) != var21) {
               throw new class_bz("commands.execute.failed", new Object[]{"detect", var4.h_()});
            }

            var12 = 10;
         }

         String var25 = a(var3, var12);
         ICommandListener var15 = new ICommandListener() {
            public String h_() {
               return var4.h_();
            }

            public IChatBaseComponent i_() {
               return var4.i_();
            }

            public void a(IChatBaseComponent var1x) {
               var2.a(var1x);
            }

            public boolean a(int var1x, String var2x) {
               return var2.a(var1x, var2x);
            }

            public BlockPosition c() {
               return var11;
            }

            public Vec3D d() {
               return new Vec3D(var5, var7, var9);
            }

            public World e() {
               return var4.world;
            }

            public Entity f() {
               return var4;
            }

            public boolean z_() {
               return var1 == null || var1.d[0].U().b("commandBlockOutput");
            }

            public void a(CommandObjectiveExecutor.EnumCommandResult var1x, int var2x) {
               var4.a(var1x, var2x);
            }

            public MinecraftServer h() {
               return var4.h();
            }
         };
         class_l var26 = var1.N();

         try {
            int var17 = var26.a(var15, var25);
            if(var17 < 1) {
               throw new class_bz("commands.execute.allInvocationsFailed", new Object[]{var25});
            }
         } catch (Throwable var24) {
            throw new class_bz("commands.execute.failed", new Object[]{var25, var4.h_()});
         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, var1.J()):(var3.length > 1 && var3.length <= 4?a(var3, 1, var4):(var3.length > 5 && var3.length <= 8 && "detect".equals(var3[4])?a(var3, 5, var4):(var3.length == 9 && "detect".equals(var3[4])?a(var3, Block.h.c()):Collections.emptyList())));
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 0;
   }
}
