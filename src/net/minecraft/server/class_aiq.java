package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.class_ato;
import net.minecraft.server.class_auj;
import net.minecraft.server.class_auk;
import net.minecraft.server.class_avc;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_wc;

public class class_aiq extends BiomeBase {
   private boolean y;
   private class_auk z = new class_auk();
   private class_auj A = new class_auj(4);

   public class_aiq(boolean var1, BiomeBase.class_a_in_class_aif var2) {
      super(var2);
      this.y = var1;
      if(var1) {
         this.r = Blocks.aJ.u();
      }

      this.v.clear();
      this.v.add(new BiomeBase.BiomeMeta(class_wc.class, 4, 2, 3));
   }

   public void a(World var1, Random var2, BlockPosition var3) {
      if(this.y) {
         int var4;
         int var5;
         int var6;
         for(var4 = 0; var4 < 3; ++var4) {
            var5 = var2.nextInt(16) + 8;
            var6 = var2.nextInt(16) + 8;
            this.z.b(var1, var2, var1.l(var3.a(var5, 0, var6)));
         }

         for(var4 = 0; var4 < 2; ++var4) {
            var5 = var2.nextInt(16) + 8;
            var6 = var2.nextInt(16) + 8;
            this.A.b(var1, var2, var1.l(var3.a(var5, 0, var6)));
         }
      }

      super.a(var1, var2, var3);
   }

   public class_ato a(Random var1) {
      return new class_avc(false);
   }
}
