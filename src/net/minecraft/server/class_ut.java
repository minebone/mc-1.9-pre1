package net.minecraft.server;

import net.minecraft.server.Village;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_vb;
import net.minecraft.server.class_wg;
import net.minecraft.server.class_yh;

public class class_ut extends class_vb {
   class_wg a;
   class_rz b;

   public class_ut(class_wg var1) {
      super(var1, false, true);
      this.a = var1;
      this.a(1);
   }

   public boolean a() {
      Village var1 = this.a.o();
      if(var1 == null) {
         return false;
      } else {
         this.b = var1.b((class_rz)this.a);
         if(this.b instanceof class_yh) {
            return false;
         } else if(!this.a(this.b, false)) {
            if(this.e.bE().nextInt(20) == 0) {
               this.b = var1.c((class_rz)this.a);
               return this.a(this.b, false);
            } else {
               return false;
            }
         } else {
            return true;
         }
      }
   }

   public void c() {
      this.a.c(this.b);
      super.c();
   }
}
