package net.minecraft.server;

import com.google.common.base.Predicate;
import java.util.Collections;
import java.util.List;
import net.minecraft.server.EntityCreature;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sl;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_vb;
import net.minecraft.server.class_ys;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_uv extends class_tj {
   private static final Logger a = LogManager.getLogger();
   private EntityInsentient b;
   private final Predicate c;
   private final class_ux.class_a_in_class_ux d;
   private class_rz e;
   private Class f;

   public class_uv(EntityInsentient var1, Class var2) {
      this.b = var1;
      this.f = var2;
      if(var1 instanceof EntityCreature) {
         a.warn("Use NearestAttackableTargetGoal.class for PathfinerMob mobs!");
      }

      this.c = new Predicate() {
         public boolean a(class_rz var1) {
            double var2 = class_uv.this.f();
            if(var1.aJ()) {
               var2 *= 0.800000011920929D;
            }

            return var1.aM()?false:((double)var1.g(class_uv.this.b) > var2?false:class_vb.a(class_uv.this.b, var1, false, true));
         }

         // $FF: synthetic method
         public boolean apply(Object var1) {
            return this.a((class_rz)var1);
         }
      };
      this.d = new class_ux.class_a_in_class_ux(var1);
   }

   public boolean a() {
      double var1 = this.f();
      List var3 = this.b.world.a(this.f, this.b.bk().b(var1, 4.0D, var1), this.c);
      Collections.sort(var3, this.d);
      if(var3.isEmpty()) {
         return false;
      } else {
         this.e = (class_rz)var3.get(0);
         return true;
      }
   }

   public boolean b() {
      class_rz var1 = this.b.A();
      if(var1 == null) {
         return false;
      } else if(!var1.at()) {
         return false;
      } else {
         double var2 = this.f();
         return this.b.h(var1) > var2 * var2?false:!(var1 instanceof EntityPlayer) || !((EntityPlayer)var1).c.isCreative();
      }
   }

   public void c() {
      this.b.c(this.e);
      super.c();
   }

   public void d() {
      this.b.c((class_rz)null);
      super.c();
   }

   protected double f() {
      class_sl var1 = this.b.a((class_sk)class_ys.b);
      return var1 == null?16.0D:var1.e();
   }
}
