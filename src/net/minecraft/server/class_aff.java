package net.minecraft.server;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.MobEffect;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_afd;
import net.minecraft.server.class_afg;
import net.minecraft.server.class_kk;

public class class_aff {
   public static List a(ItemStack var0) {
      return a(var0.o());
   }

   public static List a(class_afd var0, Collection var1) {
      ArrayList var2 = Lists.newArrayList();
      var2.addAll(var0.a());
      var2.addAll(var1);
      return var2;
   }

   public static List a(NBTTagCompound var0) {
      ArrayList var1 = Lists.newArrayList();
      var1.addAll(c(var0).a());
      a((NBTTagCompound)var0, (List)var1);
      return var1;
   }

   public static List b(ItemStack var0) {
      return b(var0.o());
   }

   public static List b(NBTTagCompound var0) {
      ArrayList var1 = Lists.newArrayList();
      a((NBTTagCompound)var0, (List)var1);
      return var1;
   }

   public static void a(NBTTagCompound var0, List var1) {
      if(var0 != null && var0.b("CustomPotionEffects", 9)) {
         NBTTagList var2 = var0.c("CustomPotionEffects", 10);

         for(int var3 = 0; var3 < var2.c(); ++var3) {
            NBTTagCompound var4 = var2.b(var3);
            MobEffect var5 = MobEffect.b(var4);
            if(var5 != null) {
               var1.add(var5);
            }
         }
      }

   }

   public static int a(Collection var0) {
      int var1 = 3694022;
      if(var0.isEmpty()) {
         return 3694022;
      } else {
         float var2 = 0.0F;
         float var3 = 0.0F;
         float var4 = 0.0F;
         int var5 = 0;
         Iterator var6 = var0.iterator();

         while(var6.hasNext()) {
            MobEffect var7 = (MobEffect)var6.next();
            if(var7.e()) {
               int var8 = var7.a().g();
               int var9 = var7.c() + 1;
               var2 += (float)(var9 * (var8 >> 16 & 255)) / 255.0F;
               var3 += (float)(var9 * (var8 >> 8 & 255)) / 255.0F;
               var4 += (float)(var9 * (var8 >> 0 & 255)) / 255.0F;
               var5 += var9;
            }
         }

         if(var5 == 0) {
            return 0;
         } else {
            var2 = var2 / (float)var5 * 255.0F;
            var3 = var3 / (float)var5 * 255.0F;
            var4 = var4 / (float)var5 * 255.0F;
            return (int)var2 << 16 | (int)var3 << 8 | (int)var4;
         }
      }
   }

   public static class_afd c(ItemStack var0) {
      return c(var0.o());
   }

   public static class_afd c(NBTTagCompound var0) {
      return var0 == null?class_afg.b:class_afd.a(var0.l("Potion"));
   }

   public static ItemStack a(ItemStack var0, class_afd var1) {
      class_kk var2 = (class_kk)class_afd.a.b(var1);
      if(var2 != null) {
         NBTTagCompound var3 = var0.n()?var0.o():new NBTTagCompound();
         var3.a("Potion", var2.toString());
         var0.d(var3);
      }

      return var0;
   }

   public static ItemStack a(ItemStack var0, Collection var1) {
      if(var1.isEmpty()) {
         return var0;
      } else {
         NBTTagCompound var2 = (NBTTagCompound)Objects.firstNonNull(var0.o(), new NBTTagCompound());
         NBTTagList var3 = var2.c("CustomPotionEffects", 9);
         Iterator var4 = var1.iterator();

         while(var4.hasNext()) {
            MobEffect var5 = (MobEffect)var4.next();
            var3.a((NBTTag)var5.a(new NBTTagCompound()));
         }

         var2.a((String)"CustomPotionEffects", (NBTTag)var3);
         var0.d(var2);
         return var0;
      }
   }
}
