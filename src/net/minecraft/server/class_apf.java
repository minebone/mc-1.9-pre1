package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_amf;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.class_arn;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_or;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;

public class class_apf extends Block {
   public static final class_arn a = class_amf.D;
   public static final class_arm b = class_arm.a("open");
   public static final BlockStateEnum c = BlockStateEnum.a("half", class_apf.class_a_in_class_apf.class);
   protected static final AxisAlignedBB d = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.1875D, 1.0D, 1.0D);
   protected static final AxisAlignedBB e = new AxisAlignedBB(0.8125D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
   protected static final AxisAlignedBB f = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.1875D);
   protected static final AxisAlignedBB g = new AxisAlignedBB(0.0D, 0.0D, 0.8125D, 1.0D, 1.0D, 1.0D);
   protected static final AxisAlignedBB B = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.1875D, 1.0D);
   protected static final AxisAlignedBB C = new AxisAlignedBB(0.0D, 0.8125D, 0.0D, 1.0D, 1.0D, 1.0D);

   protected class_apf(Material var1) {
      super(var1);
      this.w(this.A.b().set(a, EnumDirection.NORTH).set(b, Boolean.valueOf(false)).set(c, class_apf.class_a_in_class_apf.BOTTOM));
      float var2 = 0.5F;
      float var3 = 1.0F;
      this.a(CreativeModeTab.d);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      AxisAlignedBB var4;
      if(((Boolean)var1.get(b)).booleanValue()) {
         switch(class_apf.SyntheticClass_1.a[((EnumDirection)var1.get(a)).ordinal()]) {
         case 1:
         default:
            var4 = g;
            break;
         case 2:
            var4 = f;
            break;
         case 3:
            var4 = e;
            break;
         case 4:
            var4 = d;
         }
      } else if(var1.get(c) == class_apf.class_a_in_class_apf.TOP) {
         var4 = C;
      } else {
         var4 = B;
      }

      return var4;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(class_ahw var1, BlockPosition var2) {
      return !((Boolean)var1.getType(var2).get(b)).booleanValue();
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(this.x == Material.f) {
         return true;
      } else {
         var3 = var3.a(b);
         var1.a((BlockPosition)var2, (IBlockData)var3, 2);
         this.a(var4, var1, var2, ((Boolean)var3.get(b)).booleanValue());
         return true;
      }
   }

   protected void a(EntityHuman var1, World var2, BlockPosition var3, boolean var4) {
      int var5;
      if(var4) {
         var5 = this.x == Material.f?1037:1007;
         var2.a(var1, var5, var3, 0);
      } else {
         var5 = this.x == Material.f?1036:1013;
         var2.a(var1, var5, var3, 0);
      }

   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!var1.E) {
         boolean var5 = var1.y(var2);
         if(var5 || var4.u().m()) {
            boolean var6 = ((Boolean)var3.get(b)).booleanValue();
            if(var6 != var5) {
               var1.a((BlockPosition)var2, (IBlockData)var3.set(b, Boolean.valueOf(var5)), 2);
               this.a((EntityHuman)null, var1, var2, var5);
            }
         }

      }
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      IBlockData var9 = this.u();
      if(var3.k().c()) {
         var9 = var9.set(a, var3).set(b, Boolean.valueOf(false));
         var9 = var9.set(c, var5 > 0.5F?class_apf.class_a_in_class_apf.TOP:class_apf.class_a_in_class_apf.BOTTOM);
      } else {
         var9 = var9.set(a, var8.bh().d()).set(b, Boolean.valueOf(false));
         var9 = var9.set(c, var3 == EnumDirection.UP?class_apf.class_a_in_class_apf.BOTTOM:class_apf.class_a_in_class_apf.TOP);
      }

      return var9;
   }

   public boolean b(World var1, BlockPosition var2, EnumDirection var3) {
      return true;
   }

   protected static EnumDirection e(int var0) {
      switch(var0 & 3) {
      case 0:
         return EnumDirection.NORTH;
      case 1:
         return EnumDirection.SOUTH;
      case 2:
         return EnumDirection.WEST;
      case 3:
      default:
         return EnumDirection.EAST;
      }
   }

   protected static int a(EnumDirection var0) {
      switch(class_apf.SyntheticClass_1.a[var0.ordinal()]) {
      case 1:
         return 0;
      case 2:
         return 1;
      case 3:
         return 2;
      case 4:
      default:
         return 3;
      }
   }

   public IBlockData a(int var1) {
      return this.u().set(a, e(var1)).set(b, Boolean.valueOf((var1 & 4) != 0)).set(c, (var1 & 8) == 0?class_apf.class_a_in_class_apf.BOTTOM:class_apf.class_a_in_class_apf.TOP);
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | a((EnumDirection)var1.get(a));
      if(((Boolean)var1.get(b)).booleanValue()) {
         var3 |= 4;
      }

      if(var1.get(c) == class_apf.class_a_in_class_apf.TOP) {
         var3 |= 8;
      }

      return var3;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(a, var2.a((EnumDirection)var1.get(a)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(a)));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b, c});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumDirection.values().length];

      static {
         try {
            a[EnumDirection.NORTH.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.SOUTH.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.EAST.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum class_a_in_class_apf implements class_or {
      TOP("top"),
      BOTTOM("bottom");

      private final String c;

      private class_a_in_class_apf(String var3) {
         this.c = var3;
      }

      public String toString() {
         return this.c;
      }

      public String m() {
         return this.c;
      }
   }
}
