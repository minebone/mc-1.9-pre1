package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockMonsterEggs;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumMonsterType;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.class_rc;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_tr;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_uu;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_yp;
import net.minecraft.server.class_ys;

public class class_yu extends class_yp {
   private class_yu.class_b_in_class_yu a;

   public class_yu(World var1) {
      super(var1);
      this.a(0.4F, 0.3F);
   }

   protected void r() {
      this.bp.a(1, new class_tg(this));
      this.bp.a(3, this.a = new class_yu.class_b_in_class_yu(this));
      this.bp.a(4, new class_tr(this, 1.0D, false));
      this.bp.a(5, new class_yu.class_a_in_class_yu(this));
      this.bq.a(1, new class_uu(this, true, new Class[0]));
      this.bq.a(2, new class_ux(this, EntityHuman.class, true));
   }

   public double aw() {
      return 0.2D;
   }

   public float bm() {
      return 0.1F;
   }

   protected void bz() {
      super.bz();
      this.a(class_ys.a).a(8.0D);
      this.a(class_ys.d).a(0.25D);
      this.a(class_ys.e).a(1.0D);
   }

   protected boolean ad() {
      return false;
   }

   protected class_nf G() {
      return class_ng.eY;
   }

   protected class_nf bQ() {
      return class_ng.fa;
   }

   protected class_nf bR() {
      return class_ng.eZ;
   }

   protected void a(BlockPosition var1, Block var2) {
      this.a(class_ng.fb, 0.15F, 1.0F);
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b(var1)) {
         return false;
      } else {
         if((var1 instanceof class_rc || var1 == DamageSource.m) && this.a != null) {
            this.a.f();
         }

         return super.a(var1, var2);
      }
   }

   protected class_kk J() {
      return class_azs.t;
   }

   public void m() {
      this.aM = this.yaw;
      super.m();
   }

   public float a(BlockPosition var1) {
      return this.world.getType(var1.b()).getBlock() == Blocks.b?10.0F:super.a(var1);
   }

   protected boolean s_() {
      return true;
   }

   public boolean cF() {
      if(super.cF()) {
         EntityHuman var1 = this.world.b(this, 5.0D);
         return var1 == null;
      } else {
         return false;
      }
   }

   public EnumMonsterType bZ() {
      return EnumMonsterType.ARTHROPOD;
   }

   static class class_a_in_class_yu extends class_uf {
      private final class_yu a;
      private EnumDirection b;
      private boolean c;

      public class_a_in_class_yu(class_yu var1) {
         super(var1, 1.0D, 10);
         this.a = var1;
         this.a(1);
      }

      public boolean a() {
         if(!this.a.world.U().b("mobGriefing")) {
            return false;
         } else if(this.a.A() != null) {
            return false;
         } else if(!this.a.x().n()) {
            return false;
         } else {
            Random var1 = this.a.bE();
            if(var1.nextInt(10) == 0) {
               this.b = EnumDirection.a(var1);
               BlockPosition var2 = (new BlockPosition(this.a.locX, this.a.locY + 0.5D, this.a.locZ)).a(this.b);
               IBlockData var3 = this.a.world.getType(var2);
               if(BlockMonsterEggs.i(var3)) {
                  this.c = true;
                  return true;
               }
            }

            this.c = false;
            return super.a();
         }
      }

      public boolean b() {
         return this.c?false:super.b();
      }

      public void c() {
         if(!this.c) {
            super.c();
         } else {
            World var1 = this.a.world;
            BlockPosition var2 = (new BlockPosition(this.a.locX, this.a.locY + 0.5D, this.a.locZ)).a(this.b);
            IBlockData var3 = var1.getType(var2);
            if(BlockMonsterEggs.i(var3)) {
               var1.a((BlockPosition)var2, (IBlockData)Blocks.be.u().set(BlockMonsterEggs.a, BlockMonsterEggs.EnumMonsterEggVarient.a(var3)), 3);
               this.a.E();
               this.a.S();
            }

         }
      }
   }

   static class class_b_in_class_yu extends class_tj {
      private class_yu a;
      private int b;

      public class_b_in_class_yu(class_yu var1) {
         this.a = var1;
      }

      public void f() {
         if(this.b == 0) {
            this.b = 20;
         }

      }

      public boolean a() {
         return this.b > 0;
      }

      public void e() {
         --this.b;
         if(this.b <= 0) {
            World var1 = this.a.world;
            Random var2 = this.a.bE();
            BlockPosition var3 = new BlockPosition(this.a);

            for(int var4 = 0; var4 <= 5 && var4 >= -5; var4 = var4 <= 0?1 - var4:0 - var4) {
               for(int var5 = 0; var5 <= 10 && var5 >= -10; var5 = var5 <= 0?1 - var5:0 - var5) {
                  for(int var6 = 0; var6 <= 10 && var6 >= -10; var6 = var6 <= 0?1 - var6:0 - var6) {
                     BlockPosition var7 = var3.a(var5, var4, var6);
                     IBlockData var8 = var1.getType(var7);
                     if(var8.getBlock() == Blocks.be) {
                        if(var1.U().b("mobGriefing")) {
                           var1.b(var7, true);
                        } else {
                           var1.a((BlockPosition)var7, (IBlockData)((BlockMonsterEggs.EnumMonsterEggVarient)var8.get(BlockMonsterEggs.a)).d(), 3);
                        }

                        if(var2.nextBoolean()) {
                           return;
                        }
                     }
                  }
               }
            }
         }

      }
   }
}
