package net.minecraft.server;

import com.google.common.base.Predicate;
import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.AchievementList;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.BlockStatePredicate;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.GameProfileSerializer;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.ShapeDetector;
import net.minecraft.server.ShapeDetectorBuilder;
import net.minecraft.server.Statistic;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_akr;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_aqn;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_are;
import net.minecraft.server.class_arm;
import net.minecraft.server.class_arn;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_xn;

public class class_aoj extends class_ajm {
   public static final class_arn a = class_akr.H;
   public static final class_arm b = class_arm.a("nodrop");
   private static final Predicate B = new Predicate() {
      public boolean a(class_are var1) {
         return var1.a() != null && var1.a().getBlock() == Blocks.ce && var1.b() instanceof class_aqn && ((class_aqn)var1.b()).d() == 1;
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((class_are)var1);
      }
   };
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.25D, 0.0D, 0.25D, 0.75D, 0.5D, 0.75D);
   protected static final AxisAlignedBB d = new AxisAlignedBB(0.25D, 0.25D, 0.5D, 0.75D, 0.75D, 1.0D);
   protected static final AxisAlignedBB e = new AxisAlignedBB(0.25D, 0.25D, 0.0D, 0.75D, 0.75D, 0.5D);
   protected static final AxisAlignedBB f = new AxisAlignedBB(0.5D, 0.25D, 0.25D, 1.0D, 0.75D, 0.75D);
   protected static final AxisAlignedBB g = new AxisAlignedBB(0.0D, 0.25D, 0.25D, 0.5D, 0.75D, 0.75D);
   private ShapeDetector C;
   private ShapeDetector D;

   protected class_aoj() {
      super(Material.q);
      this.w(this.A.b().set(a, EnumDirection.NORTH).set(b, Boolean.valueOf(false)));
   }

   public String c() {
      return class_di.a("tile.skull.skeleton.name");
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      switch(class_aoj.SyntheticClass_1.a[((EnumDirection)var1.get(a)).ordinal()]) {
      case 1:
      default:
         return c;
      case 2:
         return d;
      case 3:
         return e;
      case 4:
         return f;
      case 5:
         return g;
      }
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return this.u().set(a, var8.bh()).set(b, Boolean.valueOf(false));
   }

   public TileEntity a(World var1, int var2) {
      return new class_aqn();
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      int var4 = 0;
      TileEntity var5 = var1.r(var2);
      if(var5 instanceof class_aqn) {
         var4 = ((class_aqn)var5).d();
      }

      return new ItemStack(Items.ch, 1, var4);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, float var4, int var5) {
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4) {
      if(var4.abilities.d) {
         var3 = var3.set(b, Boolean.valueOf(true));
         var1.a((BlockPosition)var2, (IBlockData)var3, 4);
      }

      super.a(var1, var2, var3, var4);
   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      if(!var1.E) {
         if(!((Boolean)var3.get(b)).booleanValue()) {
            TileEntity var4 = var1.r(var2);
            if(var4 instanceof class_aqn) {
               class_aqn var5 = (class_aqn)var4;
               ItemStack var6 = this.a(var1, var2, var3);
               if(var5.d() == 3 && var5.b() != null) {
                  var6.d(new NBTTagCompound());
                  NBTTagCompound var7 = new NBTTagCompound();
                  GameProfileSerializer.a(var7, var5.b());
                  var6.o().a((String)"SkullOwner", (NBTTag)var7);
               }

               a((World)var1, (BlockPosition)var2, (ItemStack)var6);
            }
         }

         super.b(var1, var2, var3);
      }
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Items.ch;
   }

   public boolean b(World var1, BlockPosition var2, ItemStack var3) {
      return var3.i() == 1 && var2.q() >= 2 && var1.ae() != EnumDifficulty.PEACEFUL && !var1.E?this.e().a(var1, var2) != null:false;
   }

   public void a(World var1, BlockPosition var2, class_aqn var3) {
      if(var3.d() == 1 && var2.q() >= 2 && var1.ae() != EnumDifficulty.PEACEFUL && !var1.E) {
         ShapeDetector var4 = this.g();
         ShapeDetector.ShapeDetectorCollection var5 = var4.a(var1, var2);
         if(var5 != null) {
            int var6;
            for(var6 = 0; var6 < 3; ++var6) {
               class_are var7 = var5.a(var6, 0, 0);
               var1.a((BlockPosition)var7.d(), (IBlockData)var7.a().set(b, Boolean.valueOf(true)), 2);
            }

            for(var6 = 0; var6 < var4.c(); ++var6) {
               for(int var13 = 0; var13 < var4.b(); ++var13) {
                  class_are var8 = var5.a(var6, var13, 0);
                  var1.a((BlockPosition)var8.d(), (IBlockData)Blocks.AIR.u(), 2);
               }
            }

            BlockPosition var12 = var5.a(1, 0, 0).d();
            class_xn var14 = new class_xn(var1);
            BlockPosition var15 = var5.a(1, 2, 0).d();
            var14.b((double)var15.p() + 0.5D, (double)var15.q() + 0.55D, (double)var15.r() + 0.5D, var5.b().k() == EnumDirection.class_a_in_class_cq.X?0.0F:90.0F, 0.0F);
            var14.aM = var5.b().k() == EnumDirection.class_a_in_class_cq.X?0.0F:90.0F;
            var14.o();
            Iterator var9 = var1.a(EntityHuman.class, var14.bk().g(50.0D)).iterator();

            while(var9.hasNext()) {
               EntityHuman var10 = (EntityHuman)var9.next();
               var10.b((Statistic)AchievementList.I);
            }

            var1.a((Entity)var14);

            int var16;
            for(var16 = 0; var16 < 120; ++var16) {
               var1.a(EnumParticle.SNOWBALL, (double)var12.p() + var1.r.nextDouble(), (double)(var12.q() - 2) + var1.r.nextDouble() * 3.9D, (double)var12.r() + var1.r.nextDouble(), 0.0D, 0.0D, 0.0D, new int[0]);
            }

            for(var16 = 0; var16 < var4.c(); ++var16) {
               for(int var17 = 0; var17 < var4.b(); ++var17) {
                  class_are var11 = var5.a(var16, var17, 0);
                  var1.c(var11.d(), Blocks.AIR);
               }
            }

         }
      }
   }

   public IBlockData a(int var1) {
      return this.u().set(a, EnumDirection.a(var1 & 7)).set(b, Boolean.valueOf((var1 & 8) > 0));
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((EnumDirection)var1.get(a)).a();
      if(((Boolean)var1.get(b)).booleanValue()) {
         var3 |= 8;
      }

      return var3;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(a, var2.a((EnumDirection)var1.get(a)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(a)));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b});
   }

   protected ShapeDetector e() {
      if(this.C == null) {
         this.C = ShapeDetectorBuilder.a().a(new String[]{"   ", "###", "~#~"}).a('#', class_are.a((Predicate)BlockStatePredicate.a(Blocks.aW))).a('~', class_are.a((Predicate)BlockStatePredicate.a(Blocks.AIR))).b();
      }

      return this.C;
   }

   protected ShapeDetector g() {
      if(this.D == null) {
         this.D = ShapeDetectorBuilder.a().a(new String[]{"^^^", "###", "~#~"}).a('#', class_are.a((Predicate)BlockStatePredicate.a(Blocks.aW))).a('^', B).a('~', class_are.a((Predicate)BlockStatePredicate.a(Blocks.AIR))).b();
      }

      return this.D;
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumDirection.values().length];

      static {
         try {
            a[EnumDirection.UP.ordinal()] = 1;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[EnumDirection.NORTH.ordinal()] = 2;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.SOUTH.ordinal()] = 3;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 4;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.EAST.ordinal()] = 5;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
