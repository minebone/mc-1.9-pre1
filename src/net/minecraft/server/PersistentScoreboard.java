package net.minecraft.server;

import java.util.Collection;
import java.util.Iterator;
import net.minecraft.server.EnumChatFormat;
import net.minecraft.server.IScoreboardCriteria;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.NBTTagString;
import net.minecraft.server.Scoreboard;
import net.minecraft.server.ScoreboardScore;
import net.minecraft.server.ScoreboardTeam;
import net.minecraft.server.ScoreboardTeamBase;
import net.minecraft.server.class_ayw;
import net.minecraft.server.class_bbk;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PersistentScoreboard extends class_ayw {
   private static final Logger b = LogManager.getLogger();
   private Scoreboard c;
   private NBTTagCompound d;

   public PersistentScoreboard() {
      this("scoreboard");
   }

   public PersistentScoreboard(String var1) {
      super(var1);
   }

   public void a(Scoreboard var1) {
      this.c = var1;
      if(this.d != null) {
         this.a(this.d);
      }

   }

   public void a(NBTTagCompound var1) {
      if(this.c == null) {
         this.d = var1;
      } else {
         this.b(var1.c("Objectives", 10));
         this.c(var1.c("PlayerScores", 10));
         if(var1.b("DisplaySlots", 10)) {
            this.c(var1.o("DisplaySlots"));
         }

         if(var1.b("Teams", 9)) {
            this.a(var1.c("Teams", 10));
         }

      }
   }

   protected void a(NBTTagList var1) {
      for(int var2 = 0; var2 < var1.c(); ++var2) {
         NBTTagCompound var3 = var1.b(var2);
         String var4 = var3.l("Name");
         if(var4.length() > 16) {
            var4 = var4.substring(0, 16);
         }

         ScoreboardTeam var5 = this.c.e(var4);
         String var6 = var3.l("DisplayName");
         if(var6.length() > 32) {
            var6 = var6.substring(0, 32);
         }

         var5.a(var6);
         if(var3.b("TeamColor", 8)) {
            var5.a(EnumChatFormat.b(var3.l("TeamColor")));
         }

         var5.b(var3.l("Prefix"));
         var5.c(var3.l("Suffix"));
         if(var3.b("AllowFriendlyFire", 99)) {
            var5.a(var3.p("AllowFriendlyFire"));
         }

         if(var3.b("SeeFriendlyInvisibles", 99)) {
            var5.b(var3.p("SeeFriendlyInvisibles"));
         }

         ScoreboardTeamBase.EnumNameTagVisibility var7;
         if(var3.b("NameTagVisibility", 8)) {
            var7 = ScoreboardTeamBase.EnumNameTagVisibility.a(var3.l("NameTagVisibility"));
            if(var7 != null) {
               var5.a(var7);
            }
         }

         if(var3.b("DeathMessageVisibility", 8)) {
            var7 = ScoreboardTeamBase.EnumNameTagVisibility.a(var3.l("DeathMessageVisibility"));
            if(var7 != null) {
               var5.b(var7);
            }
         }

         if(var3.b("CollisionRule", 8)) {
            ScoreboardTeamBase.class_a_in_class_bbq var8 = ScoreboardTeamBase.class_a_in_class_bbq.a(var3.l("CollisionRule"));
            if(var8 != null) {
               var5.a(var8);
            }
         }

         this.a(var5, var3.c("Players", 8));
      }

   }

   protected void a(ScoreboardTeam var1, NBTTagList var2) {
      for(int var3 = 0; var3 < var2.c(); ++var3) {
         this.c.a(var2.g(var3), var1.b());
      }

   }

   protected void c(NBTTagCompound var1) {
      for(int var2 = 0; var2 < 19; ++var2) {
         if(var1.b("slot_" + var2, 8)) {
            String var3 = var1.l("slot_" + var2);
            class_bbk var4 = this.c.b(var3);
            this.c.a(var2, var4);
         }
      }

   }

   protected void b(NBTTagList var1) {
      for(int var2 = 0; var2 < var1.c(); ++var2) {
         NBTTagCompound var3 = var1.b(var2);
         IScoreboardCriteria var4 = (IScoreboardCriteria)IScoreboardCriteria.a.get(var3.l("CriteriaName"));
         if(var4 != null) {
            String var5 = var3.l("Name");
            if(var5.length() > 16) {
               var5 = var5.substring(0, 16);
            }

            class_bbk var6 = this.c.a(var5, var4);
            var6.a(var3.l("DisplayName"));
            var6.a(IScoreboardCriteria.EnumScoreboardHealthDisplay.a(var3.l("RenderType")));
         }
      }

   }

   protected void c(NBTTagList var1) {
      for(int var2 = 0; var2 < var1.c(); ++var2) {
         NBTTagCompound var3 = var1.b(var2);
         class_bbk var4 = this.c.b(var3.l("Objective"));
         String var5 = var3.l("Name");
         if(var5.length() > 40) {
            var5 = var5.substring(0, 40);
         }

         ScoreboardScore var6 = this.c.c(var5, var4);
         var6.c(var3.h("Score"));
         if(var3.e("Locked")) {
            var6.a(var3.p("Locked"));
         }
      }

   }

   public void b(NBTTagCompound var1) {
      if(this.c == null) {
         b.warn("Tried to save scoreboard without having a scoreboard...");
      } else {
         var1.a((String)"Objectives", (NBTTag)this.b());
         var1.a((String)"PlayerScores", (NBTTag)this.e());
         var1.a((String)"Teams", (NBTTag)this.a());
         this.d(var1);
      }
   }

   protected NBTTagList a() {
      NBTTagList var1 = new NBTTagList();
      Collection var2 = this.c.g();
      Iterator var3 = var2.iterator();

      while(var3.hasNext()) {
         ScoreboardTeam var4 = (ScoreboardTeam)var3.next();
         NBTTagCompound var5 = new NBTTagCompound();
         var5.a("Name", var4.b());
         var5.a("DisplayName", var4.c());
         if(var4.m().b() >= 0) {
            var5.a("TeamColor", var4.m().e());
         }

         var5.a("Prefix", var4.e());
         var5.a("Suffix", var4.f());
         var5.a("AllowFriendlyFire", var4.g());
         var5.a("SeeFriendlyInvisibles", var4.h());
         var5.a("NameTagVisibility", var4.i().e);
         var5.a("DeathMessageVisibility", var4.j().e);
         var5.a("CollisionRule", var4.k().e);
         NBTTagList var6 = new NBTTagList();
         Iterator var7 = var4.d().iterator();

         while(var7.hasNext()) {
            String var8 = (String)var7.next();
            var6.a((NBTTag)(new NBTTagString(var8)));
         }

         var5.a((String)"Players", (NBTTag)var6);
         var1.a((NBTTag)var5);
      }

      return var1;
   }

   protected void d(NBTTagCompound var1) {
      NBTTagCompound var2 = new NBTTagCompound();
      boolean var3 = false;

      for(int var4 = 0; var4 < 19; ++var4) {
         class_bbk var5 = this.c.a(var4);
         if(var5 != null) {
            var2.a("slot_" + var4, var5.b());
            var3 = true;
         }
      }

      if(var3) {
         var1.a((String)"DisplaySlots", (NBTTag)var2);
      }

   }

   protected NBTTagList b() {
      NBTTagList var1 = new NBTTagList();
      Collection var2 = this.c.c();
      Iterator var3 = var2.iterator();

      while(var3.hasNext()) {
         class_bbk var4 = (class_bbk)var3.next();
         if(var4.c() != null) {
            NBTTagCompound var5 = new NBTTagCompound();
            var5.a("Name", var4.b());
            var5.a("CriteriaName", var4.c().a());
            var5.a("DisplayName", var4.d());
            var5.a("RenderType", var4.e().a());
            var1.a((NBTTag)var5);
         }
      }

      return var1;
   }

   protected NBTTagList e() {
      NBTTagList var1 = new NBTTagList();
      Collection var2 = this.c.e();
      Iterator var3 = var2.iterator();

      while(var3.hasNext()) {
         ScoreboardScore var4 = (ScoreboardScore)var3.next();
         if(var4.d() != null) {
            NBTTagCompound var5 = new NBTTagCompound();
            var5.a("Name", var4.e());
            var5.a("Objective", var4.d().b());
            var5.a("Score", var4.c());
            var5.a("Locked", var4.g());
            var1.a((NBTTag)var5);
         }
      }

      return var1;
   }
}
