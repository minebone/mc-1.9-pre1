package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MojangsonParser;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ec;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.IInventory;

public class CommandFill extends CommandAbstract {
   public String c() {
      return "fill";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.fill.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 7) {
         throw new class_cf("commands.fill.usage", new Object[0]);
      } else {
         var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_BLOCKS, 0);
         BlockPosition var4 = a(var2, var3, 0, false);
         BlockPosition var5 = a(var2, var3, 3, false);
         Block var6 = CommandAbstract.b(var2, var3[6]);
         int var7 = 0;
         if(var3.length >= 8) {
            var7 = a(var3[7], 0, 15);
         }

         BlockPosition var8 = new BlockPosition(Math.min(var4.p(), var5.p()), Math.min(var4.q(), var5.q()), Math.min(var4.r(), var5.r()));
         BlockPosition var9 = new BlockPosition(Math.max(var4.p(), var5.p()), Math.max(var4.q(), var5.q()), Math.max(var4.r(), var5.r()));
         int var10 = (var9.p() - var8.p() + 1) * (var9.q() - var8.q() + 1) * (var9.r() - var8.r() + 1);
         if(var10 > '耀') {
            throw new class_bz("commands.fill.tooManyBlocks", new Object[]{Integer.valueOf(var10), Integer.valueOf('耀')});
         } else if(var8.q() >= 0 && var9.q() < 256) {
            World var11 = var2.e();

            for(int var12 = var8.r(); var12 <= var9.r(); var12 += 16) {
               for(int var13 = var8.p(); var13 <= var9.p(); var13 += 16) {
                  if(!var11.e(new BlockPosition(var13, var9.q() - var8.q(), var12))) {
                     throw new class_bz("commands.fill.outOfWorld", new Object[0]);
                  }
               }
            }

            NBTTagCompound var23 = new NBTTagCompound();
            boolean var24 = false;
            if(var3.length >= 10 && var6.m()) {
               String var14 = a(var2, var3, 9).c();

               try {
                  var23 = MojangsonParser.a(var14);
                  var24 = true;
               } catch (class_ec var22) {
                  throw new class_bz("commands.fill.tagError", new Object[]{var22.getMessage()});
               }
            }

            ArrayList var25 = Lists.newArrayList();
            var10 = 0;

            for(int var15 = var8.r(); var15 <= var9.r(); ++var15) {
               for(int var16 = var8.q(); var16 <= var9.q(); ++var16) {
                  for(int var17 = var8.p(); var17 <= var9.p(); ++var17) {
                     BlockPosition var18 = new BlockPosition(var17, var16, var15);
                     IBlockData var20;
                     if(var3.length >= 9) {
                        if(!var3[8].equals("outline") && !var3[8].equals("hollow")) {
                           if(var3[8].equals("destroy")) {
                              var11.b(var18, true);
                           } else if(var3[8].equals("keep")) {
                              if(!var11.d(var18)) {
                                 continue;
                              }
                           } else if(var3[8].equals("replace") && !var6.m()) {
                              if(var3.length > 9) {
                                 Block var19 = CommandAbstract.b(var2, var3[9]);
                                 if(var11.getType(var18).getBlock() != var19) {
                                    continue;
                                 }
                              }

                              if(var3.length > 10) {
                                 int var29 = CommandAbstract.a(var3[10]);
                                 var20 = var11.getType(var18);
                                 if(var20.getBlock().e(var20) != var29) {
                                    continue;
                                 }
                              }
                           }
                        } else if(var17 != var8.p() && var17 != var9.p() && var16 != var8.q() && var16 != var9.q() && var15 != var8.r() && var15 != var9.r()) {
                           if(var3[8].equals("hollow")) {
                              var11.a((BlockPosition)var18, (IBlockData)Blocks.AIR.u(), 2);
                              var25.add(var18);
                           }
                           continue;
                        }
                     }

                     TileEntity var30 = var11.r(var18);
                     if(var30 != null) {
                        if(var30 instanceof IInventory) {
                           ((IInventory)var30).l();
                        }

                        var11.a(var18, Blocks.cv.u(), var6 == Blocks.cv?2:4);
                     }

                     var20 = var6.a(var7);
                     if(var11.a((BlockPosition)var18, (IBlockData)var20, 2)) {
                        var25.add(var18);
                        ++var10;
                        if(var24) {
                           TileEntity var21 = var11.r(var18);
                           if(var21 != null) {
                              var23.a("x", var18.p());
                              var23.a("y", var18.q());
                              var23.a("z", var18.r());
                              var21.a(var1, var23);
                           }
                        }
                     }
                  }
               }
            }

            Iterator var26 = var25.iterator();

            while(var26.hasNext()) {
               BlockPosition var27 = (BlockPosition)var26.next();
               Block var28 = var11.getType(var27).getBlock();
               var11.c(var27, var28);
            }

            if(var10 <= 0) {
               throw new class_bz("commands.fill.failed", new Object[0]);
            } else {
               var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_BLOCKS, var10);
               a(var2, this, "commands.fill.success", new Object[]{Integer.valueOf(var10)});
            }
         } else {
            throw new class_bz("commands.fill.outOfWorld", new Object[0]);
         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length > 0 && var3.length <= 3?a(var3, 0, var4):(var3.length > 3 && var3.length <= 6?a(var3, 3, var4):(var3.length == 7?a(var3, Block.h.c()):(var3.length == 9?a(var3, new String[]{"replace", "destroy", "keep", "hollow", "outline"}):(var3.length == 10 && "replace".equals(var3[8])?a(var3, Block.h.c()):Collections.emptyList()))));
   }
}
