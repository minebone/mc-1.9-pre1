package net.minecraft.server;

import java.io.IOException;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayInListener;

public class class_iy implements Packet {
   private int a;
   private class_iy.class_a_in_class_iy b;
   private int c;

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.readVarInt();
      this.b = (class_iy.class_a_in_class_iy)var1.a(class_iy.class_a_in_class_iy.class);
      this.c = var1.readVarInt();
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.writeVarInt(this.a);
      var1.a((Enum)this.b);
      var1.writeVarInt(this.c);
   }

   public void a(PacketPlayInListener var1) {
      var1.a(this);
   }

   public class_iy.class_a_in_class_iy b() {
      return this.b;
   }

   public int c() {
      return this.c;
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketPlayInListener)var1);
   }

   public static enum class_a_in_class_iy {
      START_SNEAKING,
      STOP_SNEAKING,
      STOP_SLEEPING,
      START_SPRINTING,
      STOP_SPRINTING,
      START_RIDING_JUMP,
      STOP_RIDING_JUMP,
      OPEN_INVENTORY,
      START_FALL_FLYING;
   }
}
