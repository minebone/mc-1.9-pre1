package net.minecraft.server;

import java.util.List;
import java.util.Map;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;

public interface class_l {
   int a(ICommandListener var1, String var2);

   List a(ICommandListener var1, String var2, BlockPosition var3);

   List a(ICommandListener var1);

   Map b();
}
