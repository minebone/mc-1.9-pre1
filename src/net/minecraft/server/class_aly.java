package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_or;
import net.minecraft.server.class_rz;

public abstract class class_aly extends Block {
   public static final BlockStateEnum a = BlockStateEnum.a("half", class_aly.class_a_in_class_aly.class);
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.5D, 1.0D);
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.0D, 0.5D, 0.0D, 1.0D, 1.0D, 1.0D);

   public class_aly(Material var1) {
      super(var1);
      this.l = this.e();
      this.d(255);
   }

   protected boolean o() {
      return false;
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return this.e()?j:(var1.get(a) == class_aly.class_a_in_class_aly.TOP?c:b);
   }

   public boolean k(IBlockData var1) {
      return ((class_aly)var1.getBlock()).e() || var1.get(a) == class_aly.class_a_in_class_aly.TOP;
   }

   public boolean b(IBlockData var1) {
      return this.e();
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      IBlockData var9 = super.a(var1, var2, var3, var4, var5, var6, var7, var8).set(a, class_aly.class_a_in_class_aly.BOTTOM);
      return this.e()?var9:(var3 != EnumDirection.DOWN && (var3 == EnumDirection.UP || (double)var5 <= 0.5D)?var9:var9.set(a, class_aly.class_a_in_class_aly.TOP));
   }

   public int a(Random var1) {
      return this.e()?2:1;
   }

   public boolean c(IBlockData var1) {
      return this.e();
   }

   public abstract String e(int var1);

   public abstract boolean e();

   public abstract IBlockState g();

   public abstract Comparable a(ItemStack var1);

   public static enum class_a_in_class_aly implements class_or {
      TOP("top"),
      BOTTOM("bottom");

      private final String c;

      private class_a_in_class_aly(String var3) {
         this.c = var3;
      }

      public String toString() {
         return this.c;
      }

      public String m() {
         return this.c;
      }
   }
}
