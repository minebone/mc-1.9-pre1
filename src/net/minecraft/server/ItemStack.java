package net.minecraft.server;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import java.text.DecimalFormat;
import java.util.Random;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.Block;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatHoverable;
import net.minecraft.server.Enchantment;
import net.minecraft.server.EnchantmentDurability;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumAnimation;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumItemRarity;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.Item;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.class_agn;
import net.minecraft.server.class_agp;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_kk;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_xr;
import net.minecraft.server.class_ys;

public final class ItemStack {
   public static final DecimalFormat a = new DecimalFormat("#.##");
   public int b;
   public int c;
   private Item d;
   private NBTTagCompound e;
   private int f;
   private class_xr g;
   private Block h;
   private boolean i;
   private Block j;
   private boolean k;

   public ItemStack(Block var1) {
      this((Block)var1, 1);
   }

   public ItemStack(Block var1, int var2) {
      this((Block)var1, var2, 0);
   }

   public ItemStack(Block var1, int var2, int var3) {
      this(Item.a(var1), var2, var3);
   }

   public ItemStack(Item var1) {
      this((Item)var1, 1);
   }

   public ItemStack(Item var1, int var2) {
      this((Item)var1, var2, 0);
   }

   public ItemStack(Item var1, int var2, int var3) {
      this.h = null;
      this.i = false;
      this.j = null;
      this.k = false;
      this.d = var1;
      this.b = var2;
      this.f = var3;
      if(this.f < 0) {
         this.f = 0;
      }

   }

   public static ItemStack a(NBTTagCompound var0) {
      ItemStack var1 = new ItemStack();
      var1.c(var0);
      return var1.b() != null?var1:null;
   }

   private ItemStack() {
      this.h = null;
      this.i = false;
      this.j = null;
      this.k = false;
   }

   public ItemStack a(int var1) {
      var1 = Math.min(var1, this.b);
      ItemStack var2 = new ItemStack(this.d, var1, this.f);
      if(this.e != null) {
         var2.e = (NBTTagCompound)this.e.b();
      }

      this.b -= var1;
      return var2;
   }

   public Item b() {
      return this.d;
   }

   public EnumResult a(EntityHuman var1, World var2, BlockPosition var3, EnumHand var4, EnumDirection var5, float var6, float var7, float var8) {
      EnumResult var9 = this.b().a(this, var1, var2, var3, var4, var5, var6, var7, var8);
      if(var9 == EnumResult.SUCCESS) {
         var1.b(StatisticList.b(this.d));
      }

      return var9;
   }

   public float a(IBlockData var1) {
      return this.b().a(this, var1);
   }

   public class_qo a(World var1, EntityHuman var2, EnumHand var3) {
      return this.b().a(this, var1, var2, var3);
   }

   public ItemStack a(World var1, class_rz var2) {
      return this.b().a(this, var1, var2);
   }

   public NBTTagCompound b(NBTTagCompound var1) {
      class_kk var2 = (class_kk)Item.f.b(this.d);
      var1.a("id", var2 == null?"minecraft:air":var2.toString());
      var1.a("Count", (byte)this.b);
      var1.a("Damage", (short)this.f);
      if(this.e != null) {
         var1.a((String)"tag", (NBTTag)this.e);
      }

      return var1;
   }

   public void c(NBTTagCompound var1) {
      this.d = Item.d(var1.l("id"));
      this.b = var1.f("Count");
      this.f = var1.g("Damage");
      if(this.f < 0) {
         this.f = 0;
      }

      if(var1.b("tag", 10)) {
         this.e = var1.o("tag");
         if(this.d != null) {
            this.d.a(this.e);
         }
      }

   }

   public int c() {
      return this.b().j();
   }

   public boolean d() {
      return this.c() > 1 && (!this.e() || !this.g());
   }

   public boolean e() {
      return this.d == null?false:(this.d.l() <= 0?false:!this.n() || !this.o().p("Unbreakable"));
   }

   public boolean f() {
      return this.d.k();
   }

   public boolean g() {
      return this.e() && this.f > 0;
   }

   public int h() {
      return this.f;
   }

   public int i() {
      return this.f;
   }

   public void b(int var1) {
      this.f = var1;
      if(this.f < 0) {
         this.f = 0;
      }

   }

   public int j() {
      return this.d == null?0:this.d.l();
   }

   public boolean a(int var1, Random var2) {
      if(!this.e()) {
         return false;
      } else {
         if(var1 > 0) {
            int var3 = class_agn.a(class_agp.s, this);
            int var4 = 0;

            for(int var5 = 0; var3 > 0 && var5 < var1; ++var5) {
               if(EnchantmentDurability.a(this, var3, var2)) {
                  ++var4;
               }
            }

            var1 -= var4;
            if(var1 <= 0) {
               return false;
            }
         }

         this.f += var1;
         return this.f > this.j();
      }
   }

   public void a(int var1, class_rz var2) {
      if(!(var2 instanceof EntityHuman) || !((EntityHuman)var2).abilities.d) {
         if(this.e()) {
            if(this.a(var1, var2.bE())) {
               var2.b(this);
               --this.b;
               if(var2 instanceof EntityHuman) {
                  EntityHuman var3 = (EntityHuman)var2;
                  var3.b(StatisticList.c(this.d));
               }

               if(this.b < 0) {
                  this.b = 0;
               }

               this.f = 0;
            }

         }
      }
   }

   public void a(class_rz var1, EntityHuman var2) {
      boolean var3 = this.d.a(this, (class_rz)var1, var2);
      if(var3) {
         var2.b(StatisticList.b(this.d));
      }

   }

   public void a(World var1, IBlockData var2, BlockPosition var3, EntityHuman var4) {
      boolean var5 = this.d.a(this, var1, var2, var3, var4);
      if(var5) {
         var4.b(StatisticList.b(this.d));
      }

   }

   public boolean b(IBlockData var1) {
      return this.d.a(var1);
   }

   public boolean a(EntityHuman var1, class_rz var2, EnumHand var3) {
      return this.d.a(this, var1, var2, var3);
   }

   public ItemStack k() {
      ItemStack var1 = new ItemStack(this.d, this.b, this.f);
      if(this.e != null) {
         var1.e = (NBTTagCompound)this.e.b();
      }

      return var1;
   }

   public static boolean a(ItemStack var0, ItemStack var1) {
      return var0 == null && var1 == null?true:(var0 != null && var1 != null?(var0.e == null && var1.e != null?false:var0.e == null || var0.e.equals(var1.e)):false);
   }

   public static boolean b(ItemStack var0, ItemStack var1) {
      return var0 == null && var1 == null?true:(var0 != null && var1 != null?var0.e(var1):false);
   }

   private boolean e(ItemStack var1) {
      return this.b != var1.b?false:(this.d != var1.d?false:(this.f != var1.f?false:(this.e == null && var1.e != null?false:this.e == null || this.e.equals(var1.e))));
   }

   public static boolean c(ItemStack var0, ItemStack var1) {
      return var0 == var1?true:(var0 != null && var1 != null?var0.a(var1):false);
   }

   public static boolean d(ItemStack var0, ItemStack var1) {
      return var0 == var1?true:(var0 != null && var1 != null?var0.b(var1):false);
   }

   public boolean a(ItemStack var1) {
      return var1 != null && this.d == var1.d && this.f == var1.f;
   }

   public boolean b(ItemStack var1) {
      return !this.e()?this.a(var1):var1 != null && this.d == var1.d;
   }

   public String a() {
      return this.d.f_(this);
   }

   public static ItemStack c(ItemStack var0) {
      return var0 == null?null:var0.k();
   }

   public String toString() {
      return this.b + "x" + this.d.a() + "@" + this.f;
   }

   public void a(World var1, Entity var2, int var3, boolean var4) {
      if(this.c > 0) {
         --this.c;
      }

      if(this.d != null) {
         this.d.a(this, var1, var2, var3, var4);
      }

   }

   public void a(World var1, EntityHuman var2, int var3) {
      var2.a(StatisticList.a(this.d), var3);
      this.d.b(this, var1, var2);
   }

   public int l() {
      return this.b().e(this);
   }

   public EnumAnimation m() {
      return this.b().f(this);
   }

   public void a(World var1, class_rz var2, int var3) {
      this.b().a(this, var1, var2, var3);
   }

   public boolean n() {
      return this.e != null;
   }

   public NBTTagCompound o() {
      return this.e;
   }

   public NBTTagCompound a(String var1, boolean var2) {
      if(this.e != null && this.e.b(var1, 10)) {
         return this.e.o(var1);
      } else if(var2) {
         NBTTagCompound var3 = new NBTTagCompound();
         this.a((String)var1, (NBTTag)var3);
         return var3;
      } else {
         return null;
      }
   }

   public NBTTagList p() {
      return this.e == null?null:this.e.c("ench", 10);
   }

   public void d(NBTTagCompound var1) {
      this.e = var1;
   }

   public String q() {
      String var1 = this.b().a(this);
      if(this.e != null && this.e.b("display", 10)) {
         NBTTagCompound var2 = this.e.o("display");
         if(var2.b("Name", 8)) {
            var1 = var2.l("Name");
         }
      }

      return var1;
   }

   public ItemStack c(String var1) {
      if(this.e == null) {
         this.e = new NBTTagCompound();
      }

      if(!this.e.b("display", 10)) {
         this.e.a((String)"display", (NBTTag)(new NBTTagCompound()));
      }

      this.e.o("display").a("Name", var1);
      return this;
   }

   public void r() {
      if(this.e != null) {
         if(this.e.b("display", 10)) {
            NBTTagCompound var1 = this.e.o("display");
            var1.q("Name");
            if(var1.c_()) {
               this.e.q("display");
               if(this.e.c_()) {
                  this.d((NBTTagCompound)null);
               }
            }

         }
      }
   }

   public boolean s() {
      return this.e == null?false:(!this.e.b("display", 10)?false:this.e.o("display").b("Name", 8));
   }

   public EnumItemRarity u() {
      return this.b().g(this);
   }

   public boolean v() {
      return !this.b().g_(this)?false:!this.w();
   }

   public void a(Enchantment var1, int var2) {
      if(this.e == null) {
         this.d(new NBTTagCompound());
      }

      if(!this.e.b("ench", 9)) {
         this.e.a((String)"ench", (NBTTag)(new NBTTagList()));
      }

      NBTTagList var3 = this.e.c("ench", 10);
      NBTTagCompound var4 = new NBTTagCompound();
      var4.a("id", (short)Enchantment.b(var1));
      var4.a("lvl", (short)((byte)var2));
      var3.a((NBTTag)var4);
   }

   public boolean w() {
      return this.e != null && this.e.b("ench", 9);
   }

   public void a(String var1, NBTTag var2) {
      if(this.e == null) {
         this.d(new NBTTagCompound());
      }

      this.e.a(var1, var2);
   }

   public boolean x() {
      return this.b().s();
   }

   public boolean y() {
      return this.g != null;
   }

   public void a(class_xr var1) {
      this.g = var1;
   }

   public class_xr z() {
      return this.g;
   }

   public int A() {
      return this.n() && this.e.b("RepairCost", 3)?this.e.h("RepairCost"):0;
   }

   public void c(int var1) {
      if(!this.n()) {
         this.e = new NBTTagCompound();
      }

      this.e.a("RepairCost", var1);
   }

   public Multimap a(EnumInventorySlot var1) {
      Object var2;
      if(this.n() && this.e.b("AttributeModifiers", 9)) {
         var2 = HashMultimap.create();
         NBTTagList var3 = this.e.c("AttributeModifiers", 10);

         for(int var4 = 0; var4 < var3.c(); ++var4) {
            NBTTagCompound var5 = var3.b(var4);
            AttributeModifier var6 = class_ys.a(var5);
            if(var6 != null && (!var5.b("Slot", 8) || var5.l("Slot").equals(var1.d())) && var6.a().getLeastSignificantBits() != 0L && var6.a().getMostSignificantBits() != 0L) {
               ((Multimap)var2).put(var5.l("AttributeName"), var6);
            }
         }
      } else {
         var2 = this.b().a(var1);
      }

      return (Multimap)var2;
   }

   public void a(String var1, AttributeModifier var2, EnumInventorySlot var3) {
      if(this.e == null) {
         this.e = new NBTTagCompound();
      }

      if(!this.e.b("AttributeModifiers", 9)) {
         this.e.a((String)"AttributeModifiers", (NBTTag)(new NBTTagList()));
      }

      NBTTagList var4 = this.e.c("AttributeModifiers", 10);
      NBTTagCompound var5 = class_ys.a(var2);
      var5.a("AttributeName", var1);
      if(var3 != null) {
         var5.a("Slot", var3.d());
      }

      var4.a((NBTTag)var5);
   }

   public void a(Item var1) {
      this.d = var1;
   }

   public IChatBaseComponent B() {
      ChatComponentText var1 = new ChatComponentText(this.q());
      if(this.s()) {
         var1.b().b(Boolean.valueOf(true));
      }

      IChatBaseComponent var2 = (new ChatComponentText("[")).a(var1).a("]");
      if(this.d != null) {
         NBTTagCompound var3 = new NBTTagCompound();
         this.b(var3);
         var2.b().a(new ChatHoverable(ChatHoverable.EnumHoverAction.SHOW_ITEM, new ChatComponentText(var3.toString())));
         var2.b().a(this.u().e);
      }

      return var2;
   }

   public boolean a(Block var1) {
      if(var1 == this.h) {
         return this.i;
      } else {
         this.h = var1;
         if(this.n() && this.e.b("CanDestroy", 9)) {
            NBTTagList var2 = this.e.c("CanDestroy", 8);

            for(int var3 = 0; var3 < var2.c(); ++var3) {
               Block var4 = Block.b(var2.g(var3));
               if(var4 == var1) {
                  this.i = true;
                  return true;
               }
            }
         }

         this.i = false;
         return false;
      }
   }

   public boolean b(Block var1) {
      if(var1 == this.j) {
         return this.k;
      } else {
         this.j = var1;
         if(this.n() && this.e.b("CanPlaceOn", 9)) {
            NBTTagList var2 = this.e.c("CanPlaceOn", 8);

            for(int var3 = 0; var3 < var2.c(); ++var3) {
               Block var4 = Block.b(var2.g(var3));
               if(var4 == var1) {
                  this.k = true;
                  return true;
               }
            }
         }

         this.k = false;
         return false;
      }
   }
}
