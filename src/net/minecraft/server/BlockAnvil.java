package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.Container;
import net.minecraft.server.ContainerAnvil;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityFallingBlock;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_alg;
import net.minecraft.server.class_amf;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arn;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_qm;
import net.minecraft.server.class_rz;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BlockAnvil extends class_alg {
   public static final class_arn a = class_amf.D;
   public static final BlockStateInteger b = BlockStateInteger.a("damage", 0, 2);
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.0D, 0.0D, 0.125D, 1.0D, 1.0D, 0.875D);
   protected static final AxisAlignedBB d = new AxisAlignedBB(0.125D, 0.0D, 0.0D, 0.875D, 1.0D, 1.0D);
   protected static final Logger e = LogManager.getLogger();

   protected BlockAnvil() {
      super(Material.g);
      this.w(this.A.b().set(a, EnumDirection.NORTH).set(b, Integer.valueOf(0)));
      this.d(0);
      this.a(CreativeModeTab.c);
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      EnumDirection var9 = var8.bh().e();

      try {
         return super.a(var1, var2, var3, var4, var5, var6, var7, var8).set(a, var9).set(b, Integer.valueOf(var7 >> 2));
      } catch (IllegalArgumentException var11) {
         if(!var1.E) {
            e.warn(String.format("Invalid damage property for anvil at %s. Found %d, must be in [0, 1, 2]", new Object[]{var2, Integer.valueOf(var7 >> 2)}));
            if(var8 instanceof EntityHuman) {
               ((EntityHuman)var8).a((IChatBaseComponent)(new ChatMessage("Invalid damage property. Please pick in [0, 1, 2]", new Object[0])));
            }
         }

         return super.a(var1, var2, var3, var4, var5, var6, 0, var8).set(a, var9).set(b, Integer.valueOf(0));
      }
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(!var1.E) {
         var4.a((class_qm)(new BlockAnvil.TileEntityContainerAnvil(var1, var2)));
      }

      return true;
   }

   public int d(IBlockData var1) {
      return ((Integer)var1.get(b)).intValue();
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      EnumDirection var4 = (EnumDirection)var1.get(a);
      return var4.k() == EnumDirection.class_a_in_class_cq.X?c:d;
   }

   protected void a(EntityFallingBlock var1) {
      var1.a(true);
   }

   public void a_(World var1, BlockPosition var2) {
      var1.b(1031, var2, 0);
   }

   public IBlockData a(int var1) {
      return this.u().set(a, EnumDirection.b(var1 & 3)).set(b, Integer.valueOf((var1 & 15) >> 2));
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((EnumDirection)var1.get(a)).b();
      var3 |= ((Integer)var1.get(b)).intValue() << 2;
      return var3;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.getBlock() != this?var1:var1.set(a, var2.a((EnumDirection)var1.get(a)));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b});
   }

   public static class TileEntityContainerAnvil implements class_qm {
      private final World a;
      private final BlockPosition b;

      public TileEntityContainerAnvil(World var1, BlockPosition var2) {
         this.a = var1;
         this.b = var2;
      }

      public String h_() {
         return "anvil";
      }

      public boolean o_() {
         return false;
      }

      public IChatBaseComponent i_() {
         return new ChatMessage(Blocks.cf.a() + ".name", new Object[0]);
      }

      public Container a(PlayerInventory var1, EntityHuman var2) {
         return new ContainerAnvil(var1, this.a, this.b, var2);
      }

      public String k() {
         return "minecraft:anvil";
      }
   }
}
