package net.minecraft.server;

import net.minecraft.server.ChatMessage;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_aht;
import net.minecraft.server.class_bz;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.class_op;

public class CommandSaveAll extends CommandAbstract {
   public String c() {
      return "save-all";
   }

   public String b(ICommandListener var1) {
      return "commands.save.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      var2.a(new ChatMessage("commands.save.start", new Object[0]));
      if(var1.getPlayerList() != null) {
         var1.getPlayerList().j();
      }

      try {
         int var4;
         WorldServer var5;
         boolean var6;
         for(var4 = 0; var4 < var1.d.length; ++var4) {
            if(var1.d[var4] != null) {
               var5 = var1.d[var4];
               var6 = var5.b;
               var5.b = false;
               var5.a(true, (class_op)null);
               var5.b = var6;
            }
         }

         if(var3.length > 0 && "flush".equals(var3[0])) {
            var2.a(new ChatMessage("commands.save.flushStart", new Object[0]));

            for(var4 = 0; var4 < var1.d.length; ++var4) {
               if(var1.d[var4] != null) {
                  var5 = var1.d[var4];
                  var6 = var5.b;
                  var5.b = false;
                  var5.q();
                  var5.b = var6;
               }
            }

            var2.a(new ChatMessage("commands.save.flushEnd", new Object[0]));
         }
      } catch (class_aht var7) {
         a(var2, this, "commands.save.failed", new Object[]{var7.getMessage()});
         return;
      }

      a(var2, this, "commands.save.success", new Object[0]);
   }
}
