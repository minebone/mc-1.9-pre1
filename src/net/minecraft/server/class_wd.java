package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Map;
import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Container;
import net.minecraft.server.CraftingManager;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.EntityAnimal;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumColor;
import net.minecraft.server.InventoryCrafting;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.PathfinderGoalTempt;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.class_qk;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rn;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_tc;
import net.minecraft.server.class_te;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_ti;
import net.minecraft.server.class_to;
import net.minecraft.server.class_ub;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_yc;
import net.minecraft.server.class_ys;

public class class_wd extends EntityAnimal {
   private static final class_ke bv = DataWatcher.a(class_wd.class, class_kg.a);
   private final InventoryCrafting bw = new InventoryCrafting(new Container() {
      public boolean a(EntityHuman var1) {
         return false;
      }
   }, 2, 1);
   private static final Map bx = Maps.newEnumMap(EnumColor.class);
   private int bz;
   private class_te bA;

   public static float[] a(EnumColor var0) {
      return (float[])bx.get(var0);
   }

   public class_wd(World var1) {
      super(var1);
      this.a(0.9F, 1.3F);
      this.bw.a(0, new ItemStack(Items.bd));
      this.bw.a(1, new ItemStack(Items.bd));
   }

   protected void r() {
      this.bp.a(0, new class_tg(this));
      this.bp.a(1, new class_ub(this, 1.25D));
      this.bp.a(2, new class_tc(this, 1.0D));
      this.bp.a(3, new PathfinderGoalTempt(this, 1.1D, Items.Q, false));
      this.bp.a(4, new class_ti(this, 1.1D));
      this.bp.a(5, this.bA = new class_te(this));
      this.bp.a(6, new class_uf(this, 1.0D));
      this.bp.a(7, new class_to(this, EntityHuman.class, 6.0F));
      this.bp.a(8, new class_ue(this));
   }

   protected void M() {
      this.bz = this.bA.f();
      super.M();
   }

   public void n() {
      if(this.world.E) {
         this.bz = Math.max(0, this.bz - 1);
      }

      super.n();
   }

   protected void bz() {
      super.bz();
      this.a((class_sk)class_ys.a).a(8.0D);
      this.a((class_sk)class_ys.d).a(0.23000000417232513D);
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)bv, (Object)Byte.valueOf((byte)0));
   }

   protected class_kk J() {
      if(this.da()) {
         return class_azs.K;
      } else {
         switch(class_wd.SyntheticClass_1.a[this.cZ().ordinal()]) {
         case 1:
         default:
            return class_azs.L;
         case 2:
            return class_azs.M;
         case 3:
            return class_azs.N;
         case 4:
            return class_azs.O;
         case 5:
            return class_azs.P;
         case 6:
            return class_azs.Q;
         case 7:
            return class_azs.R;
         case 8:
            return class_azs.S;
         case 9:
            return class_azs.T;
         case 10:
            return class_azs.U;
         case 11:
            return class_azs.V;
         case 12:
            return class_azs.W;
         case 13:
            return class_azs.X;
         case 14:
            return class_azs.Y;
         case 15:
            return class_azs.Z;
         case 16:
            return class_azs.aa;
         }
      }
   }

   public boolean a(EntityHuman var1, EnumHand var2, ItemStack var3) {
      if(var3 != null && var3.b() == Items.bl && !this.da() && !this.m_()) {
         if(!this.world.E) {
            this.o(true);
            int var4 = 1 + this.random.nextInt(3);

            for(int var5 = 0; var5 < var4; ++var5) {
               class_yc var6 = this.a(new ItemStack(Item.a(Blocks.L), 1, this.cZ().a()), 1.0F);
               var6.motY += (double)(this.random.nextFloat() * 0.05F);
               var6.motX += (double)((this.random.nextFloat() - this.random.nextFloat()) * 0.1F);
               var6.motZ += (double)((this.random.nextFloat() - this.random.nextFloat()) * 0.1F);
            }
         }

         var3.a(1, (class_rz)var1);
         this.a(class_ng.eJ, 1.0F, 1.0F);
      }

      return super.a(var1, var2, var3);
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("Sheared", this.da());
      var1.a("Color", (byte)this.cZ().a());
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.o(var1.p("Sheared"));
      this.b(EnumColor.b(var1.f("Color")));
   }

   protected class_nf G() {
      return class_ng.eG;
   }

   protected class_nf bQ() {
      return class_ng.eI;
   }

   protected class_nf bR() {
      return class_ng.eH;
   }

   protected void a(BlockPosition var1, Block var2) {
      this.a(class_ng.eK, 0.15F, 1.0F);
   }

   public EnumColor cZ() {
      return EnumColor.b(((Byte)this.datawatcher.a(bv)).byteValue() & 15);
   }

   public void b(EnumColor var1) {
      byte var2 = ((Byte)this.datawatcher.a(bv)).byteValue();
      this.datawatcher.b(bv, Byte.valueOf((byte)(var2 & 240 | var1.a() & 15)));
   }

   public boolean da() {
      return (((Byte)this.datawatcher.a(bv)).byteValue() & 16) != 0;
   }

   public void o(boolean var1) {
      byte var2 = ((Byte)this.datawatcher.a(bv)).byteValue();
      if(var1) {
         this.datawatcher.b(bv, Byte.valueOf((byte)(var2 | 16)));
      } else {
         this.datawatcher.b(bv, Byte.valueOf((byte)(var2 & -17)));
      }

   }

   public static EnumColor a(Random var0) {
      int var1 = var0.nextInt(100);
      return var1 < 5?EnumColor.BLACK:(var1 < 10?EnumColor.GRAY:(var1 < 15?EnumColor.SILVER:(var1 < 18?EnumColor.BROWN:(var0.nextInt(500) == 0?EnumColor.PINK:EnumColor.WHITE))));
   }

   public class_wd b(class_rn var1) {
      class_wd var2 = (class_wd)var1;
      class_wd var3 = new class_wd(this.world);
      var3.b(this.a((EntityAnimal)this, (EntityAnimal)var2));
      return var3;
   }

   public void B() {
      this.o(false);
      if(this.m_()) {
         this.a(60);
      }

   }

   public class_sc a(class_qk var1, class_sc var2) {
      var2 = super.a(var1, var2);
      this.b(a(this.world.r));
      return var2;
   }

   private EnumColor a(EntityAnimal var1, EntityAnimal var2) {
      int var3 = ((class_wd)var1).cZ().b();
      int var4 = ((class_wd)var2).cZ().b();
      this.bw.a(0).b(var3);
      this.bw.a(1).b(var4);
      ItemStack var5 = CraftingManager.a().a(this.bw, ((class_wd)var1).world);
      int var6;
      if(var5 != null && var5.b() == Items.bd) {
         var6 = var5.i();
      } else {
         var6 = this.world.r.nextBoolean()?var3:var4;
      }

      return EnumColor.a(var6);
   }

   public float bm() {
      return 0.95F * this.length;
   }

   // $FF: synthetic method
   public class_rn a(class_rn var1) {
      return this.b(var1);
   }

   static {
      bx.put(EnumColor.WHITE, new float[]{1.0F, 1.0F, 1.0F});
      bx.put(EnumColor.ORANGE, new float[]{0.85F, 0.5F, 0.2F});
      bx.put(EnumColor.MAGENTA, new float[]{0.7F, 0.3F, 0.85F});
      bx.put(EnumColor.LIGHT_BLUE, new float[]{0.4F, 0.6F, 0.85F});
      bx.put(EnumColor.YELLOW, new float[]{0.9F, 0.9F, 0.2F});
      bx.put(EnumColor.LIME, new float[]{0.5F, 0.8F, 0.1F});
      bx.put(EnumColor.PINK, new float[]{0.95F, 0.5F, 0.65F});
      bx.put(EnumColor.GRAY, new float[]{0.3F, 0.3F, 0.3F});
      bx.put(EnumColor.SILVER, new float[]{0.6F, 0.6F, 0.6F});
      bx.put(EnumColor.CYAN, new float[]{0.3F, 0.5F, 0.6F});
      bx.put(EnumColor.PURPLE, new float[]{0.5F, 0.25F, 0.7F});
      bx.put(EnumColor.BLUE, new float[]{0.2F, 0.3F, 0.7F});
      bx.put(EnumColor.BROWN, new float[]{0.4F, 0.3F, 0.2F});
      bx.put(EnumColor.GREEN, new float[]{0.4F, 0.5F, 0.2F});
      bx.put(EnumColor.RED, new float[]{0.6F, 0.2F, 0.2F});
      bx.put(EnumColor.BLACK, new float[]{0.1F, 0.1F, 0.1F});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumColor.values().length];

      static {
         try {
            a[EnumColor.WHITE.ordinal()] = 1;
         } catch (NoSuchFieldError var16) {
            ;
         }

         try {
            a[EnumColor.ORANGE.ordinal()] = 2;
         } catch (NoSuchFieldError var15) {
            ;
         }

         try {
            a[EnumColor.MAGENTA.ordinal()] = 3;
         } catch (NoSuchFieldError var14) {
            ;
         }

         try {
            a[EnumColor.LIGHT_BLUE.ordinal()] = 4;
         } catch (NoSuchFieldError var13) {
            ;
         }

         try {
            a[EnumColor.YELLOW.ordinal()] = 5;
         } catch (NoSuchFieldError var12) {
            ;
         }

         try {
            a[EnumColor.LIME.ordinal()] = 6;
         } catch (NoSuchFieldError var11) {
            ;
         }

         try {
            a[EnumColor.PINK.ordinal()] = 7;
         } catch (NoSuchFieldError var10) {
            ;
         }

         try {
            a[EnumColor.GRAY.ordinal()] = 8;
         } catch (NoSuchFieldError var9) {
            ;
         }

         try {
            a[EnumColor.SILVER.ordinal()] = 9;
         } catch (NoSuchFieldError var8) {
            ;
         }

         try {
            a[EnumColor.CYAN.ordinal()] = 10;
         } catch (NoSuchFieldError var7) {
            ;
         }

         try {
            a[EnumColor.PURPLE.ordinal()] = 11;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[EnumColor.BLUE.ordinal()] = 12;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[EnumColor.BROWN.ordinal()] = 13;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumColor.GREEN.ordinal()] = 14;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumColor.RED.ordinal()] = 15;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumColor.BLACK.ordinal()] = 16;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
