package net.minecraft.server;

import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.Item;
import net.minecraft.server.ItemArmor;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_adq;
import net.minecraft.server.class_akt;
import net.minecraft.server.class_kk;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;
import net.minecraft.server.EnumInventorySlot;

public class class_acw extends Item {
   public class_acw() {
      this.j = 1;
      this.e(432);
      this.a(CreativeModeTab.e);
      this.a(new class_kk("broken"), new class_adq() {
      });
      class_akt.c.a(this, ItemArmor.b);
   }

   public static boolean d(ItemStack var0) {
      return var0.h() < var0.j() - 1;
   }

   public boolean a(ItemStack var1, ItemStack var2) {
      return var2.b() == Items.aM;
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      EnumInventorySlot var5 = EntityInsentient.d(var1);
      ItemStack var6 = var3.a(var5);
      if(var6 == null) {
         var3.a(var5, var1.k());
         var1.b = 0;
         return new class_qo(EnumResult.SUCCESS, var1);
      } else {
         return new class_qo(EnumResult.FAIL, var1);
      }
   }
}
