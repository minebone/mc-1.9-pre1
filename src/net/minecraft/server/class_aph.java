package net.minecraft.server;

import com.google.common.base.Objects;
import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_amf;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_apg;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.class_arn;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.class_rz;

public class class_aph extends Block {
   public static final class_arn a = class_amf.D;
   public static final class_arm b = class_arm.a("powered");
   public static final class_arm c = class_arm.a("attached");
   protected static final AxisAlignedBB d = new AxisAlignedBB(0.3125D, 0.0D, 0.625D, 0.6875D, 0.625D, 1.0D);
   protected static final AxisAlignedBB e = new AxisAlignedBB(0.3125D, 0.0D, 0.0D, 0.6875D, 0.625D, 0.375D);
   protected static final AxisAlignedBB f = new AxisAlignedBB(0.625D, 0.0D, 0.3125D, 1.0D, 0.625D, 0.6875D);
   protected static final AxisAlignedBB g = new AxisAlignedBB(0.0D, 0.0D, 0.3125D, 0.375D, 0.625D, 0.6875D);

   public class_aph() {
      super(Material.q);
      this.w(this.A.b().set(a, EnumDirection.NORTH).set(b, Boolean.valueOf(false)).set(c, Boolean.valueOf(false)));
      this.a(CreativeModeTab.d);
      this.a(true);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      switch(class_aph.SyntheticClass_1.a[((EnumDirection)var1.get(a)).ordinal()]) {
      case 1:
      default:
         return g;
      case 2:
         return f;
      case 3:
         return e;
      case 4:
         return d;
      }
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(World var1, BlockPosition var2, EnumDirection var3) {
      return var3.k().c() && var1.getType(var2.a(var3.d())).l();
   }

   public boolean a(World var1, BlockPosition var2) {
      Iterator var3 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

      EnumDirection var4;
      do {
         if(!var3.hasNext()) {
            return false;
         }

         var4 = (EnumDirection)var3.next();
      } while(!var1.getType(var2.a(var4)).l());

      return true;
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      IBlockData var9 = this.u().set(b, Boolean.valueOf(false)).set(c, Boolean.valueOf(false));
      if(var3.k().c()) {
         var9 = var9.set(a, var3);
      }

      return var9;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, class_rz var4, ItemStack var5) {
      this.a(var1, var2, var3, false, false, -1, (IBlockData)null);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(var4 != this) {
         if(this.e(var1, var2, var3)) {
            EnumDirection var5 = (EnumDirection)var3.get(a);
            if(!var1.getType(var2.a(var5.d())).l()) {
               this.b(var1, var2, var3, 0);
               var1.g(var2);
            }
         }

      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, boolean var4, boolean var5, int var6, IBlockData var7) {
      EnumDirection var8 = (EnumDirection)var3.get(a);
      boolean var9 = ((Boolean)var3.get(c)).booleanValue();
      boolean var10 = ((Boolean)var3.get(b)).booleanValue();
      boolean var11 = !var4;
      boolean var12 = false;
      int var13 = 0;
      IBlockData[] var14 = new IBlockData[42];

      BlockPosition var16;
      for(int var15 = 1; var15 < 42; ++var15) {
         var16 = var2.a(var8, var15);
         IBlockData var17 = var1.getType(var16);
         if(var17.getBlock() == Blocks.bR) {
            if(var17.get(a) == var8.d()) {
               var13 = var15;
            }
            break;
         }

         if(var17.getBlock() != Blocks.bS && var15 != var6) {
            var14[var15] = null;
            var11 = false;
         } else {
            if(var15 == var6) {
               var17 = (IBlockData)Objects.firstNonNull(var7, var17);
            }

            boolean var18 = !((Boolean)var17.get(class_apg.c)).booleanValue();
            boolean var19 = ((Boolean)var17.get(class_apg.a)).booleanValue();
            var12 |= var18 && var19;
            var14[var15] = var17;
            if(var15 == var6) {
               var1.a((BlockPosition)var2, (Block)this, this.a(var1));
               var11 &= var18;
            }
         }
      }

      var11 &= var13 > 1;
      var12 &= var11;
      IBlockData var20 = this.u().set(c, Boolean.valueOf(var11)).set(b, Boolean.valueOf(var12));
      if(var13 > 0) {
         var16 = var2.a(var8, var13);
         EnumDirection var22 = var8.d();
         var1.a((BlockPosition)var16, (IBlockData)var20.set(a, var22), 3);
         this.a(var1, var16, var22);
         this.a(var1, var16, var11, var12, var9, var10);
      }

      this.a(var1, var2, var11, var12, var9, var10);
      if(!var4) {
         var1.a((BlockPosition)var2, (IBlockData)var20.set(a, var8), 3);
         if(var5) {
            this.a(var1, var2, var8);
         }
      }

      if(var9 != var11) {
         for(int var21 = 1; var21 < var13; ++var21) {
            BlockPosition var23 = var2.a(var8, var21);
            IBlockData var24 = var14[var21];
            if(var24 != null && var1.getType(var23).getBlock() != Blocks.AIR) {
               var1.a((BlockPosition)var23, (IBlockData)var24.set(c, Boolean.valueOf(var11)), 3);
            }
         }
      }

   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Random var4) {
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      this.a(var1, var2, var3, false, true, -1, (IBlockData)null);
   }

   private void a(World var1, BlockPosition var2, boolean var3, boolean var4, boolean var5, boolean var6) {
      if(var4 && !var6) {
         var1.a((EntityHuman)null, var2, class_ng.gg, EnumSoundCategory.BLOCKS, 0.4F, 0.6F);
      } else if(!var4 && var6) {
         var1.a((EntityHuman)null, var2, class_ng.gf, EnumSoundCategory.BLOCKS, 0.4F, 0.5F);
      } else if(var3 && !var5) {
         var1.a((EntityHuman)null, var2, class_ng.ge, EnumSoundCategory.BLOCKS, 0.4F, 0.7F);
      } else if(!var3 && var5) {
         var1.a((EntityHuman)null, var2, class_ng.gh, EnumSoundCategory.BLOCKS, 0.4F, 1.2F / (var1.r.nextFloat() * 0.2F + 0.9F));
      }

   }

   private void a(World var1, BlockPosition var2, EnumDirection var3) {
      var1.d(var2, this);
      var1.d(var2.a(var3.d()), this);
   }

   private boolean e(World var1, BlockPosition var2, IBlockData var3) {
      if(!this.a(var1, var2)) {
         this.b(var1, var2, var3, 0);
         var1.g(var2);
         return false;
      } else {
         return true;
      }
   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      boolean var4 = ((Boolean)var3.get(c)).booleanValue();
      boolean var5 = ((Boolean)var3.get(b)).booleanValue();
      if(var4 || var5) {
         this.a(var1, var2, var3, true, false, -1, (IBlockData)null);
      }

      if(var5) {
         var1.d(var2, this);
         var1.d(var2.a(((EnumDirection)var3.get(a)).d()), this);
      }

      super.b(var1, var2, var3);
   }

   public int b(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return ((Boolean)var1.get(b)).booleanValue()?15:0;
   }

   public int c(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return !((Boolean)var1.get(b)).booleanValue()?0:(var1.get(a) == var4?15:0);
   }

   public boolean g(IBlockData var1) {
      return true;
   }

   public IBlockData a(int var1) {
      return this.u().set(a, EnumDirection.b(var1 & 3)).set(b, Boolean.valueOf((var1 & 8) > 0)).set(c, Boolean.valueOf((var1 & 4) > 0));
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((EnumDirection)var1.get(a)).b();
      if(((Boolean)var1.get(b)).booleanValue()) {
         var3 |= 8;
      }

      if(((Boolean)var1.get(c)).booleanValue()) {
         var3 |= 4;
      }

      return var3;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(a, var2.a((EnumDirection)var1.get(a)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(a)));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b, c});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumDirection.values().length];

      static {
         try {
            a[EnumDirection.EAST.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.SOUTH.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.NORTH.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
