package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_aph;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_apg extends Block {
   public static final class_arm a = class_arm.a("powered");
   public static final class_arm b = class_arm.a("attached");
   public static final class_arm c = class_arm.a("disarmed");
   public static final class_arm d = class_arm.a("north");
   public static final class_arm e = class_arm.a("east");
   public static final class_arm f = class_arm.a("south");
   public static final class_arm g = class_arm.a("west");
   protected static final AxisAlignedBB B = new AxisAlignedBB(0.0D, 0.0625D, 0.0D, 1.0D, 0.15625D, 1.0D);
   protected static final AxisAlignedBB C = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.5D, 1.0D);

   public class_apg() {
      super(Material.q);
      this.w(this.A.b().set(a, Boolean.valueOf(false)).set(b, Boolean.valueOf(false)).set(c, Boolean.valueOf(false)).set(d, Boolean.valueOf(false)).set(e, Boolean.valueOf(false)).set(f, Boolean.valueOf(false)).set(g, Boolean.valueOf(false)));
      this.a(true);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return !((Boolean)var1.get(b)).booleanValue()?C:B;
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return var1.set(d, Boolean.valueOf(a(var2, var3, var1, EnumDirection.NORTH))).set(e, Boolean.valueOf(a(var2, var3, var1, EnumDirection.EAST))).set(f, Boolean.valueOf(a(var2, var3, var1, EnumDirection.SOUTH))).set(g, Boolean.valueOf(a(var2, var3, var1, EnumDirection.WEST)));
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Items.H;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Items.H);
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      var1.a((BlockPosition)var2, (IBlockData)var3, 3);
      this.e(var1, var2, var3);
   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      this.e(var1, var2, var3.set(a, Boolean.valueOf(true)));
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4) {
      if(!var1.E) {
         if(var4.ca() != null && var4.ca().b() == Items.bl) {
            var1.a((BlockPosition)var2, (IBlockData)var3.set(c, Boolean.valueOf(true)), 4);
         }

      }
   }

   private void e(World var1, BlockPosition var2, IBlockData var3) {
      EnumDirection[] var4 = new EnumDirection[]{EnumDirection.SOUTH, EnumDirection.WEST};
      int var5 = var4.length;

      for(int var6 = 0; var6 < var5; ++var6) {
         EnumDirection var7 = var4[var6];

         for(int var8 = 1; var8 < 42; ++var8) {
            BlockPosition var9 = var2.a(var7, var8);
            IBlockData var10 = var1.getType(var9);
            if(var10.getBlock() == Blocks.bR) {
               if(var10.get(class_aph.a) == var7.d()) {
                  Blocks.bR.a(var1, var9, var10, false, true, var8, var3);
               }
               break;
            }

            if(var10.getBlock() != Blocks.bS) {
               break;
            }
         }
      }

   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Entity var4) {
      if(!var1.E) {
         if(!((Boolean)var3.get(a)).booleanValue()) {
            this.b(var1, var2);
         }
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Random var4) {
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(!var1.E) {
         if(((Boolean)var1.getType(var2).get(a)).booleanValue()) {
            this.b(var1, var2);
         }
      }
   }

   private void b(World var1, BlockPosition var2) {
      IBlockData var3 = var1.getType(var2);
      boolean var4 = ((Boolean)var3.get(a)).booleanValue();
      boolean var5 = false;
      List var6 = var1.b((Entity)null, (AxisAlignedBB)var3.c(var1, var2).a(var2));
      if(!var6.isEmpty()) {
         Iterator var7 = var6.iterator();

         while(var7.hasNext()) {
            Entity var8 = (Entity)var7.next();
            if(!var8.aZ()) {
               var5 = true;
               break;
            }
         }
      }

      if(var5 != var4) {
         var3 = var3.set(a, Boolean.valueOf(var5));
         var1.a((BlockPosition)var2, (IBlockData)var3, 3);
         this.e(var1, var2, var3);
      }

      if(var5) {
         var1.a((BlockPosition)(new BlockPosition(var2)), (Block)this, this.a(var1));
      }

   }

   public static boolean a(class_ahw var0, BlockPosition var1, IBlockData var2, EnumDirection var3) {
      BlockPosition var4 = var1.a(var3);
      IBlockData var5 = var0.getType(var4);
      Block var6 = var5.getBlock();
      if(var6 == Blocks.bR) {
         EnumDirection var7 = var3.d();
         return var5.get(class_aph.a) == var7;
      } else {
         return var6 == Blocks.bS;
      }
   }

   public IBlockData a(int var1) {
      return this.u().set(a, Boolean.valueOf((var1 & 1) > 0)).set(b, Boolean.valueOf((var1 & 4) > 0)).set(c, Boolean.valueOf((var1 & 8) > 0));
   }

   public int e(IBlockData var1) {
      int var2 = 0;
      if(((Boolean)var1.get(a)).booleanValue()) {
         var2 |= 1;
      }

      if(((Boolean)var1.get(b)).booleanValue()) {
         var2 |= 4;
      }

      if(((Boolean)var1.get(c)).booleanValue()) {
         var2 |= 8;
      }

      return var2;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      switch(class_apg.SyntheticClass_1.a[var2.ordinal()]) {
      case 1:
         return var1.set(d, var1.get(f)).set(e, var1.get(g)).set(f, var1.get(d)).set(g, var1.get(e));
      case 2:
         return var1.set(d, var1.get(e)).set(e, var1.get(f)).set(f, var1.get(g)).set(g, var1.get(d));
      case 3:
         return var1.set(d, var1.get(g)).set(e, var1.get(d)).set(f, var1.get(e)).set(g, var1.get(f));
      default:
         return var1;
      }
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      switch(class_apg.SyntheticClass_1.b[var2.ordinal()]) {
      case 1:
         return var1.set(d, var1.get(f)).set(f, var1.get(d));
      case 2:
         return var1.set(e, var1.get(g)).set(g, var1.get(e));
      default:
         return super.a(var1, var2);
      }
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b, c, d, e, g, f});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b = new int[class_amq.values().length];

      static {
         try {
            b[class_amq.LEFT_RIGHT.ordinal()] = 1;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            b[class_amq.FRONT_BACK.ordinal()] = 2;
         } catch (NoSuchFieldError var4) {
            ;
         }

         a = new int[class_aod.values().length];

         try {
            a[class_aod.CLOCKWISE_180.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[class_aod.CLOCKWISE_90.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
