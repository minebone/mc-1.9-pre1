package net.minecraft.server;

import java.io.IOException;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayInListener;

public class class_it implements Packet {
   protected double a;
   protected double b;
   protected double c;
   protected float d;
   protected float e;
   protected boolean f;
   protected boolean g;
   protected boolean h;

   public void a(PacketPlayInListener var1) {
      var1.a(this);
   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.f = var1.readUnsignedByte() != 0;
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.writeByte(this.f?1:0);
   }

   public double a(double var1) {
      return this.g?this.a:var1;
   }

   public double b(double var1) {
      return this.g?this.b:var1;
   }

   public double c(double var1) {
      return this.g?this.c:var1;
   }

   public float a(float var1) {
      return this.h?this.d:var1;
   }

   public float b(float var1) {
      return this.h?this.e:var1;
   }

   public boolean a() {
      return this.f;
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketPlayInListener)var1);
   }

   public static class class_c_in_class_it extends class_it {
      public class_c_in_class_it() {
         this.h = true;
      }

      public void decode(PacketDataSerializer var1) throws IOException {
         this.d = var1.readFloat();
         this.e = var1.readFloat();
         super.decode(var1);
      }

      public void encode(PacketDataSerializer var1) throws IOException {
         var1.writeFloat(this.d);
         var1.writeFloat(this.e);
         super.encode(var1);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void handle(PacketListener var1) {
         super.a((PacketPlayInListener)var1);
      }
   }

   public static class class_a_in_class_it extends class_it {
      public class_a_in_class_it() {
         this.g = true;
      }

      public void decode(PacketDataSerializer var1) throws IOException {
         this.a = var1.readDouble();
         this.b = var1.readDouble();
         this.c = var1.readDouble();
         super.decode(var1);
      }

      public void encode(PacketDataSerializer var1) throws IOException {
         var1.writeDouble(this.a);
         var1.writeDouble(this.b);
         var1.writeDouble(this.c);
         super.encode(var1);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void handle(PacketListener var1) {
         super.a((PacketPlayInListener)var1);
      }
   }

   public static class class_b_in_class_it extends class_it {
      public class_b_in_class_it() {
         this.g = true;
         this.h = true;
      }

      public void decode(PacketDataSerializer var1) throws IOException {
         this.a = var1.readDouble();
         this.b = var1.readDouble();
         this.c = var1.readDouble();
         this.d = var1.readFloat();
         this.e = var1.readFloat();
         super.decode(var1);
      }

      public void encode(PacketDataSerializer var1) throws IOException {
         var1.writeDouble(this.a);
         var1.writeDouble(this.b);
         var1.writeDouble(this.c);
         var1.writeFloat(this.d);
         var1.writeFloat(this.e);
         super.encode(var1);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void handle(PacketListener var1) {
         super.a((PacketPlayInListener)var1);
      }
   }
}
