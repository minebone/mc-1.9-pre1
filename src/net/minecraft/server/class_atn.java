package net.minecraft.server;

import java.util.List;
import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.Blocks;
import net.minecraft.server.Chunk;
import net.minecraft.server.EnumCreatureType;
import net.minecraft.server.World;
import net.minecraft.server.class_ahm;
import net.minecraft.server.class_ake;
import net.minecraft.server.class_alg;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arx;
import net.minecraft.server.class_ate;
import net.minecraft.server.class_aua;
import net.minecraft.server.class_avp;
import net.minecraft.server.class_awt;
import net.minecraft.server.class_awx;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;

public class class_atn implements class_arx {
   private Random h;
   protected static final IBlockData a = Blocks.bH.u();
   protected static final IBlockData b = Blocks.AIR.u();
   private final class_awt i;
   private final class_awt j;
   private final class_awt k;
   public class_awt c;
   public class_awt d;
   private final World l;
   private final boolean m;
   private final class_avp n = new class_avp(this);
   private final class_awx o;
   private double[] p;
   private BiomeBase[] q;
   double[] e;
   double[] f;
   double[] g;
   private final class_aua r = new class_aua();

   public class_atn(World var1, boolean var2, long var3) {
      this.l = var1;
      this.m = var2;
      this.h = new Random(var3);
      this.i = new class_awt(this.h, 16);
      this.j = new class_awt(this.h, 16);
      this.k = new class_awt(this.h, 8);
      this.c = new class_awt(this.h, 10);
      this.d = new class_awt(this.h, 16);
      this.o = new class_awx(this.h);
   }

   public void a(int var1, int var2, class_ate var3) {
      byte var4 = 2;
      int var5 = var4 + 1;
      byte var6 = 33;
      int var7 = var4 + 1;
      this.p = this.a(this.p, var1 * var4, 0, var2 * var4, var5, var6, var7);

      for(int var8 = 0; var8 < var4; ++var8) {
         for(int var9 = 0; var9 < var4; ++var9) {
            for(int var10 = 0; var10 < 32; ++var10) {
               double var11 = 0.25D;
               double var13 = this.p[((var8 + 0) * var7 + var9 + 0) * var6 + var10 + 0];
               double var15 = this.p[((var8 + 0) * var7 + var9 + 1) * var6 + var10 + 0];
               double var17 = this.p[((var8 + 1) * var7 + var9 + 0) * var6 + var10 + 0];
               double var19 = this.p[((var8 + 1) * var7 + var9 + 1) * var6 + var10 + 0];
               double var21 = (this.p[((var8 + 0) * var7 + var9 + 0) * var6 + var10 + 1] - var13) * var11;
               double var23 = (this.p[((var8 + 0) * var7 + var9 + 1) * var6 + var10 + 1] - var15) * var11;
               double var25 = (this.p[((var8 + 1) * var7 + var9 + 0) * var6 + var10 + 1] - var17) * var11;
               double var27 = (this.p[((var8 + 1) * var7 + var9 + 1) * var6 + var10 + 1] - var19) * var11;

               for(int var29 = 0; var29 < 4; ++var29) {
                  double var30 = 0.125D;
                  double var32 = var13;
                  double var34 = var15;
                  double var36 = (var17 - var13) * var30;
                  double var38 = (var19 - var15) * var30;

                  for(int var40 = 0; var40 < 8; ++var40) {
                     double var41 = 0.125D;
                     double var43 = var32;
                     double var45 = (var34 - var32) * var41;

                     for(int var47 = 0; var47 < 8; ++var47) {
                        IBlockData var48 = b;
                        if(var43 > 0.0D) {
                           var48 = a;
                        }

                        int var49 = var40 + var8 * 8;
                        int var50 = var29 + var10 * 4;
                        int var51 = var47 + var9 * 8;
                        var3.a(var49, var50, var51, var48);
                        var43 += var45;
                     }

                     var32 += var36;
                     var34 += var38;
                  }

                  var13 += var21;
                  var15 += var23;
                  var17 += var25;
                  var19 += var27;
               }
            }
         }
      }

   }

   public void a(class_ate var1) {
      for(int var2 = 0; var2 < 16; ++var2) {
         for(int var3 = 0; var3 < 16; ++var3) {
            byte var4 = 1;
            int var5 = -1;
            IBlockData var6 = a;
            IBlockData var7 = a;

            for(int var8 = 127; var8 >= 0; --var8) {
               IBlockData var9 = var1.a(var2, var8, var3);
               if(var9.getMaterial() == Material.a) {
                  var5 = -1;
               } else if(var9.getBlock() == Blocks.b) {
                  if(var5 == -1) {
                     if(var4 <= 0) {
                        var6 = b;
                        var7 = a;
                     }

                     var5 = var4;
                     if(var8 >= 0) {
                        var1.a(var2, var8, var3, var6);
                     } else {
                        var1.a(var2, var8, var3, var7);
                     }
                  } else if(var5 > 0) {
                     --var5;
                     var1.a(var2, var8, var3, var7);
                  }
               }
            }
         }
      }

   }

   public Chunk a(int var1, int var2) {
      this.h.setSeed((long)var1 * 341873128712L + (long)var2 * 132897987541L);
      class_ate var3 = new class_ate();
      this.q = this.l.A().b(this.q, var1 * 16, var2 * 16, 16, 16);
      this.a(var1, var2, var3);
      this.a(var3);
      if(this.m) {
         this.n.a(this.l, var1, var2, var3);
      }

      Chunk var4 = new Chunk(this.l, var3, var1, var2);
      byte[] var5 = var4.l();

      for(int var6 = 0; var6 < var5.length; ++var6) {
         var5[var6] = (byte)BiomeBase.a(this.q[var6]);
      }

      var4.b();
      return var4;
   }

   private float a(int var1, int var2, int var3, int var4) {
      float var5 = (float)(var1 * 2 + var3);
      float var6 = (float)(var2 * 2 + var4);
      float var7 = 100.0F - MathHelper.c(var5 * var5 + var6 * var6) * 8.0F;
      if(var7 > 80.0F) {
         var7 = 80.0F;
      }

      if(var7 < -100.0F) {
         var7 = -100.0F;
      }

      for(int var8 = -12; var8 <= 12; ++var8) {
         for(int var9 = -12; var9 <= 12; ++var9) {
            long var10 = (long)(var1 + var8);
            long var12 = (long)(var2 + var9);
            if(var10 * var10 + var12 * var12 > 4096L && this.o.a((double)var10, (double)var12) < -0.8999999761581421D) {
               float var14 = (MathHelper.e((float)var10) * 3439.0F + MathHelper.e((float)var12) * 147.0F) % 13.0F + 9.0F;
               var5 = (float)(var3 - var8 * 2);
               var6 = (float)(var4 - var9 * 2);
               float var15 = 100.0F - MathHelper.c(var5 * var5 + var6 * var6) * var14;
               if(var15 > 80.0F) {
                  var15 = 80.0F;
               }

               if(var15 < -100.0F) {
                  var15 = -100.0F;
               }

               if(var15 > var7) {
                  var7 = var15;
               }
            }
         }
      }

      return var7;
   }

   public boolean c(int var1, int var2) {
      return (long)var1 * (long)var1 + (long)var2 * (long)var2 > 4096L && this.a(var1, var2, 1, 1) >= 0.0F;
   }

   private double[] a(double[] var1, int var2, int var3, int var4, int var5, int var6, int var7) {
      if(var1 == null) {
         var1 = new double[var5 * var6 * var7];
      }

      double var8 = 684.412D;
      double var10 = 684.412D;
      var8 *= 2.0D;
      this.e = this.k.a(this.e, var2, var3, var4, var5, var6, var7, var8 / 80.0D, var10 / 160.0D, var8 / 80.0D);
      this.f = this.i.a(this.f, var2, var3, var4, var5, var6, var7, var8, var10, var8);
      this.g = this.j.a(this.g, var2, var3, var4, var5, var6, var7, var8, var10, var8);
      int var12 = var2 / 2;
      int var13 = var4 / 2;
      int var14 = 0;

      for(int var15 = 0; var15 < var5; ++var15) {
         for(int var16 = 0; var16 < var7; ++var16) {
            float var17 = this.a(var12, var13, var15, var16);

            for(int var18 = 0; var18 < var6; ++var18) {
               double var19 = 0.0D;
               double var21 = this.f[var14] / 512.0D;
               double var23 = this.g[var14] / 512.0D;
               double var25 = (this.e[var14] / 10.0D + 1.0D) / 2.0D;
               if(var25 < 0.0D) {
                  var19 = var21;
               } else if(var25 > 1.0D) {
                  var19 = var23;
               } else {
                  var19 = var21 + (var23 - var21) * var25;
               }

               var19 -= 8.0D;
               var19 += (double)var17;
               byte var27 = 2;
               double var28;
               if(var18 > var6 / 2 - var27) {
                  var28 = (double)((float)(var18 - (var6 / 2 - var27)) / 64.0F);
                  var28 = MathHelper.a(var28, 0.0D, 1.0D);
                  var19 = var19 * (1.0D - var28) + -3000.0D * var28;
               }

               var27 = 8;
               if(var18 < var27) {
                  var28 = (double)((float)(var27 - var18) / ((float)var27 - 1.0F));
                  var19 = var19 * (1.0D - var28) + -30.0D * var28;
               }

               var1[var14] = var19;
               ++var14;
            }
         }
      }

      return var1;
   }

   public void b(int var1, int var2) {
      class_alg.f = true;
      BlockPosition var3 = new BlockPosition(var1 * 16, 0, var2 * 16);
      if(this.m) {
         this.n.a(this.l, this.h, new class_ahm(var1, var2));
      }

      this.l.b(var3.a(16, 0, 16)).a(this.l, this.l.r, var3);
      long var4 = (long)var1 * (long)var1 + (long)var2 * (long)var2;
      if(var4 > 4096L) {
         float var6 = this.a(var1, var2, 1, 1);
         if(var6 < -20.0F && this.h.nextInt(14) == 0) {
            this.r.b(this.l, this.h, var3.a(this.h.nextInt(16) + 8, 55 + this.h.nextInt(16), this.h.nextInt(16) + 8));
            if(this.h.nextInt(4) == 0) {
               this.r.b(this.l, this.h, var3.a(this.h.nextInt(16) + 8, 55 + this.h.nextInt(16), this.h.nextInt(16) + 8));
            }
         }

         if(this.a(var1, var2, 1, 1) > 40.0F) {
            int var7 = this.h.nextInt(5);

            for(int var8 = 0; var8 < var7; ++var8) {
               int var9 = this.h.nextInt(16) + 8;
               int var10 = this.h.nextInt(16) + 8;
               int var11 = this.l.l(var3.a(var9, 0, var10)).q();
               if(var11 > 0) {
                  int var12 = var11 - 1;
                  if(this.l.d(var3.a(var9, var12 + 1, var10)) && this.l.getType(var3.a(var9, var12, var10)).getBlock() == Blocks.bH) {
                     class_ake.a(this.l, var3.a(var9, var12 + 1, var10), this.h, 8);
                  }
               }
            }
         }
      }

      class_alg.f = false;
   }

   public boolean a(Chunk var1, int var2, int var3) {
      return false;
   }

   public List a(EnumCreatureType var1, BlockPosition var2) {
      return this.l.b(var2).a(var1);
   }

   public BlockPosition a(World var1, String var2, BlockPosition var3) {
      return null;
   }

   public void b(Chunk var1, int var2, int var3) {
   }
}
