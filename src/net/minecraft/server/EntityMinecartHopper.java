package net.minecraft.server;

import java.util.List;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Container;
import net.minecraft.server.DamageSource;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityMinecartAbstract;
import net.minecraft.server.EntityMinecartContainer;
import net.minecraft.server.Item;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.TileEntityHopper;
import net.minecraft.server.World;
import net.minecraft.server.class_abi;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aqg;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.IInventory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_ru;
import net.minecraft.server.class_yc;

public class EntityMinecartHopper extends EntityMinecartContainer implements class_aqg {
   private boolean a = true;
   private int b = -1;
   private BlockPosition c = BlockPosition.a;

   public EntityMinecartHopper(World var1) {
      super(var1);
   }

   public EntityMinecartHopper(World var1, double var2, double var4, double var6) {
      super(var1, var2, var4, var6);
   }

   public EntityMinecartAbstract.EnumMinecartType v() {
      return EntityMinecartAbstract.EnumMinecartType.HOPPER;
   }

   public IBlockData x() {
      return Blocks.cp.u();
   }

   public int A() {
      return 1;
   }

   public int u_() {
      return 5;
   }

   public boolean a(EntityHuman var1, ItemStack var2, EnumHand var3) {
      if(!this.world.E) {
         var1.openContainer((IInventory)this);
      }

      return true;
   }

   public void a(int var1, int var2, int var3, boolean var4) {
      boolean var5 = !var4;
      if(var5 != this.C()) {
         this.k(var5);
      }

   }

   public boolean C() {
      return this.a;
   }

   public void k(boolean var1) {
      this.a = var1;
   }

   public World D() {
      return this.world;
   }

   public double E() {
      return this.locX;
   }

   public double F() {
      return this.locY + 0.5D;
   }

   public double G() {
      return this.locZ;
   }

   public void m() {
      super.m();
      if(!this.world.E && this.at() && this.C()) {
         BlockPosition var1 = new BlockPosition(this);
         if(var1.equals(this.c)) {
            --this.b;
         } else {
            this.l(0);
         }

         if(!this.J()) {
            this.l(0);
            if(this.I()) {
               this.l(4);
               this.v_();
            }
         }
      }

   }

   public boolean I() {
      if(TileEntityHopper.a((class_aqg)this)) {
         return true;
      } else {
         List var1 = this.world.a(class_yc.class, this.bk().b(0.25D, 0.0D, 0.25D), class_ru.a);
         if(!var1.isEmpty()) {
            TileEntityHopper.a((IInventory)this, (class_yc)((class_yc)var1.get(0)));
         }

         return false;
      }
   }

   public void a(DamageSource var1) {
      super.a(var1);
      if(this.world.U().b("doEntityDrops")) {
         this.a(Item.a((Block)Blocks.cp), 1, 0.0F);
      }

   }

   protected void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("TransferCooldown", this.b);
      var1.a("Enabled", this.a);
   }

   protected void a(NBTTagCompound var1) {
      super.a(var1);
      this.b = var1.h("TransferCooldown");
      this.a = var1.e("Enabled")?var1.p("Enabled"):true;
   }

   public void l(int var1) {
      this.b = var1;
   }

   public boolean J() {
      return this.b > 0;
   }

   public String k() {
      return "minecraft:hopper";
   }

   public Container a(PlayerInventory var1, EntityHuman var2) {
      return new class_abi(var1, this, var2);
   }
}
