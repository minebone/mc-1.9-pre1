package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.Chunk;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_aib implements class_ahw {
   protected int a;
   protected int b;
   protected Chunk[][] c;
   protected boolean d;
   protected World e;

   public class_aib(World var1, BlockPosition var2, BlockPosition var3, int var4) {
      this.e = var1;
      this.a = var2.p() - var4 >> 4;
      this.b = var2.r() - var4 >> 4;
      int var5 = var3.p() + var4 >> 4;
      int var6 = var3.r() + var4 >> 4;
      this.c = new Chunk[var5 - this.a + 1][var6 - this.b + 1];
      this.d = true;

      int var7;
      int var8;
      for(var7 = this.a; var7 <= var5; ++var7) {
         for(var8 = this.b; var8 <= var6; ++var8) {
            this.c[var7 - this.a][var8 - this.b] = var1.a(var7, var8);
         }
      }

      for(var7 = var2.p() >> 4; var7 <= var3.p() >> 4; ++var7) {
         for(var8 = var2.r() >> 4; var8 <= var3.r() >> 4; ++var8) {
            Chunk var9 = this.c[var7 - this.a][var8 - this.b];
            if(var9 != null && !var9.c(var2.q(), var3.q())) {
               this.d = false;
            }
         }
      }

   }

   public TileEntity r(BlockPosition var1) {
      int var2 = (var1.p() >> 4) - this.a;
      int var3 = (var1.r() >> 4) - this.b;
      return this.c[var2][var3].a(var1, Chunk.EnumTileEntityState.IMMEDIATE);
   }

   public IBlockData getType(BlockPosition var1) {
      if(var1.q() >= 0 && var1.q() < 256) {
         int var2 = (var1.p() >> 4) - this.a;
         int var3 = (var1.r() >> 4) - this.b;
         if(var2 >= 0 && var2 < this.c.length && var3 >= 0 && var3 < this.c[var2].length) {
            Chunk var4 = this.c[var2][var3];
            if(var4 != null) {
               return var4.a(var1);
            }
         }
      }

      return Blocks.AIR.u();
   }

   public boolean d(BlockPosition var1) {
      return this.getType(var1).getMaterial() == Material.a;
   }

   public int a(BlockPosition var1, EnumDirection var2) {
      return this.getType(var1).b(this, var1, var2);
   }
}
