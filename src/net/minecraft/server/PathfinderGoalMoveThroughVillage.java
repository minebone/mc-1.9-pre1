package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.EntityCreature;
import net.minecraft.server.Vec3D;
import net.minecraft.server.Village;
import net.minecraft.server.class_ayo;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_vd;
import net.minecraft.server.class_vl;
import net.minecraft.server.class_vn;

public class PathfinderGoalMoveThroughVillage extends class_tj {
   private EntityCreature a;
   private double b;
   private class_ayo c;
   private class_vn d;
   private boolean e;
   private List f = Lists.newArrayList();

   public PathfinderGoalMoveThroughVillage(EntityCreature var1, double var2, boolean var4) {
      this.a = var1;
      this.b = var2;
      this.e = var4;
      this.a(1);
      if(!(var1.x() instanceof class_vd)) {
         throw new IllegalArgumentException("Unsupported mob for MoveThroughVillageGoal");
      }
   }

   public boolean a() {
      this.f();
      if(this.e && this.a.world.B()) {
         return false;
      } else {
         Village var1 = this.a.world.ai().a(new BlockPosition(this.a), 0);
         if(var1 == null) {
            return false;
         } else {
            this.d = this.a(var1);
            if(this.d == null) {
               return false;
            } else {
               class_vd var2 = (class_vd)this.a.x();
               boolean var3 = var2.f();
               var2.a(false);
               this.c = var2.a(this.d.d());
               var2.a(var3);
               if(this.c != null) {
                  return true;
               } else {
                  Vec3D var4 = class_vl.a(this.a, 10, 7, new Vec3D((double)this.d.d().p(), (double)this.d.d().q(), (double)this.d.d().r()));
                  if(var4 == null) {
                     return false;
                  } else {
                     var2.a(false);
                     this.c = this.a.x().a(var4.b, var4.c, var4.d);
                     var2.a(var3);
                     return this.c != null;
                  }
               }
            }
         }
      }
   }

   public boolean b() {
      if(this.a.x().n()) {
         return false;
      } else {
         float var1 = this.a.width + 4.0F;
         return this.a.c(this.d.d()) > (double)(var1 * var1);
      }
   }

   public void c() {
      this.a.x().a(this.c, this.b);
   }

   public void d() {
      if(this.a.x().n() || this.a.c(this.d.d()) < 16.0D) {
         this.f.add(this.d);
      }

   }

   private class_vn a(Village var1) {
      class_vn var2 = null;
      int var3 = Integer.MAX_VALUE;
      List var4 = var1.f();
      Iterator var5 = var4.iterator();

      while(var5.hasNext()) {
         class_vn var6 = (class_vn)var5.next();
         int var7 = var6.b(MathHelper.c(this.a.locX), MathHelper.c(this.a.locY), MathHelper.c(this.a.locZ));
         if(var7 < var3 && !this.a(var6)) {
            var2 = var6;
            var3 = var7;
         }
      }

      return var2;
   }

   private boolean a(class_vn var1) {
      Iterator var2 = this.f.iterator();

      class_vn var3;
      do {
         if(!var2.hasNext()) {
            return false;
         }

         var3 = (class_vn)var2.next();
      } while(!var1.d().equals(var3.d()));

      return true;
   }

   private void f() {
      if(this.f.size() > 15) {
         this.f.remove(0);
      }

   }
}
