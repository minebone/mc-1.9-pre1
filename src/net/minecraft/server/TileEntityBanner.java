package net.minecraft.server;

import java.util.List;
import net.minecraft.server.BlockFlowers;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumColor;
import net.minecraft.server.Items;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutTileEntityData;

public class TileEntityBanner extends TileEntity {
   private int a;
   private NBTTagList f;
   private boolean g;
   private List h;
   private List i;
   private String j;

   public void a(ItemStack var1) {
      this.f = null;
      if(var1.n() && var1.o().b("BlockEntityTag", 10)) {
         NBTTagCompound var2 = var1.o().o("BlockEntityTag");
         if(var2.e("Patterns")) {
            this.f = (NBTTagList)var2.c("Patterns", 10).b();
         }

         if(var2.b("Base", 99)) {
            this.a = var2.h("Base");
         } else {
            this.a = var1.i() & 15;
         }
      } else {
         this.a = var1.i() & 15;
      }

      this.h = null;
      this.i = null;
      this.j = "";
      this.g = true;
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      a(var1, this.a, this.f);
   }

   public static void a(NBTTagCompound var0, int var1, NBTTagList var2) {
      var0.a("Base", var1);
      if(var2 != null) {
         var0.a((String)"Patterns", (NBTTag)var2);
      }

   }

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      this.a = var2.h("Base");
      this.f = var2.c("Patterns", 10);
      this.h = null;
      this.i = null;
      this.j = null;
      this.g = true;
   }

   public Packet D_() {
      NBTTagCompound var1 = new NBTTagCompound();
      this.a(var1);
      return new PacketPlayOutTileEntityData(this.c, 6, var1);
   }

   public int b() {
      return this.a;
   }

   public static int b(ItemStack var0) {
      NBTTagCompound var1 = var0.a("BlockEntityTag", false);
      return var1 != null && var1.e("Base")?var1.h("Base"):var0.i();
   }

   public static int c(ItemStack var0) {
      NBTTagCompound var1 = var0.a("BlockEntityTag", false);
      return var1 != null && var1.e("Patterns")?var1.c("Patterns", 10).c():0;
   }

   public NBTTagList d() {
      return this.f;
   }

   public static void a(ItemStack var0, EnumColor var1) {
      NBTTagCompound var2 = var0.a("BlockEntityTag", true);
      var2.a("Base", var1.b());
   }

   public static void e(ItemStack var0) {
      NBTTagCompound var1 = var0.a("BlockEntityTag", false);
      if(var1 != null && var1.b("Patterns", 9)) {
         NBTTagList var2 = var1.c("Patterns", 10);
         if(var2.c() > 0) {
            var2.a(var2.c() - 1);
            if(var2.c_()) {
               var0.o().q("BlockEntityTag");
               if(var0.o().c_()) {
                  var0.d((NBTTagCompound)null);
               }
            }

         }
      }
   }

   public static enum EnumBannerPatternType {
      BASE("base", "b"),
      SQUARE_BOTTOM_LEFT("square_bottom_left", "bl", "   ", "   ", "#  "),
      SQUARE_BOTTOM_RIGHT("square_bottom_right", "br", "   ", "   ", "  #"),
      SQUARE_TOP_LEFT("square_top_left", "tl", "#  ", "   ", "   "),
      SQUARE_TOP_RIGHT("square_top_right", "tr", "  #", "   ", "   "),
      STRIPE_BOTTOM("stripe_bottom", "bs", "   ", "   ", "###"),
      STRIPE_TOP("stripe_top", "ts", "###", "   ", "   "),
      STRIPE_LEFT("stripe_left", "ls", "#  ", "#  ", "#  "),
      STRIPE_RIGHT("stripe_right", "rs", "  #", "  #", "  #"),
      STRIPE_CENTER("stripe_center", "cs", " # ", " # ", " # "),
      STRIPE_MIDDLE("stripe_middle", "ms", "   ", "###", "   "),
      STRIPE_DOWNRIGHT("stripe_downright", "drs", "#  ", " # ", "  #"),
      STRIPE_DOWNLEFT("stripe_downleft", "dls", "  #", " # ", "#  "),
      STRIPE_SMALL("small_stripes", "ss", "# #", "# #", "   "),
      CROSS("cross", "cr", "# #", " # ", "# #"),
      STRAIGHT_CROSS("straight_cross", "sc", " # ", "###", " # "),
      TRIANGLE_BOTTOM("triangle_bottom", "bt", "   ", " # ", "# #"),
      TRIANGLE_TOP("triangle_top", "tt", "# #", " # ", "   "),
      TRIANGLES_BOTTOM("triangles_bottom", "bts", "   ", "# #", " # "),
      TRIANGLES_TOP("triangles_top", "tts", " # ", "# #", "   "),
      DIAGONAL_LEFT("diagonal_left", "ld", "## ", "#  ", "   "),
      DIAGONAL_RIGHT("diagonal_up_right", "rd", "   ", "  #", " ##"),
      DIAGONAL_LEFT_MIRROR("diagonal_up_left", "lud", "   ", "#  ", "## "),
      DIAGONAL_RIGHT_MIRROR("diagonal_right", "rud", " ##", "  #", "   "),
      CIRCLE_MIDDLE("circle", "mc", "   ", " # ", "   "),
      RHOMBUS_MIDDLE("rhombus", "mr", " # ", "# #", " # "),
      HALF_VERTICAL("half_vertical", "vh", "## ", "## ", "## "),
      HALF_HORIZONTAL("half_horizontal", "hh", "###", "###", "   "),
      HALF_VERTICAL_MIRROR("half_vertical_right", "vhr", " ##", " ##", " ##"),
      HALF_HORIZONTAL_MIRROR("half_horizontal_bottom", "hhb", "   ", "###", "###"),
      BORDER("border", "bo", "###", "# #", "###"),
      CURLY_BORDER("curly_border", "cbo", new ItemStack(Blocks.bn)),
      CREEPER("creeper", "cre", new ItemStack(Items.ch, 1, 4)),
      GRADIENT("gradient", "gra", "# #", " # ", " # "),
      GRADIENT_UP("gradient_up", "gru", " # ", " # ", "# #"),
      BRICKS("bricks", "bri", new ItemStack(Blocks.V)),
      SKULL("skull", "sku", new ItemStack(Items.ch, 1, 1)),
      FLOWER("flower", "flo", new ItemStack(Blocks.O, 1, BlockFlowers.EnumFlowerVarient.OXEYE_DAISY.b())),
      MOJANG("mojang", "moj", new ItemStack(Items.aq, 1, 1));

      private String N;
      private String O;
      private String[] P;
      private ItemStack Q;

      private EnumBannerPatternType(String var3, String var4) {
         this.P = new String[3];
         this.N = var3;
         this.O = var4;
      }

      private EnumBannerPatternType(String var3, String var4, ItemStack var5) {
         this(var3, var4);
         this.Q = var5;
      }

      private EnumBannerPatternType(String var3, String var4, String var5, String var6, String var7) {
         this(var3, var4);
         this.P[0] = var5;
         this.P[1] = var6;
         this.P[2] = var7;
      }

      public String b() {
         return this.O;
      }

      public String[] c() {
         return this.P;
      }

      public boolean d() {
         return this.Q != null || this.P[0] != null;
      }

      public boolean e() {
         return this.Q != null;
      }

      public ItemStack f() {
         return this.Q;
      }
   }
}
