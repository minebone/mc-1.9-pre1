package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aoo;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_rz;

public class class_acb extends Item {
   protected final Block a;

   public class_acb(Block var1) {
      this.a = var1;
   }

   public class_acb b(String var1) {
      super.c(var1);
      return this;
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      IBlockData var10 = var3.getType(var4);
      Block var11 = var10.getBlock();
      if(!var11.a((class_ahw)var3, (BlockPosition)var4)) {
         var4 = var4.a(var6);
      }

      if(var1.b != 0 && var2.a(var4, var6, var1) && var3.a(this.a, var4, false, var6, (Entity)null, var1)) {
         int var12 = this.a(var1.i());
         IBlockData var13 = this.a.a(var3, var4, var6, var7, var8, var9, var12, var2);
         if(var3.a((BlockPosition)var4, (IBlockData)var13, 11)) {
            var13 = var3.getType(var4);
            if(var13.getBlock() == this.a) {
               a(var3, var2, var4, var1);
               this.a.a((World)var3, (BlockPosition)var4, (IBlockData)var13, (class_rz)var2, (ItemStack)var1);
            }

            class_aoo var14 = this.a.w();
            var3.a(var2, var4, var14.e(), EnumSoundCategory.BLOCKS, (var14.a() + 1.0F) / 2.0F, var14.b() * 0.8F);
            --var1.b;
         }

         return EnumResult.SUCCESS;
      } else {
         return EnumResult.FAIL;
      }
   }

   public static boolean a(World var0, EntityHuman var1, BlockPosition var2, ItemStack var3) {
      MinecraftServer var4 = var0.u();
      if(var4 == null) {
         return false;
      } else {
         if(var3.n() && var3.o().b("BlockEntityTag", 10)) {
            TileEntity var5 = var0.r(var2);
            if(var5 != null) {
               if(!var0.E && var5.B() && (var1 == null || !var4.getPlayerList().h(var1.getProfile()))) {
                  return false;
               }

               NBTTagCompound var6 = new NBTTagCompound();
               NBTTagCompound var7 = (NBTTagCompound)var6.b();
               var5.a(var6);
               NBTTagCompound var8 = (NBTTagCompound)var3.o().c("BlockEntityTag");
               var6.a(var8);
               var6.a("x", var2.p());
               var6.a("y", var2.q());
               var6.a("z", var2.r());
               if(!var6.equals(var7)) {
                  var5.a(var4, var6);
                  var5.v_();
                  return true;
               }
            }
         }

         return false;
      }
   }

   public String f_(ItemStack var1) {
      return this.a.a();
   }

   public String a() {
      return this.a.a();
   }

   public Block d() {
      return this.a;
   }

   // $FF: synthetic method
   public Item c(String var1) {
      return this.b(var1);
   }
}
