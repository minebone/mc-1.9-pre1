package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import java.util.Random;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_kk;

public class class_bat implements class_baq {
   private final boolean a;

   public class_bat(boolean var1) {
      this.a = var1;
   }

   public boolean a(Random var1, class_azy var2) {
      boolean var3 = var2.b() != null;
      return var3 == !this.a;
   }

   public static class class_a_in_class_bat extends class_baq.class_a_in_class_baq {
      protected class_a_in_class_bat() {
         super(new class_kk("killed_by_player"), class_bat.class);
      }

      public void a(JsonObject var1, class_bat var2, JsonSerializationContext var3) {
         var1.addProperty("inverse", Boolean.valueOf(var2.a));
      }

      public class_bat a(JsonObject var1, JsonDeserializationContext var2) {
         return new class_bat(ChatDeserializer.a(var1, "inverse", false));
      }

      // $FF: synthetic method
      public class_baq b(JsonObject var1, JsonDeserializationContext var2) {
         return this.a(var1, var2);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(JsonObject var1, class_baq var2, JsonSerializationContext var3) {
         this.a(var1, (class_bat)var2, var3);
      }
   }
}
