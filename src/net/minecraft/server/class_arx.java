package net.minecraft.server;

import java.util.List;
import net.minecraft.server.Chunk;
import net.minecraft.server.EnumCreatureType;
import net.minecraft.server.World;
import net.minecraft.server.BlockPosition;

public interface class_arx {
   Chunk a(int var1, int var2);

   void b(int var1, int var2);

   boolean a(Chunk var1, int var2, int var3);

   List a(EnumCreatureType var1, BlockPosition var2);

   BlockPosition a(World var1, String var2, BlockPosition var3);

   void b(Chunk var1, int var2, int var3);
}
