package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Enchantment;
import net.minecraft.server.Entity;
import net.minecraft.server.ItemArmor;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agm;
import net.minecraft.server.class_agn;
import net.minecraft.server.class_agp;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;

public class class_agy extends Enchantment {
   public class_agy(Enchantment.class_a_in_class_agl var1, EnumInventorySlot... var2) {
      super(var1, class_agm.ARMOR_CHEST, var2);
      this.c("thorns");
   }

   public int a(int var1) {
      return 10 + 20 * (var1 - 1);
   }

   public int b(int var1) {
      return super.a(var1) + 50;
   }

   public int b() {
      return 3;
   }

   public boolean a(ItemStack var1) {
      return var1.b() instanceof ItemArmor?true:super.a(var1);
   }

   public void b(class_rz var1, Entity var2, int var3) {
      Random var4 = var1.bE();
      ItemStack var5 = class_agn.b(class_agp.h, var1);
      if(a(var3, var4)) {
         if(var2 != null) {
            var2.a(DamageSource.a((Entity)var1), (float)b(var3, var4));
         }

         if(var5 != null) {
            var5.a(3, (class_rz)var1);
         }
      } else if(var5 != null) {
         var5.a(1, (class_rz)var1);
      }

   }

   public static boolean a(int var0, Random var1) {
      return var0 <= 0?false:var1.nextFloat() < 0.15F * (float)var0;
   }

   public static int b(int var0, Random var1) {
      return var0 > 10?var0 - 10:1 + var1.nextInt(4);
   }
}
