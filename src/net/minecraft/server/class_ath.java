package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.Blocks;
import net.minecraft.server.Chunk;
import net.minecraft.server.EnumCreatureType;
import net.minecraft.server.StructureGenerator;
import net.minecraft.server.World;
import net.minecraft.server.WorldGenDungeons;
import net.minecraft.server.WorldGenFlatInfo;
import net.minecraft.server.WorldGenFlatLayerInfo;
import net.minecraft.server.WorldGenLargeFeature;
import net.minecraft.server.WorldGenMineshaft;
import net.minecraft.server.WorldGenMonument;
import net.minecraft.server.WorldGenStronghold;
import net.minecraft.server.WorldGenVillage;
import net.minecraft.server.class_ahm;
import net.minecraft.server.class_aik;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arx;
import net.minecraft.server.class_ate;
import net.minecraft.server.class_atj;
import net.minecraft.server.class_aul;
import net.minecraft.server.BlockPosition;

public class class_ath implements class_arx {
   private final World a;
   private final Random b;
   private final IBlockData[] c = new IBlockData[256];
   private final WorldGenFlatInfo d;
   private final List e = Lists.newArrayList();
   private final boolean f;
   private final boolean g;
   private class_aul h;
   private class_aul i;

   public class_ath(World var1, long var2, boolean var4, String var5) {
      this.a = var1;
      this.b = new Random(var2);
      this.d = WorldGenFlatInfo.a(var5);
      if(var4) {
         Map var6 = this.d.b();
         if(var6.containsKey("village")) {
            Map var7 = (Map)var6.get("village");
            if(!var7.containsKey("size")) {
               var7.put("size", "1");
            }

            this.e.add(new WorldGenVillage(var7));
         }

         if(var6.containsKey("biome_1")) {
            this.e.add(new WorldGenLargeFeature((Map)var6.get("biome_1")));
         }

         if(var6.containsKey("mineshaft")) {
            this.e.add(new WorldGenMineshaft((Map)var6.get("mineshaft")));
         }

         if(var6.containsKey("stronghold")) {
            this.e.add(new WorldGenStronghold((Map)var6.get("stronghold")));
         }

         if(var6.containsKey("oceanmonument")) {
            this.e.add(new WorldGenMonument((Map)var6.get("oceanmonument")));
         }
      }

      if(this.d.b().containsKey("lake")) {
         this.h = new class_aul(Blocks.j);
      }

      if(this.d.b().containsKey("lava_lake")) {
         this.i = new class_aul(Blocks.l);
      }

      this.g = this.d.b().containsKey("dungeon");
      int var13 = 0;
      int var14 = 0;
      boolean var8 = true;
      Iterator var9 = this.d.c().iterator();

      while(var9.hasNext()) {
         WorldGenFlatLayerInfo var10 = (WorldGenFlatLayerInfo)var9.next();

         for(int var11 = var10.d(); var11 < var10.d() + var10.b(); ++var11) {
            IBlockData var12 = var10.c();
            if(var12.getBlock() != Blocks.AIR) {
               var8 = false;
               this.c[var11] = var12;
            }
         }

         if(var10.c().getBlock() == Blocks.AIR) {
            var14 += var10.b();
         } else {
            var13 += var10.b() + var14;
            var14 = 0;
         }
      }

      var1.b(var13);
      this.f = var8 && this.d.a() != BiomeBase.a(class_aik.P)?false:this.d.b().containsKey("decoration");
   }

   public Chunk a(int var1, int var2) {
      class_ate var3 = new class_ate();

      int var7;
      for(int var4 = 0; var4 < this.c.length; ++var4) {
         IBlockData var5 = this.c[var4];
         if(var5 != null) {
            for(int var6 = 0; var6 < 16; ++var6) {
               for(var7 = 0; var7 < 16; ++var7) {
                  var3.a(var6, var4, var7, var5);
               }
            }
         }
      }

      Iterator var8 = this.e.iterator();

      while(var8.hasNext()) {
         class_atj var10 = (class_atj)var8.next();
         var10.a(this.a, var1, var2, var3);
      }

      Chunk var9 = new Chunk(this.a, var3, var1, var2);
      BiomeBase[] var11 = this.a.A().b((BiomeBase[])null, var1 * 16, var2 * 16, 16, 16);
      byte[] var12 = var9.l();

      for(var7 = 0; var7 < var12.length; ++var7) {
         var12[var7] = (byte)BiomeBase.a(var11[var7]);
      }

      var9.b();
      return var9;
   }

   public void b(int var1, int var2) {
      int var3 = var1 * 16;
      int var4 = var2 * 16;
      BlockPosition var5 = new BlockPosition(var3, 0, var4);
      BiomeBase var6 = this.a.b(new BlockPosition(var3 + 16, 0, var4 + 16));
      boolean var7 = false;
      this.b.setSeed(this.a.O());
      long var8 = this.b.nextLong() / 2L * 2L + 1L;
      long var10 = this.b.nextLong() / 2L * 2L + 1L;
      this.b.setSeed((long)var1 * var8 + (long)var2 * var10 ^ this.a.O());
      class_ahm var12 = new class_ahm(var1, var2);
      Iterator var13 = this.e.iterator();

      while(var13.hasNext()) {
         StructureGenerator var14 = (StructureGenerator)var13.next();
         boolean var15 = var14.a(this.a, this.b, var12);
         if(var14 instanceof WorldGenVillage) {
            var7 |= var15;
         }
      }

      if(this.h != null && !var7 && this.b.nextInt(4) == 0) {
         this.h.b(this.a, this.b, var5.a(this.b.nextInt(16) + 8, this.b.nextInt(256), this.b.nextInt(16) + 8));
      }

      if(this.i != null && !var7 && this.b.nextInt(8) == 0) {
         BlockPosition var16 = var5.a(this.b.nextInt(16) + 8, this.b.nextInt(this.b.nextInt(248) + 8), this.b.nextInt(16) + 8);
         if(var16.q() < this.a.K() || this.b.nextInt(10) == 0) {
            this.i.b(this.a, this.b, var16);
         }
      }

      if(this.g) {
         for(int var17 = 0; var17 < 8; ++var17) {
            (new WorldGenDungeons()).b(this.a, this.b, var5.a(this.b.nextInt(16) + 8, this.b.nextInt(256), this.b.nextInt(16) + 8));
         }
      }

      if(this.f) {
         var6.a(this.a, this.b, var5);
      }

   }

   public boolean a(Chunk var1, int var2, int var3) {
      return false;
   }

   public List a(EnumCreatureType var1, BlockPosition var2) {
      BiomeBase var3 = this.a.b(var2);
      return var3.a(var1);
   }

   public BlockPosition a(World var1, String var2, BlockPosition var3) {
      if("Stronghold".equals(var2)) {
         Iterator var4 = this.e.iterator();

         while(var4.hasNext()) {
            StructureGenerator var5 = (StructureGenerator)var4.next();
            if(var5 instanceof WorldGenStronghold) {
               return var5.b(var1, var3);
            }
         }
      }

      return null;
   }

   public void b(Chunk var1, int var2, int var3) {
      Iterator var4 = this.e.iterator();

      while(var4.hasNext()) {
         StructureGenerator var5 = (StructureGenerator)var4.next();
         var5.a(this.a, var2, var3, (class_ate)null);
      }

   }
}
