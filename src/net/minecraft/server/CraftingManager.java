package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.Block;
import net.minecraft.server.BlockCobbleWall;
import net.minecraft.server.BlockDoubleStepAbstract;
import net.minecraft.server.BlockDoubleStoneStepAbstract;
import net.minecraft.server.BlockStone;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumColor;
import net.minecraft.server.InventoryCrafting;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.RecipeFireworks;
import net.minecraft.server.RecipesBanner;
import net.minecraft.server.RecipesFood;
import net.minecraft.server.RecipesTools;
import net.minecraft.server.RecipesWeapons;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_afi;
import net.minecraft.server.class_afj;
import net.minecraft.server.class_afl;
import net.minecraft.server.class_afm;
import net.minecraft.server.class_afq;
import net.minecraft.server.class_afr;
import net.minecraft.server.class_afs;
import net.minecraft.server.class_aft;
import net.minecraft.server.class_afv;
import net.minecraft.server.class_afw;
import net.minecraft.server.class_afx;
import net.minecraft.server.class_afy;
import net.minecraft.server.class_afz;
import net.minecraft.server.class_aga;

public class CraftingManager {
   private static final CraftingManager a = new CraftingManager();
   private final List b = Lists.newArrayList();

   public static CraftingManager a() {
      return a;
   }

   private CraftingManager() {
      (new RecipesTools()).a(this);
      (new RecipesWeapons()).a(this);
      (new class_afs()).a(this);
      (new RecipesFood()).a(this);
      (new class_afz()).a(this);
      (new class_afj()).a(this);
      (new class_afm()).a(this);
      this.b.add(new class_afi());
      this.b.add(new class_afl());
      this.b.add(new class_afq());
      this.b.add(new class_afr());
      this.b.add(new RecipeFireworks());
      this.b.add(new class_afv());
      this.b.add(new class_aga());
      (new RecipesBanner()).a(this);
      (new class_afy()).a(this);
      this.a(new ItemStack(Items.aR, 3), new Object[]{"###", Character.valueOf('#'), Items.aQ});
      this.b(new ItemStack(Items.aS, 1), new Object[]{Items.aR, Items.aR, Items.aR, Items.aM});
      this.b(new ItemStack(Items.bW, 1), new Object[]{Items.aS, new ItemStack(Items.bd, 1, EnumColor.BLACK.b()), Items.I});
      this.a(new ItemStack(Blocks.aO, 3), new Object[]{"W#W", "W#W", Character.valueOf('#'), Items.A, Character.valueOf('W'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.OAK.a())});
      this.a(new ItemStack(Blocks.aQ, 3), new Object[]{"W#W", "W#W", Character.valueOf('#'), Items.A, Character.valueOf('W'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.BIRCH.a())});
      this.a(new ItemStack(Blocks.aP, 3), new Object[]{"W#W", "W#W", Character.valueOf('#'), Items.A, Character.valueOf('W'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.SPRUCE.a())});
      this.a(new ItemStack(Blocks.aR, 3), new Object[]{"W#W", "W#W", Character.valueOf('#'), Items.A, Character.valueOf('W'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.JUNGLE.a())});
      this.a(new ItemStack(Blocks.aT, 3), new Object[]{"W#W", "W#W", Character.valueOf('#'), Items.A, Character.valueOf('W'), new ItemStack(Blocks.f, 1, 4 + BlockWood.EnumLogVariant.ACACIA.a() - 4)});
      this.a(new ItemStack(Blocks.aS, 3), new Object[]{"W#W", "W#W", Character.valueOf('#'), Items.A, Character.valueOf('W'), new ItemStack(Blocks.f, 1, 4 + BlockWood.EnumLogVariant.DARK_OAK.a() - 4)});
      this.a(new ItemStack(Blocks.bZ, 6, BlockCobbleWall.EnumCobbleVariant.NORMAL.a()), new Object[]{"###", "###", Character.valueOf('#'), Blocks.e});
      this.a(new ItemStack(Blocks.bZ, 6, BlockCobbleWall.EnumCobbleVariant.MOSSY.a()), new Object[]{"###", "###", Character.valueOf('#'), Blocks.Y});
      this.a(new ItemStack(Blocks.bz, 6), new Object[]{"###", "###", Character.valueOf('#'), Blocks.by});
      this.a(new ItemStack(Blocks.bo, 1), new Object[]{"#W#", "#W#", Character.valueOf('#'), Items.A, Character.valueOf('W'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.OAK.a())});
      this.a(new ItemStack(Blocks.bq, 1), new Object[]{"#W#", "#W#", Character.valueOf('#'), Items.A, Character.valueOf('W'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.BIRCH.a())});
      this.a(new ItemStack(Blocks.bp, 1), new Object[]{"#W#", "#W#", Character.valueOf('#'), Items.A, Character.valueOf('W'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.SPRUCE.a())});
      this.a(new ItemStack(Blocks.br, 1), new Object[]{"#W#", "#W#", Character.valueOf('#'), Items.A, Character.valueOf('W'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.JUNGLE.a())});
      this.a(new ItemStack(Blocks.bt, 1), new Object[]{"#W#", "#W#", Character.valueOf('#'), Items.A, Character.valueOf('W'), new ItemStack(Blocks.f, 1, 4 + BlockWood.EnumLogVariant.ACACIA.a() - 4)});
      this.a(new ItemStack(Blocks.bs, 1), new Object[]{"#W#", "#W#", Character.valueOf('#'), Items.A, Character.valueOf('W'), new ItemStack(Blocks.f, 1, 4 + BlockWood.EnumLogVariant.DARK_OAK.a() - 4)});
      this.a(new ItemStack(Blocks.aN, 1), new Object[]{"###", "#X#", "###", Character.valueOf('#'), Blocks.f, Character.valueOf('X'), Items.k});
      this.a(new ItemStack(Items.cx, 2), new Object[]{"~~ ", "~O ", "  ~", Character.valueOf('~'), Items.H, Character.valueOf('O'), Items.aT});
      this.a(new ItemStack(Blocks.B, 1), new Object[]{"###", "#X#", "###", Character.valueOf('#'), Blocks.f, Character.valueOf('X'), Items.aE});
      this.a(new ItemStack(Blocks.X, 1), new Object[]{"###", "XXX", "###", Character.valueOf('#'), Blocks.f, Character.valueOf('X'), Items.aS});
      this.a(new ItemStack(Blocks.aJ, 1), new Object[]{"##", "##", Character.valueOf('#'), Items.aF});
      this.a(new ItemStack(Blocks.aH, 6), new Object[]{"###", Character.valueOf('#'), Blocks.aJ});
      this.a(new ItemStack(Blocks.aL, 1), new Object[]{"##", "##", Character.valueOf('#'), Items.aP});
      this.a(new ItemStack(Blocks.V, 1), new Object[]{"##", "##", Character.valueOf('#'), Items.aO});
      this.a(new ItemStack(Blocks.aX, 1), new Object[]{"##", "##", Character.valueOf('#'), Items.ba});
      this.a(new ItemStack(Blocks.cq, 1), new Object[]{"##", "##", Character.valueOf('#'), Items.cq});
      this.a(new ItemStack(Blocks.L, 1), new Object[]{"##", "##", Character.valueOf('#'), Items.H});
      this.a(new ItemStack(Blocks.W, 1), new Object[]{"X#X", "#X#", "X#X", Character.valueOf('X'), Items.J, Character.valueOf('#'), Blocks.m});
      this.a(new ItemStack(Blocks.U, 6, BlockDoubleStepAbstract.EnumStoneSlabVariant.COBBLESTONE.a()), new Object[]{"###", Character.valueOf('#'), Blocks.e});
      this.a(new ItemStack(Blocks.U, 6, BlockDoubleStepAbstract.EnumStoneSlabVariant.STONE.a()), new Object[]{"###", Character.valueOf('#'), new ItemStack(Blocks.b, 1, BlockStone.EnumStoneVariant.STONE.a())});
      this.a(new ItemStack(Blocks.U, 6, BlockDoubleStepAbstract.EnumStoneSlabVariant.SAND.a()), new Object[]{"###", Character.valueOf('#'), Blocks.A});
      this.a(new ItemStack(Blocks.U, 6, BlockDoubleStepAbstract.EnumStoneSlabVariant.BRICK.a()), new Object[]{"###", Character.valueOf('#'), Blocks.V});
      this.a(new ItemStack(Blocks.U, 6, BlockDoubleStepAbstract.EnumStoneSlabVariant.SMOOTHBRICK.a()), new Object[]{"###", Character.valueOf('#'), Blocks.bf});
      this.a(new ItemStack(Blocks.U, 6, BlockDoubleStepAbstract.EnumStoneSlabVariant.NETHERBRICK.a()), new Object[]{"###", Character.valueOf('#'), Blocks.by});
      this.a(new ItemStack(Blocks.U, 6, BlockDoubleStepAbstract.EnumStoneSlabVariant.QUARTZ.a()), new Object[]{"###", Character.valueOf('#'), Blocks.cq});
      this.a(new ItemStack(Blocks.cP, 6, BlockDoubleStoneStepAbstract.EnumStoneSlab2Variant.RED_SANDSTONE.a()), new Object[]{"###", Character.valueOf('#'), Blocks.cM});
      this.a(new ItemStack(Blocks.cX, 6, 0), new Object[]{"###", Character.valueOf('#'), Blocks.cT});
      this.a(new ItemStack(Blocks.bM, 6, 0), new Object[]{"###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.OAK.a())});
      this.a(new ItemStack(Blocks.bM, 6, BlockWood.EnumLogVariant.BIRCH.a()), new Object[]{"###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.BIRCH.a())});
      this.a(new ItemStack(Blocks.bM, 6, BlockWood.EnumLogVariant.SPRUCE.a()), new Object[]{"###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.SPRUCE.a())});
      this.a(new ItemStack(Blocks.bM, 6, BlockWood.EnumLogVariant.JUNGLE.a()), new Object[]{"###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.JUNGLE.a())});
      this.a(new ItemStack(Blocks.bM, 6, 4 + BlockWood.EnumLogVariant.ACACIA.a() - 4), new Object[]{"###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, 4 + BlockWood.EnumLogVariant.ACACIA.a() - 4)});
      this.a(new ItemStack(Blocks.bM, 6, 4 + BlockWood.EnumLogVariant.DARK_OAK.a() - 4), new Object[]{"###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, 4 + BlockWood.EnumLogVariant.DARK_OAK.a() - 4)});
      this.a(new ItemStack(Blocks.au, 3), new Object[]{"# #", "###", "# #", Character.valueOf('#'), Items.A});
      this.a(new ItemStack(Items.as, 3), new Object[]{"##", "##", "##", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.OAK.a())});
      this.a(new ItemStack(Items.at, 3), new Object[]{"##", "##", "##", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.SPRUCE.a())});
      this.a(new ItemStack(Items.au, 3), new Object[]{"##", "##", "##", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.BIRCH.a())});
      this.a(new ItemStack(Items.av, 3), new Object[]{"##", "##", "##", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.JUNGLE.a())});
      this.a(new ItemStack(Items.aw, 3), new Object[]{"##", "##", "##", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.ACACIA.a())});
      this.a(new ItemStack(Items.ax, 3), new Object[]{"##", "##", "##", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.DARK_OAK.a())});
      this.a(new ItemStack(Blocks.bd, 2), new Object[]{"###", "###", Character.valueOf('#'), Blocks.f});
      this.a(new ItemStack(Items.aD, 3), new Object[]{"##", "##", "##", Character.valueOf('#'), Items.l});
      this.a(new ItemStack(Blocks.cw, 1), new Object[]{"##", "##", Character.valueOf('#'), Items.l});
      this.a(new ItemStack(Items.ar, 3), new Object[]{"###", "###", " X ", Character.valueOf('#'), Blocks.f, Character.valueOf('X'), Items.A});
      this.a(new ItemStack(Items.bg, 1), new Object[]{"AAA", "BEB", "CCC", Character.valueOf('A'), Items.aN, Character.valueOf('B'), Items.bf, Character.valueOf('C'), Items.Q, Character.valueOf('E'), Items.aW});
      this.a(new ItemStack(Items.bf, 1), new Object[]{"#", Character.valueOf('#'), Items.aQ});
      this.a(new ItemStack(Blocks.f, 4, BlockWood.EnumLogVariant.OAK.a()), new Object[]{"#", Character.valueOf('#'), new ItemStack(Blocks.r, 1, BlockWood.EnumLogVariant.OAK.a())});
      this.a(new ItemStack(Blocks.f, 4, BlockWood.EnumLogVariant.SPRUCE.a()), new Object[]{"#", Character.valueOf('#'), new ItemStack(Blocks.r, 1, BlockWood.EnumLogVariant.SPRUCE.a())});
      this.a(new ItemStack(Blocks.f, 4, BlockWood.EnumLogVariant.BIRCH.a()), new Object[]{"#", Character.valueOf('#'), new ItemStack(Blocks.r, 1, BlockWood.EnumLogVariant.BIRCH.a())});
      this.a(new ItemStack(Blocks.f, 4, BlockWood.EnumLogVariant.JUNGLE.a()), new Object[]{"#", Character.valueOf('#'), new ItemStack(Blocks.r, 1, BlockWood.EnumLogVariant.JUNGLE.a())});
      this.a(new ItemStack(Blocks.f, 4, 4 + BlockWood.EnumLogVariant.ACACIA.a() - 4), new Object[]{"#", Character.valueOf('#'), new ItemStack(Blocks.s, 1, BlockWood.EnumLogVariant.ACACIA.a() - 4)});
      this.a(new ItemStack(Blocks.f, 4, 4 + BlockWood.EnumLogVariant.DARK_OAK.a() - 4), new Object[]{"#", Character.valueOf('#'), new ItemStack(Blocks.s, 1, BlockWood.EnumLogVariant.DARK_OAK.a() - 4)});
      this.a(new ItemStack(Items.A, 4), new Object[]{"#", "#", Character.valueOf('#'), Blocks.f});
      this.a(new ItemStack(Blocks.aa, 4), new Object[]{"X", "#", Character.valueOf('X'), Items.j, Character.valueOf('#'), Items.A});
      this.a(new ItemStack(Blocks.aa, 4), new Object[]{"X", "#", Character.valueOf('X'), new ItemStack(Items.j, 1, 1), Character.valueOf('#'), Items.A});
      this.a(new ItemStack(Items.B, 4), new Object[]{"# #", " # ", Character.valueOf('#'), Blocks.f});
      this.a(new ItemStack(Items.bJ, 3), new Object[]{"# #", " # ", Character.valueOf('#'), Blocks.w});
      this.a(new ItemStack(Blocks.av, 16), new Object[]{"X X", "X#X", "X X", Character.valueOf('X'), Items.l, Character.valueOf('#'), Items.A});
      this.a(new ItemStack(Blocks.D, 6), new Object[]{"X X", "X#X", "XRX", Character.valueOf('X'), Items.m, Character.valueOf('R'), Items.aE, Character.valueOf('#'), Items.A});
      this.a(new ItemStack(Blocks.cs, 6), new Object[]{"XSX", "X#X", "XSX", Character.valueOf('X'), Items.l, Character.valueOf('#'), Blocks.aF, Character.valueOf('S'), Items.A});
      this.a(new ItemStack(Blocks.E, 6), new Object[]{"X X", "X#X", "XRX", Character.valueOf('X'), Items.l, Character.valueOf('R'), Items.aE, Character.valueOf('#'), Blocks.az});
      this.a(new ItemStack(Items.aB, 1), new Object[]{"# #", "###", Character.valueOf('#'), Items.l});
      this.a(new ItemStack(Items.bQ, 1), new Object[]{"# #", "# #", "###", Character.valueOf('#'), Items.l});
      this.a(new ItemStack(Items.bP, 1), new Object[]{" B ", "###", Character.valueOf('#'), Blocks.e, Character.valueOf('B'), Items.bC});
      this.a(new ItemStack(Blocks.aZ, 1), new Object[]{"A", "B", Character.valueOf('A'), Blocks.aU, Character.valueOf('B'), Blocks.aa});
      this.a(new ItemStack(Items.aU, 1), new Object[]{"A", "B", Character.valueOf('A'), Blocks.ae, Character.valueOf('B'), Items.aB});
      this.a(new ItemStack(Items.aV, 1), new Object[]{"A", "B", Character.valueOf('A'), Blocks.al, Character.valueOf('B'), Items.aB});
      this.a(new ItemStack(Items.cr, 1), new Object[]{"A", "B", Character.valueOf('A'), Blocks.W, Character.valueOf('B'), Items.aB});
      this.a(new ItemStack(Items.cs, 1), new Object[]{"A", "B", Character.valueOf('A'), Blocks.cp, Character.valueOf('B'), Items.aB});
      this.a(new ItemStack(Items.aG, 1), new Object[]{"# #", "###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.OAK.a())});
      this.a(new ItemStack(Items.aH, 1), new Object[]{"# #", "###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.SPRUCE.a())});
      this.a(new ItemStack(Items.aI, 1), new Object[]{"# #", "###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.BIRCH.a())});
      this.a(new ItemStack(Items.aJ, 1), new Object[]{"# #", "###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.JUNGLE.a())});
      this.a(new ItemStack(Items.aK, 1), new Object[]{"# #", "###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.ACACIA.a())});
      this.a(new ItemStack(Items.aL, 1), new Object[]{"# #", "###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.DARK_OAK.a())});
      this.a(new ItemStack(Items.ay, 1), new Object[]{"# #", " # ", Character.valueOf('#'), Items.l});
      this.a(new ItemStack(Items.ca, 1), new Object[]{"# #", " # ", Character.valueOf('#'), Items.aO});
      this.b(new ItemStack(Items.d, 1), new Object[]{new ItemStack(Items.l, 1), new ItemStack(Items.am, 1)});
      this.a(new ItemStack(Items.R, 1), new Object[]{"###", Character.valueOf('#'), Items.Q});
      this.a(new ItemStack(Blocks.ad, 4), new Object[]{"#  ", "## ", "###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.OAK.a())});
      this.a(new ItemStack(Blocks.bV, 4), new Object[]{"#  ", "## ", "###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.BIRCH.a())});
      this.a(new ItemStack(Blocks.bU, 4), new Object[]{"#  ", "## ", "###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.SPRUCE.a())});
      this.a(new ItemStack(Blocks.bW, 4), new Object[]{"#  ", "## ", "###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, BlockWood.EnumLogVariant.JUNGLE.a())});
      this.a(new ItemStack(Blocks.cC, 4), new Object[]{"#  ", "## ", "###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, 4 + BlockWood.EnumLogVariant.ACACIA.a() - 4)});
      this.a(new ItemStack(Blocks.cD, 4), new Object[]{"#  ", "## ", "###", Character.valueOf('#'), new ItemStack(Blocks.f, 1, 4 + BlockWood.EnumLogVariant.DARK_OAK.a() - 4)});
      this.a(new ItemStack(Items.aY, 1), new Object[]{"  #", " #X", "# X", Character.valueOf('#'), Items.A, Character.valueOf('X'), Items.H});
      this.a(new ItemStack(Items.ci, 1), new Object[]{"# ", " X", Character.valueOf('#'), Items.aY, Character.valueOf('X'), Items.cb});
      this.a(new ItemStack(Blocks.aw, 4), new Object[]{"#  ", "## ", "###", Character.valueOf('#'), Blocks.e});
      this.a(new ItemStack(Blocks.bu, 4), new Object[]{"#  ", "## ", "###", Character.valueOf('#'), Blocks.V});
      this.a(new ItemStack(Blocks.bv, 4), new Object[]{"#  ", "## ", "###", Character.valueOf('#'), Blocks.bf});
      this.a(new ItemStack(Blocks.bA, 4), new Object[]{"#  ", "## ", "###", Character.valueOf('#'), Blocks.by});
      this.a(new ItemStack(Blocks.bO, 4), new Object[]{"#  ", "## ", "###", Character.valueOf('#'), Blocks.A});
      this.a(new ItemStack(Blocks.cN, 4), new Object[]{"#  ", "## ", "###", Character.valueOf('#'), Blocks.cM});
      this.a(new ItemStack(Blocks.cr, 4), new Object[]{"#  ", "## ", "###", Character.valueOf('#'), Blocks.cq});
      this.a(new ItemStack(Items.ap, 1), new Object[]{"###", "#X#", "###", Character.valueOf('#'), Items.A, Character.valueOf('X'), Blocks.L});
      this.a(new ItemStack(Items.bZ, 1), new Object[]{"###", "#X#", "###", Character.valueOf('#'), Items.A, Character.valueOf('X'), Items.aM});
      this.a(new ItemStack(Items.aq), new Object[]{"###", "#X#", "###", Character.valueOf('#'), Items.m, Character.valueOf('X'), Items.e});
      this.a(new ItemStack(Items.cg), new Object[]{"###", "#X#", "###", Character.valueOf('#'), Items.bE, Character.valueOf('X'), Items.cb});
      this.a(new ItemStack(Items.bS, 1), new Object[]{"###", "#X#", "###", Character.valueOf('#'), Items.bE, Character.valueOf('X'), Items.bm});
      this.a(new ItemStack(Blocks.ay, 1), new Object[]{"X", "#", Character.valueOf('#'), Blocks.e, Character.valueOf('X'), Items.A});
      this.a(new ItemStack(Blocks.bR, 2), new Object[]{"I", "S", "#", Character.valueOf('#'), Blocks.f, Character.valueOf('S'), Items.A, Character.valueOf('I'), Items.l});
      this.a(new ItemStack(Blocks.aF, 1), new Object[]{"X", "#", Character.valueOf('#'), Items.A, Character.valueOf('X'), Items.aE});
      this.a(new ItemStack(Items.bi, 1), new Object[]{"#X#", "III", Character.valueOf('#'), Blocks.aF, Character.valueOf('X'), Items.aE, Character.valueOf('I'), new ItemStack(Blocks.b, 1, BlockStone.EnumStoneVariant.STONE.a())});
      this.a(new ItemStack(Items.co, 1), new Object[]{" # ", "#X#", "III", Character.valueOf('#'), Blocks.aF, Character.valueOf('X'), Items.cq, Character.valueOf('I'), new ItemStack(Blocks.b, 1, BlockStone.EnumStoneVariant.STONE.a())});
      this.a(new ItemStack(Items.aZ, 1), new Object[]{" # ", "#X#", " # ", Character.valueOf('#'), Items.m, Character.valueOf('X'), Items.aE});
      this.a(new ItemStack(Items.aX, 1), new Object[]{" # ", "#X#", " # ", Character.valueOf('#'), Items.l, Character.valueOf('X'), Items.aE});
      this.a(new ItemStack(Items.cf, 1), new Object[]{"###", "#X#", "###", Character.valueOf('#'), Items.aR, Character.valueOf('X'), Items.aX});
      this.a(new ItemStack(Blocks.aG, 1), new Object[]{"#", Character.valueOf('#'), new ItemStack(Blocks.b, 1, BlockStone.EnumStoneVariant.STONE.a())});
      this.a(new ItemStack(Blocks.cd, 1), new Object[]{"#", Character.valueOf('#'), Blocks.f});
      this.a(new ItemStack(Blocks.az, 1), new Object[]{"##", Character.valueOf('#'), new ItemStack(Blocks.b, 1, BlockStone.EnumStoneVariant.STONE.a())});
      this.a(new ItemStack(Blocks.aB, 1), new Object[]{"##", Character.valueOf('#'), Blocks.f});
      this.a(new ItemStack(Blocks.ci, 1), new Object[]{"##", Character.valueOf('#'), Items.l});
      this.a(new ItemStack(Blocks.ch, 1), new Object[]{"##", Character.valueOf('#'), Items.m});
      this.a(new ItemStack(Blocks.z, 1), new Object[]{"###", "#X#", "#R#", Character.valueOf('#'), Blocks.e, Character.valueOf('X'), Items.f, Character.valueOf('R'), Items.aE});
      this.a(new ItemStack(Blocks.ct, 1), new Object[]{"###", "# #", "#R#", Character.valueOf('#'), Blocks.e, Character.valueOf('R'), Items.aE});
      this.a(new ItemStack(Blocks.J, 1), new Object[]{"TTT", "#X#", "#R#", Character.valueOf('#'), Blocks.e, Character.valueOf('X'), Items.l, Character.valueOf('R'), Items.aE, Character.valueOf('T'), Blocks.f});
      this.a(new ItemStack(Blocks.F, 1), new Object[]{"S", "P", Character.valueOf('S'), Items.aT, Character.valueOf('P'), Blocks.J});
      this.a(new ItemStack(Items.bh, 1), new Object[]{"###", "XXX", Character.valueOf('#'), Blocks.L, Character.valueOf('X'), Blocks.f});
      this.a(new ItemStack(Blocks.bC, 1), new Object[]{" B ", "D#D", "###", Character.valueOf('#'), Blocks.Z, Character.valueOf('B'), Items.aS, Character.valueOf('D'), Items.k});
      this.a(new ItemStack(Blocks.cf, 1), new Object[]{"III", " i ", "iii", Character.valueOf('I'), Blocks.S, Character.valueOf('i'), Items.l});
      this.a(new ItemStack(Items.aM), new Object[]{"##", "##", Character.valueOf('#'), Items.bz});
      this.b(new ItemStack(Items.bR, 1), new Object[]{Items.bB, Items.bN});
      this.b(new ItemStack(Items.bV, 3), new Object[]{Items.J, Items.bN, Items.j});
      this.b(new ItemStack(Items.bV, 3), new Object[]{Items.J, Items.bN, new ItemStack(Items.j, 1, 1)});
      this.a(new ItemStack(Blocks.cl), new Object[]{"GGG", "QQQ", "WWW", Character.valueOf('G'), Blocks.w, Character.valueOf('Q'), Items.cq, Character.valueOf('W'), Blocks.bM});
      this.a(new ItemStack(Items.cP), new Object[]{"GGG", "GEG", "GTG", Character.valueOf('G'), Blocks.w, Character.valueOf('E'), Items.bR, Character.valueOf('T'), Items.bD});
      this.a(new ItemStack(Blocks.cp), new Object[]{"I I", "ICI", " I ", Character.valueOf('I'), Items.l, Character.valueOf('C'), Blocks.ae});
      this.a(new ItemStack(Items.ct, 1), new Object[]{"///", " / ", "/_/", Character.valueOf('/'), Items.A, Character.valueOf('_'), new ItemStack(Blocks.U, 1, BlockDoubleStepAbstract.EnumStoneSlabVariant.STONE.a())});
      this.a(new ItemStack(Blocks.cQ, 4), new Object[]{"/", "#", Character.valueOf('/'), Items.bC, Character.valueOf('#'), Items.cT});
      Collections.sort(this.b, new Comparator() {
         public int a(class_aft var1, class_aft var2) {
            return var1 instanceof class_afx && var2 instanceof class_afw?1:(var2 instanceof class_afx && var1 instanceof class_afw?-1:(var2.a() < var1.a()?-1:(var2.a() > var1.a()?1:0)));
         }

         // $FF: synthetic method
         public int compare(Object var1, Object var2) {
            return this.a((class_aft)var1, (class_aft)var2);
         }
      });
   }

   public class_afw a(ItemStack var1, Object... var2) {
      String var3 = "";
      int var4 = 0;
      int var5 = 0;
      int var6 = 0;
      if(var2[var4] instanceof String[]) {
         String[] var11 = (String[])((String[])var2[var4++]);

         for(int var8 = 0; var8 < var11.length; ++var8) {
            String var9 = var11[var8];
            ++var6;
            var5 = var9.length();
            var3 = var3 + var9;
         }
      } else {
         while(var2[var4] instanceof String) {
            String var7 = (String)var2[var4++];
            ++var6;
            var5 = var7.length();
            var3 = var3 + var7;
         }
      }

      HashMap var12;
      for(var12 = Maps.newHashMap(); var4 < var2.length; var4 += 2) {
         Character var13 = (Character)var2[var4];
         ItemStack var15 = null;
         if(var2[var4 + 1] instanceof Item) {
            var15 = new ItemStack((Item)var2[var4 + 1]);
         } else if(var2[var4 + 1] instanceof Block) {
            var15 = new ItemStack((Block)var2[var4 + 1], 1, 32767);
         } else if(var2[var4 + 1] instanceof ItemStack) {
            var15 = (ItemStack)var2[var4 + 1];
         }

         var12.put(var13, var15);
      }

      ItemStack[] var14 = new ItemStack[var5 * var6];

      for(int var16 = 0; var16 < var5 * var6; ++var16) {
         char var10 = var3.charAt(var16);
         if(var12.containsKey(Character.valueOf(var10))) {
            var14[var16] = ((ItemStack)var12.get(Character.valueOf(var10))).k();
         } else {
            var14[var16] = null;
         }
      }

      class_afw var17 = new class_afw(var5, var6, var14, var1);
      this.b.add(var17);
      return var17;
   }

   public void b(ItemStack var1, Object... var2) {
      ArrayList var3 = Lists.newArrayList();
      Object[] var4 = var2;
      int var5 = var2.length;

      for(int var6 = 0; var6 < var5; ++var6) {
         Object var7 = var4[var6];
         if(var7 instanceof ItemStack) {
            var3.add(((ItemStack)var7).k());
         } else if(var7 instanceof Item) {
            var3.add(new ItemStack((Item)var7));
         } else {
            if(!(var7 instanceof Block)) {
               throw new IllegalArgumentException("Invalid shapeless recipe: unknown type " + var7.getClass().getName() + "!");
            }

            var3.add(new ItemStack((Block)var7));
         }
      }

      this.b.add(new class_afx(var1, var3));
   }

   public void a(class_aft var1) {
      this.b.add(var1);
   }

   public ItemStack a(InventoryCrafting var1, World var2) {
      Iterator var3 = this.b.iterator();

      class_aft var4;
      do {
         if(!var3.hasNext()) {
            return null;
         }

         var4 = (class_aft)var3.next();
      } while(!var4.a(var1, var2));

      return var4.a(var1);
   }

   public ItemStack[] b(InventoryCrafting var1, World var2) {
      Iterator var3 = this.b.iterator();

      while(var3.hasNext()) {
         class_aft var4 = (class_aft)var3.next();
         if(var4.a(var1, var2)) {
            return var4.b(var1);
         }
      }

      ItemStack[] var5 = new ItemStack[var1.u_()];

      for(int var6 = 0; var6 < var5.length; ++var6) {
         var5[var6] = var1.a(var6);
      }

      return var5;
   }

   public List b() {
      return this.b;
   }
}
