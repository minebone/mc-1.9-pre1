package net.minecraft.server;

import net.minecraft.server.EnumParticle;
import net.minecraft.server.Items;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_zl;

public class class_zw extends class_zl {
   private int f = 200;

   public class_zw(World var1) {
      super(var1);
   }

   public class_zw(World var1, class_rz var2) {
      super(var1, var2);
   }

   public class_zw(World var1, double var2, double var4, double var6) {
      super(var1, var2, var4, var6);
   }

   public void m() {
      super.m();
      if(this.world.E && !this.a) {
         this.world.a(EnumParticle.SPELL_INSTANT, this.locX, this.locY, this.locZ, 0.0D, 0.0D, 0.0D, new int[0]);
      }

   }

   protected ItemStack j() {
      return new ItemStack(Items.h);
   }

   protected void a(class_rz var1) {
      super.a(var1);
      MobEffect var2 = new MobEffect(MobEffectList.x, this.f, 0);
      var1.c(var2);
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      if(var1.e("Duration")) {
         this.f = var1.h("Duration");
      }

   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("Duration", this.f);
   }
}
