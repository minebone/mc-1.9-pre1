package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_aqi;
import net.minecraft.server.class_azx;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_qt;

public abstract class class_aql extends class_aqi implements class_qt {
   protected class_kk m;
   protected long n;

   protected boolean b(NBTTagCompound var1) {
      if(var1.b("LootTable", 8)) {
         this.m = new class_kk(var1.l("LootTable"));
         this.n = var1.i("LootTableSeed");
         return true;
      } else {
         return false;
      }
   }

   protected boolean c(NBTTagCompound var1) {
      if(this.m != null) {
         var1.a("LootTable", this.m.toString());
         if(this.n != 0L) {
            var1.a("LootTableSeed", this.n);
         }

         return true;
      } else {
         return false;
      }
   }

   protected void d(EntityHuman var1) {
      if(this.m != null) {
         class_azx var2 = this.b.ak().a(this.m);
         this.m = null;
         Random var3;
         if(this.n == 0L) {
            var3 = new Random();
         } else {
            var3 = new Random(this.n);
         }

         class_azy.class_a_in_class_azy var4 = new class_azy.class_a_in_class_azy((WorldServer)this.b);
         if(var1 != null) {
            var4.a(var1.da());
         }

         var2.a(this, var3, var4.a());
      }

   }

   public class_kk b() {
      return this.m;
   }
}
