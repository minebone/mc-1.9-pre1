package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.class_or;

public class BlockWood extends Block {
   public static final BlockStateEnum a = BlockStateEnum.a("variant", BlockWood.EnumLogVariant.class);

   public BlockWood() {
      super(Material.d);
      this.w(this.A.b().set(a, BlockWood.EnumLogVariant.OAK));
      this.a(CreativeModeTab.b);
   }

   public int d(IBlockData var1) {
      return ((BlockWood.EnumLogVariant)var1.get(a)).a();
   }

   public IBlockData a(int var1) {
      return this.u().set(a, BlockWood.EnumLogVariant.a(var1));
   }

   public MaterialMapColor r(IBlockData var1) {
      return ((BlockWood.EnumLogVariant)var1.get(a)).c();
   }

   public int e(IBlockData var1) {
      return ((BlockWood.EnumLogVariant)var1.get(a)).a();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   public static enum EnumLogVariant implements class_or {
      OAK(0, "oak", MaterialMapColor.o),
      SPRUCE(1, "spruce", MaterialMapColor.J),
      BIRCH(2, "birch", MaterialMapColor.d),
      JUNGLE(3, "jungle", MaterialMapColor.l),
      ACACIA(4, "acacia", MaterialMapColor.q),
      DARK_OAK(5, "dark_oak", "big_oak", MaterialMapColor.B);

      private static final BlockWood.EnumLogVariant[] g = new BlockWood.EnumLogVariant[values().length];
      private final int h;
      private final String i;
      private final String j;
      private final MaterialMapColor k;

      private EnumLogVariant(int var3, String var4, MaterialMapColor var5) {
         this(var3, var4, var4, var5);
      }

      private EnumLogVariant(int var3, String var4, String var5, MaterialMapColor var6) {
         this.h = var3;
         this.i = var4;
         this.j = var5;
         this.k = var6;
      }

      public int a() {
         return this.h;
      }

      public MaterialMapColor c() {
         return this.k;
      }

      public String toString() {
         return this.i;
      }

      public static BlockWood.EnumLogVariant a(int var0) {
         if(var0 < 0 || var0 >= g.length) {
            var0 = 0;
         }

         return g[var0];
      }

      public String m() {
         return this.i;
      }

      public String d() {
         return this.j;
      }

      static {
         BlockWood.EnumLogVariant[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            BlockWood.EnumLogVariant var3 = var0[var2];
            g[var3.a()] = var3;
         }

      }
   }
}
