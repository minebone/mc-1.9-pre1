package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.class_aky;
import net.minecraft.server.class_amn;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_aou extends class_amn {
   protected class_aou(Material var1) {
      super(var1);
      this.a(false);
      if(var1 == Material.i) {
         this.a(true);
      }

   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!this.e(var1, var2, var3)) {
         this.f(var1, var2, var3);
      }

   }

   private void f(World var1, BlockPosition var2, IBlockData var3) {
      class_aky var4 = a(this.x);
      var1.a((BlockPosition)var2, (IBlockData)var4.u().set(b, var3.get(b)), 2);
      var1.a((BlockPosition)var2, (Block)var4, this.a(var1));
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(this.x == Material.i) {
         if(var1.U().b("doFireTick")) {
            int var5 = var4.nextInt(3);
            if(var5 > 0) {
               BlockPosition var6 = var2;

               for(int var7 = 0; var7 < var5; ++var7) {
                  var6 = var6.a(var4.nextInt(3) - 1, 1, var4.nextInt(3) - 1);
                  if(var6.q() >= 0 && var6.q() < 256 && !var1.e(var6)) {
                     return;
                  }

                  Block var8 = var1.getType(var6).getBlock();
                  if(var8.x == Material.a) {
                     if(this.c(var1, var6)) {
                        var1.a(var6, Blocks.ab.u());
                        return;
                     }
                  } else if(var8.x.c()) {
                     return;
                  }
               }
            } else {
               for(int var9 = 0; var9 < 3; ++var9) {
                  BlockPosition var10 = var2.a(var4.nextInt(3) - 1, 0, var4.nextInt(3) - 1);
                  if(var10.q() >= 0 && var10.q() < 256 && !var1.e(var10)) {
                     return;
                  }

                  if(var1.d(var10.a()) && this.d(var1, var10)) {
                     var1.a(var10.a(), Blocks.ab.u());
                  }
               }
            }

         }
      }
   }

   protected boolean c(World var1, BlockPosition var2) {
      EnumDirection[] var3 = EnumDirection.values();
      int var4 = var3.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         EnumDirection var6 = var3[var5];
         if(this.d(var1, var2.a(var6))) {
            return true;
         }
      }

      return false;
   }

   private boolean d(World var1, BlockPosition var2) {
      return var2.q() >= 0 && var2.q() < 256 && !var1.e(var2)?false:var1.getType(var2).getMaterial().h();
   }
}
