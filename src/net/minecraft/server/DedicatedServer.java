package net.minecraft.server;

import com.google.common.collect.Lists;
import com.mojang.authlib.GameProfileRepository;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Proxy;
import java.security.KeyPair;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import net.minecraft.server.CrashReport;
import net.minecraft.server.DedicatedPlayerList;
import net.minecraft.server.EULA;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.MinecraftEncryption;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MojangStatisticsGenerator;
import net.minecraft.server.NameReferencingFileConverter;
import net.minecraft.server.PlayerList;
import net.minecraft.server.PropertyManager;
import net.minecraft.server.RemoteControlCommandListener;
import net.minecraft.server.RemoteControlListener;
import net.minecraft.server.RemoteStatusListener;
import net.minecraft.server.ServerGUI;
import net.minecraft.server.ThreadWatchdog;
import net.minecraft.server.UserCache;
import net.minecraft.server.World;
import net.minecraft.server.WorldSettings;
import net.minecraft.server.WorldType;
import net.minecraft.server.class_aqn;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_kp;
import net.minecraft.server.class_kv;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_pb;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DedicatedServer extends MinecraftServer implements class_kv {
   private static final Logger k = LogManager.getLogger();
   private final List l = Collections.synchronizedList(Lists.newArrayList());
   private RemoteStatusListener m;
   private final RemoteControlCommandListener n = new RemoteControlCommandListener(this);
   private RemoteControlListener o;
   private PropertyManager p;
   private EULA q;
   private boolean r;
   private WorldSettings.EnumGamemode s;
   private boolean t;

   public DedicatedServer(File var1, class_pb var2, YggdrasilAuthenticationService var3, MinecraftSessionService var4, GameProfileRepository var5, UserCache var6) {
      super(var1, Proxy.NO_PROXY, var2, var3, var4, var5, var6);
      Thread var10001 = new Thread("Server Infinisleeper") {
         {
            this.setDaemon(true);
            this.start();
         }

         public void run() {
            while(true) {
               try {
                  Thread.sleep(2147483647L);
               } catch (InterruptedException var2) {
                  ;
               }
            }
         }
      };
   }

   protected boolean j() throws IOException {
      Thread var1 = new Thread("Server console handler") {
         public void run() {
            BufferedReader var1 = new BufferedReader(new InputStreamReader(System.in));

            String var2;
            try {
               while(!DedicatedServer.this.ak() && DedicatedServer.this.w() && (var2 = var1.readLine()) != null) {
                  DedicatedServer.this.a(var2, (ICommandListener)DedicatedServer.this);
               }
            } catch (IOException var4) {
               DedicatedServer.k.error((String)"Exception handling console input", (Throwable)var4);
            }

         }
      };
      var1.setDaemon(true);
      var1.start();
      k.info("Starting minecraft server version 1.9-pre2");
      if(Runtime.getRuntime().maxMemory() / 1024L / 1024L < 512L) {
         k.warn("To start the server with more ram, launch it as \"java -Xmx1024M -Xms1024M -jar minecraft_server.jar\"");
      }

      k.info("Loading properties");
      this.p = new PropertyManager(new File("server.properties"));
      this.q = new EULA(new File("eula.txt"));
      if(!this.q.a()) {
         k.info("You need to agree to the EULA in order to run the server. Go to eula.txt for more info.");
         this.q.b();
         return false;
      } else {
         if(this.R()) {
            this.c("127.0.0.1");
         } else {
            this.d(this.p.a("online-mode", true));
            this.c(this.p.a("server-ip", ""));
         }

         this.e(this.p.a("spawn-animals", true));
         this.f(this.p.a("spawn-npcs", true));
         this.g(this.p.a("pvp", true));
         this.h(this.p.a("allow-flight", false));
         this.a_(this.p.a("resource-pack", ""), this.aK());
         this.l(this.p.a("motd", "A Minecraft Server"));
         this.i(this.p.a("force-gamemode", false));
         this.d(this.p.a("player-idle-timeout", 0));
         if(this.p.a("difficulty", 1) < 0) {
            this.p.a("difficulty", (Object)Integer.valueOf(0));
         } else if(this.p.a("difficulty", 1) > 3) {
            this.p.a("difficulty", (Object)Integer.valueOf(3));
         }

         this.r = this.p.a("generate-structures", true);
         int var2 = this.p.a("gamemode", WorldSettings.EnumGamemode.SURVIVAL.a());
         this.s = WorldSettings.a(var2);
         k.info("Default game type: " + this.s);
         InetAddress var3 = null;
         if(!this.v().isEmpty()) {
            var3 = InetAddress.getByName(this.v());
         }

         if(this.P() < 0) {
            this.b(this.p.a("server-port", 25565));
         }

         k.info("Generating keypair");
         this.a((KeyPair)MinecraftEncryption.b());
         k.info("Starting Minecraft server on " + (this.v().isEmpty()?"*":this.v()) + ":" + this.P());

         try {
            this.am().a(var3, this.P());
         } catch (IOException var17) {
            k.warn("**** FAILED TO BIND TO PORT!");
            k.warn("The exception was: {}", new Object[]{var17.toString()});
            k.warn("Perhaps a server is already running on that port?");
            return false;
         }

         if(!this.ab()) {
            k.warn("**** SERVER IS RUNNING IN OFFLINE/INSECURE MODE!");
            k.warn("The server will make no attempt to authenticate usernames. Beware.");
            k.warn("While this makes the game possible to play without internet access, it also opens up the ability for hackers to connect with any username they choose.");
            k.warn("To change this, set \"online-mode\" to \"true\" in the server.properties file.");
         }

         if(this.aO()) {
            this.aA().c();
         }

         if(!NameReferencingFileConverter.a(this.p)) {
            return false;
         } else {
            this.a((PlayerList)(new DedicatedPlayerList(this)));
            long var4 = System.nanoTime();
            if(this.S() == null) {
               this.j(this.p.a("level-name", "world"));
            }

            String var6 = this.p.a("level-seed", "");
            String var7 = this.p.a("level-type", "DEFAULT");
            String var8 = this.p.a("generator-settings", "");
            long var9 = (new Random()).nextLong();
            if(!var6.isEmpty()) {
               try {
                  long var11 = Long.parseLong(var6);
                  if(var11 != 0L) {
                     var9 = var11;
                  }
               } catch (NumberFormatException var16) {
                  var9 = (long)var6.hashCode();
               }
            }

            WorldType var18 = WorldType.a(var7);
            if(var18 == null) {
               var18 = WorldType.b;
            }

            this.ax();
            this.ah();
            this.q();
            this.Z();
            this.aF();
            this.c(this.p.a("max-build-height", 256));
            this.c((this.aj() + 8) / 16 * 16);
            this.c(MathHelper.a(this.aj(), 64, 256));
            this.p.a("max-build-height", (Object)Integer.valueOf(this.aj()));
            class_aqn.a(this.aA());
            class_aqn.a(this.ay());
            UserCache.a(this.ab());
            k.info("Preparing level \"" + this.S() + "\"");
            this.a(this.S(), this.S(), var9, var18, var8);
            long var12 = System.nanoTime() - var4;
            String var14 = String.format("%.3fs", new Object[]{Double.valueOf((double)var12 / 1.0E9D)});
            k.info("Done (" + var14 + ")! For help, type \"help\" or \"?\"");
            if(this.p.a("enable-query", false)) {
               k.info("Starting GS4 status listener");
               this.m = new RemoteStatusListener(this);
               this.m.a();
            }

            if(this.p.a("enable-rcon", false)) {
               k.info("Starting remote control listener");
               this.o = new RemoteControlListener(this);
               this.o.a();
            }

            if(this.aP() > 0L) {
               Thread var15 = new Thread(new ThreadWatchdog(this));
               var15.setName("Server Watchdog");
               var15.setDaemon(true);
               var15.start();
            }

            return true;
         }
      }
   }

   public String aK() {
      String var1 = "";
      if(this.p.a("resource-pack-hash") && !this.p.a("resource-pack-sha1")) {
         k.warn("ressource-pack-hash is depricated. Please use ressource-pack-sha1 instead.");
         var1 = this.p.a("resource-pack-hash", "");
         this.p.a("resource-pack-sha1", var1);
         this.p.b("resource-pack-hash");
      }

      if(this.p.a("resource-pack-hash") && this.p.a("resource-pack-sha1")) {
         k.warn("ressource-pack-hash is depricated and found along side resource-pack-sha1. resource-pack-hash will be ignored.");
      }

      var1 = this.p.a("resource-pack-sha1", "");
      if(!var1.equals("") && !var1.matches("^[a-f0-9]{40}$")) {
         k.warn("Invalid sha1 for ressource-pack-sha1");
      }

      if(!this.p.a("resource-pack", "").equals("") && var1.equals("")) {
         k.warn("You specified a resource pack without providing a sha1 hash. Pack will be updated on the client only if you change the name of the pack.");
      }

      return var1;
   }

   public void a(WorldSettings.EnumGamemode var1) {
      super.a(var1);
      this.s = var1;
   }

   public boolean m() {
      return this.r;
   }

   public WorldSettings.EnumGamemode n() {
      return this.s;
   }

   public EnumDifficulty o() {
      return EnumDifficulty.a(this.p.a("difficulty", EnumDifficulty.NORMAL.a()));
   }

   public boolean p() {
      return this.p.a("hardcore", false);
   }

   protected void a(CrashReport var1) {
   }

   public CrashReport b(CrashReport var1) {
      var1 = super.b(var1);
      var1.g().a("Is Modded", new Callable() {
         public String a() throws Exception {
            String var1 = DedicatedServer.this.getServerModName();
            return !var1.equals("vanilla")?"Definitely; Server brand changed to \'" + var1 + "\'":"Unknown (can\'t tell)";
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
      var1.g().a("Type", new Callable() {
         public String a() throws Exception {
            return "Dedicated Server (map_server.txt)";
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
      return var1;
   }

   protected void B() {
      System.exit(0);
   }

   public void D() {
      super.D();
      this.aL();
   }

   public boolean E() {
      return this.p.a("allow-nether", true);
   }

   public boolean U() {
      return this.p.a("spawn-monsters", true);
   }

   public void a(MojangStatisticsGenerator var1) {
      var1.a("whitelist_enabled", Boolean.valueOf(this.aM().r()));
      var1.a("whitelist_count", Integer.valueOf(this.aM().l().length));
      super.a(var1);
   }

   public boolean Z() {
      return this.p.a("snooper-enabled", true);
   }

   public void a(String var1, ICommandListener var2) {
      this.l.add(new class_kp(var1, var2));
   }

   public void aL() {
      while(!this.l.isEmpty()) {
         class_kp var1 = (class_kp)this.l.remove(0);
         this.N().a(var1.b, var1.a);
      }

   }

   public boolean aa() {
      return true;
   }

   public boolean ae() {
      return this.p.a("use-native-transport", true);
   }

   public DedicatedPlayerList aM() {
      return (DedicatedPlayerList)super.getPlayerList();
   }

   public int a(String var1, int var2) {
      return this.p.a(var1, var2);
   }

   public String a(String var1, String var2) {
      return this.p.a(var1, var2);
   }

   public boolean a(String var1, boolean var2) {
      return this.p.a(var1, var2);
   }

   public void a(String var1, Object var2) {
      this.p.a(var1, var2);
   }

   public void a() {
      this.p.b();
   }

   public String b() {
      File var1 = this.p.c();
      return var1 != null?var1.getAbsolutePath():"No settings file";
   }

   public String d_() {
      return this.v();
   }

   public int e_() {
      return this.P();
   }

   public String f_() {
      return this.ai();
   }

   public void aN() {
      ServerGUI.a(this);
      this.t = true;
   }

   public boolean ao() {
      return this.t;
   }

   public String a(WorldSettings.EnumGamemode var1, boolean var2) {
      return "";
   }

   public boolean ah() {
      return this.p.a("enable-command-block", false);
   }

   public int as() {
      return this.p.a("spawn-protection", super.as());
   }

   public boolean a(World var1, BlockPosition var2, EntityHuman var3) {
      if(var1.s.p().a() != 0) {
         return false;
      } else if(this.aM().m().d()) {
         return false;
      } else if(this.aM().h(var3.getProfile())) {
         return false;
      } else if(this.as() <= 0) {
         return false;
      } else {
         BlockPosition var4 = var1.R();
         int var5 = MathHelper.a(var2.p() - var4.p());
         int var6 = MathHelper.a(var2.r() - var4.r());
         int var7 = Math.max(var5, var6);
         return var7 <= this.as();
      }
   }

   public int q() {
      return this.p.a("op-permission-level", 4);
   }

   public void d(int var1) {
      super.d(var1);
      this.p.a("player-idle-timeout", (Object)Integer.valueOf(var1));
      this.a();
   }

   public boolean r() {
      return this.p.a("broadcast-rcon-to-ops", true);
   }

   public boolean s() {
      return this.p.a("broadcast-console-to-ops", true);
   }

   public boolean ax() {
      return this.p.a("announce-player-achievements", true);
   }

   public int aD() {
      int var1 = this.p.a("max-world-size", super.aD());
      if(var1 < 1) {
         var1 = 1;
      } else if(var1 > super.aD()) {
         var1 = super.aD();
      }

      return var1;
   }

   public int aF() {
      return this.p.a("network-compression-threshold", super.aF());
   }

   protected boolean aO() {
      boolean var2 = false;

      int var1;
      for(var1 = 0; !var2 && var1 <= 2; ++var1) {
         if(var1 > 0) {
            k.warn("Encountered a problem while converting the user banlist, retrying in a few seconds");
            this.aR();
         }

         var2 = NameReferencingFileConverter.a((MinecraftServer)this);
      }

      boolean var3 = false;

      for(var1 = 0; !var3 && var1 <= 2; ++var1) {
         if(var1 > 0) {
            k.warn("Encountered a problem while converting the ip banlist, retrying in a few seconds");
            this.aR();
         }

         var3 = NameReferencingFileConverter.b((MinecraftServer)this);
      }

      boolean var4 = false;

      for(var1 = 0; !var4 && var1 <= 2; ++var1) {
         if(var1 > 0) {
            k.warn("Encountered a problem while converting the op list, retrying in a few seconds");
            this.aR();
         }

         var4 = NameReferencingFileConverter.c((MinecraftServer)this);
      }

      boolean var5 = false;

      for(var1 = 0; !var5 && var1 <= 2; ++var1) {
         if(var1 > 0) {
            k.warn("Encountered a problem while converting the whitelist, retrying in a few seconds");
            this.aR();
         }

         var5 = NameReferencingFileConverter.d((MinecraftServer)this);
      }

      boolean var6 = false;

      for(var1 = 0; !var6 && var1 <= 2; ++var1) {
         if(var1 > 0) {
            k.warn("Encountered a problem while converting the player save files, retrying in a few seconds");
            this.aR();
         }

         var6 = NameReferencingFileConverter.a(this, this.p);
      }

      return var2 || var3 || var4 || var5 || var6;
   }

   private void aR() {
      try {
         Thread.sleep(5000L);
      } catch (InterruptedException var2) {
         ;
      }
   }

   public long aP() {
      return this.p.a("max-tick-time", TimeUnit.MINUTES.toMillis(1L));
   }

   public String g_() {
      return "";
   }

   public String a_(String var1) {
      this.n.i();
      this.b.a(this.n, var1);
      return this.n.j();
   }

   // $FF: synthetic method
   public PlayerList getPlayerList() {
      return this.aM();
   }
}
