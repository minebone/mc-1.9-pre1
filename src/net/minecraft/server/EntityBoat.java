package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityAnimal;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_amn;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayInSteer;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rd;
import net.minecraft.server.class_ru;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_wh;

public class EntityBoat extends Entity {
   private static final class_ke a = DataWatcher.a(EntityBoat.class, class_kg.b);
   private static final class_ke b = DataWatcher.a(EntityBoat.class, class_kg.b);
   private static final class_ke c = DataWatcher.a(EntityBoat.class, class_kg.c);
   private static final class_ke d = DataWatcher.a(EntityBoat.class, class_kg.b);
   private static final class_ke[] e = new class_ke[]{DataWatcher.a(EntityBoat.class, class_kg.h), DataWatcher.a(EntityBoat.class, class_kg.h)};
   private float[] f;
   private float g;
   private float h;
   private float as;
   private int at;
   private double au;
   private double av;
   private double aw;
   private double ax;
   private double ay;
   private boolean az;
   private boolean aA;
   private boolean aB;
   private boolean aC;
   private double aD;
   private float aE;
   private EntityBoat.class_a_in_class_aaf aF;
   private EntityBoat.class_a_in_class_aaf aG;
   private double aH;

   public EntityBoat(World var1) {
      super(var1);
      this.f = new float[2];
      this.i = true;
      this.a(1.375F, 0.5625F);
   }

   public EntityBoat(World var1, double var2, double var4, double var6) {
      this(var1);
      this.b(var2, var4, var6);
      this.motX = 0.0D;
      this.motY = 0.0D;
      this.motZ = 0.0D;
      this.lastX = var2;
      this.lastY = var4;
      this.lastZ = var6;
   }

   protected boolean ad() {
      return false;
   }

   protected void i() {
      this.datawatcher.a((class_ke)a, (Object)Integer.valueOf(0));
      this.datawatcher.a((class_ke)b, (Object)Integer.valueOf(1));
      this.datawatcher.a((class_ke)c, (Object)Float.valueOf(0.0F));
      this.datawatcher.a((class_ke)d, (Object)Integer.valueOf(EntityBoat.EnumBoatVariant.OAK.ordinal()));

      for(int var1 = 0; var1 < e.length; ++var1) {
         this.datawatcher.a((class_ke)e[var1], (Object)Boolean.valueOf(false));
      }

   }

   public AxisAlignedBB j(Entity var1) {
      return var1.bk();
   }

   public AxisAlignedBB ae() {
      return this.bk();
   }

   public boolean ap() {
      return true;
   }

   public double ax() {
      return -0.1D;
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b(var1)) {
         return false;
      } else if(!this.world.E && !this.dead) {
         if(var1 instanceof class_rd && var1.j() != null && this.w(var1.j())) {
            return false;
         } else {
            this.d(-this.q());
            this.b(10);
            this.a(this.n() + var2 * 10.0F);
            this.an();
            boolean var3 = var1.j() instanceof EntityHuman && ((EntityHuman)var1.j()).abilities.d;
            if(var3 || this.n() > 40.0F) {
               if(!var3 && this.world.U().b("doEntityDrops")) {
                  this.a(this.j(), 1, 0.0F);
               }

               this.S();
            }

            return true;
         }
      } else {
         return true;
      }
   }

   public void i(Entity var1) {
      if(var1 instanceof EntityBoat) {
         if(var1.bk().b < this.bk().e) {
            super.i(var1);
         }
      } else if(var1.bk().b <= this.bk().b) {
         super.i(var1);
      }

   }

   public Item j() {
      switch(EntityBoat.SyntheticClass_1.a[this.r().ordinal()]) {
      case 1:
      default:
         return Items.aG;
      case 2:
         return Items.aH;
      case 3:
         return Items.aI;
      case 4:
         return Items.aJ;
      case 5:
         return Items.aK;
      case 6:
         return Items.aL;
      }
   }

   public boolean ao() {
      return !this.dead;
   }

   public EnumDirection bi() {
      return this.bh().e();
   }

   public void m() {
      this.aG = this.aF;
      this.aF = this.t();
      if(this.aF != EntityBoat.class_a_in_class_aaf.UNDER_WATER && this.aF != EntityBoat.class_a_in_class_aaf.UNDER_FLOWING_WATER) {
         this.h = 0.0F;
      } else {
         ++this.h;
      }

      if(!this.world.E && this.h >= 60.0F) {
         this.ay();
      }

      if(this.o() > 0) {
         this.b(this.o() - 1);
      }

      if(this.n() > 0.0F) {
         this.a(this.n() - 1.0F);
      }

      this.lastX = this.locX;
      this.lastY = this.locY;
      this.lastZ = this.locZ;
      super.m();
      this.s();
      if(this.bw()) {
         if(this.bt().size() == 0 || !(this.bt().get(0) instanceof EntityHuman)) {
            this.a(false, false);
         }

         this.w();
         if(this.world.E) {
            this.x();
            this.world.a((Packet)(new PacketPlayInSteer(this.a(0), this.a(1))));
         }

         this.d(this.motX, this.motY, this.motZ);
      } else if(!this.world.E) {
         this.motX = 0.0D;
         this.motY = 0.0D;
         this.motZ = 0.0D;
      }

      for(int var1 = 0; var1 <= 1; ++var1) {
         if(this.a(var1)) {
            this.f[var1] = (float)((double)this.f[var1] + 0.01D);
         } else {
            this.f[var1] = 0.0F;
         }
      }

      this.ab();
      List var5 = this.world.a((Entity)this, (AxisAlignedBB)this.bk().b(0.20000000298023224D, -0.009999999776482582D, 0.20000000298023224D), (Predicate)class_ru.a(this));
      if(!var5.isEmpty()) {
         boolean var2 = !this.world.E && !(this.bs() instanceof EntityHuman);

         for(int var3 = 0; var3 < var5.size(); ++var3) {
            Entity var4 = (Entity)var5.get(var3);
            if(!var4.w(this)) {
               if(var2 && this.bt().size() < 2 && !var4.aH() && var4.width < this.width && var4 instanceof class_rz && !(var4 instanceof class_wh) && !(var4 instanceof EntityHuman)) {
                  var4.m(this);
               } else {
                  this.i(var4);
               }
            }
         }
      }

   }

   private void s() {
      if(this.at > 0 && !this.bw()) {
         double var1 = this.locX + (this.au - this.locX) / (double)this.at;
         double var3 = this.locY + (this.av - this.locY) / (double)this.at;
         double var5 = this.locZ + (this.aw - this.locZ) / (double)this.at;
         double var7 = MathHelper.g(this.ax - (double)this.yaw);
         this.yaw = (float)((double)this.yaw + var7 / (double)this.at);
         this.pitch = (float)((double)this.pitch + (this.ay - (double)this.pitch) / (double)this.at);
         --this.at;
         this.b(var1, var3, var5);
         this.b(this.yaw, this.pitch);
      }
   }

   public void a(boolean var1, boolean var2) {
      this.datawatcher.b(e[0], Boolean.valueOf(var1));
      this.datawatcher.b(e[1], Boolean.valueOf(var2));
   }

   private EntityBoat.class_a_in_class_aaf t() {
      EntityBoat.class_a_in_class_aaf var1 = this.v();
      if(var1 != null) {
         this.aD = this.bk().e;
         return var1;
      } else if(this.u()) {
         return EntityBoat.class_a_in_class_aaf.IN_WATER;
      } else {
         float var2 = this.l();
         if(var2 > 0.0F) {
            this.aE = var2;
            return EntityBoat.class_a_in_class_aaf.ON_LAND;
         } else {
            return EntityBoat.class_a_in_class_aaf.IN_AIR;
         }
      }
   }

   public float k() {
      AxisAlignedBB var1 = this.bk();
      int var2 = MathHelper.c(var1.a);
      int var3 = MathHelper.f(var1.d);
      int var4 = MathHelper.c(var1.e);
      int var5 = MathHelper.f(var1.e - this.aH);
      int var6 = MathHelper.c(var1.c);
      int var7 = MathHelper.f(var1.f);
      BlockPosition.class_b_in_class_cj var8 = BlockPosition.class_b_in_class_cj.s();

      float var18;
      try {
         label108:
         for(int var9 = var4; var9 < var5; ++var9) {
            float var10 = 0.0F;

            for(int var11 = var2; var11 < var3; ++var11) {
               for(int var12 = var6; var12 < var7; ++var12) {
                  var8.d(var11, var9, var12);
                  IBlockData var13 = this.world.getType(var8);
                  if(var13.getMaterial() == Material.h) {
                     var10 = Math.max(var10, a((IBlockData)var13, (class_ahw)this.world, (BlockPosition)var8));
                  }

                  if(var10 >= 1.0F) {
                     continue label108;
                  }
               }
            }

            if(var10 < 1.0F) {
               float var17 = (float)var8.q() + var10;
               return var17;
            }
         }

         var18 = (float)(var5 + 1);
      } finally {
         var8.t();
      }

      return var18;
   }

   public float l() {
      AxisAlignedBB var1 = this.bk();
      AxisAlignedBB var2 = new AxisAlignedBB(var1.a, var1.b - 0.001D, var1.c, var1.d, var1.b, var1.f);
      int var3 = MathHelper.c(var2.a) - 1;
      int var4 = MathHelper.f(var2.d) + 1;
      int var5 = MathHelper.c(var2.b) - 1;
      int var6 = MathHelper.f(var2.e) + 1;
      int var7 = MathHelper.c(var2.c) - 1;
      int var8 = MathHelper.f(var2.f) + 1;
      ArrayList var9 = Lists.newArrayList();
      float var10 = 0.0F;
      int var11 = 0;
      BlockPosition.class_b_in_class_cj var12 = BlockPosition.class_b_in_class_cj.s();

      try {
         for(int var13 = var3; var13 < var4; ++var13) {
            for(int var14 = var7; var14 < var8; ++var14) {
               int var15 = (var13 != var3 && var13 != var4 - 1?0:1) + (var14 != var7 && var14 != var8 - 1?0:1);
               if(var15 != 2) {
                  for(int var16 = var5; var16 < var6; ++var16) {
                     if(var15 <= 0 || var16 != var5 && var16 != var6 - 1) {
                        var12.d(var13, var16, var14);
                        IBlockData var17 = this.world.getType(var12);
                        var17.a(this.world, var12, var2, var9, this);
                        if(!var9.isEmpty()) {
                           var10 += var17.getBlock().z;
                           ++var11;
                        }

                        var9.clear();
                     }
                  }
               }
            }
         }
      } finally {
         var12.t();
      }

      return var10 / (float)var11;
   }

   private boolean u() {
      AxisAlignedBB var1 = this.bk();
      int var2 = MathHelper.c(var1.a);
      int var3 = MathHelper.f(var1.d);
      int var4 = MathHelper.c(var1.b);
      int var5 = MathHelper.f(var1.b + 0.001D);
      int var6 = MathHelper.c(var1.c);
      int var7 = MathHelper.f(var1.f);
      boolean var8 = false;
      this.aD = Double.MIN_VALUE;
      BlockPosition.class_b_in_class_cj var9 = BlockPosition.class_b_in_class_cj.s();

      try {
         for(int var10 = var2; var10 < var3; ++var10) {
            for(int var11 = var4; var11 < var5; ++var11) {
               for(int var12 = var6; var12 < var7; ++var12) {
                  var9.d(var10, var11, var12);
                  IBlockData var13 = this.world.getType(var9);
                  if(var13.getMaterial() == Material.h) {
                     float var14 = b(var13, this.world, var9);
                     this.aD = Math.max((double)var14, this.aD);
                     var8 |= var1.b < (double)var14;
                  }
               }
            }
         }
      } finally {
         var9.t();
      }

      return var8;
   }

   private EntityBoat.class_a_in_class_aaf v() {
      AxisAlignedBB var1 = this.bk();
      double var2 = var1.e + 0.001D;
      int var4 = MathHelper.c(var1.a);
      int var5 = MathHelper.f(var1.d);
      int var6 = MathHelper.c(var1.e);
      int var7 = MathHelper.f(var2);
      int var8 = MathHelper.c(var1.c);
      int var9 = MathHelper.f(var1.f);
      boolean var10 = false;
      BlockPosition.class_b_in_class_cj var11 = BlockPosition.class_b_in_class_cj.s();

      try {
         for(int var12 = var4; var12 < var5; ++var12) {
            for(int var13 = var6; var13 < var7; ++var13) {
               for(int var14 = var8; var14 < var9; ++var14) {
                  var11.d(var12, var13, var14);
                  IBlockData var15 = this.world.getType(var11);
                  if(var15.getMaterial() == Material.h && var2 < (double)b(var15, this.world, var11)) {
                     if(((Integer)var15.get(class_amn.b)).intValue() != 0) {
                        EntityBoat.class_a_in_class_aaf var16 = EntityBoat.class_a_in_class_aaf.UNDER_FLOWING_WATER;
                        return var16;
                     }

                     var10 = true;
                  }
               }
            }
         }

         return var10?EntityBoat.class_a_in_class_aaf.UNDER_WATER:null;
      } finally {
         var11.t();
      }
   }

   public static float a(IBlockData var0, class_ahw var1, BlockPosition var2) {
      int var3 = ((Integer)var0.get(class_amn.b)).intValue();
      return (var3 & 7) == 0 && var1.getType(var2.a()).getMaterial() == Material.h?1.0F:1.0F - class_amn.e(var3);
   }

   public static float b(IBlockData var0, class_ahw var1, BlockPosition var2) {
      return (float)var2.q() + a(var0, var1, var2);
   }

   private void w() {
      double var1 = -0.03999999910593033D;
      double var3 = var1;
      double var5 = 0.0D;
      this.g = 0.05F;
      if(this.aG == EntityBoat.class_a_in_class_aaf.IN_AIR && this.aF != EntityBoat.class_a_in_class_aaf.IN_AIR && this.aF != EntityBoat.class_a_in_class_aaf.ON_LAND) {
         this.aD = this.bk().b + (double)this.length;
         this.b(this.locX, (double)(this.k() - this.length) + 0.101D, this.locZ);
         this.motY = 0.0D;
         this.aH = 0.0D;
         this.aF = EntityBoat.class_a_in_class_aaf.IN_WATER;
      } else {
         if(this.aF == EntityBoat.class_a_in_class_aaf.IN_WATER) {
            var5 = (this.aD - this.bk().b) / (double)this.length;
            this.g = 0.9F;
         } else if(this.aF == EntityBoat.class_a_in_class_aaf.UNDER_FLOWING_WATER) {
            var3 = -7.0E-4D;
            this.g = 0.9F;
         } else if(this.aF == EntityBoat.class_a_in_class_aaf.UNDER_WATER) {
            var5 = 0.009999999776482582D;
            this.g = 0.45F;
         } else if(this.aF == EntityBoat.class_a_in_class_aaf.IN_AIR) {
            this.g = 0.9F;
         } else if(this.aF == EntityBoat.class_a_in_class_aaf.ON_LAND) {
            this.g = this.aE;
            if(this.bs() instanceof EntityHuman) {
               this.aE /= 2.0F;
            }
         }

         this.motX *= (double)this.g;
         this.motZ *= (double)this.g;
         this.as *= this.g;
         this.motY += var3;
         if(var5 > 0.0D) {
            double var7 = 0.65D;
            this.motY += var5 * (-var1 / 0.65D);
            double var9 = 0.75D;
            this.motY *= 0.75D;
         }
      }

   }

   private void x() {
      if(this.aI()) {
         float var1 = 0.0F;
         if(this.az) {
            this.as += -1.0F;
         }

         if(this.aA) {
            ++this.as;
         }

         if(this.aA != this.az && !this.aB && !this.aC) {
            var1 += 0.005F;
         }

         this.yaw += this.as;
         if(this.aB) {
            var1 += 0.04F;
         }

         if(this.aC) {
            var1 -= 0.005F;
         }

         this.motX += (double)(MathHelper.a(-this.yaw * 0.017453292F) * var1);
         this.motZ += (double)(MathHelper.b(this.yaw * 0.017453292F) * var1);
         this.a(this.aA || this.aB, this.az || this.aB);
      }
   }

   public void k(Entity var1) {
      if(this.w(var1)) {
         float var2 = 0.0F;
         float var3 = (float)((this.dead?0.009999999776482582D:this.ax()) + var1.aw());
         if(this.bt().size() > 1) {
            int var4 = this.bt().indexOf(var1);
            if(var4 == 0) {
               var2 = 0.2F;
            } else {
               var2 = -0.6F;
            }

            if(var1 instanceof EntityAnimal) {
               var2 = (float)((double)var2 + 0.2D);
            }
         }

         Vec3D var6 = (new Vec3D((double)var2, 0.0D, 0.0D)).b(-this.yaw * 0.017453292F - 1.5707964F);
         var1.b(this.locX + var6.b, this.locY + (double)var3, this.locZ + var6.d);
         var1.yaw += this.as;
         var1.h(var1.aR() + this.as);
         this.a(var1);
         if(var1 instanceof EntityAnimal && this.bt().size() > 1) {
            int var5 = var1.getId() % 2 == 0?90:270;
            var1.i(((EntityAnimal)var1).aM + (float)var5);
            var1.h(var1.aR() + (float)var5);
         }

      }
   }

   protected void a(Entity var1) {
      var1.i(this.yaw);
      float var2 = MathHelper.g(var1.yaw - this.yaw);
      float var3 = MathHelper.a(var2, -105.0F, 105.0F);
      var1.lastYaw += var3 - var2;
      var1.yaw += var3 - var2;
      var1.h(var1.yaw);
   }

   protected void b(NBTTagCompound var1) {
      var1.a("Type", this.r().a());
   }

   protected void a(NBTTagCompound var1) {
      if(var1.b("Type", 8)) {
         this.a(EntityBoat.EnumBoatVariant.a(var1.l("Type")));
      }

   }

   public boolean a(EntityHuman var1, ItemStack var2, EnumHand var3) {
      if(!this.world.E && !var1.aJ() && this.h < 60.0F) {
         var1.m(this);
      }

      return true;
   }

   protected void a(double var1, boolean var3, IBlockData var4, BlockPosition var5) {
      this.aH = this.motY;
      if(!this.aH()) {
         if(var3) {
            if(this.fallDistance > 3.0F) {
               if(this.aF != EntityBoat.class_a_in_class_aaf.ON_LAND) {
                  this.fallDistance = 0.0F;
                  return;
               }

               this.e(this.fallDistance, 1.0F);
               if(!this.world.E && !this.dead) {
                  this.S();
                  if(this.world.U().b("doEntityDrops")) {
                     int var6;
                     for(var6 = 0; var6 < 3; ++var6) {
                        this.a(new ItemStack(Item.a(Blocks.f), 1, this.r().b()), 0.0F);
                     }

                     for(var6 = 0; var6 < 2; ++var6) {
                        this.a(Items.A, 1, 0.0F);
                     }
                  }
               }
            }

            this.fallDistance = 0.0F;
         } else if(this.world.getType((new BlockPosition(this)).b()).getMaterial() != Material.h && var1 < 0.0D) {
            this.fallDistance = (float)((double)this.fallDistance - var1);
         }

      }
   }

   public boolean a(int var1) {
      return ((Boolean)this.datawatcher.a(e[var1])).booleanValue() && this.bs() != null;
   }

   public void a(float var1) {
      this.datawatcher.b(c, Float.valueOf(var1));
   }

   public float n() {
      return ((Float)this.datawatcher.a(c)).floatValue();
   }

   public void b(int var1) {
      this.datawatcher.b(a, Integer.valueOf(var1));
   }

   public int o() {
      return ((Integer)this.datawatcher.a(a)).intValue();
   }

   public void ds(int var1) {
      this.datawatcher.b(b, Integer.valueOf(var1));
   }

   public int q() {
      return ((Integer)this.datawatcher.a(b)).intValue();
   }

   public void a(EntityBoat.EnumBoatVariant var1) {
      this.datawatcher.b(d, Integer.valueOf(var1.ordinal()));
   }

   public EntityBoat.EnumBoatVariant r() {
      return EntityBoat.EnumBoatVariant.a(((Integer)this.datawatcher.a(d)).intValue());
   }

   protected boolean q(Entity var1) {
      return this.bt().size() < 2;
   }

   public Entity bs() {
      List var1 = this.bt();
      return var1.isEmpty()?null:(Entity)var1.get(0);
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EntityBoat.EnumBoatVariant.values().length];

      static {
         try {
            a[EntityBoat.EnumBoatVariant.OAK.ordinal()] = 1;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[EntityBoat.EnumBoatVariant.SPRUCE.ordinal()] = 2;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[EntityBoat.EnumBoatVariant.BIRCH.ordinal()] = 3;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EntityBoat.EnumBoatVariant.JUNGLE.ordinal()] = 4;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EntityBoat.EnumBoatVariant.ACACIA.ordinal()] = 5;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EntityBoat.EnumBoatVariant.DARK_OAK.ordinal()] = 6;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum EnumBoatVariant {
      OAK(BlockWood.EnumLogVariant.OAK.a(), "oak"),
      SPRUCE(BlockWood.EnumLogVariant.SPRUCE.a(), "spruce"),
      BIRCH(BlockWood.EnumLogVariant.BIRCH.a(), "birch"),
      JUNGLE(BlockWood.EnumLogVariant.JUNGLE.a(), "jungle"),
      ACACIA(BlockWood.EnumLogVariant.ACACIA.a(), "acacia"),
      DARK_OAK(BlockWood.EnumLogVariant.DARK_OAK.a(), "dark_oak");

      private final String g;
      private final int h;

      private EnumBoatVariant(int var3, String var4) {
         this.g = var4;
         this.h = var3;
      }

      public String a() {
         return this.g;
      }

      public int b() {
         return this.h;
      }

      public String toString() {
         return this.g;
      }

      public static EntityBoat.EnumBoatVariant a(int var0) {
         if(var0 < 0 || var0 >= values().length) {
            var0 = 0;
         }

         return values()[var0];
      }

      public static EntityBoat.EnumBoatVariant a(String var0) {
         for(int var1 = 0; var1 < values().length; ++var1) {
            if(values()[var1].a().equals(var0)) {
               return values()[var1];
            }
         }

         return values()[0];
      }
   }

   public static enum class_a_in_class_aaf {
      IN_WATER,
      UNDER_WATER,
      UNDER_FLOWING_WATER,
      ON_LAND,
      IN_AIR;
   }
}
