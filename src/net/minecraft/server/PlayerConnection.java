package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Floats;
import com.google.common.util.concurrent.Futures;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import net.minecraft.server.AchievementList;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Blocks;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.Container;
import net.minecraft.server.ContainerAnvil;
import net.minecraft.server.CrashReport;
import net.minecraft.server.CrashReportSystemDetails;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumChatFormat;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.ItemWrittenBook;
import net.minecraft.server.Items;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.MobEffectType;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.NBTTagString;
import net.minecraft.server.NetworkManager;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketPlayInBlockDig;
import net.minecraft.server.PacketPlayInClientCommand;
import net.minecraft.server.PacketPlayInCustomPayload;
import net.minecraft.server.PacketPlayInResourcePackStatus;
import net.minecraft.server.PacketPlayInUseEntity;
import net.minecraft.server.PacketPlayOutPosition;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.Statistic;
import net.minecraft.server.TileEntity;
import net.minecraft.server.UtilColor;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.WorldSettings;
import net.minecraft.server.EntityBoat;
import net.minecraft.server.class_aai;
import net.minecraft.server.class_aaw;
import net.minecraft.server.class_abn;
import net.minecraft.server.class_abs;
import net.minecraft.server.class_acw;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_afb;
import net.minecraft.server.class_ahi;
import net.minecraft.server.class_akj;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_apx;
import net.minecraft.server.class_aqm;
import net.minecraft.server.class_aqo;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_e;
import net.minecraft.server.PacketListener;
import net.minecraft.server.class_f;
import net.minecraft.server.Packet;
import net.minecraft.server.class_fh;
import net.minecraft.server.PacketPlayOutBlockChange;
import net.minecraft.server.PacketPlayOutTabComplete;
import net.minecraft.server.PacketPlayOutChat;
import net.minecraft.server.PacketPlayOutTransaction;
import net.minecraft.server.PacketPlayOutSetSlot;
import net.minecraft.server.PacketPlayOutKickDisconnect;
import net.minecraft.server.PacketPlayOutKeepAlive;
import net.minecraft.server.class_gv;
import net.minecraft.server.PacketPlayOutRespawn;
import net.minecraft.server.PacketPlayOutHeldItemSlot;
import net.minecraft.server.PacketPlayInListener;
import net.minecraft.server.PacketPlayInTeleport;
import net.minecraft.server.class_ii;
import net.minecraft.server.class_ij;
import net.minecraft.server.class_il;
import net.minecraft.server.class_im;
import net.minecraft.server.class_in;
import net.minecraft.server.class_io;
import net.minecraft.server.class_ip;
import net.minecraft.server.class_is;
import net.minecraft.server.class_it;
import net.minecraft.server.PacketPlayInVehicleMove;
import net.minecraft.server.PacketPlayInSteer;
import net.minecraft.server.class_iw;
import net.minecraft.server.class_iy;
import net.minecraft.server.class_iz;
import net.minecraft.server.class_jb;
import net.minecraft.server.class_jc;
import net.minecraft.server.class_jd;
import net.minecraft.server.class_je;
import net.minecraft.server.class_jf;
import net.minecraft.server.class_jg;
import net.minecraft.server.class_jh;
import net.minecraft.server.class_ky;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.class_oh;
import net.minecraft.server.IInventory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rw;
import net.minecraft.server.class_si;
import net.minecraft.server.class_wj;
import net.minecraft.server.class_yc;
import net.minecraft.server.class_zl;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PlayerConnection implements PacketPlayInListener, class_ky {
   private static final Logger c = LogManager.getLogger();
   public final NetworkManager a;
   private final MinecraftServer d;
   public EntityPlayer player;
   private int e;
   private int f;
   private long g;
   private long h;
   private int i;
   private int j;
   private final class_oh k = new class_oh();
   private double l;
   private double m;
   private double n;
   private double o;
   private double p;
   private double q;
   private Entity r;
   private double s;
   private double t;
   private double u;
   private double v;
   private double w;
   private double x;
   private Vec3D vec3D;
   private int z;
   private int A;
   private boolean B;
   private int C;
   private boolean D;
   private int E;
   private int F;
   private int G;

   public PlayerConnection(MinecraftServer var1, NetworkManager var2, EntityPlayer var3) {
      this.d = var1;
      this.a = var2;
      var2.a((PacketListener)this);
      this.player = var3;
      var3.a = this;
   }

   public void c() {
      this.d();
      this.player.k_();
      this.player.move(this.l, this.m, this.n, this.player.yaw, this.player.pitch);
      ++this.e;
      this.G = this.F;
      if(this.B) {
         if(++this.C > 80) {
            c.warn(this.player.h_() + " was kicked for floating too long!");
            this.c("Flying is not enabled on this server");
            return;
         }
      } else {
         this.B = false;
         this.C = 0;
      }

      this.r = this.player.bv();
      if(this.r != this.player && this.r.bs() == this.player) {
         this.s = this.r.locX;
         this.t = this.r.locY;
         this.u = this.r.locZ;
         this.v = this.r.locX;
         this.w = this.r.locY;
         this.x = this.r.locZ;
         if(this.D && this.player.bv().bs() == this.player) {
            if(++this.E > 80) {
               c.warn(this.player.h_() + " was kicked for floating a vehicle too long!");
               this.c("Flying is not enabled on this server");
               return;
            }
         } else {
            this.D = false;
            this.E = 0;
         }
      } else {
         this.r = null;
         this.D = false;
         this.E = 0;
      }

      this.d.c.a("keepAlive");
      if((long)this.e - this.h > 40L) {
         this.h = (long)this.e;
         this.g = this.e();
         this.f = (int)this.g;
         this.a((Packet)(new PacketPlayOutKeepAlive(this.f)));
      }

      this.d.c.b();
      if(this.i > 0) {
         --this.i;
      }

      if(this.j > 0) {
         --this.j;
      }

      if(this.player.I() > 0L && this.d.aw() > 0 && MinecraftServer.av() - this.player.I() > (long)(this.d.aw() * 1000 * 60)) {
         this.c("You have been idle for too long!");
      }

   }

   private void d() {
      this.l = this.player.locX;
      this.m = this.player.locY;
      this.n = this.player.locZ;
      this.o = this.player.locX;
      this.p = this.player.locY;
      this.q = this.player.locZ;
   }

   public NetworkManager a() {
      return this.a;
   }

   public void c(String var1) {
      final ChatComponentText var2 = new ChatComponentText(var1);
      this.a.a(new PacketPlayOutKickDisconnect(var2), new GenericFutureListener() {
         public void operationComplete(Future var1) throws Exception {
            PlayerConnection.this.a.a((IChatBaseComponent)var2);
         }
      }, new GenericFutureListener[0]);
      this.a.k();
      Futures.getUnchecked(this.d.a(new Runnable() {
         public void run() {
            PlayerConnection.this.a.l();
         }
      }));
   }

   public void a(class_iz var1) {
      class_fh.a(var1, this, this.player.x());
      this.player.a(var1.a(), var1.b(), var1.c(), var1.d());
   }

   private static boolean b(class_it var0) {
      return Doubles.isFinite(var0.a(0.0D)) && Doubles.isFinite(var0.b(0.0D)) && Doubles.isFinite(var0.c(0.0D)) && Floats.isFinite(var0.b(0.0F)) && Floats.isFinite(var0.a(0.0F))?false:Math.abs(var0.a(0.0D)) <= 3.0E7D && Math.abs(var0.a(0.0D)) <= 3.0E7D;
   }

   private static boolean b(PacketPlayInVehicleMove var0) {
      return !Doubles.isFinite(var0.a()) || !Doubles.isFinite(var0.b()) || !Doubles.isFinite(var0.c()) || !Floats.isFinite(var0.e()) || !Floats.isFinite(var0.d());
   }

   public void a(PacketPlayInVehicleMove var1) {
      class_fh.a(var1, this, this.player.x());
      if(b(var1)) {
         this.c("Invalid move vehicle packet received");
      } else {
         Entity var2 = this.player.bv();
         if(var2 != this.player && var2.bs() == this.player && var2 == this.r) {
            WorldServer var3 = this.player.x();
            double var4 = var2.locX;
            double var6 = var2.locY;
            double var8 = var2.locZ;
            double var10 = var1.a();
            double var12 = var1.b();
            double var14 = var1.c();
            float var16 = var1.d();
            float var17 = var1.e();
            double var18 = var10 - this.s;
            double var20 = var12 - this.t;
            double var22 = var14 - this.u;
            double var24 = var2.motX * var2.motX + var2.motY * var2.motY + var2.motZ * var2.motZ;
            double var26 = var18 * var18 + var20 * var20 + var22 * var22;
            if(var26 - var24 > 100.0D && (!this.d.R() || !this.d.Q().equals(var2.h_()))) {
               c.warn(var2.h_() + " (vehicle of " + this.player.h_() + ") moved too quickly! " + var18 + "," + var20 + "," + var22);
               this.a.a((Packet)(new class_gv(var2)));
               return;
            }

            boolean var28 = var3.a((Entity)var2, (AxisAlignedBB)var2.bk().h(0.0625D)).isEmpty();
            var18 = var10 - this.v;
            var20 = var12 - this.w - 1.0E-6D;
            var22 = var14 - this.x;
            var2.d(var18, var20, var22);
            double var29 = var20;
            var18 = var10 - var2.locX;
            var20 = var12 - var2.locY;
            if(var20 > -0.5D || var20 < 0.5D) {
               var20 = 0.0D;
            }

            var22 = var14 - var2.locZ;
            var26 = var18 * var18 + var20 * var20 + var22 * var22;
            boolean var31 = false;
            if(var26 > 0.0625D) {
               var31 = true;
               c.warn(var2.h_() + " moved wrongly!");
            }

            var2.move(var10, var12, var14, var16, var17);
            boolean var32 = var3.a((Entity)var2, (AxisAlignedBB)var2.bk().h(0.0625D)).isEmpty();
            if(var28 && (var31 || !var32)) {
               var2.move(var4, var6, var8, var16, var17);
               this.a.a((Packet)(new class_gv(var2)));
               return;
            }

            this.d.getPlayerList().d(this.player);
            this.player.l(this.player.locX - var4, this.player.locY - var6, this.player.locZ - var8);
            this.D = var29 >= -0.03125D && !this.d.ag() && !var3.d(var2.bk().g(0.0625D).a(0.0D, -0.55D, 0.0D));
            this.v = var2.locX;
            this.w = var2.locY;
            this.x = var2.locZ;
         }

      }
   }

   public void a(PacketPlayInTeleport var1) {
      class_fh.a(var1, this, this.player.x());
      if(var1.a() == this.z) {
         this.player.move(this.vec3D.b, this.vec3D.c, this.vec3D.d, this.player.yaw, this.player.pitch);
         if(this.player.isPendingTeleport()) {
            this.o = this.vec3D.b;
            this.p = this.vec3D.c;
            this.q = this.vec3D.d;
            this.player.setPendingTeleport();
         }

         this.vec3D = null;
      }
   }

   public void a(class_it var1) {
      class_fh.a(var1, this, this.player.x());
      if(b(var1)) {
         this.c("Invalid move player packet received");
      } else {
         WorldServer var2 = this.d.a(this.player.dimension);
         if(!this.player.h) {
            if(this.e == 0) {
               this.d();
            }

            if(this.vec3D != null) {
               if(this.e - this.A > 20) {
                  this.A = this.e;
                  this.a(this.vec3D.b, this.vec3D.c, this.vec3D.d, this.player.yaw, this.player.pitch);
               }

            } else {
               this.A = this.e;
               if(this.player.aH()) {
                  this.player.move(this.player.locX, this.player.locY, this.player.locZ, var1.a(this.player.yaw), var1.b(this.player.pitch));
                  this.d.getPlayerList().d(this.player);
               } else {
                  double var3 = this.player.locX;
                  double var5 = this.player.locY;
                  double var7 = this.player.locZ;
                  double var9 = this.player.locY;
                  double var11 = var1.a(this.player.locX);
                  double var13 = var1.b(this.player.locY);
                  double var15 = var1.c(this.player.locZ);
                  float var17 = var1.a(this.player.yaw);
                  float var18 = var1.b(this.player.pitch);
                  double var19 = var11 - this.l;
                  double var21 = var13 - this.m;
                  double var23 = var15 - this.n;
                  double var25 = this.player.motX * this.player.motX + this.player.motY * this.player.motY + this.player.motZ * this.player.motZ;
                  double var27 = var19 * var19 + var21 * var21 + var23 * var23;
                  ++this.F;
                  int var29 = this.F - this.G;
                  if(var29 > 5) {
                     c.debug(this.player.h_() + " is sending move packets too frequently (" + var29 + " packets since last tick)");
                     var29 = 1;
                  }

                  if(!this.player.isPendingTeleport() && (!this.player.x().U().b("disableElytraMovementCheck") || !this.player.cA())) {
                     float var30 = this.player.cA()?300.0F:100.0F;
                     if(var27 - var25 > (double)(var30 * (float)var29) && (!this.d.R() || !this.d.Q().equals(this.player.h_()))) {
                        c.warn(this.player.h_() + " moved too quickly! " + var19 + "," + var21 + "," + var23);
                        this.a(this.player.locX, this.player.locY, this.player.locZ, this.player.yaw, this.player.pitch);
                        return;
                     }
                  }

                  boolean var35 = var2.a((Entity)this.player, (AxisAlignedBB)this.player.bk().h(0.0625D)).isEmpty();
                  var19 = var11 - this.o;
                  var21 = var13 - this.p;
                  var23 = var15 - this.q;
                  if(this.player.onGround && !var1.a() && var21 > 0.0D) {
                     this.player.cg();
                  }

                  this.player.d(var19, var21, var23);
                  this.player.onGround = var1.a();
                  double var31 = var21;
                  var19 = var11 - this.player.locX;
                  var21 = var13 - this.player.locY;
                  if(var21 > -0.5D || var21 < 0.5D) {
                     var21 = 0.0D;
                  }

                  var23 = var15 - this.player.locZ;
                  var27 = var19 * var19 + var21 * var21 + var23 * var23;
                  boolean var33 = false;
                  if(!this.player.isPendingTeleport() && var27 > 0.0625D && !this.player.ck() && !this.player.c.isCreative() && this.player.c.b() != WorldSettings.EnumGamemode.SPECTATOR) {
                     var33 = true;
                     c.warn(this.player.h_() + " moved wrongly!");
                  }

                  this.player.move(var11, var13, var15, var17, var18);
                  this.player.l(this.player.locX - var3, this.player.locY - var5, this.player.locZ - var7);
                  if(!this.player.noClip && !this.player.ck()) {
                     boolean var34 = var2.a((Entity)this.player, (AxisAlignedBB)this.player.bk().h(0.0625D)).isEmpty();
                     if(var35 && (var33 || !var34)) {
                        this.a(var3, var5, var7, var17, var18);
                        return;
                     }
                  }

                  this.B = var31 >= -0.03125D;
                  this.B &= !this.d.ag() && !this.player.abilities.c;
                  this.B &= !this.player.a((MobEffectType)MobEffectList.y) && !this.player.cA() && !var2.d(this.player.bk().g(0.0625D).a(0.0D, -0.55D, 0.0D));
                  this.player.onGround = var1.a();
                  this.d.getPlayerList().d(this.player);
                  this.player.a(this.player.locY - var9, var1.a());
                  this.o = this.player.locX;
                  this.p = this.player.locY;
                  this.q = this.player.locZ;
               }
            }
         }
      }
   }

   public void a(double var1, double var3, double var5, float var7, float var8) {
      this.a(var1, var3, var5, var7, var8, Collections.emptySet());
   }

   public void a(double var1, double var3, double var5, float var7, float var8, Set var9) {
      double var10 = var9.contains(PacketPlayOutPosition.EnumPlayerTeleportFlags.X)?this.player.locX:0.0D;
      double var12 = var9.contains(PacketPlayOutPosition.EnumPlayerTeleportFlags.Y)?this.player.locY:0.0D;
      double var14 = var9.contains(PacketPlayOutPosition.EnumPlayerTeleportFlags.Z)?this.player.locZ:0.0D;
      this.vec3D = new Vec3D(var1 + var10, var3 + var12, var5 + var14);
      float var16 = var7;
      float var17 = var8;
      if(var9.contains(PacketPlayOutPosition.EnumPlayerTeleportFlags.Y_ROT)) {
         var16 = var7 + this.player.yaw;
      }

      if(var9.contains(PacketPlayOutPosition.EnumPlayerTeleportFlags.X_ROT)) {
         var17 = var8 + this.player.pitch;
      }

      if(++this.z == Integer.MAX_VALUE) {
         this.z = 0;
      }

      this.A = this.e;
      this.player.move(this.vec3D.b, this.vec3D.c, this.vec3D.d, var16, var17);
      this.player.a.a((Packet)(new PacketPlayOutPosition(var1, var3, var5, var7, var8, var9, this.z)));
   }

   public void a(PacketPlayInBlockDig var1) {
      class_fh.a(var1, this, this.player.x());
      WorldServer var2 = this.d.a(this.player.dimension);
      BlockPosition var3 = var1.a();
      this.player.D();
      ItemStack var4;
      switch(PlayerConnection.SyntheticClass_1.a[var1.c().ordinal()]) {
      case 1:
         if(!this.player.y()) {
            var4 = this.player.b((EnumHand)EnumHand.OFF_HAND);
            this.player.a((EnumHand)EnumHand.OFF_HAND, (ItemStack)this.player.b((EnumHand)EnumHand.MAIN_HAND));
            this.player.a((EnumHand)EnumHand.MAIN_HAND, (ItemStack)var4);
         }

         return;
      case 2:
         if(!this.player.y()) {
            this.player.a(false);
         }

         return;
      case 3:
         if(!this.player.y()) {
            this.player.a(true);
         }

         return;
      case 4:
         this.player.cx();
         var4 = this.player.ca();
         if(var4 != null && var4.b == 0) {
            this.player.a((EnumHand)EnumHand.MAIN_HAND, (ItemStack)null);
         }

         return;
      case 5:
      case 6:
      case 7:
         double var5 = this.player.locX - ((double)var3.p() + 0.5D);
         double var7 = this.player.locY - ((double)var3.q() + 0.5D) + 1.5D;
         double var9 = this.player.locZ - ((double)var3.r() + 0.5D);
         double var11 = var5 * var5 + var7 * var7 + var9 * var9;
         if(var11 > 36.0D) {
            return;
         } else if(var3.q() >= this.d.aj()) {
            return;
         } else {
            if(var1.c() == PacketPlayInBlockDig.EnumPlayerDigType.START_DESTROY_BLOCK) {
               if(!this.d.a(var2, var3, this.player) && var2.aj().a(var3)) {
                  this.player.c.a(var3, var1.b());
               } else {
                  this.player.a.a((Packet)(new PacketPlayOutBlockChange(var2, var3)));
               }
            } else {
               if(var1.c() == PacketPlayInBlockDig.EnumPlayerDigType.STOP_DESTROY_BLOCK) {
                  this.player.c.a(var3);
               } else if(var1.c() == PacketPlayInBlockDig.EnumPlayerDigType.ABORT_DESTROY_BLOCK) {
                  this.player.c.e();
               }

               if(var2.getType(var3).getMaterial() != Material.a) {
                  this.player.a.a((Packet)(new PacketPlayOutBlockChange(var2, var3)));
               }
            }

            return;
         }
      default:
         throw new IllegalArgumentException("Invalid player action");
      }
   }

   public void a(class_jg var1) {
      class_fh.a(var1, this, this.player.x());
      WorldServer var2 = this.d.a(this.player.dimension);
      EnumHand var3 = var1.c();
      ItemStack var4 = this.player.b((EnumHand)var3);
      BlockPosition var5 = var1.a();
      EnumDirection var6 = var1.b();
      this.player.D();
      if(var5.q() >= this.d.aj() - 1 && (var6 == EnumDirection.UP || var5.q() >= this.d.aj())) {
         ChatMessage var7 = new ChatMessage("build.tooHigh", new Object[]{Integer.valueOf(this.d.aj())});
         var7.b().a(EnumChatFormat.RED);
         this.player.a.a((Packet)(new PacketPlayOutChat(var7)));
      } else if(this.vec3D == null && this.player.e((double)var5.p() + 0.5D, (double)var5.q() + 0.5D, (double)var5.r() + 0.5D) < 64.0D && !this.d.a(var2, var5, this.player) && var2.aj().a(var5)) {
         this.player.c.interact(this.player, var2, var4, var3, var5, var6, var1.d(), var1.e(), var1.f());
      }

      this.player.a.a((Packet)(new PacketPlayOutBlockChange(var2, var5)));
      this.player.a.a((Packet)(new PacketPlayOutBlockChange(var2, var5.a(var6))));
      var4 = this.player.b((EnumHand)var3);
      if(var4 != null && var4.b == 0) {
         this.player.a((EnumHand)var3, (ItemStack)null);
         var4 = null;
      }

   }

   public void a(class_jh var1) {
      class_fh.a(var1, this, this.player.x());
      WorldServer var2 = this.d.a(this.player.dimension);
      EnumHand var3 = var1.a();
      ItemStack var4 = this.player.b((EnumHand)var3);
      this.player.D();
      if(var4 != null) {
         this.player.c.use(this.player, var2, var4, var3);
         var4 = this.player.b((EnumHand)var3);
         if(var4 != null && var4.b == 0) {
            this.player.a((EnumHand)var3, (ItemStack)null);
            var4 = null;
         }

      }
   }

   public void a(class_jf var1) {
      class_fh.a(var1, this, this.player.x());
      if(this.player.y()) {
         Entity var2 = null;
         WorldServer[] var3 = this.d.d;
         int var4 = var3.length;

         for(int var5 = 0; var5 < var4; ++var5) {
            WorldServer var6 = var3[var5];
            if(var6 != null) {
               var2 = var1.a(var6);
               if(var2 != null) {
                  break;
               }
            }
         }

         if(var2 != null) {
            this.player.e(this.player);
            this.player.p();
            if(var2.world != this.player.world) {
               WorldServer var7 = this.player.x();
               WorldServer var8 = (WorldServer)var2.world;
               this.player.dimension = var2.dimension;
               this.a((Packet)(new PacketPlayOutRespawn(this.player.dimension, var7.ae(), var7.T().t(), this.player.c.b())));
               this.d.getPlayerList().f(this.player);
               var7.f(this.player);
               this.player.dead = false;
               this.player.b(var2.locX, var2.locY, var2.locZ, var2.yaw, var2.pitch);
               if(this.player.at()) {
                  var7.a((Entity)this.player, false);
                  var8.a((Entity)this.player);
                  var8.a((Entity)this.player, false);
               }

               this.player.a((World)var8);
               this.d.getPlayerList().a(this.player, var7);
               this.player.a(var2.locX, var2.locY, var2.locZ);
               this.player.c.a(var8);
               this.d.getPlayerList().b(this.player, var8);
               this.d.getPlayerList().g(this.player);
            } else {
               this.player.a(var2.locX, var2.locY, var2.locZ);
            }
         }
      }

   }

   public void a(PacketPlayInResourcePackStatus var1) {
   }

   public void a(PacketPlayInSteer var1) {
      class_fh.a(var1, this, this.player.x());
      Entity var2 = this.player.bx();
      if(var2 instanceof EntityBoat) {
         ((EntityBoat)var2).a(var1.a(), var1.b());
      }

   }

   public void disconnect(IChatBaseComponent var1) {
      c.info(this.player.h_() + " lost connection: " + var1);
      this.d.aC();
      ChatMessage var2 = new ChatMessage("multiplayer.player.left", new Object[]{this.player.i_()});
      var2.b().a(EnumChatFormat.YELLOW);
      this.d.getPlayerList().a((IChatBaseComponent)var2);
      this.player.t();
      this.d.getPlayerList().e(this.player);
      if(this.d.R() && this.player.h_().equals(this.d.Q())) {
         c.info("Stopping singleplayer server as player logged out");
         this.d.x();
      }

   }

   public void a(final Packet var1) {
      if(var1 instanceof PacketPlayOutChat) {
         PacketPlayOutChat var2 = (PacketPlayOutChat)var1;
         EntityHuman.EnumChatVisibility var3 = this.player.C();
         if(var3 == EntityHuman.EnumChatVisibility.HIDDEN) {
            return;
         }

         if(var3 == EntityHuman.EnumChatVisibility.SYSTEM && !var2.b()) {
            return;
         }
      }

      try {
         this.a.a(var1);
      } catch (Throwable var5) {
         CrashReport var6 = CrashReport.a(var5, "Sending packet");
         CrashReportSystemDetails var4 = var6.a("Packet being sent");
         var4.a("Packet class", new Callable() {
            public String a() throws Exception {
               return var1.getClass().getCanonicalName();
            }

            // $FF: synthetic method
            public Object call() throws Exception {
               return this.a();
            }
         });
         throw new class_e(var6);
      }
   }

   public void a(class_jb var1) {
      class_fh.a(var1, this, this.player.x());
      if(var1.a() >= 0 && var1.a() < PlayerInventory.i()) {
         this.player.br.d = var1.a();
         this.player.D();
      } else {
         c.warn(this.player.h_() + " tried to set an invalid carried item");
      }
   }

   public void a(class_ij var1) {
      class_fh.a(var1, this, this.player.x());
      if(this.player.C() == EntityHuman.EnumChatVisibility.HIDDEN) {
         ChatMessage var4 = new ChatMessage("chat.cannotSend", new Object[0]);
         var4.b().a(EnumChatFormat.RED);
         this.a((Packet)(new PacketPlayOutChat(var4)));
      } else {
         this.player.D();
         String var2 = var1.a();
         var2 = StringUtils.normalizeSpace(var2);

         for(int var3 = 0; var3 < var2.length(); ++var3) {
            if(!class_f.a(var2.charAt(var3))) {
               this.c("Illegal characters in chat");
               return;
            }
         }

         if(var2.startsWith("/")) {
            this.d(var2);
         } else {
            ChatMessage var5 = new ChatMessage("chat.type.text", new Object[]{this.player.i_(), var2});
            this.d.getPlayerList().a(var5, false);
         }

         this.i += 20;
         if(this.i > 200 && !this.d.getPlayerList().h(this.player.getProfile())) {
            this.c("disconnect.spam");
         }

      }
   }

   private void d(String var1) {
      this.d.N().a(this.player, var1);
   }

   public void a(class_je var1) {
      class_fh.a(var1, this, this.player.x());
      this.player.D();
      this.player.a(var1.a());
   }

   public void a(class_iy var1) {
      class_fh.a(var1, this, this.player.x());
      this.player.D();
      class_si var4;
      switch(PlayerConnection.SyntheticClass_1.b[var1.b().ordinal()]) {
      case 1:
         this.player.d(true);
         break;
      case 2:
         this.player.d(false);
         break;
      case 3:
         this.player.e(true);
         break;
      case 4:
         this.player.e(false);
         break;
      case 5:
         this.player.a(false, true, true);
         this.vec3D = new Vec3D(this.player.locX, this.player.locY, this.player.locZ);
         break;
      case 6:
         if(this.player.bx() instanceof class_si) {
            var4 = (class_si)this.player.bx();
            int var3 = var1.c();
            if(var4.b() && var3 > 0) {
               var4.b(var3);
            }
         }
         break;
      case 7:
         if(this.player.bx() instanceof class_si) {
            var4 = (class_si)this.player.bx();
            var4.r_();
         }
         break;
      case 8:
         if(this.player.bx() instanceof class_wj) {
            ((class_wj)this.player.bx()).f((EntityHuman)this.player);
         }
         break;
      case 9:
         if(!this.player.onGround && this.player.motY < 0.0D && !this.player.cA()) {
            ItemStack var2 = this.player.a((EnumInventorySlot)EnumInventorySlot.CHEST);
            if(var2 != null && var2.b() == Items.cR && class_acw.d(var2)) {
               this.player.M();
            }
         }
         break;
      default:
         throw new IllegalArgumentException("Invalid client command!");
      }

   }

   public void a(PacketPlayInUseEntity var1) {
      class_fh.a(var1, this, this.player.x());
      WorldServer var2 = this.d.a(this.player.dimension);
      Entity var3 = var1.a((World)var2);
      this.player.D();
      if(var3 != null) {
         boolean var4 = this.player.D(var3);
         double var5 = 36.0D;
         if(!var4) {
            var5 = 9.0D;
         }

         if(this.player.h(var3) < var5) {
            EnumHand var7;
            ItemStack var8;
            if(var1.a() == PacketPlayInUseEntity.EnumEntityUseAction.INTERACT) {
               var7 = var1.b();
               var8 = this.player.b((EnumHand)var7);
               this.player.a(var3, var8, var7);
            } else if(var1.a() == PacketPlayInUseEntity.EnumEntityUseAction.INTERACT_AT) {
               var7 = var1.b();
               var8 = this.player.b((EnumHand)var7);
               var3.a((EntityHuman)this.player, (Vec3D)var1.c(), (ItemStack)var8, (EnumHand)var7);
            } else if(var1.a() == PacketPlayInUseEntity.EnumEntityUseAction.ATTACK) {
               if(var3 instanceof class_yc || var3 instanceof class_rw || var3 instanceof class_zl || var3 == this.player) {
                  this.c("Attempting to attack an invalid entity");
                  this.d.f("Player " + this.player.h_() + " tried to attack an invalid entity");
                  return;
               }

               this.player.f(var3);
            }
         }
      }

   }

   public void a(PacketPlayInClientCommand var1) {
      class_fh.a(var1, this, this.player.x());
      this.player.D();
      PacketPlayInClientCommand.EnumClientCommand var2 = var1.a();
      switch(PlayerConnection.SyntheticClass_1.c[var2.ordinal()]) {
      case 1:
         if(this.player.h) {
            this.player = this.d.getPlayerList().a(this.player, 0, true);
         } else {
            if(this.player.bP() > 0.0F) {
               return;
            }

            this.player = this.d.getPlayerList().a(this.player, 0, false);
            if(this.d.p()) {
               this.player.a(WorldSettings.EnumGamemode.SPECTATOR);
               this.player.x().U().a("spectatorsGenerateChunks", "false");
            }
         }
         break;
      case 2:
         this.player.E().a(this.player);
         break;
      case 3:
         this.player.b((Statistic)AchievementList.f);
      }

   }

   public void a(class_ip var1) {
      class_fh.a(var1, this, this.player.x());
      this.player.s();
   }

   public void a(class_io var1) {
      class_fh.a(var1, this, this.player.x());
      this.player.D();
      if(this.player.bt.d == var1.a() && this.player.bt.c(this.player)) {
         if(this.player.y()) {
            ArrayList var2 = Lists.newArrayList();

            for(int var3 = 0; var3 < this.player.bt.c.size(); ++var3) {
               var2.add(((class_abs)this.player.bt.c.get(var3)).d());
            }

            this.player.a((Container)this.player.bt, (List)var2);
         } else {
            ItemStack var7 = this.player.bt.a(var1.b(), var1.c(), var1.f(), this.player);
            if(ItemStack.b(var1.e(), var7)) {
               this.player.a.a((Packet)(new PacketPlayOutTransaction(var1.a(), var1.d(), true)));
               this.player.f = true;
               this.player.bt.b();
               this.player.r();
               this.player.f = false;
            } else {
               this.k.a(this.player.bt.d, Short.valueOf(var1.d()));
               this.player.a.a((Packet)(new PacketPlayOutTransaction(var1.a(), var1.d(), false)));
               this.player.bt.a(this.player, false);
               ArrayList var8 = Lists.newArrayList();

               for(int var4 = 0; var4 < this.player.bt.c.size(); ++var4) {
                  ItemStack var5 = ((class_abs)this.player.bt.c.get(var4)).d();
                  ItemStack var6 = var5 != null && var5.b > 0?var5:null;
                  var8.add(var6);
               }

               this.player.a((Container)this.player.bt, (List)var8);
            }
         }
      }

   }

   public void a(class_in var1) {
      class_fh.a(var1, this, this.player.x());
      this.player.D();
      if(this.player.bt.d == var1.a() && this.player.bt.c(this.player) && !this.player.y()) {
         this.player.bt.a((EntityHuman)this.player, var1.b());
         this.player.bt.b();
      }

   }

   public void a(class_jc var1) {
      class_fh.a(var1, this, this.player.x());
      if(this.player.c.isCreative()) {
         boolean var2 = var1.a() < 0;
         ItemStack var3 = var1.b();
         if(var3 != null && var3.n() && var3.o().b("BlockEntityTag", 10)) {
            NBTTagCompound var4 = var3.o().o("BlockEntityTag");
            if(var4.e("x") && var4.e("y") && var4.e("z")) {
               BlockPosition var5 = new BlockPosition(var4.h("x"), var4.h("y"), var4.h("z"));
               TileEntity var6 = this.player.world.r(var5);
               if(var6 != null) {
                  NBTTagCompound var7 = new NBTTagCompound();
                  var6.a(var7);
                  var7.q("x");
                  var7.q("y");
                  var7.q("z");
                  var3.a((String)"BlockEntityTag", (NBTTag)var7);
               }
            }
         }

         boolean var8 = var1.a() >= 1 && var1.a() <= 45;
         boolean var9 = var3 == null || var3.b() != null;
         boolean var10 = var3 == null || var3.i() >= 0 && var3.b <= 64 && var3.b > 0;
         if(var8 && var9 && var10) {
            if(var3 == null) {
               this.player.bs.a(var1.a(), (ItemStack)null);
            } else {
               this.player.bs.a(var1.a(), var3);
            }

            this.player.bs.a(this.player, true);
         } else if(var2 && var9 && var10 && this.j < 200) {
            this.j += 20;
            class_yc var11 = this.player.a((ItemStack)var3, true);
            if(var11 != null) {
               var11.j();
            }
         }
      }

   }

   public void a(class_im var1) {
      class_fh.a(var1, this, this.player.x());
      Short var2 = (Short)this.k.a(this.player.bt.d);
      if(var2 != null && var1.b() == var2.shortValue() && this.player.bt.d == var1.a() && !this.player.bt.c(this.player) && !this.player.y()) {
         this.player.bt.a(this.player, true);
      }

   }

   public void a(class_jd var1) {
      class_fh.a(var1, this, this.player.x());
      this.player.D();
      WorldServer var2 = this.d.a(this.player.dimension);
      BlockPosition var3 = var1.a();
      if(var2.e(var3)) {
         IBlockData var4 = var2.getType(var3);
         TileEntity var5 = var2.r(var3);
         if(!(var5 instanceof class_aqm)) {
            return;
         }

         class_aqm var6 = (class_aqm)var5;
         if(!var6.b() || var6.c() != this.player) {
            this.d.f("Player " + this.player.h_() + " just tried to change non-editable sign");
            return;
         }

         String[] var7 = var1.b();

         for(int var8 = 0; var8 < var7.length; ++var8) {
            var6.a[var8] = new ChatComponentText(EnumChatFormat.a(var7[var8]));
         }

         var6.v_();
         var2.a(var3, var4, var4, 3);
      }

   }

   public void a(class_is var1) {
      if(var1.a() == this.f) {
         int var2 = (int)(this.e() - this.g);
         this.player.g = (this.player.g * 3 + var2) / 4;
      }

   }

   private long e() {
      return System.nanoTime() / 1000000L;
   }

   public void a(class_iw var1) {
      class_fh.a(var1, this, this.player.x());
      this.player.abilities.b = var1.b() && this.player.abilities.c;
   }

   public void a(class_ii var1) {
      class_fh.a(var1, this, this.player.x());
      ArrayList var2 = Lists.newArrayList();
      Iterator var3 = this.d.a(this.player, var1.a(), var1.b(), var1.c()).iterator();

      while(var3.hasNext()) {
         String var4 = (String)var3.next();
         var2.add(var4);
      }

      this.player.a.a((Packet)(new PacketPlayOutTabComplete((String[])var2.toArray(new String[var2.size()]))));
   }

   public void a(class_il var1) {
      class_fh.a(var1, this, this.player.x());
      this.player.a(var1);
   }

   public void a(PacketPlayInCustomPayload var1) {
      class_fh.a(var1, this, this.player.x());
      String var2 = var1.a();
      PacketDataSerializer var3;
      ItemStack var4;
      ItemStack var5;
      if("MC|BEdit".equals(var2)) {
         var3 = new PacketDataSerializer(Unpooled.wrappedBuffer((ByteBuf)var1.b()));

         try {
            var4 = var3.k();
            if(var4 == null) {
               return;
            }

            if(!class_afb.b(var4.o())) {
               throw new IOException("Invalid book tag!");
            }

            var5 = this.player.ca();
            if(var5 != null) {
               if(var4.b() == Items.bW && var4.b() == var5.b()) {
                  var5.a((String)"pages", (NBTTag)var4.o().c("pages", 8));
               }

               return;
            }
         } catch (Exception var114) {
            c.error((String)"Couldn\'t handle book info", (Throwable)var114);
            return;
         } finally {
            var3.release();
         }

         return;
      } else {
         String var8;
         if("MC|BSign".equals(var2)) {
            var3 = new PacketDataSerializer(Unpooled.wrappedBuffer((ByteBuf)var1.b()));

            try {
               var4 = var3.k();
               if(var4 == null) {
                  return;
               }

               if(!ItemWrittenBook.b(var4.o())) {
                  throw new IOException("Invalid book tag!");
               }

               var5 = this.player.ca();
               if(var5 == null) {
                  return;
               }

               if(var4.b() == Items.bW && var5.b() == Items.bW) {
                  var5.a((String)"author", (NBTTag)(new NBTTagString(this.player.h_())));
                  var5.a((String)"title", (NBTTag)(new NBTTagString(var4.o().l("title"))));
                  NBTTagList var6 = var4.o().c("pages", 8);

                  for(int var7 = 0; var7 < var6.c(); ++var7) {
                     var8 = var6.g(var7);
                     ChatComponentText var9 = new ChatComponentText(var8);
                     var8 = IChatBaseComponent.ChatSerializer.a((IChatBaseComponent)var9);
                     var6.a(var7, new NBTTagString(var8));
                  }

                  var5.a((String)"pages", (NBTTag)var6);
                  var5.a(Items.bX);
               }
            } catch (Exception var112) {
               c.error((String)"Couldn\'t sign book", (Throwable)var112);
            } finally {
               var3.release();
            }
         } else if("MC|TrSel".equals(var2)) {
            try {
               int var116 = var1.b().readInt();
               Container var117 = this.player.bt;
               if(var117 instanceof class_abn) {
                  ((class_abn)var117).d(var116);
               }
            } catch (Exception var111) {
               c.error((String)"Couldn\'t select trade", (Throwable)var111);
            }
         } else {
            TileEntity var123;
            if("MC|AdvCmd".equals(var2)) {
               if(!this.d.ah()) {
                  this.player.a((IChatBaseComponent)(new ChatMessage("advMode.notEnabled", new Object[0])));
                  return;
               }

               if(!this.player.a(2, "") || !this.player.abilities.d) {
                  this.player.a((IChatBaseComponent)(new ChatMessage("advMode.notAllowed", new Object[0])));
                  return;
               }

               var3 = var1.b();

               try {
                  byte var118 = var3.readByte();
                  class_ahi var121 = null;
                  if(var118 == 0) {
                     var123 = this.player.world.r(new BlockPosition(var3.readInt(), var3.readInt(), var3.readInt()));
                     if(var123 instanceof class_apx) {
                        var121 = ((class_apx)var123).b();
                     }
                  } else if(var118 == 1) {
                     Entity var126 = this.player.world.a(var3.readInt());
                     if(var126 instanceof class_aai) {
                        var121 = ((class_aai)var126).j();
                     }
                  }

                  String var128 = var3.c(var3.readableBytes());
                  boolean var132 = var3.readBoolean();
                  if(var121 != null) {
                     var121.a(var128);
                     var121.a(var132);
                     if(!var132) {
                        var121.b((IChatBaseComponent)null);
                     }

                     var121.i();
                     this.player.a((IChatBaseComponent)(new ChatMessage("advMode.setCommand.success", new Object[]{var128})));
                  }
               } catch (Exception var109) {
                  c.error((String)"Couldn\'t set command block", (Throwable)var109);
               } finally {
                  var3.release();
               }
            } else if("MC|AutoCmd".equals(var2)) {
               if(!this.d.ah()) {
                  this.player.a((IChatBaseComponent)(new ChatMessage("advMode.notEnabled", new Object[0])));
                  return;
               }

               if(!this.player.a(2, "") || !this.player.abilities.d) {
                  this.player.a((IChatBaseComponent)(new ChatMessage("advMode.notAllowed", new Object[0])));
                  return;
               }

               var3 = var1.b();

               try {
                  class_ahi var120 = null;
                  class_apx var125 = null;
                  BlockPosition var129 = new BlockPosition(var3.readInt(), var3.readInt(), var3.readInt());
                  TileEntity var133 = this.player.world.r(var129);
                  if(var133 instanceof class_apx) {
                     var125 = (class_apx)var133;
                     var120 = var125.b();
                  }

                  var8 = var3.c(var3.readableBytes());
                  boolean var138 = var3.readBoolean();
                  class_apx.class_a_in_class_apx var10 = class_apx.class_a_in_class_apx.valueOf(var3.c(16));
                  boolean var11 = var3.readBoolean();
                  boolean var12 = var3.readBoolean();
                  if(var120 != null) {
                     EnumDirection var13 = (EnumDirection)this.player.world.getType(var129).get(class_akj.a);
                     IBlockData var14;
                     switch(PlayerConnection.SyntheticClass_1.d[var10.ordinal()]) {
                     case 1:
                        var14 = Blocks.dd.u();
                        this.player.world.a((BlockPosition)var129, (IBlockData)var14.set(class_akj.a, var13).set(class_akj.b, Boolean.valueOf(var11)), 2);
                        break;
                     case 2:
                        var14 = Blocks.dc.u();
                        this.player.world.a((BlockPosition)var129, (IBlockData)var14.set(class_akj.a, var13).set(class_akj.b, Boolean.valueOf(var11)), 2);
                        break;
                     case 3:
                        var14 = Blocks.bX.u();
                        this.player.world.a((BlockPosition)var129, (IBlockData)var14.set(class_akj.a, var13).set(class_akj.b, Boolean.valueOf(var11)), 2);
                     }

                     var133.z();
                     this.player.world.a(var129, var133);
                     var120.a(var8);
                     var120.a(var138);
                     if(!var138) {
                        var120.b((IChatBaseComponent)null);
                     }

                     var125.b(var12);
                     var120.i();
                     if(!UtilColor.b(var8)) {
                        this.player.a((IChatBaseComponent)(new ChatMessage("advMode.setCommand.success", new Object[]{var8})));
                     }
                  }
               } catch (Exception var107) {
                  c.error((String)"Couldn\'t set command block", (Throwable)var107);
               } finally {
                  var3.release();
               }
            } else {
               int var122;
               if("MC|Beacon".equals(var2)) {
                  if(this.player.bt instanceof class_aaw) {
                     try {
                        var3 = var1.b();
                        var122 = var3.readInt();
                        int var130 = var3.readInt();
                        class_aaw var131 = (class_aaw)this.player.bt;
                        class_abs var135 = var131.a(0);
                        if(var135.e()) {
                           var135.a(1);
                           IInventory var137 = var131.e();
                           var137.b(1, var122);
                           var137.b(2, var130);
                           var137.v_();
                        }
                     } catch (Exception var106) {
                        c.error((String)"Couldn\'t set beacon", (Throwable)var106);
                     }
                  }
               } else if("MC|ItemName".equals(var2)) {
                  if(this.player.bt instanceof ContainerAnvil) {
                     ContainerAnvil var119 = (ContainerAnvil)this.player.bt;
                     if(var1.b() != null && var1.b().readableBytes() >= 1) {
                        String var124 = class_f.a(var1.b().c(32767));
                        if(var124.length() <= 30) {
                           var119.a(var124);
                        }
                     } else {
                        var119.a("");
                     }
                  }
               } else if("MC|Struct".equals(var2)) {
                  var3 = var1.b();

                  try {
                     if(this.player.a(4, "") && this.player.abilities.d) {
                        BlockPosition var127 = new BlockPosition(var3.readInt(), var3.readInt(), var3.readInt());
                        IBlockData var134 = this.player.world.getType(var127);
                        var123 = this.player.world.r(var127);
                        if(var123 instanceof class_aqo) {
                           class_aqo var136 = (class_aqo)var123;
                           byte var139 = var3.readByte();
                           String var140 = var3.c(32);
                           var136.a(class_aqo.class_a_in_class_aqo.valueOf(var140));
                           var136.a(var3.c(64));
                           var136.b(new BlockPosition(var3.readInt(), var3.readInt(), var3.readInt()));
                           var136.c(new BlockPosition(var3.readInt(), var3.readInt(), var3.readInt()));
                           String var141 = var3.c(32);
                           var136.a(class_amq.valueOf(var141));
                           String var142 = var3.c(32);
                           var136.a(class_aod.valueOf(var142));
                           var136.b(var3.c(128));
                           var136.a(var3.readBoolean());
                           if(var139 == 2) {
                              if(var136.m()) {
                                 this.player.b((IChatBaseComponent)(new ChatComponentText("Structure saved")));
                              } else {
                                 this.player.b((IChatBaseComponent)(new ChatComponentText("Structure NOT saved")));
                              }
                           } else if(var139 == 3) {
                              if(var136.n()) {
                                 this.player.b((IChatBaseComponent)(new ChatComponentText("Structure loaded")));
                              } else {
                                 this.player.b((IChatBaseComponent)(new ChatComponentText("Structure prepared")));
                              }
                           } else if(var139 == 4 && var136.l()) {
                              this.player.b((IChatBaseComponent)(new ChatComponentText("Size detected")));
                           }

                           var136.v_();
                           this.player.world.a(var127, var134, var134, 3);
                        }
                     }
                  } catch (Exception var104) {
                     c.error((String)"Couldn\'t set structure block", (Throwable)var104);
                  } finally {
                     var3.release();
                  }
               } else if("MC|PickItem".equals(var2)) {
                  var3 = var1.b();

                  try {
                     var122 = var3.readVarInt();
                     this.player.br.d(var122);
                     this.player.a.a((Packet)(new PacketPlayOutSetSlot(-2, this.player.br.d, this.player.br.a(this.player.br.d))));
                     this.player.a.a((Packet)(new PacketPlayOutSetSlot(-2, var122, this.player.br.a(var122))));
                     this.player.a.a((Packet)(new PacketPlayOutHeldItemSlot(this.player.br.d)));
                  } catch (Exception var102) {
                     c.error((String)"Couldn\'t pick item", (Throwable)var102);
                  } finally {
                     var3.release();
                  }
               }
            }
         }
      }

   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b;
      // $FF: synthetic field
      static final int[] c;
      // $FF: synthetic field
      static final int[] d = new int[class_apx.class_a_in_class_apx.values().length];

      static {
         try {
            d[class_apx.class_a_in_class_apx.SEQUENCE.ordinal()] = 1;
         } catch (NoSuchFieldError var22) {
            ;
         }

         try {
            d[class_apx.class_a_in_class_apx.AUTO.ordinal()] = 2;
         } catch (NoSuchFieldError var21) {
            ;
         }

         try {
            d[class_apx.class_a_in_class_apx.REDSTONE.ordinal()] = 3;
         } catch (NoSuchFieldError var20) {
            ;
         }

         c = new int[PacketPlayInClientCommand.EnumClientCommand.values().length];

         try {
            c[PacketPlayInClientCommand.EnumClientCommand.PERFORM_RESPAWN.ordinal()] = 1;
         } catch (NoSuchFieldError var19) {
            ;
         }

         try {
            c[PacketPlayInClientCommand.EnumClientCommand.REQUEST_STATS.ordinal()] = 2;
         } catch (NoSuchFieldError var18) {
            ;
         }

         try {
            c[PacketPlayInClientCommand.EnumClientCommand.OPEN_INVENTORY_ACHIEVEMENT.ordinal()] = 3;
         } catch (NoSuchFieldError var17) {
            ;
         }

         b = new int[class_iy.class_a_in_class_iy.values().length];

         try {
            b[class_iy.class_a_in_class_iy.START_SNEAKING.ordinal()] = 1;
         } catch (NoSuchFieldError var16) {
            ;
         }

         try {
            b[class_iy.class_a_in_class_iy.STOP_SNEAKING.ordinal()] = 2;
         } catch (NoSuchFieldError var15) {
            ;
         }

         try {
            b[class_iy.class_a_in_class_iy.START_SPRINTING.ordinal()] = 3;
         } catch (NoSuchFieldError var14) {
            ;
         }

         try {
            b[class_iy.class_a_in_class_iy.STOP_SPRINTING.ordinal()] = 4;
         } catch (NoSuchFieldError var13) {
            ;
         }

         try {
            b[class_iy.class_a_in_class_iy.STOP_SLEEPING.ordinal()] = 5;
         } catch (NoSuchFieldError var12) {
            ;
         }

         try {
            b[class_iy.class_a_in_class_iy.START_RIDING_JUMP.ordinal()] = 6;
         } catch (NoSuchFieldError var11) {
            ;
         }

         try {
            b[class_iy.class_a_in_class_iy.STOP_RIDING_JUMP.ordinal()] = 7;
         } catch (NoSuchFieldError var10) {
            ;
         }

         try {
            b[class_iy.class_a_in_class_iy.OPEN_INVENTORY.ordinal()] = 8;
         } catch (NoSuchFieldError var9) {
            ;
         }

         try {
            b[class_iy.class_a_in_class_iy.START_FALL_FLYING.ordinal()] = 9;
         } catch (NoSuchFieldError var8) {
            ;
         }

         a = new int[PacketPlayInBlockDig.EnumPlayerDigType.values().length];

         try {
            a[PacketPlayInBlockDig.EnumPlayerDigType.SWAP_HELD_ITEMS.ordinal()] = 1;
         } catch (NoSuchFieldError var7) {
            ;
         }

         try {
            a[PacketPlayInBlockDig.EnumPlayerDigType.DROP_ITEM.ordinal()] = 2;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[PacketPlayInBlockDig.EnumPlayerDigType.DROP_ALL_ITEMS.ordinal()] = 3;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[PacketPlayInBlockDig.EnumPlayerDigType.RELEASE_USE_ITEM.ordinal()] = 4;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[PacketPlayInBlockDig.EnumPlayerDigType.START_DESTROY_BLOCK.ordinal()] = 5;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[PacketPlayInBlockDig.EnumPlayerDigType.ABORT_DESTROY_BLOCK.ordinal()] = 6;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[PacketPlayInBlockDig.EnumPlayerDigType.STOP_DESTROY_BLOCK.ordinal()] = 7;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
