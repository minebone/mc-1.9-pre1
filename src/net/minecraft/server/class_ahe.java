package net.minecraft.server;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MerchantRecipe;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahg;

public interface class_ahe {
   void a_(EntityHuman var1);

   EntityHuman t_();

   class_ahg b_(EntityHuman var1);

   void a(MerchantRecipe var1);

   void a(ItemStack var1);

   IChatBaseComponent i_();
}
