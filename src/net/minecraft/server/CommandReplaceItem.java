package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MojangsonParser;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cb;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ec;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.IInventory;
import net.minecraft.server.EnumInventorySlot;

public class CommandReplaceItem extends CommandAbstract {
   private static final Map a = Maps.newHashMap();

   public String c() {
      return "replaceitem";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.replaceitem.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 1) {
         throw new class_cf("commands.replaceitem.usage", new Object[0]);
      } else {
         boolean var4;
         if(var3[0].equals("entity")) {
            var4 = false;
         } else {
            if(!var3[0].equals("block")) {
               throw new class_cf("commands.replaceitem.usage", new Object[0]);
            }

            var4 = true;
         }

         byte var5;
         if(var4) {
            if(var3.length < 6) {
               throw new class_cf("commands.replaceitem.block.usage", new Object[0]);
            }

            var5 = 4;
         } else {
            if(var3.length < 4) {
               throw new class_cf("commands.replaceitem.entity.usage", new Object[0]);
            }

            var5 = 2;
         }

         String var6 = var3[var5];
         int var18 = var5 + 1;
         int var7 = this.e(var3[var5]);

         Item var8;
         try {
            var8 = a(var2, var3[var18]);
         } catch (class_cb var17) {
            if(Block.b(var3[var18]) != Blocks.AIR) {
               throw var17;
            }

            var8 = null;
         }

         ++var18;
         int var9 = var3.length > var18?a(var3[var18++], 1, 64):1;
         int var10 = var3.length > var18?a(var3[var18++]):0;
         ItemStack var11 = new ItemStack(var8, var9, var10);
         if(var3.length > var18) {
            String var12 = a(var2, var3, var18).c();

            try {
               var11.d(MojangsonParser.a(var12));
            } catch (class_ec var16) {
               throw new class_bz("commands.replaceitem.tagError", new Object[]{var16.getMessage()});
            }
         }

         if(var11.b() == null) {
            var11 = null;
         }

         if(var4) {
            var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_ITEMS, 0);
            BlockPosition var20 = a(var2, var3, 1, false);
            World var13 = var2.e();
            TileEntity var14 = var13.r(var20);
            if(var14 == null || !(var14 instanceof IInventory)) {
               throw new class_bz("commands.replaceitem.noContainer", new Object[]{Integer.valueOf(var20.p()), Integer.valueOf(var20.q()), Integer.valueOf(var20.r())});
            }

            IInventory var15 = (IInventory)var14;
            if(var7 >= 0 && var7 < var15.u_()) {
               var15.a(var7, var11);
            }
         } else {
            Entity var19 = b(var1, var2, var3[1]);
            var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_ITEMS, 0);
            if(var19 instanceof EntityHuman) {
               ((EntityHuman)var19).bs.b();
            }

            if(!var19.c(var7, var11)) {
               throw new class_bz("commands.replaceitem.failed", new Object[]{var6, Integer.valueOf(var9), var11 == null?"Air":var11.B()});
            }

            if(var19 instanceof EntityHuman) {
               ((EntityHuman)var19).bs.b();
            }
         }

         var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_ITEMS, var9);
         a(var2, this, "commands.replaceitem.success", new Object[]{var6, Integer.valueOf(var9), var11 == null?"Air":var11.B()});
      }
   }

   private int e(String var1) throws class_bz {
      if(!a.containsKey(var1)) {
         throw new class_bz("commands.generic.parameter.invalid", new Object[]{var1});
      } else {
         return ((Integer)a.get(var1)).intValue();
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, new String[]{"entity", "block"}):(var3.length == 2 && var3[0].equals("entity")?a(var3, var1.J()):(var3.length >= 2 && var3.length <= 4 && var3[0].equals("block")?a(var3, 1, var4):(var3.length == 3 && var3[0].equals("entity") || var3.length == 5 && var3[0].equals("block")?a(var3, a.keySet()):((var3.length != 4 || !var3[0].equals("entity")) && (var3.length != 6 || !var3[0].equals("block"))?Collections.emptyList():a(var3, Item.f.c())))));
   }

   public boolean b(String[] var1, int var2) {
      return var1.length > 0 && var1[0].equals("entity") && var2 == 1;
   }

   static {
      int var0;
      for(var0 = 0; var0 < 54; ++var0) {
         a.put("slot.container." + var0, Integer.valueOf(var0));
      }

      for(var0 = 0; var0 < 9; ++var0) {
         a.put("slot.hotbar." + var0, Integer.valueOf(var0));
      }

      for(var0 = 0; var0 < 27; ++var0) {
         a.put("slot.inventory." + var0, Integer.valueOf(9 + var0));
      }

      for(var0 = 0; var0 < 27; ++var0) {
         a.put("slot.enderchest." + var0, Integer.valueOf(200 + var0));
      }

      for(var0 = 0; var0 < 8; ++var0) {
         a.put("slot.villager." + var0, Integer.valueOf(300 + var0));
      }

      for(var0 = 0; var0 < 15; ++var0) {
         a.put("slot.horse." + var0, Integer.valueOf(500 + var0));
      }

      a.put("slot.weapon", Integer.valueOf(98));
      a.put("slot.weapon.mainhand", Integer.valueOf(98));
      a.put("slot.weapon.offhand", Integer.valueOf(99));
      a.put("slot.armor.head", Integer.valueOf(100 + EnumInventorySlot.HEAD.b()));
      a.put("slot.armor.chest", Integer.valueOf(100 + EnumInventorySlot.CHEST.b()));
      a.put("slot.armor.legs", Integer.valueOf(100 + EnumInventorySlot.LEGS.b()));
      a.put("slot.armor.feet", Integer.valueOf(100 + EnumInventorySlot.FEET.b()));
      a.put("slot.horse.saddle", Integer.valueOf(400));
      a.put("slot.horse.armor", Integer.valueOf(401));
      a.put("slot.horse.chest", Integer.valueOf(499));
   }
}
