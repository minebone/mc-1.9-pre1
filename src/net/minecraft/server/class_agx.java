package net.minecraft.server;

import net.minecraft.server.DamageSource;
import net.minecraft.server.Enchantment;
import net.minecraft.server.class_agm;
import net.minecraft.server.class_agn;
import net.minecraft.server.class_agp;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;

public class class_agx extends Enchantment {
   public final class_agx.class_a_in_class_agx a;

   public class_agx(Enchantment.class_a_in_class_agl var1, class_agx.class_a_in_class_agx var2, EnumInventorySlot... var3) {
      super(var1, class_agm.ARMOR, var3);
      this.a = var2;
      if(var2 == class_agx.class_a_in_class_agx.FALL) {
         this.c = class_agm.ARMOR_FEET;
      }

   }

   public int a(int var1) {
      return this.a.b() + (var1 - 1) * this.a.c();
   }

   public int b(int var1) {
      return this.a(var1) + this.a.c();
   }

   public int b() {
      return 4;
   }

   public int a(int var1, DamageSource var2) {
      return var2.g()?0:(this.a == class_agx.class_a_in_class_agx.ALL?var1:(this.a == class_agx.class_a_in_class_agx.FIRE && var2.o()?var1 * 2:(this.a == class_agx.class_a_in_class_agx.FALL && var2 == DamageSource.i?var1 * 3:(this.a == class_agx.class_a_in_class_agx.EXPLOSION && var2.c()?var1 * 2:(this.a == class_agx.class_a_in_class_agx.PROJECTILE && var2.a()?var1 * 2:0)))));
   }

   public String a() {
      return "enchantment.protect." + this.a.a();
   }

   public boolean a(Enchantment var1) {
      if(var1 instanceof class_agx) {
         class_agx var2 = (class_agx)var1;
         return this.a == var2.a?false:this.a == class_agx.class_a_in_class_agx.FALL || var2.a == class_agx.class_a_in_class_agx.FALL;
      } else {
         return super.a(var1);
      }
   }

   public static int a(class_rz var0, int var1) {
      int var2 = class_agn.a(class_agp.b, var0);
      if(var2 > 0) {
         var1 -= MathHelper.d((float)var1 * (float)var2 * 0.15F);
      }

      return var1;
   }

   public static double a(class_rz var0, double var1) {
      int var3 = class_agn.a(class_agp.d, var0);
      if(var3 > 0) {
         var1 -= (double)MathHelper.c(var1 * (double)((float)var3 * 0.15F));
      }

      return var1;
   }

   public static enum class_a_in_class_agx {
      ALL("all", 1, 11, 20),
      FIRE("fire", 10, 8, 12),
      FALL("fall", 5, 6, 10),
      EXPLOSION("explosion", 5, 8, 12),
      PROJECTILE("projectile", 3, 6, 15);

      private final String f;
      private final int g;
      private final int h;
      private final int i;

      private class_a_in_class_agx(String var3, int var4, int var5, int var6) {
         this.f = var3;
         this.g = var4;
         this.h = var5;
         this.i = var6;
      }

      public String a() {
         return this.f;
      }

      public int b() {
         return this.g;
      }

      public int c() {
         return this.h;
      }
   }
}
