package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.class_atx;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_wc;

public class class_ail extends BiomeBase {
   public class_ail(BiomeBase.class_a_in_class_aif var1) {
      super(var1);
      this.v.clear();
      this.r = Blocks.m.u();
      this.s = Blocks.m.u();
      this.t.z = -999;
      this.t.C = 2;
      this.t.E = 50;
      this.t.F = 10;
      this.v.clear();
      this.v.add(new BiomeBase.BiomeMeta(class_wc.class, 4, 2, 3));
   }

   public void a(World var1, Random var2, BlockPosition var3) {
      super.a(var1, var2, var3);
      if(var2.nextInt(1000) == 0) {
         int var4 = var2.nextInt(16) + 8;
         int var5 = var2.nextInt(16) + 8;
         BlockPosition var6 = var1.l(var3.a(var4, 0, var5)).a();
         (new class_atx()).b(var1, var2, var6);
      }

   }
}
