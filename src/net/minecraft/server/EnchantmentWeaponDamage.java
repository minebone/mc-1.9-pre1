package net.minecraft.server;

import net.minecraft.server.Enchantment;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumMonsterType;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.class_aby;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agm;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;

public class EnchantmentWeaponDamage extends Enchantment {
   private static final String[] e = new String[]{"all", "undead", "arthropods"};
   private static final int[] f = new int[]{1, 5, 5};
   private static final int[] g = new int[]{11, 8, 8};
   private static final int[] h = new int[]{20, 20, 20};
   public final int a;

   public EnchantmentWeaponDamage(Enchantment.class_a_in_class_agl var1, int var2, EnumInventorySlot... var3) {
      super(var1, class_agm.WEAPON, var3);
      this.a = var2;
   }

   public int a(int var1) {
      return f[this.a] + (var1 - 1) * g[this.a];
   }

   public int b(int var1) {
      return this.a(var1) + h[this.a];
   }

   public int b() {
      return 5;
   }

   public float a(int var1, EnumMonsterType var2) {
      return this.a == 0?1.0F + (float)Math.max(0, var1 - 1) * 0.5F:(this.a == 1 && var2 == EnumMonsterType.UNDEAD?(float)var1 * 2.5F:(this.a == 2 && var2 == EnumMonsterType.ARTHROPOD?(float)var1 * 2.5F:0.0F));
   }

   public String a() {
      return "enchantment.damage." + e[this.a];
   }

   public boolean a(Enchantment var1) {
      return !(var1 instanceof EnchantmentWeaponDamage);
   }

   public boolean a(ItemStack var1) {
      return var1.b() instanceof class_aby?true:super.a(var1);
   }

   public void a(class_rz var1, Entity var2, int var3) {
      if(var2 instanceof class_rz) {
         class_rz var4 = (class_rz)var2;
         if(this.a == 2 && var4.bZ() == EnumMonsterType.ARTHROPOD) {
            int var5 = 20 + var1.bE().nextInt(10 * var3);
            var4.c(new MobEffect(MobEffectList.b, var5, 3));
         }
      }

   }
}
