package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aho;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_yd;
import net.minecraft.server.class_zl;

public class class_apd extends Block {
   public static final class_arm a = class_arm.a("explode");

   public class_apd() {
      super(Material.u);
      this.w(this.A.b().set(a, Boolean.valueOf(false)));
      this.a(CreativeModeTab.d);
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      super.c(var1, var2, var3);
      if(var1.y(var2)) {
         this.d(var1, var2, var3.set(a, Boolean.valueOf(true)));
         var1.g(var2);
      }

   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(var1.y(var2)) {
         this.d(var1, var2, var3.set(a, Boolean.valueOf(true)));
         var1.g(var2);
      }

   }

   public void a(World var1, BlockPosition var2, class_aho var3) {
      if(!var1.E) {
         class_yd var4 = new class_yd(var1, (double)((float)var2.p() + 0.5F), (double)var2.q(), (double)((float)var2.r() + 0.5F), var3.c());
         var4.a((short)(var1.r.nextInt(var4.l() / 4) + var4.l() / 8));
         var1.a((Entity)var4);
      }
   }

   public void d(World var1, BlockPosition var2, IBlockData var3) {
      this.a(var1, var2, var3, (class_rz)null);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, class_rz var4) {
      if(!var1.E) {
         if(((Boolean)var3.get(a)).booleanValue()) {
            class_yd var5 = new class_yd(var1, (double)((float)var2.p() + 0.5F), (double)var2.q(), (double)((float)var2.r() + 0.5F), var4);
            var1.a((Entity)var5);
            var1.a((EntityHuman)null, var5.locX, var5.locY, var5.locZ, class_ng.gd, EnumSoundCategory.BLOCKS, 1.0F, 1.0F);
         }

      }
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(var6 != null && (var6.b() == Items.d || var6.b() == Items.bV)) {
         this.a(var1, var2, var3.set(a, Boolean.valueOf(true)), (class_rz)var4);
         var1.a((BlockPosition)var2, (IBlockData)Blocks.AIR.u(), 11);
         if(var6.b() == Items.d) {
            var6.a(1, (class_rz)var4);
         } else if(!var4.abilities.d) {
            --var6.b;
         }

         return true;
      } else {
         return super.a(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Entity var4) {
      if(!var1.E && var4 instanceof class_zl) {
         class_zl var5 = (class_zl)var4;
         if(var5.aG()) {
            this.a(var1, var2, var1.getType(var2).set(a, Boolean.valueOf(true)), var5.e instanceof class_rz?(class_rz)var5.e:null);
            var1.g(var2);
         }
      }

   }

   public boolean a(class_aho var1) {
      return false;
   }

   public IBlockData a(int var1) {
      return this.u().set(a, Boolean.valueOf((var1 & 1) > 0));
   }

   public int e(IBlockData var1) {
      return ((Boolean)var1.get(a)).booleanValue()?1:0;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }
}
