package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.ExceptionEntityNotFound;
import net.minecraft.server.ExceptionPlayerNotFound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.PlayerSelector;
import net.minecraft.server.ScoreboardTeamBase;
import net.minecraft.server.World;
import net.minecraft.server.Material;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.MathHelper;

public class CommandSpreadPlayers extends CommandAbstract {
   public String c() {
      return "spreadplayers";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.spreadplayers.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 6) {
         throw new class_cf("commands.spreadplayers.usage", new Object[0]);
      } else {
         byte var4 = 0;
         BlockPosition var5 = var2.c();
         double var10000 = (double)var5.p();
         int var18 = var4 + 1;
         double var6 = b(var10000, var3[var4], true);
         double var8 = b((double)var5.r(), var3[var18++], true);
         double var10 = a(var3[var18++], 0.0D);
         double var12 = a(var3[var18++], var10 + 1.0D);
         boolean var14 = d(var3[var18++]);
         ArrayList var15 = Lists.newArrayList();

         while(var18 < var3.length) {
            String var16 = var3[var18++];
            if(PlayerSelector.b(var16)) {
               List var17 = PlayerSelector.b(var2, var16, Entity.class);
               if(var17.isEmpty()) {
                  throw new ExceptionEntityNotFound();
               }

               var15.addAll(var17);
            } else {
               EntityPlayer var19 = var1.getPlayerList().a(var16);
               if(var19 == null) {
                  throw new ExceptionPlayerNotFound();
               }

               var15.add(var19);
            }
         }

         var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_ENTITIES, var15.size());
         if(var15.isEmpty()) {
            throw new ExceptionEntityNotFound();
         } else {
            var2.a(new ChatMessage("commands.spreadplayers.spreading." + (var14?"teams":"players"), new Object[]{Integer.valueOf(var15.size()), Double.valueOf(var12), Double.valueOf(var6), Double.valueOf(var8), Double.valueOf(var10)}));
            this.a(var2, var15, new CommandSpreadPlayers.class_a_in_class_bi(var6, var8), var10, var12, ((Entity)var15.get(0)).world, var14);
         }
      }
   }

   private void a(ICommandListener var1, List var2, CommandSpreadPlayers.class_a_in_class_bi var3, double var4, double var6, World var8, boolean var9) throws class_bz {
      Random var10 = new Random();
      double var11 = var3.a - var6;
      double var13 = var3.b - var6;
      double var15 = var3.a + var6;
      double var17 = var3.b + var6;
      CommandSpreadPlayers.class_a_in_class_bi[] var19 = this.a(var10, var9?this.b(var2):var2.size(), var11, var13, var15, var17);
      int var20 = this.a(var3, var4, var8, var10, var11, var13, var15, var17, var19, var9);
      double var21 = this.a(var2, var8, var19, var9);
      a(var1, this, "commands.spreadplayers.success." + (var9?"teams":"players"), new Object[]{Integer.valueOf(var19.length), Double.valueOf(var3.a), Double.valueOf(var3.b)});
      if(var19.length > 1) {
         var1.a(new ChatMessage("commands.spreadplayers.info." + (var9?"teams":"players"), new Object[]{String.format("%.2f", new Object[]{Double.valueOf(var21)}), Integer.valueOf(var20)}));
      }

   }

   private int b(List var1) {
      HashSet var2 = Sets.newHashSet();
      Iterator var3 = var1.iterator();

      while(var3.hasNext()) {
         Entity var4 = (Entity)var3.next();
         if(var4 instanceof EntityHuman) {
            var2.add(((EntityHuman)var4).aN());
         } else {
            var2.add((Object)null);
         }
      }

      return var2.size();
   }

   private int a(CommandSpreadPlayers.class_a_in_class_bi var1, double var2, World var4, Random var5, double var6, double var8, double var10, double var12, CommandSpreadPlayers.class_a_in_class_bi[] var14, boolean var15) throws class_bz {
      boolean var16 = true;
      double var18 = 3.4028234663852886E38D;

      int var17;
      for(var17 = 0; var17 < 10000 && var16; ++var17) {
         var16 = false;
         var18 = 3.4028234663852886E38D;

         int var22;
         CommandSpreadPlayers.class_a_in_class_bi var23;
         for(int var20 = 0; var20 < var14.length; ++var20) {
            CommandSpreadPlayers.class_a_in_class_bi var21 = var14[var20];
            var22 = 0;
            var23 = new CommandSpreadPlayers.class_a_in_class_bi();

            for(int var24 = 0; var24 < var14.length; ++var24) {
               if(var20 != var24) {
                  CommandSpreadPlayers.class_a_in_class_bi var25 = var14[var24];
                  double var26 = var21.a(var25);
                  var18 = Math.min(var26, var18);
                  if(var26 < var2) {
                     ++var22;
                     var23.a += var25.a - var21.a;
                     var23.b += var25.b - var21.b;
                  }
               }
            }

            if(var22 > 0) {
               var23.a /= (double)var22;
               var23.b /= (double)var22;
               double var30 = (double)var23.b();
               if(var30 > 0.0D) {
                  var23.a();
                  var21.b(var23);
               } else {
                  var21.a(var5, var6, var8, var10, var12);
               }

               var16 = true;
            }

            if(var21.a(var6, var8, var10, var12)) {
               var16 = true;
            }
         }

         if(!var16) {
            CommandSpreadPlayers.class_a_in_class_bi[] var28 = var14;
            int var29 = var14.length;

            for(var22 = 0; var22 < var29; ++var22) {
               var23 = var28[var22];
               if(!var23.b(var4)) {
                  var23.a(var5, var6, var8, var10, var12);
                  var16 = true;
               }
            }
         }
      }

      if(var17 >= 10000) {
         throw new class_bz("commands.spreadplayers.failure." + (var15?"teams":"players"), new Object[]{Integer.valueOf(var14.length), Double.valueOf(var1.a), Double.valueOf(var1.b), String.format("%.2f", new Object[]{Double.valueOf(var18)})});
      } else {
         return var17;
      }
   }

   private double a(List var1, World var2, CommandSpreadPlayers.class_a_in_class_bi[] var3, boolean var4) {
      double var5 = 0.0D;
      int var7 = 0;
      HashMap var8 = Maps.newHashMap();

      for(int var9 = 0; var9 < var1.size(); ++var9) {
         Entity var10 = (Entity)var1.get(var9);
         CommandSpreadPlayers.class_a_in_class_bi var11;
         if(var4) {
            ScoreboardTeamBase var12 = var10 instanceof EntityHuman?((EntityHuman)var10).aN():null;
            if(!var8.containsKey(var12)) {
               var8.put(var12, var3[var7++]);
            }

            var11 = (CommandSpreadPlayers.class_a_in_class_bi)var8.get(var12);
         } else {
            var11 = var3[var7++];
         }

         var10.a((double)((float)MathHelper.c(var11.a) + 0.5F), (double)var11.a(var2), (double)MathHelper.c(var11.b) + 0.5D);
         double var17 = Double.MAX_VALUE;

         for(int var14 = 0; var14 < var3.length; ++var14) {
            if(var11 != var3[var14]) {
               double var15 = var11.a(var3[var14]);
               var17 = Math.min(var15, var17);
            }
         }

         var5 += var17;
      }

      var5 /= (double)var1.size();
      return var5;
   }

   private CommandSpreadPlayers.class_a_in_class_bi[] a(Random var1, int var2, double var3, double var5, double var7, double var9) {
      CommandSpreadPlayers.class_a_in_class_bi[] var11 = new CommandSpreadPlayers.class_a_in_class_bi[var2];

      for(int var12 = 0; var12 < var11.length; ++var12) {
         CommandSpreadPlayers.class_a_in_class_bi var13 = new CommandSpreadPlayers.class_a_in_class_bi();
         var13.a(var1, var3, var5, var7, var9);
         var11[var12] = var13;
      }

      return var11;
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length >= 1 && var3.length <= 2?b(var3, 0, var4):Collections.emptyList();
   }

   static class class_a_in_class_bi {
      double a;
      double b;

      class_a_in_class_bi() {
      }

      class_a_in_class_bi(double var1, double var3) {
         this.a = var1;
         this.b = var3;
      }

      double a(CommandSpreadPlayers.class_a_in_class_bi var1) {
         double var2 = this.a - var1.a;
         double var4 = this.b - var1.b;
         return Math.sqrt(var2 * var2 + var4 * var4);
      }

      void a() {
         double var1 = (double)this.b();
         this.a /= var1;
         this.b /= var1;
      }

      float b() {
         return MathHelper.a(this.a * this.a + this.b * this.b);
      }

      public void b(CommandSpreadPlayers.class_a_in_class_bi var1) {
         this.a -= var1.a;
         this.b -= var1.b;
      }

      public boolean a(double var1, double var3, double var5, double var7) {
         boolean var9 = false;
         if(this.a < var1) {
            this.a = var1;
            var9 = true;
         } else if(this.a > var5) {
            this.a = var5;
            var9 = true;
         }

         if(this.b < var3) {
            this.b = var3;
            var9 = true;
         } else if(this.b > var7) {
            this.b = var7;
            var9 = true;
         }

         return var9;
      }

      public int a(World var1) {
         BlockPosition var2 = new BlockPosition(this.a, 256.0D, this.b);

         do {
            if(var2.q() <= 0) {
               return 257;
            }

            var2 = var2.b();
         } while(var1.getType(var2).getMaterial() == Material.a);

         return var2.q() + 1;
      }

      public boolean b(World var1) {
         BlockPosition var2 = new BlockPosition(this.a, 256.0D, this.b);

         Material var3;
         do {
            if(var2.q() <= 0) {
               return false;
            }

            var2 = var2.b();
            var3 = var1.getType(var2).getMaterial();
         } while(var3 == Material.a);

         return !var3.d() && var3 != Material.o;
      }

      public void a(Random var1, double var2, double var4, double var6, double var8) {
         this.a = MathHelper.a(var1, var2, var6);
         this.b = MathHelper.a(var1, var4, var8);
      }
   }
}
