package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.BlockTallPlant;
import net.minecraft.server.Blocks;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aju;
import net.minecraft.server.class_ajx;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_or;

public class BlockLongGrass extends class_ajx implements class_aju {
   public static final BlockStateEnum a = BlockStateEnum.a("type", BlockLongGrass.EnumTallGrassType.class);
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.09999999403953552D, 0.0D, 0.09999999403953552D, 0.8999999761581421D, 0.800000011920929D, 0.8999999761581421D);

   protected BlockLongGrass() {
      super(Material.l);
      this.w(this.A.b().set(a, BlockLongGrass.EnumTallGrassType.DEAD_BUSH));
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return c;
   }

   public boolean f(World var1, BlockPosition var2, IBlockData var3) {
      return this.i(var1.getType(var2.b()));
   }

   public boolean a(class_ahw var1, BlockPosition var2) {
      return true;
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return var2.nextInt(8) == 0?Items.P:null;
   }

   public int a(int var1, Random var2) {
      return 1 + var2.nextInt(var1 * 2 + 1);
   }

   public void a(World var1, EntityHuman var2, BlockPosition var3, IBlockData var4, TileEntity var5, ItemStack var6) {
      if(!var1.E && var6 != null && var6.b() == Items.bl) {
         var2.b(StatisticList.a((Block)this));
         a((World)var1, (BlockPosition)var3, (ItemStack)(new ItemStack(Blocks.H, 1, ((BlockLongGrass.EnumTallGrassType)var4.get(a)).a())));
      } else {
         super.a(var1, var2, var3, var4, var5, var6);
      }

   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(this, 1, var3.getBlock().e(var3));
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, boolean var4) {
      return var3.get(a) != BlockLongGrass.EnumTallGrassType.DEAD_BUSH;
   }

   public boolean a(World var1, Random var2, BlockPosition var3, IBlockData var4) {
      return true;
   }

   public void b(World var1, Random var2, BlockPosition var3, IBlockData var4) {
      BlockTallPlant.EnumTallFlowerVariants var5 = BlockTallPlant.EnumTallFlowerVariants.GRASS;
      if(var4.get(a) == BlockLongGrass.EnumTallGrassType.FERN) {
         var5 = BlockTallPlant.EnumTallFlowerVariants.FERN;
      }

      if(Blocks.cF.a(var1, var3)) {
         Blocks.cF.a(var1, var3, var5, 2);
      }

   }

   public IBlockData a(int var1) {
      return this.u().set(a, BlockLongGrass.EnumTallGrassType.a(var1));
   }

   public int e(IBlockData var1) {
      return ((BlockLongGrass.EnumTallGrassType)var1.get(a)).a();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   public static enum EnumTallGrassType implements class_or {
      DEAD_BUSH(0, "dead_bush"),
      GRASS(1, "tall_grass"),
      FERN(2, "fern");

      private static final BlockLongGrass.EnumTallGrassType[] d = new BlockLongGrass.EnumTallGrassType[values().length];
      private final int e;
      private final String f;

      private EnumTallGrassType(int var3, String var4) {
         this.e = var3;
         this.f = var4;
      }

      public int a() {
         return this.e;
      }

      public String toString() {
         return this.f;
      }

      public static BlockLongGrass.EnumTallGrassType a(int var0) {
         if(var0 < 0 || var0 >= d.length) {
            var0 = 0;
         }

         return d[var0];
      }

      public String m() {
         return this.f;
      }

      static {
         BlockLongGrass.EnumTallGrassType[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            BlockLongGrass.EnumTallGrassType var3 = var0[var2];
            d[var3.a()] = var3;
         }

      }
   }
}
