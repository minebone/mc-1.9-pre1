package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.Achievement;
import net.minecraft.server.AchievementList;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.Statistic;
import net.minecraft.server.StatisticList;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;

public class CommandAchievement extends CommandAbstract {
   public String c() {
      return "achievement";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.achievement.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 2) {
         throw new class_cf("commands.achievement.usage", new Object[0]);
      } else {
         final Statistic var4 = StatisticList.a(var3[1]);
         if((var4 != null || var3[1].equals("*")) && (var4 == null || var4.d())) {
            final EntityPlayer var5 = var3.length >= 3?a(var1, var2, var3[2]):a(var2);
            boolean var6 = var3[0].equalsIgnoreCase("give");
            boolean var7 = var3[0].equalsIgnoreCase("take");
            if(var6 || var7) {
               if(var4 == null) {
                  Iterator var15;
                  Achievement var16;
                  if(var6) {
                     var15 = AchievementList.e.iterator();

                     while(var15.hasNext()) {
                        var16 = (Achievement)var15.next();
                        var5.b((Statistic)var16);
                     }

                     a(var2, this, "commands.achievement.give.success.all", new Object[]{var5.h_()});
                  } else if(var7) {
                     var15 = Lists.reverse(AchievementList.e).iterator();

                     while(var15.hasNext()) {
                        var16 = (Achievement)var15.next();
                        var5.a((Statistic)var16);
                     }

                     a(var2, this, "commands.achievement.take.success.all", new Object[]{var5.h_()});
                  }

               } else {
                  if(var4 instanceof Achievement) {
                     Achievement var8 = (Achievement)var4;
                     ArrayList var9;
                     if(var6) {
                        if(var5.E().a((Achievement)var8)) {
                           throw new class_bz("commands.achievement.alreadyHave", new Object[]{var5.h_(), var4.j()});
                        }

                        for(var9 = Lists.newArrayList(); var8.c != null && !var5.E().a((Achievement)var8.c); var8 = var8.c) {
                           var9.add(var8.c);
                        }

                        Iterator var10 = Lists.reverse(var9).iterator();

                        while(var10.hasNext()) {
                           Achievement var11 = (Achievement)var10.next();
                           var5.b((Statistic)var11);
                        }
                     } else if(var7) {
                        if(!var5.E().a((Achievement)var8)) {
                           throw new class_bz("commands.achievement.dontHave", new Object[]{var5.h_(), var4.j()});
                        }

                        var9 = Lists.newArrayList((Iterator)Iterators.filter(AchievementList.e.iterator(), new Predicate() {
                           public boolean a(Achievement var1) {
                              return var5.E().a((Achievement)var1) && var1 != var4;
                           }

                           // $FF: synthetic method
                           public boolean apply(Object var1) {
                              return this.a((Achievement)var1);
                           }
                        }));
                        ArrayList var17 = Lists.newArrayList((Iterable)var9);
                        Iterator var18 = var9.iterator();

                        label119:
                        while(true) {
                           Achievement var12;
                           Achievement var13;
                           boolean var14;
                           do {
                              if(!var18.hasNext()) {
                                 var18 = var17.iterator();

                                 while(var18.hasNext()) {
                                    var12 = (Achievement)var18.next();
                                    var5.a((Statistic)var12);
                                 }
                                 break label119;
                              }

                              var12 = (Achievement)var18.next();
                              var13 = var12;

                              for(var14 = false; var13 != null; var13 = var13.c) {
                                 if(var13 == var4) {
                                    var14 = true;
                                 }
                              }
                           } while(var14);

                           for(var13 = var12; var13 != null; var13 = var13.c) {
                              var17.remove(var12);
                           }
                        }
                     }
                  }

                  if(var6) {
                     var5.b((Statistic)var4);
                     a(var2, this, "commands.achievement.give.success.one", new Object[]{var5.h_(), var4.j()});
                  } else if(var7) {
                     var5.a(var4);
                     a(var2, this, "commands.achievement.take.success.one", new Object[]{var4.j(), var5.h_()});
                  }

               }
            }
         } else {
            throw new class_bz("commands.achievement.unknownAchievement", new Object[]{var3[1]});
         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      if(var3.length == 1) {
         return a(var3, new String[]{"give", "take"});
      } else if(var3.length != 2) {
         return var3.length == 3?a(var3, var1.J()):Collections.emptyList();
      } else {
         ArrayList var5 = Lists.newArrayList();
         Iterator var6 = AchievementList.e.iterator();

         while(var6.hasNext()) {
            Statistic var7 = (Statistic)var6.next();
            var5.add(var7.e);
         }

         return a(var3, var5);
      }
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 2;
   }
}
