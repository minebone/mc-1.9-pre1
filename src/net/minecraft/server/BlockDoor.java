package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_amf;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.class_arn;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.class_axg;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;
import net.minecraft.server.class_or;
import net.minecraft.server.EnumHand;

public class BlockDoor extends Block {
   public static final class_arn a = class_amf.D;
   public static final class_arm b = class_arm.a("open");
   public static final BlockStateEnum c = BlockStateEnum.a("hinge", BlockDoor.EnumDoorHinge.class);
   public static final class_arm d = class_arm.a("powered");
   public static final BlockStateEnum e = BlockStateEnum.a("half", BlockDoor.class_a_in_class_aku.class);
   protected static final AxisAlignedBB f = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.1875D);
   protected static final AxisAlignedBB g = new AxisAlignedBB(0.0D, 0.0D, 0.8125D, 1.0D, 1.0D, 1.0D);
   protected static final AxisAlignedBB B = new AxisAlignedBB(0.8125D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
   protected static final AxisAlignedBB C = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.1875D, 1.0D, 1.0D);

   protected BlockDoor(Material var1) {
      super(var1);
      this.w(this.A.b().set(a, EnumDirection.NORTH).set(b, Boolean.valueOf(false)).set(c, BlockDoor.EnumDoorHinge.LEFT).set(d, Boolean.valueOf(false)).set(e, BlockDoor.class_a_in_class_aku.LOWER));
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      var1 = var1.b(var2, var3);
      EnumDirection var4 = (EnumDirection)var1.get(a);
      boolean var5 = !((Boolean)var1.get(b)).booleanValue();
      boolean var6 = var1.get(c) == BlockDoor.EnumDoorHinge.RIGHT;
      switch(BlockDoor.SyntheticClass_1.a[var4.ordinal()]) {
      case 1:
      default:
         return var5?C:(var6?g:f);
      case 2:
         return var5?f:(var6?C:B);
      case 3:
         return var5?B:(var6?f:g);
      case 4:
         return var5?g:(var6?B:C);
      }
   }

   public String c() {
      return class_di.a((this.a() + ".name").replaceAll("tile", "item"));
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean b(class_ahw var1, BlockPosition var2) {
      return g(c(var1, var2));
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   private int e() {
      return this.x == Material.f?1011:1012;
   }

   private int g() {
      return this.x == Material.f?1005:1006;
   }

   public MaterialMapColor r(IBlockData var1) {
      return var1.getBlock() == Blocks.aA?MaterialMapColor.h:(var1.getBlock() == Blocks.ao?BlockWood.EnumLogVariant.OAK.c():(var1.getBlock() == Blocks.ap?BlockWood.EnumLogVariant.SPRUCE.c():(var1.getBlock() == Blocks.aq?BlockWood.EnumLogVariant.BIRCH.c():(var1.getBlock() == Blocks.ar?BlockWood.EnumLogVariant.JUNGLE.c():(var1.getBlock() == Blocks.as?BlockWood.EnumLogVariant.ACACIA.c():(var1.getBlock() == Blocks.at?BlockWood.EnumLogVariant.DARK_OAK.c():super.r(var1)))))));
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(this.x == Material.f) {
         return true;
      } else {
         BlockPosition var11 = var3.get(e) == BlockDoor.class_a_in_class_aku.LOWER?var2:var2.b();
         IBlockData var12 = var2.equals(var11)?var3:var1.getType(var11);
         if(var12.getBlock() != this) {
            return false;
         } else {
            var3 = var12.a(b);
            var1.a((BlockPosition)var11, (IBlockData)var3, 10);
            var1.b(var11, var2);
            var1.a(var4, ((Boolean)var3.get(b)).booleanValue()?this.g():this.e(), var2, 0);
            return true;
         }
      }
   }

   public void a(World var1, BlockPosition var2, boolean var3) {
      IBlockData var4 = var1.getType(var2);
      if(var4.getBlock() == this) {
         BlockPosition var5 = var4.get(e) == BlockDoor.class_a_in_class_aku.LOWER?var2:var2.b();
         IBlockData var6 = var2 == var5?var4:var1.getType(var5);
         if(var6.getBlock() == this && ((Boolean)var6.get(b)).booleanValue() != var3) {
            var1.a((BlockPosition)var5, (IBlockData)var6.set(b, Boolean.valueOf(var3)), 10);
            var1.b(var5, var2);
            var1.a((EntityHuman)null, var3?this.g():this.e(), var2, 0);
         }

      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(var3.get(e) == BlockDoor.class_a_in_class_aku.UPPER) {
         BlockPosition var5 = var2.b();
         IBlockData var6 = var1.getType(var5);
         if(var6.getBlock() != this) {
            var1.g(var2);
         } else if(var4 != this) {
            this.a(var1, var5, var6, var4);
         }
      } else {
         boolean var9 = false;
         BlockPosition var10 = var2.a();
         IBlockData var7 = var1.getType(var10);
         if(var7.getBlock() != this) {
            var1.g(var2);
            var9 = true;
         }

         if(!var1.getType(var2.b()).q()) {
            var1.g(var2);
            var9 = true;
            if(var7.getBlock() == this) {
               var1.g(var10);
            }
         }

         if(var9) {
            if(!var1.E) {
               this.b(var1, var2, var3, 0);
            }
         } else {
            boolean var8 = var1.y(var2) || var1.y(var10);
            if(var4 != this && (var8 || var4.u().m()) && var8 != ((Boolean)var7.get(d)).booleanValue()) {
               var1.a((BlockPosition)var10, (IBlockData)var7.set(d, Boolean.valueOf(var8)), 2);
               if(var8 != ((Boolean)var3.get(b)).booleanValue()) {
                  var1.a((BlockPosition)var2, (IBlockData)var3.set(b, Boolean.valueOf(var8)), 2);
                  var1.b(var2, var2);
                  var1.a((EntityHuman)null, var8?this.g():this.e(), var2, 0);
               }
            }
         }
      }

   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return var1.get(e) == BlockDoor.class_a_in_class_aku.UPPER?null:this.h();
   }

   public boolean a(World var1, BlockPosition var2) {
      return var2.q() >= 255?false:var1.getType(var2.b()).q() && super.a(var1, var2) && super.a(var1, var2.a());
   }

   public class_axg h(IBlockData var1) {
      return class_axg.DESTROY;
   }

   public static int c(class_ahw var0, BlockPosition var1) {
      IBlockData var2 = var0.getType(var1);
      int var3 = var2.getBlock().e(var2);
      boolean var4 = i(var3);
      IBlockData var5 = var0.getType(var1.b());
      int var6 = var5.getBlock().e(var5);
      int var7 = var4?var6:var3;
      IBlockData var8 = var0.getType(var1.a());
      int var9 = var8.getBlock().e(var8);
      int var10 = var4?var3:var9;
      boolean var11 = (var10 & 1) != 0;
      boolean var12 = (var10 & 2) != 0;
      return e(var7) | (var4?8:0) | (var11?16:0) | (var12?32:0);
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(this.h());
   }

   private Item h() {
      return this == Blocks.aA?Items.aD:(this == Blocks.ap?Items.at:(this == Blocks.aq?Items.au:(this == Blocks.ar?Items.av:(this == Blocks.as?Items.aw:(this == Blocks.at?Items.ax:Items.as)))));
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4) {
      BlockPosition var5 = var2.b();
      BlockPosition var6 = var2.a();
      if(var4.abilities.d && var3.get(e) == BlockDoor.class_a_in_class_aku.UPPER && var1.getType(var5).getBlock() == this) {
         var1.g(var5);
      }

      if(var3.get(e) == BlockDoor.class_a_in_class_aku.LOWER && var1.getType(var6).getBlock() == this) {
         if(var4.abilities.d) {
            var1.g(var2);
         }

         var1.g(var6);
      }

   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      IBlockData var4;
      if(var1.get(e) == BlockDoor.class_a_in_class_aku.LOWER) {
         var4 = var2.getType(var3.a());
         if(var4.getBlock() == this) {
            var1 = var1.set(c, var4.get(c)).set(d, var4.get(d));
         }
      } else {
         var4 = var2.getType(var3.b());
         if(var4.getBlock() == this) {
            var1 = var1.set(a, var4.get(a)).set(b, var4.get(b));
         }
      }

      return var1;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.get(e) != BlockDoor.class_a_in_class_aku.LOWER?var1:var1.set(a, var2.a((EnumDirection)var1.get(a)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(a)));
   }

   public IBlockData a(int var1) {
      return (var1 & 8) > 0?this.u().set(e, BlockDoor.class_a_in_class_aku.UPPER).set(c, (var1 & 1) > 0?BlockDoor.EnumDoorHinge.RIGHT:BlockDoor.EnumDoorHinge.LEFT).set(d, Boolean.valueOf((var1 & 2) > 0)):this.u().set(e, BlockDoor.class_a_in_class_aku.LOWER).set(a, EnumDirection.b(var1 & 3).f()).set(b, Boolean.valueOf((var1 & 4) > 0));
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3;
      if(var1.get(e) == BlockDoor.class_a_in_class_aku.UPPER) {
         var3 = var2 | 8;
         if(var1.get(c) == BlockDoor.EnumDoorHinge.RIGHT) {
            var3 |= 1;
         }

         if(((Boolean)var1.get(d)).booleanValue()) {
            var3 |= 2;
         }
      } else {
         var3 = var2 | ((EnumDirection)var1.get(a)).e().b();
         if(((Boolean)var1.get(b)).booleanValue()) {
            var3 |= 4;
         }
      }

      return var3;
   }

   protected static int e(int var0) {
      return var0 & 7;
   }

   public static boolean d(class_ahw var0, BlockPosition var1) {
      return g(c(var0, var1));
   }

   public static EnumDirection f(class_ahw var0, BlockPosition var1) {
      return f(c(var0, var1));
   }

   public static EnumDirection f(int var0) {
      return EnumDirection.b(var0 & 3).f();
   }

   protected static boolean g(int var0) {
      return (var0 & 4) != 0;
   }

   protected static boolean i(int var0) {
      return (var0 & 8) != 0;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{e, a, b, c, d});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumDirection.values().length];

      static {
         try {
            a[EnumDirection.EAST.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.SOUTH.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.NORTH.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum EnumDoorHinge implements class_or {
      LEFT,
      RIGHT;

      public String toString() {
         return this.m();
      }

      public String m() {
         return this == LEFT?"left":"right";
      }
   }

   public static enum class_a_in_class_aku implements class_or {
      UPPER,
      LOWER;

      public String toString() {
         return this.m();
      }

      public String m() {
         return this == UPPER?"upper":"lower";
      }
   }
}
