package net.minecraft.server;

import com.google.common.base.Optional;
import java.util.UUID;
import net.minecraft.server.Block;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_dc;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kf;
import net.minecraft.server.class_oa;

public class class_kg {
   private static final class_oa n = new class_oa(16);
   public static final class_kf a = new class_kf() {
      public void a(PacketDataSerializer var1, Byte var2) {
         var1.writeByte(var2.byteValue());
      }

      public Byte b(PacketDataSerializer var1) {
         return Byte.valueOf(var1.readByte());
      }

      public class_ke a(int var1) {
         return new class_ke(var1, this);
      }

      // $FF: synthetic method
      public Object a(PacketDataSerializer var1) {
         return this.b(var1);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(PacketDataSerializer var1, Object var2) {
         this.a(var1, (Byte)var2);
      }
   };
   public static final class_kf b = new class_kf() {
      public void a(PacketDataSerializer var1, Integer var2) {
         var1.writeVarInt(var2.intValue());
      }

      public Integer b(PacketDataSerializer var1) {
         return Integer.valueOf(var1.readVarInt());
      }

      public class_ke a(int var1) {
         return new class_ke(var1, this);
      }

      // $FF: synthetic method
      public Object a(PacketDataSerializer var1) {
         return this.b(var1);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(PacketDataSerializer var1, Object var2) {
         this.a(var1, (Integer)var2);
      }
   };
   public static final class_kf c = new class_kf() {
      public void a(PacketDataSerializer var1, Float var2) {
         var1.writeFloat(var2.floatValue());
      }

      public Float b(PacketDataSerializer var1) {
         return Float.valueOf(var1.readFloat());
      }

      public class_ke a(int var1) {
         return new class_ke(var1, this);
      }

      // $FF: synthetic method
      public Object a(PacketDataSerializer var1) {
         return this.b(var1);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(PacketDataSerializer var1, Object var2) {
         this.a(var1, (Float)var2);
      }
   };
   public static final class_kf d = new class_kf() {
      public void a(PacketDataSerializer var1, String var2) {
         var1.a(var2);
      }

      public String b(PacketDataSerializer var1) {
         return var1.c(32767);
      }

      public class_ke a(int var1) {
         return new class_ke(var1, this);
      }

      // $FF: synthetic method
      public Object a(PacketDataSerializer var1) {
         return this.b(var1);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(PacketDataSerializer var1, Object var2) {
         this.a(var1, (String)var2);
      }
   };
   public static final class_kf e = new class_kf() {
      public void a(PacketDataSerializer var1, IChatBaseComponent var2) {
         var1.a(var2);
      }

      public IChatBaseComponent b(PacketDataSerializer var1) {
         return var1.f();
      }

      public class_ke a(int var1) {
         return new class_ke(var1, this);
      }

      // $FF: synthetic method
      public Object a(PacketDataSerializer var1) {
         return this.b(var1);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(PacketDataSerializer var1, Object var2) {
         this.a(var1, (IChatBaseComponent)var2);
      }
   };
   public static final class_kf f = new class_kf() {
      public void a(PacketDataSerializer var1, Optional var2) {
         var1.a((ItemStack)var2.orNull());
      }

      public Optional b(PacketDataSerializer var1) {
         return Optional.fromNullable(var1.k());
      }

      public class_ke a(int var1) {
         return new class_ke(var1, this);
      }

      // $FF: synthetic method
      public Object a(PacketDataSerializer var1) {
         return this.b(var1);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(PacketDataSerializer var1, Object var2) {
         this.a(var1, (Optional)var2);
      }
   };
   public static final class_kf g = new class_kf() {
      public void a(PacketDataSerializer var1, Optional var2) {
         if(var2.isPresent()) {
            var1.writeVarInt(Block.j((IBlockData)var2.get()));
         } else {
            var1.writeVarInt(0);
         }

      }

      public Optional b(PacketDataSerializer var1) {
         int var2 = var1.readVarInt();
         return var2 == 0?Optional.absent():Optional.of(Block.c(var2));
      }

      public class_ke a(int var1) {
         return new class_ke(var1, this);
      }

      // $FF: synthetic method
      public Object a(PacketDataSerializer var1) {
         return this.b(var1);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(PacketDataSerializer var1, Object var2) {
         this.a(var1, (Optional)var2);
      }
   };
   public static final class_kf h = new class_kf() {
      public void a(PacketDataSerializer var1, Boolean var2) {
         var1.writeBoolean(var2.booleanValue());
      }

      public Boolean b(PacketDataSerializer var1) {
         return Boolean.valueOf(var1.readBoolean());
      }

      public class_ke a(int var1) {
         return new class_ke(var1, this);
      }

      // $FF: synthetic method
      public Object a(PacketDataSerializer var1) {
         return this.b(var1);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(PacketDataSerializer var1, Object var2) {
         this.a(var1, (Boolean)var2);
      }
   };
   public static final class_kf i = new class_kf() {
      public void a(PacketDataSerializer var1, class_dc var2) {
         var1.writeFloat(var2.b());
         var1.writeFloat(var2.c());
         var1.writeFloat(var2.d());
      }

      public class_dc b(PacketDataSerializer var1) {
         return new class_dc(var1.readFloat(), var1.readFloat(), var1.readFloat());
      }

      public class_ke a(int var1) {
         return new class_ke(var1, this);
      }

      // $FF: synthetic method
      public Object a(PacketDataSerializer var1) {
         return this.b(var1);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(PacketDataSerializer var1, Object var2) {
         this.a(var1, (class_dc)var2);
      }
   };
   public static final class_kf j = new class_kf() {
      public void a(PacketDataSerializer var1, BlockPosition var2) {
         var1.a(var2);
      }

      public BlockPosition b(PacketDataSerializer var1) {
         return var1.e();
      }

      public class_ke a(int var1) {
         return new class_ke(var1, this);
      }

      // $FF: synthetic method
      public Object a(PacketDataSerializer var1) {
         return this.b(var1);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(PacketDataSerializer var1, Object var2) {
         this.a(var1, (BlockPosition)var2);
      }
   };
   public static final class_kf k = new class_kf() {
      public void a(PacketDataSerializer var1, Optional var2) {
         var1.writeBoolean(var2.isPresent());
         if(var2.isPresent()) {
            var1.a((BlockPosition)var2.get());
         }

      }

      public Optional b(PacketDataSerializer var1) {
         return !var1.readBoolean()?Optional.absent():Optional.of(var1.e());
      }

      public class_ke a(int var1) {
         return new class_ke(var1, this);
      }

      // $FF: synthetic method
      public Object a(PacketDataSerializer var1) {
         return this.b(var1);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(PacketDataSerializer var1, Object var2) {
         this.a(var1, (Optional)var2);
      }
   };
   public static final class_kf l = new class_kf() {
      public void a(PacketDataSerializer var1, EnumDirection var2) {
         var1.a((Enum)var2);
      }

      public EnumDirection b(PacketDataSerializer var1) {
         return (EnumDirection)var1.a(EnumDirection.class);
      }

      public class_ke a(int var1) {
         return new class_ke(var1, this);
      }

      // $FF: synthetic method
      public Object a(PacketDataSerializer var1) {
         return this.b(var1);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(PacketDataSerializer var1, Object var2) {
         this.a(var1, (EnumDirection)var2);
      }
   };
   public static final class_kf m = new class_kf() {
      public void a(PacketDataSerializer var1, Optional var2) {
         var1.writeBoolean(var2.isPresent());
         if(var2.isPresent()) {
            var1.a((UUID)var2.get());
         }

      }

      public Optional b(PacketDataSerializer var1) {
         return !var1.readBoolean()?Optional.absent():Optional.of(var1.i());
      }

      public class_ke a(int var1) {
         return new class_ke(var1, this);
      }

      // $FF: synthetic method
      public Object a(PacketDataSerializer var1) {
         return this.b(var1);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(PacketDataSerializer var1, Object var2) {
         this.a(var1, (Optional)var2);
      }
   };

   public static void a(class_kf var0) {
      n.c(var0);
   }

   public static class_kf a(int var0) {
      return (class_kf)n.a(var0);
   }

   public static int b(class_kf var0) {
      return n.a(var0);
   }

   static {
      a(a);
      a(b);
      a(c);
      a(d);
      a(e);
      a(f);
      a(h);
      a(i);
      a(j);
      a(k);
      a(l);
      a(m);
      a(g);
   }
}
