package net.minecraft.server;

import com.google.common.collect.Maps;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSyntaxException;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Random;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_bap;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_bas;
import net.minecraft.server.class_bat;
import net.minecraft.server.class_bau;
import net.minecraft.server.class_bav;
import net.minecraft.server.class_kk;

public class class_bar {
   private static final Map a = Maps.newHashMap();
   private static final Map b = Maps.newHashMap();

   public static void a(class_baq.class_a_in_class_baq var0) {
      class_kk var1 = var0.a();
      Class var2 = var0.b();
      if(a.containsKey(var1)) {
         throw new IllegalArgumentException("Can\'t re-register item condition name " + var1);
      } else if(b.containsKey(var2)) {
         throw new IllegalArgumentException("Can\'t re-register item condition class " + var2.getName());
      } else {
         a.put(var1, var0);
         b.put(var2, var0);
      }
   }

   public static boolean a(class_baq[] var0, Random var1, class_azy var2) {
      if(var0 == null) {
         return true;
      } else {
         int var3 = 0;

         for(int var4 = var0.length; var3 < var4; ++var3) {
            class_baq var5 = var0[var3];
            if(!var5.a(var1, var2)) {
               return false;
            }
         }

         return true;
      }
   }

   public static class_baq.class_a_in_class_baq a(class_kk var0) {
      class_baq.class_a_in_class_baq var1 = (class_baq.class_a_in_class_baq)a.get(var0);
      if(var1 == null) {
         throw new IllegalArgumentException("Unknown loot item condition \'" + var0 + "\'");
      } else {
         return var1;
      }
   }

   public static class_baq.class_a_in_class_baq a(class_baq var0) {
      class_baq.class_a_in_class_baq var1 = (class_baq.class_a_in_class_baq)b.get(var0.getClass());
      if(var1 == null) {
         throw new IllegalArgumentException("Unknown loot item condition " + var0);
      } else {
         return var1;
      }
   }

   static {
      a((class_baq.class_a_in_class_baq)(new class_bau.class_a_in_class_bau()));
      a((class_baq.class_a_in_class_baq)(new class_bav.class_a_in_class_bav()));
      a((class_baq.class_a_in_class_baq)(new class_bas.class_a_in_class_bas()));
      a((class_baq.class_a_in_class_baq)(new class_bat.class_a_in_class_bat()));
      a((class_baq.class_a_in_class_baq)(new class_bap.class_a_in_class_bap()));
   }

   public static class class_a_in_class_bar implements JsonDeserializer, JsonSerializer {
      public class_baq a(JsonElement var1, Type var2, JsonDeserializationContext var3) throws JsonParseException {
         JsonObject var4 = ChatDeserializer.m(var1, "condition");
         class_kk var5 = new class_kk(ChatDeserializer.h(var4, "condition"));

         class_baq.class_a_in_class_baq var6;
         try {
            var6 = class_bar.a(var5);
         } catch (IllegalArgumentException var8) {
            throw new JsonSyntaxException("Unknown condition \'" + var5 + "\'");
         }

         return var6.b(var4, var3);
      }

      public JsonElement a(class_baq var1, Type var2, JsonSerializationContext var3) {
         class_baq.class_a_in_class_baq var4 = class_bar.a(var1);
         JsonObject var5 = new JsonObject();
         var4.a(var5, var1, var3);
         var5.addProperty("condition", var4.a().toString());
         return var5;
      }

      // $FF: synthetic method
      public JsonElement serialize(Object var1, Type var2, JsonSerializationContext var3) {
         return this.a((class_baq)var1, var2, var3);
      }

      // $FF: synthetic method
      public Object deserialize(JsonElement var1, Type var2, JsonDeserializationContext var3) throws JsonParseException {
         return this.a(var1, var2, var3);
      }
   }
}
