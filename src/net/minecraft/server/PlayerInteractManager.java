package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.PacketPlayOutPlayerInfo;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.WorldSettings;
import net.minecraft.server.class_acb;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aew;
import net.minecraft.server.BlockChest;
import net.minecraft.server.class_akj;
import net.minecraft.server.class_apw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutBlockChange;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.IInventory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;
import net.minecraft.server.class_qr;

public class PlayerInteractManager {
   public World world;
   public EntityPlayer player;
   private WorldSettings.EnumGamemode gamemode = WorldSettings.EnumGamemode.NOT_SET;
   private boolean d;
   private int lastDigTick;
   private BlockPosition f = BlockPosition.a;
   private int currentTick;
   private boolean h;
   private BlockPosition i = BlockPosition.a;
   private int j;
   private int k = -1;

   public PlayerInteractManager(World var1) {
      this.world = var1;
   }

   public void a(WorldSettings.EnumGamemode var1) {
      this.gamemode = var1;
      var1.a(this.player.abilities);
      this.player.updateAbilities();
      this.player.server.getPlayerList().sendAll((Packet)(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.UPDATE_GAME_MODE, new EntityPlayer[]{this.player})));
      this.world.e();
   }

   public WorldSettings.EnumGamemode b() {
      return this.gamemode;
   }

   public boolean c() {
      return this.gamemode.e();
   }

   public boolean isCreative() {
      return this.gamemode.d();
   }

   public void b(WorldSettings.EnumGamemode var1) {
      if(this.gamemode == WorldSettings.EnumGamemode.NOT_SET) {
         this.gamemode = var1;
      }

      this.a(this.gamemode);
   }

   public void a() {
      ++this.currentTick;
      float var4;
      int var5;
      if(this.h) {
         int var1 = this.currentTick - this.j;
         IBlockData var2 = this.world.getType(this.i);
         Block var3 = var2.getBlock();
         if(var2.getMaterial() == Material.a) {
            this.h = false;
         } else {
            var4 = var2.a(this.player, this.player.world, this.i) * (float)(var1 + 1);
            var5 = (int)(var4 * 10.0F);
            if(var5 != this.k) {
               this.world.c(this.player.getId(), this.i, var5);
               this.k = var5;
            }

            if(var4 >= 1.0F) {
               this.h = false;
               this.breakBlock(this.i);
            }
         }
      } else if(this.d) {
         IBlockData var6 = this.world.getType(this.f);
         Block var7 = var6.getBlock();
         if(var6.getMaterial() == Material.a) {
            this.world.c(this.player.getId(), this.f, -1);
            this.k = -1;
            this.d = false;
         } else {
            int var8 = this.currentTick - this.lastDigTick;
            var4 = var6.a(this.player, this.player.world, this.i) * (float)(var8 + 1);
            var5 = (int)(var4 * 10.0F);
            if(var5 != this.k) {
               this.world.c(this.player.getId(), this.f, var5);
               this.k = var5;
            }
         }
      }

   }

   public void a(BlockPosition var1, EnumDirection var2) {
      if(this.isCreative()) {
         if(!this.world.douseFire((EntityHuman)null, (BlockPosition)var1, (EnumDirection)var2)) {
            this.breakBlock(var1);
         }

      } else {
         IBlockData var3 = this.world.getType(var1);
         Block var4 = var3.getBlock();
         if(this.gamemode.c()) {
            if(this.gamemode == WorldSettings.EnumGamemode.SPECTATOR) {
               return;
            }

            if(!this.player.cT()) {
               ItemStack var5 = this.player.ca();
               if(var5 == null) {
                  return;
               }

               if(!var5.a(var4)) {
                  return;
               }
            }
         }

         this.world.douseFire((EntityHuman)null, (BlockPosition)var1, (EnumDirection)var2);
         this.lastDigTick = this.currentTick;
         float var7 = 1.0F;
         if(var3.getMaterial() != Material.a) {
            var4.a((World)this.world, (BlockPosition)var1, (EntityHuman)this.player);
            var7 = var3.a(this.player, this.player.world, var1);
         }

         if(var3.getMaterial() != Material.a && var7 >= 1.0F) {
            this.breakBlock(var1);
         } else {
            this.d = true;
            this.f = var1;
            int var6 = (int)(var7 * 10.0F);
            this.world.c(this.player.getId(), var1, var6);
            this.k = var6;
         }

      }
   }

   public void a(BlockPosition var1) {
      if(var1.equals(this.f)) {
         int var2 = this.currentTick - this.lastDigTick;
         IBlockData var3 = this.world.getType(var1);
         if(var3.getMaterial() != Material.a) {
            float var4 = var3.a(this.player, this.player.world, var1) * (float)(var2 + 1);
            if(var4 >= 0.7F) {
               this.d = false;
               this.world.c(this.player.getId(), var1, -1);
               this.breakBlock(var1);
            } else if(!this.h) {
               this.d = false;
               this.h = true;
               this.i = var1;
               this.j = this.lastDigTick;
            }
         }
      }

   }

   public void e() {
      this.d = false;
      this.world.c(this.player.getId(), this.f, -1);
   }

   private boolean c(BlockPosition var1) {
      IBlockData var2 = this.world.getType(var1);
      var2.getBlock().a((World)this.world, (BlockPosition)var1, (IBlockData)var2, (EntityHuman)this.player);
      boolean var3 = this.world.g(var1);
      if(var3) {
         var2.getBlock().d(this.world, var1, var2);
      }

      return var3;
   }

   public boolean breakBlock(BlockPosition var1) {
      if(this.gamemode.d() && this.player.ca() != null && this.player.ca().b() instanceof class_aew) {
         return false;
      } else {
         IBlockData var2 = this.world.getType(var1);
         TileEntity var3 = this.world.r(var1);
         if(var2.getBlock() instanceof class_akj && !this.player.a(2, "")) {
            this.world.a(var1, var2, var2, 3);
            return false;
         } else {
            if(this.gamemode.c()) {
               if(this.gamemode == WorldSettings.EnumGamemode.SPECTATOR) {
                  return false;
               }

               if(!this.player.cT()) {
                  ItemStack var4 = this.player.ca();
                  if(var4 == null) {
                     return false;
                  }

                  if(!var4.a(var2.getBlock())) {
                     return false;
                  }
               }
            }

            this.world.a(this.player, 2001, var1, Block.j(var2));
            boolean var8 = this.c(var1);
            if(this.isCreative()) {
               this.player.a.a((Packet)(new PacketPlayOutBlockChange(this.world, var1)));
            } else {
               ItemStack var5 = this.player.ca();
               ItemStack var6 = var5 == null?null:var5.k();
               boolean var7 = this.player.b((IBlockData)var2);
               if(var5 != null) {
                  var5.a(this.world, var2, var1, this.player);
                  if(var5.b == 0) {
                     this.player.a((EnumHand)EnumHand.MAIN_HAND, (ItemStack)null);
                  }
               }

               if(var8 && var7) {
                  var2.getBlock().a((World)this.world, (EntityHuman)this.player, var1, (IBlockData)var2, (TileEntity)var3, (ItemStack)var6);
               }
            }

            return var8;
         }
      }
   }

   public EnumResult use(EntityHuman var1, World var2, ItemStack var3, EnumHand var4) {
      if(this.gamemode == WorldSettings.EnumGamemode.SPECTATOR) {
         return EnumResult.PASS;
      } else if(var1.cZ().a(var3.b())) {
         return EnumResult.PASS;
      } else {
         int var5 = var3.b;
         int var6 = var3.i();
         class_qo var7 = var3.a(var2, var1, var4);
         ItemStack var8 = (ItemStack)var7.b();
         if(var8 == var3 && var8.b == var5 && var8.l() <= 0 && var8.i() == var6) {
            return var7.a();
         } else {
            var1.a((EnumHand)var4, (ItemStack)var8);
            if(this.isCreative()) {
               var8.b = var5;
               if(var8.e()) {
                  var8.b(var6);
               }
            }

            if(var8.b == 0) {
               var1.a((EnumHand)var4, (ItemStack)null);
            }

            if(!var1.cr()) {
               ((EntityPlayer)var1).a(var1.bs);
            }

            return var7.a();
         }
      }
   }

   public EnumResult interact(EntityHuman var1, World var2, ItemStack var3, EnumHand var4, BlockPosition var5, EnumDirection var6, float var7, float var8, float var9) {
      if(this.gamemode == WorldSettings.EnumGamemode.SPECTATOR) {
         TileEntity var14 = var2.r(var5);
         if(var14 instanceof class_qr) {
            Block var15 = var2.getType(var5).getBlock();
            class_qr var16 = (class_qr)var14;
            if(var16 instanceof class_apw && var15 instanceof BlockChest) {
               var16 = ((BlockChest)var15).c(var2, var5);
            }

            if(var16 != null) {
               var1.openContainer((IInventory)var16);
               return EnumResult.SUCCESS;
            }
         } else if(var14 instanceof IInventory) {
            var1.openContainer((IInventory)var14);
            return EnumResult.SUCCESS;
         }

         return EnumResult.PASS;
      } else {
         if(!var1.aJ() || var1.ca() == null && var1.cb() == null) {
            IBlockData var10 = var2.getType(var5);
            if(var10.getBlock().a(var2, var5, var10, var1, var4, var3, var6, var7, var8, var9)) {
               return EnumResult.SUCCESS;
            }
         }

         if(var3 == null) {
            return EnumResult.PASS;
         } else if(var1.cZ().a(var3.b())) {
            return EnumResult.PASS;
         } else if(var3.b() instanceof class_acb && ((class_acb)var3.b()).d() instanceof class_akj && !var1.a(2, "")) {
            return EnumResult.FAIL;
         } else if(this.isCreative()) {
            int var13 = var3.i();
            int var11 = var3.b;
            EnumResult var12 = var3.a(var1, var2, var5, var4, var6, var7, var8, var9);
            var3.b(var13);
            var3.b = var11;
            return var12;
         } else {
            return var3.a(var1, var2, var5, var4, var6, var7, var8, var9);
         }
      }
   }

   public void a(WorldServer var1) {
      this.world = var1;
   }
}
