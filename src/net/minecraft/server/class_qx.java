package net.minecraft.server;

import net.minecraft.server.EnumDirection;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IInventory;

public interface class_qx extends IInventory {
   int[] a(EnumDirection var1);

   boolean a(int var1, ItemStack var2, EnumDirection var3);

   boolean b(int var1, ItemStack var2, EnumDirection var3);
}
