package net.minecraft.server;

import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.GameRules;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.WorldData;
import net.minecraft.server.WorldProviderNormal;
import net.minecraft.server.WorldSettings;
import net.minecraft.server.WorldType;
import net.minecraft.server.BlockPosition;

public class class_azd extends WorldData {
   private final WorldData b;

   public class_azd(WorldData var1) {
      this.b = var1;
   }

   public NBTTagCompound a(NBTTagCompound var1) {
      return this.b.a(var1);
   }

   public long a() {
      return this.b.a();
   }

   public int b() {
      return this.b.b();
   }

   public int c() {
      return this.b.c();
   }

   public int d() {
      return this.b.d();
   }

   public long e() {
      return this.b.e();
   }

   public long f() {
      return this.b.f();
   }

   public NBTTagCompound h() {
      return this.b.h();
   }

   public String j() {
      return this.b.j();
   }

   public int k() {
      return this.b.k();
   }

   public boolean m() {
      return this.b.m();
   }

   public int n() {
      return this.b.n();
   }

   public boolean o() {
      return this.b.o();
   }

   public int p() {
      return this.b.p();
   }

   public WorldSettings.EnumGamemode q() {
      return this.b.q();
   }

   public void b(long var1) {
   }

   public void c(long var1) {
   }

   public void a(BlockPosition var1) {
   }

   public void a(String var1) {
   }

   public void e(int var1) {
   }

   public void a(boolean var1) {
   }

   public void f(int var1) {
   }

   public void b(boolean var1) {
   }

   public void g(int var1) {
   }

   public boolean r() {
      return this.b.r();
   }

   public boolean s() {
      return this.b.s();
   }

   public WorldType t() {
      return this.b.t();
   }

   public void a(WorldType var1) {
   }

   public boolean u() {
      return this.b.u();
   }

   public void c(boolean var1) {
   }

   public boolean v() {
      return this.b.v();
   }

   public void d(boolean var1) {
   }

   public GameRules w() {
      return this.b.w();
   }

   public EnumDifficulty x() {
      return this.b.x();
   }

   public void a(EnumDifficulty var1) {
   }

   public boolean y() {
      return this.b.y();
   }

   public void e(boolean var1) {
   }

   public void a(WorldProviderNormal var1, NBTTagCompound var2) {
      this.b.a(var1, var2);
   }

   public NBTTagCompound a(WorldProviderNormal var1) {
      return this.b.a(var1);
   }
}
