package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;

public class class_aqk extends TileEntity {
   public byte a;
   public boolean f;

   public void a(NBTTagCompound var1) {
      super.a(var1);
      var1.a("note", this.a);
      var1.a("powered", this.f);
   }

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      this.a = var2.f("note");
      this.a = (byte)MathHelper.a(this.a, 0, 24);
      this.f = var2.p("powered");
   }

   public void b() {
      this.a = (byte)((this.a + 1) % 25);
      this.v_();
   }

   public void a(World var1, BlockPosition var2) {
      if(var1.getType(var2.a()).getMaterial() == Material.a) {
         Material var3 = var1.getType(var2.b()).getMaterial();
         byte var4 = 0;
         if(var3 == Material.e) {
            var4 = 1;
         }

         if(var3 == Material.p) {
            var4 = 2;
         }

         if(var3 == Material.s) {
            var4 = 3;
         }

         if(var3 == Material.d) {
            var4 = 4;
         }

         var1.c(var2, Blocks.B, var4, this.a);
      }
   }
}
