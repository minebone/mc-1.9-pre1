package net.minecraft.server;

import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.class_ku;
import net.minecraft.server.class_qb;

public class class_fh {
   public static void a(final Packet var0, final PacketListener var1, class_qb var2) throws class_ku {
      if(!var2.aE()) {
         var2.a(new Runnable() {
            public void run() {
               var0.handle(var1);
            }
         });
         throw class_ku.a;
      }
   }
}
