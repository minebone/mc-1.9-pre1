package net.minecraft.server;

import net.minecraft.server.PacketPlayInBlockDig;
import net.minecraft.server.PacketPlayInClientCommand;
import net.minecraft.server.PacketPlayInCustomPayload;
import net.minecraft.server.PacketPlayInResourcePackStatus;
import net.minecraft.server.PacketPlayInUseEntity;
import net.minecraft.server.PacketListener;
import net.minecraft.server.PacketPlayInTeleport;
import net.minecraft.server.class_ii;
import net.minecraft.server.class_ij;
import net.minecraft.server.class_il;
import net.minecraft.server.class_im;
import net.minecraft.server.class_in;
import net.minecraft.server.class_io;
import net.minecraft.server.class_ip;
import net.minecraft.server.class_is;
import net.minecraft.server.class_it;
import net.minecraft.server.PacketPlayInVehicleMove;
import net.minecraft.server.PacketPlayInSteer;
import net.minecraft.server.class_iw;
import net.minecraft.server.class_iy;
import net.minecraft.server.class_iz;
import net.minecraft.server.class_jb;
import net.minecraft.server.class_jc;
import net.minecraft.server.class_jd;
import net.minecraft.server.class_je;
import net.minecraft.server.class_jf;
import net.minecraft.server.class_jg;
import net.minecraft.server.class_jh;

public interface PacketPlayInListener extends PacketListener {
   void a(class_je var1);

   void a(class_ij var1);

   void a(class_ii var1);

   void a(PacketPlayInClientCommand var1);

   void a(class_il var1);

   void a(class_im var1);

   void a(class_in var1);

   void a(class_io var1);

   void a(class_ip var1);

   void a(PacketPlayInCustomPayload var1);

   void a(PacketPlayInUseEntity var1);

   void a(class_is var1);

   void a(class_it var1);

   void a(class_iw var1);

   void a(PacketPlayInBlockDig var1);

   void a(class_iy var1);

   void a(class_iz var1);

   void a(class_jb var1);

   void a(class_jc var1);

   void a(class_jd var1);

   void a(class_jg var1);

   void a(class_jh var1);

   void a(class_jf var1);

   void a(PacketPlayInResourcePackStatus var1);

   void a(PacketPlayInSteer var1);

   void a(PacketPlayInVehicleMove var1);

   void a(PacketPlayInTeleport var1);
}
