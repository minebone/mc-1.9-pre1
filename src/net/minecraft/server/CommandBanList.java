package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;

public class CommandBanList extends CommandAbstract {
   public String c() {
      return "banlist";
   }

   public int a() {
      return 3;
   }

   public boolean a(MinecraftServer var1, ICommandListener var2) {
      return (var1.getPlayerList().i().b() || var1.getPlayerList().h().b()) && super.a(var1, var2);
   }

   public String b(ICommandListener var1) {
      return "commands.banlist.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length >= 1 && var3[0].equalsIgnoreCase("ips")) {
         var2.a(new ChatMessage("commands.banlist.ips", new Object[]{Integer.valueOf(var1.getPlayerList().i().a().length)}));
         var2.a(new ChatComponentText(a(var1.getPlayerList().i().a())));
      } else {
         var2.a(new ChatMessage("commands.banlist.players", new Object[]{Integer.valueOf(var1.getPlayerList().h().a().length)}));
         var2.a(new ChatComponentText(a(var1.getPlayerList().h().a())));
      }

   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, new String[]{"players", "ips"}):Collections.emptyList();
   }
}
