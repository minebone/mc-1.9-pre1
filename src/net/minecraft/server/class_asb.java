package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ash;

public class class_asb implements class_ash {
   public int a(IBlockData var1) {
      int var2 = Block.i.a(var1);
      return var2 == -1?0:var2;
   }

   public IBlockData a(int var1) {
      IBlockData var2 = (IBlockData)Block.i.a(var1);
      return var2 == null?Blocks.AIR.u():var2;
   }

   public void b(PacketDataSerializer var1) {
      var1.writeVarInt(0);
   }

   public int a() {
      return PacketDataSerializer.a(0);
   }
}
