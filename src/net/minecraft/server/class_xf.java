package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.Vec3D;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_ro;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_wt;
import net.minecraft.server.class_ww;
import net.minecraft.server.class_xj;

public class class_xf extends class_ww {
   private int b;
   private int c;
   private class_ro d;

   public class_xf(class_wt var1) {
      super(var1);
   }

   public void b() {
      ++this.b;
      if(this.b % 2 == 0 && this.b < 10) {
         Vec3D var1 = this.a.a(1.0F).a();
         var1.b(-0.7853982F);
         double var2 = this.a.bu.locX;
         double var4 = this.a.bu.locY + (double)(this.a.bu.length / 2.0F);
         double var6 = this.a.bu.locZ;

         for(int var8 = 0; var8 < 8; ++var8) {
            double var9 = var2 + this.a.bE().nextGaussian() / 2.0D;
            double var11 = var4 + this.a.bE().nextGaussian() / 2.0D;
            double var13 = var6 + this.a.bE().nextGaussian() / 2.0D;

            for(int var15 = 0; var15 < 6; ++var15) {
               this.a.world.a(EnumParticle.DRAGON_BREATH, var9, var11, var13, -var1.b * 0.07999999821186066D * (double)var15, -var1.c * 0.6000000238418579D, -var1.d * 0.07999999821186066D * (double)var15, new int[0]);
            }

            var1.b(0.19634955F);
         }
      }

   }

   public void c() {
      ++this.b;
      if(this.b >= 200) {
         if(this.c >= 4) {
            this.a.cT().a(class_xj.e);
         } else {
            this.a.cT().a(class_xj.g);
         }
      } else if(this.b == 10) {
         Vec3D var1 = (new Vec3D(this.a.bu.locX - this.a.locX, 0.0D, this.a.bu.locZ - this.a.locZ)).a();
         float var2 = 5.0F;
         double var3 = this.a.bu.locX + var1.b * (double)var2 / 2.0D;
         double var5 = this.a.bu.locZ + var1.d * (double)var2 / 2.0D;
         double var7 = this.a.bu.locY + (double)(this.a.bu.length / 2.0F);
         BlockPosition.class_a_in_class_cj var9 = new BlockPosition.class_a_in_class_cj(MathHelper.c(var3), MathHelper.c(var7), MathHelper.c(var5));

         while(this.a.world.d((BlockPosition)var9)) {
            --var7;
            var9.c(MathHelper.c(var3), MathHelper.c(var7), MathHelper.c(var5));
         }

         var7 = (double)(MathHelper.c(var7) + 1);
         this.d = new class_ro(this.a.world, var3, var7, var5);
         this.d.a((class_rz)this.a);
         this.d.a(var2);
         this.d.b(200);
         this.d.a(EnumParticle.DRAGON_BREATH);
         this.d.a(new MobEffect(MobEffectList.g));
         this.a.world.a((Entity)this.d);
      }

   }

   public void d() {
      this.b = 0;
      ++this.c;
   }

   public void e() {
      if(this.d != null) {
         this.d.S();
         this.d = null;
      }

   }

   public class_xj i() {
      return class_xj.f;
   }

   public void j() {
      this.c = 0;
   }
}
