package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_auc;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_auv extends class_auc {
   public boolean b(World var1, Random var2, BlockPosition var3) {
      for(int var4 = 0; var4 < 20; ++var4) {
         BlockPosition var5 = var3.a(var2.nextInt(4) - var2.nextInt(4), 0, var2.nextInt(4) - var2.nextInt(4));
         if(var1.d(var5)) {
            BlockPosition var6 = var5.b();
            if(var1.getType(var6.e()).getMaterial() == Material.h || var1.getType(var6.f()).getMaterial() == Material.h || var1.getType(var6.c()).getMaterial() == Material.h || var1.getType(var6.d()).getMaterial() == Material.h) {
               int var7 = 2 + var2.nextInt(var2.nextInt(3) + 1);

               for(int var8 = 0; var8 < var7; ++var8) {
                  if(Blocks.aM.b(var1, var5)) {
                     var1.a((BlockPosition)var5.b(var8), (IBlockData)Blocks.aM.u(), 2);
                  }
               }
            }
         }
      }

      return true;
   }
}
