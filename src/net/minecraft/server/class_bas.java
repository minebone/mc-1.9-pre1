package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.util.Map.Entry;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.Entity;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_bax;
import net.minecraft.server.class_bay;
import net.minecraft.server.class_kk;

public class class_bas implements class_baq {
   private final class_bay[] a;
   private final class_azy.class_b_in_class_azy b;

   public class_bas(class_bay[] var1, class_azy.class_b_in_class_azy var2) {
      this.a = var1;
      this.b = var2;
   }

   public boolean a(Random var1, class_azy var2) {
      Entity var3 = var2.a(this.b);
      if(var3 == null) {
         return false;
      } else {
         int var4 = 0;

         for(int var5 = this.a.length; var4 < var5; ++var4) {
            if(!this.a[var4].a(var1, var3)) {
               return false;
            }
         }

         return true;
      }
   }

   public static class class_a_in_class_bas extends class_baq.class_a_in_class_baq {
      protected class_a_in_class_bas() {
         super(new class_kk("entity_properties"), class_bas.class);
      }

      public void a(JsonObject var1, class_bas var2, JsonSerializationContext var3) {
         JsonObject var4 = new JsonObject();
         class_bay[] var5 = var2.a;
         int var6 = var5.length;

         for(int var7 = 0; var7 < var6; ++var7) {
            class_bay var8 = var5[var7];
            class_bay.class_a_in_class_bay var9 = class_bax.a(var8);
            var4.add(var9.a().toString(), var9.a(var8, var3));
         }

         var1.add("properties", var4);
         var1.add("entity", var3.serialize(var2.b));
      }

      public class_bas a(JsonObject var1, JsonDeserializationContext var2) {
         Set var3 = ChatDeserializer.t(var1, "properties").entrySet();
         class_bay[] var4 = new class_bay[var3.size()];
         int var5 = 0;

         Entry var7;
         for(Iterator var6 = var3.iterator(); var6.hasNext(); var4[var5++] = class_bax.a(new class_kk((String)var7.getKey())).a((JsonElement)var7.getValue(), var2)) {
            var7 = (Entry)var6.next();
         }

         return new class_bas(var4, (class_azy.class_b_in_class_azy)ChatDeserializer.a(var1, "entity", var2, class_azy.class_b_in_class_azy.class));
      }

      // $FF: synthetic method
      public class_baq b(JsonObject var1, JsonDeserializationContext var2) {
         return this.a(var1, var2);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(JsonObject var1, class_baq var2, JsonSerializationContext var3) {
         this.a(var1, (class_bas)var2, var3);
      }
   }
}
