package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityMinecartAbstract;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_alr;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;

public class EntityMinecartFurnace extends EntityMinecartAbstract {
   private static final class_ke c = DataWatcher.a(EntityMinecartFurnace.class, class_kg.h);
   private int d;
   public double a;
   public double b;

   public EntityMinecartFurnace(World var1) {
      super(var1);
   }

   public EntityMinecartFurnace(World var1, double var2, double var4, double var6) {
      super(var1, var2, var4, var6);
   }

   public EntityMinecartAbstract.EnumMinecartType v() {
      return EntityMinecartAbstract.EnumMinecartType.FURNACE;
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)c, (Object)Boolean.valueOf(false));
   }

   public void m() {
      super.m();
      if(this.d > 0) {
         --this.d;
      }

      if(this.d <= 0) {
         this.a = this.b = 0.0D;
      }

      this.k(this.d > 0);
      if(this.j() && this.random.nextInt(4) == 0) {
         this.world.a(EnumParticle.SMOKE_LARGE, this.locX, this.locY + 0.8D, this.locZ, 0.0D, 0.0D, 0.0D, new int[0]);
      }

   }

   protected double o() {
      return 0.2D;
   }

   public void a(DamageSource var1) {
      super.a(var1);
      if(!var1.c() && this.world.U().b("doEntityDrops")) {
         this.a(new ItemStack(Blocks.al, 1), 0.0F);
      }

   }

   protected void a(BlockPosition var1, IBlockData var2) {
      super.a(var1, var2);
      double var3 = this.a * this.a + this.b * this.b;
      if(var3 > 1.0E-4D && this.motX * this.motX + this.motZ * this.motZ > 0.001D) {
         var3 = (double)MathHelper.a(var3);
         this.a /= var3;
         this.b /= var3;
         if(this.a * this.motX + this.b * this.motZ < 0.0D) {
            this.a = 0.0D;
            this.b = 0.0D;
         } else {
            double var5 = var3 / this.o();
            this.a *= var5;
            this.b *= var5;
         }
      }

   }

   protected void r() {
      double var1 = this.a * this.a + this.b * this.b;
      if(var1 > 1.0E-4D) {
         var1 = (double)MathHelper.a(var1);
         this.a /= var1;
         this.b /= var1;
         double var3 = 1.0D;
         this.motX *= 0.800000011920929D;
         this.motY *= 0.0D;
         this.motZ *= 0.800000011920929D;
         this.motX += this.a * var3;
         this.motZ += this.b * var3;
      } else {
         this.motX *= 0.9800000190734863D;
         this.motY *= 0.0D;
         this.motZ *= 0.9800000190734863D;
      }

      super.r();
   }

   public boolean a(EntityHuman var1, ItemStack var2, EnumHand var3) {
      if(var2 != null && var2.b() == Items.j && this.d + 3600 <= 32000) {
         if(!var1.abilities.d) {
            --var2.b;
         }

         this.d += 3600;
      }

      this.a = this.locX - var1.locX;
      this.b = this.locZ - var1.locZ;
      return true;
   }

   protected void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("PushX", this.a);
      var1.a("PushZ", this.b);
      var1.a("Fuel", (short)this.d);
   }

   protected void a(NBTTagCompound var1) {
      super.a(var1);
      this.a = var1.k("PushX");
      this.b = var1.k("PushZ");
      this.d = var1.g("Fuel");
   }

   protected boolean j() {
      return ((Boolean)this.datawatcher.a(c)).booleanValue();
   }

   protected void k(boolean var1) {
      this.datawatcher.b(c, Boolean.valueOf(var1));
   }

   public IBlockData x() {
      return (this.j()?Blocks.am:Blocks.al).u().set(class_alr.a, EnumDirection.NORTH);
   }
}
