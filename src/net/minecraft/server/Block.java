package net.minecraft.server;

import com.google.common.collect.Sets;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BlockAnvil;
import net.minecraft.server.BlockBed;
import net.minecraft.server.BlockBrewingStand;
import net.minecraft.server.BlockCake;
import net.minecraft.server.BlockCobbleWall;
import net.minecraft.server.BlockDaylightDetector;
import net.minecraft.server.BlockDirt;
import net.minecraft.server.BlockDoor;
import net.minecraft.server.BlockFenceGate;
import net.minecraft.server.BlockFlowerPot;
import net.minecraft.server.BlockHopper;
import net.minecraft.server.BlockHugeMushroom;
import net.minecraft.server.BlockJukeBox;
import net.minecraft.server.BlockLever;
import net.minecraft.server.BlockLongGrass;
import net.minecraft.server.BlockMonsterEggs;
import net.minecraft.server.BlockPistonExtension;
import net.minecraft.server.BlockPortal;
import net.minecraft.server.BlockPressurePlateBinary;
import net.minecraft.server.BlockPrismarine;
import net.minecraft.server.BlockPumpkin;
import net.minecraft.server.BlockQuartz;
import net.minecraft.server.BlockRedSandstone;
import net.minecraft.server.BlockRedstoneComparator;
import net.minecraft.server.BlockRedstoneWire;
import net.minecraft.server.BlockRepeater;
import net.minecraft.server.BlockSand;
import net.minecraft.server.BlockSandStone;
import net.minecraft.server.BlockSapling;
import net.minecraft.server.BlockSmoothBrick;
import net.minecraft.server.BlockSnow;
import net.minecraft.server.BlockSoil;
import net.minecraft.server.BlockSponge;
import net.minecraft.server.BlockStairs;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.BlockStone;
import net.minecraft.server.BlockTallPlant;
import net.minecraft.server.BlockVine;
import net.minecraft.server.BlockWood;
import net.minecraft.server.BlockWorkbench;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.class_acb;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agn;
import net.minecraft.server.class_agp;
import net.minecraft.server.class_aho;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aji;
import net.minecraft.server.class_ajk;
import net.minecraft.server.class_ajl;
import net.minecraft.server.class_ajp;
import net.minecraft.server.class_ajr;
import net.minecraft.server.class_ajv;
import net.minecraft.server.class_ajz;
import net.minecraft.server.class_akb;
import net.minecraft.server.class_akc;
import net.minecraft.server.BlockChest;
import net.minecraft.server.class_ake;
import net.minecraft.server.class_akf;
import net.minecraft.server.class_akg;
import net.minecraft.server.class_akh;
import net.minecraft.server.class_aki;
import net.minecraft.server.class_akj;
import net.minecraft.server.class_akm;
import net.minecraft.server.class_ako;
import net.minecraft.server.class_akp;
import net.minecraft.server.class_akt;
import net.minecraft.server.class_akw;
import net.minecraft.server.class_akx;
import net.minecraft.server.class_aky;
import net.minecraft.server.class_akz;
import net.minecraft.server.class_ala;
import net.minecraft.server.class_alb;
import net.minecraft.server.class_alc;
import net.minecraft.server.class_ald;
import net.minecraft.server.class_ale;
import net.minecraft.server.class_ali;
import net.minecraft.server.class_alk;
import net.minecraft.server.class_aln;
import net.minecraft.server.class_alo;
import net.minecraft.server.class_alp;
import net.minecraft.server.class_alq;
import net.minecraft.server.class_alr;
import net.minecraft.server.class_als;
import net.minecraft.server.class_alt;
import net.minecraft.server.class_alu;
import net.minecraft.server.class_alv;
import net.minecraft.server.class_alw;
import net.minecraft.server.class_alx;
import net.minecraft.server.class_aly;
import net.minecraft.server.class_alz;
import net.minecraft.server.class_amb;
import net.minecraft.server.class_amc;
import net.minecraft.server.class_amd;
import net.minecraft.server.class_amh;
import net.minecraft.server.class_amj;
import net.minecraft.server.class_amp;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_amr;
import net.minecraft.server.class_amt;
import net.minecraft.server.class_amu;
import net.minecraft.server.class_amv;
import net.minecraft.server.class_amw;
import net.minecraft.server.class_amx;
import net.minecraft.server.class_amy;
import net.minecraft.server.class_amz;
import net.minecraft.server.class_anb;
import net.minecraft.server.class_anc;
import net.minecraft.server.class_and;
import net.minecraft.server.class_ane;
import net.minecraft.server.class_anf;
import net.minecraft.server.class_ang;
import net.minecraft.server.class_anh;
import net.minecraft.server.class_ank;
import net.minecraft.server.class_anl;
import net.minecraft.server.class_anm;
import net.minecraft.server.class_anq;
import net.minecraft.server.class_ans;
import net.minecraft.server.class_ant;
import net.minecraft.server.class_anv;
import net.minecraft.server.class_anx;
import net.minecraft.server.class_any;
import net.minecraft.server.class_anz;
import net.minecraft.server.class_aoa;
import net.minecraft.server.class_aoc;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_aoh;
import net.minecraft.server.class_aoj;
import net.minecraft.server.class_aok;
import net.minecraft.server.class_aol;
import net.minecraft.server.class_aon;
import net.minecraft.server.class_aoo;
import net.minecraft.server.class_aoq;
import net.minecraft.server.class_aor;
import net.minecraft.server.class_aot;
import net.minecraft.server.class_aou;
import net.minecraft.server.class_aov;
import net.minecraft.server.class_aoy;
import net.minecraft.server.class_apa;
import net.minecraft.server.class_apc;
import net.minecraft.server.class_apd;
import net.minecraft.server.class_ape;
import net.minecraft.server.class_apf;
import net.minecraft.server.class_apg;
import net.minecraft.server.class_aph;
import net.minecraft.server.class_apk;
import net.minecraft.server.class_apl;
import net.minecraft.server.class_apm;
import net.minecraft.server.class_apn;
import net.minecraft.server.class_apo;
import net.minecraft.server.class_apq;
import net.minecraft.server.class_apr;
import net.minecraft.server.class_aqt;
import net.minecraft.server.class_aqv;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.class_axg;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_co;
import net.minecraft.server.class_ct;
import net.minecraft.server.class_di;
import net.minecraft.server.class_kk;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rw;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_yc;

public class Block {
   private static final class_kk a = new class_kk("air");
   public static final class_co h = new class_co(a);
   public static final class_ct i = new class_ct();
   public static final AxisAlignedBB j = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
   public static final AxisAlignedBB k = null;
   private CreativeModeTab b;
   protected boolean l;
   protected int m;
   protected boolean n;
   protected int o;
   protected boolean p;
   protected float q;
   protected float r;
   protected boolean s;
   protected boolean t;
   protected boolean u;
   protected class_aoo v;
   public float w;
   protected final Material x;
   protected final MaterialMapColor y;
   public float z;
   protected final BlockStateList A;
   private IBlockData c;
   private String d;

   public static int a(Block var0) {
      return h.a((Object)var0);
   }

   public static int j(IBlockData var0) {
      Block var1 = var0.getBlock();
      return a(var1) + (var1.e(var0) << 12);
   }

   public static Block b(int var0) {
      return (Block)h.a(var0);
   }

   public static IBlockData c(int var0) {
      int var1 = var0 & 4095;
      int var2 = var0 >> 12 & 15;
      return b(var1).a(var2);
   }

   public static Block a(Item var0) {
      return var0 instanceof class_acb?((class_acb)var0).d():null;
   }

   public static Block b(String var0) {
      class_kk var1 = new class_kk(var0);
      if(h.d(var1)) {
         return (Block)h.c(var1);
      } else {
         try {
            return (Block)h.a(Integer.parseInt(var0));
         } catch (NumberFormatException var3) {
            return null;
         }
      }
   }

   public boolean k(IBlockData var1) {
      return var1.getMaterial().k() && var1.h();
   }

   public boolean l(IBlockData var1) {
      return this.l;
   }

   public int m(IBlockData var1) {
      return this.m;
   }

   public int o(IBlockData var1) {
      return this.o;
   }

   public boolean p(IBlockData var1) {
      return this.p;
   }

   public Material q(IBlockData var1) {
      return this.x;
   }

   public MaterialMapColor r(IBlockData var1) {
      return this.y;
   }

   public IBlockData a(int var1) {
      return this.u();
   }

   public int e(IBlockData var1) {
      if(var1 != null && !var1.r().isEmpty()) {
         throw new IllegalArgumentException("Don\'t know how to convert " + var1 + " back into data...");
      } else {
         return 0;
      }
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return var1;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1;
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1;
   }

   public Block(Material var1, MaterialMapColor var2) {
      this.s = true;
      this.v = class_aoo.d;
      this.w = 1.0F;
      this.z = 0.6F;
      this.x = var1;
      this.y = var2;
      this.A = this.b();
      this.w(this.A.b());
      this.l = this.u().p();
      this.m = this.l?255:0;
      this.n = !var1.b();
   }

   protected Block(Material var1) {
      this(var1, var1.r());
   }

   protected Block a(class_aoo var1) {
      this.v = var1;
      return this;
   }

   protected Block d(int var1) {
      this.m = var1;
      return this;
   }

   protected Block a(float var1) {
      this.o = (int)(15.0F * var1);
      return this;
   }

   protected Block b(float var1) {
      this.r = var1 * 3.0F;
      return this;
   }

   public boolean s(IBlockData var1) {
      return var1.getMaterial().c() && var1.h();
   }

   public boolean t(IBlockData var1) {
      return var1.getMaterial().k() && var1.h() && !var1.m();
   }

   public boolean j() {
      return this.x.c() && this.u().h();
   }

   public boolean c(IBlockData var1) {
      return true;
   }

   public boolean b(class_ahw var1, BlockPosition var2) {
      return !this.x.c();
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.MODEL;
   }

   public boolean a(class_ahw var1, BlockPosition var2) {
      return false;
   }

   protected Block c(float var1) {
      this.q = var1;
      if(this.r < var1 * 5.0F) {
         this.r = var1 * 5.0F;
      }

      return this;
   }

   protected Block k() {
      this.c(-1.0F);
      return this;
   }

   public float b(IBlockData var1, World var2, BlockPosition var3) {
      return this.q;
   }

   protected Block a(boolean var1) {
      this.t = var1;
      return this;
   }

   public boolean l() {
      return this.t;
   }

   public boolean m() {
      return this.u;
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return j;
   }

   public boolean a(class_ahw var1, BlockPosition var2, EnumDirection var3) {
      return var1.getType(var2).getMaterial().a();
   }

   public void a(IBlockData var1, World var2, BlockPosition var3, AxisAlignedBB var4, List<AxisAlignedBB> var5, Entity var6) {
      a(var3, var4, var5, var1.d(var2, var3));
   }

   protected static void a(BlockPosition var0, AxisAlignedBB var1, List<AxisAlignedBB> var2, AxisAlignedBB var3) {
      if(var3 != k) {
         AxisAlignedBB var4 = var3.a(var0);
         if(var1.b(var4)) {
            var2.add(var4);
         }
      }

   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return var1.c(var2, var3);
   }

   public boolean b(IBlockData var1) {
      return true;
   }

   public boolean a(IBlockData var1, boolean var2) {
      return this.n();
   }

   public boolean n() {
      return true;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      this.b(var1, var2, var3, var4);
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
   }

   public void d(World var1, BlockPosition var2, IBlockData var3) {
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
   }

   public int a(World var1) {
      return 10;
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
   }

   public int a(Random var1) {
      return 1;
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Item.a(this);
   }

   public float a(IBlockData var1, EntityHuman var2, World var3, BlockPosition var4) {
      float var5 = var1.b(var3, var4);
      return var5 < 0.0F?0.0F:(!var2.b(var1)?var2.a(var1) / var5 / 100.0F:var2.a(var1) / var5 / 30.0F);
   }

   public final void b(World var1, BlockPosition var2, IBlockData var3, int var4) {
      this.a(var1, var2, var3, 1.0F, var4);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, float var4, int var5) {
      if(!var1.E) {
         int var6 = this.a(var5, var1.r);

         for(int var7 = 0; var7 < var6; ++var7) {
            if(var1.r.nextFloat() <= var4) {
               Item var8 = this.a(var3, var1.r, var5);
               if(var8 != null) {
                  a(var1, var2, new ItemStack(var8, 1, this.d(var3)));
               }
            }
         }

      }
   }

   public static void a(World var0, BlockPosition var1, ItemStack var2) {
      if(!var0.E && var0.U().b("doTileDrops")) {
         float var3 = 0.5F;
         double var4 = (double)(var0.r.nextFloat() * var3) + (double)(1.0F - var3) * 0.5D;
         double var6 = (double)(var0.r.nextFloat() * var3) + (double)(1.0F - var3) * 0.5D;
         double var8 = (double)(var0.r.nextFloat() * var3) + (double)(1.0F - var3) * 0.5D;
         class_yc var10 = new class_yc(var0, (double)var1.p() + var4, (double)var1.q() + var6, (double)var1.r() + var8, var2);
         var10.q();
         var0.a((Entity)var10);
      }
   }

   protected void b(World var1, BlockPosition var2, int var3) {
      if(!var1.E && var1.U().b("doTileDrops")) {
         while(var3 > 0) {
            int var4 = class_rw.a(var3);
            var3 -= var4;
            var1.a((Entity)(new class_rw(var1, (double)var2.p() + 0.5D, (double)var2.q() + 0.5D, (double)var2.r() + 0.5D, var4)));
         }
      }

   }

   public int d(IBlockData var1) {
      return 0;
   }

   public float a(Entity var1) {
      return this.r / 5.0F;
   }

   public MovingObjectPosition a(IBlockData var1, World var2, BlockPosition var3, Vec3D var4, Vec3D var5) {
      return this.a(var3, var4, var5, var1.c(var2, var3));
   }

   protected MovingObjectPosition a(BlockPosition var1, Vec3D var2, Vec3D var3, AxisAlignedBB var4) {
      Vec3D var5 = var2.a((double)var1.p(), (double)var1.q(), (double)var1.r());
      Vec3D var6 = var3.a((double)var1.p(), (double)var1.q(), (double)var1.r());
      MovingObjectPosition var7 = var4.a(var5, var6);
      return var7 == null?null:new MovingObjectPosition(var7.c.b((double)var1.p(), (double)var1.q(), (double)var1.r()), var7.b, var1);
   }

   public void a(World var1, BlockPosition var2, class_aho var3) {
   }

   public boolean a(World var1, BlockPosition var2, EnumDirection var3, ItemStack var4) {
      return this.b(var1, var2, var3);
   }

   public boolean b(World var1, BlockPosition var2, EnumDirection var3) {
      return this.a(var1, var2);
   }

   public boolean a(World var1, BlockPosition var2) {
      return var1.getType(var2).getBlock().x.j();
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      return false;
   }

   public void a(World var1, BlockPosition var2, Entity var3) {
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return this.a(var7);
   }

   public void a(World var1, BlockPosition var2, EntityHuman var3) {
   }

   public Vec3D a(World var1, BlockPosition var2, Entity var3, Vec3D var4) {
      return var4;
   }

   public int b(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return 0;
   }

   public boolean g(IBlockData var1) {
      return false;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Entity var4) {
   }

   public int c(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return 0;
   }

   public void a(World var1, EntityHuman var2, BlockPosition var3, IBlockData var4, TileEntity var5, ItemStack var6) {
      var2.b(StatisticList.a(this));
      var2.a(0.025F);
      if(this.o() && class_agn.a(class_agp.r, var6) > 0) {
         ItemStack var8 = this.u(var4);
         if(var8 != null) {
            a(var1, var3, var8);
         }
      } else {
         int var7 = class_agn.a(class_agp.t, var6);
         this.b(var1, var3, var4, var7);
      }

   }

   protected boolean o() {
      return this.u().h() && !this.u;
   }

   protected ItemStack u(IBlockData var1) {
      Item var2 = Item.a(this);
      if(var2 == null) {
         return null;
      } else {
         int var3 = 0;
         if(var2.k()) {
            var3 = this.e(var1);
         }

         return new ItemStack(var2, 1, var3);
      }
   }

   public int a(int var1, Random var2) {
      return this.a(var2);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, class_rz var4, ItemStack var5) {
   }

   public boolean d() {
      return !this.x.a() && !this.x.d();
   }

   public Block c(String var1) {
      this.d = var1;
      return this;
   }

   public String c() {
      return class_di.a(this.a() + ".name");
   }

   public String a() {
      return "tile." + this.d;
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, int var4, int var5) {
      return false;
   }

   public boolean p() {
      return this.s;
   }

   protected Block q() {
      this.s = false;
      return this;
   }

   public class_axg h(IBlockData var1) {
      return this.x.m();
   }

   public void a(World var1, BlockPosition var2, Entity var3, float var4) {
      var3.e(var4, 1.0F);
   }

   public void a(World var1, Entity var2) {
      var2.motY = 0.0D;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Item.a(this), 1, this.d(var3));
   }

   public Block a(CreativeModeTab var1) {
      this.b = var1;
      return this;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4) {
   }

   public void h(World var1, BlockPosition var2) {
   }

   public boolean s() {
      return true;
   }

   public boolean a(class_aho var1) {
      return true;
   }

   public boolean b(Block var1) {
      return this == var1;
   }

   public static boolean a(Block var0, Block var1) {
      return var0 != null && var1 != null?(var0 == var1?true:var0.b(var1)):false;
   }

   public boolean v(IBlockData var1) {
      return false;
   }

   public int d(IBlockData var1, World var2, BlockPosition var3) {
      return 0;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[0]);
   }

   public BlockStateList t() {
      return this.A;
   }

   protected final void w(IBlockData var1) {
      this.c = var1;
   }

   public final IBlockData u() {
      return this.c;
   }

   public class_aoo w() {
      return this.v;
   }

   public String toString() {
      return "Block{" + h.b(this) + "}";
   }

   public static void x() {
      a(0, (class_kk)a, (new class_aji()).c("air"));
      a(1, (String)"stone", (new BlockStone()).c(1.5F).b(10.0F).a(class_aoo.d).c("stone"));
      a(2, (String)"grass", (new class_alu()).c(0.6F).a(class_aoo.c).c("grass"));
      a(3, (String)"dirt", (new BlockDirt()).c(0.5F).a(class_aoo.b).c("dirt"));
      Block var0 = (new Block(Material.e)).c(2.0F).b(10.0F).a(class_aoo.d).c("stonebrick").a(CreativeModeTab.b);
      a(4, (String)"cobblestone", var0);
      Block var1 = (new BlockWood()).c(2.0F).b(5.0F).a(class_aoo.a).c("wood");
      a(5, (String)"planks", var1);
      a(6, (String)"sapling", (new BlockSapling()).c(0.0F).a(class_aoo.c).c("sapling"));
      a(7, (String)"bedrock", (new class_anb(Material.e)).k().b(6000000.0F).a(class_aoo.d).c("bedrock").q().a(CreativeModeTab.b));
      a(8, (String)"flowing_water", (new class_aky(Material.h)).c(100.0F).d(3).c("water").q());
      a(9, (String)"water", (new class_aou(Material.h)).c(100.0F).d(3).c("water").q());
      a(10, (String)"flowing_lava", (new class_aky(Material.i)).c(100.0F).a(1.0F).c("lava").q());
      a(11, (String)"lava", (new class_aou(Material.i)).c(100.0F).a(1.0F).c("lava").q());
      a(12, (String)"sand", (new BlockSand()).c(0.5F).a(class_aoo.h).c("sand"));
      a(13, (String)"gravel", (new class_alw()).c(0.6F).a(class_aoo.b).c("gravel"));
      a(14, (String)"gold_ore", (new class_ang()).c(3.0F).b(5.0F).a(class_aoo.d).c("oreGold"));
      a(15, (String)"iron_ore", (new class_ang()).c(3.0F).b(5.0F).a(class_aoo.d).c("oreIron"));
      a(16, (String)"coal_ore", (new class_ang()).c(3.0F).b(5.0F).a(class_aoo.d).c("oreCoal"));
      a(17, (String)"log", (new class_anf()).c("log"));
      a(18, (String)"leaves", (new class_ane()).c("leaves"));
      a(19, (String)"sponge", (new BlockSponge()).c(0.6F).a(class_aoo.c).c("sponge"));
      a(20, (String)"glass", (new class_als(Material.s, false)).c(0.3F).a(class_aoo.f).c("glass"));
      a(21, (String)"lapis_ore", (new class_ang()).c(3.0F).b(5.0F).a(class_aoo.d).c("oreLapis"));
      a(22, (String)"lapis_block", (new Block(Material.f, MaterialMapColor.H)).c(3.0F).b(5.0F).a(class_aoo.d).c("blockLapis").a(CreativeModeTab.b));
      a(23, (String)"dispenser", (new class_akt()).c(3.5F).a(class_aoo.d).c("dispenser"));
      Block var2 = (new BlockSandStone()).a(class_aoo.d).c(0.8F).c("sandStone");
      a(24, (String)"sandstone", var2);
      a(25, (String)"noteblock", (new class_anc()).a(class_aoo.a).c(0.8F).c("musicBlock"));
      a(26, (String)"bed", (new BlockBed()).a(class_aoo.a).c(0.2F).c("bed").q());
      a(27, (String)"golden_rail", (new class_anm()).c(0.7F).a(class_aoo.e).c("goldenRail"));
      a(28, (String)"detector_rail", (new class_akp()).c(0.7F).a(class_aoo.e).c("detectorRail"));
      a(29, (String)"sticky_piston", (new class_aqt(true)).c("pistonStickyBase"));
      a(30, (String)"web", (new class_apm()).d(1).c(4.0F).c("web"));
      a(31, (String)"tallgrass", (new BlockLongGrass()).c(0.0F).a(class_aoo.c).c("tallgrass"));
      a(32, (String)"deadbush", (new class_ako()).c(0.0F).a(class_aoo.c).c("deadbush"));
      a(33, (String)"piston", (new class_aqt(false)).c("pistonBase"));
      a(34, (String)"piston_head", (new BlockPistonExtension()).c("pistonBase"));
      a(35, (String)"wool", (new class_aki(Material.n)).c(0.8F).a(class_aoo.g).c("cloth"));
      a(36, (String)"piston_extension", new class_aqv());
      a(37, (String)"yellow_flower", (new class_apr()).c(0.0F).a(class_aoo.c).c("flower1"));
      a(38, (String)"red_flower", (new class_ant()).c(0.0F).a(class_aoo.c).c("flower2"));
      Block var3 = (new class_amt()).c(0.0F).a(class_aoo.c).a(0.125F).c("mushroom");
      a(39, (String)"brown_mushroom", var3);
      Block var4 = (new class_amt()).c(0.0F).a(class_aoo.c).c("mushroom");
      a(40, (String)"red_mushroom", var4);
      a(41, (String)"gold_block", (new Block(Material.f, MaterialMapColor.F)).c(3.0F).b(10.0F).a(class_aoo.e).c("blockGold").a(CreativeModeTab.b));
      a(42, (String)"iron_block", (new Block(Material.f, MaterialMapColor.h)).c(5.0F).b(10.0F).a(class_aoo.e).c("blockIron").a(CreativeModeTab.b));
      a(43, (String)"double_stone_slab", (new class_alp()).c(2.0F).b(10.0F).a(class_aoo.d).c("stoneSlab"));
      a(44, (String)"stone_slab", (new class_alz()).c(2.0F).b(10.0F).a(class_aoo.d).c("stoneSlab"));
      Block var5 = (new Block(Material.e, MaterialMapColor.D)).c(2.0F).b(10.0F).a(class_aoo.d).c("brick").a(CreativeModeTab.b);
      a(45, (String)"brick_block", var5);
      a(46, (String)"tnt", (new class_apd()).c(0.0F).a(class_aoo.c).c("tnt"));
      a(47, (String)"bookshelf", (new class_ajv()).c(1.5F).a(class_aoo.a).c("bookshelf"));
      a(48, (String)"mossy_cobblestone", (new Block(Material.e)).c(2.0F).b(10.0F).a(class_aoo.d).c("stoneMoss").a(CreativeModeTab.b));
      a(49, (String)"obsidian", (new class_and()).c(50.0F).b(2000.0F).a(class_aoo.d).c("obsidian"));
      a(50, (String)"torch", (new class_ape()).c(0.0F).a(0.9375F).a(class_aoo.a).c("torch"));
      a(51, (String)"fire", (new class_alk()).c(0.0F).a(1.0F).a(class_aoo.g).c("fire").q());
      a(52, (String)"mob_spawner", (new class_amr()).c(5.0F).a(class_aoo.e).c("mobSpawner").q());
      a(53, (String)"oak_stairs", (new BlockStairs(var1.u().set(BlockWood.a, BlockWood.EnumLogVariant.OAK))).c("stairsWood"));
      a(54, (String)"chest", (new BlockChest(BlockChest.class_a_in_class_akd.BASIC)).c(2.5F).a(class_aoo.a).c("chest"));
      a(55, (String)"redstone_wire", (new BlockRedstoneWire()).c(0.0F).a(class_aoo.d).c("redstoneDust").q());
      a(56, (String)"diamond_ore", (new class_ang()).c(3.0F).b(5.0F).a(class_aoo.d).c("oreDiamond"));
      a(57, (String)"diamond_block", (new Block(Material.f, MaterialMapColor.G)).c(5.0F).b(10.0F).a(class_aoo.e).c("blockDiamond").a(CreativeModeTab.b));
      a(58, (String)"crafting_table", (new BlockWorkbench()).c(2.5F).a(class_aoo.a).c("workbench"));
      a(59, (String)"wheat", (new class_akm()).c("crops"));
      Block var6 = (new BlockSoil()).c(0.6F).a(class_aoo.b).c("farmland");
      a(60, (String)"farmland", var6);
      a(61, (String)"furnace", (new class_alr(false)).c(3.5F).a(class_aoo.d).c("furnace").a(CreativeModeTab.c));
      a(62, (String)"lit_furnace", (new class_alr(true)).c(3.5F).a(class_aoo.d).a(0.875F).c("furnace"));
      a(63, (String)"standing_sign", (new class_aot()).c(1.0F).a(class_aoo.a).c("sign").q());
      a(64, (String)"wooden_door", (new BlockDoor(Material.d)).c(3.0F).a(class_aoo.a).c("doorOak").q());
      a(65, (String)"ladder", (new class_amj()).c(0.4F).a(class_aoo.j).c("ladder"));
      a(66, (String)"rail", (new class_ans()).c(0.7F).a(class_aoo.e).c("rail"));
      a(67, (String)"stone_stairs", (new BlockStairs(var0.u())).c("stairsStone"));
      a(68, (String)"wall_sign", (new class_apk()).c(1.0F).a(class_aoo.a).c("sign").q());
      a(69, (String)"lever", (new BlockLever()).c(0.5F).a(class_aoo.a).c("lever"));
      a(70, (String)"stone_pressure_plate", (new BlockPressurePlateBinary(Material.e, BlockPressurePlateBinary.EnumMobType.MOBS)).c(0.5F).a(class_aoo.d).c("pressurePlateStone"));
      a(71, (String)"iron_door", (new BlockDoor(Material.f)).c(5.0F).a(class_aoo.e).c("doorIron").q());
      a(72, (String)"wooden_pressure_plate", (new BlockPressurePlateBinary(Material.d, BlockPressurePlateBinary.EnumMobType.EVERYTHING)).c(0.5F).a(class_aoo.a).c("pressurePlateWood"));
      a(73, (String)"redstone_ore", (new class_anv(false)).c(3.0F).b(5.0F).a(class_aoo.d).c("oreRedstone").a(CreativeModeTab.b));
      a(74, (String)"lit_redstone_ore", (new class_anv(true)).a(0.625F).c(3.0F).b(5.0F).a(class_aoo.d).c("oreRedstone"));
      a(75, (String)"unlit_redstone_torch", (new class_any(false)).c(0.0F).a(class_aoo.a).c("notGate"));
      a(76, (String)"redstone_torch", (new class_any(true)).c(0.0F).a(0.5F).a(class_aoo.a).c("notGate").a(CreativeModeTab.d));
      a(77, (String)"stone_button", (new class_aoy()).c(0.5F).a(class_aoo.d).c("button"));
      a(78, (String)"snow_layer", (new BlockSnow()).c(0.1F).a(class_aoo.i).c("snow").d(0));
      a(79, (String)"ice", (new class_amh()).c(0.5F).d(3).a(class_aoo.f).c("ice"));
      a(80, (String)"snow", (new class_aol()).c(0.2F).a(class_aoo.i).c("snow"));
      a(81, (String)"cactus", (new class_ajz()).c(0.4F).a(class_aoo.g).c("cactus"));
      a(82, (String)"clay", (new class_akg()).c(0.6F).a(class_aoo.b).c("clay"));
      a(83, (String)"reeds", (new class_anz()).c(0.0F).a(class_aoo.c).c("reeds").q());
      a(84, (String)"jukebox", (new BlockJukeBox()).c(2.0F).b(10.0F).a(class_aoo.d).c("jukebox"));
      a(85, (String)"fence", (new class_ali(Material.d, BlockWood.EnumLogVariant.OAK.c())).c(2.0F).b(5.0F).a(class_aoo.a).c("fence"));
      Block var7 = (new BlockPumpkin()).c(1.0F).a(class_aoo.a).c("pumpkin");
      a(86, (String)"pumpkin", var7);
      a(87, (String)"netherrack", (new class_amx()).c(0.4F).a(class_aoo.d).c("hellrock"));
      a(88, (String)"soul_sand", (new class_aon()).c(0.5F).a(class_aoo.h).c("hellsand"));
      a(89, (String)"glowstone", (new class_alt(Material.s)).c(0.3F).a(class_aoo.f).a(1.0F).c("lightgem"));
      a(90, (String)"portal", (new BlockPortal()).c(-1.0F).a(class_aoo.f).a(0.75F).c("portal"));
      a(91, (String)"lit_pumpkin", (new BlockPumpkin()).c(1.0F).a(class_aoo.a).a(1.0F).c("litpumpkin"));
      a(92, (String)"cake", (new BlockCake()).c(0.5F).a(class_aoo.g).c("cake").q());
      a(93, (String)"unpowered_repeater", (new BlockRepeater(false)).c(0.0F).a(class_aoo.a).c("diode").q());
      a(94, (String)"powered_repeater", (new BlockRepeater(true)).c(0.0F).a(class_aoo.a).c("diode").q());
      a(95, (String)"stained_glass", (new class_aoq(Material.s)).c(0.3F).a(class_aoo.f).c("stainedGlass"));
      a(96, (String)"trapdoor", (new class_apf(Material.d)).c(3.0F).a(class_aoo.a).c("trapdoor").q());
      a(97, (String)"monster_egg", (new BlockMonsterEggs()).c(0.75F).c("monsterStoneEgg"));
      Block var8 = (new BlockSmoothBrick()).c(1.5F).b(10.0F).a(class_aoo.d).c("stonebricksmooth");
      a(98, (String)"stonebrick", var8);
      a(99, (String)"brown_mushroom_block", (new BlockHugeMushroom(Material.d, MaterialMapColor.l, var3)).c(0.2F).a(class_aoo.a).c("mushroom"));
      a(100, (String)"red_mushroom_block", (new BlockHugeMushroom(Material.d, MaterialMapColor.D, var4)).c(0.2F).a(class_aoo.a).c("mushroom"));
      a(101, (String)"iron_bars", (new class_apc(Material.f, true)).c(5.0F).b(10.0F).a(class_aoo.e).c("fenceIron"));
      a(102, (String)"glass_pane", (new class_apc(Material.s, false)).c(0.3F).a(class_aoo.f).c("thinGlass"));
      Block var9 = (new class_amp()).c(1.0F).a(class_aoo.a).c("melon");
      a(103, (String)"melon_block", var9);
      a(104, (String)"pumpkin_stem", (new class_aov(var7)).c(0.0F).a(class_aoo.a).c("pumpkinStem"));
      a(105, (String)"melon_stem", (new class_aov(var9)).c(0.0F).a(class_aoo.a).c("pumpkinStem"));
      a(106, (String)"vine", (new BlockVine()).c(0.2F).a(class_aoo.c).c("vine"));
      a(107, (String)"fence_gate", (new BlockFenceGate(BlockWood.EnumLogVariant.OAK)).c(2.0F).b(5.0F).a(class_aoo.a).c("fenceGate"));
      a(108, (String)"brick_stairs", (new BlockStairs(var5.u())).c("stairsBrick"));
      a(109, (String)"stone_brick_stairs", (new BlockStairs(var8.u().set(BlockSmoothBrick.a, BlockSmoothBrick.EnumStonebrickType.DEFAULT))).c("stairsStoneBrickSmooth"));
      a(110, (String)"mycelium", (new class_amu()).c(0.6F).a(class_aoo.c).c("mycel"));
      a(111, (String)"waterlily", (new class_apl()).c(0.0F).a(class_aoo.c).c("waterlily"));
      Block var10 = (new class_amv()).c(2.0F).b(10.0F).a(class_aoo.d).c("netherBrick").a(CreativeModeTab.b);
      a(112, (String)"nether_brick", var10);
      a(113, (String)"nether_brick_fence", (new class_ali(Material.e, MaterialMapColor.K)).c(2.0F).b(10.0F).a(class_aoo.d).c("netherFence"));
      a(114, (String)"nether_brick_stairs", (new BlockStairs(var10.u())).c("stairsNetherBrick"));
      a(115, (String)"nether_wart", (new class_amw()).c("netherStalk"));
      a(116, (String)"enchanting_table", (new class_akz()).c(5.0F).b(2000.0F).c("enchantmentTable"));
      a(117, (String)"brewing_stand", (new BlockBrewingStand()).c(0.5F).a(0.125F).c("brewingStand"));
      a(118, (String)"cauldron", (new class_akc()).c(2.0F).c("cauldron"));
      a(119, (String)"end_portal", (new class_alb(Material.E)).c(-1.0F).b(6000000.0F));
      a(120, (String)"end_portal_frame", (new class_alc()).a(class_aoo.f).a(0.125F).c(-1.0F).c("endPortalFrame").b(6000000.0F).a(CreativeModeTab.c));
      a(121, (String)"end_stone", (new Block(Material.e, MaterialMapColor.d)).c(3.0F).b(15.0F).a(class_aoo.d).c("whiteStone").a(CreativeModeTab.b));
      a(122, (String)"dragon_egg", (new class_akw()).c(3.0F).b(15.0F).a(class_aoo.d).a(0.125F).c("dragonEgg"));
      a(123, (String)"redstone_lamp", (new class_anx(false)).c(0.3F).a(class_aoo.f).c("redstoneLight").a(CreativeModeTab.d));
      a(124, (String)"lit_redstone_lamp", (new class_anx(true)).c(0.3F).a(class_aoo.f).c("redstoneLight"));
      a(125, (String)"double_wooden_slab", (new class_alq()).c(2.0F).b(5.0F).a(class_aoo.a).c("woodSlab"));
      a(126, (String)"wooden_slab", (new class_amb()).c(2.0F).b(5.0F).a(class_aoo.a).c("woodSlab"));
      a(127, (String)"cocoa", (new class_akh()).c(0.2F).b(5.0F).a(class_aoo.a).c("cocoa"));
      a(128, (String)"sandstone_stairs", (new BlockStairs(var2.u().set(BlockSandStone.a, BlockSandStone.EnumSandstoneVariant.SMOOTH))).c("stairsSandStone"));
      a(129, (String)"emerald_ore", (new class_ang()).c(3.0F).b(5.0F).a(class_aoo.d).c("oreEmerald"));
      a(130, (String)"ender_chest", (new class_ale()).c(22.5F).b(1000.0F).a(class_aoo.d).c("enderChest").a(0.5F));
      a(131, (String)"tripwire_hook", (new class_aph()).c("tripWireSource"));
      a(132, (String)"tripwire", (new class_apg()).c("tripWire"));
      a(133, (String)"emerald_block", (new Block(Material.f, MaterialMapColor.I)).c(5.0F).b(10.0F).a(class_aoo.e).c("blockEmerald").a(CreativeModeTab.b));
      a(134, (String)"spruce_stairs", (new BlockStairs(var1.u().set(BlockWood.a, BlockWood.EnumLogVariant.SPRUCE))).c("stairsWoodSpruce"));
      a(135, (String)"birch_stairs", (new BlockStairs(var1.u().set(BlockWood.a, BlockWood.EnumLogVariant.BIRCH))).c("stairsWoodBirch"));
      a(136, (String)"jungle_stairs", (new BlockStairs(var1.u().set(BlockWood.a, BlockWood.EnumLogVariant.JUNGLE))).c("stairsWoodJungle"));
      a(137, (String)"command_block", (new class_akj(MaterialMapColor.B)).k().b(6000000.0F).c("commandBlock"));
      a(138, (String)"beacon", (new class_ajp()).c("beacon").a(1.0F));
      a(139, (String)"cobblestone_wall", (new BlockCobbleWall(var0)).c("cobbleWall"));
      a(140, (String)"flower_pot", (new BlockFlowerPot()).c(0.0F).a(class_aoo.d).c("flowerPot"));
      a(141, (String)"carrots", (new class_akb()).c("carrots"));
      a(142, (String)"potatoes", (new class_ank()).c("potatoes"));
      a(143, (String)"wooden_button", (new class_apo()).c(0.5F).a(class_aoo.a).c("button"));
      a(144, (String)"skull", (new class_aoj()).c(1.0F).a(class_aoo.d).c("skull"));
      a(145, (String)"anvil", (new BlockAnvil()).c(5.0F).a(class_aoo.k).b(2000.0F).c("anvil"));
      a(146, (String)"trapped_chest", (new BlockChest(BlockChest.class_a_in_class_akd.TRAP)).c(2.5F).a(class_aoo.a).c("chestTrap"));
      a(147, (String)"light_weighted_pressure_plate", (new class_apn(Material.f, 15, MaterialMapColor.F)).c(0.5F).a(class_aoo.a).c("weightedPlate_light"));
      a(148, (String)"heavy_weighted_pressure_plate", (new class_apn(Material.f, 150)).c(0.5F).a(class_aoo.a).c("weightedPlate_heavy"));
      a(149, (String)"unpowered_comparator", (new BlockRedstoneComparator(false)).c(0.0F).a(class_aoo.a).c("comparator").q());
      a(150, (String)"powered_comparator", (new BlockRedstoneComparator(true)).c(0.0F).a(0.625F).a(class_aoo.a).c("comparator").q());
      a(151, (String)"daylight_detector", new BlockDaylightDetector(false));
      a(152, (String)"redstone_block", (new class_anl(Material.f, MaterialMapColor.f)).c(5.0F).b(10.0F).a(class_aoo.e).c("blockRedstone").a(CreativeModeTab.d));
      a(153, (String)"quartz_ore", (new class_ang(MaterialMapColor.K)).c(3.0F).b(5.0F).a(class_aoo.d).c("netherquartz"));
      a(154, (String)"hopper", (new BlockHopper()).c(3.0F).b(8.0F).a(class_aoo.e).c("hopper"));
      Block var11 = (new BlockQuartz()).a(class_aoo.d).c(0.8F).c("quartzBlock");
      a(155, (String)"quartz_block", var11);
      a(156, (String)"quartz_stairs", (new BlockStairs(var11.u().set(BlockQuartz.a, BlockQuartz.EnumQuartzVariant.DEFAULT))).c("stairsQuartz"));
      a(157, (String)"activator_rail", (new class_anm()).c(0.7F).a(class_aoo.e).c("activatorRail"));
      a(158, (String)"dropper", (new class_akx()).c(3.5F).a(class_aoo.d).c("dropper"));
      a(159, (String)"stained_hardened_clay", (new class_aki(Material.e)).c(1.25F).b(7.0F).a(class_aoo.d).c("clayHardenedStained"));
      a(160, (String)"stained_glass_pane", (new class_aor()).c(0.3F).a(class_aoo.f).c("thinStainedGlass"));
      a(161, (String)"leaves2", (new class_amy()).c("leaves"));
      a(162, (String)"log2", (new class_amz()).c("log"));
      a(163, (String)"acacia_stairs", (new BlockStairs(var1.u().set(BlockWood.a, BlockWood.EnumLogVariant.ACACIA))).c("stairsWoodAcacia"));
      a(164, (String)"dark_oak_stairs", (new BlockStairs(var1.u().set(BlockWood.a, BlockWood.EnumLogVariant.DARK_OAK))).c("stairsWoodDarkOak"));
      a(165, (String)"slime", (new class_aok()).c("slime").a(class_aoo.l));
      a(166, (String)"barrier", (new class_ajl()).c("barrier"));
      a(167, (String)"iron_trapdoor", (new class_apf(Material.f)).c(5.0F).a(class_aoo.e).c("ironTrapdoor").q());
      a(168, (String)"prismarine", (new BlockPrismarine()).c(1.5F).b(10.0F).a(class_aoo.d).c("prismarine"));
      a(169, (String)"sea_lantern", (new class_aoh(Material.s)).c(0.3F).a(class_aoo.f).a(1.0F).c("seaLantern"));
      a(170, (String)"hay_block", (new class_amd()).c(0.5F).a(class_aoo.c).c("hayBlock").a(CreativeModeTab.b));
      a(171, (String)"carpet", (new class_apq()).c(0.1F).a(class_aoo.g).c("woolCarpet").d(0));
      a(172, (String)"hardened_clay", (new class_amc()).c(1.25F).b(7.0F).a(class_aoo.d).c("clayHardened"));
      a(173, (String)"coal_block", (new Block(Material.e, MaterialMapColor.E)).c(5.0F).b(10.0F).a(class_aoo.d).c("blockCoal").a(CreativeModeTab.b));
      a(174, (String)"packed_ice", (new class_anh()).c(0.5F).a(class_aoo.f).c("icePacked"));
      a(175, (String)"double_plant", new BlockTallPlant());
      a(176, (String)"standing_banner", (new class_ajk.class_a_in_class_ajk()).c(1.0F).a(class_aoo.a).c("banner").q());
      a(177, (String)"wall_banner", (new class_ajk.class_b_in_class_ajk()).c(1.0F).a(class_aoo.a).c("banner").q());
      a(178, (String)"daylight_detector_inverted", new BlockDaylightDetector(true));
      Block var12 = (new BlockRedSandstone()).a(class_aoo.d).c(0.8F).c("redSandStone");
      a(179, (String)"red_sandstone", var12);
      a(180, (String)"red_sandstone_stairs", (new BlockStairs(var12.u().set(BlockRedSandstone.a, BlockRedSandstone.EnumRedSandstoneVariant.SMOOTH))).c("stairsRedSandStone"));
      a(181, (String)"double_stone_slab2", (new class_alo()).c(2.0F).b(10.0F).a(class_aoo.d).c("stoneSlab2"));
      a(182, (String)"stone_slab2", (new class_alx()).c(2.0F).b(10.0F).a(class_aoo.d).c("stoneSlab2"));
      a(183, (String)"spruce_fence_gate", (new BlockFenceGate(BlockWood.EnumLogVariant.SPRUCE)).c(2.0F).b(5.0F).a(class_aoo.a).c("spruceFenceGate"));
      a(184, (String)"birch_fence_gate", (new BlockFenceGate(BlockWood.EnumLogVariant.BIRCH)).c(2.0F).b(5.0F).a(class_aoo.a).c("birchFenceGate"));
      a(185, (String)"jungle_fence_gate", (new BlockFenceGate(BlockWood.EnumLogVariant.JUNGLE)).c(2.0F).b(5.0F).a(class_aoo.a).c("jungleFenceGate"));
      a(186, (String)"dark_oak_fence_gate", (new BlockFenceGate(BlockWood.EnumLogVariant.DARK_OAK)).c(2.0F).b(5.0F).a(class_aoo.a).c("darkOakFenceGate"));
      a(187, (String)"acacia_fence_gate", (new BlockFenceGate(BlockWood.EnumLogVariant.ACACIA)).c(2.0F).b(5.0F).a(class_aoo.a).c("acaciaFenceGate"));
      a(188, (String)"spruce_fence", (new class_ali(Material.d, BlockWood.EnumLogVariant.SPRUCE.c())).c(2.0F).b(5.0F).a(class_aoo.a).c("spruceFence"));
      a(189, (String)"birch_fence", (new class_ali(Material.d, BlockWood.EnumLogVariant.BIRCH.c())).c(2.0F).b(5.0F).a(class_aoo.a).c("birchFence"));
      a(190, (String)"jungle_fence", (new class_ali(Material.d, BlockWood.EnumLogVariant.JUNGLE.c())).c(2.0F).b(5.0F).a(class_aoo.a).c("jungleFence"));
      a(191, (String)"dark_oak_fence", (new class_ali(Material.d, BlockWood.EnumLogVariant.DARK_OAK.c())).c(2.0F).b(5.0F).a(class_aoo.a).c("darkOakFence"));
      a(192, (String)"acacia_fence", (new class_ali(Material.d, BlockWood.EnumLogVariant.ACACIA.c())).c(2.0F).b(5.0F).a(class_aoo.a).c("acaciaFence"));
      a(193, (String)"spruce_door", (new BlockDoor(Material.d)).c(3.0F).a(class_aoo.a).c("doorSpruce").q());
      a(194, (String)"birch_door", (new BlockDoor(Material.d)).c(3.0F).a(class_aoo.a).c("doorBirch").q());
      a(195, (String)"jungle_door", (new BlockDoor(Material.d)).c(3.0F).a(class_aoo.a).c("doorJungle").q());
      a(196, (String)"acacia_door", (new BlockDoor(Material.d)).c(3.0F).a(class_aoo.a).c("doorAcacia").q());
      a(197, (String)"dark_oak_door", (new BlockDoor(Material.d)).c(3.0F).a(class_aoo.a).c("doorDarkOak").q());
      a(198, (String)"end_rod", (new class_ald()).c(0.0F).a(0.9375F).a(class_aoo.a).c("endRod"));
      a(199, (String)"chorus_plant", (new class_akf()).c(0.4F).a(class_aoo.a).c("chorusPlant"));
      a(200, (String)"chorus_flower", (new class_ake()).c(0.4F).a(class_aoo.a).c("chorusFlower"));
      Block var13 = (new Block(Material.e)).c(1.5F).b(10.0F).a(class_aoo.d).a(CreativeModeTab.b).c("purpurBlock");
      a(201, (String)"purpur_block", var13);
      a(202, (String)"purpur_pillar", (new class_aoc(Material.e)).c(1.5F).b(10.0F).a(class_aoo.d).a(CreativeModeTab.b).c("purpurPillar"));
      a(203, (String)"purpur_stairs", (new BlockStairs(var13.u())).c("stairsPurpur"));
      a(204, (String)"purpur_double_slab", (new class_anq.class_a_in_class_anq()).c(2.0F).b(10.0F).a(class_aoo.d).c("purpurSlab"));
      a(205, (String)"purpur_slab", (new class_anq.class_b_in_class_anq()).c(2.0F).b(10.0F).a(class_aoo.d).c("purpurSlab"));
      a(206, (String)"end_bricks", (new Block(Material.e)).a(class_aoo.d).c(0.8F).a(CreativeModeTab.b).c("endBricks"));
      a(207, (String)"beetroots", (new class_ajr()).c("beetroots"));
      Block var14 = (new class_alv()).c(0.65F).a(class_aoo.c).c("grassPath").q();
      a(208, (String)"grass_path", var14);
      a(209, (String)"end_gateway", (new class_ala(Material.E)).c(-1.0F).b(6000000.0F));
      a(210, (String)"repeating_command_block", (new class_akj(MaterialMapColor.z)).k().b(6000000.0F).c("repeatingCommandBlock"));
      a(211, (String)"chain_command_block", (new class_akj(MaterialMapColor.C)).k().b(6000000.0F).c("chainCommandBlock"));
      a(212, (String)"frosted_ice", (new class_aln()).c(0.5F).d(3).a(class_aoo.f).c("frostedIce"));
      a(255, (String)"structure_block", (new class_apa()).k().b(6000000.0F).c("structureBlock").a(1.0F));
      h.a();
      Iterator<?> var15 = h.iterator();

      while(true) {
         while(var15.hasNext()) {
            Block var16 = (Block)var15.next();
            if(var16.x == Material.a) {
               var16.p = false;
            } else {
               boolean var17 = false;
               boolean var18 = var16 instanceof BlockStairs;
               boolean var19 = var16 instanceof class_aly;
               boolean var20 = var16 == var6 || var16 == var14;
               boolean var21 = var16.n;
               boolean var22 = var16.m == 0;
               if(var18 || var19 || var20 || var21 || var22) {
                  var17 = true;
               }

               var16.p = var17;
            }
         }

         HashSet<Object> var23 = Sets.newHashSet((Object[])(new Block[]{(Block)h.c(new class_kk("tripwire"))}));
         Iterator<?> var24 = h.iterator();

         while(true) {
            while(var24.hasNext()) {
               Block var25 = (Block)var24.next();
               if(var23.contains(var25)) {
                  for(int var27 = 0; var27 < 15; ++var27) {
                     int var29 = h.a((Object)var25) << 4 | var27;
                     i.a(var25.a(var27), var29);
                  }
               } else {
                  Iterator<?> var26 = var25.t().a().iterator();

                  while(var26.hasNext()) {
                     IBlockData var28 = (IBlockData)var26.next();
                     int var30 = h.a((Object)var25) << 4 | var25.e(var28);
                     i.a(var28, var30);
                  }
               }
            }

            return;
         }
      }
   }

   private static void a(int var0, class_kk var1, Block var2) {
      h.a(var0, var1, var2);
   }

   private static void a(int var0, String var1, Block var2) {
      a(var0, new class_kk(var1), var2);
   }
}
