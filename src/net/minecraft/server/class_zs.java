package net.minecraft.server;

import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Entity;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.MathHelper;

public final class class_zs {
   public static MovingObjectPosition a(Entity var0, boolean var1, boolean var2, Entity var3) {
      double var4 = var0.locX;
      double var6 = var0.locY;
      double var8 = var0.locZ;
      double var10 = var0.motX;
      double var12 = var0.motY;
      double var14 = var0.motZ;
      World var16 = var0.world;
      Vec3D var17 = new Vec3D(var4, var6, var8);
      Vec3D var18 = new Vec3D(var4 + var10, var6 + var12, var8 + var14);
      MovingObjectPosition var19 = var16.a(var17, var18, false, true, false);
      if(var1) {
         if(var19 != null) {
            var18 = new Vec3D(var19.c.b, var19.c.c, var19.c.d);
         }

         Entity var20 = null;
         List var21 = var16.b(var0, var0.bk().a(var10, var12, var14).g(1.0D));
         double var22 = 0.0D;

         for(int var24 = 0; var24 < var21.size(); ++var24) {
            Entity var25 = (Entity)var21.get(var24);
            if(var25.ao() && (var2 || !var25.s(var3)) && !var25.noClip) {
               AxisAlignedBB var26 = var25.bk().g(0.30000001192092896D);
               MovingObjectPosition var27 = var26.a(var17, var18);
               if(var27 != null) {
                  double var28 = var17.g(var27.c);
                  if(var28 < var22 || var22 == 0.0D) {
                     var20 = var25;
                     var22 = var28;
                  }
               }
            }
         }

         if(var20 != null) {
            var19 = new MovingObjectPosition(var20);
         }
      }

      return var19;
   }

   public static final void a(Entity var0, float var1) {
      double var2 = var0.motX;
      double var4 = var0.motY;
      double var6 = var0.motZ;
      float var8 = MathHelper.a(var2 * var2 + var6 * var6);
      var0.yaw = (float)(MathHelper.b(var6, var2) * 57.2957763671875D) + 90.0F;

      for(var0.pitch = (float)(MathHelper.b((double)var8, var4) * 57.2957763671875D) - 90.0F; var0.pitch - var0.lastPitch < -180.0F; var0.lastPitch -= 360.0F) {
         ;
      }

      while(var0.pitch - var0.lastPitch >= 180.0F) {
         var0.lastPitch += 360.0F;
      }

      while(var0.yaw - var0.lastYaw < -180.0F) {
         var0.lastYaw -= 360.0F;
      }

      while(var0.yaw - var0.lastYaw >= 180.0F) {
         var0.lastYaw += 360.0F;
      }

      var0.pitch = var0.lastPitch + (var0.pitch - var0.lastPitch) * var1;
      var0.yaw = var0.lastYaw + (var0.yaw - var0.lastYaw) * var1;
   }
}
