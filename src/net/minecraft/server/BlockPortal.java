package net.minecraft.server;

import com.google.common.cache.LoadingCache;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityTypes;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.ShapeDetector;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aet;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ama;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_are;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_yq;

public class BlockPortal extends class_ama {
   public static final BlockStateEnum a = BlockStateEnum.a("axis", EnumDirection.class_a_in_class_cq.class, (Enum[])(new EnumDirection.class_a_in_class_cq[]{EnumDirection.class_a_in_class_cq.X, EnumDirection.class_a_in_class_cq.Z}));
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.0D, 0.0D, 0.375D, 1.0D, 1.0D, 0.625D);
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.375D, 0.0D, 0.0D, 0.625D, 1.0D, 1.0D);
   protected static final AxisAlignedBB d = new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 1.0D, 0.625D);

   public BlockPortal() {
      super(Material.E, false);
      this.w(this.A.b().set(a, EnumDirection.class_a_in_class_cq.X));
      this.a(true);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      switch(BlockPortal.SyntheticClass_1.a[((EnumDirection.class_a_in_class_cq)var1.get(a)).ordinal()]) {
      case 1:
         return b;
      case 2:
      default:
         return d;
      case 3:
         return c;
      }
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      super.b(var1, var2, var3, var4);
      if(var1.s.d() && var1.U().b("doMobSpawning") && var4.nextInt(2000) < var1.ae().a()) {
         int var5 = var2.q();

         BlockPosition var6;
         for(var6 = var2; !var1.getType(var6).q() && var6.q() > 0; var6 = var6.b()) {
            ;
         }

         if(var5 > 0 && !var1.getType(var6.a()).l()) {
            Entity var7 = class_aet.a(var1, EntityTypes.a(class_yq.class), (double)var6.p() + 0.5D, (double)var6.q() + 1.1D, (double)var6.r() + 0.5D);
            if(var7 != null) {
               var7.portalCooldown = var7.aB();
            }
         }
      }

   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public static int a(EnumDirection.class_a_in_class_cq var0) {
      return var0 == EnumDirection.class_a_in_class_cq.X?1:(var0 == EnumDirection.class_a_in_class_cq.Z?2:0);
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(World var1, BlockPosition var2) {
      BlockPortal.class_a_in_class_anj var3 = new BlockPortal.class_a_in_class_anj(var1, var2, EnumDirection.class_a_in_class_cq.X);
      if(var3.d() && var3.e == 0) {
         var3.e();
         return true;
      } else {
         BlockPortal.class_a_in_class_anj var4 = new BlockPortal.class_a_in_class_anj(var1, var2, EnumDirection.class_a_in_class_cq.Z);
         if(var4.d() && var4.e == 0) {
            var4.e();
            return true;
         } else {
            return false;
         }
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      EnumDirection.class_a_in_class_cq var5 = (EnumDirection.class_a_in_class_cq)var3.get(a);
      BlockPortal.class_a_in_class_anj var6;
      if(var5 == EnumDirection.class_a_in_class_cq.X) {
         var6 = new BlockPortal.class_a_in_class_anj(var1, var2, EnumDirection.class_a_in_class_cq.X);
         if(!var6.d() || var6.e < var6.h * var6.g) {
            var1.a(var2, Blocks.AIR.u());
         }
      } else if(var5 == EnumDirection.class_a_in_class_cq.Z) {
         var6 = new BlockPortal.class_a_in_class_anj(var1, var2, EnumDirection.class_a_in_class_cq.Z);
         if(!var6.d() || var6.e < var6.h * var6.g) {
            var1.a(var2, Blocks.AIR.u());
         }
      }

   }

   public int a(Random var1) {
      return 0;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Entity var4) {
      if(!var4.aH() && !var4.aI() && var4.aU()) {
         var4.e(var2);
      }

   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return null;
   }

   public IBlockData a(int var1) {
      return this.u().set(a, (var1 & 3) == 2?EnumDirection.class_a_in_class_cq.Z:EnumDirection.class_a_in_class_cq.X);
   }

   public int e(IBlockData var1) {
      return a((EnumDirection.class_a_in_class_cq)var1.get(a));
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      switch(BlockPortal.SyntheticClass_1.b[var2.ordinal()]) {
      case 1:
      case 2:
         switch(BlockPortal.SyntheticClass_1.a[((EnumDirection.class_a_in_class_cq)var1.get(a)).ordinal()]) {
         case 1:
            return var1.set(a, EnumDirection.class_a_in_class_cq.Z);
         case 3:
            return var1.set(a, EnumDirection.class_a_in_class_cq.X);
         default:
            return var1;
         }
      default:
         return var1;
      }
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   public ShapeDetector.ShapeDetectorCollection c(World var1, BlockPosition var2) {
      EnumDirection.class_a_in_class_cq var3 = EnumDirection.class_a_in_class_cq.Z;
      BlockPortal.class_a_in_class_anj var4 = new BlockPortal.class_a_in_class_anj(var1, var2, EnumDirection.class_a_in_class_cq.X);
      LoadingCache var5 = ShapeDetector.a(var1, true);
      if(!var4.d()) {
         var3 = EnumDirection.class_a_in_class_cq.X;
         var4 = new BlockPortal.class_a_in_class_anj(var1, var2, EnumDirection.class_a_in_class_cq.Z);
      }

      if(!var4.d()) {
         return new ShapeDetector.ShapeDetectorCollection(var2, EnumDirection.NORTH, EnumDirection.UP, var5, 1, 1, 1);
      } else {
         int[] var6 = new int[EnumDirection.EnumAxisDirection.values().length];
         EnumDirection var7 = var4.c.f();
         BlockPosition var8 = var4.f.b(var4.a() - 1);
         EnumDirection.EnumAxisDirection[] var9 = EnumDirection.EnumAxisDirection.values();
         int var10 = var9.length;

         int var11;
         for(var11 = 0; var11 < var10; ++var11) {
            EnumDirection.EnumAxisDirection var12 = var9[var11];
            ShapeDetector.ShapeDetectorCollection var13 = new ShapeDetector.ShapeDetectorCollection(var7.c() == var12?var8:var8.a(var4.c, var4.b() - 1), EnumDirection.a(var12, var3), EnumDirection.UP, var5, var4.b(), var4.a(), 1);

            for(int var14 = 0; var14 < var4.b(); ++var14) {
               for(int var15 = 0; var15 < var4.a(); ++var15) {
                  class_are var16 = var13.a(var14, var15, 1);
                  if(var16.a() != null && var16.a().getMaterial() != Material.a) {
                     ++var6[var12.ordinal()];
                  }
               }
            }
         }

         EnumDirection.EnumAxisDirection var17 = EnumDirection.EnumAxisDirection.POSITIVE;
         EnumDirection.EnumAxisDirection[] var18 = EnumDirection.EnumAxisDirection.values();
         var11 = var18.length;

         for(int var19 = 0; var19 < var11; ++var19) {
            EnumDirection.EnumAxisDirection var20 = var18[var19];
            if(var6[var20.ordinal()] < var6[var17.ordinal()]) {
               var17 = var20;
            }
         }

         return new ShapeDetector.ShapeDetectorCollection(var7.c() == var17?var8:var8.a(var4.c, var4.b() - 1), EnumDirection.a(var17, var3), EnumDirection.UP, var5, var4.b(), var4.a(), 1);
      }
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b = new int[class_aod.values().length];

      static {
         try {
            b[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 1;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            b[class_aod.CLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var4) {
            ;
         }

         a = new int[EnumDirection.class_a_in_class_cq.values().length];

         try {
            a[EnumDirection.class_a_in_class_cq.X.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.class_a_in_class_cq.Y.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.class_a_in_class_cq.Z.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static class class_a_in_class_anj {
      private final World a;
      private final EnumDirection.class_a_in_class_cq b;
      private final EnumDirection c;
      private final EnumDirection d;
      private int e = 0;
      private BlockPosition f;
      private int g;
      private int h;

      public class_a_in_class_anj(World var1, BlockPosition var2, EnumDirection.class_a_in_class_cq var3) {
         this.a = var1;
         this.b = var3;
         if(var3 == EnumDirection.class_a_in_class_cq.X) {
            this.d = EnumDirection.EAST;
            this.c = EnumDirection.WEST;
         } else {
            this.d = EnumDirection.NORTH;
            this.c = EnumDirection.SOUTH;
         }

         for(BlockPosition var4 = var2; var2.q() > var4.q() - 21 && var2.q() > 0 && this.a(var1.getType(var2.b()).getBlock()); var2 = var2.b()) {
            ;
         }

         int var5 = this.a(var2, this.d) - 1;
         if(var5 >= 0) {
            this.f = var2.a(this.d, var5);
            this.h = this.a(this.f, this.c);
            if(this.h < 2 || this.h > 21) {
               this.f = null;
               this.h = 0;
            }
         }

         if(this.f != null) {
            this.g = this.c();
         }

      }

      protected int a(BlockPosition var1, EnumDirection var2) {
         int var3;
         for(var3 = 0; var3 < 22; ++var3) {
            BlockPosition var4 = var1.a(var2, var3);
            if(!this.a(this.a.getType(var4).getBlock()) || this.a.getType(var4.b()).getBlock() != Blocks.Z) {
               break;
            }
         }

         Block var5 = this.a.getType(var1.a(var2, var3)).getBlock();
         return var5 == Blocks.Z?var3:0;
      }

      public int a() {
         return this.g;
      }

      public int b() {
         return this.h;
      }

      protected int c() {
         int var1;
         label56:
         for(this.g = 0; this.g < 21; ++this.g) {
            for(var1 = 0; var1 < this.h; ++var1) {
               BlockPosition var2 = this.f.a(this.c, var1).b(this.g);
               Block var3 = this.a.getType(var2).getBlock();
               if(!this.a(var3)) {
                  break label56;
               }

               if(var3 == Blocks.aY) {
                  ++this.e;
               }

               if(var1 == 0) {
                  var3 = this.a.getType(var2.a(this.d)).getBlock();
                  if(var3 != Blocks.Z) {
                     break label56;
                  }
               } else if(var1 == this.h - 1) {
                  var3 = this.a.getType(var2.a(this.c)).getBlock();
                  if(var3 != Blocks.Z) {
                     break label56;
                  }
               }
            }
         }

         for(var1 = 0; var1 < this.h; ++var1) {
            if(this.a.getType(this.f.a(this.c, var1).b(this.g)).getBlock() != Blocks.Z) {
               this.g = 0;
               break;
            }
         }

         if(this.g <= 21 && this.g >= 3) {
            return this.g;
         } else {
            this.f = null;
            this.h = 0;
            this.g = 0;
            return 0;
         }
      }

      protected boolean a(Block var1) {
         return var1.x == Material.a || var1 == Blocks.ab || var1 == Blocks.aY;
      }

      public boolean d() {
         return this.f != null && this.h >= 2 && this.h <= 21 && this.g >= 3 && this.g <= 21;
      }

      public void e() {
         for(int var1 = 0; var1 < this.h; ++var1) {
            BlockPosition var2 = this.f.a(this.c, var1);

            for(int var3 = 0; var3 < this.g; ++var3) {
               this.a.a((BlockPosition)var2.b(var3), (IBlockData)Blocks.aY.u().set(BlockPortal.a, this.b), 2);
            }
         }

      }
   }
}
