package net.minecraft.server;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Iterator;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockCobbleWall;
import net.minecraft.server.BlockDoor;
import net.minecraft.server.BlockFenceGate;
import net.minecraft.server.BlockMinecartTrackAbstract;
import net.minecraft.server.Blocks;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ali;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.class_ayl;
import net.minecraft.server.class_aym;
import net.minecraft.server.class_ayn;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;

public class class_ayr extends class_ayn {
   private float j;

   public void a(class_ahw var1, EntityInsentient var2) {
      super.a(var1, var2);
      this.j = var2.a(class_ayl.WATER);
   }

   public void a() {
      super.a();
      this.b.a(class_ayl.WATER, this.j);
   }

   public class_aym b() {
      int var1;
      BlockPosition var2;
      if(this.e() && this.b.ah()) {
         var1 = (int)this.b.bk().b;
         BlockPosition.class_a_in_class_cj var8 = new BlockPosition.class_a_in_class_cj(MathHelper.c(this.b.locX), var1, MathHelper.c(this.b.locZ));

         for(Block var3 = this.a.getType(var8).getBlock(); var3 == Blocks.i || var3 == Blocks.j; var3 = this.a.getType(var8).getBlock()) {
            ++var1;
            var8.c(MathHelper.c(this.b.locX), var1, MathHelper.c(this.b.locZ));
         }
      } else if(!this.b.onGround) {
         for(var2 = new BlockPosition(this.b); (this.a.getType(var2).getMaterial() == Material.a || this.a.getType(var2).getBlock().b(this.a, var2)) && var2.q() > 0; var2 = var2.b()) {
            ;
         }

         var1 = var2.a().q();
      } else {
         var1 = MathHelper.c(this.b.bk().b + 0.5D);
      }

      var2 = new BlockPosition(this.b);
      class_ayl var9 = this.a(this.b, var2.p(), var1, var2.r());
      if(this.b.a(var9) < 0.0F) {
         HashSet var4 = new HashSet();
         var4.add(new BlockPosition(this.b.bk().a, (double)var1, this.b.bk().c));
         var4.add(new BlockPosition(this.b.bk().a, (double)var1, this.b.bk().f));
         var4.add(new BlockPosition(this.b.bk().d, (double)var1, this.b.bk().c));
         var4.add(new BlockPosition(this.b.bk().d, (double)var1, this.b.bk().f));
         Iterator var5 = var4.iterator();

         while(var5.hasNext()) {
            BlockPosition var6 = (BlockPosition)var5.next();
            class_ayl var7 = this.a(this.b, var6);
            if(this.b.a(var7) >= 0.0F) {
               return this.a(var6.p(), var6.q(), var6.r());
            }
         }
      }

      return this.a(var2.p(), var1, var2.r());
   }

   public class_aym a(double var1, double var3, double var5) {
      return this.a(MathHelper.c(var1 - (double)(this.b.width / 2.0F)), MathHelper.c(var3), MathHelper.c(var5 - (double)(this.b.width / 2.0F)));
   }

   public int a(class_aym[] var1, class_aym var2, class_aym var3, float var4) {
      int var5 = 0;
      byte var6 = 0;
      class_ayl var7 = this.a(this.b, var2.a, var2.b + 1, var2.c);
      if(this.b.a(var7) >= 0.0F) {
         var6 = 1;
      }

      BlockPosition var8 = (new BlockPosition(var2.a, var2.b, var2.c)).b();
      double var9 = (double)var2.b - (1.0D - this.a.getType(var8).c(this.a, var8).e);
      class_aym var11 = this.a(var2.a, var2.b, var2.c + 1, var6, var9, EnumDirection.SOUTH);
      class_aym var12 = this.a(var2.a - 1, var2.b, var2.c, var6, var9, EnumDirection.WEST);
      class_aym var13 = this.a(var2.a + 1, var2.b, var2.c, var6, var9, EnumDirection.EAST);
      class_aym var14 = this.a(var2.a, var2.b, var2.c - 1, var6, var9, EnumDirection.NORTH);
      if(var11 != null && !var11.i && var11.a(var3) < var4) {
         var1[var5++] = var11;
      }

      if(var12 != null && !var12.i && var12.a(var3) < var4) {
         var1[var5++] = var12;
      }

      if(var13 != null && !var13.i && var13.a(var3) < var4) {
         var1[var5++] = var13;
      }

      if(var14 != null && !var14.i && var14.a(var3) < var4) {
         var1[var5++] = var14;
      }

      boolean var15 = var14 == null || var14.m == class_ayl.OPEN || var14.l != 0.0F;
      boolean var16 = var11 == null || var11.m == class_ayl.OPEN || var11.l != 0.0F;
      boolean var17 = var13 == null || var13.m == class_ayl.OPEN || var13.l != 0.0F;
      boolean var18 = var12 == null || var12.m == class_ayl.OPEN || var12.l != 0.0F;
      class_aym var19;
      if(var15 && var18) {
         var19 = this.a(var2.a - 1, var2.b, var2.c - 1, var6, var9, EnumDirection.NORTH);
         if(var19 != null && !var19.i && var19.a(var3) < var4) {
            var1[var5++] = var19;
         }
      }

      if(var15 && var17) {
         var19 = this.a(var2.a + 1, var2.b, var2.c - 1, var6, var9, EnumDirection.NORTH);
         if(var19 != null && !var19.i && var19.a(var3) < var4) {
            var1[var5++] = var19;
         }
      }

      if(var16 && var18) {
         var19 = this.a(var2.a - 1, var2.b, var2.c + 1, var6, var9, EnumDirection.SOUTH);
         if(var19 != null && !var19.i && var19.a(var3) < var4) {
            var1[var5++] = var19;
         }
      }

      if(var16 && var17) {
         var19 = this.a(var2.a + 1, var2.b, var2.c + 1, var6, var9, EnumDirection.SOUTH);
         if(var19 != null && !var19.i && var19.a(var3) < var4) {
            var1[var5++] = var19;
         }
      }

      return var5;
   }

   private class_aym a(int var1, int var2, int var3, int var4, double var5, EnumDirection var7) {
      class_aym var8 = null;
      BlockPosition var9 = new BlockPosition(var1, var2, var3);
      BlockPosition var10 = var9.b();
      double var11 = (double)var2 - (1.0D - this.a.getType(var10).c(this.a, var10).e);
      if(var11 - var5 > 1.0D) {
         return null;
      } else {
         class_ayl var13 = this.a(this.b, var1, var2, var3);
         float var14 = this.b.a(var13);
         double var15 = (double)this.b.width / 2.0D;
         if(var14 >= 0.0F) {
            var8 = this.a(var1, var2, var3);
            var8.m = var13;
            var8.l = Math.max(var8.l, var14);
         }

         if(var13 == class_ayl.WALKABLE) {
            return var8;
         } else {
            if(var8 == null && var4 > 0 && var13 != class_ayl.FENCE && var13 != class_ayl.TRAPDOOR) {
               var8 = this.a(var1, var2 + 1, var3, var4 - 1, var5, var7);
               if(var8 != null && (var8.m == class_ayl.OPEN || var8.m == class_ayl.WALKABLE)) {
                  double var17 = (double)(var1 - var7.g()) + 0.5D;
                  double var19 = (double)(var3 - var7.i()) + 0.5D;
                  AxisAlignedBB var21 = new AxisAlignedBB(var17 - var15, (double)var2 + 0.001D, var19 - var15, var17 + var15, (double)((float)var2 + this.b.length), var19 + var15);
                  AxisAlignedBB var22 = this.a.getType(var9).c(this.a, var9);
                  AxisAlignedBB var23 = var21.a(0.0D, var22.e - 0.002D, 0.0D);
                  if(this.b.world.b(var23)) {
                     var8 = null;
                  }
               }
            }

            if(var13 == class_ayl.OPEN) {
               AxisAlignedBB var24 = new AxisAlignedBB((double)var1 - var15 + 0.5D, (double)var2 + 0.001D, (double)var3 - var15 + 0.5D, (double)var1 + var15 + 0.5D, (double)((float)var2 + this.b.length), (double)var3 + var15 + 0.5D);
               if(this.b.world.b(var24)) {
                  return null;
               }

               int var18 = 0;

               while(var2 > 0 && var13 == class_ayl.OPEN) {
                  --var2;
                  if(var18++ >= this.b.aV()) {
                     return null;
                  }

                  var13 = this.a(this.b, var1, var2, var3);
                  var14 = this.b.a(var13);
                  if(var13 != class_ayl.OPEN && var14 >= 0.0F) {
                     var8 = this.a(var1, var2, var3);
                     var8.m = var13;
                     var8.l = Math.max(var8.l, var14);
                     break;
                  }

                  if(var14 < 0.0F) {
                     return null;
                  }
               }
            }

            return var8;
         }
      }
   }

   public class_ayl a(class_ahw var1, int var2, int var3, int var4, EntityInsentient var5, int var6, int var7, int var8, boolean var9, boolean var10) {
      EnumSet var11 = EnumSet.noneOf(class_ayl.class);
      class_ayl var12 = class_ayl.BLOCKED;
      double var13 = (double)var5.width / 2.0D;
      BlockPosition var15 = new BlockPosition(var5);

      for(int var16 = var2; var16 < var2 + var6; ++var16) {
         for(int var17 = var3; var17 < var3 + var7; ++var17) {
            for(int var18 = var4; var18 < var4 + var8; ++var18) {
               class_ayl var19 = a(var1, var16, var17, var18);
               if(var19 == class_ayl.DOOR_WOOD_CLOSED && var9 && var10) {
                  var19 = class_ayl.WALKABLE;
               }

               if(var19 == class_ayl.DOOR_OPEN && !var10) {
                  var19 = class_ayl.BLOCKED;
               }

               if(var19 == class_ayl.RAIL && !(var1.getType(var15).getBlock() instanceof BlockMinecartTrackAbstract) && !(var1.getType(var15.b()).getBlock() instanceof BlockMinecartTrackAbstract)) {
                  var19 = class_ayl.FENCE;
               }

               if(var16 == var2 && var17 == var3 && var18 == var4) {
                  var12 = var19;
               }

               if(var17 > var3 && var19 != class_ayl.OPEN) {
                  AxisAlignedBB var20 = new AxisAlignedBB((double)var16 - var13 + 0.5D, (double)var3 + 0.001D, (double)var18 - var13 + 0.5D, (double)var16 + var13 + 0.5D, (double)((float)var3 + var5.length), (double)var18 + var13 + 0.5D);
                  if(!var5.world.b(var20)) {
                     var19 = class_ayl.OPEN;
                  }
               }

               var11.add(var19);
            }
         }
      }

      if(var11.contains(class_ayl.FENCE)) {
         return class_ayl.FENCE;
      } else {
         class_ayl var21 = class_ayl.BLOCKED;
         Iterator var22 = var11.iterator();

         while(var22.hasNext()) {
            class_ayl var23 = (class_ayl)var22.next();
            if(var5.a(var23) < 0.0F) {
               return var23;
            }

            if(var5.a(var23) >= var5.a(var21)) {
               var21 = var23;
            }
         }

         if(var12 == class_ayl.OPEN && var5.a(var21) == 0.0F) {
            return class_ayl.OPEN;
         } else {
            return var21;
         }
      }
   }

   private class_ayl a(EntityInsentient var1, BlockPosition var2) {
      return this.a(this.a, var2.p(), var2.q(), var2.r(), var1, this.d, this.e, this.f, this.d(), this.c());
   }

   private class_ayl a(EntityInsentient var1, int var2, int var3, int var4) {
      return this.a(this.a, var2, var3, var4, var1, this.d, this.e, this.f, this.d(), this.c());
   }

   public static class_ayl a(class_ahw var0, int var1, int var2, int var3) {
      BlockPosition var4 = new BlockPosition(var1, var2, var3);
      IBlockData var5 = var0.getType(var4);
      Block var6 = var5.getBlock();
      Material var7 = var5.getMaterial();
      class_ayl var8 = class_ayl.BLOCKED;
      if(var6 != Blocks.bd && var6 != Blocks.cw && var6 != Blocks.bx) {
         if(var6 == Blocks.ab) {
            return class_ayl.DAMAGE_FIRE;
         } else if(var6 == Blocks.aK) {
            return class_ayl.DAMAGE_CACTUS;
         } else if(var6 instanceof BlockDoor && var7 == Material.d && !((Boolean)var5.get(BlockDoor.b)).booleanValue()) {
            return class_ayl.DOOR_WOOD_CLOSED;
         } else if(var6 instanceof BlockDoor && var7 == Material.f && !((Boolean)var5.get(BlockDoor.b)).booleanValue()) {
            return class_ayl.DOOR_IRON_CLOSED;
         } else if(var6 instanceof BlockDoor && ((Boolean)var5.get(BlockDoor.b)).booleanValue()) {
            return class_ayl.DOOR_OPEN;
         } else if(var6 instanceof BlockMinecartTrackAbstract) {
            return class_ayl.RAIL;
         } else if(!(var6 instanceof class_ali) && !(var6 instanceof BlockCobbleWall) && (!(var6 instanceof BlockFenceGate) || ((Boolean)var5.get(BlockFenceGate.a)).booleanValue())) {
            if(var7 == Material.a) {
               var8 = class_ayl.OPEN;
            } else {
               if(var7 == Material.h) {
                  return class_ayl.WATER;
               }

               if(var7 == Material.i) {
                  return class_ayl.LAVA;
               }
            }

            if(var6.b(var0, var4) && var8 == class_ayl.BLOCKED) {
               var8 = class_ayl.OPEN;
            }

            if(var8 == class_ayl.OPEN && var2 >= 1) {
               class_ayl var9 = a(var0, var1, var2 - 1, var3);
               var8 = var9 != class_ayl.WALKABLE && var9 != class_ayl.OPEN && var9 != class_ayl.WATER && var9 != class_ayl.LAVA?class_ayl.WALKABLE:class_ayl.OPEN;
            }

            if(var8 == class_ayl.WALKABLE) {
               for(int var12 = var1 - 1; var12 <= var1 + 1; ++var12) {
                  for(int var10 = var3 - 1; var10 <= var3 + 1; ++var10) {
                     if(var12 != var1 || var10 != var3) {
                        Block var11 = var0.getType(new BlockPosition(var12, var2, var10)).getBlock();
                        if(var11 == Blocks.aK) {
                           var8 = class_ayl.DANGER_CACTUS;
                        } else if(var11 == Blocks.ab) {
                           var8 = class_ayl.DANGER_FIRE;
                        }
                     }
                  }
               }
            }

            return var8;
         } else {
            return class_ayl.FENCE;
         }
      } else {
         return class_ayl.TRAPDOOR;
      }
   }
}
