package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Items;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.StructureBoundingBox;
import net.minecraft.server.StructurePiece;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.WorldGenFactory;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_apw;
import net.minecraft.server.class_awh;
import net.minecraft.server.class_awl;
import net.minecraft.server.class_awm;
import net.minecraft.server.class_awn;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.BaseBlockPosition;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_ou;
import net.minecraft.server.class_xr;
import net.minecraft.server.class_yt;

public class class_avq {
   public static final class_awl a = new class_awl();
   private static final class_awm b = (new class_awm()).a(true);
   private static final class_awm c = (new class_awm()).a(true).a(Blocks.AIR);
   private static final class_avq.class_b_in_class_avq d = new class_avq.class_b_in_class_avq() {
      public void a() {
      }

      public boolean a(int var1, class_avq.class_a_in_class_avq var2, BlockPosition var3, List var4, Random var5) {
         if(var1 > 8) {
            return false;
         } else {
            class_aod var6 = var2.b.c();
            class_avq.class_a_in_class_avq var7;
            var4.add(var7 = class_avq.b(var2, var3, "base_floor", var6, true));
            int var8 = var5.nextInt(3);
            if(var8 == 0) {
               var4.add(class_avq.b(var7, new BlockPosition(-1, 4, -1), "base_roof", var6, true));
            } else if(var8 == 1) {
               var4.add(var7 = class_avq.b(var7, new BlockPosition(-1, 0, -1), "second_floor_2", var6, false));
               var4.add(var7 = class_avq.b(var7, new BlockPosition(-1, 8, -1), "second_roof", var6, false));
               class_avq.b(class_avq.f, var1 + 1, var7, (BlockPosition)null, var4, var5);
            } else if(var8 == 2) {
               var4.add(var7 = class_avq.b(var7, new BlockPosition(-1, 0, -1), "second_floor_2", var6, false));
               var4.add(var7 = class_avq.b(var7, new BlockPosition(-1, 4, -1), "third_floor_c", var6, false));
               var4.add(var7 = class_avq.b(var7, new BlockPosition(-1, 8, -1), "third_roof", var6, true));
               class_avq.b(class_avq.f, var1 + 1, var7, (BlockPosition)null, var4, var5);
            }

            return true;
         }
      }
   };
   private static final List e = Lists.newArrayList((Object[])(new class_ou[]{new class_ou(class_aod.NONE, new BlockPosition(1, -1, 0)), new class_ou(class_aod.CLOCKWISE_90, new BlockPosition(6, -1, 1)), new class_ou(class_aod.COUNTERCLOCKWISE_90, new BlockPosition(0, -1, 5)), new class_ou(class_aod.CLOCKWISE_180, new BlockPosition(5, -1, 6))}));
   private static final class_avq.class_b_in_class_avq f = new class_avq.class_b_in_class_avq() {
      public void a() {
      }

      public boolean a(int var1, class_avq.class_a_in_class_avq var2, BlockPosition var3, List var4, Random var5) {
         class_aod var6 = var2.b.c();
         class_avq.class_a_in_class_avq var7;
         var4.add(var7 = class_avq.b(var2, new BlockPosition(3 + var5.nextInt(2), -3, 3 + var5.nextInt(2)), "tower_base", var6, true));
         var4.add(var7 = class_avq.b(var7, new BlockPosition(0, 7, 0), "tower_piece", var6, true));
         class_avq.class_a_in_class_avq var8 = var5.nextInt(3) == 0?var7:null;
         int var9 = 1 + var5.nextInt(3);

         for(int var10 = 0; var10 < var9; ++var10) {
            var4.add(var7 = class_avq.b(var7, new BlockPosition(0, 4, 0), "tower_piece", var6, true));
            if(var10 < var9 - 1 && var5.nextBoolean()) {
               var8 = var7;
            }
         }

         if(var8 != null) {
            Iterator var13 = class_avq.e.iterator();

            while(var13.hasNext()) {
               class_ou var11 = (class_ou)var13.next();
               if(var5.nextBoolean()) {
                  class_avq.class_a_in_class_avq var12;
                  var4.add(var12 = class_avq.b(var8, (BlockPosition)var11.b(), "bridge_end", var6.a((class_aod)var11.a()), true));
                  class_avq.b(class_avq.g, var1 + 1, var12, (BlockPosition)null, var4, var5);
               }
            }

            var4.add(class_avq.b(var7, new BlockPosition(-1, 4, -1), "tower_top", var6, true));
         } else {
            if(var1 != 7) {
               return class_avq.b(class_avq.i, var1 + 1, var7, (BlockPosition)null, var4, var5);
            }

            var4.add(class_avq.b(var7, new BlockPosition(-1, 4, -1), "tower_top", var6, true));
         }

         return true;
      }
   };
   private static final class_avq.class_b_in_class_avq g = new class_avq.class_b_in_class_avq() {
      public boolean a;

      public void a() {
         this.a = false;
      }

      public boolean a(int var1, class_avq.class_a_in_class_avq var2, BlockPosition var3, List var4, Random var5) {
         class_aod var6 = var2.b.c();
         int var7 = var5.nextInt(4) + 1;
         class_avq.class_a_in_class_avq var8;
         var4.add(var8 = class_avq.b(var2, new BlockPosition(0, 0, -4), "bridge_piece", var6, true));
         var8.m = -1;
         byte var9 = 0;

         for(int var10 = 0; var10 < var7; ++var10) {
            if(var5.nextBoolean()) {
               var4.add(var8 = class_avq.b(var8, new BlockPosition(0, var9, -4), "bridge_piece", var6, true));
               var9 = 0;
            } else {
               if(var5.nextBoolean()) {
                  var4.add(var8 = class_avq.b(var8, new BlockPosition(0, var9, -4), "bridge_steep_stairs", var6, true));
               } else {
                  var4.add(var8 = class_avq.b(var8, new BlockPosition(0, var9, -8), "bridge_gentle_stairs", var6, true));
               }

               var9 = 4;
            }
         }

         if(!this.a && var5.nextInt(10 - var1) == 0) {
            var4.add(class_avq.b(var8, new BlockPosition(-8 + var5.nextInt(8), var9, -70 + var5.nextInt(10)), "ship", var6, true));
            this.a = true;
         } else if(!class_avq.b(class_avq.d, var1 + 1, var8, new BlockPosition(-3, var9 + 1, -11), var4, var5)) {
            return false;
         }

         var4.add(var8 = class_avq.b(var8, new BlockPosition(4, var9, 0), "bridge_end", var6.a(class_aod.CLOCKWISE_180), true));
         var8.m = -1;
         return true;
      }
   };
   private static final List h = Lists.newArrayList((Object[])(new class_ou[]{new class_ou(class_aod.NONE, new BlockPosition(4, -1, 0)), new class_ou(class_aod.CLOCKWISE_90, new BlockPosition(12, -1, 4)), new class_ou(class_aod.COUNTERCLOCKWISE_90, new BlockPosition(0, -1, 8)), new class_ou(class_aod.CLOCKWISE_180, new BlockPosition(8, -1, 12))}));
   private static final class_avq.class_b_in_class_avq i = new class_avq.class_b_in_class_avq() {
      public void a() {
      }

      public boolean a(int var1, class_avq.class_a_in_class_avq var2, BlockPosition var3, List var4, Random var5) {
         class_aod var7 = var2.b.c();
         class_avq.class_a_in_class_avq var6;
         var4.add(var6 = class_avq.b(var2, new BlockPosition(-3, 4, -3), "fat_tower_base", var7, true));
         var4.add(var6 = class_avq.b(var6, new BlockPosition(0, 4, 0), "fat_tower_middle", var7, true));

         for(int var8 = 0; var8 < 2 && var5.nextInt(3) != 0; ++var8) {
            var4.add(var6 = class_avq.b(var6, new BlockPosition(0, 8, 0), "fat_tower_middle", var7, true));
            Iterator var9 = class_avq.h.iterator();

            while(var9.hasNext()) {
               class_ou var10 = (class_ou)var9.next();
               if(var5.nextBoolean()) {
                  class_avq.class_a_in_class_avq var11;
                  var4.add(var11 = class_avq.b(var6, (BlockPosition)var10.b(), "bridge_end", var7.a((class_aod)var10.a()), true));
                  class_avq.b(class_avq.g, var1 + 1, var11, (BlockPosition)null, var4, var5);
               }
            }
         }

         var4.add(class_avq.b(var6, new BlockPosition(-2, 8, -2), "fat_tower_top", var7, true));
         return true;
      }
   };

   public static void a() {
      WorldGenFactory.a(class_avq.class_a_in_class_avq.class, "ECP");
   }

   private static class_avq.class_a_in_class_avq b(class_avq.class_a_in_class_avq var0, BlockPosition var1, String var2, class_aod var3, boolean var4) {
      class_avq.class_a_in_class_avq var5 = new class_avq.class_a_in_class_avq(var2, var0.c, var3, var4);
      BlockPosition var6 = var0.a.a(var0.b, var1, var5.b, BlockPosition.a);
      var5.a(var6.p(), var6.q(), var6.r());
      return var5;
   }

   public static void a(BlockPosition var0, class_aod var1, List var2, Random var3) {
      i.a();
      d.a();
      g.a();
      f.a();
      class_avq.class_a_in_class_avq var4 = new class_avq.class_a_in_class_avq("base_floor", var0, var1, true);
      var2.add(var4);
      class_avq.class_a_in_class_avq var5;
      var2.add(var5 = b(var4, new BlockPosition(-1, 0, -1), "second_floor", var1, false));
      var2.add(var5 = b(var5, new BlockPosition(-1, 4, -1), "third_floor", var1, false));
      var2.add(var5 = b(var5, new BlockPosition(-1, 8, -1), "third_roof", var1, true));
      b(f, 1, var5, (BlockPosition)null, var2, var3);
   }

   private static boolean b(class_avq.class_b_in_class_avq var0, int var1, class_avq.class_a_in_class_avq var2, BlockPosition var3, List var4, Random var5) {
      if(var1 > 8) {
         return false;
      } else {
         ArrayList var6 = Lists.newArrayList();
         if(var0.a(var1, var2, var3, var6, var5)) {
            boolean var7 = false;
            int var8 = var5.nextInt();
            Iterator var9 = var6.iterator();

            while(var9.hasNext()) {
               StructurePiece var10 = (StructurePiece)var9.next();
               var10.m = var8;
               StructurePiece var11 = StructurePiece.a(var4, var10.c());
               if(var11 != null && var11.m != var2.m) {
                  var7 = true;
                  break;
               }
            }

            if(!var7) {
               var4.addAll(var6);
               return true;
            }
         }

         return false;
      }
   }

   interface class_b_in_class_avq {
      void a();

      boolean a(int var1, class_avq.class_a_in_class_avq var2, BlockPosition var3, List var4, Random var5);
   }

   public static class class_a_in_class_avq extends class_awh {
      private String d;
      private class_aod e;
      private boolean f;

      public class_a_in_class_avq() {
      }

      public class_a_in_class_avq(String var1, BlockPosition var2, class_aod var3, boolean var4) {
         super(0);
         this.d = var1;
         this.e = var3;
         this.f = var4;
         this.a(var2);
      }

      private void a(BlockPosition var1) {
         class_awn var2 = class_avq.a.a((MinecraftServer)null, (class_kk)(new class_kk("endcity/" + this.d)));
         class_awm var3;
         if(this.f) {
            var3 = class_avq.b.a().a(this.e);
         } else {
            var3 = class_avq.c.a().a(this.e);
         }

         this.a(var2, var1, var3);
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("Template", this.d);
         var1.a("Rot", this.e.name());
         var1.a("OW", this.f);
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.d = var1.l("Template");
         this.e = class_aod.valueOf(var1.l("Rot"));
         this.f = var1.p("OW");
         this.a(this.c);
      }

      protected void a(String var1, BlockPosition var2, World var3, Random var4, StructureBoundingBox var5) {
         if(var1.startsWith("Chest")) {
            BlockPosition var6 = var2.b();
            if(var5.b((BaseBlockPosition)var6)) {
               TileEntity var7 = var3.r(var6);
               if(var7 instanceof class_apw) {
                  ((class_apw)var7).a(class_azs.c, var4.nextLong());
               }
            }
         } else if(var1.startsWith("Sentry")) {
            class_yt var8 = new class_yt(var3);
            var8.b((double)var2.p() + 0.5D, (double)var2.q() + 0.5D, (double)var2.r() + 0.5D);
            var8.g(var2);
            var3.a((Entity)var8);
         } else if(var1.startsWith("Elytra")) {
            class_xr var9 = new class_xr(var3, var2, this.e.a(EnumDirection.SOUTH));
            var9.a(new ItemStack(Items.cR));
            var3.a((Entity)var9);
         }

      }
   }
}
