package net.minecraft.server;

import net.minecraft.server.ChatMessage;
import net.minecraft.server.Entity;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_di;
import net.minecraft.server.class_rc;
import net.minecraft.server.class_rz;

public class class_rd extends class_rc {
   private final Entity t;

   public class_rd(String var1, Entity var2, Entity var3) {
      super(var1, var2);
      this.t = var3;
   }

   public Entity i() {
      return this.s;
   }

   public Entity j() {
      return this.t;
   }

   public IChatBaseComponent c(class_rz var1) {
      IChatBaseComponent var2 = this.t == null?this.s.i_():this.t.i_();
      ItemStack var3 = this.t instanceof class_rz?((class_rz)this.t).ca():null;
      String var4 = "death.attack." + this.r;
      String var5 = var4 + ".item";
      return var3 != null && var3.s() && class_di.c(var5)?new ChatMessage(var5, new Object[]{var1.i_(), var2, var3.B()}):new ChatMessage(var4, new Object[]{var1.i_(), var2});
   }
}
