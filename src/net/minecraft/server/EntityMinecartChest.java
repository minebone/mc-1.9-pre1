package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Container;
import net.minecraft.server.DamageSource;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityMinecartAbstract;
import net.minecraft.server.EntityMinecartContainer;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.World;
import net.minecraft.server.class_aba;
import net.minecraft.server.BlockChest;
import net.minecraft.server.IBlockData;

public class EntityMinecartChest extends EntityMinecartContainer {
   public EntityMinecartChest(World var1) {
      super(var1);
   }

   public EntityMinecartChest(World var1, double var2, double var4, double var6) {
      super(var1, var2, var4, var6);
   }

   public void a(DamageSource var1) {
      super.a(var1);
      if(this.world.U().b("doEntityDrops")) {
         this.a(Item.a((Block)Blocks.ae), 1, 0.0F);
      }

   }

   public int u_() {
      return 27;
   }

   public EntityMinecartAbstract.EnumMinecartType v() {
      return EntityMinecartAbstract.EnumMinecartType.CHEST;
   }

   public IBlockData x() {
      return Blocks.ae.u().set(BlockChest.a, EnumDirection.NORTH);
   }

   public int A() {
      return 8;
   }

   public String k() {
      return "minecraft:chest";
   }

   public Container a(PlayerInventory var1, EntityHuman var2) {
      this.f(var2);
      return new class_aba(var1, this, var2);
   }
}
