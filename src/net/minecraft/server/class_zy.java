package net.minecraft.server;

import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityProjectile;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.World;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_vw;

public class class_zy extends EntityProjectile {
   public class_zy(World var1) {
      super(var1);
   }

   public class_zy(World var1, class_rz var2) {
      super(var1, var2);
   }

   public class_zy(World var1, double var2, double var4, double var6) {
      super(var1, var2, var4, var6);
   }

   protected void a(MovingObjectPosition var1) {
      if(var1.d != null) {
         var1.d.a(DamageSource.a((Entity)this, (Entity)this.k()), 0.0F);
      }

      if(!this.world.E && this.random.nextInt(8) == 0) {
         byte var2 = 1;
         if(this.random.nextInt(32) == 0) {
            var2 = 4;
         }

         for(int var3 = 0; var3 < var2; ++var3) {
            class_vw var4 = new class_vw(this.world);
            var4.b_(-24000);
            var4.b(this.locX, this.locY, this.locZ, this.yaw, 0.0F);
            this.world.a((Entity)var4);
         }
      }

      double var5 = 0.08D;

      for(int var6 = 0; var6 < 8; ++var6) {
         this.world.a(EnumParticle.ITEM_CRACK, this.locX, this.locY, this.locZ, ((double)this.random.nextFloat() - 0.5D) * 0.08D, ((double)this.random.nextFloat() - 0.5D) * 0.08D, ((double)this.random.nextFloat() - 0.5D) * 0.08D, new int[]{Item.a(Items.aW)});
      }

      if(!this.world.E) {
         this.S();
      }

   }
}
