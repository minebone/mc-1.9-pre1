package net.minecraft.server;

import java.util.Collection;
import java.util.Iterator;
import java.util.UUID;
import net.minecraft.server.AttributeMapBase;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.AttributeRanged;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_ys {
   private static final Logger i = LogManager.getLogger();
   public static final class_sk a = (new AttributeRanged((class_sk)null, "generic.maxHealth", 20.0D, 0.0D, 1024.0D)).a("Max Health").a(true);
   public static final class_sk b = (new AttributeRanged((class_sk)null, "generic.followRange", 32.0D, 0.0D, 2048.0D)).a("Follow Range");
   public static final class_sk c = (new AttributeRanged((class_sk)null, "generic.knockbackResistance", 0.0D, 0.0D, 1.0D)).a("Knockback Resistance");
   public static final class_sk d = (new AttributeRanged((class_sk)null, "generic.movementSpeed", 0.699999988079071D, 0.0D, 1024.0D)).a("Movement Speed").a(true);
   public static final class_sk e = new AttributeRanged((class_sk)null, "generic.attackDamage", 2.0D, 0.0D, 2048.0D);
   public static final class_sk f = (new AttributeRanged((class_sk)null, "generic.attackSpeed", 4.0D, 0.0D, 1024.0D)).a(true);
   public static final class_sk g = (new AttributeRanged((class_sk)null, "generic.armor", 0.0D, 0.0D, 30.0D)).a(true);
   public static final class_sk h = (new AttributeRanged((class_sk)null, "generic.luck", 0.0D, -1024.0D, 1024.0D)).a(true);

   public static NBTTagList a(AttributeMapBase var0) {
      NBTTagList var1 = new NBTTagList();
      Iterator var2 = var0.a().iterator();

      while(var2.hasNext()) {
         class_sl var3 = (class_sl)var2.next();
         var1.a((NBTTag)a(var3));
      }

      return var1;
   }

   private static NBTTagCompound a(class_sl var0) {
      NBTTagCompound var1 = new NBTTagCompound();
      class_sk var2 = var0.a();
      var1.a("Name", var2.a());
      var1.a("Base", var0.b());
      Collection var3 = var0.c();
      if(var3 != null && !var3.isEmpty()) {
         NBTTagList var4 = new NBTTagList();
         Iterator var5 = var3.iterator();

         while(var5.hasNext()) {
            AttributeModifier var6 = (AttributeModifier)var5.next();
            if(var6.e()) {
               var4.a((NBTTag)a(var6));
            }
         }

         var1.a((String)"Modifiers", (NBTTag)var4);
      }

      return var1;
   }

   public static NBTTagCompound a(AttributeModifier var0) {
      NBTTagCompound var1 = new NBTTagCompound();
      var1.a("Name", var0.b());
      var1.a("Amount", var0.d());
      var1.a("Operation", var0.c());
      var1.a("UUID", var0.a());
      return var1;
   }

   public static void a(AttributeMapBase var0, NBTTagList var1) {
      for(int var2 = 0; var2 < var1.c(); ++var2) {
         NBTTagCompound var3 = var1.b(var2);
         class_sl var4 = var0.a(var3.l("Name"));
         if(var4 != null) {
            a(var4, var3);
         } else {
            i.warn("Ignoring unknown attribute \'" + var3.l("Name") + "\'");
         }
      }

   }

   private static void a(class_sl var0, NBTTagCompound var1) {
      var0.a(var1.k("Base"));
      if(var1.b("Modifiers", 9)) {
         NBTTagList var2 = var1.c("Modifiers", 10);

         for(int var3 = 0; var3 < var2.c(); ++var3) {
            AttributeModifier var4 = a(var2.b(var3));
            if(var4 != null) {
               AttributeModifier var5 = var0.a(var4.a());
               if(var5 != null) {
                  var0.c(var5);
               }

               var0.b(var4);
            }
         }
      }

   }

   public static AttributeModifier a(NBTTagCompound var0) {
      UUID var1 = var0.a("UUID");

      try {
         return new AttributeModifier(var1, var0.l("Name"), var0.k("Amount"), var0.h("Operation"));
      } catch (Exception var3) {
         i.warn("Unable to create attribute: " + var3.getMessage());
         return null;
      }
   }
}
