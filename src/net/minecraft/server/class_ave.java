package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BlockLongGrass;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_auc;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_ave extends class_auc {
   private final IBlockData a;

   public class_ave(BlockLongGrass.EnumTallGrassType var1) {
      this.a = Blocks.H.u().set(BlockLongGrass.a, var1);
   }

   public boolean b(World var1, Random var2, BlockPosition var3) {
      IBlockData var4;
      while(((var4 = var1.getType(var3)).getMaterial() == Material.a || var4.getMaterial() == Material.j) && var3.q() > 0) {
         var3 = var3.b();
      }

      for(int var5 = 0; var5 < 128; ++var5) {
         BlockPosition var6 = var3.a(var2.nextInt(8) - var2.nextInt(8), var2.nextInt(4) - var2.nextInt(4), var2.nextInt(8) - var2.nextInt(8));
         if(var1.d(var6) && Blocks.H.f(var1, var6, this.a)) {
            var1.a((BlockPosition)var6, (IBlockData)this.a, 2);
         }
      }

      return true;
   }
}
