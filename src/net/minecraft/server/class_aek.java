package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_rz;

public class class_aek extends Item {
   public class_aek() {
      this.d(1);
      this.e(238);
      this.a(CreativeModeTab.i);
   }

   public boolean a(ItemStack var1, World var2, IBlockData var3, BlockPosition var4, class_rz var5) {
      var1.a(1, (class_rz)var5);
      Block var6 = var3.getBlock();
      return var3.getMaterial() != Material.j && var6 != Blocks.G && var6 != Blocks.H && var6 != Blocks.bn && var6 != Blocks.bS && var6 != Blocks.L?super.a(var1, var2, var3, var4, var5):true;
   }

   public boolean a(IBlockData var1) {
      Block var2 = var1.getBlock();
      return var2 == Blocks.G || var2 == Blocks.af || var2 == Blocks.bS;
   }

   public float a(ItemStack var1, IBlockData var2) {
      Block var3 = var2.getBlock();
      return var3 != Blocks.G && var2.getMaterial() != Material.j?(var3 == Blocks.L?5.0F:super.a(var1, var2)):15.0F;
   }
}
