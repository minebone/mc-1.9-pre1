package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_or;

public class BlockDirt extends Block {
   public static final BlockStateEnum a = BlockStateEnum.a("variant", BlockDirt.EnumDirtVariant.class);
   public static final class_arm b = class_arm.a("snowy");

   protected BlockDirt() {
      super(Material.c);
      this.w(this.A.b().set(a, BlockDirt.EnumDirtVariant.DIRT).set(b, Boolean.valueOf(false)));
      this.a(CreativeModeTab.b);
   }

   public MaterialMapColor r(IBlockData var1) {
      return ((BlockDirt.EnumDirtVariant)var1.get(a)).d();
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      if(var1.get(a) == BlockDirt.EnumDirtVariant.PODZOL) {
         Block var4 = var2.getType(var3.a()).getBlock();
         var1 = var1.set(b, Boolean.valueOf(var4 == Blocks.aJ || var4 == Blocks.aH));
      }

      return var1;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(this, 1, ((BlockDirt.EnumDirtVariant)var3.get(a)).a());
   }

   public IBlockData a(int var1) {
      return this.u().set(a, BlockDirt.EnumDirtVariant.a(var1));
   }

   public int e(IBlockData var1) {
      return ((BlockDirt.EnumDirtVariant)var1.get(a)).a();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b});
   }

   public int d(IBlockData var1) {
      BlockDirt.EnumDirtVariant var2 = (BlockDirt.EnumDirtVariant)var1.get(a);
      if(var2 == BlockDirt.EnumDirtVariant.PODZOL) {
         var2 = BlockDirt.EnumDirtVariant.DIRT;
      }

      return var2.a();
   }

   public static enum EnumDirtVariant implements class_or {
      DIRT(0, "dirt", "default", MaterialMapColor.l),
      COARSE_DIRT(1, "coarse_dirt", "coarse", MaterialMapColor.l),
      PODZOL(2, "podzol", MaterialMapColor.J);

      private static final BlockDirt.EnumDirtVariant[] d = new BlockDirt.EnumDirtVariant[values().length];
      private final int e;
      private final String f;
      private final String g;
      private final MaterialMapColor h;

      private EnumDirtVariant(int var3, String var4, MaterialMapColor var5) {
         this(var3, var4, var4, var5);
      }

      private EnumDirtVariant(int var3, String var4, String var5, MaterialMapColor var6) {
         this.e = var3;
         this.f = var4;
         this.g = var5;
         this.h = var6;
      }

      public int a() {
         return this.e;
      }

      public String c() {
         return this.g;
      }

      public MaterialMapColor d() {
         return this.h;
      }

      public String toString() {
         return this.f;
      }

      public static BlockDirt.EnumDirtVariant a(int var0) {
         if(var0 < 0 || var0 >= d.length) {
            var0 = 0;
         }

         return d[var0];
      }

      public String m() {
         return this.f;
      }

      static {
         BlockDirt.EnumDirtVariant[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            BlockDirt.EnumDirtVariant var3 = var0[var2];
            d[var3.a()] = var3;
         }

      }
   }
}
