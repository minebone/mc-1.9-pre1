package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;

public class BlockStatePredicate implements Predicate {
   public static final Predicate a = new Predicate() {
      public boolean a(IBlockData var1) {
         return true;
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((IBlockData)var1);
      }
   };
   private final BlockStateList b;
   private final Map c = Maps.newHashMap();

   private BlockStatePredicate(BlockStateList var1) {
      this.b = var1;
   }

   public static BlockStatePredicate a(Block var0) {
      return new BlockStatePredicate(var0.t());
   }

   public boolean a(IBlockData var1) {
      if(var1 != null && var1.getBlock().equals(this.b.c())) {
         Iterator var2 = this.c.entrySet().iterator();

         Entry var3;
         do {
            if(!var2.hasNext()) {
               return true;
            }

            var3 = (Entry)var2.next();
         } while(this.a(var1, (IBlockState)var3.getKey(), (Predicate)var3.getValue()));

         return false;
      } else {
         return false;
      }
   }

   protected boolean a(IBlockData var1, IBlockState var2, Predicate var3) {
      return var3.apply(var1.get(var2));
   }

   public BlockStatePredicate a(IBlockState var1, Predicate var2) {
      if(!this.b.d().contains(var1)) {
         throw new IllegalArgumentException(this.b + " cannot support property " + var1);
      } else {
         this.c.put(var1, var2);
         return this;
      }
   }

   // $FF: synthetic method
   public boolean apply(Object var1) {
      return this.a((IBlockData)var1);
   }
}
