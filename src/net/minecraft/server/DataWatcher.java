package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.netty.handler.codec.DecoderException;
import io.netty.handler.codec.EncoderException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import net.minecraft.server.CrashReport;
import net.minecraft.server.CrashReportSystemDetails;
import net.minecraft.server.Entity;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.class_e;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kf;
import net.minecraft.server.class_kg;
import org.apache.commons.lang3.ObjectUtils;

public class DataWatcher {
   private static final Map a = Maps.newHashMap();
   private final Entity b;
   private final Map c = Maps.newHashMap();
   private final ReadWriteLock d = new ReentrantReadWriteLock();
   private boolean e = true;
   private boolean f;

   public DataWatcher(Entity var1) {
      this.b = var1;
   }

   public static class_ke a(Class var0, class_kf var1) {
	   
      int var2;
      if(a.containsKey(var0)) {
         var2 = ((Integer)a.get(var0)).intValue() + 1;
      } else {
         int var3 = 0;
         Class var4 = var0;

         while(var4 != Entity.class) {
            var4 = var4.getSuperclass();
            if(a.containsKey(var4)) {
               var3 = ((Integer)a.get(var4)).intValue() + 1;
               break;
            }
         }

         var2 = var3;
      }

      if(var2 > 254) {
         throw new IllegalArgumentException("Data value id is too big with " + var2 + "! (Max is " + 254 + ")");
      } else {
    	  System.out.println(var0.getSimpleName() + " - " + var2);
         a.put(var0, Integer.valueOf(var2));
         return var1.a(var2);
      }
   }

   public void a(class_ke var1, Object var2) {
      int var3 = var1.a();
      if(var3 > 254) {
         throw new IllegalArgumentException("Data value id is too big with " + var3 + "! (Max is " + 254 + ")");
      } else if(this.c.containsKey(Integer.valueOf(var3))) {
         throw new IllegalArgumentException("Duplicate id value for " + var3 + "!");
      } else if(class_kg.b(var1.b()) < 0) {
         throw new IllegalArgumentException("Unregistered serializer " + var1.b() + " for " + var3 + "!");
      } else {
         this.c(var1, var2);
      }
   }

   private void c(class_ke var1, Object var2) {
      DataWatcher.WatchableObject var3 = new DataWatcher.WatchableObject(var1, var2);
      this.d.writeLock().lock();
      this.c.put(Integer.valueOf(var1.a()), var3);
      this.e = false;
      this.d.writeLock().unlock();
   }

   private DataWatcher.WatchableObject c(class_ke var1) {
      this.d.readLock().lock();

      DataWatcher.WatchableObject var2;
      try {
         var2 = (DataWatcher.WatchableObject)this.c.get(Integer.valueOf(var1.a()));
      } catch (Throwable var6) {
         CrashReport var4 = CrashReport.a(var6, "Getting synched entity data");
         CrashReportSystemDetails var5 = var4.a("Synched entity data");
         var5.a((String)"Data ID", (Object)var1);
         throw new class_e(var4);
      }

      this.d.readLock().unlock();
      return var2;
   }

   public Object a(class_ke var1) {
      return this.c(var1).b();
   }

   public void b(class_ke var1, Object var2) {
      DataWatcher.WatchableObject var3 = this.c(var1);
      if(ObjectUtils.notEqual(var2, var3.b())) {
         var3.a(var2);
         this.b.a(var1);
         var3.a(true);
         this.f = true;
      }

   }

   public void b(class_ke var1) {
      this.c(var1).c = true;
      this.f = true;
   }

   public boolean a() {
      return this.f;
   }

   public static void a(List var0, PacketDataSerializer var1) throws IOException {
      if(var0 != null) {
         int var2 = 0;

         for(int var3 = var0.size(); var2 < var3; ++var2) {
            DataWatcher.WatchableObject var4 = (DataWatcher.WatchableObject)var0.get(var2);
            a(var1, var4);
         }
      }

      var1.writeByte(255);
   }

   public List b() {
      ArrayList var1 = null;
      if(this.f) {
         this.d.readLock().lock();
         Iterator var2 = this.c.values().iterator();

         while(var2.hasNext()) {
            DataWatcher.WatchableObject var3 = (DataWatcher.WatchableObject)var2.next();
            if(var3.c()) {
               var3.a(false);
               if(var1 == null) {
                  var1 = Lists.newArrayList();
               }

               var1.add(var3);
            }
         }

         this.d.readLock().unlock();
      }

      this.f = false;
      return var1;
   }

   public void a(PacketDataSerializer var1) throws IOException {
      this.d.readLock().lock();
      Iterator var2 = this.c.values().iterator();

      while(var2.hasNext()) {
         DataWatcher.WatchableObject var3 = (DataWatcher.WatchableObject)var2.next();
         a(var1, var3);
      }

      this.d.readLock().unlock();
      var1.writeByte(255);
   }

   public List c() {
      ArrayList var1 = null;
      this.d.readLock().lock();

      DataWatcher.WatchableObject var3;
      for(Iterator var2 = this.c.values().iterator(); var2.hasNext(); var1.add(var3)) {
         var3 = (DataWatcher.WatchableObject)var2.next();
         if(var1 == null) {
            var1 = Lists.newArrayList();
         }
      }

      this.d.readLock().unlock();
      return var1;
   }

   private static void a(PacketDataSerializer var0, DataWatcher.WatchableObject var1) throws IOException {
      class_ke var2 = var1.a();
      int var3 = class_kg.b(var2.b());
      if(var3 < 0) {
         throw new EncoderException("Unknown serializer type " + var2.b());
      } else {
         var0.writeByte(var2.a());
         var0.writeVarInt(var3);
         var2.b().a(var0, var1.b());
      }
   }

   public static List b(PacketDataSerializer var0) throws IOException {
      ArrayList var1 = null;

      short var2;
      while((var2 = var0.readUnsignedByte()) != 255) {
         if(var1 == null) {
            var1 = Lists.newArrayList();
         }

         short var3 = var0.readUnsignedByte();
         class_kf var4 = class_kg.a(var3);
         if(var4 == null) {
            throw new DecoderException("Unknown serializer type " + var3);
         }

         var1.add(new DataWatcher.WatchableObject(var4.a(var2), var4.a(var0)));
      }

      return var1;
   }

   public boolean d() {
      return this.e;
   }

   public void e() {
      this.f = false;
   }

   public static class WatchableObject {
      private final class_ke a;
      private Object b;
      private boolean c;

      public WatchableObject(class_ke var1, Object var2) {
         this.a = var1;
         this.b = var2;
         this.c = true;
      }

      public class_ke a() {
         return this.a;
      }

      public void a(Object var1) {
         this.b = var1;
      }

      public Object b() {
         return this.b;
      }

      public boolean c() {
         return this.c;
      }

      public void a(boolean var1) {
         this.c = var1;
      }
   }
}
