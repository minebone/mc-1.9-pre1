package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;

public class class_ama extends Block {
   private boolean a;

   protected class_ama(Material var1, boolean var2) {
      this(var1, var2, var1.r());
   }

   protected class_ama(Material var1, boolean var2, MaterialMapColor var3) {
      super(var1, var3);
      this.a = var2;
   }

   public boolean b(IBlockData var1) {
      return false;
   }
}
