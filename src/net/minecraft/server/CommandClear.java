package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.Item;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MojangsonParser;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.class_bz;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ec;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;

public class CommandClear extends CommandAbstract {
   public String c() {
      return "clear";
   }

   public String b(ICommandListener var1) {
      return "commands.clear.usage";
   }

   public int a() {
      return 2;
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      EntityPlayer var4 = var3.length == 0?a(var2):a(var1, var2, var3[0]);
      Item var5 = var3.length >= 2?a(var2, var3[1]):null;
      int var6 = var3.length >= 3?a(var3[2], -1):-1;
      int var7 = var3.length >= 4?a(var3[3], -1):-1;
      NBTTagCompound var8 = null;
      if(var3.length >= 5) {
         try {
            var8 = MojangsonParser.a(a(var3, 4));
         } catch (class_ec var10) {
            throw new class_bz("commands.clear.tagError", new Object[]{var10.getMessage()});
         }
      }

      if(var3.length >= 2 && var5 == null) {
         throw new class_bz("commands.clear.failure", new Object[]{var4.h_()});
      } else {
         int var9 = var4.br.a(var5, var6, var7, var8);
         var4.bs.b();
         if(!var4.abilities.d) {
            var4.r();
         }

         var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_ITEMS, var9);
         if(var9 == 0) {
            throw new class_bz("commands.clear.failure", new Object[]{var4.h_()});
         } else {
            if(var7 == 0) {
               var2.a(new ChatMessage("commands.clear.testing", new Object[]{var4.h_(), Integer.valueOf(var9)}));
            } else {
               a(var2, this, "commands.clear.success", new Object[]{var4.h_(), Integer.valueOf(var9)});
            }

         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, var1.J()):(var3.length == 2?a(var3, Item.f.c()):Collections.emptyList());
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 0;
   }
}
