package net.minecraft.server;

import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;

public class TileEntityComparator extends TileEntity {
   private int a;

   public void a(NBTTagCompound var1) {
      super.a(var1);
      var1.a("OutputSignal", this.a);
   }

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      this.a = var2.h("OutputSignal");
   }

   public int b() {
      return this.a;
   }

   public void a(int var1) {
      this.a = var1;
   }
}
