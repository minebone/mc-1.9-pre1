package net.minecraft.server;

import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.class_pa;
import net.minecraft.server.class_pc;
import net.minecraft.server.class_py;

public class class_px extends class_py {
   private final String[] a;

   public class_px(String var1, String... var2) {
      super("id", var1);
      this.a = var2;
   }

   NBTTagCompound b(class_pa var1, NBTTagCompound var2, int var3) {
      int var4 = 0;

      for(int var5 = this.a.length; var4 < var5; ++var4) {
         var2 = class_pc.b(var1, var2, var3, this.a[var4]);
      }

      return var2;
   }
}
