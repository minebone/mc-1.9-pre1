package net.minecraft.server;

import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.StructureBoundingBox;
import net.minecraft.server.StructurePiece;
import net.minecraft.server.World;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_awm;
import net.minecraft.server.class_awn;
import net.minecraft.server.BlockPosition;

public abstract class class_awh extends StructurePiece {
   private static final class_awm d = new class_awm();
   protected class_awn a;
   protected class_awm b;
   protected BlockPosition c;

   public class_awh() {
      this.b = d.a(true).a(Blocks.AIR);
   }

   public class_awh(int var1) {
      super(var1);
      this.b = d.a(true).a(Blocks.AIR);
   }

   protected void a(class_awn var1, BlockPosition var2, class_awm var3) {
      this.a = var1;
      this.a(EnumDirection.NORTH);
      this.c = var2;
      this.b = var3;
      this.h();
   }

   protected void a(NBTTagCompound var1) {
      var1.a("TPX", this.c.p());
      var1.a("TPY", this.c.q());
      var1.a("TPZ", this.c.r());
   }

   protected void b(NBTTagCompound var1) {
      this.c = new BlockPosition(var1.h("TPX"), var1.h("TPY"), var1.h("TPZ"));
   }

   public boolean a(World var1, Random var2, StructureBoundingBox var3) {
      this.b.a(var3);
      this.a.b(var1, this.c, this.b);
      Map var4 = this.a.a(this.c, this.b);
      Iterator var5 = var4.keySet().iterator();

      while(var5.hasNext()) {
         BlockPosition var6 = (BlockPosition)var5.next();
         String var7 = (String)var4.get(var6);
         this.a(var7, var6, var1, var2, var3);
      }

      return true;
   }

   protected abstract void a(String var1, BlockPosition var2, World var3, Random var4, StructureBoundingBox var5);

   private void h() {
      class_aod var1 = this.b.c();
      BlockPosition var2 = this.a.a(var1);
      this.l = new StructureBoundingBox(0, 0, 0, var2.p(), var2.q() - 1, var2.r());
      switch(class_awh.SyntheticClass_1.a[var1.ordinal()]) {
      case 1:
      default:
         break;
      case 2:
         this.l.a(-var2.p(), 0, 0);
         break;
      case 3:
         this.l.a(0, 0, -var2.r());
         break;
      case 4:
         this.l.a(-var2.p(), 0, -var2.r());
      }

      this.l.a(this.c.p(), this.c.q(), this.c.r());
   }

   public void a(int var1, int var2, int var3) {
      super.a(var1, var2, var3);
      this.c = this.c.a(var1, var2, var3);
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[class_aod.values().length];

      static {
         try {
            a[class_aod.NONE.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[class_aod.CLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[class_aod.CLOCKWISE_180.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
