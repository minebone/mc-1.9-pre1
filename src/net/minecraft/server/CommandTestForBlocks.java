package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.Blocks;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.StructureBoundingBox;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;

public class CommandTestForBlocks extends CommandAbstract {
   public String c() {
      return "testforblocks";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.compare.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 9) {
         throw new class_cf("commands.compare.usage", new Object[0]);
      } else {
         var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_BLOCKS, 0);
         BlockPosition var4 = a(var2, var3, 0, false);
         BlockPosition var5 = a(var2, var3, 3, false);
         BlockPosition var6 = a(var2, var3, 6, false);
         StructureBoundingBox var7 = new StructureBoundingBox(var4, var5);
         StructureBoundingBox var8 = new StructureBoundingBox(var6, var6.a(var7.b()));
         int var9 = var7.c() * var7.d() * var7.e();
         if(var9 > 524288) {
            throw new class_bz("commands.compare.tooManyBlocks", new Object[]{Integer.valueOf(var9), Integer.valueOf(524288)});
         } else if(var7.b >= 0 && var7.e < 256 && var8.b >= 0 && var8.e < 256) {
            World var10 = var2.e();
            if(var10.a(var7) && var10.a(var8)) {
               boolean var11 = false;
               if(var3.length > 9 && var3[9].equals("masked")) {
                  var11 = true;
               }

               var9 = 0;
               BlockPosition var12 = new BlockPosition(var8.a - var7.a, var8.b - var7.b, var8.c - var7.c);
               BlockPosition.class_a_in_class_cj var13 = new BlockPosition.class_a_in_class_cj();
               BlockPosition.class_a_in_class_cj var14 = new BlockPosition.class_a_in_class_cj();

               for(int var15 = var7.c; var15 <= var7.f; ++var15) {
                  for(int var16 = var7.b; var16 <= var7.e; ++var16) {
                     for(int var17 = var7.a; var17 <= var7.d; ++var17) {
                        var13.c(var17, var16, var15);
                        var14.c(var17 + var12.p(), var16 + var12.q(), var15 + var12.r());
                        boolean var18 = false;
                        IBlockData var19 = var10.getType(var13);
                        if(!var11 || var19.getBlock() != Blocks.AIR) {
                           if(var19 == var10.getType(var14)) {
                              TileEntity var20 = var10.r(var13);
                              TileEntity var21 = var10.r(var14);
                              if(var20 != null && var21 != null) {
                                 NBTTagCompound var22 = new NBTTagCompound();
                                 var20.a(var22);
                                 var22.q("x");
                                 var22.q("y");
                                 var22.q("z");
                                 NBTTagCompound var23 = new NBTTagCompound();
                                 var21.a(var23);
                                 var23.q("x");
                                 var23.q("y");
                                 var23.q("z");
                                 if(!var22.equals(var23)) {
                                    var18 = true;
                                 }
                              } else if(var20 != null) {
                                 var18 = true;
                              }
                           } else {
                              var18 = true;
                           }

                           ++var9;
                           if(var18) {
                              throw new class_bz("commands.compare.failed", new Object[0]);
                           }
                        }
                     }
                  }
               }

               var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_BLOCKS, var9);
               a(var2, this, "commands.compare.success", new Object[]{Integer.valueOf(var9)});
            } else {
               throw new class_bz("commands.compare.outOfWorld", new Object[0]);
            }
         } else {
            throw new class_bz("commands.compare.outOfWorld", new Object[0]);
         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length > 0 && var3.length <= 3?a(var3, 0, var4):(var3.length > 3 && var3.length <= 6?a(var3, 3, var4):(var3.length > 6 && var3.length <= 9?a(var3, 6, var4):(var3.length == 10?a(var3, new String[]{"masked", "all"}):Collections.emptyList())));
   }
}
