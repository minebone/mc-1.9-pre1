package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ali;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_xq;

public class class_xs extends class_xq {
   public class_xs(World var1) {
      super(var1);
   }

   public class_xs(World var1, BlockPosition var2) {
      super(var1, var2);
      this.b((double)var2.p() + 0.5D, (double)var2.q() + 0.5D, (double)var2.r() + 0.5D);
      float var3 = 0.125F;
      float var4 = 0.1875F;
      float var5 = 0.25F;
      this.a((AxisAlignedBB)(new AxisAlignedBB(this.locX - 0.1875D, this.locY - 0.25D + 0.125D, this.locZ - 0.1875D, this.locX + 0.1875D, this.locY + 0.25D + 0.125D, this.locZ + 0.1875D)));
   }

   public void b(double var1, double var3, double var5) {
      super.b((double)MathHelper.c(var1) + 0.5D, (double)MathHelper.c(var3) + 0.5D, (double)MathHelper.c(var5) + 0.5D);
   }

   protected void j() {
      this.locX = (double)this.a.p() + 0.5D;
      this.locY = (double)this.a.q() + 0.5D;
      this.locZ = (double)this.a.r() + 0.5D;
   }

   public void a(EnumDirection var1) {
   }

   public int l() {
      return 9;
   }

   public int n() {
      return 9;
   }

   public float bm() {
      return -0.0625F;
   }

   public void a(Entity var1) {
      this.a(class_ng.da, 1.0F, 1.0F);
   }

   public boolean d(NBTTagCompound var1) {
      return false;
   }

   public void b(NBTTagCompound var1) {
   }

   public void a(NBTTagCompound var1) {
   }

   public boolean a(EntityHuman var1, ItemStack var2, EnumHand var3) {
      if(this.world.E) {
         return true;
      } else {
         boolean var4 = false;
         double var5;
         List var7;
         Iterator var8;
         EntityInsentient var9;
         if(var2 != null && var2.b() == Items.cx) {
            var5 = 7.0D;
            var7 = this.world.a(EntityInsentient.class, new AxisAlignedBB(this.locX - var5, this.locY - var5, this.locZ - var5, this.locX + var5, this.locY + var5, this.locZ + var5));
            var8 = var7.iterator();

            while(var8.hasNext()) {
               var9 = (EntityInsentient)var8.next();
               if(var9.cP() && var9.cQ() == var1) {
                  var9.b(this, true);
                  var4 = true;
               }
            }
         }

         if(!var4) {
            this.S();
            if(var1.abilities.d) {
               var5 = 7.0D;
               var7 = this.world.a(EntityInsentient.class, new AxisAlignedBB(this.locX - var5, this.locY - var5, this.locZ - var5, this.locX + var5, this.locY + var5, this.locZ + var5));
               var8 = var7.iterator();

               while(var8.hasNext()) {
                  var9 = (EntityInsentient)var8.next();
                  if(var9.cP() && var9.cQ() == this) {
                     var9.a(true, false);
                  }
               }
            }
         }

         return true;
      }
   }

   public boolean k() {
      return this.world.getType(this.a).getBlock() instanceof class_ali;
   }

   public static class_xs a(World var0, BlockPosition var1) {
      class_xs var2 = new class_xs(var0, var1);
      var2.attachedToPlayer = true;
      var0.a((Entity)var2);
      var2.o();
      return var2;
   }

   public static class_xs b(World var0, BlockPosition var1) {
      int var2 = var1.p();
      int var3 = var1.q();
      int var4 = var1.r();
      List var5 = var0.a(class_xs.class, new AxisAlignedBB((double)var2 - 1.0D, (double)var3 - 1.0D, (double)var4 - 1.0D, (double)var2 + 1.0D, (double)var3 + 1.0D, (double)var4 + 1.0D));
      Iterator var6 = var5.iterator();

      class_xs var7;
      do {
         if(!var6.hasNext()) {
            return null;
         }

         var7 = (class_xs)var6.next();
      } while(!var7.q().equals(var1));

      return var7;
   }

   public void o() {
      this.a(class_ng.db, 1.0F, 1.0F);
   }
}
