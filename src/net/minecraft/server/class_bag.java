package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import java.util.Random;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.Entity;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agn;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_bab;
import net.minecraft.server.class_bae;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_rz;

public class class_bag extends class_bae {
   private final class_bab a;

   public class_bag(class_baq[] var1, class_bab var2) {
      super(var1);
      this.a = var2;
   }

   public ItemStack a(ItemStack var1, Random var2, class_azy var3) {
      Entity var4 = var3.c();
      if(var4 instanceof class_rz) {
         int var5 = class_agn.h((class_rz)var4);
         if(var5 == 0) {
            return var1;
         }

         float var6 = (float)var5 * this.a.b(var2);
         var1.b += Math.round(var6);
      }

      return var1;
   }

   public static class class_a_in_class_bag extends class_bae.class_a_in_class_bae {
      protected class_a_in_class_bag() {
         super(new class_kk("looting_enchant"), class_bag.class);
      }

      public void a(JsonObject var1, class_bag var2, JsonSerializationContext var3) {
         var1.add("count", var3.serialize(var2.a));
      }

      public class_bag a(JsonObject var1, JsonDeserializationContext var2, class_baq[] var3) {
         return new class_bag(var3, (class_bab)ChatDeserializer.a(var1, "count", var2, class_bab.class));
      }

      // $FF: synthetic method
      public class_bae b(JsonObject var1, JsonDeserializationContext var2, class_baq[] var3) {
         return this.a(var1, var2, var3);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(JsonObject var1, class_bae var2, JsonSerializationContext var3) {
         this.a(var1, (class_bag)var2, var3);
      }
   }
}
