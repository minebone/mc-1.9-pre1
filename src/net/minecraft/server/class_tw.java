package net.minecraft.server;

import net.minecraft.server.EntityCreature;
import net.minecraft.server.Vec3D;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_vl;

public class class_tw extends class_tj {
   private EntityCreature a;
   private class_rz b;
   private double c;
   private double d;
   private double e;
   private double f;
   private float g;

   public class_tw(EntityCreature var1, double var2, float var4) {
      this.a = var1;
      this.f = var2;
      this.g = var4;
      this.a(1);
   }

   public boolean a() {
      this.b = this.a.A();
      if(this.b == null) {
         return false;
      } else if(this.b.h(this.a) > (double)(this.g * this.g)) {
         return false;
      } else {
         Vec3D var1 = class_vl.a(this.a, 16, 7, new Vec3D(this.b.locX, this.b.locY, this.b.locZ));
         if(var1 == null) {
            return false;
         } else {
            this.c = var1.b;
            this.d = var1.c;
            this.e = var1.d;
            return true;
         }
      }
   }

   public boolean b() {
      return !this.a.x().n() && this.b.at() && this.b.h(this.a) < (double)(this.g * this.g);
   }

   public void d() {
      this.b = null;
   }

   public void c() {
      this.a.x().a(this.c, this.d, this.e, this.f);
   }
}
