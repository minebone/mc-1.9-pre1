package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;

public class class_aei extends Item {
   private Block a;
   private Block b;

   public class_aei(Block var1, Block var2) {
      this.a = var1;
      this.b = var2;
      this.a(CreativeModeTab.l);
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      if(var6 == EnumDirection.UP && var2.a(var4.a(var6), var6, var1) && var3.getType(var4).getBlock() == this.b && var3.d(var4.a())) {
         var3.a(var4.a(), this.a.u());
         --var1.b;
         return EnumResult.SUCCESS;
      } else {
         return EnumResult.FAIL;
      }
   }
}
