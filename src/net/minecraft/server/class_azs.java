package net.minecraft.server;

import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.Set;
import net.minecraft.server.class_kk;

public class class_azs {
   private static final Set ap = Sets.newHashSet();
   private static final Set aq = Collections.unmodifiableSet(ap);
   public static final class_kk a = a("empty");
   public static final class_kk b = a("chests/spawn_bonus_chest");
   public static final class_kk c = a("chests/end_city_treasure");
   public static final class_kk d = a("chests/simple_dungeon");
   public static final class_kk e = a("chests/village_blacksmith");
   public static final class_kk f = a("chests/abandoned_mineshaft");
   public static final class_kk g = a("chests/nether_bridge");
   public static final class_kk h = a("chests/stronghold_library");
   public static final class_kk i = a("chests/stronghold_crossing");
   public static final class_kk j = a("chests/stronghold_corridor");
   public static final class_kk k = a("chests/desert_pyramid");
   public static final class_kk l = a("chests/jungle_temple");
   public static final class_kk m = a("chests/igloo_chest");
   public static final class_kk n = a("entities/witch");
   public static final class_kk o = a("entities/blaze");
   public static final class_kk p = a("entities/creeper");
   public static final class_kk q = a("entities/spider");
   public static final class_kk r = a("entities/cave_spider");
   public static final class_kk s = a("entities/giant");
   public static final class_kk t = a("entities/silverfish");
   public static final class_kk u = a("entities/enderman");
   public static final class_kk v = a("entities/guardian");
   public static final class_kk w = a("entities/elder_guardian");
   public static final class_kk x = a("entities/shulker");
   public static final class_kk y = a("entities/iron_golem");
   public static final class_kk z = a("entities/snowman");
   public static final class_kk A = a("entities/rabbit");
   public static final class_kk B = a("entities/chicken");
   public static final class_kk C = a("entities/pig");
   public static final class_kk D = a("entities/horse");
   public static final class_kk E = a("entities/zombie_horse");
   public static final class_kk F = a("entities/skeleton_horse");
   public static final class_kk G = a("entities/cow");
   public static final class_kk H = a("entities/mushroom_cow");
   public static final class_kk I = a("entities/wolf");
   public static final class_kk J = a("entities/ocelot");
   public static final class_kk K = a("entities/sheep");
   public static final class_kk L = a("entities/sheep/white");
   public static final class_kk M = a("entities/sheep/orange");
   public static final class_kk N = a("entities/sheep/magenta");
   public static final class_kk O = a("entities/sheep/light_blue");
   public static final class_kk P = a("entities/sheep/yellow");
   public static final class_kk Q = a("entities/sheep/lime");
   public static final class_kk R = a("entities/sheep/pink");
   public static final class_kk S = a("entities/sheep/gray");
   public static final class_kk T = a("entities/sheep/silver");
   public static final class_kk U = a("entities/sheep/cyan");
   public static final class_kk V = a("entities/sheep/purple");
   public static final class_kk W = a("entities/sheep/blue");
   public static final class_kk X = a("entities/sheep/brown");
   public static final class_kk Y = a("entities/sheep/green");
   public static final class_kk Z = a("entities/sheep/red");
   public static final class_kk aa = a("entities/sheep/black");
   public static final class_kk ab = a("entities/bat");
   public static final class_kk ac = a("entities/slime");
   public static final class_kk ad = a("entities/magma_cube");
   public static final class_kk ae = a("entities/ghast");
   public static final class_kk af = a("entities/squid");
   public static final class_kk ag = a("entities/endermite");
   public static final class_kk ah = a("entities/zombie");
   public static final class_kk ai = a("entities/zombie_pigman");
   public static final class_kk aj = a("entities/skeleton");
   public static final class_kk ak = a("entities/wither_skeleton");
   public static final class_kk al = a("gameplay/fishing");
   public static final class_kk am = a("gameplay/fishing/junk");
   public static final class_kk an = a("gameplay/fishing/treasure");
   public static final class_kk ao = a("gameplay/fishing/fish");

   private static class_kk a(String var0) {
      return a(new class_kk("minecraft", var0));
   }

   public static class_kk a(class_kk var0) {
      if(ap.add(var0)) {
         return var0;
      } else {
         throw new IllegalArgumentException(var0 + " is already a registered built-in loot table");
      }
   }

   public static Set a() {
      return aq;
   }
}
