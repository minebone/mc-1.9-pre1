package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.Chunk;
import net.minecraft.server.TileEntity;
import net.minecraft.server.class_ahm;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutBlockChange;
import net.minecraft.server.PacketPlayOutMultiBlockChange;
import net.minecraft.server.PacketPlayOutChunkUnload;
import net.minecraft.server.PacketPlayOutMapChunk;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.class_lv;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_lu {
   private static final Logger a = LogManager.getLogger();
   private final class_lv b;
   private final List c = Lists.newArrayList();
   private final class_ahm d;
   private final short[] e = new short[64];
   private Chunk f;
   private int g;
   private int h;
   private long i;
   private boolean j;

   public class_lu(class_lv var1, int var2, int var3) {
      this.b = var1;
      this.d = new class_ahm(var2, var3);
      this.f = var1.a().r().c(var2, var3);
   }

   public class_ahm a() {
      return this.d;
   }

   public void a(EntityPlayer var1) {
      if(this.c.contains(var1)) {
         a.debug("Failed to add player. {} already is in chunk {}, {}", new Object[]{var1, Integer.valueOf(this.d.a), Integer.valueOf(this.d.b)});
      } else {
         if(this.c.isEmpty()) {
            this.i = this.b.a().P();
         }

         this.c.add(var1);
         if(this.j) {
            this.c(var1);
         }

      }
   }

   public void b(EntityPlayer var1) {
      if(this.c.contains(var1)) {
         if(this.j) {
            var1.a.a((Packet)(new PacketPlayOutChunkUnload(this.d.a, this.d.b)));
         }

         this.c.remove(var1);
         if(this.c.isEmpty()) {
            this.b.b(this);
         }

      }
   }

   public boolean a(boolean var1) {
      if(this.f != null) {
         return true;
      } else {
         if(var1) {
            this.f = this.b.a().r().d(this.d.a, this.d.b);
         } else {
            this.f = this.b.a().r().c(this.d.a, this.d.b);
         }

         return this.f != null;
      }
   }

   public boolean b() {
      if(this.j) {
         return true;
      } else if(this.f == null) {
         return false;
      } else if(!this.f.i()) {
         return false;
      } else {
         this.g = 0;
         this.h = 0;
         this.j = true;
         ArrayList var1 = Lists.newArrayList((Iterable)this.b.a().a(this.d.a * 16, 0, this.d.b * 16, this.d.a * 16 + 16, 256, this.d.b * 16 + 16));
         PacketPlayOutMapChunk var2 = new PacketPlayOutMapChunk(this.f, true, '\uffff');
         Iterator var3 = this.c.iterator();

         while(var3.hasNext()) {
            EntityPlayer var4 = (EntityPlayer)var3.next();
            var4.a.a((Packet)var2);
            Iterator var5 = var1.iterator();

            while(var5.hasNext()) {
               TileEntity var6 = (TileEntity)var5.next();
               Packet var7 = var6.D_();
               if(var7 != null) {
                  var4.a.a(var7);
               }
            }

            this.b.a().v().a(var4, this.f);
         }

         return true;
      }
   }

   public void c(EntityPlayer var1) {
      if(this.j) {
         var1.a.a((Packet)(new PacketPlayOutMapChunk(this.f, true, '\uffff')));
         Iterator var2 = this.b.a().a(this.d.a * 16, 0, this.d.b * 16, this.d.a * 16 + 16, 256, this.d.b * 16 + 16).iterator();

         while(var2.hasNext()) {
            TileEntity var3 = (TileEntity)var2.next();
            Packet var4 = var3.D_();
            if(var4 != null) {
               var1.a.a(var4);
            }
         }

         this.b.a().v().a(var1, this.f);
      }
   }

   public void c() {
      if(this.f != null) {
         this.f.c(this.f.x() + this.b.a().P() - this.i);
      }

      this.i = this.b.a().P();
   }

   public void a(int var1, int var2, int var3) {
      if(this.j) {
         if(this.g == 0) {
            this.b.a(this);
         }

         this.h |= 1 << (var2 >> 4);
         if(this.g < 64) {
            short var4 = (short)(var1 << 12 | var3 << 8 | var2);

            for(int var5 = 0; var5 < this.g; ++var5) {
               if(this.e[var5] == var4) {
                  return;
               }
            }

            this.e[this.g++] = var4;
         }

      }
   }

   public void a(Packet var1) {
      if(this.j) {
         for(int var2 = 0; var2 < this.c.size(); ++var2) {
            ((EntityPlayer)this.c.get(var2)).a.a(var1);
         }

      }
   }

   public void d() {
      if(this.j && this.f != null) {
         if(this.g != 0) {
            int var1;
            int var2;
            int var3;
            if(this.g == 1) {
               var1 = (this.e[0] >> 12 & 15) + this.d.a * 16;
               var2 = this.e[0] & 255;
               var3 = (this.e[0] >> 8 & 15) + this.d.b * 16;
               BlockPosition var4 = new BlockPosition(var1, var2, var3);
               this.a((Packet)(new PacketPlayOutBlockChange(this.b.a(), var4)));
               if(this.b.a().getType(var4).getBlock().m()) {
                  this.a(this.b.a().r(var4));
               }
            } else {
               int var7;
               if(this.g == 64) {
                  var1 = this.d.a * 16;
                  var2 = this.d.b * 16;
                  this.a((Packet)(new PacketPlayOutMapChunk(this.f, false, this.h)));

                  for(var3 = 0; var3 < 16; ++var3) {
                     if((this.h & 1 << var3) != 0) {
                        var7 = var3 << 4;
                        List var5 = this.b.a().a(var1, var7, var2, var1 + 16, var7 + 16, var2 + 16);

                        for(int var6 = 0; var6 < var5.size(); ++var6) {
                           this.a((TileEntity)var5.get(var6));
                        }
                     }
                  }
               } else {
                  this.a((Packet)(new PacketPlayOutMultiBlockChange(this.g, this.e, this.f)));

                  for(var1 = 0; var1 < this.g; ++var1) {
                     var2 = (this.e[var1] >> 12 & 15) + this.d.a * 16;
                     var3 = this.e[var1] & 255;
                     var7 = (this.e[var1] >> 8 & 15) + this.d.b * 16;
                     BlockPosition var8 = new BlockPosition(var2, var3, var7);
                     if(this.b.a().getType(var8).getBlock().m()) {
                        this.a(this.b.a().r(var8));
                     }
                  }
               }
            }

            this.g = 0;
            this.h = 0;
         }
      }
   }

   private void a(TileEntity var1) {
      if(var1 != null) {
         Packet var2 = var1.D_();
         if(var2 != null) {
            this.a(var2);
         }
      }

   }

   public boolean d(EntityPlayer var1) {
      return this.c.contains(var1);
   }

   public boolean a(Predicate var1) {
      return Iterables.tryFind(this.c, var1).isPresent();
   }

   public boolean a(double var1, Predicate var3) {
      int var4 = 0;

      for(int var5 = this.c.size(); var4 < var5; ++var4) {
         EntityPlayer var6 = (EntityPlayer)this.c.get(var4);
         if(var3.apply(var6) && this.d.a(var6) < var1 * var1) {
            return true;
         }
      }

      return false;
   }

   public boolean e() {
      return this.j;
   }

   public Chunk f() {
      return this.f;
   }

   public double g() {
      double var1 = Double.MAX_VALUE;
      Iterator var3 = this.c.iterator();

      while(var3.hasNext()) {
         EntityPlayer var4 = (EntityPlayer)var3.next();
         double var5 = this.d.a(var4);
         if(var5 < var1) {
            var1 = var5;
         }
      }

      return var1;
   }
}
