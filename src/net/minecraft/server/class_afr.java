package net.minecraft.server;

import net.minecraft.server.InventoryCrafting;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_afw;
import net.minecraft.server.class_ayy;

public class class_afr extends class_afw {
   public class_afr() {
      super(3, 3, new ItemStack[]{new ItemStack(Items.aR), new ItemStack(Items.aR), new ItemStack(Items.aR), new ItemStack(Items.aR), new ItemStack(Items.bk, 0, 32767), new ItemStack(Items.aR), new ItemStack(Items.aR), new ItemStack(Items.aR), new ItemStack(Items.aR)}, new ItemStack(Items.cf, 0, 0));
   }

   public boolean a(InventoryCrafting var1, World var2) {
      if(!super.a(var1, var2)) {
         return false;
      } else {
         ItemStack var3 = null;

         for(int var4 = 0; var4 < var1.u_() && var3 == null; ++var4) {
            ItemStack var5 = var1.a(var4);
            if(var5 != null && var5.b() == Items.bk) {
               var3 = var5;
            }
         }

         if(var3 == null) {
            return false;
         } else {
            class_ayy var6 = Items.bk.a(var3, var2);
            return var6 == null?false:var6.f < 4;
         }
      }
   }

   public ItemStack a(InventoryCrafting var1) {
      ItemStack var2 = null;

      for(int var3 = 0; var3 < var1.u_() && var2 == null; ++var3) {
         ItemStack var4 = var1.a(var3);
         if(var4 != null && var4.b() == Items.bk) {
            var2 = var4;
         }
      }

      var2 = var2.k();
      var2.b = 1;
      if(var2.o() == null) {
         var2.d(new NBTTagCompound());
      }

      var2.o().a("map_scale_direction", (int)1);
      return var2;
   }
}
