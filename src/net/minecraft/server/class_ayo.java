package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.Vec3D;
import net.minecraft.server.class_aym;

public class class_ayo {
   private final class_aym[] a;
   private class_aym[] b = new class_aym[0];
   private class_aym[] c = new class_aym[0];
   private int e;
   private int f;

   public class_ayo(class_aym[] var1) {
      this.a = var1;
      this.f = var1.length;
   }

   public void a() {
      ++this.e;
   }

   public boolean b() {
      return this.e >= this.f;
   }

   public class_aym c() {
      return this.f > 0?this.a[this.f - 1]:null;
   }

   public class_aym a(int var1) {
      return this.a[var1];
   }

   public void a(int var1, class_aym var2) {
      this.a[var1] = var2;
   }

   public int d() {
      return this.f;
   }

   public void b(int var1) {
      this.f = var1;
   }

   public int e() {
      return this.e;
   }

   public void c(int var1) {
      this.e = var1;
   }

   public Vec3D a(Entity var1, int var2) {
      double var3 = (double)this.a[var2].a + (double)((int)(var1.width + 1.0F)) * 0.5D;
      double var5 = (double)this.a[var2].b;
      double var7 = (double)this.a[var2].c + (double)((int)(var1.width + 1.0F)) * 0.5D;
      return new Vec3D(var3, var5, var7);
   }

   public Vec3D a(Entity var1) {
      return this.a(var1, this.e);
   }

   public Vec3D f() {
      class_aym var1 = this.a[this.e];
      return new Vec3D((double)var1.a, (double)var1.b, (double)var1.c);
   }

   public boolean a(class_ayo var1) {
      if(var1 == null) {
         return false;
      } else if(var1.a.length != this.a.length) {
         return false;
      } else {
         for(int var2 = 0; var2 < this.a.length; ++var2) {
            if(this.a[var2].a != var1.a[var2].a || this.a[var2].b != var1.a[var2].b || this.a[var2].c != var1.a[var2].c) {
               return false;
            }
         }

         return true;
      }
   }

   public boolean b(Vec3D var1) {
      class_aym var2 = this.c();
      return var2 == null?false:var2.a == (int)var1.b && var2.c == (int)var1.d;
   }
}
