package net.minecraft.server;

import java.io.IOException;
import java.util.UUID;
import net.minecraft.server.Entity;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketListenerPlayOut;
import net.minecraft.server.MathHelper;

/*TODO: Packet changed*/
public class PacketPlayOutSpawnEntity implements Packet {
   private int a;
   private UUID b;
   private double c;
   private double d;
   private double e;
   private int f;
   private int g;
   private int h;
   private int i;
   private int j;
   private int k;
   private int l;

   public PacketPlayOutSpawnEntity() {
   }

   public PacketPlayOutSpawnEntity(Entity var1, int var2) {
      this(var1, var2, 0);
   }

   public PacketPlayOutSpawnEntity(Entity var1, int var2, int var3) {
      this.a = var1.getId();
      this.b = var1.getUniqueId();
      this.c = var1.locX;
      this.d = var1.locY;
      this.e = var1.locZ;
      this.i = MathHelper.d(var1.pitch * 256.0F / 360.0F);
      this.j = MathHelper.d(var1.yaw * 256.0F / 360.0F);
      this.k = var2;
      this.l = var3;
      double var4 = 3.9D;
      this.f = (int)(MathHelper.a(var1.motX, -3.9D, 3.9D) * 8000.0D);
      this.g = (int)(MathHelper.a(var1.motY, -3.9D, 3.9D) * 8000.0D);
      this.h = (int)(MathHelper.a(var1.motZ, -3.9D, 3.9D) * 8000.0D);
   }

   public PacketPlayOutSpawnEntity(Entity var1, int var2, int var3, BlockPosition var4) {
      this(var1, var2, var3);
      this.c = (double)var4.p();
      this.d = (double)var4.q();
      this.e = (double)var4.r();
   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.readVarInt();
      this.b = var1.i();
      this.k = var1.readByte();
      this.c = var1.readDouble();
      this.d = var1.readDouble();
      this.e = var1.readDouble();
      this.i = var1.readByte();
      this.j = var1.readByte();
      this.l = var1.readInt();
      this.f = var1.readShort();
      this.g = var1.readShort();
      this.h = var1.readShort();
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.writeVarInt(this.a);
      var1.a(this.b);
      var1.writeByte(this.k);
      var1.writeDouble(this.c);
      var1.writeDouble(this.d);
      var1.writeDouble(this.e);
      var1.writeByte(this.i);
      var1.writeByte(this.j);
      var1.writeInt(this.l);
      var1.writeShort(this.f);
      var1.writeShort(this.g);
      var1.writeShort(this.h);
   }

   public void a(PacketListenerPlayOut var1) {
      var1.a(this);
   }

   public void a(int var1) {
      this.f = var1;
   }

   public void b(int var1) {
      this.g = var1;
   }

   public void c(int var1) {
      this.h = var1;
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketListenerPlayOut)var1);
   }
}
