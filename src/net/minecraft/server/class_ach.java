package net.minecraft.server;

import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.class_adj;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_rz;

public class class_ach extends class_adj {
   public class_ach(int var1) {
      super(var1, false);
      this.d(1);
   }

   public ItemStack a(ItemStack var1, World var2, class_rz var3) {
      super.a(var1, var2, var3);
      return new ItemStack(Items.B);
   }
}
