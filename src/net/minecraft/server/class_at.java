package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;

public class class_at extends CommandAbstract {
   public String c() {
      return "particle";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.particle.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 8) {
         throw new class_cf("commands.particle.usage", new Object[0]);
      } else {
         boolean var4 = false;
         EnumParticle var5 = EnumParticle.a(var3[0]);
         if(var5 == null) {
            throw new class_bz("commands.particle.notFound", new Object[]{var3[0]});
         } else {
            String var6 = var3[0];
            Vec3D var7 = var2.d();
            double var8 = (double)((float)b(var7.b, var3[1], true));
            double var10 = (double)((float)b(var7.c, var3[2], true));
            double var12 = (double)((float)b(var7.d, var3[3], true));
            double var14 = (double)((float)c(var3[4]));
            double var16 = (double)((float)c(var3[5]));
            double var18 = (double)((float)c(var3[6]));
            double var20 = (double)((float)c(var3[7]));
            int var22 = 0;
            if(var3.length > 8) {
               var22 = a(var3[8], 0);
            }

            boolean var23 = false;
            if(var3.length > 9 && "force".equals(var3[9])) {
               var23 = true;
            }

            EntityPlayer var24;
            if(var3.length > 10) {
               var24 = a(var1, var2, var3[10]);
            } else {
               var24 = null;
            }

            int[] var25 = new int[var5.d()];

            for(int var26 = 0; var26 < var25.length; ++var26) {
               if(var3.length > 11 + var26) {
                  try {
                     var25[var26] = Integer.parseInt(var3[11 + var26]);
                  } catch (NumberFormatException var28) {
                     throw new class_bz("commands.particle.invalidParam", new Object[]{var3[11 + var26]});
                  }
               }
            }

            World var29 = var2.e();
            if(var29 instanceof WorldServer) {
               WorldServer var27 = (WorldServer)var29;
               if(var24 == null) {
                  var27.a(var5, var23, var8, var10, var12, var22, var14, var16, var18, var20, var25);
               } else {
                  var27.a(var24, var5, var23, var8, var10, var12, var22, var14, var16, var18, var20, var25);
               }

               a(var2, this, "commands.particle.success", new Object[]{var6, Integer.valueOf(Math.max(var22, 1))});
            }

         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, EnumParticle.a()):(var3.length > 1 && var3.length <= 4?a(var3, 1, var4):(var3.length == 10?a(var3, new String[]{"normal", "force"}):(var3.length == 11?a(var3, var1.J()):Collections.emptyList())));
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 10;
   }
}
