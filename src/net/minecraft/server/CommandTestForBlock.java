package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.Block;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.GameProfileSerializer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MojangsonParser;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cb;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ec;
import net.minecraft.server.ICommandListener;

public class CommandTestForBlock extends CommandAbstract {
   public String c() {
      return "testforblock";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.testforblock.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 4) {
         throw new class_cf("commands.testforblock.usage", new Object[0]);
      } else {
         var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_BLOCKS, 0);
         BlockPosition var4 = a(var2, var3, 0, false);
         Block var5 = Block.b(var3[3]);
         if(var5 == null) {
            throw new class_cb("commands.setblock.notFound", new Object[]{var3[3]});
         } else {
            int var6 = -1;
            if(var3.length >= 5) {
               var6 = a(var3[4], -1, 15);
            }

            World var7 = var2.e();
            if(!var7.e(var4)) {
               throw new class_bz("commands.testforblock.outOfWorld", new Object[0]);
            } else {
               NBTTagCompound var8 = new NBTTagCompound();
               boolean var9 = false;
               if(var3.length >= 6 && var5.m()) {
                  String var10 = a(var2, var3, 5).c();

                  try {
                     var8 = MojangsonParser.a(var10);
                     var9 = true;
                  } catch (class_ec var14) {
                     throw new class_bz("commands.setblock.tagError", new Object[]{var14.getMessage()});
                  }
               }

               IBlockData var15 = var7.getType(var4);
               Block var11 = var15.getBlock();
               if(var11 != var5) {
                  throw new class_bz("commands.testforblock.failed.tile", new Object[]{Integer.valueOf(var4.p()), Integer.valueOf(var4.q()), Integer.valueOf(var4.r()), var11.c(), var5.c()});
               } else {
                  if(var6 > -1) {
                     int var12 = var15.getBlock().e(var15);
                     if(var12 != var6) {
                        throw new class_bz("commands.testforblock.failed.data", new Object[]{Integer.valueOf(var4.p()), Integer.valueOf(var4.q()), Integer.valueOf(var4.r()), Integer.valueOf(var12), Integer.valueOf(var6)});
                     }
                  }

                  if(var9) {
                     TileEntity var16 = var7.r(var4);
                     if(var16 == null) {
                        throw new class_bz("commands.testforblock.failed.tileEntity", new Object[]{Integer.valueOf(var4.p()), Integer.valueOf(var4.q()), Integer.valueOf(var4.r())});
                     }

                     NBTTagCompound var13 = new NBTTagCompound();
                     var16.a(var13);
                     if(!GameProfileSerializer.a(var8, var13, true)) {
                        throw new class_bz("commands.testforblock.failed.nbt", new Object[]{Integer.valueOf(var4.p()), Integer.valueOf(var4.q()), Integer.valueOf(var4.r())});
                     }
                  }

                  var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_BLOCKS, 1);
                  a(var2, this, "commands.testforblock.success", new Object[]{Integer.valueOf(var4.p()), Integer.valueOf(var4.q()), Integer.valueOf(var4.r())});
               }
            }
         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length > 0 && var3.length <= 3?a(var3, 0, var4):(var3.length == 4?a(var3, Block.h.c()):Collections.emptyList());
   }
}
