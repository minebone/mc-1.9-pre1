package net.minecraft.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import net.minecraft.server.NBTCompressedStreamTools;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.WorldData;
import net.minecraft.server.WorldNBTStorage;
import net.minecraft.server.class_azh;
import net.minecraft.server.class_azj;
import net.minecraft.server.class_op;
import net.minecraft.server.class_oy;
import net.minecraft.server.class_oz;
import net.minecraft.server.class_pb;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_azf implements class_azj {
   private static final Logger c = LogManager.getLogger();
   protected final File a;
   protected final class_pb b;

   public class_azf(File var1, class_pb var2) {
      this.b = var2;
      if(!var1.exists()) {
         var1.mkdirs();
      }

      this.a = var1;
   }

   public WorldData c(String var1) {
      File var2 = new File(this.a, var1);
      if(!var2.exists()) {
         return null;
      } else {
         File var3 = new File(var2, "level.dat");
         if(var3.exists()) {
            WorldData var4 = a(var3, this.b);
            if(var4 != null) {
               return var4;
            }
         }

         var3 = new File(var2, "level.dat_old");
         return var3.exists()?a(var3, this.b):null;
      }
   }

   public static WorldData a(File var0, class_pb var1) {
      try {
         NBTTagCompound var2 = NBTCompressedStreamTools.a((InputStream)(new FileInputStream(var0)));
         NBTTagCompound var3 = var2.o("Data");
         return new WorldData(var1.a((class_oy)class_oz.LEVEL, (NBTTagCompound)var3));
      } catch (Exception var4) {
         c.error((String)("Exception reading " + var0), (Throwable)var4);
         return null;
      }
   }

   public class_azh a(String var1, boolean var2) {
      return new WorldNBTStorage(this.a, var1, var2, this.b);
   }

   public boolean b(String var1) {
      return false;
   }

   public boolean a(String var1, class_op var2) {
      return false;
   }

   public File b(String var1, String var2) {
      return new File(new File(this.a, var1), var2);
   }
}
