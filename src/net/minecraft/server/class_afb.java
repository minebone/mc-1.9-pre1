package net.minecraft.server;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;

public class class_afb extends Item {
   public class_afb() {
      this.d(1);
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      var3.a(var1, var4);
      var3.b(StatisticList.b((Item)this));
      return new class_qo(EnumResult.SUCCESS, var1);
   }

   public static boolean b(NBTTagCompound var0) {
      if(var0 == null) {
         return false;
      } else if(!var0.b("pages", 9)) {
         return false;
      } else {
         NBTTagList var1 = var0.c("pages", 8);

         for(int var2 = 0; var2 < var1.c(); ++var2) {
            String var3 = var1.g(var2);
            if(var3 == null) {
               return false;
            }

            if(var3.length() > 32767) {
               return false;
            }
         }

         return true;
      }
   }
}
