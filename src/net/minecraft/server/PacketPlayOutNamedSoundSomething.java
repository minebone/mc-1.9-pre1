package net.minecraft.server;

import java.io.IOException;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketListenerPlayOut;
import net.minecraft.server.class_nf;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;
import org.apache.commons.lang3.Validate;

/*TODO: new packet*/
public class PacketPlayOutNamedSoundSomething implements Packet {
   private class_nf a;
   private EnumSoundCategory b;
   private int c;
   private int d;
   private int e;
   private float f;
   private int g;

   public PacketPlayOutNamedSoundSomething() {
   }

   public PacketPlayOutNamedSoundSomething(class_nf var1, EnumSoundCategory var2, double var3, double var5, double var7, float var9, float var10) {
      Validate.notNull(var1, "sound", new Object[0]);
      this.a = var1;
      this.b = var2;
      this.c = (int)(var3 * 8.0D);
      this.d = (int)(var5 * 8.0D);
      this.e = (int)(var7 * 8.0D);
      this.f = var9;
      this.g = (int)(var10 * 63.5F);
      var10 = MathHelper.a(var10, 0.0F, 255.0F);
   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = (class_nf)class_nf.a.a(var1.readVarInt());
      this.b = (EnumSoundCategory)var1.a(EnumSoundCategory.class);
      this.c = var1.readInt();
      this.d = var1.readInt();
      this.e = var1.readInt();
      this.f = var1.readFloat();
      this.g = var1.readUnsignedByte();
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.writeVarInt(class_nf.a.a(this.a));
      var1.a((Enum)this.b);
      var1.writeInt(this.c);
      var1.writeInt(this.d);
      var1.writeInt(this.e);
      var1.writeFloat(this.f);
      var1.writeByte(this.g);
   }

   public void a(PacketListenerPlayOut var1) {
      var1.a(this);
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketListenerPlayOut)var1);
   }
}
