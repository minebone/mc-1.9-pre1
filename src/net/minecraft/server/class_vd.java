package net.minecraft.server;

import java.util.Iterator;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.NavigationAbstract;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.class_ayl;
import net.minecraft.server.class_aym;
import net.minecraft.server.class_ayo;
import net.minecraft.server.class_ayp;
import net.minecraft.server.class_ayr;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;

public class class_vd extends NavigationAbstract {
   private boolean f;

   public class_vd(EntityInsentient var1, World var2) {
      super(var1, var2);
   }

   protected class_ayp a() {
      this.e = new class_ayr();
      this.e.a(true);
      return new class_ayp(this.e);
   }

   protected boolean b() {
      return this.a.onGround || this.g() && this.p() || this.a.aH();
   }

   protected Vec3D c() {
      return new Vec3D(this.a.locX, (double)this.r(), this.a.locZ);
   }

   public class_ayo a(BlockPosition var1) {
      BlockPosition var2;
      if(this.b.getType(var1).getMaterial() == Material.a) {
         for(var2 = var1.b(); var2.q() > 0 && this.b.getType(var2).getMaterial() == Material.a; var2 = var2.b()) {
            ;
         }

         if(var2.q() > 0) {
            return super.a(var2.a());
         }

         while(var2.q() < this.b.Y() && this.b.getType(var2).getMaterial() == Material.a) {
            var2 = var2.a();
         }

         var1 = var2;
      }

      if(!this.b.getType(var1).getMaterial().a()) {
         return super.a(var1);
      } else {
         for(var2 = var1.a(); var2.q() < this.b.Y() && this.b.getType(var2).getMaterial().a(); var2 = var2.a()) {
            ;
         }

         return super.a(var2);
      }
   }

   public class_ayo a(Entity var1) {
      BlockPosition var2 = new BlockPosition(var1);
      return this.a(var2);
   }

   private int r() {
      if(this.a.ah() && this.g()) {
         int var1 = (int)this.a.bk().b;
         Block var2 = this.b.getType(new BlockPosition(MathHelper.c(this.a.locX), var1, MathHelper.c(this.a.locZ))).getBlock();
         int var3 = 0;

         do {
            if(var2 != Blocks.i && var2 != Blocks.j) {
               return var1;
            }

            ++var1;
            var2 = this.b.getType(new BlockPosition(MathHelper.c(this.a.locX), var1, MathHelper.c(this.a.locZ))).getBlock();
            ++var3;
         } while(var3 <= 16);

         return (int)this.a.bk().b;
      } else {
         return (int)(this.a.bk().b + 0.5D);
      }
   }

   protected void d() {
      super.d();

      int var1;
      class_aym var2;
      for(var1 = 0; var1 < this.c.d(); ++var1) {
         var2 = this.c.a(var1);
         class_aym var3 = var1 + 1 < this.c.d()?this.c.a(var1 + 1):null;
         IBlockData var4 = this.b.getType(new BlockPosition(var2.a, var2.b, var2.c));
         Block var5 = var4.getBlock();
         if(var5 == Blocks.bE) {
            this.c.a(var1, var2.a(var2.a, var2.b + 1, var2.c));
            if(var3 != null && var2.b >= var3.b) {
               this.c.a(var1 + 1, var3.a(var3.a, var2.b + 1, var3.c));
            }
         }
      }

      if(this.f) {
         if(this.b.h(new BlockPosition(MathHelper.c(this.a.locX), (int)(this.a.bk().b + 0.5D), MathHelper.c(this.a.locZ)))) {
            return;
         }

         for(var1 = 0; var1 < this.c.d(); ++var1) {
            var2 = this.c.a(var1);
            if(this.b.h(new BlockPosition(var2.a, var2.b, var2.c))) {
               this.c.b(var1 - 1);
               return;
            }
         }
      }

   }

   protected boolean a(Vec3D var1, Vec3D var2, int var3, int var4, int var5) {
      int var6 = MathHelper.c(var1.b);
      int var7 = MathHelper.c(var1.d);
      double var8 = var2.b - var1.b;
      double var10 = var2.d - var1.d;
      double var12 = var8 * var8 + var10 * var10;
      if(var12 < 1.0E-8D) {
         return false;
      } else {
         double var14 = 1.0D / Math.sqrt(var12);
         var8 *= var14;
         var10 *= var14;
         var3 += 2;
         var5 += 2;
         if(!this.a(var6, (int)var1.c, var7, var3, var4, var5, var1, var8, var10)) {
            return false;
         } else {
            var3 -= 2;
            var5 -= 2;
            double var16 = 1.0D / Math.abs(var8);
            double var18 = 1.0D / Math.abs(var10);
            double var20 = (double)var6 - var1.b;
            double var22 = (double)var7 - var1.d;
            if(var8 >= 0.0D) {
               ++var20;
            }

            if(var10 >= 0.0D) {
               ++var22;
            }

            var20 /= var8;
            var22 /= var10;
            int var24 = var8 < 0.0D?-1:1;
            int var25 = var10 < 0.0D?-1:1;
            int var26 = MathHelper.c(var2.b);
            int var27 = MathHelper.c(var2.d);
            int var28 = var26 - var6;
            int var29 = var27 - var7;

            do {
               if(var28 * var24 <= 0 && var29 * var25 <= 0) {
                  return true;
               }

               if(var20 < var22) {
                  var20 += var16;
                  var6 += var24;
                  var28 = var26 - var6;
               } else {
                  var22 += var18;
                  var7 += var25;
                  var29 = var27 - var7;
               }
            } while(this.a(var6, (int)var1.c, var7, var3, var4, var5, var1, var8, var10));

            return false;
         }
      }
   }

   private boolean a(int var1, int var2, int var3, int var4, int var5, int var6, Vec3D var7, double var8, double var10) {
      int var12 = var1 - var4 / 2;
      int var13 = var3 - var6 / 2;
      if(!this.b(var12, var2, var13, var4, var5, var6, var7, var8, var10)) {
         return false;
      } else {
         for(int var14 = var12; var14 < var12 + var4; ++var14) {
            for(int var15 = var13; var15 < var13 + var6; ++var15) {
               double var16 = (double)var14 + 0.5D - var7.b;
               double var18 = (double)var15 + 0.5D - var7.d;
               if(var16 * var8 + var18 * var10 >= 0.0D) {
                  class_ayl var20 = this.e.a(this.b, var14, var2 - 1, var15, this.a, var4, var5, var6, true, true);
                  if(var20 == class_ayl.WATER) {
                     return false;
                  }

                  if(var20 == class_ayl.LAVA) {
                     return false;
                  }

                  if(var20 == class_ayl.OPEN) {
                     return false;
                  }

                  var20 = this.e.a(this.b, var14, var2, var15, this.a, var4, var5, var6, true, true);
                  float var21 = this.a.a(var20);
                  if(var21 < 0.0F || var21 >= 8.0F) {
                     return false;
                  }

                  if(var20 == class_ayl.DAMAGE_FIRE || var20 == class_ayl.DANGER_FIRE || var20 == class_ayl.DAMAGE_OTHER) {
                     return false;
                  }
               }
            }
         }

         return true;
      }
   }

   private boolean b(int var1, int var2, int var3, int var4, int var5, int var6, Vec3D var7, double var8, double var10) {
      Iterator var12 = BlockPosition.a(new BlockPosition(var1, var2, var3), new BlockPosition(var1 + var4 - 1, var2 + var5 - 1, var3 + var6 - 1)).iterator();

      while(var12.hasNext()) {
         BlockPosition var13 = (BlockPosition)var12.next();
         double var14 = (double)var13.p() + 0.5D - var7.b;
         double var16 = (double)var13.r() + 0.5D - var7.d;
         if(var14 * var8 + var16 * var10 >= 0.0D) {
            Block var18 = this.b.getType(var13).getBlock();
            if(!var18.b(this.b, var13)) {
               return false;
            }
         }
      }

      return true;
   }

   public void a(boolean var1) {
      this.e.b(var1);
   }

   public void b(boolean var1) {
      this.e.a(var1);
   }

   public boolean f() {
      return this.e.c();
   }

   public void c(boolean var1) {
      this.e.c(var1);
   }

   public boolean g() {
      return this.e.e();
   }

   public void d(boolean var1) {
      this.f = var1;
   }
}
