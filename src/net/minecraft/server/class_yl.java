package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AchievementList;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityLargeFireball;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.Statistic;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rx;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sx;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_uw;
import net.minecraft.server.class_yk;
import net.minecraft.server.class_ys;

public class class_yl extends class_rx implements class_yk {
   private static final class_ke a = DataWatcher.a(class_yl.class, class_kg.h);
   private int b = 1;

   public class_yl(World var1) {
      super(var1);
      this.a(4.0F, 4.0F);
      this.fireProof = true;
      this.b_ = 5;
      this.f = new class_yl.class_b_in_class_yl(this);
   }

   protected void r() {
      this.bp.a(5, new class_yl.class_d_in_class_yl(this));
      this.bp.a(7, new class_yl.class_a_in_class_yl(this));
      this.bp.a(7, new class_yl.class_c_in_class_yl(this));
      this.bq.a(1, new class_uw(this));
   }

   public void a(boolean var1) {
      this.datawatcher.b(a, Boolean.valueOf(var1));
   }

   public int cT() {
      return this.b;
   }

   public void m() {
      super.m();
      if(!this.world.E && this.world.ae() == EnumDifficulty.PEACEFUL) {
         this.S();
      }

   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b(var1)) {
         return false;
      } else if("fireball".equals(var1.p()) && var1.j() instanceof EntityHuman) {
         super.a(var1, 1000.0F);
         ((EntityHuman)var1.j()).b((Statistic)AchievementList.z);
         return true;
      } else {
         return super.a(var1, var2);
      }
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)a, (Object)Boolean.valueOf(false));
   }

   protected void bz() {
      super.bz();
      this.a(class_ys.a).a(10.0D);
      this.a(class_ys.b).a(100.0D);
   }

   public EnumSoundCategory by() {
      return EnumSoundCategory.HOSTILE;
   }

   protected class_nf G() {
      return class_ng.bH;
   }

   protected class_nf bQ() {
      return class_ng.bJ;
   }

   protected class_nf bR() {
      return class_ng.bI;
   }

   protected class_kk J() {
      return class_azs.ae;
   }

   protected float cc() {
      return 10.0F;
   }

   public boolean cF() {
      return this.random.nextInt(20) == 0 && super.cF() && this.world.ae() != EnumDifficulty.PEACEFUL;
   }

   public int cJ() {
      return 1;
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("ExplosionPower", this.b);
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      if(var1.b("ExplosionPower", 99)) {
         this.b = var1.h("ExplosionPower");
      }

   }

   public float bm() {
      return 2.6F;
   }

   static class class_c_in_class_yl extends class_tj {
      private class_yl b;
      public int a;

      public class_c_in_class_yl(class_yl var1) {
         this.b = var1;
      }

      public boolean a() {
         return this.b.A() != null;
      }

      public void c() {
         this.a = 0;
      }

      public void d() {
         this.b.a(false);
      }

      public void e() {
         class_rz var1 = this.b.A();
         double var2 = 64.0D;
         if(var1.h(this.b) < var2 * var2 && this.b.D(var1)) {
            World var4 = this.b.world;
            ++this.a;
            if(this.a == 10) {
               var4.a((EntityHuman)null, 1015, new BlockPosition(this.b), 0);
            }

            if(this.a == 20) {
               double var5 = 4.0D;
               Vec3D var7 = this.b.f(1.0F);
               double var8 = var1.locX - (this.b.locX + var7.b * var5);
               double var10 = var1.bk().b + (double)(var1.length / 2.0F) - (0.5D + this.b.locY + (double)(this.b.length / 2.0F));
               double var12 = var1.locZ - (this.b.locZ + var7.d * var5);
               var4.a((EntityHuman)null, 1016, new BlockPosition(this.b), 0);
               EntityLargeFireball var14 = new EntityLargeFireball(var4, this.b, var8, var10, var12);
               var14.e = this.b.cT();
               var14.locX = this.b.locX + var7.b * var5;
               var14.locY = this.b.locY + (double)(this.b.length / 2.0F) + 0.5D;
               var14.locZ = this.b.locZ + var7.d * var5;
               var4.a((Entity)var14);
               this.a = -40;
            }
         } else if(this.a > 0) {
            --this.a;
         }

         this.b.a(this.a > 10);
      }
   }

   static class class_a_in_class_yl extends class_tj {
      private class_yl a;

      public class_a_in_class_yl(class_yl var1) {
         this.a = var1;
         this.a(2);
      }

      public boolean a() {
         return true;
      }

      public void e() {
         if(this.a.A() == null) {
            this.a.aM = this.a.yaw = -((float)MathHelper.b(this.a.motX, this.a.motZ)) * 57.295776F;
         } else {
            class_rz var1 = this.a.A();
            double var2 = 64.0D;
            if(var1.h(this.a) < var2 * var2) {
               double var4 = var1.locX - this.a.locX;
               double var6 = var1.locZ - this.a.locZ;
               this.a.aM = this.a.yaw = -((float)MathHelper.b(var4, var6)) * 57.295776F;
            }
         }

      }
   }

   static class class_d_in_class_yl extends class_tj {
      private class_yl a;

      public class_d_in_class_yl(class_yl var1) {
         this.a = var1;
         this.a(1);
      }

      public boolean a() {
         class_sx var1 = this.a.u();
         if(!var1.a()) {
            return true;
         } else {
            double var2 = var1.d() - this.a.locX;
            double var4 = var1.e() - this.a.locY;
            double var6 = var1.f() - this.a.locZ;
            double var8 = var2 * var2 + var4 * var4 + var6 * var6;
            return var8 < 1.0D || var8 > 3600.0D;
         }
      }

      public boolean b() {
         return false;
      }

      public void c() {
         Random var1 = this.a.bE();
         double var2 = this.a.locX + (double)((var1.nextFloat() * 2.0F - 1.0F) * 16.0F);
         double var4 = this.a.locY + (double)((var1.nextFloat() * 2.0F - 1.0F) * 16.0F);
         double var6 = this.a.locZ + (double)((var1.nextFloat() * 2.0F - 1.0F) * 16.0F);
         this.a.u().a(var2, var4, var6, 1.0D);
      }
   }

   static class class_b_in_class_yl extends class_sx {
      private class_yl i;
      private int j;

      public class_b_in_class_yl(class_yl var1) {
         super(var1);
         this.i = var1;
      }

      public void c() {
         if(this.h == class_sx.class_a_in_class_sx.MOVE_TO) {
            double var1 = this.b - this.i.locX;
            double var3 = this.c - this.i.locY;
            double var5 = this.d - this.i.locZ;
            double var7 = var1 * var1 + var3 * var3 + var5 * var5;
            if(this.j-- <= 0) {
               this.j += this.i.bE().nextInt(5) + 2;
               var7 = (double)MathHelper.a(var7);
               if(this.b(this.b, this.c, this.d, var7)) {
                  this.i.motX += var1 / var7 * 0.1D;
                  this.i.motY += var3 / var7 * 0.1D;
                  this.i.motZ += var5 / var7 * 0.1D;
               } else {
                  this.h = class_sx.class_a_in_class_sx.WAIT;
               }
            }

         }
      }

      private boolean b(double var1, double var3, double var5, double var7) {
         double var9 = (var1 - this.i.locX) / var7;
         double var11 = (var3 - this.i.locY) / var7;
         double var13 = (var5 - this.i.locZ) / var7;
         AxisAlignedBB var15 = this.i.bk();

         for(int var16 = 1; (double)var16 < var7; ++var16) {
            var15 = var15.c(var9, var11, var13);
            if(!this.i.world.a((Entity)this.i, (AxisAlignedBB)var15).isEmpty()) {
               return false;
            }
         }

         return true;
      }
   }
}
