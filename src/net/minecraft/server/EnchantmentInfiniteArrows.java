package net.minecraft.server;

import net.minecraft.server.Enchantment;
import net.minecraft.server.class_agm;
import net.minecraft.server.EnumInventorySlot;

public class EnchantmentInfiniteArrows extends Enchantment {
   public EnchantmentInfiniteArrows(Enchantment.class_a_in_class_agl var1, EnumInventorySlot... var2) {
      super(var1, class_agm.BOW, var2);
      this.c("arrowInfinite");
   }

   public int a(int var1) {
      return 20;
   }

   public int b(int var1) {
      return 50;
   }

   public int b() {
      return 1;
   }
}
