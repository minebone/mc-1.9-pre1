package net.minecraft.server;

import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_ajz extends Block {
   public static final BlockStateInteger a = BlockStateInteger.a("age", 0, 15);
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.0625D, 0.0D, 0.0625D, 0.9375D, 0.9375D, 0.9375D);
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.0625D, 0.0D, 0.0625D, 0.9375D, 1.0D, 0.9375D);

   protected class_ajz() {
      super(Material.A);
      this.w(this.A.b().set(a, Integer.valueOf(0)));
      this.a(true);
      this.a(CreativeModeTab.c);
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      BlockPosition var5 = var2.a();
      if(var1.d(var5)) {
         int var6;
         for(var6 = 1; var1.getType(var2.c(var6)).getBlock() == this; ++var6) {
            ;
         }

         if(var6 < 3) {
            int var7 = ((Integer)var3.get(a)).intValue();
            if(var7 == 15) {
               var1.a(var5, this.u());
               IBlockData var8 = var3.set(a, Integer.valueOf(0));
               var1.a((BlockPosition)var2, (IBlockData)var8, 4);
               this.a(var1, var5, var8, (Block)this);
            } else {
               var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Integer.valueOf(var7 + 1)), 4);
            }

         }
      }
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return b;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean a(World var1, BlockPosition var2) {
      return super.a(var1, var2)?this.b(var1, var2):false;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!this.b(var1, var2)) {
         var1.b(var2, true);
      }

   }

   public boolean b(World var1, BlockPosition var2) {
      Iterator var3 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

      Material var5;
      do {
         if(!var3.hasNext()) {
            Block var6 = var1.getType(var2.b()).getBlock();
            return var6 == Blocks.aK || var6 == Blocks.m && !var1.getType(var2.a()).getMaterial().d();
         }

         EnumDirection var4 = (EnumDirection)var3.next();
         var5 = var1.getType(var2.a(var4)).getMaterial();
      } while(!var5.a() && var5 != Material.i);

      return false;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Entity var4) {
      var4.a(DamageSource.h, 1.0F);
   }

   public IBlockData a(int var1) {
      return this.u().set(a, Integer.valueOf(var1));
   }

   public int e(IBlockData var1) {
      return ((Integer)var1.get(a)).intValue();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }
}
