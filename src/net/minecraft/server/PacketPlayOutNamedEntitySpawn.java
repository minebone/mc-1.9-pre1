package net.minecraft.server;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketListenerPlayOut;

public class PacketPlayOutNamedEntitySpawn implements Packet {
   private int a;
   private UUID b;
   private double c;
   private double d;
   private double e;
   private byte f;
   private byte g;
   private DataWatcher h;
   private List i;

   public PacketPlayOutNamedEntitySpawn() {
   }

   public PacketPlayOutNamedEntitySpawn(EntityHuman var1) {
      this.a = var1.getId();
      this.b = var1.getProfile().getId();
      this.c = var1.locX;
      this.d = var1.locY;
      this.e = var1.locZ;
      this.f = (byte)((int)(var1.yaw * 256.0F / 360.0F));
      this.g = (byte)((int)(var1.pitch * 256.0F / 360.0F));
      this.h = var1.Q();
   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.readVarInt();
      this.b = var1.i();
      this.c = var1.readDouble();
      this.d = var1.readDouble();
      this.e = var1.readDouble();
      this.f = var1.readByte();
      this.g = var1.readByte();
      this.i = DataWatcher.b(var1);
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.writeVarInt(this.a);
      var1.a(this.b);
      var1.writeDouble(this.c);
      var1.writeDouble(this.d);
      var1.writeDouble(this.e);
      var1.writeByte(this.f);
      var1.writeByte(this.g);
      this.h.a(var1);
   }

   public void a(PacketListenerPlayOut var1) {
      var1.a(this);
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketListenerPlayOut)var1);
   }
}
