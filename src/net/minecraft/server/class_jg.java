package net.minecraft.server;

import java.io.IOException;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayInListener;
import net.minecraft.server.EnumHand;

public class class_jg implements Packet {
   private BlockPosition a;
   private EnumDirection b;
   private EnumHand c;
   private float d;
   private float e;
   private float f;

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.e();
      this.b = (EnumDirection)var1.a(EnumDirection.class);
      this.c = (EnumHand)var1.a(EnumHand.class);
      this.d = (float)var1.readUnsignedByte() / 16.0F;
      this.e = (float)var1.readUnsignedByte() / 16.0F;
      this.f = (float)var1.readUnsignedByte() / 16.0F;
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.a(this.a);
      var1.a((Enum)this.b);
      var1.a((Enum)this.c);
      var1.writeByte((int)(this.d * 16.0F));
      var1.writeByte((int)(this.e * 16.0F));
      var1.writeByte((int)(this.f * 16.0F));
   }

   public void a(PacketPlayInListener var1) {
      var1.a(this);
   }

   public BlockPosition a() {
      return this.a;
   }

   public EnumDirection b() {
      return this.b;
   }

   public EnumHand c() {
      return this.c;
   }

   public float d() {
      return this.d;
   }

   public float e() {
      return this.e;
   }

   public float f() {
      return this.f;
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketPlayInListener)var1);
   }
}
