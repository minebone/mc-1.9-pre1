package net.minecraft.server;

import net.minecraft.server.BiomeBase;
import net.minecraft.server.IntCache;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_axu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GenLayerRegionHills extends class_axu {
   private static final Logger c = LogManager.getLogger();
   private class_axu d;

   public GenLayerRegionHills(long var1, class_axu var3, class_axu var4) {
      super(var1);
      this.a = var3;
      this.d = var4;
   }

   public int[] a(int var1, int var2, int var3, int var4) {
      int[] var5 = this.a.a(var1 - 1, var2 - 1, var3 + 2, var4 + 2);
      int[] var6 = this.d.a(var1 - 1, var2 - 1, var3 + 2, var4 + 2);
      int[] var7 = IntCache.a(var3 * var4);

      for(int var8 = 0; var8 < var4; ++var8) {
         for(int var9 = 0; var9 < var3; ++var9) {
            this.a((long)(var9 + var1), (long)(var8 + var2));
            int var10 = var5[var9 + 1 + (var8 + 1) * (var3 + 2)];
            int var11 = var6[var9 + 1 + (var8 + 1) * (var3 + 2)];
            boolean var12 = (var11 - 2) % 29 == 0;
            if(var10 > 255) {
               c.debug("old! " + var10);
            }

            BiomeBase var13 = BiomeBase.a(var10);
            boolean var14 = var13 != null && var13.b();
            BiomeBase var15;
            if(var10 != 0 && var11 >= 2 && (var11 - 2) % 29 == 1 && !var14) {
               var15 = BiomeBase.b(var13);
               var7[var9 + var8 * var3] = var15 == null?var10:BiomeBase.a(var15);
            } else if(this.a(3) != 0 && !var12) {
               var7[var9 + var8 * var3] = var10;
            } else {
               var15 = var13;
               int var16;
               if(var13 == class_aik.d) {
                  var15 = class_aik.s;
               } else if(var13 == class_aik.f) {
                  var15 = class_aik.t;
               } else if(var13 == class_aik.C) {
                  var15 = class_aik.D;
               } else if(var13 == class_aik.E) {
                  var15 = class_aik.c;
               } else if(var13 == class_aik.g) {
                  var15 = class_aik.u;
               } else if(var13 == class_aik.H) {
                  var15 = class_aik.I;
               } else if(var13 == class_aik.F) {
                  var15 = class_aik.G;
               } else if(var13 == class_aik.c) {
                  if(this.a(3) == 0) {
                     var15 = class_aik.t;
                  } else {
                     var15 = class_aik.f;
                  }
               } else if(var13 == class_aik.n) {
                  var15 = class_aik.o;
               } else if(var13 == class_aik.w) {
                  var15 = class_aik.x;
               } else if(var13 == class_aik.a) {
                  var15 = class_aik.z;
               } else if(var13 == class_aik.e) {
                  var15 = class_aik.J;
               } else if(var13 == class_aik.K) {
                  var15 = class_aik.L;
               } else if(a(var10, BiomeBase.a(class_aik.N))) {
                  var15 = class_aik.M;
               } else if(var13 == class_aik.z && this.a(3) == 0) {
                  var16 = this.a(2);
                  if(var16 == 0) {
                     var15 = class_aik.c;
                  } else {
                     var15 = class_aik.f;
                  }
               }

               var16 = BiomeBase.a(var15);
               if(var12 && var16 != var10) {
                  BiomeBase var17 = BiomeBase.b(var15);
                  var16 = var17 == null?var10:BiomeBase.a(var17);
               }

               if(var16 == var10) {
                  var7[var9 + var8 * var3] = var10;
               } else {
                  int var22 = var5[var9 + 1 + (var8 + 0) * (var3 + 2)];
                  int var18 = var5[var9 + 2 + (var8 + 1) * (var3 + 2)];
                  int var19 = var5[var9 + 0 + (var8 + 1) * (var3 + 2)];
                  int var20 = var5[var9 + 1 + (var8 + 2) * (var3 + 2)];
                  int var21 = 0;
                  if(a(var22, var10)) {
                     ++var21;
                  }

                  if(a(var18, var10)) {
                     ++var21;
                  }

                  if(a(var19, var10)) {
                     ++var21;
                  }

                  if(a(var20, var10)) {
                     ++var21;
                  }

                  if(var21 >= 3) {
                     var7[var9 + var8 * var3] = var16;
                  } else {
                     var7[var9 + var8 * var3] = var10;
                  }
               }
            }
         }
      }

      return var7;
   }
}
