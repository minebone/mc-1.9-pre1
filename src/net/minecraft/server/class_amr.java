package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Item;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_aoa;
import net.minecraft.server.class_aqj;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_amr extends class_ajm {
   protected class_amr() {
      super(Material.e);
   }

   public TileEntity a(World var1, int var2) {
      return new class_aqj();
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return null;
   }

   public int a(Random var1) {
      return 0;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, float var4, int var5) {
      super.a(var1, var2, var3, var4, var5);
      int var6 = 15 + var1.r.nextInt(15) + var1.r.nextInt(15);
      this.b(var1, var2, var6);
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.MODEL;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return null;
   }
}
