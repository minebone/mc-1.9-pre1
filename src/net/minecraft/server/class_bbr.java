package net.minecraft.server;

import net.minecraft.server.EnumChatFormat;
import net.minecraft.server.IScoreboardCriteria;

public class class_bbr implements IScoreboardCriteria {
   private final String o;

   public class_bbr(String var1, EnumChatFormat var2) {
      this.o = var1 + var2.e();
      IScoreboardCriteria.a.put(this.o, this);
   }

   public String a() {
      return this.o;
   }

   public boolean b() {
      return false;
   }

   public IScoreboardCriteria.EnumScoreboardHealthDisplay c() {
      return IScoreboardCriteria.EnumScoreboardHealthDisplay.INTEGER;
   }
}
