package net.minecraft.server;

import net.minecraft.server.EntityCreature;
import net.minecraft.server.Vec3D;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_vl;

public class class_uf extends class_tj {
   private EntityCreature a;
   private double b;
   private double c;
   private double d;
   private double e;
   private int f;
   private boolean g;

   public class_uf(EntityCreature var1, double var2) {
      this(var1, var2, 120);
   }

   public class_uf(EntityCreature var1, double var2, int var4) {
      this.a = var1;
      this.e = var2;
      this.f = var4;
      this.a(1);
   }

   public boolean a() {
      if(!this.g) {
         if(this.a.bJ() >= 100) {
            return false;
         }

         if(this.a.bE().nextInt(this.f) != 0) {
            return false;
         }
      }

      Vec3D var1 = class_vl.a(this.a, 10, 7);
      if(var1 == null) {
         return false;
      } else {
         this.b = var1.b;
         this.c = var1.c;
         this.d = var1.d;
         this.g = false;
         return true;
      }
   }

   public boolean b() {
      return !this.a.x().n();
   }

   public void c() {
      this.a.x().a(this.b, this.c, this.d, this.e);
   }

   public void f() {
      this.g = true;
   }

   public void b(int var1) {
      this.f = var1;
   }
}
