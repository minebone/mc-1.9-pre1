package net.minecraft.server;

import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityProjectile;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.World;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_yf;

public class class_zv extends EntityProjectile {
   public class_zv(World var1) {
      super(var1);
   }

   public class_zv(World var1, class_rz var2) {
      super(var1, var2);
   }

   public class_zv(World var1, double var2, double var4, double var6) {
      super(var1, var2, var4, var6);
   }

   protected void a(MovingObjectPosition var1) {
      if(var1.d != null) {
         byte var2 = 0;
         if(var1.d instanceof class_yf) {
            var2 = 3;
         }

         var1.d.a(DamageSource.a((Entity)this, (Entity)this.k()), (float)var2);
      }

      for(int var3 = 0; var3 < 8; ++var3) {
         this.world.a(EnumParticle.SNOWBALL, this.locX, this.locY, this.locZ, 0.0D, 0.0D, 0.0D, new int[0]);
      }

      if(!this.world.E) {
         this.S();
      }

   }
}
