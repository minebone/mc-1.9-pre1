package net.minecraft.server;

import java.io.IOException;
import java.util.Collection;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.class_ayx;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketListenerPlayOut;

public class PacketPlayOutMap implements Packet {
   private int a;
   private byte b;
   private boolean c;
   private class_ayx[] d;
   private int e;
   private int f;
   private int g;
   private int h;
   private byte[] i;

   public PacketPlayOutMap() {
   }

   public PacketPlayOutMap(int var1, byte var2, boolean var3, Collection var4, byte[] var5, int var6, int var7, int var8, int var9) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = (class_ayx[])var4.toArray(new class_ayx[var4.size()]);
      this.e = var6;
      this.f = var7;
      this.g = var8;
      this.h = var9;
      this.i = new byte[var8 * var9];

      for(int var10 = 0; var10 < var8; ++var10) {
         for(int var11 = 0; var11 < var9; ++var11) {
            this.i[var10 + var11 * var8] = var5[var6 + var10 + (var7 + var11) * 128];
         }
      }

   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.readVarInt();
      this.b = var1.readByte();
      this.c = var1.readBoolean();
      this.d = new class_ayx[var1.readVarInt()];

      for(int var2 = 0; var2 < this.d.length; ++var2) {
         short var3 = (short)var1.readByte();
         this.d[var2] = new class_ayx((byte)(var3 >> 4 & 15), var1.readByte(), var1.readByte(), (byte)(var3 & 15));
      }

      this.g = var1.readUnsignedByte();
      if(this.g > 0) {
         this.h = var1.readUnsignedByte();
         this.e = var1.readUnsignedByte();
         this.f = var1.readUnsignedByte();
         this.i = var1.a();
      }

   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.writeVarInt(this.a);
      var1.writeByte(this.b);
      var1.writeBoolean(this.c);
      var1.writeVarInt(this.d.length);
      class_ayx[] var2 = this.d;
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         class_ayx var5 = var2[var4];
         var1.writeByte((var5.a() & 15) << 4 | var5.d() & 15);
         var1.writeByte(var5.b());
         var1.writeByte(var5.c());
      }

      var1.writeByte(this.g);
      if(this.g > 0) {
         var1.writeByte(this.h);
         var1.writeByte(this.e);
         var1.writeByte(this.f);
         var1.a(this.i);
      }

   }

   public void a(PacketListenerPlayOut var1) {
      var1.a(this);
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketListenerPlayOut)var1);
   }
}
