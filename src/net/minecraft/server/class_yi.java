package net.minecraft.server;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Item;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.class_ayl;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rd;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sl;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_to;
import net.minecraft.server.class_tr;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_uu;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_yj;
import net.minecraft.server.class_yp;
import net.minecraft.server.class_ys;

public class class_yi extends class_yp {
   private static final UUID a = UUID.fromString("020E0DFB-87AE-4653-9556-831010E291A0");
   private static final AttributeModifier b = (new AttributeModifier(a, "Attacking speed boost", 0.15000000596046448D, 0)).a(false);
   private static final Set c = Sets.newIdentityHashSet();
   private static final class_ke bv = DataWatcher.a(class_yi.class, class_kg.g);
   private static final class_ke bw = DataWatcher.a(class_yi.class, class_kg.h);
   private int bx = 0;
   private int by = 0;

   public class_yi(World var1) {
      super(var1);
      this.a(0.6F, 2.9F);
      this.P = 1.0F;
      this.a(class_ayl.WATER, -1.0F);
   }

   protected void r() {
      this.bp.a(0, new class_tg(this));
      this.bp.a(2, new class_tr(this, 1.0D, false));
      this.bp.a(7, new class_uf(this, 1.0D));
      this.bp.a(8, new class_to(this, EntityHuman.class, 8.0F));
      this.bp.a(8, new class_ue(this));
      this.bp.a(10, new class_yi.class_a_in_class_yi(this));
      this.bp.a(11, new class_yi.class_c_in_class_yi(this));
      this.bq.a(1, new class_yi.class_b_in_class_yi(this));
      this.bq.a(2, new class_uu(this, false, new Class[0]));
      this.bq.a(3, new class_ux(this, class_yj.class, 10, true, false, new Predicate() {
         public boolean a(class_yj var1) {
            return var1.o();
         }

         // $FF: synthetic method
         public boolean apply(Object var1) {
            return this.a((class_yj)var1);
         }
      }));
   }

   protected void bz() {
      super.bz();
      this.a((class_sk)class_ys.a).a(40.0D);
      this.a((class_sk)class_ys.d).a(0.30000001192092896D);
      this.a((class_sk)class_ys.e).a(7.0D);
      this.a((class_sk)class_ys.b).a(64.0D);
   }

   public void c(class_rz var1) {
      super.c(var1);
      class_sl var2 = this.a((class_sk)class_ys.d);
      if(var1 == null) {
         this.by = 0;
         this.datawatcher.b(bw, Boolean.valueOf(false));
         var2.c(b);
      } else {
         this.by = this.ticksLived;
         this.datawatcher.b(bw, Boolean.valueOf(true));
         if(!var2.a(b)) {
            var2.b(b);
         }
      }

   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)bv, (Object)Optional.absent());
      this.datawatcher.a((class_ke)bw, (Object)Boolean.valueOf(false));
   }

   public void o() {
      if(this.ticksLived >= this.bx + 400) {
         this.bx = this.ticksLived;
         if(!this.ac()) {
            this.world.a(this.locX, this.locY + (double)this.bm(), this.locZ, class_ng.aW, this.by(), 2.5F, 1.0F, false);
         }
      }

   }

   public void a(class_ke var1) {
      if(bw.equals(var1) && this.dc() && this.world.E) {
         this.o();
      }

      super.a(var1);
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      IBlockData var2 = this.db();
      if(var2 != null) {
         var1.a("carried", (short)Block.a(var2.getBlock()));
         var1.a("carriedData", (short)var2.getBlock().e(var2));
      }

   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      IBlockData var2;
      if(var1.b("carried", 8)) {
         var2 = Block.b(var1.l("carried")).a(var1.g("carriedData") & '\uffff');
      } else {
         var2 = Block.b(var1.g("carried")).a(var1.g("carriedData") & '\uffff');
      }

      if(var2 == null || var2.getBlock() == null || var2.getMaterial() == Material.a) {
         var2 = null;
      }

      this.a(var2);
   }

   private boolean c(EntityHuman var1) {
      ItemStack var2 = var1.br.b[3];
      if(var2 != null && var2.b() == Item.a(Blocks.aU)) {
         return false;
      } else {
         Vec3D var3 = var1.f(1.0F).a();
         Vec3D var4 = new Vec3D(this.locX - var1.locX, this.bk().b + (double)(this.length / 2.0F) - (var1.locY + (double)var1.bm()), this.locZ - var1.locZ);
         double var5 = var4.b();
         var4 = var4.a();
         double var7 = var3.b(var4);
         return var7 > 1.0D - 0.025D / var5?var1.D(this):false;
      }
   }

   public float bm() {
      return 2.55F;
   }

   public void n() {
      if(this.world.E) {
         for(int var1 = 0; var1 < 2; ++var1) {
            this.world.a(EnumParticle.PORTAL, this.locX + (this.random.nextDouble() - 0.5D) * (double)this.width, this.locY + this.random.nextDouble() * (double)this.length - 0.25D, this.locZ + (this.random.nextDouble() - 0.5D) * (double)this.width, (this.random.nextDouble() - 0.5D) * 2.0D, -this.random.nextDouble(), (this.random.nextDouble() - 0.5D) * 2.0D, new int[0]);
         }
      }

      this.bc = false;
      super.n();
   }

   protected void M() {
      if(this.ag()) {
         this.a(DamageSource.f, 1.0F);
      }

      if(this.world.B() && this.ticksLived >= this.by + 600) {
         float var1 = this.e(1.0F);
         if(var1 > 0.5F && this.world.h(new BlockPosition(this)) && this.random.nextFloat() * 30.0F < (var1 - 0.4F) * 2.0F) {
            this.c((class_rz)null);
            this.da();
         }
      }

      super.M();
   }

   protected boolean da() {
      double var1 = this.locX + (this.random.nextDouble() - 0.5D) * 64.0D;
      double var3 = this.locY + (double)(this.random.nextInt(64) - 32);
      double var5 = this.locZ + (this.random.nextDouble() - 0.5D) * 64.0D;
      return this.l(var1, var3, var5);
   }

   protected boolean a(Entity var1) {
      Vec3D var2 = new Vec3D(this.locX - var1.locX, this.bk().b + (double)(this.length / 2.0F) - var1.locY + (double)var1.bm(), this.locZ - var1.locZ);
      var2 = var2.a();
      double var3 = 16.0D;
      double var5 = this.locX + (this.random.nextDouble() - 0.5D) * 8.0D - var2.b * var3;
      double var7 = this.locY + (double)(this.random.nextInt(16) - 8) - var2.c * var3;
      double var9 = this.locZ + (this.random.nextDouble() - 0.5D) * 8.0D - var2.d * var3;
      return this.l(var5, var7, var9);
   }

   private boolean l(double var1, double var3, double var5) {
      boolean var7 = this.k(var1, var3, var5);
      if(var7) {
         this.world.a((EntityHuman)null, this.lastX, this.lastY, this.lastZ, class_ng.aX, this.by(), 1.0F, 1.0F);
         this.a(class_ng.aX, 1.0F, 1.0F);
      }

      return var7;
   }

   protected class_nf G() {
      return this.dc()?class_ng.aV:class_ng.aS;
   }

   protected class_nf bQ() {
      return class_ng.aU;
   }

   protected class_nf bR() {
      return class_ng.aT;
   }

   protected void a(boolean var1, int var2) {
      super.a(var1, var2);
      IBlockData var3 = this.db();
      if(var3 != null) {
         this.a(new ItemStack(var3.getBlock(), 1, var3.getBlock().e(var3)), 0.0F);
      }

   }

   protected class_kk J() {
      return class_azs.u;
   }

   public void a(IBlockData var1) {
      this.datawatcher.b(bv, Optional.fromNullable(var1));
   }

   public IBlockData db() {
      return (IBlockData)((Optional)this.datawatcher.a(bv)).orNull();
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b(var1)) {
         return false;
      } else if(var1 instanceof class_rd) {
         for(int var4 = 0; var4 < 64; ++var4) {
            if(this.da()) {
               return true;
            }
         }

         return false;
      } else {
         boolean var3 = super.a(var1, var2);
         if(var1.e() && this.random.nextInt(10) != 0) {
            this.da();
         }

         return var3;
      }
   }

   public boolean dc() {
      return ((Boolean)this.datawatcher.a(bw)).booleanValue();
   }

   static {
      c.add(Blocks.c);
      c.add(Blocks.d);
      c.add(Blocks.m);
      c.add(Blocks.n);
      c.add(Blocks.N);
      c.add(Blocks.O);
      c.add(Blocks.P);
      c.add(Blocks.Q);
      c.add(Blocks.W);
      c.add(Blocks.aK);
      c.add(Blocks.aL);
      c.add(Blocks.aU);
      c.add(Blocks.bk);
      c.add(Blocks.bw);
   }

   static class class_c_in_class_yi extends class_tj {
      private final class_yi a;

      public class_c_in_class_yi(class_yi var1) {
         this.a = var1;
      }

      public boolean a() {
         return this.a.db() != null?false:(!this.a.world.U().b("mobGriefing")?false:this.a.bE().nextInt(20) == 0);
      }

      public void e() {
         Random var1 = this.a.bE();
         World var2 = this.a.world;
         int var3 = MathHelper.c(this.a.locX - 2.0D + var1.nextDouble() * 4.0D);
         int var4 = MathHelper.c(this.a.locY + var1.nextDouble() * 3.0D);
         int var5 = MathHelper.c(this.a.locZ - 2.0D + var1.nextDouble() * 4.0D);
         BlockPosition var6 = new BlockPosition(var3, var4, var5);
         IBlockData var7 = var2.getType(var6);
         Block var8 = var7.getBlock();
         MovingObjectPosition var9 = var2.a(new Vec3D((double)((float)MathHelper.c(this.a.locX) + 0.5F), (double)((float)var4 + 0.5F), (double)((float)MathHelper.c(this.a.locZ) + 0.5F)), new Vec3D((double)((float)var3 + 0.5F), (double)((float)var4 + 0.5F), (double)((float)var5 + 0.5F)), false, true, false);
         boolean var10 = var9 != null && var9.a().equals(var6);
         if(class_yi.c.contains(var8) && var10) {
            this.a.a(var7);
            var2.g(var6);
         }

      }
   }

   static class class_a_in_class_yi extends class_tj {
      private final class_yi a;

      public class_a_in_class_yi(class_yi var1) {
         this.a = var1;
      }

      public boolean a() {
         return this.a.db() == null?false:(!this.a.world.U().b("mobGriefing")?false:this.a.bE().nextInt(2000) == 0);
      }

      public void e() {
         Random var1 = this.a.bE();
         World var2 = this.a.world;
         int var3 = MathHelper.c(this.a.locX - 1.0D + var1.nextDouble() * 2.0D);
         int var4 = MathHelper.c(this.a.locY + var1.nextDouble() * 2.0D);
         int var5 = MathHelper.c(this.a.locZ - 1.0D + var1.nextDouble() * 2.0D);
         BlockPosition var6 = new BlockPosition(var3, var4, var5);
         IBlockData var7 = var2.getType(var6);
         IBlockData var8 = var2.getType(var6.b());
         IBlockData var9 = this.a.db();
         if(var9 != null && this.a(var2, var6, var9.getBlock(), var7, var8)) {
            var2.a((BlockPosition)var6, (IBlockData)var9, 3);
            this.a.a((IBlockData)null);
         }

      }

      private boolean a(World var1, BlockPosition var2, Block var3, IBlockData var4, IBlockData var5) {
         return !var3.a(var1, var2)?false:(var4.getMaterial() != Material.a?false:(var5.getMaterial() == Material.a?false:var5.h()));
      }
   }

   static class class_b_in_class_yi extends class_ux {
      private final class_yi i;
      private EntityHuman j;
      private int k;
      private int l;

      public class_b_in_class_yi(class_yi var1) {
         super(var1, EntityHuman.class, false);
         this.i = var1;
      }

      public boolean a() {
         double var1 = this.f();
         this.j = this.i.world.a(this.i.locX, this.i.locY, this.i.locZ, var1, var1, (Function)null, new Predicate() {
            public boolean a(EntityHuman var1) {
               return var1 != null && class_b_in_class_yi.this.i.c(var1);
            }

            // $FF: synthetic method
            public boolean apply(Object var1) {
               return this.a((EntityHuman)var1);
            }
         });
         return this.j != null;
      }

      public void c() {
         this.k = 5;
         this.l = 0;
      }

      public void d() {
         this.j = null;
         super.d();
      }

      public boolean b() {
         if(this.j != null) {
            if(!this.i.c(this.j)) {
               return false;
            } else {
               this.i.a(this.j, 10.0F, 10.0F);
               return true;
            }
         } else {
            return this.d != null && ((EntityHuman)this.d).at()?true:super.b();
         }
      }

      public void e() {
         if(this.j != null) {
            if(--this.k <= 0) {
               this.d = this.j;
               this.j = null;
               super.c();
            }
         } else {
            if(this.d != null) {
               if(this.i.c((EntityHuman)this.d)) {
                  if(((EntityHuman)this.d).h(this.i) < 16.0D) {
                     this.i.da();
                  }

                  this.l = 0;
               } else if(((EntityHuman)this.d).h(this.i) > 256.0D && this.l++ >= 30 && this.i.a((Entity)this.d)) {
                  this.l = 0;
               }
            }

            super.e();
         }

      }
   }
}
