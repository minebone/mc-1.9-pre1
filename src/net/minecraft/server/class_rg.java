package net.minecraft.server;

import net.minecraft.server.AttributeModifier;
import net.minecraft.server.MobEffectType;

public class class_rg extends MobEffectType {
   protected final double a;

   protected class_rg(boolean var1, int var2, double var3) {
      super(var1, var2);
      this.a = var3;
   }

   public double a(int var1, AttributeModifier var2) {
      return this.a * (double)(var1 + 1);
   }
}
