package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_aoa;
import net.minecraft.server.class_aqo;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;

public class class_apa extends class_ajm {
   public static final BlockStateEnum a = BlockStateEnum.a("mode", class_aqo.class_a_in_class_aqo.class);

   public class_apa() {
      super(Material.f, MaterialMapColor.x);
      this.w(this.A.b());
   }

   public TileEntity a(World var1, int var2) {
      return new class_aqo();
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      return false;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, class_rz var4, ItemStack var5) {
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return null;
   }

   public int a(Random var1) {
      return 0;
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.MODEL;
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return this.u().set(a, class_aqo.class_a_in_class_aqo.DATA);
   }

   public IBlockData a(int var1) {
      return this.u().set(a, class_aqo.class_a_in_class_aqo.a(var1));
   }

   public int e(IBlockData var1) {
      return ((class_aqo.class_a_in_class_aqo)var1.get(a)).a();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }
}
