package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockBed;
import net.minecraft.server.Blocks;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.class_apw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_tu;
import net.minecraft.server.class_wa;

public class class_ty extends class_tu {
   private final class_wa c;

   public class_ty(class_wa var1, double var2) {
      super(var1, var2, 8);
      this.c = var1;
   }

   public boolean a() {
      return this.c.cZ() && !this.c.db() && super.a();
   }

   public boolean b() {
      return super.b();
   }

   public void c() {
      super.c();
      this.c.dd().a(false);
   }

   public void d() {
      super.d();
      this.c.q(false);
   }

   public void e() {
      super.e();
      this.c.dd().a(false);
      if(!this.f()) {
         this.c.q(false);
      } else if(!this.c.db()) {
         this.c.q(true);
      }

   }

   protected boolean a(World var1, BlockPosition var2) {
      if(!var1.d(var2.a())) {
         return false;
      } else {
         IBlockData var3 = var1.getType(var2);
         Block var4 = var3.getBlock();
         if(var4 == Blocks.ae) {
            TileEntity var5 = var1.r(var2);
            if(var5 instanceof class_apw && ((class_apw)var5).l < 1) {
               return true;
            }
         } else {
            if(var4 == Blocks.am) {
               return true;
            }

            if(var4 == Blocks.C && var3.get(BlockBed.a) != BlockBed.EnumBedPart.HEAD) {
               return true;
            }
         }

         return false;
      }
   }
}
