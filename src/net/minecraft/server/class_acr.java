package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockDoor;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aoo;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;

public class class_acr extends Item {
   private Block a;

   public class_acr(Block var1) {
      this.a = var1;
      this.a(CreativeModeTab.d);
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      if(var6 != EnumDirection.UP) {
         return EnumResult.FAIL;
      } else {
         IBlockData var10 = var3.getType(var4);
         Block var11 = var10.getBlock();
         if(!var11.a((class_ahw)var3, (BlockPosition)var4)) {
            var4 = var4.a(var6);
         }

         if(var2.a(var4, var6, var1) && this.a.a(var3, var4)) {
            EnumDirection var12 = EnumDirection.a((double)var2.yaw);
            int var13 = var12.g();
            int var14 = var12.i();
            boolean var15 = var13 < 0 && var9 < 0.5F || var13 > 0 && var9 > 0.5F || var14 < 0 && var7 > 0.5F || var14 > 0 && var7 < 0.5F;
            a(var3, var4, var12, this.a, var15);
            class_aoo var16 = this.a.w();
            var3.a(var2, var4, var16.e(), EnumSoundCategory.BLOCKS, (var16.a() + 1.0F) / 2.0F, var16.b() * 0.8F);
            --var1.b;
            return EnumResult.SUCCESS;
         } else {
            return EnumResult.FAIL;
         }
      }
   }

   public static void a(World var0, BlockPosition var1, EnumDirection var2, Block var3, boolean var4) {
      BlockPosition var5 = var1.a(var2.e());
      BlockPosition var6 = var1.a(var2.f());
      int var7 = (var0.getType(var6).l()?1:0) + (var0.getType(var6.a()).l()?1:0);
      int var8 = (var0.getType(var5).l()?1:0) + (var0.getType(var5.a()).l()?1:0);
      boolean var9 = var0.getType(var6).getBlock() == var3 || var0.getType(var6.a()).getBlock() == var3;
      boolean var10 = var0.getType(var5).getBlock() == var3 || var0.getType(var5.a()).getBlock() == var3;
      if((!var9 || var10) && var8 <= var7) {
         if(var10 && !var9 || var8 < var7) {
            var4 = false;
         }
      } else {
         var4 = true;
      }

      BlockPosition var11 = var1.a();
      boolean var12 = var0.y(var1) || var0.y(var11);
      IBlockData var13 = var3.u().set(BlockDoor.a, var2).set(BlockDoor.c, var4?BlockDoor.EnumDoorHinge.RIGHT:BlockDoor.EnumDoorHinge.LEFT).set(BlockDoor.d, Boolean.valueOf(var12)).set(BlockDoor.b, Boolean.valueOf(var12));
      var0.a((BlockPosition)var1, (IBlockData)var13.set(BlockDoor.e, BlockDoor.class_a_in_class_aku.LOWER), 2);
      var0.a((BlockPosition)var11, (IBlockData)var13.set(BlockDoor.e, BlockDoor.class_a_in_class_aku.UPPER), 2);
      var0.d(var1, var3);
      var0.d(var11, var3);
   }
}
