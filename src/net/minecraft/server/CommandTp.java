package net.minecraft.server;

import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.Entity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.PacketPlayOutPosition;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.MathHelper;

public class CommandTp extends CommandAbstract {
   public String c() {
      return "tp";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.tp.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 1) {
         throw new class_cf("commands.tp.usage", new Object[0]);
      } else {
         byte var4 = 0;
         Object var5;
         if(var3.length != 2 && var3.length != 4 && var3.length != 6) {
            var5 = a(var2);
         } else {
            var5 = b(var1, var2, var3[0]);
            var4 = 1;
         }

         if(var3.length != 1 && var3.length != 2) {
            if(var3.length < var4 + 3) {
               throw new class_cf("commands.tp.usage", new Object[0]);
            } else if(((Entity)var5).world != null) {
               int var15 = var4 + 1;
               CommandAbstract.class_a_in_class_i var7 = a(((Entity)var5).locX, var3[var4], true);
               CommandAbstract.class_a_in_class_i var8 = a(((Entity)var5).locY, var3[var15++], 0, 0, false);
               CommandAbstract.class_a_in_class_i var9 = a(((Entity)var5).locZ, var3[var15++], true);
               CommandAbstract.class_a_in_class_i var10 = a((double)((Entity)var5).yaw, var3.length > var15?var3[var15++]:"~", false);
               CommandAbstract.class_a_in_class_i var11 = a((double)((Entity)var5).pitch, var3.length > var15?var3[var15]:"~", false);
               float var13;
               if(var5 instanceof EntityPlayer) {
                  EnumSet var12 = EnumSet.noneOf(PacketPlayOutPosition.EnumPlayerTeleportFlags.class);
                  if(var7.c()) {
                     var12.add(PacketPlayOutPosition.EnumPlayerTeleportFlags.X);
                  }

                  if(var8.c()) {
                     var12.add(PacketPlayOutPosition.EnumPlayerTeleportFlags.Y);
                  }

                  if(var9.c()) {
                     var12.add(PacketPlayOutPosition.EnumPlayerTeleportFlags.Z);
                  }

                  if(var11.c()) {
                     var12.add(PacketPlayOutPosition.EnumPlayerTeleportFlags.X_ROT);
                  }

                  if(var10.c()) {
                     var12.add(PacketPlayOutPosition.EnumPlayerTeleportFlags.Y_ROT);
                  }

                  var13 = (float)var10.b();
                  if(!var10.c()) {
                     var13 = MathHelper.g(var13);
                  }

                  float var14 = (float)var11.b();
                  if(!var11.c()) {
                     var14 = MathHelper.g(var14);
                  }

                  ((Entity)var5).p();
                  ((EntityPlayer)var5).a.a(var7.b(), var8.b(), var9.b(), var13, var14, var12);
                  ((Entity)var5).h(var13);
               } else {
                  float var16 = (float)MathHelper.g(var10.a());
                  var13 = (float)MathHelper.g(var11.a());
                  var13 = MathHelper.a(var13, -90.0F, 90.0F);
                  ((Entity)var5).b(var7.a(), var8.a(), var9.a(), var16, var13);
                  ((Entity)var5).h(var16);
               }

               a(var2, this, "commands.tp.success.coordinates", new Object[]{((Entity)var5).h_(), Double.valueOf(var7.a()), Double.valueOf(var8.a()), Double.valueOf(var9.a())});
            }
         } else {
            Entity var6 = b(var1, var2, var3[var3.length - 1]);
            if(var6.world != ((Entity)var5).world) {
               throw new class_bz("commands.tp.notSameDimension", new Object[0]);
            } else {
               ((Entity)var5).p();
               if(var5 instanceof EntityPlayer) {
                  ((EntityPlayer)var5).a.a(var6.locX, var6.locY, var6.locZ, var6.yaw, var6.pitch);
               } else {
                  ((Entity)var5).b(var6.locX, var6.locY, var6.locZ, var6.yaw, var6.pitch);
               }

               a(var2, this, "commands.tp.success", new Object[]{((Entity)var5).h_(), var6.h_()});
            }
         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length != 1 && var3.length != 2?Collections.emptyList():a(var3, var1.J());
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 0;
   }
}
