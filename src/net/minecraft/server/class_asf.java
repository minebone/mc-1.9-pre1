package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ash;
import net.minecraft.server.class_asi;

public class class_asf implements class_ash {
   private final IBlockData[] a;
   private final class_asi b;
   private final int c;
   private int d;

   public class_asf(int var1, class_asi var2) {
      this.a = new IBlockData[1 << var1];
      this.c = var1;
      this.b = var2;
   }

   public int a(IBlockData var1) {
      int var2;
      for(var2 = 0; var2 < this.d; ++var2) {
         if(this.a[var2] == var1) {
            return var2;
         }
      }

      var2 = this.d++;
      if(var2 < this.a.length) {
         this.a[var2] = var1;
         if(var2 == 16) {
            System.out.println("");
         }

         return var2;
      } else {
         return this.b.a(this.c + 1, var1);
      }
   }

   public IBlockData a(int var1) {
      return var1 > 0 && var1 < this.d?this.a[var1]:null;
   }

   public void b(PacketDataSerializer var1) {
      var1.writeVarInt(this.d);

      for(int var2 = 0; var2 < this.d; ++var2) {
         var1.writeVarInt(Block.i.a(this.a[var2]));
      }

   }

   public int a() {
      int var1 = PacketDataSerializer.a(this.d);

      for(int var2 = 0; var2 < this.d; ++var2) {
         var1 += PacketDataSerializer.a(Block.i.a(this.a[var2]));
      }

      return var1;
   }
}
