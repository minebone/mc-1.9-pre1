package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;

public class class_anv extends Block {
   private final boolean a;

   public class_anv(boolean var1) {
      super(Material.e);
      if(var1) {
         this.a(true);
      }

      this.a = var1;
   }

   public int a(World var1) {
      return 30;
   }

   public void a(World var1, BlockPosition var2, EntityHuman var3) {
      this.b(var1, var2);
      super.a(var1, var2, var3);
   }

   public void a(World var1, BlockPosition var2, Entity var3) {
      this.b(var1, var2);
      super.a(var1, var2, var3);
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      this.b(var1, var2);
      return super.a(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
   }

   private void b(World var1, BlockPosition var2) {
      this.c(var1, var2);
      if(this == Blocks.aC) {
         var1.a(var2, Blocks.aD.u());
      }

   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(this == Blocks.aD) {
         var1.a(var2, Blocks.aC.u());
      }

   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Items.aE;
   }

   public int a(int var1, Random var2) {
      return this.a(var2) + var2.nextInt(var1 + 1);
   }

   public int a(Random var1) {
      return 4 + var1.nextInt(2);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, float var4, int var5) {
      super.a(var1, var2, var3, var4, var5);
      if(this.a(var3, var1.r, var5) != Item.a((Block)this)) {
         int var6 = 1 + var1.r.nextInt(5);
         this.b(var1, var2, var6);
      }

   }

   private void c(World var1, BlockPosition var2) {
      Random var3 = var1.r;
      double var4 = 0.0625D;

      for(int var6 = 0; var6 < 6; ++var6) {
         double var7 = (double)((float)var2.p() + var3.nextFloat());
         double var9 = (double)((float)var2.q() + var3.nextFloat());
         double var11 = (double)((float)var2.r() + var3.nextFloat());
         if(var6 == 0 && !var1.getType(var2.a()).p()) {
            var9 = (double)var2.q() + var4 + 1.0D;
         }

         if(var6 == 1 && !var1.getType(var2.b()).p()) {
            var9 = (double)var2.q() - var4;
         }

         if(var6 == 2 && !var1.getType(var2.d()).p()) {
            var11 = (double)var2.r() + var4 + 1.0D;
         }

         if(var6 == 3 && !var1.getType(var2.c()).p()) {
            var11 = (double)var2.r() - var4;
         }

         if(var6 == 4 && !var1.getType(var2.f()).p()) {
            var7 = (double)var2.p() + var4 + 1.0D;
         }

         if(var6 == 5 && !var1.getType(var2.e()).p()) {
            var7 = (double)var2.p() - var4;
         }

         if(var7 < (double)var2.p() || var7 > (double)(var2.p() + 1) || var9 < 0.0D || var9 > (double)(var2.q() + 1) || var11 < (double)var2.r() || var11 > (double)(var2.r() + 1)) {
            var1.a(EnumParticle.REDSTONE, var7, var9, var11, 0.0D, 0.0D, 0.0D, new int[0]);
         }
      }

   }

   protected ItemStack u(IBlockData var1) {
      return new ItemStack(Blocks.aC);
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Item.a(Blocks.aC), 1, this.d(var3));
   }
}
