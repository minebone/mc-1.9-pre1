package net.minecraft.server;

import net.minecraft.server.Vec3D;
import net.minecraft.server.class_wt;
import net.minecraft.server.class_wv;
import net.minecraft.server.class_xj;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_wx extends class_wv {
   private static final Logger b = LogManager.getLogger();
   private Vec3D c;
   private int d = 0;

   public class_wx(class_wt var1) {
      super(var1);
   }

   public void c() {
      if(this.c == null) {
         b.warn("Aborting charge player as no target was set.");
         this.a.cT().a(class_xj.a);
      } else if(this.d > 0 && this.d++ >= 10) {
         this.a.cT().a(class_xj.a);
      } else {
         double var1 = this.c.c(this.a.locX, this.a.locY, this.a.locZ);
         if(var1 < 100.0D || var1 > 22500.0D || this.a.positionChanged || this.a.B) {
            ++this.d;
         }

      }
   }

   public void d() {
      this.c = null;
      this.d = 0;
   }

   public void a(Vec3D var1) {
      this.c = var1;
   }

   public float f() {
      return 3.0F;
   }

   public Vec3D g() {
      return this.c;
   }

   public class_xj i() {
      return class_xj.i;
   }
}
