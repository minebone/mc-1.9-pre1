package net.minecraft.server;

import net.minecraft.server.Container;
import net.minecraft.server.CraftingManager;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.InventoryCraftResult;
import net.minecraft.server.InventoryCrafting;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.class_abr;
import net.minecraft.server.class_abs;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IInventory;
import net.minecraft.server.EnumInventorySlot;

public class class_abk extends Container {
   private static final EnumInventorySlot[] h = new EnumInventorySlot[]{EnumInventorySlot.HEAD, EnumInventorySlot.CHEST, EnumInventorySlot.LEGS, EnumInventorySlot.FEET};
   public InventoryCrafting a = new InventoryCrafting(this, 2, 2);
   public IInventory f = new InventoryCraftResult();
   public boolean g;
   private final EntityHuman i;

   public class_abk(final PlayerInventory var1, boolean var2, EntityHuman var3) {
      this.g = var2;
      this.i = var3;
      this.a((class_abs)(new class_abr(var1.e, this.a, this.f, 0, 154, 28)));

      int var4;
      int var5;
      for(var4 = 0; var4 < 2; ++var4) {
         for(var5 = 0; var5 < 2; ++var5) {
            this.a((class_abs)(new class_abs(this.a, var5 + var4 * 2, 98 + var5 * 18, 18 + var4 * 18)));
         }
      }

      for(var4 = 0; var4 < 4; ++var4) {
         final EnumInventorySlot var6 = h[var4];
         this.a((class_abs)(new class_abs(var1, 36 + (3 - var4), 8, 8 + var4 * 18) {
            public int a() {
               return 1;
            }

            public boolean a(ItemStack var1) {
               if(var1 == null) {
                  return false;
               } else {
                  EnumInventorySlot var2 = EntityInsentient.d(var1);
                  return var2 == var6;
               }
            }
         }));
      }

      for(var4 = 0; var4 < 3; ++var4) {
         for(var5 = 0; var5 < 9; ++var5) {
            this.a((class_abs)(new class_abs(var1, var5 + (var4 + 1) * 9, 8 + var5 * 18, 84 + var4 * 18)));
         }
      }

      for(var4 = 0; var4 < 9; ++var4) {
         this.a((class_abs)(new class_abs(var1, var4, 8 + var4 * 18, 142)));
      }

      this.a((class_abs)(new class_abs(var1, 40, 77, 62) {
         public boolean a(ItemStack var1) {
            return super.a(var1);
         }
      }));
      this.a((IInventory)this.a);
   }

   public void a(IInventory var1) {
      this.f.a(0, CraftingManager.a().a(this.a, this.i.world));
   }

   public void b(EntityHuman var1) {
      super.b(var1);

      for(int var2 = 0; var2 < 4; ++var2) {
         ItemStack var3 = this.a.b(var2);
         if(var3 != null) {
            var1.a(var3, false);
         }
      }

      this.f.a(0, (ItemStack)null);
   }

   public boolean a(EntityHuman var1) {
      return true;
   }

   public ItemStack b(EntityHuman var1, int var2) {
      ItemStack var3 = null;
      class_abs var4 = (class_abs)this.c.get(var2);
      if(var4 != null && var4.e()) {
         ItemStack var5 = var4.d();
         var3 = var5.k();
         EnumInventorySlot var6 = EntityInsentient.d(var3);
         if(var2 == 0) {
            if(!this.a(var5, 9, 45, true)) {
               return null;
            }

            var4.a(var5, var3);
         } else if(var2 >= 1 && var2 < 5) {
            if(!this.a(var5, 9, 45, false)) {
               return null;
            }
         } else if(var2 >= 5 && var2 < 9) {
            if(!this.a(var5, 9, 45, false)) {
               return null;
            }
         } else if(var6.a() == EnumInventorySlot.EnumSlotType.ARMOR && !((class_abs)this.c.get(8 - var6.b())).e()) {
            int var7 = 8 - var6.b();
            if(!this.a(var5, var7, var7 + 1, false)) {
               return null;
            }
         } else if(var2 >= 9 && var2 < 36) {
            if(!this.a(var5, 36, 45, false)) {
               return null;
            }
         } else if(var2 >= 36 && var2 < 45) {
            if(!this.a(var5, 9, 36, false)) {
               return null;
            }
         } else if(!this.a(var5, 9, 45, false)) {
            return null;
         }

         if(var5.b == 0) {
            var4.d((ItemStack)null);
         } else {
            var4.f();
         }

         if(var5.b == var3.b) {
            return null;
         }

         var4.a(var1, var5);
      }

      return var3;
   }

   public boolean a(ItemStack var1, class_abs var2) {
      return var2.d != this.f && super.a(var1, var2);
   }
}
