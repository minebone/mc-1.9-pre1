package net.minecraft.server;

import java.util.List;
import java.util.UUID;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_kk;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_zr;

public abstract class EntityProjectile extends Entity implements class_zr {
   private int d;
   private int e;
   private int f;
   private Block g;
   protected boolean a;
   public int b;
   private class_rz h;
   private String as;
   private int at;
   private int au;
   public Entity c;
   private int av;

   public EntityProjectile(World var1) {
      super(var1);
      this.d = -1;
      this.e = -1;
      this.f = -1;
      this.a(0.25F, 0.25F);
   }

   public EntityProjectile(World var1, double var2, double var4, double var6) {
      this(var1);
      this.b(var2, var4, var6);
   }

   public EntityProjectile(World var1, class_rz var2) {
      this(var1, var2.locX, var2.locY + (double)var2.bm() - 0.10000000149011612D, var2.locZ);
      this.h = var2;
   }

   protected void i() {
   }

   public void a(Entity var1, float var2, float var3, float var4, float var5, float var6) {
      float var7 = -MathHelper.a(var3 * 0.017453292F) * MathHelper.b(var2 * 0.017453292F);
      float var8 = -MathHelper.a((var2 + var4) * 0.017453292F);
      float var9 = MathHelper.b(var3 * 0.017453292F) * MathHelper.b(var2 * 0.017453292F);
      this.c((double)var7, (double)var8, (double)var9, var5, var6);
      this.motX += var1.motX;
      this.motZ += var1.motZ;
      if(!var1.onGround) {
         this.motY += var1.motY;
      }

   }

   public void c(double var1, double var3, double var5, float var7, float var8) {
      float var9 = MathHelper.a(var1 * var1 + var3 * var3 + var5 * var5);
      var1 /= (double)var9;
      var3 /= (double)var9;
      var5 /= (double)var9;
      var1 += this.random.nextGaussian() * 0.007499999832361937D * (double)var8;
      var3 += this.random.nextGaussian() * 0.007499999832361937D * (double)var8;
      var5 += this.random.nextGaussian() * 0.007499999832361937D * (double)var8;
      var1 *= (double)var7;
      var3 *= (double)var7;
      var5 *= (double)var7;
      this.motX = var1;
      this.motY = var3;
      this.motZ = var5;
      float var10 = MathHelper.a(var1 * var1 + var5 * var5);
      this.lastYaw = this.yaw = (float)(MathHelper.b(var1, var5) * 57.2957763671875D);
      this.lastPitch = this.pitch = (float)(MathHelper.b(var3, (double)var10) * 57.2957763671875D);
      this.at = 0;
   }

   public void m() {
      this.M = this.locX;
      this.N = this.locY;
      this.O = this.locZ;
      super.m();
      if(this.b > 0) {
         --this.b;
      }

      if(this.a) {
         if(this.world.getType(new BlockPosition(this.d, this.e, this.f)).getBlock() == this.g) {
            ++this.at;
            if(this.at == 1200) {
               this.S();
            }

            return;
         }

         this.a = false;
         this.motX *= (double)(this.random.nextFloat() * 0.2F);
         this.motY *= (double)(this.random.nextFloat() * 0.2F);
         this.motZ *= (double)(this.random.nextFloat() * 0.2F);
         this.at = 0;
         this.au = 0;
      } else {
         ++this.au;
      }

      Vec3D var1 = new Vec3D(this.locX, this.locY, this.locZ);
      Vec3D var2 = new Vec3D(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
      MovingObjectPosition var3 = this.world.a(var1, var2);
      var1 = new Vec3D(this.locX, this.locY, this.locZ);
      var2 = new Vec3D(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
      if(var3 != null) {
         var2 = new Vec3D(var3.c.b, var3.c.c, var3.c.d);
      }

      Entity var4 = null;
      List var5 = this.world.b((Entity)this, (AxisAlignedBB)this.bk().a(this.motX, this.motY, this.motZ).g(1.0D));
      double var6 = 0.0D;
      boolean var8 = false;

      for(int var9 = 0; var9 < var5.size(); ++var9) {
         Entity var10 = (Entity)var5.get(var9);
         if(var10.ao()) {
            if(var10 == this.c) {
               var8 = true;
            } else if(this.ticksLived < 2 && this.c == null) {
               this.c = var10;
               var8 = true;
            } else {
               var8 = false;
               AxisAlignedBB var11 = var10.bk().g(0.30000001192092896D);
               MovingObjectPosition var12 = var11.a(var1, var2);
               if(var12 != null) {
                  double var13 = var1.g(var12.c);
                  if(var13 < var6 || var6 == 0.0D) {
                     var4 = var10;
                     var6 = var13;
                  }
               }
            }
         }
      }

      if(this.c != null) {
         if(var8) {
            this.av = 2;
         } else if(this.av-- <= 0) {
            this.c = null;
         }
      }

      if(var4 != null) {
         var3 = new MovingObjectPosition(var4);
      }

      if(var3 != null) {
         if(var3.a == MovingObjectPosition.EnumMovingObjectType.BLOCK && this.world.getType(var3.a()).getBlock() == Blocks.aY) {
            this.e(var3.a());
         } else {
            this.a(var3);
         }
      }

      this.locX += this.motX;
      this.locY += this.motY;
      this.locZ += this.motZ;
      float var15 = MathHelper.a(this.motX * this.motX + this.motZ * this.motZ);
      this.yaw = (float)(MathHelper.b(this.motX, this.motZ) * 57.2957763671875D);

      for(this.pitch = (float)(MathHelper.b(this.motY, (double)var15) * 57.2957763671875D); this.pitch - this.lastPitch < -180.0F; this.lastPitch -= 360.0F) {
         ;
      }

      while(this.pitch - this.lastPitch >= 180.0F) {
         this.lastPitch += 360.0F;
      }

      while(this.yaw - this.lastYaw < -180.0F) {
         this.lastYaw -= 360.0F;
      }

      while(this.yaw - this.lastYaw >= 180.0F) {
         this.lastYaw += 360.0F;
      }

      this.pitch = this.lastPitch + (this.pitch - this.lastPitch) * 0.2F;
      this.yaw = this.lastYaw + (this.yaw - this.lastYaw) * 0.2F;
      float var16 = 0.99F;
      float var17 = this.j();
      if(this.ah()) {
         for(int var18 = 0; var18 < 4; ++var18) {
            float var19 = 0.25F;
            this.world.a(EnumParticle.WATER_BUBBLE, this.locX - this.motX * (double)var19, this.locY - this.motY * (double)var19, this.locZ - this.motZ * (double)var19, this.motX, this.motY, this.motZ, new int[0]);
         }

         var16 = 0.8F;
      }

      this.motX *= (double)var16;
      this.motY *= (double)var16;
      this.motZ *= (double)var16;
      this.motY -= (double)var17;
      this.b(this.locX, this.locY, this.locZ);
   }

   protected float j() {
      return 0.03F;
   }

   protected abstract void a(MovingObjectPosition var1);

   public void b(NBTTagCompound var1) {
      var1.a("xTile", this.d);
      var1.a("yTile", this.e);
      var1.a("zTile", this.f);
      class_kk var2 = (class_kk)Block.h.b(this.g);
      var1.a("inTile", var2 == null?"":var2.toString());
      var1.a("shake", (byte)this.b);
      var1.a("inGround", (byte)(this.a?1:0));
      if((this.as == null || this.as.isEmpty()) && this.h instanceof EntityHuman) {
         this.as = this.h.h_();
      }

      var1.a("ownerName", this.as == null?"":this.as);
   }

   public void a(NBTTagCompound var1) {
      this.d = var1.h("xTile");
      this.e = var1.h("yTile");
      this.f = var1.h("zTile");
      if(var1.b("inTile", 8)) {
         this.g = Block.b(var1.l("inTile"));
      } else {
         this.g = Block.b(var1.f("inTile") & 255);
      }

      this.b = var1.f("shake") & 255;
      this.a = var1.f("inGround") == 1;
      this.h = null;
      this.as = var1.l("ownerName");
      if(this.as != null && this.as.isEmpty()) {
         this.as = null;
      }

      this.h = this.k();
   }

   public class_rz k() {
      if(this.h == null && this.as != null && !this.as.isEmpty()) {
         this.h = this.world.a(this.as);
         if(this.h == null && this.world instanceof WorldServer) {
            try {
               Entity var1 = ((WorldServer)this.world).a(UUID.fromString(this.as));
               if(var1 instanceof class_rz) {
                  this.h = (class_rz)var1;
               }
            } catch (Throwable var2) {
               this.h = null;
            }
         }
      }

      return this.h;
   }
}
