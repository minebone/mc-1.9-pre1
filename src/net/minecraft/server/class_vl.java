package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.EntityCreature;
import net.minecraft.server.NavigationAbstract;
import net.minecraft.server.Vec3D;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;

public class class_vl {
   private static Vec3D a = Vec3D.a;

   public static Vec3D a(EntityCreature var0, int var1, int var2) {
      return c(var0, var1, var2, (Vec3D)null);
   }

   public static Vec3D a(EntityCreature var0, int var1, int var2, Vec3D var3) {
      a = var3.a(var0.locX, var0.locY, var0.locZ);
      return c(var0, var1, var2, a);
   }

   public static Vec3D b(EntityCreature var0, int var1, int var2, Vec3D var3) {
      a = (new Vec3D(var0.locX, var0.locY, var0.locZ)).d(var3);
      return c(var0, var1, var2, a);
   }

   private static Vec3D c(EntityCreature var0, int var1, int var2, Vec3D var3) {
      NavigationAbstract var4 = var0.x();
      Random var5 = var0.bE();
      boolean var6 = false;
      int var7 = 0;
      int var8 = 0;
      int var9 = 0;
      float var10 = -99999.0F;
      boolean var11;
      if(var0.cY()) {
         double var12 = var0.cV().e((double)MathHelper.c(var0.locX), (double)MathHelper.c(var0.locY), (double)MathHelper.c(var0.locZ)) + 4.0D;
         double var14 = (double)(var0.cW() + (float)var1);
         var11 = var12 < var14 * var14;
      } else {
         var11 = false;
      }

      for(int var18 = 0; var18 < 10; ++var18) {
         int var13 = var5.nextInt(2 * var1 + 1) - var1;
         int var19 = var5.nextInt(2 * var2 + 1) - var2;
         int var15 = var5.nextInt(2 * var1 + 1) - var1;
         if(var3 == null || (double)var13 * var3.b + (double)var15 * var3.d >= 0.0D) {
            BlockPosition var16;
            if(var0.cY() && var1 > 1) {
               var16 = var0.cV();
               if(var0.locX > (double)var16.p()) {
                  var13 -= var5.nextInt(var1 / 2);
               } else {
                  var13 += var5.nextInt(var1 / 2);
               }

               if(var0.locZ > (double)var16.r()) {
                  var15 -= var5.nextInt(var1 / 2);
               } else {
                  var15 += var5.nextInt(var1 / 2);
               }
            }

            var13 += MathHelper.c(var0.locX);
            var19 += MathHelper.c(var0.locY);
            var15 += MathHelper.c(var0.locZ);
            var16 = new BlockPosition(var13, var19, var15);
            if((!var11 || var0.f(var16)) && var4.b(var16)) {
               float var17 = var0.a(var16);
               if(var17 > var10) {
                  var10 = var17;
                  var7 = var13;
                  var8 = var19;
                  var9 = var15;
                  var6 = true;
               }
            }
         }
      }

      if(var6) {
         return new Vec3D((double)var7, (double)var8, (double)var9);
      } else {
         return null;
      }
   }
}
