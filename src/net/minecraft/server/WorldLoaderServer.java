package net.minecraft.server;

import com.google.common.collect.Lists;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import net.minecraft.server.NBTCompressedStreamTools;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.RegionFile;
import net.minecraft.server.WorldData;
import net.minecraft.server.WorldType;
import net.minecraft.server.class_aij;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_ain;
import net.minecraft.server.class_aso;
import net.minecraft.server.class_azb;
import net.minecraft.server.class_azf;
import net.minecraft.server.class_azh;
import net.minecraft.server.class_op;
import net.minecraft.server.class_pb;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WorldLoaderServer extends class_azf {
   private static final Logger c = LogManager.getLogger();

   public WorldLoaderServer(File var1, class_pb var2) {
      super(var1, var2);
   }

   protected int c() {
      return 19133;
   }

   public class_azh a(String var1, boolean var2) {
      return new class_azb(this.a, var1, var2, this.b);
   }

   public boolean b(String var1) {
      WorldData var2 = this.c(var1);
      return var2 != null && var2.k() != this.c();
   }

   public boolean a(String var1, class_op var2) {
      var2.a(0);
      ArrayList var3 = Lists.newArrayList();
      ArrayList var4 = Lists.newArrayList();
      ArrayList var5 = Lists.newArrayList();
      File var6 = new File(this.a, var1);
      File var7 = new File(var6, "DIM-1");
      File var8 = new File(var6, "DIM1");
      c.info("Scanning folders...");
      this.a((File)var6, (Collection)var3);
      if(var7.exists()) {
         this.a((File)var7, (Collection)var4);
      }

      if(var8.exists()) {
         this.a((File)var8, (Collection)var5);
      }

      int var9 = var3.size() + var4.size() + var5.size();
      c.info("Total conversion count is " + var9);
      WorldData var10 = this.c(var1);
      Object var11 = null;
      if(var10.t() == WorldType.c) {
         var11 = new class_ain(class_aik.c);
      } else {
         var11 = new class_aij(var10);
      }

      this.a(new File(var6, "region"), (Iterable)var3, (class_aij)var11, 0, var9, var2);
      this.a(new File(var7, "region"), (Iterable)var4, new class_ain(class_aik.j), var3.size(), var9, var2);
      this.a(new File(var8, "region"), (Iterable)var5, new class_ain(class_aik.k), var3.size() + var4.size(), var9, var2);
      var10.e(19133);
      if(var10.t() == WorldType.h) {
         var10.a(WorldType.b);
      }

      this.g(var1);
      class_azh var12 = this.a(var1, false);
      var12.a(var10);
      return true;
   }

   private void g(String var1) {
      File var2 = new File(this.a, var1);
      if(!var2.exists()) {
         c.warn("Unable to create level.dat_mcr backup");
      } else {
         File var3 = new File(var2, "level.dat");
         if(!var3.exists()) {
            c.warn("Unable to create level.dat_mcr backup");
         } else {
            File var4 = new File(var2, "level.dat_mcr");
            if(!var3.renameTo(var4)) {
               c.warn("Unable to create level.dat_mcr backup");
            }

         }
      }
   }

   private void a(File var1, Iterable var2, class_aij var3, int var4, int var5, class_op var6) {
      Iterator var7 = var2.iterator();

      while(var7.hasNext()) {
         File var8 = (File)var7.next();
         this.a(var1, var8, var3, var4, var5, var6);
         ++var4;
         int var9 = (int)Math.round(100.0D * (double)var4 / (double)var5);
         var6.a(var9);
      }

   }

   private void a(File var1, File var2, class_aij var3, int var4, int var5, class_op var6) {
      try {
         String var7 = var2.getName();
         RegionFile var8 = new RegionFile(var2);
         RegionFile var9 = new RegionFile(new File(var1, var7.substring(0, var7.length() - ".mcr".length()) + ".mca"));

         for(int var10 = 0; var10 < 32; ++var10) {
            int var11;
            for(var11 = 0; var11 < 32; ++var11) {
               if(var8.c(var10, var11) && !var9.c(var10, var11)) {
                  DataInputStream var12 = var8.a(var10, var11);
                  if(var12 == null) {
                     c.warn("Failed to fetch input stream");
                  } else {
                     NBTTagCompound var13 = NBTCompressedStreamTools.a(var12);
                     var12.close();
                     NBTTagCompound var14 = var13.o("Level");
                     class_aso.class_a_in_class_aso var15 = class_aso.a(var14);
                     NBTTagCompound var16 = new NBTTagCompound();
                     NBTTagCompound var17 = new NBTTagCompound();
                     var16.a((String)"Level", (NBTTag)var17);
                     class_aso.a(var15, var17, var3);
                     DataOutputStream var18 = var9.b(var10, var11);
                     NBTCompressedStreamTools.a((NBTTagCompound)var16, (DataOutput)var18);
                     var18.close();
                  }
               }
            }

            var11 = (int)Math.round(100.0D * (double)(var4 * 1024) / (double)(var5 * 1024));
            int var20 = (int)Math.round(100.0D * (double)((var10 + 1) * 32 + var4 * 1024) / (double)(var5 * 1024));
            if(var20 > var11) {
               var6.a(var20);
            }
         }

         var8.c();
         var9.c();
      } catch (IOException var19) {
         var19.printStackTrace();
      }

   }

   private void a(File var1, Collection var2) {
      File var3 = new File(var1, "region");
      File[] var4 = var3.listFiles(new FilenameFilter() {
         public boolean accept(File var1, String var2) {
            return var2.endsWith(".mcr");
         }
      });
      if(var4 != null) {
         Collections.addAll(var2, var4);
      }

   }
}
