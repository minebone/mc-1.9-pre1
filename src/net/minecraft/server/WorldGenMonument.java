package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Map.Entry;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.StructureBoundingBox;
import net.minecraft.server.StructureGenerator;
import net.minecraft.server.StructureStart;
import net.minecraft.server.World;
import net.minecraft.server.WorldGenMonumentPieces;
import net.minecraft.server.class_ahm;
import net.minecraft.server.class_aik;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_yn;

public class WorldGenMonument extends StructureGenerator {
   private int d;
   private int h;
   public static final List a = Arrays.asList(new BiomeBase[]{class_aik.a, class_aik.z, class_aik.i, class_aik.l, class_aik.m});
   public static final List b = Arrays.asList(new BiomeBase[]{class_aik.z});
   private static final List i = Lists.newArrayList();

   public WorldGenMonument() {
      this.d = 32;
      this.h = 5;
   }

   public WorldGenMonument(Map var1) {
      this();
      Iterator var2 = var1.entrySet().iterator();

      while(var2.hasNext()) {
         Entry var3 = (Entry)var2.next();
         if(((String)var3.getKey()).equals("spacing")) {
            this.d = MathHelper.a((String)((String)var3.getValue()), this.d, 1);
         } else if(((String)var3.getKey()).equals("separation")) {
            this.h = MathHelper.a((String)((String)var3.getValue()), this.h, 1);
         }
      }

   }

   public String a() {
      return "Monument";
   }

   protected boolean a(int var1, int var2) {
      int var3 = var1;
      int var4 = var2;
      if(var1 < 0) {
         var1 -= this.d - 1;
      }

      if(var2 < 0) {
         var2 -= this.d - 1;
      }

      int var5 = var1 / this.d;
      int var6 = var2 / this.d;
      Random var7 = this.g.a(var5, var6, 10387313);
      var5 *= this.d;
      var6 *= this.d;
      var5 += (var7.nextInt(this.d - this.h) + var7.nextInt(this.d - this.h)) / 2;
      var6 += (var7.nextInt(this.d - this.h) + var7.nextInt(this.d - this.h)) / 2;
      if(var3 == var5 && var4 == var6) {
         if(!this.g.A().a(var3 * 16 + 8, var4 * 16 + 8, 16, b)) {
            return false;
         }

         boolean var8 = this.g.A().a(var3 * 16 + 8, var4 * 16 + 8, 29, a);
         if(var8) {
            return true;
         }
      }

      return false;
   }

   protected StructureStart b(int var1, int var2) {
      return new WorldGenMonument.WorldGenMonumentStart(this.g, this.f, var1, var2);
   }

   public List b() {
      return i;
   }

   static {
      i.add(new BiomeBase.BiomeMeta(class_yn.class, 1, 2, 4));
   }

   public static class WorldGenMonumentStart extends StructureStart {
      private Set c = Sets.newHashSet();
      private boolean d;

      public WorldGenMonumentStart() {
      }

      public WorldGenMonumentStart(World var1, Random var2, int var3, int var4) {
         super(var3, var4);
         this.b(var1, var2, var3, var4);
      }

      private void b(World var1, Random var2, int var3, int var4) {
         var2.setSeed(var1.O());
         long var5 = var2.nextLong();
         long var7 = var2.nextLong();
         long var9 = (long)var3 * var5;
         long var11 = (long)var4 * var7;
         var2.setSeed(var9 ^ var11 ^ var1.O());
         int var13 = var3 * 16 + 8 - 29;
         int var14 = var4 * 16 + 8 - 29;
         EnumDirection var15 = EnumDirection.EnumDirectionLimit.HORIZONTAL.a(var2);
         this.a.add(new WorldGenMonumentPieces.class_h_in_class_avx(var2, var13, var14, var15));
         this.d();
         this.d = true;
      }

      public void a(World var1, Random var2, StructureBoundingBox var3) {
         if(!this.d) {
            this.a.clear();
            this.b(var1, var2, this.e(), this.f());
         }

         super.a(var1, var2, var3);
      }

      public boolean a(class_ahm var1) {
         return this.c.contains(var1)?false:super.a(var1);
      }

      public void b(class_ahm var1) {
         super.b(var1);
         this.c.add(var1);
      }

      public void a(NBTTagCompound var1) {
         super.a(var1);
         NBTTagList var2 = new NBTTagList();
         Iterator var3 = this.c.iterator();

         while(var3.hasNext()) {
            class_ahm var4 = (class_ahm)var3.next();
            NBTTagCompound var5 = new NBTTagCompound();
            var5.a("X", var4.a);
            var5.a("Z", var4.b);
            var2.a((NBTTag)var5);
         }

         var1.a((String)"Processed", (NBTTag)var2);
      }

      public void b(NBTTagCompound var1) {
         super.b(var1);
         if(var1.b("Processed", 9)) {
            NBTTagList var2 = var1.c("Processed", 10);

            for(int var3 = 0; var3 < var2.c(); ++var3) {
               NBTTagCompound var4 = var2.b(var3);
               this.c.add(new class_ahm(var4.h("X"), var4.h("Z")));
            }
         }

      }
   }
}
