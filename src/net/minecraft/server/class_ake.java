package net.minecraft.server;

import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_ake extends Block {
   public static final BlockStateInteger a = BlockStateInteger.a("age", 0, 5);

   protected class_ake() {
      super(Material.k);
      this.w(this.A.b().set(a, Integer.valueOf(0)));
      this.a(CreativeModeTab.c);
      this.a(true);
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return null;
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(!this.b(var1, var2)) {
         var1.b(var2, true);
      } else {
         BlockPosition var5 = var2.a();
         if(var1.d(var5) && var5.q() < 256) {
            int var6 = ((Integer)var3.get(a)).intValue();
            if(var6 < 5 && var4.nextInt(1) == 0) {
               boolean var7 = false;
               boolean var8 = false;
               Block var9 = var1.getType(var2.b()).getBlock();
               int var10;
               if(var9 == Blocks.bH) {
                  var7 = true;
               } else if(var9 == Blocks.cR) {
                  var10 = 1;

                  int var11;
                  for(var11 = 0; var11 < 4; ++var11) {
                     Block var12 = var1.getType(var2.c(var10 + 1)).getBlock();
                     if(var12 != Blocks.cR) {
                        if(var12 == Blocks.bH) {
                           var8 = true;
                        }
                        break;
                     }

                     ++var10;
                  }

                  var11 = 4;
                  if(var8) {
                     ++var11;
                  }

                  if(var10 < 2 || var4.nextInt(var11) >= var10) {
                     var7 = true;
                  }
               } else if(var9 == Blocks.AIR) {
                  var7 = true;
               }

               if(var7 && a(var1, var5, (EnumDirection)null) && var1.d(var2.b(2))) {
                  var1.a((BlockPosition)var2, (IBlockData)Blocks.cR.u(), 2);
                  this.a(var1, var5, var6);
               } else if(var6 < 4) {
                  var10 = var4.nextInt(4);
                  boolean var16 = false;
                  if(var8) {
                     ++var10;
                  }

                  for(int var15 = 0; var15 < var10; ++var15) {
                     EnumDirection var13 = EnumDirection.EnumDirectionLimit.HORIZONTAL.a(var4);
                     BlockPosition var14 = var2.a(var13);
                     if(var1.d(var14) && var1.d(var14.b()) && a(var1, var14, var13.d())) {
                        this.a(var1, var14, var6 + 1);
                        var16 = true;
                     }
                  }

                  if(var16) {
                     var1.a((BlockPosition)var2, (IBlockData)Blocks.cR.u(), 2);
                  } else {
                     this.c(var1, var2);
                  }
               } else if(var6 == 4) {
                  this.c(var1, var2);
               }

            }
         }
      }
   }

   private void a(World var1, BlockPosition var2, int var3) {
      var1.a((BlockPosition)var2, (IBlockData)this.u().set(a, Integer.valueOf(var3)), 2);
      var1.b(1033, var2, 0);
   }

   private void c(World var1, BlockPosition var2) {
      var1.a((BlockPosition)var2, (IBlockData)this.u().set(a, Integer.valueOf(5)), 2);
      var1.b(1034, var2, 0);
   }

   private static boolean a(World var0, BlockPosition var1, EnumDirection var2) {
      Iterator var3 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

      EnumDirection var4;
      do {
         if(!var3.hasNext()) {
            return true;
         }

         var4 = (EnumDirection)var3.next();
      } while(var4 == var2 || var0.d(var1.a(var4)));

      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean a(World var1, BlockPosition var2) {
      return super.a(var1, var2) && this.b(var1, var2);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!this.b(var1, var2)) {
         var1.a((BlockPosition)var2, (Block)this, 1);
      }

   }

   public boolean b(World var1, BlockPosition var2) {
      Block var3 = var1.getType(var2.b()).getBlock();
      if(var3 != Blocks.cR && var3 != Blocks.bH) {
         if(var3 == Blocks.AIR) {
            int var4 = 0;
            Iterator var5 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

            while(var5.hasNext()) {
               EnumDirection var6 = (EnumDirection)var5.next();
               Block var7 = var1.getType(var2.a(var6)).getBlock();
               if(var7 == Blocks.cR) {
                  ++var4;
               } else if(var7 != Blocks.AIR) {
                  return false;
               }
            }

            return var4 == 1;
         } else {
            return false;
         }
      } else {
         return true;
      }
   }

   public void a(World var1, EntityHuman var2, BlockPosition var3, IBlockData var4, TileEntity var5, ItemStack var6) {
      super.a(var1, var2, var3, var4, var5, var6);
      a(var1, var3, new ItemStack(Item.a((Block)this)));
   }

   protected ItemStack u(IBlockData var1) {
      return null;
   }

   public IBlockData a(int var1) {
      return this.u().set(a, Integer.valueOf(var1));
   }

   public int e(IBlockData var1) {
      return ((Integer)var1.get(a)).intValue();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      super.c(var1, var2, var3);
   }

   public static void a(World var0, BlockPosition var1, Random var2, int var3) {
      var0.a((BlockPosition)var1, (IBlockData)Blocks.cR.u(), 2);
      a(var0, var1, var2, var1, var3, 0);
   }

   private static void a(World var0, BlockPosition var1, Random var2, BlockPosition var3, int var4, int var5) {
      int var6 = var2.nextInt(4) + 1;
      if(var5 == 0) {
         ++var6;
      }

      for(int var7 = 0; var7 < var6; ++var7) {
         BlockPosition var8 = var1.b(var7 + 1);
         if(!a(var0, var8, (EnumDirection)null)) {
            return;
         }

         var0.a((BlockPosition)var8, (IBlockData)Blocks.cR.u(), 2);
      }

      boolean var12 = false;
      if(var5 < 4) {
         int var13 = var2.nextInt(4);
         if(var5 == 0) {
            ++var13;
         }

         for(int var9 = 0; var9 < var13; ++var9) {
            EnumDirection var10 = EnumDirection.EnumDirectionLimit.HORIZONTAL.a(var2);
            BlockPosition var11 = var1.b(var6).a(var10);
            if(Math.abs(var11.p() - var3.p()) < var4 && Math.abs(var11.r() - var3.r()) < var4 && var0.d(var11) && var0.d(var11.b()) && a(var0, var11, var10.d())) {
               var12 = true;
               var0.a((BlockPosition)var11, (IBlockData)Blocks.cR.u(), 2);
               a(var0, var11, var2, var3, var4, var5 + 1);
            }
         }
      }

      if(!var12) {
         var0.a((BlockPosition)var1.b(var6), (IBlockData)Blocks.cS.u().set(a, Integer.valueOf(5)), 2);
      }

   }
}
