package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CraftingManager;
import net.minecraft.server.EnumColor;
import net.minecraft.server.Items;
import net.minecraft.server.ItemStack;

public class class_afs {
   private Object[][] a = new Object[][]{{Blocks.R, new ItemStack(Items.m, 9)}, {Blocks.S, new ItemStack(Items.l, 9)}, {Blocks.ah, new ItemStack(Items.k, 9)}, {Blocks.bT, new ItemStack(Items.bY, 9)}, {Blocks.y, new ItemStack(Items.bd, 9, EnumColor.BLUE.b())}, {Blocks.cn, new ItemStack(Items.aE, 9)}, {Blocks.cA, new ItemStack(Items.j, 9, 0)}, {Blocks.cx, new ItemStack(Items.Q, 9)}, {Blocks.cE, new ItemStack(Items.aT, 9)}};

   public void a(CraftingManager var1) {
      for(int var2 = 0; var2 < this.a.length; ++var2) {
         Block var3 = (Block)this.a[var2][0];
         ItemStack var4 = (ItemStack)this.a[var2][1];
         var1.a(new ItemStack(var3), new Object[]{"###", "###", "###", Character.valueOf('#'), var4});
         var1.a(var4, new Object[]{"#", Character.valueOf('#'), var3});
      }

      var1.a(new ItemStack(Items.m), new Object[]{"###", "###", "###", Character.valueOf('#'), Items.bE});
      var1.a(new ItemStack(Items.bE, 9), new Object[]{"#", Character.valueOf('#'), Items.m});
   }
}
