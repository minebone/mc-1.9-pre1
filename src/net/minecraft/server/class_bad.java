package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import java.util.Random;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agn;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_bab;
import net.minecraft.server.class_bae;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_kk;

public class class_bad extends class_bae {
   private final class_bab a;
   private final boolean b;

   public class_bad(class_baq[] var1, class_bab var2, boolean var3) {
      super(var1);
      this.a = var2;
      this.b = var3;
   }

   public ItemStack a(ItemStack var1, Random var2, class_azy var3) {
      class_agn.a(var2, var1, this.a.a(var2), this.b);
      return var1;
   }

   public static class class_a_in_class_bad extends class_bae.class_a_in_class_bae {
      public class_a_in_class_bad() {
         super(new class_kk("enchant_with_levels"), class_bad.class);
      }

      public void a(JsonObject var1, class_bad var2, JsonSerializationContext var3) {
         var1.add("levels", var3.serialize(var2.a));
         var1.addProperty("treasure", Boolean.valueOf(var2.b));
      }

      public class_bad a(JsonObject var1, JsonDeserializationContext var2, class_baq[] var3) {
         class_bab var4 = (class_bab)ChatDeserializer.a(var1, "levels", var2, class_bab.class);
         boolean var5 = ChatDeserializer.a(var1, "treasure", false);
         return new class_bad(var3, var4, var5);
      }

      // $FF: synthetic method
      public class_bae b(JsonObject var1, JsonDeserializationContext var2, class_baq[] var3) {
         return this.a(var1, var2, var3);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(JsonObject var1, class_bae var2, JsonSerializationContext var3) {
         this.a(var1, (class_bad)var2, var3);
      }
   }
}
