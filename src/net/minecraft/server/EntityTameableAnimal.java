package net.minecraft.server;

import com.google.common.base.Optional;
import java.util.UUID;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityAnimal;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NameReferencingFileConverter;
import net.minecraft.server.ScoreboardTeamBase;
import net.minecraft.server.World;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sf;
import net.minecraft.server.class_ul;

public abstract class EntityTameableAnimal extends EntityAnimal implements class_sf {
   protected static final class_ke bv = DataWatcher.a(EntityTameableAnimal.class, class_kg.a);
   protected static final class_ke bw = DataWatcher.a(EntityTameableAnimal.class, class_kg.m);
   protected class_ul bx;

   public EntityTameableAnimal(World var1) {
      super(var1);
      this.da();
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)bv, (Object)Byte.valueOf((byte)0));
      this.datawatcher.a((class_ke)bw, (Object)Optional.absent());
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      if(this.b() == null) {
         var1.a("OwnerUUID", "");
      } else {
         var1.a("OwnerUUID", this.b().toString());
      }

      var1.a("Sitting", this.db());
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      String var2 = "";
      if(var1.b("OwnerUUID", 8)) {
         var2 = var1.l("OwnerUUID");
      } else {
         String var3 = var1.l("Owner");
         var2 = NameReferencingFileConverter.a(this.h(), var3);
      }

      if(!var2.isEmpty()) {
         try {
            this.b(UUID.fromString(var2));
            this.p(true);
         } catch (Throwable var4) {
            this.p(false);
         }
      }

      if(this.bx != null) {
         this.bx.a(var1.p("Sitting"));
      }

      this.q(var1.p("Sitting"));
   }

   public boolean a(EntityHuman var1) {
      return this.cZ() && this.d(var1);
   }

   protected void o(boolean var1) {
      EnumParticle var2 = EnumParticle.HEART;
      if(!var1) {
         var2 = EnumParticle.SMOKE_NORMAL;
      }

      for(int var3 = 0; var3 < 7; ++var3) {
         double var4 = this.random.nextGaussian() * 0.02D;
         double var6 = this.random.nextGaussian() * 0.02D;
         double var8 = this.random.nextGaussian() * 0.02D;
         this.world.a(var2, this.locX + (double)(this.random.nextFloat() * this.width * 2.0F) - (double)this.width, this.locY + 0.5D + (double)(this.random.nextFloat() * this.length), this.locZ + (double)(this.random.nextFloat() * this.width * 2.0F) - (double)this.width, var4, var6, var8, new int[0]);
      }

   }

   public boolean cZ() {
      return (((Byte)this.datawatcher.a(bv)).byteValue() & 4) != 0;
   }

   public void p(boolean var1) {
      byte var2 = ((Byte)this.datawatcher.a(bv)).byteValue();
      if(var1) {
         this.datawatcher.b(bv, Byte.valueOf((byte)(var2 | 4)));
      } else {
         this.datawatcher.b(bv, Byte.valueOf((byte)(var2 & -5)));
      }

      this.da();
   }

   protected void da() {
   }

   public boolean db() {
      return (((Byte)this.datawatcher.a(bv)).byteValue() & 1) != 0;
   }

   public void q(boolean var1) {
      byte var2 = ((Byte)this.datawatcher.a(bv)).byteValue();
      if(var1) {
         this.datawatcher.b(bv, Byte.valueOf((byte)(var2 | 1)));
      } else {
         this.datawatcher.b(bv, Byte.valueOf((byte)(var2 & -2)));
      }

   }

   public UUID b() {
      return (UUID)((Optional)this.datawatcher.a(bw)).orNull();
   }

   public void b(UUID var1) {
      this.datawatcher.b(bw, Optional.fromNullable(var1));
   }

   public class_rz dc() {
      try {
         UUID var1 = this.b();
         return var1 == null?null:this.world.b(var1);
      } catch (IllegalArgumentException var2) {
         return null;
      }
   }

   public boolean d(class_rz var1) {
      return var1 == this.dc();
   }

   public class_ul dd() {
      return this.bx;
   }

   public boolean a(class_rz var1, class_rz var2) {
      return true;
   }

   public ScoreboardTeamBase aN() {
      if(this.cZ()) {
         class_rz var1 = this.dc();
         if(var1 != null) {
            return var1.aN();
         }
      }

      return super.aN();
   }

   public boolean r(Entity var1) {
      if(this.cZ()) {
         class_rz var2 = this.dc();
         if(var1 == var2) {
            return true;
         }

         if(var2 != null) {
            return var2.r(var1);
         }
      }

      return super.r(var1);
   }

   public void a(DamageSource var1) {
      if(!this.world.E && this.world.U().b("showDeathMessages") && this.dc() instanceof EntityPlayer) {
         this.dc().a((IChatBaseComponent)this.bT().b());
      }

      super.a((DamageSource)var1);
   }

   // $FF: synthetic method
   public Entity p_() {
      return this.dc();
   }
}
