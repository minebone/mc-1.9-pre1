package net.minecraft.server;

import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.class_aoc;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_amd extends class_aoc {
   public class_amd() {
      super(Material.b, MaterialMapColor.t);
      this.w(this.A.b().set(c, EnumDirection.class_a_in_class_cq.Y));
      this.a(CreativeModeTab.b);
   }

   public void a(World var1, BlockPosition var2, Entity var3, float var4) {
      var3.e(var4, 0.2F);
   }
}
