package net.minecraft.server;

import java.util.UUID;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EntityTypes;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahj;
import net.minecraft.server.class_ali;
import net.minecraft.server.class_amn;
import net.minecraft.server.class_aqj;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_qk;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sc;

public class class_aet extends Item {
   public class_aet() {
      this.a(CreativeModeTab.f);
   }

   public String a(ItemStack var1) {
      String var2 = ("" + class_di.a(this.a() + ".name")).trim();
      String var3 = h(var1);
      if(var3 != null) {
         var2 = var2 + " " + class_di.a("entity." + var3 + ".name");
      }

      return var2;
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      if(var3.E) {
         return EnumResult.SUCCESS;
      } else if(!var2.a(var4.a(var6), var6, var1)) {
         return EnumResult.FAIL;
      } else {
         IBlockData var10 = var3.getType(var4);
         if(var10.getBlock() == Blocks.ac) {
            TileEntity var11 = var3.r(var4);
            if(var11 instanceof class_aqj) {
               class_ahj var12 = ((class_aqj)var11).b();
               var12.a(h(var1));
               var11.v_();
               var3.a(var4, var10, var10, 3);
               if(!var2.abilities.d) {
                  --var1.b;
               }

               return EnumResult.SUCCESS;
            }
         }

         var4 = var4.a(var6);
         double var14 = 0.0D;
         if(var6 == EnumDirection.UP && var10 instanceof class_ali) {
            var14 = 0.5D;
         }

         Entity var13 = a(var3, h(var1), (double)var4.p() + 0.5D, (double)var4.q() + var14, (double)var4.r() + 0.5D);
         if(var13 != null) {
            if(var13 instanceof class_rz && var1.s()) {
               var13.c(var1.q());
            }

            a(var3, var2, var1, var13);
            if(!var2.abilities.d) {
               --var1.b;
            }
         }

         return EnumResult.SUCCESS;
      }
   }

   public static void a(World var0, EntityHuman var1, ItemStack var2, Entity var3) {
      MinecraftServer var4 = var0.u();
      if(var4 != null && var3 != null) {
         NBTTagCompound var5 = var2.o();
         if(var5 != null && var5.b("EntityTag", 10)) {
            if(!var0.E && var3.bq() && (var1 == null || !var4.getPlayerList().h(var1.getProfile()))) {
               return;
            }

            NBTTagCompound var6 = new NBTTagCompound();
            var3.e(var6);
            UUID var7 = var3.getUniqueId();
            var6.a(var5.o("EntityTag"));
            var3.a(var7);
            var3.f(var6);
         }

      }
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      if(var2.E) {
         return new class_qo(EnumResult.PASS, var1);
      } else {
         MovingObjectPosition var5 = this.a(var2, var3, true);
         if(var5 != null && var5.a == MovingObjectPosition.EnumMovingObjectType.BLOCK) {
            BlockPosition var6 = var5.a();
            if(!(var2.getType(var6).getBlock() instanceof class_amn)) {
               return new class_qo(EnumResult.PASS, var1);
            } else if(var2.a(var3, var6) && var3.a(var6, var5.b, var1)) {
               Entity var7 = a(var2, h(var1), (double)var6.p() + 0.5D, (double)var6.q() + 0.5D, (double)var6.r() + 0.5D);
               if(var7 == null) {
                  return new class_qo(EnumResult.PASS, var1);
               } else {
                  if(var7 instanceof class_rz && var1.s()) {
                     var7.c(var1.q());
                  }

                  a(var2, var3, var1, var7);
                  if(!var3.abilities.d) {
                     --var1.b;
                  }

                  var3.b(StatisticList.b((Item)this));
                  return new class_qo(EnumResult.SUCCESS, var1);
               }
            } else {
               return new class_qo(EnumResult.FAIL, var1);
            }
         } else {
            return new class_qo(EnumResult.PASS, var1);
         }
      }
   }

   public static Entity a(World var0, String var1, double var2, double var4, double var6) {
      if(var1 != null && EntityTypes.a.containsKey(var1)) {
         Entity var8 = null;

         for(int var9 = 0; var9 < 1; ++var9) {
            var8 = EntityTypes.b(var1, var0);
            if(var8 instanceof class_rz) {
               EntityInsentient var10 = (EntityInsentient)var8;
               var8.b(var2, var4, var6, MathHelper.g(var0.r.nextFloat() * 360.0F), 0.0F);
               var10.aO = var10.yaw;
               var10.aM = var10.yaw;
               var10.a((class_qk)var0.D(new BlockPosition(var10)), (class_sc)null);
               var0.a(var8);
               var10.D();
            }
         }

         return var8;
      } else {
         return null;
      }
   }

   public static String h(ItemStack var0) {
      NBTTagCompound var1 = var0.o();
      if(var1 == null) {
         return null;
      } else if(!var1.b("EntityTag", 10)) {
         return null;
      } else {
         NBTTagCompound var2 = var1.o("EntityTag");
         return !var2.b("id", 8)?null:var2.l("id");
      }
   }
}
