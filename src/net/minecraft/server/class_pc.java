package net.minecraft.server;

import net.minecraft.server.EntityHanging;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.class_ox;
import net.minecraft.server.class_oy;
import net.minecraft.server.class_oz;
import net.minecraft.server.class_pa;
import net.minecraft.server.class_pb;
import net.minecraft.server.class_pd;
import net.minecraft.server.class_pe;
import net.minecraft.server.class_pf;
import net.minecraft.server.class_pg;
import net.minecraft.server.class_ph;
import net.minecraft.server.class_pj;
import net.minecraft.server.class_pk;
import net.minecraft.server.class_pl;
import net.minecraft.server.class_pm;
import net.minecraft.server.class_pn;
import net.minecraft.server.class_po;
import net.minecraft.server.class_pp;
import net.minecraft.server.class_pq;
import net.minecraft.server.class_pr;
import net.minecraft.server.class_pu;
import net.minecraft.server.class_pv;
import net.minecraft.server.class_pw;
import net.minecraft.server.class_px;

public class class_pc {
   private static void a(class_pb var0) {
      var0.a((class_oy)class_oz.ENTITY, (class_ox)(new class_pf()));
      var0.a((class_oy)class_oz.BLOCK_ENTITY, (class_ox)(new class_pq()));
      var0.a((class_oy)class_oz.ITEM_INSTANCE, (class_ox)(new class_pm()));
      var0.a((class_oy)class_oz.ITEM_INSTANCE, (class_ox)(new class_pp()));
      var0.a((class_oy)class_oz.ITEM_INSTANCE, (class_ox)(new class_pr()));
      var0.a((class_oy)class_oz.ENTITY, (class_ox)(new class_pn()));
      var0.a((class_oy)class_oz.BLOCK_ENTITY, (class_ox)(new class_po()));
      var0.a((class_oy)class_oz.ENTITY, (class_ox)(new class_pl()));
      var0.a((class_oy)class_oz.ENTITY, (class_ox)(new class_pg()));
      var0.a((class_oy)class_oz.ENTITY, (class_ox)(new class_ph()));
      var0.a((class_oy)class_oz.ENTITY, (class_ox)(new EntityHanging()));
      var0.a((class_oy)class_oz.ENTITY, (class_ox)(new class_pj()));
      var0.a((class_oy)class_oz.ENTITY, (class_ox)(new class_pk()));
      var0.a((class_oy)class_oz.ENTITY, (class_ox)(new class_pe()));
   }

   public static class_pb a() {
      class_pb var0 = new class_pb(165);
      var0.a(class_oz.LEVEL, new class_pd() {
         public NBTTagCompound a(class_pa var1, NBTTagCompound var2, int var3) {
            if(var2.b("Player", 10)) {
               var2.a((String)"Player", (NBTTag)var1.a(class_oz.PLAYER, var2.o("Player"), var3));
            }

            return var2;
         }
      });
      var0.a(class_oz.PLAYER, new class_pd() {
         public NBTTagCompound a(class_pa var1, NBTTagCompound var2, int var3) {
            class_pc.b(var1, var2, var3, "Inventory");
            class_pc.b(var1, var2, var3, "EnderItems");
            return var2;
         }
      });
      var0.a(class_oz.CHUNK, new class_pd() {
         public NBTTagCompound a(class_pa var1, NBTTagCompound var2, int var3) {
            if(var2.b("Level", 10)) {
               NBTTagCompound var4 = var2.o("Level");
               NBTTagList var5;
               int var6;
               if(var4.b("Entities", 9)) {
                  var5 = var4.c("Entities", 10);

                  for(var6 = 0; var6 < var5.c(); ++var6) {
                     var5.a(var6, var1.a(class_oz.ENTITY, (NBTTagCompound)var5.h(var6), var3));
                  }
               }

               if(var4.b("TileEntities", 9)) {
                  var5 = var4.c("TileEntities", 10);

                  for(var6 = 0; var6 < var5.c(); ++var6) {
                     var5.a(var6, var1.a(class_oz.BLOCK_ENTITY, (NBTTagCompound)var5.h(var6), var3));
                  }
               }
            }

            return var2;
         }
      });
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_pu("Item", new String[]{"Item"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_pu("ThrownPotion", new String[]{"Potion"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_pu("ItemFrame", new String[]{"Item"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_pu("FireworksRocketEntity", new String[]{"FireworksItem"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_pu("TippedArrow", new String[]{"Item"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("MinecartChest", new String[]{"Items"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("MinecartHopper", new String[]{"Items"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Enderman", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("ArmorStand", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Bat", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Blaze", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("CaveSpider", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Chicken", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Cow", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Creeper", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("EnderDragon", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Endermite", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Ghast", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Giant", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Guardian", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("LavaSlime", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Mob", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Monster", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("MushroomCow", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Ozelot", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Pig", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("PigZombie", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Rabbit", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Sheep", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Shulker", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Silverfish", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Skeleton", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Slime", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("SnowMan", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Spider", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Squid", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("VillagerGolem", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Witch", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("WitherBoss", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Wolf", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Zombie", new String[]{"ArmorItems", "HandItems"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("EntityHorse", new String[]{"ArmorItems", "HandItems", "Items"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_pu("EntityHorse", new String[]{"ArmorItem", "SaddleItem"})));
      var0.a((class_oz)class_oz.ENTITY, (class_pd)(new class_px("Villager", new String[]{"ArmorItems", "HandItems", "Inventory"})));
      var0.a(class_oz.ENTITY, new class_pd() {
         public NBTTagCompound a(class_pa var1, NBTTagCompound var2, int var3) {
            if("Villager".equals(var2.l("id")) && var2.b("Offers", 10)) {
               NBTTagCompound var4 = var2.o("Offers");
               if(var4.b("Recipes", 9)) {
                  NBTTagList var5 = var4.c("Recipes", 10);

                  for(int var6 = 0; var6 < var5.c(); ++var6) {
                     NBTTagCompound var7 = var5.b(var6);
                     class_pc.a(var1, var7, var3, "buy");
                     class_pc.a(var1, var7, var3, "buyB");
                     class_pc.a(var1, var7, var3, "sell");
                     var5.a(var6, var7);
                  }
               }
            }

            return var2;
         }
      });
      var0.a(class_oz.ENTITY, new class_pd() {
         public NBTTagCompound a(class_pa var1, NBTTagCompound var2, int var3) {
            if("MinecartSpawner".equals(var2.l("id"))) {
               var2.a("id", "MobSpawner");
               var1.a(class_oz.BLOCK_ENTITY, var2, var3);
               var2.a("id", "MinecartSpawner");
            }

            return var2;
         }
      });
      var0.a(class_oz.ENTITY, new class_pd() {
         public NBTTagCompound a(class_pa var1, NBTTagCompound var2, int var3) {
            if("MinecartCommandBlock".equals(var2.l("id"))) {
               var2.a("id", "Control");
               var1.a(class_oz.BLOCK_ENTITY, var2, var3);
               var2.a("id", "MinecartCommandBlock");
            }

            return var2;
         }
      });
      var0.a((class_oz)class_oz.BLOCK_ENTITY, (class_pd)(new class_px("Furnace", new String[]{"Items"})));
      var0.a((class_oz)class_oz.BLOCK_ENTITY, (class_pd)(new class_px("Chest", new String[]{"Items"})));
      var0.a((class_oz)class_oz.BLOCK_ENTITY, (class_pd)(new class_px("Trap", new String[]{"Items"})));
      var0.a((class_oz)class_oz.BLOCK_ENTITY, (class_pd)(new class_px("Dropper", new String[]{"Items"})));
      var0.a((class_oz)class_oz.BLOCK_ENTITY, (class_pd)(new class_px("Cauldron", new String[]{"Items"})));
      var0.a((class_oz)class_oz.BLOCK_ENTITY, (class_pd)(new class_px("Hopper", new String[]{"Items"})));
      var0.a((class_oz)class_oz.BLOCK_ENTITY, (class_pd)(new class_pu("RecordPlayer", new String[]{"RecordItem"})));
      var0.a(class_oz.BLOCK_ENTITY, new class_pd() {
         public NBTTagCompound a(class_pa var1, NBTTagCompound var2, int var3) {
            if("MobSpawner".equals(var2.l("id"))) {
               if(var2.b("SpawnPotentials", 9)) {
                  NBTTagList var4 = var2.c("SpawnPotentials", 10);

                  for(int var5 = 0; var5 < var4.c(); ++var5) {
                     NBTTagCompound var6 = var4.b(var5);
                     var6.a((String)"Entity", (NBTTag)var1.a(class_oz.ENTITY, var6.o("Entity"), var3));
                  }
               }

               var2.a((String)"SpawnData", (NBTTag)var1.a(class_oz.ENTITY, var2.o("SpawnData"), var3));
            }

            return var2;
         }
      });
      var0.a((class_oz)class_oz.ITEM_INSTANCE, (class_pd)(new class_pv()));
      var0.a((class_oz)class_oz.ITEM_INSTANCE, (class_pd)(new class_pw()));
      a(var0);
      return var0;
   }

   public static NBTTagCompound a(class_pa var0, NBTTagCompound var1, int var2, String var3) {
      if(var1.b(var3, 10)) {
         var1.a((String)var3, (NBTTag)var0.a(class_oz.ITEM_INSTANCE, var1.o(var3), var2));
      }

      return var1;
   }

   public static NBTTagCompound b(class_pa var0, NBTTagCompound var1, int var2, String var3) {
      if(var1.b(var3, 9)) {
         NBTTagList var4 = var1.c(var3, 10);

         for(int var5 = 0; var5 < var4.c(); ++var5) {
            var4.a(var5, var0.a(class_oz.ITEM_INSTANCE, var4.b(var5), var2));
         }
      }

      return var1;
   }
}
