package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.ScoreboardTeamBase;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_xp;
import net.minecraft.server.class_yt;

public final class class_ru {
   public static final Predicate a = new Predicate() {
      public boolean a(Entity var1) {
         return var1.at();
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((Entity)var1);
      }
   };
   public static final Predicate b = new Predicate() {
      public boolean a(Entity var1) {
         return var1.at() && !var1.aI() && !var1.aH();
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((Entity)var1);
      }
   };
   public static final Predicate c = new Predicate() {
      public boolean a(Entity var1) {
         return var1 instanceof IInventory && var1.at();
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((Entity)var1);
      }
   };
   public static final Predicate d = new Predicate() {
      public boolean a(Entity var1) {
         return !(var1 instanceof EntityHuman) || !((EntityHuman)var1).y() && !((EntityHuman)var1).l_();
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((Entity)var1);
      }
   };
   public static final Predicate e = new Predicate() {
      public boolean a(Entity var1) {
         return !(var1 instanceof EntityHuman) || !((EntityHuman)var1).y();
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((Entity)var1);
      }
   };
   public static final Predicate f = new Predicate() {
      public boolean a(Entity var1) {
         return var1 instanceof class_yt && var1.at();
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((Entity)var1);
      }
   };

   public static Predicate a(final double var0, final double var2, final double var4, double var6) {
      final double var8 = var6 * var6;
      return new Predicate() {
         public boolean a(Entity var1) {
            return var1 != null && var1.e(var0, var2, var4) <= var8;
         }

         // $FF: synthetic method
         public boolean apply(Object var1) {
            return this.a((Entity)var1);
         }
      };
   }

   public static Predicate a(final Entity var0) {
      final ScoreboardTeamBase var1 = var0.aN();
      final ScoreboardTeamBase.class_a_in_class_bbq var2 = var1 == null?ScoreboardTeamBase.class_a_in_class_bbq.ALWAYS:var1.k();
      return var2 == ScoreboardTeamBase.class_a_in_class_bbq.NEVER?Predicates.alwaysFalse():Predicates.and(e, new Predicate() {
         public boolean a(Entity var1x) {
            if(!var1x.ap()) {
               return false;
            } else if(var0.world.E && (!(var1x instanceof EntityHuman) || !((EntityHuman)var1x).cI())) {
               return false;
            } else {
               ScoreboardTeamBase var2x = var1x.aN();
               ScoreboardTeamBase.class_a_in_class_bbq var3 = var2x == null?ScoreboardTeamBase.class_a_in_class_bbq.ALWAYS:var2x.k();
               if(var3 == ScoreboardTeamBase.class_a_in_class_bbq.NEVER) {
                  return false;
               } else {
                  boolean var4 = var1 != null && var1.a(var2x);
                  return (var2 == ScoreboardTeamBase.class_a_in_class_bbq.HIDE_FOR_OWN_TEAM || var3 == ScoreboardTeamBase.class_a_in_class_bbq.HIDE_FOR_OWN_TEAM) && var4?false:var2 != ScoreboardTeamBase.class_a_in_class_bbq.HIDE_FOR_OTHER_TEAMS && var3 != ScoreboardTeamBase.class_a_in_class_bbq.HIDE_FOR_OTHER_TEAMS || var4;
               }
            }
         }

         // $FF: synthetic method
         public boolean apply(Object var1x) {
            return this.a((Entity)var1x);
         }
      });
   }

   public static class class_a_in_class_ru implements Predicate {
      private final ItemStack a;

      public class_a_in_class_ru(ItemStack var1) {
         this.a = var1;
      }

      public boolean a(Entity var1) {
         if(!var1.at()) {
            return false;
         } else if(!(var1 instanceof class_rz)) {
            return false;
         } else {
            class_rz var2 = (class_rz)var1;
            return var2.a(EntityInsentient.d(this.a)) != null?false:(var2 instanceof EntityInsentient?((EntityInsentient)var2).cM():(var2 instanceof class_xp?true:var2 instanceof EntityHuman));
         }
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((Entity)var1);
      }
   }
}
