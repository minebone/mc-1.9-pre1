package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Chunk;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.HttpUtilities;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.TileEntityBeacon;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_aoa;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.IInventory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;

public class class_ajp extends class_ajm {
   public class_ajp() {
      super(Material.s, MaterialMapColor.G);
      this.c(3.0F);
      this.a(CreativeModeTab.f);
   }

   public TileEntity a(World var1, int var2) {
      return new TileEntityBeacon();
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(var1.E) {
         return true;
      } else {
         TileEntity var11 = var1.r(var2);
         if(var11 instanceof TileEntityBeacon) {
            var4.openContainer((IInventory)((TileEntityBeacon)var11));
            var4.b(StatisticList.P);
         }

         return true;
      }
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.MODEL;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, class_rz var4, ItemStack var5) {
      super.a(var1, var2, var3, var4, var5);
      if(var5.s()) {
         TileEntity var6 = var1.r(var2);
         if(var6 instanceof TileEntityBeacon) {
            ((TileEntityBeacon)var6).a(var5.q());
         }
      }

   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      TileEntity var5 = var1.r(var2);
      if(var5 instanceof TileEntityBeacon) {
         ((TileEntityBeacon)var5).m();
         var1.c(var2, this, 1, 0);
      }

   }

   public static void c(final World var0, final BlockPosition var1) {
      HttpUtilities.a.submit(new Runnable() {
         public void run() {
            Chunk var1x = var0.f(var1);

            for(int var2 = var1.q() - 1; var2 >= 0; --var2) {
               final BlockPosition var3 = new BlockPosition(var1.p(), var2, var1.r());
               if(!var1x.c(var3)) {
                  break;
               }

               IBlockData var4 = var0.getType(var3);
               if(var4.getBlock() == Blocks.bY) {
                  ((WorldServer)var0).a(new Runnable() {
                     public void run() {
                        TileEntity var1x = var0.r(var3);
                        if(var1x instanceof TileEntityBeacon) {
                           ((TileEntityBeacon)var1x).m();
                           var0.c(var3, Blocks.bY, 1, 0);
                        }

                     }
                  });
               }
            }

         }
      });
   }
}
