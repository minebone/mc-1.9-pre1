package net.minecraft.server;

import net.minecraft.server.BiomeBase;
import net.minecraft.server.Blocks;

public class class_aie extends BiomeBase {
   public class_aie(BiomeBase.class_a_in_class_aif var1) {
      super(var1);
      this.v.clear();
      this.r = Blocks.m.u();
      this.s = Blocks.m.u();
      this.t.z = -999;
      this.t.C = 0;
      this.t.E = 0;
      this.t.F = 0;
   }
}
