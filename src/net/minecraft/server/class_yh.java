package net.minecraft.server;

import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_azs;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sz;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_to;
import net.minecraft.server.class_tr;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_um;
import net.minecraft.server.class_uu;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_wa;
import net.minecraft.server.class_xz;
import net.minecraft.server.class_yp;
import net.minecraft.server.class_ys;
import net.minecraft.server.class_yv;

public class class_yh extends class_yp {
   private static final class_ke a = DataWatcher.a(class_yh.class, class_kg.b);
   private static final class_ke b = DataWatcher.a(class_yh.class, class_kg.h);
   private static final class_ke c = DataWatcher.a(class_yh.class, class_kg.h);
   private int bv;
   private int bw;
   private int bx = 30;
   private int by = 3;
   private int bz = 0;

   public class_yh(World var1) {
      super(var1);
      this.a(0.6F, 1.7F);
   }

   protected void r() {
      this.bp.a(1, new class_tg(this));
      this.bp.a(2, new class_um(this));
      this.bp.a(3, new class_sz(this, class_wa.class, 6.0F, 1.0D, 1.2D));
      this.bp.a(4, new class_tr(this, 1.0D, false));
      this.bp.a(5, new class_uf(this, 0.8D));
      this.bp.a(6, new class_to(this, EntityHuman.class, 8.0F));
      this.bp.a(6, new class_ue(this));
      this.bq.a(1, new class_ux(this, EntityHuman.class, true));
      this.bq.a(2, new class_uu(this, false, new Class[0]));
   }

   protected void bz() {
      super.bz();
      this.a((class_sk)class_ys.d).a(0.25D);
   }

   public int aV() {
      return this.A() == null?3:3 + (int)(this.bP() - 1.0F);
   }

   public void e(float var1, float var2) {
      super.e(var1, var2);
      this.bw = (int)((float)this.bw + var1 * 1.5F);
      if(this.bw > this.bx - 5) {
         this.bw = this.bx - 5;
      }

   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)a, (Object)Integer.valueOf(-1));
      this.datawatcher.a((class_ke)b, (Object)Boolean.valueOf(false));
      this.datawatcher.a((class_ke)c, (Object)Boolean.valueOf(false));
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      if(((Boolean)this.datawatcher.a(b)).booleanValue()) {
         var1.a("powered", true);
      }

      var1.a("Fuse", (short)this.bx);
      var1.a("ExplosionRadius", (byte)this.by);
      var1.a("ignited", this.db());
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.datawatcher.b(b, Boolean.valueOf(var1.p("powered")));
      if(var1.b("Fuse", 99)) {
         this.bx = var1.g("Fuse");
      }

      if(var1.b("ExplosionRadius", 99)) {
         this.by = var1.f("ExplosionRadius");
      }

      if(var1.p("ignited")) {
         this.dc();
      }

   }

   public void m() {
      if(this.at()) {
         this.bv = this.bw;
         if(this.db()) {
            this.a(1);
         }

         int var1 = this.da();
         if(var1 > 0 && this.bw == 0) {
            this.a(class_ng.ar, 1.0F, 0.5F);
         }

         this.bw += var1;
         if(this.bw < 0) {
            this.bw = 0;
         }

         if(this.bw >= this.bx) {
            this.bw = this.bx;
            this.df();
         }
      }

      super.m();
   }

   protected class_nf bQ() {
      return class_ng.aq;
   }

   protected class_nf bR() {
      return class_ng.ap;
   }

   public void a(DamageSource var1) {
      super.a(var1);
      if(this.world.U().b("doMobLoot")) {
         if(var1.j() instanceof class_yv) {
            int var2 = Item.a(Items.cA);
            int var3 = Item.a(Items.cL);
            int var4 = var2 + this.random.nextInt(var3 - var2 + 1);
            this.a(Item.c(var4), 1);
         } else if(var1.j() instanceof class_yh && var1.j() != this && ((class_yh)var1.j()).o() && ((class_yh)var1.j()).dd()) {
            ((class_yh)var1.j()).de();
            this.a(new ItemStack(Items.ch, 1, 4), 0.0F);
         }
      }

   }

   public boolean B(Entity var1) {
      return true;
   }

   public boolean o() {
      return ((Boolean)this.datawatcher.a(b)).booleanValue();
   }

   protected class_kk J() {
      return class_azs.p;
   }

   public int da() {
      return ((Integer)this.datawatcher.a(a)).intValue();
   }

   public void a(int var1) {
      this.datawatcher.b(a, Integer.valueOf(var1));
   }

   public void a(class_xz var1) {
      super.a(var1);
      this.datawatcher.b(b, Boolean.valueOf(true));
   }

   protected boolean a(EntityHuman var1, EnumHand var2, ItemStack var3) {
      if(var3 != null && var3.b() == Items.d) {
         this.world.a(var1, this.locX, this.locY, this.locZ, class_ng.bu, this.by(), 1.0F, this.random.nextFloat() * 0.4F + 0.8F);
         var1.a((EnumHand)var2);
         if(!this.world.E) {
            this.dc();
            var3.a(1, (class_rz)var1);
            return true;
         }
      }

      return super.a(var1, var2, var3);
   }

   private void df() {
      if(!this.world.E) {
         boolean var1 = this.world.U().b("mobGriefing");
         float var2 = this.o()?2.0F:1.0F;
         this.aT = true;
         this.world.a(this, this.locX, this.locY, this.locZ, (float)this.by * var2, var1);
         this.S();
      }

   }

   public boolean db() {
      return ((Boolean)this.datawatcher.a(c)).booleanValue();
   }

   public void dc() {
      this.datawatcher.b(c, Boolean.valueOf(true));
   }

   public boolean dd() {
      return this.bz < 1 && this.world.U().b("doMobLoot");
   }

   public void de() {
      ++this.bz;
   }
}
