package net.minecraft.server;

import net.minecraft.server.CraftingManager;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.ItemStack;

public class class_afj {
   private String[][] a = new String[][]{{"XXX", "X X"}, {"X X", "XXX", "XXX"}, {"XXX", "X X", "X X"}, {"X X", "X X"}};
   private Item[][] b = new Item[][]{{Items.aM, Items.l, Items.k, Items.m}, {Items.S, Items.aa, Items.ae, Items.ai}, {Items.T, Items.ab, Items.af, Items.aj}, {Items.U, Items.ac, Items.ag, Items.ak}, {Items.V, Items.ad, Items.ah, Items.al}};

   public void a(CraftingManager var1) {
      for(int var2 = 0; var2 < this.b[0].length; ++var2) {
         Item var3 = this.b[0][var2];

         for(int var4 = 0; var4 < this.b.length - 1; ++var4) {
            Item var5 = this.b[var4 + 1][var2];
            var1.a(new ItemStack(var5), new Object[]{this.a[var4], Character.valueOf('X'), var3});
         }
      }

   }
}
