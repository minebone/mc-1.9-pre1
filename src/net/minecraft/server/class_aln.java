package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_amh;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;

public class class_aln extends class_amh {
   public static final BlockStateInteger a = BlockStateInteger.a("age", 0, 3);

   public class_aln() {
      this.w(this.A.b().set(a, Integer.valueOf(0)));
   }

   public int e(IBlockData var1) {
      return ((Integer)var1.get(a)).intValue();
   }

   public IBlockData a(int var1) {
      return this.u().set(a, Integer.valueOf(MathHelper.a(var1, 0, 3)));
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if((var4.nextInt(3) == 0 || this.c(var1, var2) < 4) && var1.k(var2) > 11 - ((Integer)var3.get(a)).intValue() - var3.c()) {
         this.a(var1, var2, var3, var4, true);
      } else {
         var1.a((BlockPosition)var2, (Block)this, MathHelper.a((Random)var4, 20, 40));
      }

   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(var4 == this) {
         int var5 = this.c(var1, var2);
         if(var5 < 2) {
            this.b(var1, var2);
         }
      }

   }

   private int c(World var1, BlockPosition var2) {
      int var3 = 0;
      EnumDirection[] var4 = EnumDirection.values();
      int var5 = var4.length;

      for(int var6 = 0; var6 < var5; ++var6) {
         EnumDirection var7 = var4[var6];
         if(var1.getType(var2.a(var7)).getBlock() == this) {
            ++var3;
            if(var3 >= 4) {
               return var3;
            }
         }
      }

      return var3;
   }

   protected void a(World var1, BlockPosition var2, IBlockData var3, Random var4, boolean var5) {
      int var6 = ((Integer)var3.get(a)).intValue();
      if(var6 < 3) {
         var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Integer.valueOf(var6 + 1)), 2);
         var1.a((BlockPosition)var2, (Block)this, MathHelper.a((Random)var4, 20, 40));
      } else {
         this.b(var1, var2);
         if(var5) {
            EnumDirection[] var7 = EnumDirection.values();
            int var8 = var7.length;

            for(int var9 = 0; var9 < var8; ++var9) {
               EnumDirection var10 = var7[var9];
               BlockPosition var11 = var2.a(var10);
               IBlockData var12 = var1.getType(var11);
               if(var12.getBlock() == this) {
                  this.a(var1, var11, var12, var4, false);
               }
            }
         }
      }

   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return null;
   }
}
