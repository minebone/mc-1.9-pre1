package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Vec3D;
import net.minecraft.server.BlockPosition;

public class MovingObjectPosition {
   private BlockPosition e;
   public MovingObjectPosition.EnumMovingObjectType a;
   public EnumDirection b;
   public Vec3D c;
   public Entity d;

   public MovingObjectPosition(Vec3D var1, EnumDirection var2, BlockPosition var3) {
      this(MovingObjectPosition.EnumMovingObjectType.BLOCK, var1, var2, var3);
   }

   public MovingObjectPosition(Vec3D var1, EnumDirection var2) {
      this(MovingObjectPosition.EnumMovingObjectType.BLOCK, var1, var2, BlockPosition.a);
   }

   public MovingObjectPosition(Entity var1) {
      this(var1, new Vec3D(var1.locX, var1.locY, var1.locZ));
   }

   public MovingObjectPosition(MovingObjectPosition.EnumMovingObjectType var1, Vec3D var2, EnumDirection var3, BlockPosition var4) {
      this.a = var1;
      this.e = var4;
      this.b = var3;
      this.c = new Vec3D(var2.b, var2.c, var2.d);
   }

   public MovingObjectPosition(Entity var1, Vec3D var2) {
      this.a = MovingObjectPosition.EnumMovingObjectType.ENTITY;
      this.d = var1;
      this.c = var2;
   }

   public BlockPosition a() {
      return this.e;
   }

   public String toString() {
      return "HitResult{type=" + this.a + ", blockpos=" + this.e + ", f=" + this.b + ", pos=" + this.c + ", entity=" + this.d + '}';
   }

   public static enum EnumMovingObjectType {
      MISS,
      BLOCK,
      ENTITY;
   }
}
