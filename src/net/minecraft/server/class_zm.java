package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityFireball;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.World;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ro;
import net.minecraft.server.class_rz;

public class class_zm extends EntityFireball {
   public class_zm(World var1) {
      super(var1);
      this.a(0.3125F, 0.3125F);
   }

   public class_zm(World var1, class_rz var2, double var3, double var5, double var7) {
      super(var1, var2, var3, var5, var7);
      this.a(0.3125F, 0.3125F);
   }

   protected void a(MovingObjectPosition var1) {
      if(!this.world.E) {
         List var2 = this.world.a(class_rz.class, this.bk().b(4.0D, 2.0D, 4.0D));
         class_ro var3 = new class_ro(this.world, this.locX, this.locY, this.locZ);
         var3.a(this.a);
         var3.a(EnumParticle.DRAGON_BREATH);
         var3.a(3.0F);
         var3.b(2400);
         var3.c((7.0F - var3.j()) / (float)var3.o());
         var3.a(new MobEffect(MobEffectList.g, 1, 1));
         if(!var2.isEmpty()) {
            Iterator var4 = var2.iterator();

            while(var4.hasNext()) {
               class_rz var5 = (class_rz)var4.next();
               double var6 = this.h(var5);
               if(var6 < 16.0D) {
                  var3.b(var5.locX, var5.locY, var5.locZ);
                  break;
               }
            }
         }

         this.world.b(2006, new BlockPosition(this.locX, this.locY, this.locZ), 0);
         this.world.a((Entity)var3);
         this.S();
      }

   }

   public boolean ao() {
      return false;
   }

   public boolean a(DamageSource var1, float var2) {
      return false;
   }

   protected EnumParticle j() {
      return EnumParticle.DRAGON_BREATH;
   }

   protected boolean k() {
      return false;
   }
}
