package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_auc;
import net.minecraft.server.BlockPosition;

public class class_auq extends class_auc {
   public boolean b(World var1, Random var2, BlockPosition var3) {
      for(int var4 = 0; var4 < 64; ++var4) {
         BlockPosition var5 = var3.a(var2.nextInt(8) - var2.nextInt(8), var2.nextInt(4) - var2.nextInt(4), var2.nextInt(8) - var2.nextInt(8));
         if(Blocks.bk.a(var1, var5) && var1.getType(var5.b()).getBlock() == Blocks.c) {
            var1.a((BlockPosition)var5, (IBlockData)Blocks.bk.u(), 2);
         }
      }

      return true;
   }
}
