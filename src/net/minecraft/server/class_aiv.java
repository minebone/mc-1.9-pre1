package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.BlockDirt;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.class_aiz;
import net.minecraft.server.class_ate;
import net.minecraft.server.BlockPosition;

public class class_aiv extends class_aiz {
   public class_aiv(BiomeBase.class_a_in_class_aif var1) {
      super(var1);
      this.t.z = 2;
      this.t.A = 2;
      this.t.B = 5;
   }

   public void a(World var1, Random var2, class_ate var3, int var4, int var5, double var6) {
      this.r = Blocks.c.u();
      this.s = Blocks.d.u();
      if(var6 > 1.75D) {
         this.r = Blocks.b.u();
         this.s = Blocks.b.u();
      } else if(var6 > -0.5D) {
         this.r = Blocks.d.u().set(BlockDirt.a, BlockDirt.EnumDirtVariant.COARSE_DIRT);
      }

      this.b(var1, var2, var3, var4, var5, var6);
   }

   public void a(World var1, Random var2, BlockPosition var3) {
      this.t.a(var1, var2, this, var3);
   }
}
