package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumAnimation;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.MobEffect;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aff;
import net.minecraft.server.class_di;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;
import net.minecraft.server.class_rz;

public class class_aec extends Item {
   public class_aec() {
      this.d(1);
      this.a(CreativeModeTab.k);
   }

   public ItemStack a(ItemStack var1, World var2, class_rz var3) {
      EntityHuman var4 = var3 instanceof EntityHuman?(EntityHuman)var3:null;
      if(var4 == null || !var4.abilities.d) {
         --var1.b;
      }

      if(!var2.E) {
         List var5 = class_aff.a(var1);
         Iterator var6 = var5.iterator();

         while(var6.hasNext()) {
            MobEffect var7 = (MobEffect)var6.next();
            var3.c(new MobEffect(var7));
         }
      }

      if(var4 != null) {
         var4.b(StatisticList.b((Item)this));
      }

      if(var4 == null || !var4.abilities.d) {
         if(var1.b <= 0) {
            return new ItemStack(Items.bJ);
         }

         if(var4 != null) {
            var4.br.c(new ItemStack(Items.bJ));
         }
      }

      return var1;
   }

   public int e(ItemStack var1) {
      return 32;
   }

   public EnumAnimation f(ItemStack var1) {
      return EnumAnimation.DRINK;
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      var3.c(var4);
      return new class_qo(EnumResult.SUCCESS, var1);
   }

   public String a(ItemStack var1) {
      return class_di.a(class_aff.c(var1).b("potion.effect."));
   }
}
