package net.minecraft.server;

import net.minecraft.server.EntityCreature;
import net.minecraft.server.Vec3D;
import net.minecraft.server.Village;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_vl;
import net.minecraft.server.class_vn;

public class class_ts extends class_tj {
   private EntityCreature a;
   private class_vn b;
   private int c = -1;
   private int d = -1;

   public class_ts(EntityCreature var1) {
      this.a = var1;
      this.a(1);
   }

   public boolean a() {
      BlockPosition var1 = new BlockPosition(this.a);
      if((!this.a.world.B() || this.a.world.W() && !this.a.world.b(var1).d()) && !this.a.world.s.isNotOverworld()) {
         if(this.a.bE().nextInt(50) != 0) {
            return false;
         } else if(this.c != -1 && this.a.e((double)this.c, this.a.locY, (double)this.d) < 4.0D) {
            return false;
         } else {
            Village var2 = this.a.world.ai().a(var1, 14);
            if(var2 == null) {
               return false;
            } else {
               this.b = var2.c(var1);
               return this.b != null;
            }
         }
      } else {
         return false;
      }
   }

   public boolean b() {
      return !this.a.x().n();
   }

   public void c() {
      this.c = -1;
      BlockPosition var1 = this.b.e();
      int var2 = var1.p();
      int var3 = var1.q();
      int var4 = var1.r();
      if(this.a.c(var1) > 256.0D) {
         Vec3D var5 = class_vl.a(this.a, 14, 3, new Vec3D((double)var2 + 0.5D, (double)var3, (double)var4 + 0.5D));
         if(var5 != null) {
            this.a.x().a(var5.b, var5.c, var5.d, 1.0D);
         }
      } else {
         this.a.x().a((double)var2 + 0.5D, (double)var3, (double)var4 + 0.5D, 1.0D);
      }

   }

   public void d() {
      this.c = this.b.e().p();
      this.d = this.b.e().r();
      this.b = null;
   }
}
