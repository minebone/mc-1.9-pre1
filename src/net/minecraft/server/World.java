package net.minecraft.server;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.Callable;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Chunk;
import net.minecraft.server.CrashReport;
import net.minecraft.server.CrashReportSystemDetails;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.EnumSkyBlock;
import net.minecraft.server.GameRules;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.PersistentCollection;
import net.minecraft.server.PersistentVillage;
import net.minecraft.server.Scoreboard;
import net.minecraft.server.StructureBoundingBox;
import net.minecraft.server.TileEntity;
import net.minecraft.server.Vec3D;
import net.minecraft.server.WorldData;
import net.minecraft.server.WorldSettings;
import net.minecraft.server.WorldType;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aho;
import net.minecraft.server.class_aht;
import net.minecraft.server.class_ahu;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aij;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_amn;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_aru;
import net.minecraft.server.class_ary;
import net.minecraft.server.class_asu;
import net.minecraft.server.Material;
import net.minecraft.server.class_ayw;
import net.minecraft.server.class_azh;
import net.minecraft.server.class_baa;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_e;
import net.minecraft.server.Packet;
import net.minecraft.server.class_ky;
import net.minecraft.server.class_nf;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.class_oh;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_oo;
import net.minecraft.server.class_qk;
import net.minecraft.server.class_ru;
import net.minecraft.server.class_vf;

public abstract class World implements class_ahw {
   private int a = 63;
   protected boolean d;
   public final List e = Lists.newArrayList();
   protected final List f = Lists.newArrayList();
   public final List g = Lists.newArrayList();
   public final List h = Lists.newArrayList();
   private final List b = Lists.newArrayList();
   private final List c = Lists.newArrayList();
   public final List i = Lists.newArrayList();
   public final List j = Lists.newArrayList();
   protected final class_oh k = new class_oh();
   private long I = 16777215L;
   private int J;
   protected int l = (new Random()).nextInt();
   protected final int m = 1013904223;
   protected float n;
   protected float o;
   protected float p;
   protected float q;
   private int K;
   public final Random r = new Random();
   public final class_asu s;
   protected class_vf t = new class_vf();
   protected List u;
   protected class_ary v;
   protected final class_azh w;
   protected WorldData x;
   protected boolean y;
   protected PersistentCollection z;
   protected PersistentVillage A;
   protected class_baa B;
   public final class_oo C;
   private final Calendar L;
   protected Scoreboard D;
   public final boolean E;
   protected boolean F;
   protected boolean G;
   private boolean M;
   private final class_aru N;
   int[] H;

   protected World(class_azh var1, WorldData var2, class_asu var3, class_oo var4, boolean var5) {
      this.u = Lists.newArrayList((Object[])(new class_ahu[]{this.t}));
      this.L = Calendar.getInstance();
      this.D = new Scoreboard();
      this.F = true;
      this.G = true;
      this.H = new int['耀'];
      this.w = var1;
      this.C = var4;
      this.x = var2;
      this.s = var3;
      this.E = var5;
      this.N = var3.o();
   }

   public World b() {
      return this;
   }

   public BiomeBase b(final BlockPosition var1) {
      if(this.e(var1)) {
         Chunk var2 = this.f(var1);

         try {
            return var2.a(var1, this.s.k());
         } catch (Throwable var6) {
            CrashReport var4 = CrashReport.a(var6, "Getting biome");
            CrashReportSystemDetails var5 = var4.a("Coordinates of biome request");
            var5.a("Location", new Callable() {
               public String a() throws Exception {
                  return CrashReportSystemDetails.a(var1);
               }

               // $FF: synthetic method
               public Object call() throws Exception {
                  return this.a();
               }
            });
            throw new class_e(var4);
         }
      } else {
         return this.s.k().a(var1, class_aik.c);
      }
   }

   public class_aij A() {
      return this.s.k();
   }

   protected abstract class_ary n();

   public void a(WorldSettings var1) {
      this.x.d(true);
   }

   public MinecraftServer u() {
      return null;
   }

   public IBlockData c(BlockPosition var1) {
      BlockPosition var2;
      for(var2 = new BlockPosition(var1.p(), this.K(), var1.r()); !this.d(var2.a()); var2 = var2.a()) {
         ;
      }

      return this.getType(var2);
   }

   private boolean a(BlockPosition var1) {
      return var1.p() >= -30000000 && var1.r() >= -30000000 && var1.p() < 30000000 && var1.r() < 30000000 && var1.q() >= 0 && var1.q() < 256;
   }

   public boolean d(BlockPosition var1) {
      return this.getType(var1).getMaterial() == Material.a;
   }

   public boolean e(BlockPosition var1) {
      return this.a(var1, true);
   }

   public boolean a(BlockPosition var1, boolean var2) {
      return !this.a(var1)?false:this.a(var1.p() >> 4, var1.r() >> 4, var2);
   }

   public boolean a(BlockPosition var1, int var2) {
      return this.a(var1, var2, true);
   }

   public boolean a(BlockPosition var1, int var2, boolean var3) {
      return this.a(var1.p() - var2, var1.q() - var2, var1.r() - var2, var1.p() + var2, var1.q() + var2, var1.r() + var2, var3);
   }

   public boolean a(BlockPosition var1, BlockPosition var2) {
      return this.a(var1, var2, true);
   }

   public boolean a(BlockPosition var1, BlockPosition var2, boolean var3) {
      return this.a(var1.p(), var1.q(), var1.r(), var2.p(), var2.q(), var2.r(), var3);
   }

   public boolean a(StructureBoundingBox var1) {
      return this.b(var1, true);
   }

   public boolean b(StructureBoundingBox var1, boolean var2) {
      return this.a(var1.a, var1.b, var1.c, var1.d, var1.e, var1.f, var2);
   }

   private boolean a(int var1, int var2, int var3, int var4, int var5, int var6, boolean var7) {
      if(var5 >= 0 && var2 < 256) {
         var1 >>= 4;
         var3 >>= 4;
         var4 >>= 4;
         var6 >>= 4;

         for(int var8 = var1; var8 <= var4; ++var8) {
            for(int var9 = var3; var9 <= var6; ++var9) {
               if(!this.a(var8, var9, var7)) {
                  return false;
               }
            }
         }

         return true;
      } else {
         return false;
      }
   }

   protected abstract boolean a(int var1, int var2, boolean var3);

   public Chunk f(BlockPosition var1) {
      return this.a(var1.p() >> 4, var1.r() >> 4);
   }

   public Chunk a(int var1, int var2) {
      return this.v.d(var1, var2);
   }

   public boolean a(BlockPosition var1, IBlockData var2, int var3) {
      if(!this.a(var1)) {
         return false;
      } else if(!this.E && this.x.t() == WorldType.g) {
         return false;
      } else {
         Chunk var4 = this.f(var1);
         Block var5 = var2.getBlock();
         IBlockData var6 = var4.a(var1, var2);
         if(var6 == null) {
            return false;
         } else {
            if(var2.c() != var6.c() || var2.d() != var6.d()) {
               this.C.a("checkLight");
               this.w(var1);
               this.C.b();
            }

            if((var3 & 2) != 0 && (!this.E || (var3 & 4) == 0) && var4.i()) {
               this.a(var1, var6, var2, var3);
            }

            if(!this.E && (var3 & 1) != 0) {
               this.c(var1, var6.getBlock());
               if(var2.n()) {
                  this.f(var1, var5);
               }
            }

            return true;
         }
      }
   }

   public boolean g(BlockPosition var1) {
      return this.a((BlockPosition)var1, (IBlockData)Blocks.AIR.u(), 3);
   }

   public boolean b(BlockPosition var1, boolean var2) {
      IBlockData var3 = this.getType(var1);
      Block var4 = var3.getBlock();
      if(var3.getMaterial() == Material.a) {
         return false;
      } else {
         this.b(2001, var1, Block.j(var3));
         if(var2) {
            var4.b(this, var1, var3, 0);
         }

         return this.a((BlockPosition)var1, (IBlockData)Blocks.AIR.u(), 3);
      }
   }

   public boolean a(BlockPosition var1, IBlockData var2) {
      return this.a((BlockPosition)var1, (IBlockData)var2, 3);
   }

   public void a(BlockPosition var1, IBlockData var2, IBlockData var3, int var4) {
      for(int var5 = 0; var5 < this.u.size(); ++var5) {
         ((class_ahu)this.u.get(var5)).a(this, var1, var2, var3, var4);
      }

   }

   public void c(BlockPosition var1, Block var2) {
      if(this.x.t() != WorldType.g) {
         this.d(var1, var2);
      }

   }

   public void a(int var1, int var2, int var3, int var4) {
      int var5;
      if(var3 > var4) {
         var5 = var4;
         var4 = var3;
         var3 = var5;
      }

      if(!this.s.isNotOverworld()) {
         for(var5 = var3; var5 <= var4; ++var5) {
            this.c(EnumSkyBlock.SKY, new BlockPosition(var1, var5, var2));
         }
      }

      this.b(var1, var3, var2, var1, var4, var2);
   }

   public void b(BlockPosition var1, BlockPosition var2) {
      this.b(var1.p(), var1.q(), var1.r(), var2.p(), var2.q(), var2.r());
   }

   public void b(int var1, int var2, int var3, int var4, int var5, int var6) {
      for(int var7 = 0; var7 < this.u.size(); ++var7) {
         ((class_ahu)this.u.get(var7)).a(var1, var2, var3, var4, var5, var6);
      }

   }

   public void d(BlockPosition var1, Block var2) {
      this.e(var1.e(), var2);
      this.e(var1.f(), var2);
      this.e(var1.b(), var2);
      this.e(var1.a(), var2);
      this.e(var1.c(), var2);
      this.e(var1.d(), var2);
   }

   public void a(BlockPosition var1, Block var2, EnumDirection var3) {
      if(var3 != EnumDirection.WEST) {
         this.e(var1.e(), var2);
      }

      if(var3 != EnumDirection.EAST) {
         this.e(var1.f(), var2);
      }

      if(var3 != EnumDirection.DOWN) {
         this.e(var1.b(), var2);
      }

      if(var3 != EnumDirection.UP) {
         this.e(var1.a(), var2);
      }

      if(var3 != EnumDirection.NORTH) {
         this.e(var1.c(), var2);
      }

      if(var3 != EnumDirection.SOUTH) {
         this.e(var1.d(), var2);
      }

   }

   public void e(BlockPosition var1, final Block var2) {
      if(!this.E) {
         IBlockData var3 = this.getType(var1);

         try {
            var3.getBlock().a(this, var1, var3, var2);
         } catch (Throwable var7) {
            CrashReport var5 = CrashReport.a(var7, "Exception while updating neighbours");
            CrashReportSystemDetails var6 = var5.a("Block being updated");
            var6.a("Source block type", new Callable() {
               public String a() throws Exception {
                  try {
                     return String.format("ID #%d (%s // %s)", new Object[]{Integer.valueOf(Block.a(var2)), var2.a(), var2.getClass().getCanonicalName()});
                  } catch (Throwable var2x) {
                     return "ID #" + Block.a(var2);
                  }
               }

               // $FF: synthetic method
               public Object call() throws Exception {
                  return this.a();
               }
            });
            CrashReportSystemDetails.a(var6, var1, var3);
            throw new class_e(var5);
         }
      }
   }

   public boolean a(BlockPosition var1, Block var2) {
      return false;
   }

   public boolean h(BlockPosition var1) {
      return this.f(var1).c(var1);
   }

   public boolean i(BlockPosition var1) {
      if(var1.q() >= this.K()) {
         return this.h(var1);
      } else {
         BlockPosition var2 = new BlockPosition(var1.p(), this.K(), var1.r());
         if(!this.h(var2)) {
            return false;
         } else {
            for(var2 = var2.b(); var2.q() > var1.q(); var2 = var2.b()) {
               IBlockData var3 = this.getType(var2);
               if(var3.c() > 0 && !var3.getMaterial().d()) {
                  return false;
               }
            }

            return true;
         }
      }
   }

   public int j(BlockPosition var1) {
      if(var1.q() < 0) {
         return 0;
      } else {
         if(var1.q() >= 256) {
            var1 = new BlockPosition(var1.p(), 255, var1.r());
         }

         return this.f(var1).a((BlockPosition)var1, 0);
      }
   }

   public int k(BlockPosition var1) {
      return this.c(var1, true);
   }

   public int c(BlockPosition var1, boolean var2) {
      if(var1.p() >= -30000000 && var1.r() >= -30000000 && var1.p() < 30000000 && var1.r() < 30000000) {
         if(var2 && this.getType(var1).f()) {
            int var8 = this.c(var1.a(), false);
            int var4 = this.c(var1.f(), false);
            int var5 = this.c(var1.e(), false);
            int var6 = this.c(var1.d(), false);
            int var7 = this.c(var1.c(), false);
            if(var4 > var8) {
               var8 = var4;
            }

            if(var5 > var8) {
               var8 = var5;
            }

            if(var6 > var8) {
               var8 = var6;
            }

            if(var7 > var8) {
               var8 = var7;
            }

            return var8;
         } else if(var1.q() < 0) {
            return 0;
         } else {
            if(var1.q() >= 256) {
               var1 = new BlockPosition(var1.p(), 255, var1.r());
            }

            Chunk var3 = this.f(var1);
            return var3.a(var1, this.J);
         }
      } else {
         return 15;
      }
   }

   public BlockPosition l(BlockPosition var1) {
      int var2;
      if(var1.p() >= -30000000 && var1.r() >= -30000000 && var1.p() < 30000000 && var1.r() < 30000000) {
         if(this.a(var1.p() >> 4, var1.r() >> 4, true)) {
            var2 = this.a(var1.p() >> 4, var1.r() >> 4).b(var1.p() & 15, var1.r() & 15);
         } else {
            var2 = 0;
         }
      } else {
         var2 = this.K() + 1;
      }

      return new BlockPosition(var1.p(), var2, var1.r());
   }

   public int b(int var1, int var2) {
      if(var1 >= -30000000 && var2 >= -30000000 && var1 < 30000000 && var2 < 30000000) {
         if(!this.a(var1 >> 4, var2 >> 4, true)) {
            return 0;
         } else {
            Chunk var3 = this.a(var1 >> 4, var2 >> 4);
            return var3.w();
         }
      } else {
         return this.K() + 1;
      }
   }

   public int b(EnumSkyBlock var1, BlockPosition var2) {
      if(var2.q() < 0) {
         var2 = new BlockPosition(var2.p(), 0, var2.r());
      }

      if(!this.a(var2)) {
         return var1.c;
      } else if(!this.e(var2)) {
         return var1.c;
      } else {
         Chunk var3 = this.f(var2);
         return var3.a(var1, var2);
      }
   }

   public void a(EnumSkyBlock var1, BlockPosition var2, int var3) {
      if(this.a(var2)) {
         if(this.e(var2)) {
            Chunk var4 = this.f(var2);
            var4.a(var1, var2, var3);
            this.m(var2);
         }
      }
   }

   public void m(BlockPosition var1) {
      for(int var2 = 0; var2 < this.u.size(); ++var2) {
         ((class_ahu)this.u.get(var2)).a(var1);
      }

   }

   public float n(BlockPosition var1) {
      return this.s.n()[this.k(var1)];
   }

   public IBlockData getType(BlockPosition var1) {
      if(!this.a(var1)) {
         return Blocks.AIR.u();
      } else {
         Chunk var2 = this.f(var1);
         return var2.a(var1);
      }
   }

   public boolean B() {
      return this.J < 4;
   }

   public MovingObjectPosition a(Vec3D var1, Vec3D var2) {
      return this.a(var1, var2, false, false, false);
   }

   public MovingObjectPosition a(Vec3D var1, Vec3D var2, boolean var3) {
      return this.a(var1, var2, var3, false, false);
   }

   public MovingObjectPosition a(Vec3D var1, Vec3D var2, boolean var3, boolean var4, boolean var5) {
      if(!Double.isNaN(var1.b) && !Double.isNaN(var1.c) && !Double.isNaN(var1.d)) {
         if(!Double.isNaN(var2.b) && !Double.isNaN(var2.c) && !Double.isNaN(var2.d)) {
            int var6 = MathHelper.c(var2.b);
            int var7 = MathHelper.c(var2.c);
            int var8 = MathHelper.c(var2.d);
            int var9 = MathHelper.c(var1.b);
            int var10 = MathHelper.c(var1.c);
            int var11 = MathHelper.c(var1.d);
            BlockPosition var12 = new BlockPosition(var9, var10, var11);
            IBlockData var13 = this.getType(var12);
            Block var14 = var13.getBlock();
            if((!var4 || var13.d(this, var12) != Block.k) && var14.a(var13, var3)) {
               MovingObjectPosition var15 = var13.a(this, var12, var1, var2);
               if(var15 != null) {
                  return var15;
               }
            }

            MovingObjectPosition var40 = null;
            int var41 = 200;

            while(var41-- >= 0) {
               if(Double.isNaN(var1.b) || Double.isNaN(var1.c) || Double.isNaN(var1.d)) {
                  return null;
               }

               if(var9 == var6 && var10 == var7 && var11 == var8) {
                  return var5?var40:null;
               }

               boolean var42 = true;
               boolean var16 = true;
               boolean var17 = true;
               double var18 = 999.0D;
               double var20 = 999.0D;
               double var22 = 999.0D;
               if(var6 > var9) {
                  var18 = (double)var9 + 1.0D;
               } else if(var6 < var9) {
                  var18 = (double)var9 + 0.0D;
               } else {
                  var42 = false;
               }

               if(var7 > var10) {
                  var20 = (double)var10 + 1.0D;
               } else if(var7 < var10) {
                  var20 = (double)var10 + 0.0D;
               } else {
                  var16 = false;
               }

               if(var8 > var11) {
                  var22 = (double)var11 + 1.0D;
               } else if(var8 < var11) {
                  var22 = (double)var11 + 0.0D;
               } else {
                  var17 = false;
               }

               double var24 = 999.0D;
               double var26 = 999.0D;
               double var28 = 999.0D;
               double var30 = var2.b - var1.b;
               double var32 = var2.c - var1.c;
               double var34 = var2.d - var1.d;
               if(var42) {
                  var24 = (var18 - var1.b) / var30;
               }

               if(var16) {
                  var26 = (var20 - var1.c) / var32;
               }

               if(var17) {
                  var28 = (var22 - var1.d) / var34;
               }

               if(var24 == -0.0D) {
                  var24 = -1.0E-4D;
               }

               if(var26 == -0.0D) {
                  var26 = -1.0E-4D;
               }

               if(var28 == -0.0D) {
                  var28 = -1.0E-4D;
               }

               EnumDirection var36;
               if(var24 < var26 && var24 < var28) {
                  var36 = var6 > var9?EnumDirection.WEST:EnumDirection.EAST;
                  var1 = new Vec3D(var18, var1.c + var32 * var24, var1.d + var34 * var24);
               } else if(var26 < var28) {
                  var36 = var7 > var10?EnumDirection.DOWN:EnumDirection.UP;
                  var1 = new Vec3D(var1.b + var30 * var26, var20, var1.d + var34 * var26);
               } else {
                  var36 = var8 > var11?EnumDirection.NORTH:EnumDirection.SOUTH;
                  var1 = new Vec3D(var1.b + var30 * var28, var1.c + var32 * var28, var22);
               }

               var9 = MathHelper.c(var1.b) - (var36 == EnumDirection.EAST?1:0);
               var10 = MathHelper.c(var1.c) - (var36 == EnumDirection.UP?1:0);
               var11 = MathHelper.c(var1.d) - (var36 == EnumDirection.SOUTH?1:0);
               var12 = new BlockPosition(var9, var10, var11);
               IBlockData var37 = this.getType(var12);
               Block var38 = var37.getBlock();
               if(!var4 || var37.getMaterial() == Material.E || var37.d(this, var12) != Block.k) {
                  if(var38.a(var37, var3)) {
                     MovingObjectPosition var39 = var37.a(this, var12, var1, var2);
                     if(var39 != null) {
                        return var39;
                     }
                  } else {
                     var40 = new MovingObjectPosition(MovingObjectPosition.EnumMovingObjectType.MISS, var1, var36, var12);
                  }
               }
            }

            return var5?var40:null;
         } else {
            return null;
         }
      } else {
         return null;
      }
   }

   public void a(EntityHuman var1, BlockPosition var2, class_nf var3, EnumSoundCategory var4, float var5, float var6) {
      this.a(var1, (double)var2.p() + 0.5D, (double)var2.q() + 0.5D, (double)var2.r() + 0.5D, var3, var4, var5, var6);
   }

   public void a(EntityHuman var1, double var2, double var4, double var6, class_nf var8, EnumSoundCategory var9, float var10, float var11) {
      for(int var12 = 0; var12 < this.u.size(); ++var12) {
         ((class_ahu)this.u.get(var12)).a(var1, var8, var9, var2, var4, var6, var10, var11);
      }

   }

   public void a(double var1, double var3, double var5, class_nf var7, EnumSoundCategory var8, float var9, float var10, boolean var11) {
   }

   public void a(BlockPosition var1, class_nf var2) {
      for(int var3 = 0; var3 < this.u.size(); ++var3) {
         ((class_ahu)this.u.get(var3)).a(var2, var1);
      }

   }

   public void a(EnumParticle var1, double var2, double var4, double var6, double var8, double var10, double var12, int... var14) {
      this.a(var1.c(), var1.e(), var2, var4, var6, var8, var10, var12, var14);
   }

   private void a(int var1, boolean var2, double var3, double var5, double var7, double var9, double var11, double var13, int... var15) {
      for(int var16 = 0; var16 < this.u.size(); ++var16) {
         ((class_ahu)this.u.get(var16)).a(var1, var2, var3, var5, var7, var9, var11, var13, var15);
      }

   }

   public boolean d(Entity var1) {
      this.j.add(var1);
      return true;
   }

   public boolean a(Entity var1) {
      int var2 = MathHelper.c(var1.locX / 16.0D);
      int var3 = MathHelper.c(var1.locZ / 16.0D);
      boolean var4 = var1.attachedToPlayer;
      if(var1 instanceof EntityHuman) {
         var4 = true;
      }

      if(!var4 && !this.a(var2, var3, false)) {
         return false;
      } else {
         if(var1 instanceof EntityHuman) {
            EntityHuman var5 = (EntityHuman)var1;
            this.i.add(var5);
            this.e();
         }

         this.a(var2, var3).a(var1);
         this.e.add(var1);
         this.b(var1);
         return true;
      }
   }

   protected void b(Entity var1) {
      for(int var2 = 0; var2 < this.u.size(); ++var2) {
         ((class_ahu)this.u.get(var2)).a(var1);
      }

   }

   protected void c(Entity var1) {
      for(int var2 = 0; var2 < this.u.size(); ++var2) {
         ((class_ahu)this.u.get(var2)).b(var1);
      }

   }

   public void e(Entity var1) {
      if(var1.aI()) {
         var1.ay();
      }

      if(var1.aH()) {
         var1.p();
      }

      var1.S();
      if(var1 instanceof EntityHuman) {
         this.i.remove(var1);
         this.e();
         this.c(var1);
      }

   }

   public void f(Entity var1) {
      var1.b(false);
      var1.S();
      if(var1 instanceof EntityHuman) {
         this.i.remove(var1);
         this.e();
      }

      int var2 = var1.ab;
      int var3 = var1.ad;
      if(var1.aa && this.a(var2, var3, true)) {
         this.a(var2, var3).b(var1);
      }

      this.e.remove(var1);
      this.c(var1);
   }

   public class_vf C() {
      return this.t;
   }

   public void a(class_ahu var1) {
      this.u.add(var1);
   }

   public List a(Entity var1, AxisAlignedBB var2) {
      ArrayList var3 = Lists.newArrayList();
      int var4 = MathHelper.c(var2.a) - 1;
      int var5 = MathHelper.f(var2.d) + 1;
      int var6 = MathHelper.c(var2.b) - 1;
      int var7 = MathHelper.f(var2.e) + 1;
      int var8 = MathHelper.c(var2.c) - 1;
      int var9 = MathHelper.f(var2.f) + 1;
      class_aru var10 = this.aj();
      boolean var11 = var1 != null && var1.bn();
      boolean var12 = var1 != null && this.a(var10, var1);
      IBlockData var13 = Blocks.b.u();
      BlockPosition.class_b_in_class_cj var14 = BlockPosition.class_b_in_class_cj.s();

      int var16;
      for(int var15 = var4; var15 < var5; ++var15) {
         for(var16 = var8; var16 < var9; ++var16) {
            int var17 = (var15 != var4 && var15 != var5 - 1?0:1) + (var16 != var8 && var16 != var9 - 1?0:1);
            if(var17 != 2 && this.e((BlockPosition)var14.d(var15, 64, var16))) {
               for(int var18 = var6; var18 < var7; ++var18) {
                  if(var17 <= 0 || var18 != var6 && var18 != var7 - 1) {
                     var14.d(var15, var18, var16);
                     if(var1 != null) {
                        if(var11 && var12) {
                           var1.j(false);
                        } else if(!var11 && !var12) {
                           var1.j(true);
                        }
                     }

                     IBlockData var19 = var13;
                     if(var10.a((BlockPosition)var14) || !var12) {
                        var19 = this.getType(var14);
                     }

                     var19.a(this, var14, var2, var3, var1);
                  }
               }
            }
         }
      }

      var14.t();
      if(var1 != null) {
         List var20 = this.b(var1, var2.g(0.25D));

         for(var16 = 0; var16 < var20.size(); ++var16) {
            Entity var21 = (Entity)var20.get(var16);
            if(!var1.x(var21)) {
               AxisAlignedBB var22 = var21.ae();
               if(var22 != null && var22.b(var2)) {
                  var3.add(var22);
               }

               var22 = var1.j(var21);
               if(var22 != null && var22.b(var2)) {
                  var3.add(var22);
               }
            }
         }
      }

      return var3;
   }

   public boolean a(class_aru var1, Entity var2) {
      double var3 = var1.b();
      double var5 = var1.c();
      double var7 = var1.d();
      double var9 = var1.e();
      if(var2.bn()) {
         ++var3;
         ++var5;
         --var7;
         --var9;
      } else {
         --var3;
         --var5;
         ++var7;
         ++var9;
      }

      return var2.locX > var3 && var2.locX < var7 && var2.locZ > var5 && var2.locZ < var9;
   }

   public List a(AxisAlignedBB var1) {
      ArrayList var2 = Lists.newArrayList();
      int var3 = MathHelper.c(var1.a) - 1;
      int var4 = MathHelper.f(var1.d) + 1;
      int var5 = MathHelper.c(var1.b) - 1;
      int var6 = MathHelper.f(var1.e) + 1;
      int var7 = MathHelper.c(var1.c) - 1;
      int var8 = MathHelper.f(var1.f) + 1;
      BlockPosition.class_b_in_class_cj var9 = BlockPosition.class_b_in_class_cj.s();

      for(int var10 = var3; var10 < var4; ++var10) {
         for(int var11 = var7; var11 < var8; ++var11) {
            int var12 = (var10 != var3 && var10 != var4 - 1?0:1) + (var11 != var7 && var11 != var8 - 1?0:1);
            if(var12 != 2 && this.e((BlockPosition)var9.d(var10, 64, var11))) {
               for(int var13 = var5; var13 < var6; ++var13) {
                  if(var12 <= 0 || var13 != var5 && var13 != var6 - 1) {
                     var9.d(var10, var13, var11);
                     IBlockData var14;
                     if(var10 >= -30000000 && var10 < 30000000 && var11 >= -30000000 && var11 < 30000000) {
                        var14 = this.getType(var9);
                     } else {
                        var14 = Blocks.h.u();
                     }

                     var14.a(this, var9, var1, var2, (Entity)null);
                  }
               }
            }
         }
      }

      var9.t();
      return var2;
   }

   public boolean b(AxisAlignedBB var1) {
      ArrayList var2 = Lists.newArrayList();
      int var3 = MathHelper.c(var1.a) - 1;
      int var4 = MathHelper.f(var1.d) + 1;
      int var5 = MathHelper.c(var1.b) - 1;
      int var6 = MathHelper.f(var1.e) + 1;
      int var7 = MathHelper.c(var1.c) - 1;
      int var8 = MathHelper.f(var1.f) + 1;
      BlockPosition.class_b_in_class_cj var9 = BlockPosition.class_b_in_class_cj.s();

      try {
         for(int var10 = var3; var10 < var4; ++var10) {
            for(int var11 = var7; var11 < var8; ++var11) {
               int var12 = (var10 != var3 && var10 != var4 - 1?0:1) + (var11 != var7 && var11 != var8 - 1?0:1);
               if(var12 != 2 && this.e((BlockPosition)var9.d(var10, 64, var11))) {
                  for(int var13 = var5; var13 < var6; ++var13) {
                     if(var12 <= 0 || var13 != var5 && var13 != var6 - 1) {
                        var9.d(var10, var13, var11);
                        if(var10 < -30000000 || var10 >= 30000000 || var11 < -30000000 || var11 >= 30000000) {
                           boolean var19 = true;
                           return var19;
                        }

                        IBlockData var14 = this.getType(var9);
                        var14.a(this, var9, var1, var2, (Entity)null);
                        if(!var2.isEmpty()) {
                           boolean var15 = true;
                           return var15;
                        }
                     }
                  }
               }
            }
         }

         return false;
      } finally {
         var9.t();
      }
   }

   public int a(float var1) {
      float var2 = this.c(var1);
      float var3 = 1.0F - (MathHelper.b(var2 * 6.2831855F) * 2.0F + 0.5F);
      var3 = MathHelper.a(var3, 0.0F, 1.0F);
      var3 = 1.0F - var3;
      var3 = (float)((double)var3 * (1.0D - (double)(this.j(var1) * 5.0F) / 16.0D));
      var3 = (float)((double)var3 * (1.0D - (double)(this.h(var1) * 5.0F) / 16.0D));
      var3 = 1.0F - var3;
      return (int)(var3 * 11.0F);
   }

   public float c(float var1) {
      return this.s.a(this.x.f(), var1);
   }

   public float E() {
      return class_asu.a[this.s.a(this.x.f())];
   }

   public float d(float var1) {
      float var2 = this.c(var1);
      return var2 * 6.2831855F;
   }

   public BlockPosition p(BlockPosition var1) {
      return this.f(var1).f(var1);
   }

   public BlockPosition q(BlockPosition var1) {
      Chunk var2 = this.f(var1);

      BlockPosition var3;
      BlockPosition var4;
      for(var3 = new BlockPosition(var1.p(), var2.g() + 16, var1.r()); var3.q() >= 0; var3 = var4) {
         var4 = var3.b();
         Material var5 = var2.a(var4).getMaterial();
         if(var5.c() && var5 != Material.j) {
            break;
         }
      }

      return var3;
   }

   public boolean b(BlockPosition var1, Block var2) {
      return true;
   }

   public void a(BlockPosition var1, Block var2, int var3) {
   }

   public void a(BlockPosition var1, Block var2, int var3, int var4) {
   }

   public void b(BlockPosition var1, Block var2, int var3, int var4) {
   }

   public void k() {
      this.C.a("entities");
      this.C.a("global");

      int var1;
      Entity var2;
      for(var1 = 0; var1 < this.j.size(); ++var1) {
         var2 = (Entity)this.j.get(var1);

         try {
            ++var2.ticksLived;
            var2.m();
         } catch (Throwable var9) {
            CrashReport var4 = CrashReport.a(var9, "Ticking entity");
            CrashReportSystemDetails var5 = var4.a("Entity being ticked");
            if(var2 == null) {
               var5.a((String)"Entity", (Object)"~~NULL~~");
            } else {
               var2.a(var5);
            }

            throw new class_e(var4);
         }

         if(var2.dead) {
            this.j.remove(var1--);
         }
      }

      this.C.c("remove");
      this.e.removeAll(this.f);

      int var15;
      for(var1 = 0; var1 < this.f.size(); ++var1) {
         var2 = (Entity)this.f.get(var1);
         int var3 = var2.ab;
         var15 = var2.ad;
         if(var2.aa && this.a(var3, var15, true)) {
            this.a(var3, var15).b(var2);
         }
      }

      for(var1 = 0; var1 < this.f.size(); ++var1) {
         this.c((Entity)this.f.get(var1));
      }

      this.f.clear();
      this.l();
      this.C.c("regular");

      CrashReportSystemDetails var6;
      CrashReport var18;
      for(var1 = 0; var1 < this.e.size(); ++var1) {
         var2 = (Entity)this.e.get(var1);
         Entity var11 = var2.bx();
         if(var11 != null) {
            if(!var11.dead && var11.w(var2)) {
               continue;
            }

            var2.p();
         }

         this.C.a("tick");
         if(!var2.dead) {
            try {
               this.g(var2);
            } catch (Throwable var8) {
               var18 = CrashReport.a(var8, "Ticking entity");
               var6 = var18.a("Entity being ticked");
               var2.a(var6);
               throw new class_e(var18);
            }
         }

         this.C.b();
         this.C.a("remove");
         if(var2.dead) {
            var15 = var2.ab;
            int var19 = var2.ad;
            if(var2.aa && this.a(var15, var19, true)) {
               this.a(var15, var19).b(var2);
            }

            this.e.remove(var1--);
            this.c(var2);
         }

         this.C.b();
      }

      this.C.c("blockEntities");
      this.M = true;
      Iterator var16 = this.h.iterator();

      while(var16.hasNext()) {
         TileEntity var10 = (TileEntity)var16.next();
         if(!var10.x() && var10.t()) {
            BlockPosition var13 = var10.v();
            if(this.e(var13) && this.N.a(var13)) {
               try {
                  this.C.a(var10.getClass().getSimpleName());
                  ((class_ky)var10).c();
                  this.C.b();
               } catch (Throwable var7) {
                  var18 = CrashReport.a(var7, "Ticking block entity");
                  var6 = var18.a("Block entity being ticked");
                  var10.a(var6);
                  throw new class_e(var18);
               }
            }
         }

         if(var10.x()) {
            var16.remove();
            this.g.remove(var10);
            if(this.e(var10.v())) {
               this.f(var10.v()).d(var10.v());
            }
         }
      }

      this.M = false;
      if(!this.c.isEmpty()) {
         this.h.removeAll(this.c);
         this.g.removeAll(this.c);
         this.c.clear();
      }

      this.C.c("pendingBlockEntities");
      if(!this.b.isEmpty()) {
         for(int var12 = 0; var12 < this.b.size(); ++var12) {
            TileEntity var14 = (TileEntity)this.b.get(var12);
            if(!var14.x()) {
               if(!this.g.contains(var14)) {
                  this.a(var14);
               }

               if(this.e(var14.v())) {
                  Chunk var17 = this.f(var14.v());
                  IBlockData var20 = var17.a(var14.v());
                  var17.a(var14.v(), var14);
                  this.a(var14.v(), var20, var20, 3);
               }
            }
         }

         this.b.clear();
      }

      this.C.b();
      this.C.b();
   }

   protected void l() {
   }

   public boolean a(TileEntity var1) {
      boolean var2 = this.g.add(var1);
      if(var2 && var1 instanceof class_ky) {
         this.h.add(var1);
      }

      return var2;
   }

   public void b(Collection var1) {
      if(this.M) {
         this.b.addAll(var1);
      } else {
         Iterator var2 = var1.iterator();

         while(var2.hasNext()) {
            TileEntity var3 = (TileEntity)var2.next();
            this.a(var3);
         }
      }

   }

   public void g(Entity var1) {
      this.a(var1, true);
   }

   public void a(Entity var1, boolean var2) {
      int var3 = MathHelper.c(var1.locX);
      int var4 = MathHelper.c(var1.locZ);
      byte var5 = 32;
      if(!var2 || this.a(var3 - var5, 0, var4 - var5, var3 + var5, 0, var4 + var5, true)) {
         var1.M = var1.locX;
         var1.N = var1.locY;
         var1.O = var1.locZ;
         var1.lastYaw = var1.yaw;
         var1.lastPitch = var1.pitch;
         if(var2 && var1.aa) {
            ++var1.ticksLived;
            if(var1.aH()) {
               var1.av();
            } else {
               var1.m();
            }
         }

         this.C.a("chunkCheck");
         if(Double.isNaN(var1.locX) || Double.isInfinite(var1.locX)) {
            var1.locX = var1.M;
         }

         if(Double.isNaN(var1.locY) || Double.isInfinite(var1.locY)) {
            var1.locY = var1.N;
         }

         if(Double.isNaN(var1.locZ) || Double.isInfinite(var1.locZ)) {
            var1.locZ = var1.O;
         }

         if(Double.isNaN((double)var1.pitch) || Double.isInfinite((double)var1.pitch)) {
            var1.pitch = var1.lastPitch;
         }

         if(Double.isNaN((double)var1.yaw) || Double.isInfinite((double)var1.yaw)) {
            var1.yaw = var1.lastYaw;
         }

         int var6 = MathHelper.c(var1.locX / 16.0D);
         int var7 = MathHelper.c(var1.locY / 16.0D);
         int var8 = MathHelper.c(var1.locZ / 16.0D);
         if(!var1.aa || var1.ab != var6 || var1.ac != var7 || var1.ad != var8) {
            if(var1.aa && this.a(var1.ab, var1.ad, true)) {
               this.a(var1.ab, var1.ad).a(var1, var1.ac);
            }

            if(!var1.br() && !this.a(var6, var8, true)) {
               var1.aa = false;
            } else {
               this.a(var6, var8).a(var1);
            }
         }

         this.C.b();
         if(var2 && var1.aa) {
            Iterator var9 = var1.bt().iterator();

            while(true) {
               while(var9.hasNext()) {
                  Entity var10 = (Entity)var9.next();
                  if(!var10.dead && var10.bx() == var1) {
                     this.g(var10);
                  } else {
                     var10.p();
                  }
               }

               return;
            }
         }
      }
   }

   public boolean c(AxisAlignedBB var1) {
      return this.a((AxisAlignedBB)var1, (Entity)null);
   }

   public boolean a(AxisAlignedBB var1, Entity var2) {
      List var3 = this.b((Entity)null, (AxisAlignedBB)var1);

      for(int var4 = 0; var4 < var3.size(); ++var4) {
         Entity var5 = (Entity)var3.get(var4);
         if(!var5.dead && var5.i && var5 != var2 && (var2 == null || var5.x(var2))) {
            return false;
         }
      }

      return true;
   }

   public boolean d(AxisAlignedBB var1) {
      int var2 = MathHelper.c(var1.a);
      int var3 = MathHelper.f(var1.d);
      int var4 = MathHelper.c(var1.b);
      int var5 = MathHelper.f(var1.e);
      int var6 = MathHelper.c(var1.c);
      int var7 = MathHelper.f(var1.f);
      BlockPosition.class_b_in_class_cj var8 = BlockPosition.class_b_in_class_cj.s();

      for(int var9 = var2; var9 < var3; ++var9) {
         for(int var10 = var4; var10 < var5; ++var10) {
            for(int var11 = var6; var11 < var7; ++var11) {
               IBlockData var12 = this.getType(var8.d(var9, var10, var11));
               if(var12.getMaterial() != Material.a) {
                  var8.t();
                  return true;
               }
            }
         }
      }

      var8.t();
      return false;
   }

   public boolean e(AxisAlignedBB var1) {
      int var2 = MathHelper.c(var1.a);
      int var3 = MathHelper.f(var1.d);
      int var4 = MathHelper.c(var1.b);
      int var5 = MathHelper.f(var1.e);
      int var6 = MathHelper.c(var1.c);
      int var7 = MathHelper.f(var1.f);
      BlockPosition.class_b_in_class_cj var8 = BlockPosition.class_b_in_class_cj.s();

      for(int var9 = var2; var9 < var3; ++var9) {
         for(int var10 = var4; var10 < var5; ++var10) {
            for(int var11 = var6; var11 < var7; ++var11) {
               IBlockData var12 = this.getType(var8.d(var9, var10, var11));
               if(var12.getMaterial().d()) {
                  var8.t();
                  return true;
               }
            }
         }
      }

      var8.t();
      return false;
   }

   public boolean f(AxisAlignedBB var1) {
      int var2 = MathHelper.c(var1.a);
      int var3 = MathHelper.f(var1.d);
      int var4 = MathHelper.c(var1.b);
      int var5 = MathHelper.f(var1.e);
      int var6 = MathHelper.c(var1.c);
      int var7 = MathHelper.f(var1.f);
      if(this.a(var2, var4, var6, var3, var5, var7, true)) {
         BlockPosition.class_b_in_class_cj var8 = BlockPosition.class_b_in_class_cj.s();
         int var9 = var2;

         while(true) {
            if(var9 >= var3) {
               var8.t();
               break;
            }

            for(int var10 = var4; var10 < var5; ++var10) {
               for(int var11 = var6; var11 < var7; ++var11) {
                  Block var12 = this.getType(var8.d(var9, var10, var11)).getBlock();
                  if(var12 == Blocks.ab || var12 == Blocks.k || var12 == Blocks.l) {
                     var8.t();
                     return true;
                  }
               }
            }

            ++var9;
         }
      }

      return false;
   }

   public boolean a(AxisAlignedBB var1, Material var2, Entity var3) {
      int var4 = MathHelper.c(var1.a);
      int var5 = MathHelper.f(var1.d);
      int var6 = MathHelper.c(var1.b);
      int var7 = MathHelper.f(var1.e);
      int var8 = MathHelper.c(var1.c);
      int var9 = MathHelper.f(var1.f);
      if(!this.a(var4, var6, var8, var5, var7, var9, true)) {
         return false;
      } else {
         boolean var10 = false;
         Vec3D var11 = Vec3D.a;
         BlockPosition.class_b_in_class_cj var12 = BlockPosition.class_b_in_class_cj.s();

         for(int var13 = var4; var13 < var5; ++var13) {
            for(int var14 = var6; var14 < var7; ++var14) {
               for(int var15 = var8; var15 < var9; ++var15) {
                  var12.d(var13, var14, var15);
                  IBlockData var16 = this.getType(var12);
                  Block var17 = var16.getBlock();
                  if(var16.getMaterial() == var2) {
                     double var18 = (double)((float)(var14 + 1) - class_amn.e(((Integer)var16.get(class_amn.b)).intValue()));
                     if((double)var7 >= var18) {
                        var10 = true;
                        var11 = var17.a((World)this, (BlockPosition)var12, (Entity)var3, (Vec3D)var11);
                     }
                  }
               }
            }
         }

         var12.t();
         if(var11.b() > 0.0D && var3.bc()) {
            var11 = var11.a();
            double var20 = 0.014D;
            var3.motX += var11.b * var20;
            var3.motY += var11.c * var20;
            var3.motZ += var11.d * var20;
         }

         return var10;
      }
   }

   public boolean a(AxisAlignedBB var1, Material var2) {
      int var3 = MathHelper.c(var1.a);
      int var4 = MathHelper.f(var1.d);
      int var5 = MathHelper.c(var1.b);
      int var6 = MathHelper.f(var1.e);
      int var7 = MathHelper.c(var1.c);
      int var8 = MathHelper.f(var1.f);
      BlockPosition.class_b_in_class_cj var9 = BlockPosition.class_b_in_class_cj.s();

      for(int var10 = var3; var10 < var4; ++var10) {
         for(int var11 = var5; var11 < var6; ++var11) {
            for(int var12 = var7; var12 < var8; ++var12) {
               if(this.getType(var9.d(var10, var11, var12)).getMaterial() == var2) {
                  var9.t();
                  return true;
               }
            }
         }
      }

      var9.t();
      return false;
   }

   public boolean b(AxisAlignedBB var1, Material var2) {
      int var3 = MathHelper.c(var1.a);
      int var4 = MathHelper.f(var1.d);
      int var5 = MathHelper.c(var1.b);
      int var6 = MathHelper.f(var1.e);
      int var7 = MathHelper.c(var1.c);
      int var8 = MathHelper.f(var1.f);
      BlockPosition.class_b_in_class_cj var9 = BlockPosition.class_b_in_class_cj.s();

      for(int var10 = var3; var10 < var4; ++var10) {
         for(int var11 = var5; var11 < var6; ++var11) {
            for(int var12 = var7; var12 < var8; ++var12) {
               IBlockData var13 = this.getType(var9.d(var10, var11, var12));
               if(var13.getMaterial() == var2) {
                  int var14 = ((Integer)var13.get(class_amn.b)).intValue();
                  double var15 = (double)(var11 + 1);
                  if(var14 < 8) {
                     var15 = (double)(var11 + 1) - (double)var14 / 8.0D;
                  }

                  if(var15 >= var1.b) {
                     var9.t();
                     return true;
                  }
               }
            }
         }
      }

      var9.t();
      return false;
   }

   public class_aho a(Entity var1, double var2, double var4, double var6, float var8, boolean var9) {
      return this.a(var1, var2, var4, var6, var8, false, var9);
   }

   public class_aho a(Entity var1, double var2, double var4, double var6, float var8, boolean var9, boolean var10) {
      class_aho var11 = new class_aho(this, var1, var2, var4, var6, var8, var9, var10);
      var11.a();
      var11.a(true);
      return var11;
   }

   public float a(Vec3D var1, AxisAlignedBB var2) {
      double var3 = 1.0D / ((var2.d - var2.a) * 2.0D + 1.0D);
      double var5 = 1.0D / ((var2.e - var2.b) * 2.0D + 1.0D);
      double var7 = 1.0D / ((var2.f - var2.c) * 2.0D + 1.0D);
      double var9 = (1.0D - Math.floor(1.0D / var3) * var3) / 2.0D;
      double var11 = (1.0D - Math.floor(1.0D / var7) * var7) / 2.0D;
      if(var3 >= 0.0D && var5 >= 0.0D && var7 >= 0.0D) {
         int var13 = 0;
         int var14 = 0;

         for(float var15 = 0.0F; var15 <= 1.0F; var15 = (float)((double)var15 + var3)) {
            for(float var16 = 0.0F; var16 <= 1.0F; var16 = (float)((double)var16 + var5)) {
               for(float var17 = 0.0F; var17 <= 1.0F; var17 = (float)((double)var17 + var7)) {
                  double var18 = var2.a + (var2.d - var2.a) * (double)var15;
                  double var20 = var2.b + (var2.e - var2.b) * (double)var16;
                  double var22 = var2.c + (var2.f - var2.c) * (double)var17;
                  if(this.a(new Vec3D(var18 + var9, var20, var22 + var11), var1) == null) {
                     ++var13;
                  }

                  ++var14;
               }
            }
         }

         return (float)var13 / (float)var14;
      } else {
         return 0.0F;
      }
   }

   public boolean douseFire(EntityHuman var1, BlockPosition var2, EnumDirection var3) {
      var2 = var2.a(var3);
      if(this.getType(var2).getBlock() == Blocks.ab) {
         this.a(var1, 1009, var2, 0);
         this.g(var2);
         return true;
      } else {
         return false;
      }
   }

   public TileEntity r(BlockPosition var1) {
      if(!this.a(var1)) {
         return null;
      } else {
         TileEntity var2 = null;
         int var3;
         TileEntity var4;
         if(this.M) {
            for(var3 = 0; var3 < this.b.size(); ++var3) {
               var4 = (TileEntity)this.b.get(var3);
               if(!var4.x() && var4.v().equals(var1)) {
                  var2 = var4;
                  break;
               }
            }
         }

         if(var2 == null) {
            var2 = this.f(var1).a(var1, Chunk.EnumTileEntityState.IMMEDIATE);
         }

         if(var2 == null) {
            for(var3 = 0; var3 < this.b.size(); ++var3) {
               var4 = (TileEntity)this.b.get(var3);
               if(!var4.x() && var4.v().equals(var1)) {
                  var2 = var4;
                  break;
               }
            }
         }

         return var2;
      }
   }

   public void a(BlockPosition var1, TileEntity var2) {
      if(var2 != null && !var2.x()) {
         if(this.M) {
            var2.a(var1);
            Iterator var3 = this.b.iterator();

            while(var3.hasNext()) {
               TileEntity var4 = (TileEntity)var3.next();
               if(var4.v().equals(var1)) {
                  var4.y();
                  var3.remove();
               }
            }

            this.b.add(var2);
         } else {
            this.a(var2);
            this.f(var1).a(var1, var2);
         }
      }

   }

   public void s(BlockPosition var1) {
      TileEntity var2 = this.r(var1);
      if(var2 != null && this.M) {
         var2.y();
         this.b.remove(var2);
      } else {
         if(var2 != null) {
            this.b.remove(var2);
            this.g.remove(var2);
            this.h.remove(var2);
         }

         this.f(var1).d(var1);
      }

   }

   public void b(TileEntity var1) {
      this.c.add(var1);
   }

   public boolean t(BlockPosition var1) {
      AxisAlignedBB var2 = this.getType(var1).d(this, var1);
      return var2 != Block.k && var2.a() >= 1.0D;
   }

   public boolean d(BlockPosition var1, boolean var2) {
      if(!this.a(var1)) {
         return var2;
      } else {
         Chunk var3 = this.v.b(var1.p() >> 4, var1.r() >> 4);
         if(var3 != null && !var3.f()) {
            IBlockData var4 = this.getType(var1);
            return var4.getMaterial().k() && var4.h();
         } else {
            return var2;
         }
      }
   }

   public void H() {
      int var1 = this.a(1.0F);
      if(var1 != this.J) {
         this.J = var1;
      }

   }

   public void a(boolean var1, boolean var2) {
      this.F = var1;
      this.G = var2;
   }

   public void d() {
      this.t();
   }

   protected void I() {
      if(this.x.o()) {
         this.o = 1.0F;
         if(this.x.m()) {
            this.q = 1.0F;
         }
      }

   }

   protected void t() {
      if(!this.s.isNotOverworld()) {
         if(!this.E) {
            int var1 = this.x.z();
            if(var1 > 0) {
               --var1;
               this.x.i(var1);
               this.x.f(this.x.m()?1:2);
               this.x.g(this.x.o()?1:2);
            }

            int var2 = this.x.n();
            if(var2 <= 0) {
               if(this.x.m()) {
                  this.x.f(this.r.nextInt(12000) + 3600);
               } else {
                  this.x.f(this.r.nextInt(168000) + 12000);
               }
            } else {
               --var2;
               this.x.f(var2);
               if(var2 <= 0) {
                  this.x.a(!this.x.m());
               }
            }

            this.p = this.q;
            if(this.x.m()) {
               this.q = (float)((double)this.q + 0.01D);
            } else {
               this.q = (float)((double)this.q - 0.01D);
            }

            this.q = MathHelper.a(this.q, 0.0F, 1.0F);
            int var3 = this.x.p();
            if(var3 <= 0) {
               if(this.x.o()) {
                  this.x.g(this.r.nextInt(12000) + 12000);
               } else {
                  this.x.g(this.r.nextInt(168000) + 12000);
               }
            } else {
               --var3;
               this.x.g(var3);
               if(var3 <= 0) {
                  this.x.b(!this.x.o());
               }
            }

            this.n = this.o;
            if(this.x.o()) {
               this.o = (float)((double)this.o + 0.01D);
            } else {
               this.o = (float)((double)this.o - 0.01D);
            }

            this.o = MathHelper.a(this.o, 0.0F, 1.0F);
         }
      }
   }

   protected void j() {
   }

   public void a(Block var1, BlockPosition var2, Random var3) {
      this.d = true;
      var1.b(this, var2, this.getType(var2), var3);
      this.d = false;
   }

   public boolean u(BlockPosition var1) {
      return this.e(var1, false);
   }

   public boolean v(BlockPosition var1) {
      return this.e(var1, true);
   }

   public boolean e(BlockPosition var1, boolean var2) {
      BiomeBase var3 = this.b(var1);
      float var4 = var3.a(var1);
      if(var4 > 0.15F) {
         return false;
      } else {
         if(var1.q() >= 0 && var1.q() < 256 && this.b(EnumSkyBlock.BLOCK, var1) < 10) {
            IBlockData var5 = this.getType(var1);
            Block var6 = var5.getBlock();
            if((var6 == Blocks.j || var6 == Blocks.i) && ((Integer)var5.get(class_amn.b)).intValue() == 0) {
               if(!var2) {
                  return true;
               }

               boolean var7 = this.E(var1.e()) && this.E(var1.f()) && this.E(var1.c()) && this.E(var1.d());
               if(!var7) {
                  return true;
               }
            }
         }

         return false;
      }
   }

   private boolean E(BlockPosition var1) {
      return this.getType(var1).getMaterial() == Material.h;
   }

   public boolean f(BlockPosition var1, boolean var2) {
      BiomeBase var3 = this.b(var1);
      float var4 = var3.a(var1);
      if(var4 > 0.15F) {
         return false;
      } else if(!var2) {
         return true;
      } else {
         if(var1.q() >= 0 && var1.q() < 256 && this.b(EnumSkyBlock.BLOCK, var1) < 10) {
            IBlockData var5 = this.getType(var1);
            if(var5.getMaterial() == Material.a && Blocks.aH.a(this, var1)) {
               return true;
            }
         }

         return false;
      }
   }

   public boolean w(BlockPosition var1) {
      boolean var2 = false;
      if(!this.s.isNotOverworld()) {
         var2 |= this.c(EnumSkyBlock.SKY, var1);
      }

      var2 |= this.c(EnumSkyBlock.BLOCK, var1);
      return var2;
   }

   private int a(BlockPosition var1, EnumSkyBlock var2) {
      if(var2 == EnumSkyBlock.SKY && this.h(var1)) {
         return 15;
      } else {
         IBlockData var3 = this.getType(var1);
         int var4 = var2 == EnumSkyBlock.SKY?0:var3.d();
         int var5 = var3.c();
         if(var5 >= 15 && var3.d() > 0) {
            var5 = 1;
         }

         if(var5 < 1) {
            var5 = 1;
         }

         if(var5 >= 15) {
            return 0;
         } else if(var4 >= 14) {
            return var4;
         } else {
            BlockPosition.class_b_in_class_cj var6 = BlockPosition.class_b_in_class_cj.s();
            EnumDirection[] var7 = EnumDirection.values();
            int var8 = var7.length;

            for(int var9 = 0; var9 < var8; ++var9) {
               EnumDirection var10 = var7[var9];
               var6.h(var1).c(var10);
               int var11 = this.b((EnumSkyBlock)var2, (BlockPosition)var6) - var5;
               if(var11 > var4) {
                  var4 = var11;
               }

               if(var4 >= 14) {
                  return var4;
               }
            }

            var6.t();
            return var4;
         }
      }
   }

   public boolean c(EnumSkyBlock var1, BlockPosition var2) {
      if(!this.a(var2, 17, false)) {
         return false;
      } else {
         int var3 = 0;
         int var4 = 0;
         this.C.a("getBrightness");
         int var5 = this.b(var1, var2);
         int var6 = this.a(var2, var1);
         int var7 = var2.p();
         int var8 = var2.q();
         int var9 = var2.r();
         int var10;
         int var11;
         int var12;
         int var13;
         int var16;
         int var17;
         int var18;
         int var19;
         if(var6 > var5) {
            this.H[var4++] = 133152;
         } else if(var6 < var5) {
            this.H[var4++] = 133152 | var5 << 18;

            label92:
            while(true) {
               int var14;
               do {
                  do {
                     BlockPosition var15;
                     do {
                        if(var3 >= var4) {
                           var3 = 0;
                           break label92;
                        }

                        var10 = this.H[var3++];
                        var11 = (var10 & 63) - 32 + var7;
                        var12 = (var10 >> 6 & 63) - 32 + var8;
                        var13 = (var10 >> 12 & 63) - 32 + var9;
                        var14 = var10 >> 18 & 15;
                        var15 = new BlockPosition(var11, var12, var13);
                        var16 = this.b(var1, var15);
                     } while(var16 != var14);

                     this.a((EnumSkyBlock)var1, (BlockPosition)var15, 0);
                  } while(var14 <= 0);

                  var17 = MathHelper.a(var11 - var7);
                  var18 = MathHelper.a(var12 - var8);
                  var19 = MathHelper.a(var13 - var9);
               } while(var17 + var18 + var19 >= 17);

               BlockPosition.class_b_in_class_cj var20 = BlockPosition.class_b_in_class_cj.s();
               EnumDirection[] var21 = EnumDirection.values();
               int var22 = var21.length;

               for(int var23 = 0; var23 < var22; ++var23) {
                  EnumDirection var24 = var21[var23];
                  int var25 = var11 + var24.g();
                  int var26 = var12 + var24.h();
                  int var27 = var13 + var24.i();
                  var20.d(var25, var26, var27);
                  int var28 = Math.max(1, this.getType(var20).c());
                  var16 = this.b((EnumSkyBlock)var1, (BlockPosition)var20);
                  if(var16 == var14 - var28 && var4 < this.H.length) {
                     this.H[var4++] = var25 - var7 + 32 | var26 - var8 + 32 << 6 | var27 - var9 + 32 << 12 | var14 - var28 << 18;
                  }
               }

               var20.t();
            }
         }

         this.C.b();
         this.C.a("checkedPosition < toCheckCount");

         while(var3 < var4) {
            var10 = this.H[var3++];
            var11 = (var10 & 63) - 32 + var7;
            var12 = (var10 >> 6 & 63) - 32 + var8;
            var13 = (var10 >> 12 & 63) - 32 + var9;
            BlockPosition var29 = new BlockPosition(var11, var12, var13);
            int var30 = this.b(var1, var29);
            var16 = this.a(var29, var1);
            if(var16 != var30) {
               this.a(var1, var29, var16);
               if(var16 > var30) {
                  var17 = Math.abs(var11 - var7);
                  var18 = Math.abs(var12 - var8);
                  var19 = Math.abs(var13 - var9);
                  boolean var31 = var4 < this.H.length - 6;
                  if(var17 + var18 + var19 < 17 && var31) {
                     if(this.b(var1, var29.e()) < var16) {
                        this.H[var4++] = var11 - 1 - var7 + 32 + (var12 - var8 + 32 << 6) + (var13 - var9 + 32 << 12);
                     }

                     if(this.b(var1, var29.f()) < var16) {
                        this.H[var4++] = var11 + 1 - var7 + 32 + (var12 - var8 + 32 << 6) + (var13 - var9 + 32 << 12);
                     }

                     if(this.b(var1, var29.b()) < var16) {
                        this.H[var4++] = var11 - var7 + 32 + (var12 - 1 - var8 + 32 << 6) + (var13 - var9 + 32 << 12);
                     }

                     if(this.b(var1, var29.a()) < var16) {
                        this.H[var4++] = var11 - var7 + 32 + (var12 + 1 - var8 + 32 << 6) + (var13 - var9 + 32 << 12);
                     }

                     if(this.b(var1, var29.c()) < var16) {
                        this.H[var4++] = var11 - var7 + 32 + (var12 - var8 + 32 << 6) + (var13 - 1 - var9 + 32 << 12);
                     }

                     if(this.b(var1, var29.d()) < var16) {
                        this.H[var4++] = var11 - var7 + 32 + (var12 - var8 + 32 << 6) + (var13 + 1 - var9 + 32 << 12);
                     }
                  }
               }
            }
         }

         this.C.b();
         return true;
      }
   }

   public boolean a(boolean var1) {
      return false;
   }

   public List a(Chunk var1, boolean var2) {
      return null;
   }

   public List a(StructureBoundingBox var1, boolean var2) {
      return null;
   }

   public List b(Entity var1, AxisAlignedBB var2) {
      return this.a(var1, var2, class_ru.e);
   }

   public List a(Entity var1, AxisAlignedBB var2, Predicate var3) {
      ArrayList var4 = Lists.newArrayList();
      int var5 = MathHelper.c((var2.a - 2.0D) / 16.0D);
      int var6 = MathHelper.c((var2.d + 2.0D) / 16.0D);
      int var7 = MathHelper.c((var2.c - 2.0D) / 16.0D);
      int var8 = MathHelper.c((var2.f + 2.0D) / 16.0D);

      for(int var9 = var5; var9 <= var6; ++var9) {
         for(int var10 = var7; var10 <= var8; ++var10) {
            if(this.a(var9, var10, true)) {
               this.a(var9, var10).a((Entity)var1, var2, var4, var3);
            }
         }
      }

      return var4;
   }

   public List a(Class var1, Predicate var2) {
      ArrayList var3 = Lists.newArrayList();
      Iterator var4 = this.e.iterator();

      while(var4.hasNext()) {
         Entity var5 = (Entity)var4.next();
         if(var1.isAssignableFrom(var5.getClass()) && var2.apply(var5)) {
            var3.add(var5);
         }
      }

      return var3;
   }

   public List b(Class var1, Predicate var2) {
      ArrayList var3 = Lists.newArrayList();
      Iterator var4 = this.i.iterator();

      while(var4.hasNext()) {
         Entity var5 = (Entity)var4.next();
         if(var1.isAssignableFrom(var5.getClass()) && var2.apply(var5)) {
            var3.add(var5);
         }
      }

      return var3;
   }

   public List a(Class var1, AxisAlignedBB var2) {
      return this.a(var1, var2, class_ru.e);
   }

   public List a(Class var1, AxisAlignedBB var2, Predicate var3) {
      int var4 = MathHelper.c((var2.a - 2.0D) / 16.0D);
      int var5 = MathHelper.f((var2.d + 2.0D) / 16.0D);
      int var6 = MathHelper.c((var2.c - 2.0D) / 16.0D);
      int var7 = MathHelper.f((var2.f + 2.0D) / 16.0D);
      ArrayList var8 = Lists.newArrayList();

      for(int var9 = var4; var9 < var5; ++var9) {
         for(int var10 = var6; var10 < var7; ++var10) {
            if(this.a(var9, var10, true)) {
               this.a(var9, var10).a((Class)var1, var2, var8, var3);
            }
         }
      }

      return var8;
   }

   public Entity a(Class var1, AxisAlignedBB var2, Entity var3) {
      List var4 = this.a(var1, var2);
      Entity var5 = null;
      double var6 = Double.MAX_VALUE;

      for(int var8 = 0; var8 < var4.size(); ++var8) {
         Entity var9 = (Entity)var4.get(var8);
         if(var9 != var3 && class_ru.e.apply(var9)) {
            double var10 = var3.h(var9);
            if(var10 <= var6) {
               var5 = var9;
               var6 = var10;
            }
         }
      }

      return var5;
   }

   public Entity a(int var1) {
      return (Entity)this.k.a(var1);
   }

   public void b(BlockPosition var1, TileEntity var2) {
      if(this.e(var1)) {
         this.f(var1).e();
      }

   }

   public int a(Class var1) {
      int var2 = 0;
      Iterator var3 = this.e.iterator();

      while(true) {
         Entity var4;
         do {
            if(!var3.hasNext()) {
               return var2;
            }

            var4 = (Entity)var3.next();
         } while(var4 instanceof EntityInsentient && ((EntityInsentient)var4).cN());

         if(var1.isAssignableFrom(var4.getClass())) {
            ++var2;
         }
      }
   }

   public void a(Collection var1) {
      this.e.addAll(var1);
      Iterator var2 = var1.iterator();

      while(var2.hasNext()) {
         Entity var3 = (Entity)var2.next();
         this.b(var3);
      }

   }

   public void c(Collection var1) {
      this.f.addAll(var1);
   }

   public boolean a(Block var1, BlockPosition var2, boolean var3, EnumDirection var4, Entity var5, ItemStack var6) {
      IBlockData var7 = this.getType(var2);
      AxisAlignedBB var8 = var3?null:var1.u().d(this, var2);
      return var8 != Block.k && !this.a(var8.a(var2), var5)?false:(var7.getMaterial() == Material.q && var1 == Blocks.cf?true:var7.getMaterial().j() && var1.a(this, var2, var4, var6));
   }

   public int K() {
      return this.a;
   }

   public void b(int var1) {
      this.a = var1;
   }

   public int a(BlockPosition var1, EnumDirection var2) {
      return this.getType(var1).b(this, var1, var2);
   }

   public WorldType L() {
      return this.x.t();
   }

   public int x(BlockPosition var1) {
      byte var2 = 0;
      int var3 = Math.max(var2, this.a(var1.b(), EnumDirection.DOWN));
      if(var3 >= 15) {
         return var3;
      } else {
         var3 = Math.max(var3, this.a(var1.a(), EnumDirection.UP));
         if(var3 >= 15) {
            return var3;
         } else {
            var3 = Math.max(var3, this.a(var1.c(), EnumDirection.NORTH));
            if(var3 >= 15) {
               return var3;
            } else {
               var3 = Math.max(var3, this.a(var1.d(), EnumDirection.SOUTH));
               if(var3 >= 15) {
                  return var3;
               } else {
                  var3 = Math.max(var3, this.a(var1.e(), EnumDirection.WEST));
                  if(var3 >= 15) {
                     return var3;
                  } else {
                     var3 = Math.max(var3, this.a(var1.f(), EnumDirection.EAST));
                     return var3 >= 15?var3:var3;
                  }
               }
            }
         }
      }
   }

   public boolean b(BlockPosition var1, EnumDirection var2) {
      return this.c(var1, var2) > 0;
   }

   public int c(BlockPosition var1, EnumDirection var2) {
      IBlockData var3 = this.getType(var1);
      return var3.l()?this.x(var1):var3.a(this, var1, var2);
   }

   public boolean y(BlockPosition var1) {
      return this.c(var1.b(), EnumDirection.DOWN) > 0?true:(this.c(var1.a(), EnumDirection.UP) > 0?true:(this.c(var1.c(), EnumDirection.NORTH) > 0?true:(this.c(var1.d(), EnumDirection.SOUTH) > 0?true:(this.c(var1.e(), EnumDirection.WEST) > 0?true:this.c(var1.f(), EnumDirection.EAST) > 0))));
   }

   public int z(BlockPosition var1) {
      int var2 = 0;
      EnumDirection[] var3 = EnumDirection.values();
      int var4 = var3.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         EnumDirection var6 = var3[var5];
         int var7 = this.c(var1.a(var6), var6);
         if(var7 >= 15) {
            return 15;
         }

         if(var7 > var2) {
            var2 = var7;
         }
      }

      return var2;
   }

   public EntityHuman a(Entity var1, double var2) {
      return this.a(var1.locX, var1.locY, var1.locZ, var2, false);
   }

   public EntityHuman b(Entity var1, double var2) {
      return this.a(var1.locX, var1.locY, var1.locZ, var2, true);
   }

   public EntityHuman a(double var1, double var3, double var5, double var7, boolean var9) {
      double var10 = -1.0D;
      EntityHuman var12 = null;

      for(int var13 = 0; var13 < this.i.size(); ++var13) {
         EntityHuman var14 = (EntityHuman)this.i.get(var13);
         if((class_ru.d.apply(var14) || !var9) && (class_ru.e.apply(var14) || var9)) {
            double var15 = var14.e(var1, var3, var5);
            if((var7 < 0.0D || var15 < var7 * var7) && (var10 == -1.0D || var15 < var10)) {
               var10 = var15;
               var12 = var14;
            }
         }
      }

      return var12;
   }

   public boolean a(double var1, double var3, double var5, double var7) {
      for(int var9 = 0; var9 < this.i.size(); ++var9) {
         EntityHuman var10 = (EntityHuman)this.i.get(var9);
         if(class_ru.e.apply(var10)) {
            double var11 = var10.e(var1, var3, var5);
            if(var7 < 0.0D || var11 < var7 * var7) {
               return true;
            }
         }
      }

      return false;
   }

   public EntityHuman a(Entity var1, double var2, double var4) {
      return this.a(var1.locX, var1.locY, var1.locZ, var2, var4, (Function)null, (Predicate)null);
   }

   public EntityHuman a(BlockPosition var1, double var2, double var4) {
      return this.a((double)((float)var1.p() + 0.5F), (double)((float)var1.q() + 0.5F), (double)((float)var1.r() + 0.5F), var2, var4, (Function)null, (Predicate)null);
   }

   public EntityHuman a(double var1, double var3, double var5, double var7, double var9, Function var11, Predicate var12) {
      double var13 = -1.0D;
      EntityHuman var15 = null;

      for(int var16 = 0; var16 < this.i.size(); ++var16) {
         EntityHuman var17 = (EntityHuman)this.i.get(var16);
         if(!var17.abilities.a && var17.at() && !var17.y() && (var12 == null || var12.apply(var17))) {
            double var18 = var17.e(var1, var17.locY, var5);
            double var20 = var7;
            if(var17.aJ()) {
               var20 = var7 * 0.800000011920929D;
            }

            if(var17.aM()) {
               float var22 = var17.cF();
               if(var22 < 0.1F) {
                  var22 = 0.1F;
               }

               var20 *= (double)(0.7F * var22);
            }

            if(var11 != null) {
               var20 *= ((Double)Objects.firstNonNull(var11.apply(var17), Double.valueOf(1.0D))).doubleValue();
            }

            if((var9 < 0.0D || Math.abs(var17.locY - var3) < var9 * var9) && (var7 < 0.0D || var18 < var20 * var20) && (var13 == -1.0D || var18 < var13)) {
               var13 = var18;
               var15 = var17;
            }
         }
      }

      return var15;
   }

   public EntityHuman a(String var1) {
      for(int var2 = 0; var2 < this.i.size(); ++var2) {
         EntityHuman var3 = (EntityHuman)this.i.get(var2);
         if(var1.equals(var3.h_())) {
            return var3;
         }
      }

      return null;
   }

   public EntityHuman b(UUID var1) {
      for(int var2 = 0; var2 < this.i.size(); ++var2) {
         EntityHuman var3 = (EntityHuman)this.i.get(var2);
         if(var1.equals(var3.getUniqueId())) {
            return var3;
         }
      }

      return null;
   }

   public void N() throws class_aht {
      this.w.c();
   }

   public long O() {
      return this.x.a();
   }

   public long P() {
      return this.x.e();
   }

   public long Q() {
      return this.x.f();
   }

   public void b(long var1) {
      this.x.c(var1);
   }

   public BlockPosition R() {
      BlockPosition var1 = new BlockPosition(this.x.b(), this.x.c(), this.x.d());
      if(!this.aj().a(var1)) {
         var1 = this.l(new BlockPosition(this.aj().f(), 0.0D, this.aj().g()));
      }

      return var1;
   }

   public void A(BlockPosition var1) {
      this.x.a(var1);
   }

   public boolean a(EntityHuman var1, BlockPosition var2) {
      return true;
   }

   public void a(Entity var1, byte var2) {
   }

   public class_ary z() {
      return this.v;
   }

   public void c(BlockPosition var1, Block var2, int var3, int var4) {
      var2.a(this, var1, this.getType(var1), var3, var4);
   }

   public class_azh S() {
      return this.w;
   }

   public WorldData T() {
      return this.x;
   }

   public GameRules U() {
      return this.x.w();
   }

   public void e() {
   }

   public float h(float var1) {
      return (this.p + (this.q - this.p) * var1) * this.j(var1);
   }

   public float j(float var1) {
      return this.n + (this.o - this.n) * var1;
   }

   public boolean V() {
      return (double)this.h(1.0F) > 0.9D;
   }

   public boolean W() {
      return (double)this.j(1.0F) > 0.2D;
   }

   public boolean B(BlockPosition var1) {
      if(!this.W()) {
         return false;
      } else if(!this.h(var1)) {
         return false;
      } else if(this.p(var1).q() > var1.q()) {
         return false;
      } else {
         BiomeBase var2 = this.b(var1);
         return var2.c()?false:(this.f(var1, false)?false:var2.d());
      }
   }

   public boolean C(BlockPosition var1) {
      BiomeBase var2 = this.b(var1);
      return var2.e();
   }

   public PersistentCollection X() {
      return this.z;
   }

   public void a(String var1, class_ayw var2) {
      this.z.a(var1, var2);
   }

   public class_ayw a(Class var1, String var2) {
      return this.z.a(var1, var2);
   }

   public int b(String var1) {
      return this.z.a(var1);
   }

   public void a(int var1, BlockPosition var2, int var3) {
      for(int var4 = 0; var4 < this.u.size(); ++var4) {
         ((class_ahu)this.u.get(var4)).a(var1, var2, var3);
      }

   }

   public void b(int var1, BlockPosition var2, int var3) {
      this.a((EntityHuman)null, var1, var2, var3);
   }

   public void a(EntityHuman var1, int var2, BlockPosition var3, int var4) {
      try {
         for(int var5 = 0; var5 < this.u.size(); ++var5) {
            ((class_ahu)this.u.get(var5)).a(var1, var2, var3, var4);
         }

      } catch (Throwable var8) {
         CrashReport var6 = CrashReport.a(var8, "Playing level event");
         CrashReportSystemDetails var7 = var6.a("Level event being played");
         var7.a((String)"Block coordinates", (Object)CrashReportSystemDetails.a(var3));
         var7.a((String)"Event source", (Object)var1);
         var7.a((String)"Event type", (Object)Integer.valueOf(var2));
         var7.a((String)"Event data", (Object)Integer.valueOf(var4));
         throw new class_e(var6);
      }
   }

   public int Y() {
      return 256;
   }

   public int Z() {
      return this.s.isNotOverworld()?128:256;
   }

   public Random a(int var1, int var2, int var3) {
      long var4 = (long)var1 * 341873128712L + (long)var2 * 132897987541L + this.T().a() + (long)var3;
      this.r.setSeed(var4);
      return this.r;
   }

   public CrashReportSystemDetails a(CrashReport var1) {
      CrashReportSystemDetails var2 = var1.a("Affected level", 1);
      var2.a((String)"Level name", (Object)(this.x == null?"????":this.x.j()));
      var2.a("All players", new Callable() {
         public String a() {
            return World.this.i.size() + " total; " + World.this.i.toString();
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
      var2.a("Chunk stats", new Callable() {
         public String a() {
            return World.this.v.f();
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });

      try {
         this.x.a(var2);
      } catch (Throwable var4) {
         var2.a("Level Data Unobtainable", var4);
      }

      return var2;
   }

   public void c(int var1, BlockPosition var2, int var3) {
      for(int var4 = 0; var4 < this.u.size(); ++var4) {
         class_ahu var5 = (class_ahu)this.u.get(var4);
         var5.b(var1, var2, var3);
      }

   }

   public Calendar ac() {
      if(this.P() % 600L == 0L) {
         this.L.setTimeInMillis(MinecraftServer.av());
      }

      return this.L;
   }

   public Scoreboard ad() {
      return this.D;
   }

   public void f(BlockPosition var1, Block var2) {
      Iterator var3 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

      while(var3.hasNext()) {
         EnumDirection var4 = (EnumDirection)var3.next();
         BlockPosition var5 = var1.a(var4);
         if(this.e(var5)) {
            IBlockData var6 = this.getType(var5);
            if(Blocks.cj.C(var6)) {
               var6.getBlock().a(this, var5, var6, var2);
            } else if(var6.l()) {
               var5 = var5.a(var4);
               var6 = this.getType(var5);
               if(Blocks.cj.C(var6)) {
                  var6.getBlock().a(this, var5, var6, var2);
               }
            }
         }
      }

   }

   public class_qk D(BlockPosition var1) {
      long var2 = 0L;
      float var4 = 0.0F;
      if(this.e(var1)) {
         var4 = this.E();
         var2 = this.f(var1).x();
      }

      return new class_qk(this.ae(), this.Q(), var2, var4);
   }

   public EnumDifficulty ae() {
      return this.T().x();
   }

   public int af() {
      return this.J;
   }

   public void c(int var1) {
      this.J = var1;
   }

   public void d(int var1) {
      this.K = var1;
   }

   public PersistentVillage ai() {
      return this.A;
   }

   public class_aru aj() {
      return this.N;
   }

   public boolean c(int var1, int var2) {
      BlockPosition var3 = this.R();
      int var4 = var1 * 16 + 8 - var3.p();
      int var5 = var2 * 16 + 8 - var3.r();
      short var6 = 128;
      return var4 >= -var6 && var4 <= var6 && var5 >= -var6 && var5 <= var6;
   }

   public void a(Packet var1) {
      throw new UnsupportedOperationException("Can\'t send packets to server unless you\'re on the client.");
   }

   public class_baa ak() {
      return this.B;
   }
}
