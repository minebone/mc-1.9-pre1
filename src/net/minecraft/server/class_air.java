package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.BlockLeaves;
import net.minecraft.server.BlockLongGrass;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.class_ane;
import net.minecraft.server.class_anf;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ato;
import net.minecraft.server.class_auc;
import net.minecraft.server.class_aue;
import net.minecraft.server.class_aun;
import net.minecraft.server.class_auq;
import net.minecraft.server.class_ave;
import net.minecraft.server.class_avf;
import net.minecraft.server.class_avg;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_vw;
import net.minecraft.server.class_wa;

public class class_air extends BiomeBase {
   private boolean y;
   private static final IBlockData z = Blocks.r.u().set(class_anf.b, BlockWood.EnumLogVariant.JUNGLE);
   private static final IBlockData A = Blocks.t.u().set(class_ane.e, BlockWood.EnumLogVariant.JUNGLE).set(BlockLeaves.b, Boolean.valueOf(false));
   private static final IBlockData B = Blocks.t.u().set(class_ane.e, BlockWood.EnumLogVariant.OAK).set(BlockLeaves.b, Boolean.valueOf(false));

   public class_air(boolean var1, BiomeBase.class_a_in_class_aif var2) {
      super(var2);
      this.y = var1;
      if(var1) {
         this.t.z = 2;
      } else {
         this.t.z = 50;
      }

      this.t.B = 25;
      this.t.A = 4;
      if(!var1) {
         this.u.add(new BiomeBase.BiomeMeta(class_wa.class, 2, 1, 1));
      }

      this.v.add(new BiomeBase.BiomeMeta(class_vw.class, 10, 4, 4));
   }

   public class_ato a(Random var1) {
      return (class_ato)(var1.nextInt(10) == 0?o:(var1.nextInt(2) == 0?new class_aue(z, B):(!this.y && var1.nextInt(3) == 0?new class_aun(false, 10, 20, z, A):new class_avf(false, 4 + var1.nextInt(7), z, A, true))));
   }

   public class_auc b(Random var1) {
      return var1.nextInt(4) == 0?new class_ave(BlockLongGrass.EnumTallGrassType.FERN):new class_ave(BlockLongGrass.EnumTallGrassType.GRASS);
   }

   public void a(World var1, Random var2, BlockPosition var3) {
      super.a(var1, var2, var3);
      int var4 = var2.nextInt(16) + 8;
      int var5 = var2.nextInt(16) + 8;
      int var6 = var2.nextInt(var1.l(var3.a(var4, 0, var5)).q() * 2);
      (new class_auq()).b(var1, var2, var3.a(var4, var6, var5));
      class_avg var9 = new class_avg();

      for(var5 = 0; var5 < 50; ++var5) {
         var6 = var2.nextInt(16) + 8;
         boolean var7 = true;
         int var8 = var2.nextInt(16) + 8;
         var9.b(var1, var2, var3.a(var6, 128, var8));
      }

   }
}
