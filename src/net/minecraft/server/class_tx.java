package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.World;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_tj;

public class class_tx extends class_tj {
   World a;
   EntityInsentient b;
   class_rz c;
   int d;

   public class_tx(EntityInsentient var1) {
      this.b = var1;
      this.a = var1.world;
      this.a(3);
   }

   public boolean a() {
      class_rz var1 = this.b.A();
      if(var1 == null) {
         return false;
      } else {
         this.c = var1;
         return true;
      }
   }

   public boolean b() {
      return !this.c.at()?false:(this.b.h(this.c) > 225.0D?false:!this.b.x().n() || this.a());
   }

   public void d() {
      this.c = null;
      this.b.x().o();
   }

   public void e() {
      this.b.t().a(this.c, 30.0F, 30.0F);
      double var1 = (double)(this.b.width * 2.0F * this.b.width * 2.0F);
      double var3 = this.b.e(this.c.locX, this.c.bk().b, this.c.locZ);
      double var5 = 0.8D;
      if(var3 > var1 && var3 < 16.0D) {
         var5 = 1.33D;
      } else if(var3 < 225.0D) {
         var5 = 0.6D;
      }

      this.b.x().a((Entity)this.c, var5);
      this.d = Math.max(this.d - 1, 0);
      if(var3 <= var1) {
         if(this.d <= 0) {
            this.d = 20;
            this.b.B(this.c);
         }
      }
   }
}
