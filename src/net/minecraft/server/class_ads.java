package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ali;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_xs;

public class class_ads extends Item {
   public class_ads() {
      this.a(CreativeModeTab.i);
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      Block var10 = var3.getType(var4).getBlock();
      if(!(var10 instanceof class_ali)) {
         return EnumResult.PASS;
      } else {
         if(!var3.E) {
            a(var2, var3, var4);
         }

         return EnumResult.SUCCESS;
      }
   }

   public static boolean a(EntityHuman var0, World var1, BlockPosition var2) {
      class_xs var3 = class_xs.b(var1, var2);
      boolean var4 = false;
      double var5 = 7.0D;
      int var7 = var2.p();
      int var8 = var2.q();
      int var9 = var2.r();
      List var10 = var1.a(EntityInsentient.class, new AxisAlignedBB((double)var7 - var5, (double)var8 - var5, (double)var9 - var5, (double)var7 + var5, (double)var8 + var5, (double)var9 + var5));
      Iterator var11 = var10.iterator();

      while(var11.hasNext()) {
         EntityInsentient var12 = (EntityInsentient)var11.next();
         if(var12.cP() && var12.cQ() == var0) {
            if(var3 == null) {
               var3 = class_xs.a(var1, var2);
            }

            var12.b(var3, true);
            var4 = true;
         }
      }

      return var4;
   }
}
