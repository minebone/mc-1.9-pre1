package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.class_alf;
import net.minecraft.server.class_aoa;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public abstract class class_ajm extends Block implements class_alf {
   protected class_ajm(Material var1) {
      this(var1, var1.r());
   }

   protected class_ajm(Material var1, MaterialMapColor var2) {
      super(var1, var2);
      this.u = true;
   }

   protected boolean a(World var1, BlockPosition var2, EnumDirection var3) {
      return var1.getType(var2.a(var3)).getMaterial() == Material.A;
   }

   protected boolean b(World var1, BlockPosition var2) {
      return this.a(var1, var2, EnumDirection.NORTH) || this.a(var1, var2, EnumDirection.SOUTH) || this.a(var1, var2, EnumDirection.WEST) || this.a(var1, var2, EnumDirection.EAST);
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.INVISIBLE;
   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      super.b(var1, var2, var3);
      var1.s(var2);
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, int var4, int var5) {
      super.a(var1, var2, var3, var4, var5);
      TileEntity var6 = var1.r(var2);
      return var6 == null?false:var6.c(var4, var5);
   }
}
