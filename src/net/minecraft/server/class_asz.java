package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.Lists;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Blocks;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.Chunk;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.GameProfileSerializer;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.ShapeDetector;
import net.minecraft.server.ShapeDetectorBuilder;
import net.minecraft.server.TileEntity;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_aje;
import net.minecraft.server.class_aqq;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_are;
import net.minecraft.server.class_ari;
import net.minecraft.server.class_asy;
import net.minecraft.server.class_atz;
import net.minecraft.server.class_aub;
import net.minecraft.server.class_ava;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_dt;
import net.minecraft.server.class_ln;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.BossBar;
import net.minecraft.server.class_ru;
import net.minecraft.server.class_ws;
import net.minecraft.server.class_wt;
import net.minecraft.server.class_xj;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_asz {
   private static final Logger a = LogManager.getLogger();
   private static final Predicate b = Predicates.and(class_ru.a, class_ru.a(0.0D, 128.0D, 0.0D, 192.0D));
   private final class_ln c = (class_ln)(new class_ln(new ChatMessage("entity.EnderDragon.name", new Object[0]), BossBar.EnumBossBarColor.PINK, BossBar.EnumBossBarType.PROGRESS)).b(true).c(true);
   private final WorldServer d;
   private final List e = Lists.newArrayList();
   private final ShapeDetector f;
   private int g = 0;
   private int h = 0;
   private int i = 0;
   private int j = 0;
   private boolean k = false;
   private boolean l = false;
   private UUID m = null;
   private boolean n = false;
   private BlockPosition o = null;
   private class_asy p;
   private int q = 0;
   private List r;

   public class_asz(WorldServer var1, NBTTagCompound var2) {
      this.d = var1;
      if(var2.b("DragonKilled", 1)) {
         if(var2.b("DragonUUID")) {
            this.m = var2.a("DragonUUID");
         }

         this.k = var2.p("DragonKilled");
         this.l = var2.p("PreviouslyKilled");
         if(var2.p("IsRespawning")) {
            this.p = class_asy.a;
         }

         if(var2.b("ExitPortalLocation", 10)) {
            this.o = GameProfileSerializer.c(var2.o("ExitPortalLocation"));
         }
      } else {
         this.n = true;
         this.k = true;
         this.l = true;
      }

      if(var2.b("Gateways", 9)) {
         NBTTagList var3 = var2.c("Gateways", 3);

         for(int var4 = 0; var4 < var3.c(); ++var4) {
            this.e.add(Integer.valueOf(var3.c(var4)));
         }
      } else {
         this.e.addAll(ContiguousSet.create(Range.closedOpen(Integer.valueOf(0), Integer.valueOf(20)), DiscreteDomain.integers()));
         Collections.shuffle(this.e, new Random(var1.O()));
      }

      this.f = ShapeDetectorBuilder.a().a(new String[]{"       ", "       ", "       ", "   #   ", "       ", "       ", "       "}).a(new String[]{"       ", "       ", "       ", "   #   ", "       ", "       ", "       "}).a(new String[]{"       ", "       ", "       ", "   #   ", "       ", "       ", "       "}).a(new String[]{"  ###  ", " #   # ", "#     #", "#  #  #", "#     #", " #   # ", "  ###  "}).a(new String[]{"       ", "  ###  ", " ##### ", " ##### ", " ##### ", "  ###  ", "       "}).a('#', class_are.a((Predicate)class_ari.a(Blocks.h))).b();
   }

   public NBTTagCompound a() {
      if(this.n) {
         return new NBTTagCompound();
      } else {
         NBTTagCompound var1 = new NBTTagCompound();
         if(this.m != null) {
            var1.a("DragonUUID", this.m);
         }

         var1.a("DragonKilled", this.k);
         var1.a("PreviouslyKilled", this.l);
         if(this.o != null) {
            var1.a((String)"ExitPortalLocation", (NBTTag)GameProfileSerializer.a(this.o));
         }

         NBTTagList var2 = new NBTTagList();
         Iterator var3 = this.e.iterator();

         while(var3.hasNext()) {
            int var4 = ((Integer)var3.next()).intValue();
            var2.a((NBTTag)(new class_dt(var4)));
         }

         var1.a((String)"Gateways", (NBTTag)var2);
         return var1;
      }
   }

   public void b() {
      this.c.d(!this.k);
      if(++this.j >= 20) {
         this.j();
         this.j = 0;
      }

      if(!this.c.c().isEmpty()) {
         List var1;
         if(this.n) {
            a.info("Scanning for legacy world dragon fight...");
            this.i();
            this.n = false;
            if(this.g()) {
               a.info("Found that the dragon has been killed in this world already.");
               this.l = true;
            } else {
               a.info("Found that the dragon has not yet been killed in this world.");
               this.l = false;
            }

            var1 = this.d.a((Class)class_wt.class, (Predicate)class_ru.a);
            if(!var1.isEmpty()) {
               class_wt var2 = (class_wt)var1.get(0);
               this.m = var2.getUniqueId();
               a.info("Found that there\'s a dragon still alive (" + var2 + ")");
               this.k = false;
            } else {
               this.k = true;
            }

            if(!this.l && this.k) {
               this.k = false;
            }
         }

         if(this.p != null) {
            if(this.r == null) {
               this.p = null;
               this.e();
            }

            this.p.a(this.d, this, this.r, this.q++, this.o);
         }

         if(!this.k) {
            if(this.m == null || ++this.g >= 1200) {
               this.i();
               var1 = this.d.a((Class)class_wt.class, (Predicate)class_ru.a);
               if(!var1.isEmpty()) {
                  a.debug("Haven\'t seen our dragon, but found another one to use.");
                  this.m = ((class_wt)var1.get(0)).getUniqueId();
               } else {
                  a.debug("Haven\'t seen the dragon, respawning it");
                  this.m();
                  this.a(false);
               }

               this.g = 0;
            }

            if(++this.i >= 100) {
               this.k();
               this.i = 0;
            }
         }
      }

   }

   protected void a(class_asy var1) {
      if(this.p == null) {
         throw new IllegalStateException("Dragon respawn isn\'t in progress, can\'t skip ahead in the animation.");
      } else {
         this.q = 0;
         if(var1 == class_asy.e) {
            this.p = null;
            this.k = false;
            this.m();
         } else {
            this.p = var1;
         }

      }
   }

   private boolean g() {
      for(int var1 = -8; var1 <= 8; ++var1) {
         for(int var2 = -8; var2 <= 8; ++var2) {
            Chunk var3 = this.d.a(var1, var2);
            Iterator var4 = var3.s().values().iterator();

            while(var4.hasNext()) {
               TileEntity var5 = (TileEntity)var4.next();
               if(var5 instanceof class_aqq) {
                  return true;
               }
            }
         }
      }

      return false;
   }

   private ShapeDetector.ShapeDetectorCollection h() {
      int var1;
      int var2;
      for(var1 = -8; var1 <= 8; ++var1) {
         for(var2 = -8; var2 <= 8; ++var2) {
            Chunk var3 = this.d.a(var1, var2);
            Iterator var4 = var3.s().values().iterator();

            while(var4.hasNext()) {
               TileEntity var5 = (TileEntity)var4.next();
               if(var5 instanceof class_aqq) {
                  ShapeDetector.ShapeDetectorCollection var6 = this.f.a(this.d, var5.v());
                  if(var6 != null) {
                     BlockPosition var7 = var6.a(3, 3, 4).d();
                     if(this.o == null && var7.p() == 0 && var7.r() == 0) {
                        this.o = var7;
                     }

                     return var6;
                  }
               }
            }
         }
      }

      var1 = this.d.l(class_aub.a).q();

      for(var2 = var1; var2 >= 0; --var2) {
         ShapeDetector.ShapeDetectorCollection var8 = this.f.a(this.d, new BlockPosition(class_aub.a.p(), var2, class_aub.a.r()));
         if(var8 != null) {
            if(this.o == null) {
               this.o = var8.a(3, 3, 4).d();
            }

            return var8;
         }
      }

      return null;
   }

   private void i() {
      for(int var1 = -8; var1 <= 8; ++var1) {
         for(int var2 = -8; var2 <= 8; ++var2) {
            this.d.a(var1, var2);
         }
      }

   }

   private void j() {
      HashSet var1 = Sets.newHashSet();
      Iterator var2 = this.d.b(EntityPlayer.class, b).iterator();

      while(var2.hasNext()) {
         EntityPlayer var3 = (EntityPlayer)var2.next();
         this.c.a(var3);
         var1.add(var3);
      }

      HashSet var5 = Sets.newHashSet((Iterable)this.c.c());
      var5.removeAll(var1);
      Iterator var6 = var5.iterator();

      while(var6.hasNext()) {
         EntityPlayer var4 = (EntityPlayer)var6.next();
         this.c.b(var4);
      }

   }

   private void k() {
      this.i = 0;
      this.h = 0;
      class_ava.class_a_in_class_ava[] var1 = class_aje.a(this.d);
      int var2 = var1.length;

      for(int var3 = 0; var3 < var2; ++var3) {
         class_ava.class_a_in_class_ava var4 = var1[var3];
         this.h += this.d.a((Class)class_ws.class, (AxisAlignedBB)var4.f()).size();
      }

      a.debug("Found {} end crystals still alive", new Object[]{Integer.valueOf(this.h)});
   }

   public void a(class_wt var1) {
      if(var1.getUniqueId().equals(this.m)) {
         this.c.a(0.0F);
         this.c.d(false);
         this.a(true);
         this.l();
         if(!this.l) {
            this.d.a((BlockPosition)this.d.l(class_aub.a), (IBlockData)Blocks.bI.u());
         }

         this.l = true;
         this.k = true;
      }

   }

   private void l() {
      if(!this.e.isEmpty()) {
         int var1 = ((Integer)this.e.remove(this.e.size() - 1)).intValue();
         int var2 = (int)(96.0D * Math.cos(2.0D * (-3.141592653589793D + 0.15707963267948966D * (double)var1)));
         int var3 = (int)(96.0D * Math.sin(2.0D * (-3.141592653589793D + 0.15707963267948966D * (double)var1)));
         this.a(new BlockPosition(var2, 75, var3));
      }
   }

   private void a(BlockPosition var1) {
      this.d.b(3000, var1, 0);
      (new class_atz()).b(this.d, new Random(), var1);
   }

   private void a(boolean var1) {
      class_aub var2 = new class_aub(var1);
      if(this.o == null) {
         for(this.o = this.d.q(class_aub.a).b(); this.d.getType(this.o).getBlock() == Blocks.h && this.o.q() > this.d.K(); this.o = this.o.b()) {
            ;
         }
      }

      var2.b(this.d, new Random(), this.o);
   }

   private void m() {
      this.d.f(new BlockPosition(0, 128, 0));
      class_wt var1 = new class_wt(this.d);
      var1.cT().a(class_xj.a);
      var1.b(0.0D, 128.0D, 0.0D, this.d.r.nextFloat() * 360.0F, 0.0F);
      this.d.a((Entity)var1);
      this.m = var1.getUniqueId();
   }

   public void b(class_wt var1) {
      if(var1.getUniqueId().equals(this.m)) {
         this.c.a(var1.bP() / var1.bV());
         this.g = 0;
      }

   }

   public int c() {
      return this.h;
   }

   public void a(class_ws var1, DamageSource var2) {
      if(this.p != null && this.r.contains(var1)) {
         a.debug("Aborting respawn sequence");
         this.p = null;
         this.q = 0;
         this.f();
         this.a(true);
      } else {
         this.k();
         Entity var3 = this.d.a(this.m);
         if(var3 instanceof class_wt) {
            ((class_wt)var3).a(var1, new BlockPosition(var1), var2);
         }
      }

   }

   public boolean d() {
      return this.l;
   }

   public void e() {
      if(this.k && this.p == null) {
         BlockPosition var1 = this.o;
         if(var1 == null) {
            a.debug("Tried to respawn, but need to find the portal first.");
            ShapeDetector.ShapeDetectorCollection var2 = this.h();
            if(var2 == null) {
               a.debug("Couldn\'t find a portal, so we made one.");
               this.a(true);
               var1 = this.o;
            } else {
               a.debug("Found the exit portal & temporarily using it.");
               var1 = var2.a(3, 3, 3).d();
            }
         }

         ArrayList var7 = Lists.newArrayList();
         BlockPosition var3 = var1.b(1);
         Iterator var4 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

         while(var4.hasNext()) {
            EnumDirection var5 = (EnumDirection)var4.next();
            List var6 = this.d.a((Class)class_ws.class, (AxisAlignedBB)(new AxisAlignedBB(var3.a(var5, 2))));
            if(var6.isEmpty()) {
               return;
            }

            var7.addAll(var6);
         }

         a.debug("Found all crystals, respawning dragon.");
         this.a((List)var7);
      }

   }

   private void a(List var1) {
      if(this.k && this.p == null) {
         for(ShapeDetector.ShapeDetectorCollection var2 = this.h(); var2 != null; var2 = this.h()) {
            for(int var3 = 0; var3 < this.f.c(); ++var3) {
               for(int var4 = 0; var4 < this.f.b(); ++var4) {
                  for(int var5 = 0; var5 < this.f.a(); ++var5) {
                     class_are var6 = var2.a(var3, var4, var5);
                     if(var6.a().getBlock() == Blocks.h || var6.a().getBlock() == Blocks.bF) {
                        this.d.a((BlockPosition)var6.d(), (IBlockData)Blocks.bH.u());
                     }
                  }
               }
            }
         }

         this.p = class_asy.a;
         this.q = 0;
         this.a(false);
         this.r = var1;
      }

   }

   public void f() {
      class_ava.class_a_in_class_ava[] var1 = class_aje.a(this.d);
      int var2 = var1.length;

      for(int var3 = 0; var3 < var2; ++var3) {
         class_ava.class_a_in_class_ava var4 = var1[var3];
         List var5 = this.d.a((Class)class_ws.class, (AxisAlignedBB)var4.f());
         Iterator var6 = var5.iterator();

         while(var6.hasNext()) {
            class_ws var7 = (class_ws)var6.next();
            var7.h(false);
            var7.a((BlockPosition)null);
         }
      }

   }
}
