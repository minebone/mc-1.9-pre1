package net.minecraft.server;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.class_to;
import net.minecraft.server.class_zd;

public class class_tp extends class_to {
   private final class_zd e;

   public class_tp(class_zd var1) {
      super(var1, EntityHuman.class, 8.0F);
      this.e = var1;
   }

   public boolean a() {
      if(this.e.dc()) {
         this.b = this.e.t_();
         return true;
      } else {
         return false;
      }
   }
}
