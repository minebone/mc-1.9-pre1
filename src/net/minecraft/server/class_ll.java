package net.minecraft.server;

import net.minecraft.server.MinecraftServer;
import net.minecraft.server.PersistentVillage;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_ars;
import net.minecraft.server.class_aru;
import net.minecraft.server.class_ayw;
import net.minecraft.server.class_azd;
import net.minecraft.server.class_azh;
import net.minecraft.server.class_oo;

public class class_ll extends WorldServer {
   private WorldServer a;

   public class_ll(MinecraftServer var1, class_azh var2, int var3, WorldServer var4, class_oo var5) {
      super(var1, var2, new class_azd(var4.T()), var3, var5);
      this.a = var4;
      var4.aj().a(new class_ars() {
         public void a(class_aru var1, double var2) {
            class_ll.this.aj().a(var2);
         }

         public void a(class_aru var1, double var2, double var4, long var6) {
            class_ll.this.aj().a(var2, var4, var6);
         }

         public void a(class_aru var1, double var2, double var4) {
            class_ll.this.aj().c(var2, var4);
         }

         public void a(class_aru var1, int var2) {
            class_ll.this.aj().b(var2);
         }

         public void b(class_aru var1, int var2) {
            class_ll.this.aj().c(var2);
         }

         public void b(class_aru var1, double var2) {
            class_ll.this.aj().c(var2);
         }

         public void c(class_aru var1, double var2) {
            class_ll.this.aj().b(var2);
         }
      });
   }

   protected void a() {
   }

   public World b() {
      this.z = this.a.X();
      this.D = this.a.ad();
      this.B = this.a.ak();
      String var1 = PersistentVillage.a(this.s);
      PersistentVillage var2 = (PersistentVillage)this.z.a(PersistentVillage.class, var1);
      if(var2 == null) {
         this.A = new PersistentVillage(this);
         this.z.a((String)var1, (class_ayw)this.A);
      } else {
         this.A = var2;
         this.A.a((World)this);
      }

      return this;
   }

   public void c() {
      this.s.q();
   }
}
