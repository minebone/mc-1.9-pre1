package net.minecraft.server;

import net.minecraft.server.class_agm;

public abstract class CreativeModeTab {
   public static final CreativeModeTab[] a = new CreativeModeTab[12];
   public static final CreativeModeTab b = new CreativeModeTab(0, "buildingBlocks") {
   };
   public static final CreativeModeTab c = new CreativeModeTab(1, "decorations") {
   };
   public static final CreativeModeTab d = new CreativeModeTab(2, "redstone") {
   };
   public static final CreativeModeTab e = new CreativeModeTab(3, "transportation") {
   };
   public static final CreativeModeTab f = (new CreativeModeTab(4, "misc") {
   }).a(new class_agm[]{class_agm.ALL});
   public static final CreativeModeTab g = (new CreativeModeTab(5, "search") {
   }).a("item_search.png");
   public static final CreativeModeTab h = new CreativeModeTab(6, "food") {
   };
   public static final CreativeModeTab i = (new CreativeModeTab(7, "tools") {
   }).a(new class_agm[]{class_agm.DIGGER, class_agm.FISHING_ROD, class_agm.BREAKABLE});
   public static final CreativeModeTab j = (new CreativeModeTab(8, "combat") {
   }).a(new class_agm[]{class_agm.ARMOR, class_agm.ARMOR_FEET, class_agm.ARMOR_HEAD, class_agm.ARMOR_LEGS, class_agm.ARMOR_CHEST, class_agm.BOW, class_agm.WEAPON});
   public static final CreativeModeTab k = new CreativeModeTab(9, "brewing") {
   };
   public static final CreativeModeTab l = new CreativeModeTab(10, "materials") {
   };
   public static final CreativeModeTab m = (new CreativeModeTab(11, "inventory") {
   }).a("inventory.png").k().i();
   private final int n;
   private final String o;
   private String p = "items.png";
   private boolean q = true;
   private boolean r = true;
   private class_agm[] s;

   public CreativeModeTab(int var1, String var2) {
      this.n = var1;
      this.o = var2;
      a[var1] = this;
   }

   public CreativeModeTab a(String var1) {
      this.p = var1;
      return this;
   }

   public CreativeModeTab i() {
      this.r = false;
      return this;
   }

   public CreativeModeTab k() {
      this.q = false;
      return this;
   }

   public CreativeModeTab a(class_agm... var1) {
      this.s = var1;
      return this;
   }
}
