package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_akm;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_qu;
import net.minecraft.server.class_tu;
import net.minecraft.server.class_zd;

public class class_tl extends class_tu {
   private final class_zd c;
   private boolean d;
   private boolean e;
   private int f;

   public class_tl(class_zd var1, double var2) {
      super(var1, var2, 16);
      this.c = var1;
   }

   public boolean a() {
      if(this.a <= 0) {
         if(!this.c.world.U().b("mobGriefing")) {
            return false;
         }

         this.f = -1;
         this.d = this.c.di();
         this.e = this.c.dh();
      }

      return super.a();
   }

   public boolean b() {
      return this.f >= 0 && super.b();
   }

   public void c() {
      super.c();
   }

   public void d() {
      super.d();
   }

   public void e() {
      super.e();
      this.c.t().a((double)this.b.p() + 0.5D, (double)(this.b.q() + 1), (double)this.b.r() + 0.5D, 10.0F, (float)this.c.cD());
      if(this.f()) {
         World var1 = this.c.world;
         BlockPosition var2 = this.b.a();
         IBlockData var3 = var1.getType(var2);
         Block var4 = var3.getBlock();
         if(this.f == 0 && var4 instanceof class_akm && ((class_akm)var4).y(var3)) {
            var1.b(var2, true);
         } else if(this.f == 1 && var4 == Blocks.AIR) {
            class_qu var5 = this.c.de();

            for(int var6 = 0; var6 < var5.u_(); ++var6) {
               ItemStack var7 = var5.a(var6);
               boolean var8 = false;
               if(var7 != null) {
                  if(var7.b() == Items.P) {
                     var1.a((BlockPosition)var2, (IBlockData)Blocks.aj.u(), 3);
                     var8 = true;
                  } else if(var7.b() == Items.cc) {
                     var1.a((BlockPosition)var2, (IBlockData)Blocks.cc.u(), 3);
                     var8 = true;
                  } else if(var7.b() == Items.cb) {
                     var1.a((BlockPosition)var2, (IBlockData)Blocks.cb.u(), 3);
                     var8 = true;
                  } else if(var7.b() == Items.cU) {
                     var1.a((BlockPosition)var2, (IBlockData)Blocks.cZ.u(), 3);
                     var8 = true;
                  }
               }

               if(var8) {
                  --var7.b;
                  if(var7.b <= 0) {
                     var5.a(var6, (ItemStack)null);
                  }
                  break;
               }
            }
         }

         this.f = -1;
         this.a = 10;
      }

   }

   protected boolean a(World var1, BlockPosition var2) {
      Block var3 = var1.getType(var2).getBlock();
      if(var3 == Blocks.ak) {
         var2 = var2.a();
         IBlockData var4 = var1.getType(var2);
         var3 = var4.getBlock();
         if(var3 instanceof class_akm && ((class_akm)var3).y(var4) && this.e && (this.f == 0 || this.f < 0)) {
            this.f = 0;
            return true;
         }

         if(var3 == Blocks.AIR && this.d && (this.f == 1 || this.f < 0)) {
            this.f = 1;
            return true;
         }
      }

      return false;
   }
}
