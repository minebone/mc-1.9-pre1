package net.minecraft.server;

import net.minecraft.server.Container;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Items;
import net.minecraft.server.class_abs;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_wj;
import net.minecraft.server.class_wk;

public class class_abj extends Container {
   private IInventory a;
   private class_wj f;

   public class_abj(IInventory var1, final IInventory var2, final class_wj var3, EntityHuman var4) {
      this.a = var2;
      this.f = var3;
      byte var5 = 3;
      var2.b(var4);
      int var6 = (var5 - 4) * 18;
      this.a(new class_abs(var2, 0, 8, 18) {
         public boolean a(ItemStack var1) {
            return super.a(var1) && var1.b() == Items.aC && !this.e();
         }
      });
      this.a(new class_abs(var2, 1, 8, 36) {
         public boolean a(ItemStack var1) {
            return super.a(var1) && var3.cZ().j() && class_wk.b(var1.b());
         }
      });
      int var7;
      int var8;
      if(var3.dk()) {
         for(var7 = 0; var7 < var5; ++var7) {
            for(var8 = 0; var8 < 5; ++var8) {
               this.a(new class_abs(var2, 2 + var8 + var7 * 5, 80 + var8 * 18, 18 + var7 * 18));
            }
         }
      }

      for(var7 = 0; var7 < 3; ++var7) {
         for(var8 = 0; var8 < 9; ++var8) {
            this.a(new class_abs(var1, var8 + var7 * 9 + 9, 8 + var8 * 18, 102 + var7 * 18 + var6));
         }
      }

      for(var7 = 0; var7 < 9; ++var7) {
         this.a(new class_abs(var1, var7, 8 + var7 * 18, 160 + var6));
      }

   }

   public boolean a(EntityHuman var1) {
      return this.a.a(var1) && this.f.at() && this.f.g(var1) < 8.0F;
   }

   public ItemStack b(EntityHuman var1, int var2) {
      ItemStack var3 = null;
      class_abs var4 = (class_abs)this.c.get(var2);
      if(var4 != null && var4.e()) {
         ItemStack var5 = var4.d();
         var3 = var5.k();
         if(var2 < this.a.u_()) {
            if(!this.a(var5, this.a.u_(), this.c.size(), true)) {
               return null;
            }
         } else if(this.a(1).a(var5) && !this.a(1).e()) {
            if(!this.a(var5, 1, 2, false)) {
               return null;
            }
         } else if(this.a(0).a(var5)) {
            if(!this.a(var5, 0, 1, false)) {
               return null;
            }
         } else if(this.a.u_() <= 2 || !this.a(var5, 2, this.a.u_(), false)) {
            return null;
         }

         if(var5.b == 0) {
            var4.d((ItemStack)null);
         } else {
            var4.f();
         }
      }

      return var3;
   }

   public void b(EntityHuman var1) {
      super.b(var1);
      this.a.c(var1);
   }
}
