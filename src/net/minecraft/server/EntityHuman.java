package net.minecraft.server;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.mojang.authlib.GameProfile;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import net.minecraft.server.AchievementList;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockBed;
import net.minecraft.server.Blocks;
import net.minecraft.server.ChatClickable;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChestLock;
import net.minecraft.server.Container;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EntityMinecartAbstract;
import net.minecraft.server.EntityTypes;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumMainHandOption;
import net.minecraft.server.EnumMonsterType;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.IScoreboardCriteria;
import net.minecraft.server.InventoryEnderChest;
import net.minecraft.server.ItemArmor;
import net.minecraft.server.Items;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.MobEffectType;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.PlayerAbilities;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.Scoreboard;
import net.minecraft.server.ScoreboardScore;
import net.minecraft.server.ScoreboardTeam;
import net.minecraft.server.ScoreboardTeamBase;
import net.minecraft.server.Statistic;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.WorldSettings;
import net.minecraft.server.EntityBoat;
import net.minecraft.server.class_aar;
import net.minecraft.server.class_abk;
import net.minecraft.server.class_aby;
import net.minecraft.server.class_acw;
import net.minecraft.server.class_ado;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aew;
import net.minecraft.server.class_agn;
import net.minecraft.server.class_ahe;
import net.minecraft.server.class_ahi;
import net.minecraft.server.class_amf;
import net.minecraft.server.class_apx;
import net.minecraft.server.class_aqm;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.class_bbk;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutEntityVelocity;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;
import net.minecraft.server.IInventory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_qm;
import net.minecraft.server.EnumResult;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sl;
import net.minecraft.server.class_wb;
import net.minecraft.server.class_wj;
import net.minecraft.server.class_wq;
import net.minecraft.server.class_wr;
import net.minecraft.server.class_xv;
import net.minecraft.server.class_yc;
import net.minecraft.server.class_yk;
import net.minecraft.server.class_yp;
import net.minecraft.server.class_ys;
import net.minecraft.server.class_zl;

public abstract class EntityHuman extends class_rz {
   private static final class_ke a = DataWatcher.a(EntityHuman.class, class_kg.c);
   private static final class_ke b = DataWatcher.a(EntityHuman.class, class_kg.b);
   protected static final class_ke bp = DataWatcher.a(EntityHuman.class, class_kg.a);
   protected static final class_ke bq = DataWatcher.a(EntityHuman.class, class_kg.a);
   public PlayerInventory br = new PlayerInventory(this);
   private InventoryEnderChest c = new InventoryEnderChest();
   public Container bs;
   public Container bt;
   protected class_aar bu = new class_aar();
   protected int bv;
   public float bw;
   public float bx;
   public int by;
   public double bz;
   public double bA;
   public double bB;
   public double bC;
   public double bD;
   public double bE;
   protected boolean bF;
   public BlockPosition bG;
   private int d;
   public float bH;
   public float bI;
   private BlockPosition e;
   private boolean f;
   private BlockPosition g;
   public PlayerAbilities abilities = new PlayerAbilities();
   public int bK;
   public int bL;
   public float bM;
   private int h;
   protected float bN = 0.1F;
   protected float bO = 0.02F;
   private int bQ;
   private final GameProfile bR;
   private boolean bS = false;
   private ItemStack bT = null;
   private final class_ado bU = this.l();
   public class_xv bP;

   protected class_ado l() {
      return new class_ado();
   }

   public EntityHuman(World var1, GameProfile var2) {
      super(var1);
      this.uniqueID = a(var2);
      this.bR = var2;
      this.bs = new class_abk(this.br, !var1.E, this);
      this.bt = this.bs;
      BlockPosition var3 = var1.R();
      this.b((double)var3.p() + 0.5D, (double)(var3.q() + 1), (double)var3.r() + 0.5D, 0.0F, 0.0F);
      this.aZ = 180.0F;
      this.maxFireTicks = 20;
   }

   protected void bz() {
      super.bz();
      this.bY().b(class_ys.e).a(1.0D);
      this.a((class_sk)class_ys.d).a(0.10000000149011612D);
      this.bY().b(class_ys.f);
      this.bY().b(class_ys.h);
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)a, (Object)Float.valueOf(0.0F));
      this.datawatcher.a((class_ke)b, (Object)Integer.valueOf(0));
      this.datawatcher.a((class_ke)bp, (Object)Byte.valueOf((byte)0));
      this.datawatcher.a((class_ke)bq, (Object)Byte.valueOf((byte)1));
   }

   public void m() {
      this.noClip = this.y();
      if(this.y()) {
         this.onGround = false;
      }

      if(this.by > 0) {
         --this.by;
      }

      if(this.ck()) {
         ++this.d;
         if(this.d > 100) {
            this.d = 100;
         }

         if(!this.world.E) {
            if(!this.r()) {
               this.a(true, true, false);
            } else if(this.world.B()) {
               this.a(false, true, true);
            }
         }
      } else if(this.d > 0) {
         ++this.d;
         if(this.d >= 110) {
            this.d = 0;
         }
      }

      super.m();
      if(!this.world.E && this.bt != null && !this.bt.a(this)) {
         this.q();
         this.bt = this.bs;
      }

      if(this.aG() && this.abilities.a) {
         this.W();
      }

      this.o();
      if(!this.aH()) {
         this.g = null;
      }

      if(!this.world.E) {
         this.bu.a(this);
         this.b(StatisticList.g);
         if(this.at()) {
            this.b(StatisticList.h);
         }

         if(this.aJ()) {
            this.b(StatisticList.i);
         }
      }

      int var1 = 29999999;
      double var2 = MathHelper.a(this.locX, -2.9999999E7D, 2.9999999E7D);
      double var4 = MathHelper.a(this.locZ, -2.9999999E7D, 2.9999999E7D);
      if(var2 != this.locX || var4 != this.locZ) {
         this.b(var2, this.locY, var4);
      }

      ++this.aD;
      ItemStack var6 = this.ca();
      if(!ItemStack.b(this.bT, var6)) {
         if(!ItemStack.d(this.bT, var6)) {
            this.cY();
         }

         this.bT = var6 == null?null:var6.k();
      }

      this.bU.a();
      this.cD();
   }

   private void o() {
      this.bz = this.bC;
      this.bA = this.bD;
      this.bB = this.bE;
      double var1 = this.locX - this.bC;
      double var3 = this.locY - this.bD;
      double var5 = this.locZ - this.bE;
      double var7 = 10.0D;
      if(var1 > var7) {
         this.bz = this.bC = this.locX;
      }

      if(var5 > var7) {
         this.bB = this.bE = this.locZ;
      }

      if(var3 > var7) {
         this.bA = this.bD = this.locY;
      }

      if(var1 < -var7) {
         this.bz = this.bC = this.locX;
      }

      if(var5 < -var7) {
         this.bB = this.bE = this.locZ;
      }

      if(var3 < -var7) {
         this.bA = this.bD = this.locY;
      }

      this.bC += var1 * 0.25D;
      this.bE += var5 * 0.25D;
      this.bD += var3 * 0.25D;
   }

   protected void cD() {
      float var1 = this.width;
      float var2 = this.length;
      if(this.cA()) {
         var1 = 0.6F;
         var2 = 0.6F;
      } else if(this.ck()) {
         var1 = 0.2F;
         var2 = 0.2F;
      } else if(this.aJ()) {
         var1 = 0.6F;
         var2 = 1.65F;
      } else {
         var1 = 0.6F;
         var2 = 1.8F;
      }

      if(var1 != this.width || var2 != this.length) {
         AxisAlignedBB var3 = this.bk();
         var3 = new AxisAlignedBB(var3.a, var3.b, var3.c, var3.a + (double)var1, var3.b + (double)var2, var3.c + (double)var1);
         if(!this.world.b(var3)) {
            this.a(var1, var2);
         }
      }

   }

   public int U() {
      return this.abilities.a?1:80;
   }

   protected class_nf Z() {
      return class_ng.ef;
   }

   protected class_nf aa() {
      return class_ng.ee;
   }

   public int aB() {
      return 10;
   }

   public void a(class_nf var1, float var2, float var3) {
      this.world.a(this, this.locX, this.locY, this.locZ, var1, this.by(), var2, var3);
   }

   public EnumSoundCategory by() {
      return EnumSoundCategory.PLAYERS;
   }

   protected boolean ce() {
      return this.bP() <= 0.0F || this.ck();
   }

   protected void q() {
      this.bt = this.bs;
   }

   public void av() {
      if(!this.world.E && this.aJ() && this.aH()) {
         this.p();
         this.d(false);
      } else {
         double var1 = this.locX;
         double var3 = this.locY;
         double var5 = this.locZ;
         float var7 = this.yaw;
         float var8 = this.pitch;
         super.av();
         this.bw = this.bx;
         this.bx = 0.0F;
         this.m(this.locX - var1, this.locY - var3, this.locZ - var5);
         if(this.bx() instanceof class_wb) {
            this.pitch = var8;
            this.yaw = var7;
            this.aM = ((class_wb)this.bx()).aM;
         }

      }
   }

   protected void cl() {
      super.cl();
      this.bX();
      this.aO = this.yaw;
   }

   public void n() {
      if(this.bv > 0) {
         --this.bv;
      }

      if(this.world.ae() == EnumDifficulty.PEACEFUL && this.world.U().b("naturalRegeneration")) {
         if(this.bP() < this.bV() && this.ticksLived % 20 == 0) {
            this.b(1.0F);
         }

         if(this.bu.c() && this.ticksLived % 10 == 0) {
            this.bu.a(this.bu.a() + 1);
         }
      }

      this.br.m();
      this.bw = this.bx;
      super.n();
      class_sl var1 = this.a((class_sk)class_ys.d);
      if(!this.world.E) {
         var1.a((double)this.abilities.b());
      }

      this.aQ = this.bO;
      if(this.aK()) {
         this.aQ = (float)((double)this.aQ + (double)this.bO * 0.3D);
      }

      this.l((float)var1.e());
      float var2 = MathHelper.a(this.motX * this.motX + this.motZ * this.motZ);
      float var3 = (float)(Math.atan(-this.motY * 0.20000000298023224D) * 15.0D);
      if(var2 > 0.1F) {
         var2 = 0.1F;
      }

      if(!this.onGround || this.bP() <= 0.0F) {
         var2 = 0.0F;
      }

      if(this.onGround || this.bP() <= 0.0F) {
         var3 = 0.0F;
      }

      this.bx += (var2 - this.bx) * 0.4F;
      this.aJ += (var3 - this.aJ) * 0.8F;
      if(this.bP() > 0.0F && !this.y()) {
         AxisAlignedBB var4 = null;
         if(this.aH() && !this.bx().dead) {
            var4 = this.bk().a(this.bx().bk()).b(1.0D, 0.0D, 1.0D);
         } else {
            var4 = this.bk().b(1.0D, 0.5D, 1.0D);
         }

         List var5 = this.world.b((Entity)this, (AxisAlignedBB)var4);

         for(int var6 = 0; var6 < var5.size(); ++var6) {
            Entity var7 = (Entity)var5.get(var6);
            if(!var7.dead) {
               this.c(var7);
            }
         }
      }

   }

   private void c(Entity var1) {
      var1.d(this);
   }

   public int cE() {
      return ((Integer)this.datawatcher.a(b)).intValue();
   }

   public void l(int var1) {
      this.datawatcher.b(b, Integer.valueOf(var1));
   }

   public void m(int var1) {
      int var2 = this.cE();
      this.datawatcher.b(b, Integer.valueOf(var2 + var1));
   }

   public void a(DamageSource var1) {
      super.a(var1);
      this.a(0.2F, 0.2F);
      this.b(this.locX, this.locY, this.locZ);
      this.motY = 0.10000000149011612D;
      if(this.h_().equals("Notch")) {
         this.a(new ItemStack(Items.e, 1), true, false);
      }

      if(!this.world.U().b("keepInventory") && !this.y()) {
         this.br.n();
      }

      if(var1 != null) {
         this.motX = (double)(-MathHelper.b((this.az + this.yaw) * 0.017453292F) * 0.1F);
         this.motZ = (double)(-MathHelper.a((this.az + this.yaw) * 0.017453292F) * 0.1F);
      } else {
         this.motX = this.motZ = 0.0D;
      }

      this.b(StatisticList.A);
      this.a(StatisticList.h);
   }

   protected class_nf bQ() {
      return class_ng.eb;
   }

   protected class_nf bR() {
      return class_ng.ea;
   }

   public void b(Entity var1, int var2) {
      if(var1 != this) {
         this.m(var2);
         Collection var3 = this.cV().a(IScoreboardCriteria.f);
         if(var1 instanceof EntityHuman) {
            this.b(StatisticList.D);
            var3.addAll(this.cV().a(IScoreboardCriteria.e));
         } else {
            this.b(StatisticList.B);
         }

         var3.addAll(this.d(var1));
         Iterator var4 = var3.iterator();

         while(var4.hasNext()) {
            class_bbk var5 = (class_bbk)var4.next();
            ScoreboardScore var6 = this.cV().c(this.h_(), var5);
            var6.a();
         }

      }
   }

   private Collection d(Entity var1) {
      String var2 = var1 instanceof EntityHuman?var1.h_():var1.getUniqueId().toString();
      ScoreboardTeam var3 = this.cV().g(this.h_());
      if(var3 != null) {
         int var4 = var3.m().b();
         if(var4 >= 0 && var4 < IScoreboardCriteria.n.length) {
            Iterator var5 = this.cV().a(IScoreboardCriteria.n[var4]).iterator();

            while(var5.hasNext()) {
               class_bbk var6 = (class_bbk)var5.next();
               ScoreboardScore var7 = this.cV().c(var2, var6);
               var7.a();
            }
         }
      }

      ScoreboardTeam var8 = this.cV().g(var2);
      if(var8 != null) {
         int var9 = var8.m().b();
         if(var9 >= 0 && var9 < IScoreboardCriteria.m.length) {
            return this.cV().a(IScoreboardCriteria.m[var9]);
         }
      }

      return Lists.newArrayList();
   }

   public class_yc a(boolean var1) {
      return this.a(this.br.a(this.br.d, var1 && this.br.h() != null?this.br.h().b:1), false, true);
   }

   public class_yc a(ItemStack var1, boolean var2) {
      return this.a(var1, false, false);
   }

   public class_yc a(ItemStack var1, boolean var2, boolean var3) {
      if(var1 == null) {
         return null;
      } else if(var1.b == 0) {
         return null;
      } else {
         double var4 = this.locY - 0.30000001192092896D + (double)this.bm();
         class_yc var6 = new class_yc(this.world, this.locX, var4, this.locZ, var1);
         var6.a(40);
         if(var3) {
            var6.e(this.h_());
         }

         float var7;
         float var8;
         if(var2) {
            var7 = this.random.nextFloat() * 0.5F;
            var8 = this.random.nextFloat() * 6.2831855F;
            var6.motX = (double)(-MathHelper.a(var8) * var7);
            var6.motZ = (double)(MathHelper.b(var8) * var7);
            var6.motY = 0.20000000298023224D;
         } else {
            var7 = 0.3F;
            var6.motX = (double)(-MathHelper.a(this.yaw * 0.017453292F) * MathHelper.b(this.pitch * 0.017453292F) * var7);
            var6.motZ = (double)(MathHelper.b(this.yaw * 0.017453292F) * MathHelper.b(this.pitch * 0.017453292F) * var7);
            var6.motY = (double)(-MathHelper.a(this.pitch * 0.017453292F) * var7 + 0.1F);
            var8 = this.random.nextFloat() * 6.2831855F;
            var7 = 0.02F * this.random.nextFloat();
            var6.motX += Math.cos((double)var8) * (double)var7;
            var6.motY += (double)((this.random.nextFloat() - this.random.nextFloat()) * 0.1F);
            var6.motZ += Math.sin((double)var8) * (double)var7;
         }

         ItemStack var9 = this.a(var6);
         if(var3) {
            if(var9 != null) {
               this.a(StatisticList.e(var9.b()), var1.b);
            }

            this.b(StatisticList.x);
         }

         return var6;
      }
   }

   protected ItemStack a(class_yc var1) {
      this.world.a((Entity)var1);
      ItemStack var2 = var1.k();
      return var2;
   }

   public float a(IBlockData var1) {
      float var2 = this.br.a(var1);
      if(var2 > 1.0F) {
         int var3 = class_agn.e(this);
         ItemStack var4 = this.ca();
         if(var3 > 0 && var4 != null) {
            var2 += (float)(var3 * var3 + 1);
         }
      }

      if(this.a((MobEffectType)MobEffectList.c)) {
         var2 *= 1.0F + (float)(this.b((MobEffectType)MobEffectList.c).c() + 1) * 0.2F;
      }

      if(this.a((MobEffectType)MobEffectList.d)) {
         float var5 = 1.0F;
         switch(this.b((MobEffectType)MobEffectList.d).c()) {
         case 0:
            var5 = 0.3F;
            break;
         case 1:
            var5 = 0.09F;
            break;
         case 2:
            var5 = 0.0027F;
            break;
         case 3:
         default:
            var5 = 8.1E-4F;
         }

         var2 *= var5;
      }

      if(this.a((Material)Material.h) && !class_agn.i(this)) {
         var2 /= 5.0F;
      }

      if(!this.onGround) {
         var2 /= 5.0F;
      }

      return var2;
   }

   public boolean b(IBlockData var1) {
      return this.br.b(var1);
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.uniqueID = a(this.bR);
      NBTTagList var2 = var1.c("Inventory", 10);
      this.br.b(var2);
      this.br.d = var1.h("SelectedItemSlot");
      this.bF = var1.p("Sleeping");
      this.d = var1.g("SleepTimer");
      this.bM = var1.j("XpP");
      this.bK = var1.h("XpLevel");
      this.bL = var1.h("XpTotal");
      this.h = var1.h("XpSeed");
      if(this.h == 0) {
         this.h = this.random.nextInt();
      }

      this.l(var1.h("Score"));
      if(this.bF) {
         this.bG = new BlockPosition(this);
         this.a(true, true, false);
      }

      if(var1.b("SpawnX", 99) && var1.b("SpawnY", 99) && var1.b("SpawnZ", 99)) {
         this.e = new BlockPosition(var1.h("SpawnX"), var1.h("SpawnY"), var1.h("SpawnZ"));
         this.f = var1.p("SpawnForced");
      }

      this.bu.a(var1);
      this.abilities.b(var1);
      if(var1.b("EnderItems", 9)) {
         NBTTagList var3 = var1.c("EnderItems", 10);
         this.c.a(var3);
      }

   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("DataVersion", (int)165);
      var1.a((String)"Inventory", (NBTTag)this.br.a(new NBTTagList()));
      var1.a("SelectedItemSlot", this.br.d);
      var1.a("Sleeping", this.bF);
      var1.a("SleepTimer", (short)this.d);
      var1.a("XpP", this.bM);
      var1.a("XpLevel", this.bK);
      var1.a("XpTotal", this.bL);
      var1.a("XpSeed", this.h);
      var1.a("Score", this.cE());
      if(this.e != null) {
         var1.a("SpawnX", this.e.p());
         var1.a("SpawnY", this.e.q());
         var1.a("SpawnZ", this.e.r());
         var1.a("SpawnForced", this.f);
      }

      this.bu.b(var1);
      this.abilities.a(var1);
      var1.a((String)"EnderItems", (NBTTag)this.c.h());
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b((DamageSource)var1)) {
         return false;
      } else if(this.abilities.a && !var1.g()) {
         return false;
      } else {
         this.aU = 0;
         if(this.bP() <= 0.0F) {
            return false;
         } else {
            if(this.ck() && !this.world.E) {
               this.a(true, true, false);
            }

            if(var1.r()) {
               if(this.world.ae() == EnumDifficulty.PEACEFUL) {
                  var2 = 0.0F;
               }

               if(this.world.ae() == EnumDifficulty.EASY) {
                  var2 = var2 / 2.0F + 1.0F;
               }

               if(this.world.ae() == EnumDifficulty.HARD) {
                  var2 = var2 * 3.0F / 2.0F;
               }
            }

            if(var2 == 0.0F) {
               return false;
            } else {
               Entity var3 = var1.j();
               if(var3 instanceof class_zl && ((class_zl)var3).e != null) {
                  var3 = ((class_zl)var3).e;
               }

               return super.a(var1, var2);
            }
         }
      }
   }

   public boolean a(EntityHuman var1) {
      ScoreboardTeamBase var2 = this.aN();
      ScoreboardTeamBase var3 = var1.aN();
      return var2 == null?true:(!var2.a(var3)?true:var2.g());
   }

   protected void j(float var1) {
      this.br.a(var1);
   }

   protected void k(float var1) {
      if(var1 > 0.0F && this.bm != null && this.bm.b() == Items.cQ) {
         int var2 = 1 + MathHelper.d(var1);
         this.bm.a(var2, (class_rz)this);
         if(this.bm.b <= 0) {
            EnumHand var3 = this.cs();
            if(var3 == EnumHand.MAIN_HAND) {
               this.a((EnumInventorySlot)EnumInventorySlot.MAINHAND, (ItemStack)null);
            } else {
               this.a((EnumInventorySlot)EnumInventorySlot.OFFHAND, (ItemStack)null);
            }

            this.bm = null;
            this.a(class_ng.eM, 0.8F, 0.8F + this.world.r.nextFloat() * 0.4F);
         }
      }

   }

   public float cF() {
      int var1 = 0;
      ItemStack[] var2 = this.br.b;
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         ItemStack var5 = var2[var4];
         if(var5 != null) {
            ++var1;
         }
      }

      return (float)var1 / (float)this.br.b.length;
   }

   protected void d(DamageSource var1, float var2) {
      if(!this.b((DamageSource)var1)) {
         var2 = this.b(var1, var2);
         var2 = this.c(var1, var2);
         float var3 = var2;
         var2 = Math.max(var2 - this.co(), 0.0F);
         this.n(this.co() - (var3 - var2));
         if(var2 != 0.0F) {
            this.a(var1.f());
            float var4 = this.bP();
            this.c(this.bP() - var2);
            this.bT().a(var1, var4, var2);
            if(var2 < 3.4028235E37F) {
               this.a(StatisticList.z, Math.round(var2 * 10.0F));
            }

         }
      }
   }

   public void a(class_aqm var1) {
   }

   public void a(class_ahi var1) {
   }

   public void a(class_apx var1) {
   }

   public void a(class_ahe var1) {
   }

   public void openContainer(IInventory var1) {
   }

   public void a(class_wj var1, IInventory var2) {
   }

   public void a(class_qm var1) {
   }

   public void a(ItemStack var1, EnumHand var2) {
   }

   public EnumResult a(Entity var1, ItemStack var2, EnumHand var3) {
      if(this.y()) {
         if(var1 instanceof IInventory) {
            this.openContainer((IInventory)var1);
         }

         return EnumResult.PASS;
      } else {
         ItemStack var4 = var2 != null?var2.k():null;
         if(!var1.a(this, var2, var3)) {
            if(var2 != null && var1 instanceof class_rz) {
               if(this.abilities.d) {
                  var2 = var4;
               }

               if(var2.a(this, (class_rz)var1, var3)) {
                  if(var2.b <= 0 && !this.abilities.d) {
                     this.a((EnumHand)var3, (ItemStack)null);
                  }

                  return EnumResult.SUCCESS;
               }
            }

            return EnumResult.PASS;
         } else {
            if(var2 != null && var2 == this.b((EnumHand)var3)) {
               if(var2.b <= 0 && !this.abilities.d) {
                  this.a((EnumHand)var3, (ItemStack)null);
               } else if(var2.b < var4.b && this.abilities.d) {
                  var2.b = var4.b;
               }
            }

            return EnumResult.SUCCESS;
         }
      }
   }

   public double aw() {
      return -0.35D;
   }

   public void p() {
      super.p();
      this.j = 0;
   }

   public void f(Entity var1) {
      if(var1.aS()) {
         if(!var1.t(this)) {
            float var2 = (float)this.a((class_sk)class_ys.e).e();
            float var3 = 0.0F;
            if(var1 instanceof class_rz) {
               var3 = class_agn.a(this.ca(), ((class_rz)var1).bZ());
            } else {
               var3 = class_agn.a(this.ca(), EnumMonsterType.UNDEFINED);
            }

            float var4 = this.o(0.5F);
            var2 *= 0.2F + var4 * var4 * 0.8F;
            var3 *= var4;
            this.cY();
            if(var2 > 0.0F || var3 > 0.0F) {
               boolean var5 = var4 > 0.9F;
               boolean var6 = false;
               boolean var7 = false;
               boolean var8 = false;
               byte var9 = 0;
               int var26 = var9 + class_agn.a((class_rz)this);
               if(this.aK() && var5) {
                  this.world.a((EntityHuman)null, this.locX, this.locY, this.locZ, class_ng.dT, this.by(), 1.0F, 1.0F);
                  ++var26;
                  var6 = true;
               }

               var7 = var5 && this.fallDistance > 0.0F && !this.onGround && !this.n_() && !this.ah() && !this.a((MobEffectType)MobEffectList.o) && !this.aH() && var1 instanceof class_rz;
               var7 = var7 && !this.aK();
               if(var7) {
                  var2 *= 1.5F;
               }

               var2 += var3;
               double var10 = (double)(this.J - this.I);
               if(var5 && !var7 && !var6 && this.onGround && var10 < (double)this.cj()) {
                  ItemStack var12 = this.b((EnumHand)EnumHand.MAIN_HAND);
                  if(var12 != null && var12.b() instanceof class_aew) {
                     var8 = true;
                  }
               }

               float var27 = 0.0F;
               boolean var13 = false;
               int var14 = class_agn.b(this);
               if(var1 instanceof class_rz) {
                  var27 = ((class_rz)var1).bP();
                  if(var14 > 0 && !var1.aG()) {
                     var13 = true;
                     var1.g(1);
                  }
               }

               double var15 = var1.motX;
               double var17 = var1.motY;
               double var19 = var1.motZ;
               boolean var21 = var1.a(DamageSource.a(this), var2);
               if(var21) {
                  if(var26 > 0) {
                     if(var1 instanceof class_rz) {
                        ((class_rz)var1).a(this, (float)var26 * 0.5F, (double)MathHelper.a(this.yaw * 0.017453292F), (double)(-MathHelper.b(this.yaw * 0.017453292F)));
                     } else {
                        var1.g((double)(-MathHelper.a(this.yaw * 0.017453292F) * (float)var26 * 0.5F), 0.1D, (double)(MathHelper.b(this.yaw * 0.017453292F) * (float)var26 * 0.5F));
                     }

                     this.motX *= 0.6D;
                     this.motZ *= 0.6D;
                     this.e(false);
                  }

                  if(var8) {
                     List var22 = this.world.a(class_rz.class, var1.bk().b(1.0D, 0.25D, 1.0D));
                     Iterator var23 = var22.iterator();

                     while(var23.hasNext()) {
                        class_rz var24 = (class_rz)var23.next();
                        if(var24 != this && var24 != var1 && !this.r(var24) && this.h(var24) < 9.0D) {
                           var24.a(this, 0.4F, (double)MathHelper.a(this.yaw * 0.017453292F), (double)(-MathHelper.b(this.yaw * 0.017453292F)));
                           var24.a(DamageSource.a(this), 1.0F);
                        }
                     }

                     this.world.a((EntityHuman)null, this.locX, this.locY, this.locZ, class_ng.dW, this.by(), 1.0F, 1.0F);
                     this.cG();
                  }

                  if(var1 instanceof EntityPlayer && var1.velocityChanged) {
                     ((EntityPlayer)var1).a.a((Packet)(new PacketPlayOutEntityVelocity(var1)));
                     var1.velocityChanged = false;
                     var1.motX = var15;
                     var1.motY = var17;
                     var1.motZ = var19;
                  }

                  if(var7) {
                     this.world.a((EntityHuman)null, this.locX, this.locY, this.locZ, class_ng.dS, this.by(), 1.0F, 1.0F);
                     this.a(var1);
                  }

                  if(!var7 && !var8) {
                     if(var5) {
                        this.world.a((EntityHuman)null, this.locX, this.locY, this.locZ, class_ng.dV, this.by(), 1.0F, 1.0F);
                     } else {
                        this.world.a((EntityHuman)null, this.locX, this.locY, this.locZ, class_ng.dX, this.by(), 1.0F, 1.0F);
                     }
                  }

                  if(var3 > 0.0F) {
                     this.b(var1);
                  }

                  if(!this.world.E && var1 instanceof EntityHuman) {
                     EntityHuman var28 = (EntityHuman)var1;
                     ItemStack var30 = this.ca();
                     ItemStack var32 = var28.cr()?var28.cu():null;
                     if(var30 != null && var32 != null && var30.b() instanceof class_aby && var32.b() == Items.cQ) {
                        float var25 = 0.25F + (float)class_agn.e(this) * 0.05F;
                        if(var6) {
                           var25 += 0.75F;
                        }

                        if(this.random.nextFloat() < var25) {
                           var28.cZ().a(Items.cQ, 100);
                           this.world.a((Entity)var28, (byte)30);
                        }
                     }
                  }

                  if(var2 >= 18.0F) {
                     this.b((Statistic)AchievementList.F);
                  }

                  this.z(var1);
                  if(var1 instanceof class_rz) {
                     class_agn.a((class_rz)((class_rz)var1), (Entity)this);
                  }

                  class_agn.b((class_rz)this, (Entity)var1);
                  ItemStack var29 = this.ca();
                  Object var31 = var1;
                  if(var1 instanceof class_wr) {
                     class_wq var33 = ((class_wr)var1).a;
                     if(var33 instanceof class_rz) {
                        var31 = (class_rz)var33;
                     }
                  }

                  if(var29 != null && var31 instanceof class_rz) {
                     var29.a((class_rz)var31, this);
                     if(var29.b <= 0) {
                        this.a((EnumHand)EnumHand.MAIN_HAND, (ItemStack)null);
                     }
                  }

                  if(var1 instanceof class_rz) {
                     float var34 = var27 - ((class_rz)var1).bP();
                     this.a(StatisticList.y, Math.round(var34 * 10.0F));
                     if(var14 > 0) {
                        var1.g(var14 * 4);
                     }

                     if(this.world instanceof WorldServer && var34 > 2.0F) {
                        int var35 = (int)((double)var34 * 0.5D);
                        ((WorldServer)this.world).a(EnumParticle.DAMAGE_INDICATOR, var1.locX, var1.locY + (double)(var1.length * 0.5F), var1.locZ, var35, 0.1D, 0.0D, 0.1D, 0.2D, new int[0]);
                     }
                  }

                  this.a(0.3F);
               } else {
                  this.world.a((EntityHuman)null, this.locX, this.locY, this.locZ, class_ng.dU, this.by(), 1.0F, 1.0F);
                  if(var13) {
                     var1.W();
                  }
               }
            }

         }
      }
   }

   public void a(Entity var1) {
   }

   public void b(Entity var1) {
   }

   public void cG() {
      double var1 = (double)(-MathHelper.a(this.yaw * 0.017453292F));
      double var3 = (double)MathHelper.b(this.yaw * 0.017453292F);
      if(this.world instanceof WorldServer) {
         ((WorldServer)this.world).a(EnumParticle.SWEEP_ATTACK, this.locX + var1, this.locY + (double)this.length * 0.5D, this.locZ + var3, 0, var1, 0.0D, var3, 0.0D, new int[0]);
      }

   }

   public void S() {
      super.S();
      this.bs.b(this);
      if(this.bt != null) {
         this.bt.b(this);
      }

   }

   public boolean au() {
      return !this.bF && super.au();
   }

   public boolean cI() {
      return false;
   }

   public GameProfile getProfile() {
      return this.bR;
   }

   public EntityHuman.EnumBedResult a(BlockPosition var1) {
      if(!this.world.E) {
         if(this.ck() || !this.at()) {
            return EntityHuman.EnumBedResult.OTHER_PROBLEM;
         }

         if(!this.world.s.d()) {
            return EntityHuman.EnumBedResult.NOT_POSSIBLE_HERE;
         }

         if(this.world.B()) {
            return EntityHuman.EnumBedResult.NOT_POSSIBLE_NOW;
         }

         if(Math.abs(this.locX - (double)var1.p()) > 3.0D || Math.abs(this.locY - (double)var1.q()) > 2.0D || Math.abs(this.locZ - (double)var1.r()) > 3.0D) {
            return EntityHuman.EnumBedResult.TOO_FAR_AWAY;
         }

         double var2 = 8.0D;
         double var4 = 5.0D;
         List var6 = this.world.a(class_yp.class, new AxisAlignedBB((double)var1.p() - var2, (double)var1.q() - var4, (double)var1.r() - var2, (double)var1.p() + var2, (double)var1.q() + var4, (double)var1.r() + var2));
         if(!var6.isEmpty()) {
            return EntityHuman.EnumBedResult.NOT_SAFE;
         }
      }

      if(this.aH()) {
         this.p();
      }

      this.a(0.2F, 0.2F);
      if(this.world.e(var1)) {
         EnumDirection var7 = (EnumDirection)this.world.getType(var1).get(class_amf.D);
         float var3 = 0.5F;
         float var8 = 0.5F;
         switch(EntityHuman.SyntheticClass_1.a[var7.ordinal()]) {
         case 1:
            var8 = 0.9F;
            break;
         case 2:
            var8 = 0.1F;
            break;
         case 3:
            var3 = 0.1F;
            break;
         case 4:
            var3 = 0.9F;
         }

         this.a(var7);
         this.b((double)((float)var1.p() + var3), (double)((float)var1.q() + 0.6875F), (double)((float)var1.r() + var8));
      } else {
         this.b((double)((float)var1.p() + 0.5F), (double)((float)var1.q() + 0.6875F), (double)((float)var1.r() + 0.5F));
      }

      this.bF = true;
      this.d = 0;
      this.bG = var1;
      this.motX = this.motZ = this.motY = 0.0D;
      if(!this.world.E) {
         this.world.e();
      }

      return EntityHuman.EnumBedResult.OK;
   }

   private void a(EnumDirection var1) {
      this.bH = 0.0F;
      this.bI = 0.0F;
      switch(EntityHuman.SyntheticClass_1.a[var1.ordinal()]) {
      case 1:
         this.bI = -1.8F;
         break;
      case 2:
         this.bI = 1.8F;
         break;
      case 3:
         this.bH = 1.8F;
         break;
      case 4:
         this.bH = -1.8F;
      }

   }

   public void a(boolean var1, boolean var2, boolean var3) {
      this.a(0.6F, 1.8F);
      IBlockData var4 = this.world.getType(this.bG);
      if(this.bG != null && var4.getBlock() == Blocks.C) {
         this.world.a((BlockPosition)this.bG, (IBlockData)var4.set(BlockBed.b, Boolean.valueOf(false)), 4);
         BlockPosition var5 = BlockBed.a((World)this.world, (BlockPosition)this.bG, 0);
         if(var5 == null) {
            var5 = this.bG.a();
         }

         this.b((double)((float)var5.p() + 0.5F), (double)((float)var5.q() + 0.1F), (double)((float)var5.r() + 0.5F));
      }

      this.bF = false;
      if(!this.world.E && var2) {
         this.world.e();
      }

      this.d = var1?0:100;
      if(var3) {
         this.a(this.bG, false);
      }

   }

   private boolean r() {
      return this.world.getType(this.bG).getBlock() == Blocks.C;
   }

   public static BlockPosition a(World var0, BlockPosition var1, boolean var2) {
      Block var3 = var0.getType(var1).getBlock();
      if(var3 != Blocks.C) {
         if(!var2) {
            return null;
         } else {
            boolean var4 = var3.d();
            boolean var5 = var0.getType(var1.a()).getBlock().d();
            return var4 && var5?var1:null;
         }
      } else {
         return BlockBed.a((World)var0, (BlockPosition)var1, 0);
      }
   }

   public boolean ck() {
      return this.bF;
   }

   public boolean cL() {
      return this.bF && this.d >= 100;
   }

   public void b(IChatBaseComponent var1) {
   }

   public BlockPosition cN() {
      return this.e;
   }

   public boolean cO() {
      return this.f;
   }

   public void a(BlockPosition var1, boolean var2) {
      if(var1 != null) {
         this.e = var1;
         this.f = var2;
      } else {
         this.e = null;
         this.f = false;
      }

   }

   public void b(Statistic var1) {
      this.a(var1, 1);
   }

   public void a(Statistic var1, int var2) {
   }

   public void a(Statistic var1) {
   }

   public void cg() {
      super.cg();
      this.b(StatisticList.w);
      if(this.aK()) {
         this.a(0.8F);
      } else {
         this.a(0.2F);
      }

   }

   public void g(float var1, float var2) {
      double var3 = this.locX;
      double var5 = this.locY;
      double var7 = this.locZ;
      if(this.abilities.b && !this.aH()) {
         double var9 = this.motY;
         float var11 = this.aQ;
         this.aQ = this.abilities.a() * (float)(this.aK()?2:1);
         super.g(var1, var2);
         this.motY = var9 * 0.6D;
         this.aQ = var11;
         this.fallDistance = 0.0F;
         this.b(7, false);
      } else {
         super.g(var1, var2);
      }

      this.l(this.locX - var3, this.locY - var5, this.locZ - var7);
   }

   public float cj() {
      return (float)this.a((class_sk)class_ys.d).e();
   }

   public void l(double var1, double var3, double var5) {
      if(!this.aH()) {
         int var7;
         if(this.a((Material)Material.h)) {
            var7 = Math.round(MathHelper.a(var1 * var1 + var3 * var3 + var5 * var5) * 100.0F);
            if(var7 > 0) {
               this.a(StatisticList.q, var7);
               this.a(0.015F * (float)var7 * 0.01F);
            }
         } else if(this.ah()) {
            var7 = Math.round(MathHelper.a(var1 * var1 + var5 * var5) * 100.0F);
            if(var7 > 0) {
               this.a(StatisticList.m, var7);
               this.a(0.015F * (float)var7 * 0.01F);
            }
         } else if(this.n_()) {
            if(var3 > 0.0D) {
               this.a(StatisticList.o, (int)Math.round(var3 * 100.0D));
            }
         } else if(this.onGround) {
            var7 = Math.round(MathHelper.a(var1 * var1 + var5 * var5) * 100.0F);
            if(var7 > 0) {
               if(this.aK()) {
                  this.a(StatisticList.l, var7);
                  this.a(0.099999994F * (float)var7 * 0.01F);
               } else if(this.aJ()) {
                  this.a(StatisticList.k, var7);
                  this.a(0.005F * (float)var7 * 0.01F);
               } else {
                  this.a(StatisticList.j, var7);
                  this.a(0.01F * (float)var7 * 0.01F);
               }
            }
         } else if(this.cA()) {
            var7 = Math.round(MathHelper.a(var1 * var1 + var3 * var3 + var5 * var5) * 100.0F);
            this.a(StatisticList.v, var7);
         } else {
            var7 = Math.round(MathHelper.a(var1 * var1 + var5 * var5) * 100.0F);
            if(var7 > 25) {
               this.a(StatisticList.p, var7);
            }
         }

      }
   }

   private void m(double var1, double var3, double var5) {
      if(this.aH()) {
         int var7 = Math.round(MathHelper.a(var1 * var1 + var3 * var3 + var5 * var5) * 100.0F);
         if(var7 > 0) {
            if(this.bx() instanceof EntityMinecartAbstract) {
               this.a(StatisticList.r, var7);
               if(this.g == null) {
                  this.g = new BlockPosition(this);
               } else if(this.g.e((double)MathHelper.c(this.locX), (double)MathHelper.c(this.locY), (double)MathHelper.c(this.locZ)) >= 1000000.0D) {
                  this.b((Statistic)AchievementList.q);
               }
            } else if(this.bx() instanceof EntityBoat) {
               this.a(StatisticList.s, var7);
            } else if(this.bx() instanceof class_wb) {
               this.a(StatisticList.t, var7);
            } else if(this.bx() instanceof class_wj) {
               this.a(StatisticList.u, var7);
            }
         }
      }

   }

   public void e(float var1, float var2) {
      if(!this.abilities.c) {
         if(var1 >= 2.0F) {
            this.a(StatisticList.n, (int)Math.round((double)var1 * 100.0D));
         }

         super.e(var1, var2);
      }
   }

   protected void aj() {
      if(!this.y()) {
         super.aj();
      }

   }

   protected class_nf e(int var1) {
      return var1 > 4?class_ng.dY:class_ng.ed;
   }

   public void b(class_rz var1) {
      if(var1 instanceof class_yk) {
         this.b((Statistic)AchievementList.s);
      }

      EntityTypes.class_a_in_class_rs var2 = (EntityTypes.class_a_in_class_rs)EntityTypes.a.get(EntityTypes.b((Entity)var1));
      if(var2 != null) {
         this.b(var2.d);
      }

   }

   public void aP() {
      if(!this.abilities.b) {
         super.aP();
      }

   }

   public void n(int var1) {
      this.m(var1);
      int var2 = Integer.MAX_VALUE - this.bL;
      if(var1 > var2) {
         var1 = var2;
      }

      this.bM += (float)var1 / (float)this.cQ();

      for(this.bL += var1; this.bM >= 1.0F; this.bM /= (float)this.cQ()) {
         this.bM = (this.bM - 1.0F) * (float)this.cQ();
         this.a(1);
      }

   }

   public int cP() {
      return this.h;
   }

   public void b(int var1) {
      this.bK -= var1;
      if(this.bK < 0) {
         this.bK = 0;
         this.bM = 0.0F;
         this.bL = 0;
      }

      this.h = this.random.nextInt();
   }

   public void a(int var1) {
      this.bK += var1;
      if(this.bK < 0) {
         this.bK = 0;
         this.bM = 0.0F;
         this.bL = 0;
      }

      if(var1 > 0 && this.bK % 5 == 0 && (float)this.bQ < (float)this.ticksLived - 100.0F) {
         float var2 = this.bK > 30?1.0F:(float)this.bK / 30.0F;
         this.world.a((EntityHuman)null, this.locX, this.locY, this.locZ, class_ng.ec, this.by(), var2 * 0.75F, 1.0F);
         this.bQ = this.ticksLived;
      }

   }

   public int cQ() {
      return this.bK >= 30?112 + (this.bK - 30) * 9:(this.bK >= 15?37 + (this.bK - 15) * 5:7 + this.bK * 2);
   }

   public void a(float var1) {
      if(!this.abilities.a) {
         if(!this.world.E) {
            this.bu.a(var1);
         }

      }
   }

   public class_aar cR() {
      return this.bu;
   }

   public boolean l(boolean var1) {
      return (var1 || this.bu.c()) && !this.abilities.a;
   }

   public boolean cS() {
      return this.bP() > 0.0F && this.bP() < this.bV();
   }

   public boolean cT() {
      return this.abilities.e;
   }

   public boolean a(BlockPosition var1, EnumDirection var2, ItemStack var3) {
      if(this.abilities.e) {
         return true;
      } else if(var3 == null) {
         return false;
      } else {
         BlockPosition var4 = var1.a(var2.d());
         Block var5 = this.world.getType(var4).getBlock();
         return var3.b(var5) || var3.x();
      }
   }

   protected int b(EntityHuman var1) {
      if(!this.world.U().b("keepInventory") && !this.y()) {
         int var2 = this.bK * 7;
         return var2 > 100?100:var2;
      } else {
         return 0;
      }
   }

   protected boolean bD() {
      return true;
   }

   public void a(EntityHuman var1, boolean var2) {
      if(var2) {
         this.br.a(var1.br);
         this.c(var1.bP());
         this.bu = var1.bu;
         this.bK = var1.bK;
         this.bL = var1.bL;
         this.bM = var1.bM;
         this.l(var1.cE());
         this.an = var1.an;
         this.ao = var1.ao;
         this.ap = var1.ap;
      } else if(this.world.U().b("keepInventory") || var1.y()) {
         this.br.a(var1.br);
         this.bK = var1.bK;
         this.bL = var1.bL;
         this.bM = var1.bM;
         this.l(var1.cE());
      }

      this.h = var1.h;
      this.c = var1.c;
      this.Q().b(bp, var1.Q().a(bp));
   }

   protected boolean ad() {
      return !this.abilities.b;
   }

   public void updateAbilities() {
   }

   public void a(WorldSettings.EnumGamemode var1) {
   }

   public String h_() {
      return this.bR.getName();
   }

   public InventoryEnderChest cU() {
      return this.c;
   }

   public ItemStack a(EnumInventorySlot var1) {
      return var1 == EnumInventorySlot.MAINHAND?this.br.h():(var1 == EnumInventorySlot.OFFHAND?this.br.c[0]:(var1.a() == EnumInventorySlot.EnumSlotType.ARMOR?this.br.b[var1.b()]:null));
   }

   public void a(EnumInventorySlot var1, ItemStack var2) {
      if(var1 == EnumInventorySlot.MAINHAND) {
         this.a_(var2);
         this.br.a[this.br.d] = var2;
      } else if(var1 == EnumInventorySlot.OFFHAND) {
         this.a_(var2);
         this.br.c[0] = var2;
      } else if(var1.a() == EnumInventorySlot.EnumSlotType.ARMOR) {
         this.a_(var2);
         this.br.b[var1.b()] = var2;
      }

   }

   public Iterable aD() {
      return Lists.newArrayList((Object[])(new ItemStack[]{this.ca(), this.cb()}));
   }

   public Iterable aE() {
      return Arrays.asList(this.br.b);
   }

   public abstract boolean y();

   public abstract boolean l_();

   public boolean bc() {
      return !this.abilities.b;
   }

   public Scoreboard cV() {
      return this.world.ad();
   }

   public ScoreboardTeamBase aN() {
      return this.cV().g(this.h_());
   }

   public IChatBaseComponent i_() {
      ChatComponentText var1 = new ChatComponentText(ScoreboardTeam.a(this.aN(), this.h_()));
      var1.b().a(new ChatClickable(ChatClickable.class_a_in_class_et.SUGGEST_COMMAND, "/msg " + this.h_() + " "));
      var1.b().a(this.bj());
      var1.b().a(this.h_());
      return var1;
   }

   public float bm() {
      float var1 = 1.62F;
      if(this.ck()) {
         var1 = 0.2F;
      } else if(!this.aJ() && this.length != 1.65F) {
         if(this.cA() || this.length == 0.6F) {
            var1 = 0.4F;
         }
      } else {
         var1 -= 0.08F;
      }

      return var1;
   }

   public void n(float var1) {
      if(var1 < 0.0F) {
         var1 = 0.0F;
      }

      this.Q().b(a, Float.valueOf(var1));
   }

   public float co() {
      return ((Float)this.Q().a(a)).floatValue();
   }

   public static UUID a(GameProfile var0) {
      UUID var1 = var0.getId();
      if(var1 == null) {
         var1 = d(var0.getName());
      }

      return var1;
   }

   public static UUID d(String var0) {
      return UUID.nameUUIDFromBytes(("OfflinePlayer:" + var0).getBytes(Charsets.UTF_8));
   }

   public boolean a(ChestLock var1) {
      if(var1.a()) {
         return true;
      } else {
         ItemStack var2 = this.ca();
         return var2 != null && var2.s()?var2.q().equals(var1.b()):false;
      }
   }

   public boolean z_() {
      return this.h().d[0].U().b("sendCommandFeedback");
   }

   public boolean c(int var1, ItemStack var2) {
      if(var1 >= 0 && var1 < this.br.a.length) {
         this.br.a(var1, var2);
         return true;
      } else {
         EnumInventorySlot var3;
         if(var1 == 100 + EnumInventorySlot.HEAD.b()) {
            var3 = EnumInventorySlot.HEAD;
         } else if(var1 == 100 + EnumInventorySlot.CHEST.b()) {
            var3 = EnumInventorySlot.CHEST;
         } else if(var1 == 100 + EnumInventorySlot.LEGS.b()) {
            var3 = EnumInventorySlot.LEGS;
         } else if(var1 == 100 + EnumInventorySlot.FEET.b()) {
            var3 = EnumInventorySlot.FEET;
         } else {
            var3 = null;
         }

         if(var1 == 98) {
            this.a(EnumInventorySlot.MAINHAND, var2);
            return true;
         } else if(var1 == 99) {
            this.a(EnumInventorySlot.OFFHAND, var2);
            return true;
         } else if(var3 == null) {
            int var4 = var1 - 200;
            if(var4 >= 0 && var4 < this.c.u_()) {
               this.c.a(var4, var2);
               return true;
            } else {
               return false;
            }
         } else {
            if(var2 != null && var2.b() != null) {
               if(!(var2.b() instanceof ItemArmor) && !(var2.b() instanceof class_acw)) {
                  if(var3 != EnumInventorySlot.HEAD) {
                     return false;
                  }
               } else if(EntityInsentient.d(var2) != var3) {
                  return false;
               }
            }

            this.br.a(var3.b() + this.br.a.length, var2);
            return true;
         }
      }
   }

   public EnumMainHandOption cq() {
      return ((Byte)this.datawatcher.a(bq)).byteValue() == 0?EnumMainHandOption.LEFT:EnumMainHandOption.RIGHT;
   }

   public void a(EnumMainHandOption var1) {
      this.datawatcher.b(bq, Byte.valueOf((byte)(var1 == EnumMainHandOption.LEFT?0:1)));
   }

   public float cX() {
      return (float)(1.0D / this.a((class_sk)class_ys.f).e() * 20.0D);
   }

   public float o(float var1) {
      return MathHelper.a(((float)this.aD + var1) / this.cX(), 0.0F, 1.0F);
   }

   public void cY() {
      this.aD = 0;
   }

   public class_ado cZ() {
      return this.bU;
   }

   public void i(Entity var1) {
      if(!this.ck()) {
         super.i(var1);
      }

   }

   public float da() {
      return (float)this.a((class_sk)class_ys.h).e();
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumDirection.values().length];

      static {
         try {
            a[EnumDirection.SOUTH.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.NORTH.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.EAST.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum EnumBedResult {
      OK,
      NOT_POSSIBLE_HERE,
      NOT_POSSIBLE_NOW,
      TOO_FAR_AWAY,
      OTHER_PROBLEM,
      NOT_SAFE;
   }

   public static enum EnumChatVisibility {
      FULL(0, "options.chat.visibility.full"),
      SYSTEM(1, "options.chat.visibility.system"),
      HIDDEN(2, "options.chat.visibility.hidden");

      private static final EntityHuman.EnumChatVisibility[] d = new EntityHuman.EnumChatVisibility[values().length];
      private final int e;
      private final String f;

      private EnumChatVisibility(int var3, String var4) {
         this.e = var3;
         this.f = var4;
      }

      static {
         EntityHuman.EnumChatVisibility[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            EntityHuman.EnumChatVisibility var3 = var0[var2];
            d[var3.e] = var3;
         }

      }
   }
}
