package net.minecraft.server;

import net.minecraft.server.DamageSource;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Vec3D;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_wr;
import net.minecraft.server.class_ws;
import net.minecraft.server.class_wt;
import net.minecraft.server.class_xd;

public abstract class class_wv implements class_xd {
   protected final class_wt a;

   public class_wv(class_wt var1) {
      this.a = var1;
   }

   public boolean a() {
      return false;
   }

   public void b() {
   }

   public void c() {
   }

   public void a(class_ws var1, BlockPosition var2, DamageSource var3, EntityHuman var4) {
   }

   public void d() {
   }

   public void e() {
   }

   public float f() {
      return 0.6F;
   }

   public Vec3D g() {
      return null;
   }

   public float a(class_wr var1, DamageSource var2, float var3) {
      return var3;
   }

   public float h() {
      float var1 = MathHelper.a(this.a.motX * this.a.motX + this.a.motZ * this.a.motZ) + 1.0F;
      float var2 = Math.min(var1, 40.0F);
      return 0.7F / var2 / var1;
   }
}
