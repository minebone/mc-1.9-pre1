package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectType;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cb;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.class_rz;

public class CommandEffect extends CommandAbstract {
   public String c() {
      return "effect";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.effect.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 2) {
         throw new class_cf("commands.effect.usage", new Object[0]);
      } else {
         class_rz var4 = (class_rz)a(var1, var2, var3[0], class_rz.class);
         if(var3[1].equals("clear")) {
            if(var4.bN().isEmpty()) {
               throw new class_bz("commands.effect.failure.notActive.all", new Object[]{var4.h_()});
            } else {
               var4.bM();
               a(var2, this, "commands.effect.success.removed.all", new Object[]{var4.h_()});
            }
         } else {
            MobEffectType var5;
            try {
               var5 = MobEffectType.a(a(var3[1], 1));
            } catch (class_cb var11) {
               var5 = MobEffectType.b(var3[1]);
            }

            if(var5 == null) {
               throw new class_cb("commands.effect.notFound", new Object[]{var3[1]});
            } else {
               int var6 = 600;
               int var7 = 30;
               int var8 = 0;
               if(var3.length >= 3) {
                  var7 = a(var3[2], 0, 1000000);
                  if(var5.b()) {
                     var6 = var7;
                  } else {
                     var6 = var7 * 20;
                  }
               } else if(var5.b()) {
                  var6 = 1;
               }

               if(var3.length >= 4) {
                  var8 = a(var3[3], 0, 255);
               }

               boolean var9 = true;
               if(var3.length >= 5 && "true".equalsIgnoreCase(var3[4])) {
                  var9 = false;
               }

               if(var7 > 0) {
                  MobEffect var10 = new MobEffect(var5, var6, var8, false, var9);
                  var4.c(var10);
                  a(var2, this, "commands.effect.success", new Object[]{new ChatMessage(var10.f(), new Object[0]), Integer.valueOf(MobEffectType.a(var5)), Integer.valueOf(var8), var4.h_(), Integer.valueOf(var7)});
               } else if(var4.a(var5)) {
                  var4.d(var5);
                  a(var2, this, "commands.effect.success.removed", new Object[]{new ChatMessage(var5.a(), new Object[0]), var4.h_()});
               } else {
                  throw new class_bz("commands.effect.failure.notActive", new Object[]{new ChatMessage(var5.a(), new Object[0]), var4.h_()});
               }
            }
         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, var1.J()):(var3.length == 2?a(var3, MobEffectType.b.c()):(var3.length == 5?a(var3, new String[]{"true", "false"}):Collections.emptyList()));
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 0;
   }
}
