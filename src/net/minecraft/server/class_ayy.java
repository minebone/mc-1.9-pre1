package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ayw;
import net.minecraft.server.class_ayx;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutMap;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_xr;

public class class_ayy extends class_ayw {
   public int b;
   public int c;
   public byte d;
   public boolean e;
   public byte f;
   public byte[] g = new byte[16384];
   public List h = Lists.newArrayList();
   private Map j = Maps.newHashMap();
   public Map i = Maps.newLinkedHashMap();

   public class_ayy(String var1) {
      super(var1);
   }

   public void a(double var1, double var3, int var5) {
      int var6 = 128 * (1 << var5);
      int var7 = MathHelper.c((var1 + 64.0D) / (double)var6);
      int var8 = MathHelper.c((var3 + 64.0D) / (double)var6);
      this.b = var7 * var6 + var6 / 2 - 64;
      this.c = var8 * var6 + var6 / 2 - 64;
   }

   public void a(NBTTagCompound var1) {
      this.d = var1.f("dimension");
      this.b = var1.h("xCenter");
      this.c = var1.h("zCenter");
      this.f = var1.f("scale");
      this.f = (byte)MathHelper.a(this.f, 0, 4);
      if(var1.b("trackingPosition", 1)) {
         this.e = var1.p("trackingPosition");
      } else {
         this.e = true;
      }

      short var2 = var1.g("width");
      short var3 = var1.g("height");
      if(var2 == 128 && var3 == 128) {
         this.g = var1.m("colors");
      } else {
         byte[] var4 = var1.m("colors");
         this.g = new byte[16384];
         int var5 = (128 - var2) / 2;
         int var6 = (128 - var3) / 2;

         for(int var7 = 0; var7 < var3; ++var7) {
            int var8 = var7 + var6;
            if(var8 >= 0 || var8 < 128) {
               for(int var9 = 0; var9 < var2; ++var9) {
                  int var10 = var9 + var5;
                  if(var10 >= 0 || var10 < 128) {
                     this.g[var10 + var8 * 128] = var4[var9 + var7 * var2];
                  }
               }
            }
         }
      }

   }

   public void b(NBTTagCompound var1) {
      var1.a("dimension", this.d);
      var1.a("xCenter", this.b);
      var1.a("zCenter", this.c);
      var1.a("scale", this.f);
      var1.a("width", (short)128);
      var1.a("height", (short)128);
      var1.a("colors", this.g);
      var1.a("trackingPosition", this.e);
   }

   public void a(EntityHuman var1, ItemStack var2) {
      if(!this.j.containsKey(var1)) {
         class_ayy.class_a_in_class_ayy var3 = new class_ayy.class_a_in_class_ayy(var1);
         this.j.put(var1, var3);
         this.h.add(var3);
      }

      if(!var1.br.f(var2)) {
         this.i.remove(var1.h_());
      }

      for(int var6 = 0; var6 < this.h.size(); ++var6) {
         class_ayy.class_a_in_class_ayy var4 = (class_ayy.class_a_in_class_ayy)this.h.get(var6);
         if(var4.a.dead || !var4.a.br.f(var2) && !var2.y()) {
            this.j.remove(var4.a);
            this.h.remove(var4);
         } else if(!var2.y() && var4.a.dimension == this.d && this.e) {
            this.a(0, var4.a.world, var4.a.h_(), var4.a.locX, var4.a.locZ, (double)var4.a.yaw);
         }
      }

      if(var2.y() && this.e) {
         class_xr var7 = var2.z();
         BlockPosition var8 = var7.q();
         this.a(1, var1.world, "frame-" + var7.getId(), (double)var8.p(), (double)var8.r(), (double)(var7.b.b() * 90));
      }

      if(var2.n() && var2.o().b("Decorations", 9)) {
         NBTTagList var9 = var2.o().c("Decorations", 10);

         for(int var10 = 0; var10 < var9.c(); ++var10) {
            NBTTagCompound var5 = var9.b(var10);
            if(!this.i.containsKey(var5.l("id"))) {
               this.a(var5.f("type"), var1.world, var5.l("id"), var5.k("x"), var5.k("z"), var5.k("rot"));
            }
         }
      }

   }

   private void a(int var1, World var2, String var3, double var4, double var6, double var8) {
      int var10 = 1 << this.f;
      float var11 = (float)(var4 - (double)this.b) / (float)var10;
      float var12 = (float)(var6 - (double)this.c) / (float)var10;
      byte var13 = (byte)((int)((double)(var11 * 2.0F) + 0.5D));
      byte var14 = (byte)((int)((double)(var12 * 2.0F) + 0.5D));
      byte var16 = 63;
      byte var15;
      if(var11 >= (float)(-var16) && var12 >= (float)(-var16) && var11 <= (float)var16 && var12 <= (float)var16) {
         var8 += var8 < 0.0D?-8.0D:8.0D;
         var15 = (byte)((int)(var8 * 16.0D / 360.0D));
         if(this.d < 0) {
            int var17 = (int)(var2.T().f() / 10L);
            var15 = (byte)(var17 * var17 * 34187121 + var17 * 121 >> 15 & 15);
         }
      } else {
         if(Math.abs(var11) >= 320.0F || Math.abs(var12) >= 320.0F) {
            this.i.remove(var3);
            return;
         }

         var1 = 6;
         var15 = 0;
         if(var11 <= (float)(-var16)) {
            var13 = (byte)((int)((double)(var16 * 2) + 2.5D));
         }

         if(var12 <= (float)(-var16)) {
            var14 = (byte)((int)((double)(var16 * 2) + 2.5D));
         }

         if(var11 >= (float)var16) {
            var13 = (byte)(var16 * 2 + 1);
         }

         if(var12 >= (float)var16) {
            var14 = (byte)(var16 * 2 + 1);
         }
      }

      this.i.put(var3, new class_ayx((byte)var1, var13, var14, var15));
   }

   public Packet a(ItemStack var1, World var2, EntityHuman var3) {
      class_ayy.class_a_in_class_ayy var4 = (class_ayy.class_a_in_class_ayy)this.j.get(var3);
      return var4 == null?null:var4.a(var1);
   }

   public void a(int var1, int var2) {
      super.c();
      Iterator var3 = this.h.iterator();

      while(var3.hasNext()) {
         class_ayy.class_a_in_class_ayy var4 = (class_ayy.class_a_in_class_ayy)var3.next();
         var4.a(var1, var2);
      }

   }

   public class_ayy.class_a_in_class_ayy a(EntityHuman var1) {
      class_ayy.class_a_in_class_ayy var2 = (class_ayy.class_a_in_class_ayy)this.j.get(var1);
      if(var2 == null) {
         var2 = new class_ayy.class_a_in_class_ayy(var1);
         this.j.put(var1, var2);
         this.h.add(var2);
      }

      return var2;
   }

   public class class_a_in_class_ayy {
      public final EntityHuman a;
      private boolean d = true;
      private int e = 0;
      private int f = 0;
      private int g = 127;
      private int h = 127;
      private int i;
      public int b;

      public class_a_in_class_ayy(EntityHuman var2) {
         this.a = var2;
      }

      public Packet a(ItemStack var1) {
         if(this.d) {
            this.d = false;
            return new PacketPlayOutMap(var1.i(), class_ayy.this.f, class_ayy.this.e, class_ayy.this.i.values(), class_ayy.this.g, this.e, this.f, this.g + 1 - this.e, this.h + 1 - this.f);
         } else {
            return this.i++ % 5 == 0?new PacketPlayOutMap(var1.i(), class_ayy.this.f, class_ayy.this.e, class_ayy.this.i.values(), class_ayy.this.g, 0, 0, 0, 0):null;
         }
      }

      public void a(int var1, int var2) {
         if(this.d) {
            this.e = Math.min(this.e, var1);
            this.f = Math.min(this.f, var2);
            this.g = Math.max(this.g, var1);
            this.h = Math.max(this.h, var2);
         } else {
            this.d = true;
            this.e = var1;
            this.f = var2;
            this.g = var1;
            this.h = var2;
         }

      }
   }
}
