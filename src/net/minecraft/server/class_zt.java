package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.GameProfileSerializer;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_zs;

public class class_zt extends Entity {
   private class_rz a;
   private Entity b;
   private EnumDirection c;
   private int d;
   private double e;
   private double f;
   private double g;
   private UUID h;
   private BlockPosition as;
   private UUID at;
   private BlockPosition au;

   public class_zt(World var1) {
      super(var1);
      this.a(0.3125F, 0.3125F);
      this.noClip = true;
   }

   public EnumSoundCategory by() {
      return EnumSoundCategory.HOSTILE;
   }

   public class_zt(World var1, class_rz var2, Entity var3, EnumDirection.class_a_in_class_cq var4) {
      this(var1);
      this.a = var2;
      BlockPosition var5 = new BlockPosition(var2);
      double var6 = (double)var5.p() + 0.5D;
      double var8 = (double)var5.q() + 0.5D;
      double var10 = (double)var5.r() + 0.5D;
      this.b(var6, var8, var10, this.yaw, this.pitch);
      this.b = var3;
      this.c = EnumDirection.UP;
      this.a(var4);
   }

   protected void b(NBTTagCompound var1) {
      BlockPosition var2;
      NBTTagCompound var3;
      if(this.a != null) {
         var2 = new BlockPosition(this.a);
         var3 = GameProfileSerializer.a(this.a.getUniqueId());
         var3.a("X", var2.p());
         var3.a("Y", var2.q());
         var3.a("Z", var2.r());
         var1.a((String)"Owner", (NBTTag)var3);
      }

      if(this.b != null) {
         var2 = new BlockPosition(this.b);
         var3 = GameProfileSerializer.a(this.b.getUniqueId());
         var3.a("X", var2.p());
         var3.a("Y", var2.q());
         var3.a("Z", var2.r());
         var1.a((String)"Target", (NBTTag)var3);
      }

      if(this.c != null) {
         var1.a("Dir", this.c.a());
      }

      var1.a("Steps", this.d);
      var1.a("TXD", this.e);
      var1.a("TYD", this.f);
      var1.a("TZD", this.g);
   }

   protected void a(NBTTagCompound var1) {
      this.d = var1.h("Steps");
      this.e = var1.k("TXD");
      this.f = var1.k("TYD");
      this.g = var1.k("TZD");
      if(var1.b("Dir", 99)) {
         this.c = EnumDirection.a(var1.h("Dir"));
      }

      NBTTagCompound var2;
      if(var1.b("Owner", 10)) {
         var2 = var1.o("Owner");
         this.h = GameProfileSerializer.b(var2);
         this.as = new BlockPosition(var2.h("X"), var2.h("Y"), var2.h("Z"));
      }

      if(var1.b("Target", 10)) {
         var2 = var1.o("Target");
         this.at = GameProfileSerializer.b(var2);
         this.au = new BlockPosition(var2.h("X"), var2.h("Y"), var2.h("Z"));
      }

   }

   protected void i() {
   }

   private void a(EnumDirection var1) {
      this.c = var1;
   }

   private void a(EnumDirection.class_a_in_class_cq var1) {
      double var3 = 0.5D;
      BlockPosition var2;
      if(this.b == null) {
         var2 = (new BlockPosition(this)).b();
      } else {
         var3 = (double)this.b.length * 0.5D;
         var2 = new BlockPosition(this.b.locX, this.b.locY + var3, this.b.locZ);
      }

      double var5 = (double)var2.p() + 0.5D;
      double var7 = (double)var2.q() + var3;
      double var9 = (double)var2.r() + 0.5D;
      EnumDirection var11 = null;
      if(var2.f(this.locX, this.locY, this.locZ) >= 4.0D) {
         BlockPosition var12 = new BlockPosition(this);
         ArrayList var13 = Lists.newArrayList();
         if(var1 != EnumDirection.class_a_in_class_cq.X) {
            if(var12.p() < var2.p() && this.world.d(var12.f())) {
               var13.add(EnumDirection.EAST);
            } else if(var12.p() > var2.p() && this.world.d(var12.e())) {
               var13.add(EnumDirection.WEST);
            }
         }

         if(var1 != EnumDirection.class_a_in_class_cq.Y) {
            if(var12.q() < var2.q() && this.world.d(var12.a())) {
               var13.add(EnumDirection.UP);
            } else if(var12.q() > var2.q() && this.world.d(var12.b())) {
               var13.add(EnumDirection.DOWN);
            }
         }

         if(var1 != EnumDirection.class_a_in_class_cq.Z) {
            if(var12.r() < var2.r() && this.world.d(var12.d())) {
               var13.add(EnumDirection.SOUTH);
            } else if(var12.r() > var2.r() && this.world.d(var12.c())) {
               var13.add(EnumDirection.NORTH);
            }
         }

         var11 = EnumDirection.a(this.random);
         if(var13.isEmpty()) {
            for(int var14 = 5; !this.world.d(var12.a(var11)) && var14 > 0; --var14) {
               var11 = EnumDirection.a(this.random);
            }
         } else {
            var11 = (EnumDirection)var13.get(this.random.nextInt(var13.size()));
         }

         var5 = this.locX + (double)var11.g();
         var7 = this.locY + (double)var11.h();
         var9 = this.locZ + (double)var11.i();
      }

      this.a(var11);
      double var20 = var5 - this.locX;
      double var21 = var7 - this.locY;
      double var16 = var9 - this.locZ;
      double var18 = (double)MathHelper.a(var20 * var20 + var21 * var21 + var16 * var16);
      if(var18 == 0.0D) {
         this.e = 0.0D;
         this.f = 0.0D;
         this.g = 0.0D;
      } else {
         this.e = var20 / var18 * 0.15D;
         this.f = var21 / var18 * 0.15D;
         this.g = var16 / var18 * 0.15D;
      }

      this.ai = true;
      this.d = 10 + this.random.nextInt(5) * 10;
   }

   public void m() {
      if(!this.world.E && this.world.ae() == EnumDifficulty.PEACEFUL) {
         this.S();
      } else {
         super.m();
         if(!this.world.E) {
            List var1;
            Iterator var2;
            class_rz var3;
            if(this.b == null && this.at != null) {
               var1 = this.world.a(class_rz.class, new AxisAlignedBB(this.au.a(-2, -2, -2), this.au.a(2, 2, 2)));
               var2 = var1.iterator();

               while(var2.hasNext()) {
                  var3 = (class_rz)var2.next();
                  if(var3.getUniqueId().equals(this.at)) {
                     this.b = var3;
                     break;
                  }
               }

               this.at = null;
            }

            if(this.a == null && this.h != null) {
               var1 = this.world.a(class_rz.class, new AxisAlignedBB(this.as.a(-2, -2, -2), this.as.a(2, 2, 2)));
               var2 = var1.iterator();

               while(var2.hasNext()) {
                  var3 = (class_rz)var2.next();
                  if(var3.getUniqueId().equals(this.h)) {
                     this.a = var3;
                     break;
                  }
               }

               this.h = null;
            }

            if(this.b == null || !this.b.at() || this.b instanceof EntityHuman && ((EntityHuman)this.b).y()) {
               this.motY -= 0.04D;
            } else {
               this.e = MathHelper.a(this.e * 1.025D, -1.0D, 1.0D);
               this.f = MathHelper.a(this.f * 1.025D, -1.0D, 1.0D);
               this.g = MathHelper.a(this.g * 1.025D, -1.0D, 1.0D);
               this.motX += (this.e - this.motX) * 0.2D;
               this.motY += (this.f - this.motY) * 0.2D;
               this.motZ += (this.g - this.motZ) * 0.2D;
            }

            MovingObjectPosition var4 = class_zs.a(this, true, false, this.a);
            if(var4 != null) {
               this.a(var4);
            }
         }

         this.b(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
         class_zs.a(this, 0.5F);
         if(this.world.E) {
            this.world.a(EnumParticle.END_ROD, this.locX - this.motX, this.locY - this.motY + 0.15D, this.locZ - this.motZ, 0.0D, 0.0D, 0.0D, new int[0]);
         } else if(this.b != null && !this.b.dead) {
            if(this.d > 0) {
               --this.d;
               if(this.d == 0) {
                  this.a(this.c == null?null:this.c.k());
               }
            }

            if(this.c != null) {
               BlockPosition var5 = new BlockPosition(this);
               EnumDirection.class_a_in_class_cq var6 = this.c.k();
               if(this.world.d(var5.a(this.c), false)) {
                  this.a(var6);
               } else {
                  BlockPosition var7 = new BlockPosition(this.b);
                  if(var6 == EnumDirection.class_a_in_class_cq.X && var5.p() == var7.p() || var6 == EnumDirection.class_a_in_class_cq.Z && var5.r() == var7.r() || var6 == EnumDirection.class_a_in_class_cq.Y && var5.q() == var7.q()) {
                     this.a(var6);
                  }
               }
            }
         }

      }
   }

   public boolean aG() {
      return false;
   }

   public float e(float var1) {
      return 1.0F;
   }

   protected void a(MovingObjectPosition var1) {
      if(var1.d == null) {
         ((WorldServer)this.world).a(EnumParticle.EXPLOSION_LARGE, this.locX, this.locY, this.locZ, 2, 0.2D, 0.2D, 0.2D, 0.0D, new int[0]);
         this.a(class_ng.eP, 1.0F, 1.0F);
      } else {
         boolean var2 = var1.d.a(DamageSource.a((Entity)this, (class_rz)this.a).b(), 4.0F);
         if(var2) {
            this.a(this.a, var1.d);
            if(var1.d instanceof class_rz) {
               ((class_rz)var1.d).c(new MobEffect(MobEffectList.y, 200));
            }
         }
      }

      this.S();
   }

   public boolean ao() {
      return true;
   }

   public boolean a(DamageSource var1, float var2) {
      if(!this.world.E) {
         this.a(class_ng.eQ, 1.0F, 1.0F);
         ((WorldServer)this.world).a(EnumParticle.CRIT, this.locX, this.locY, this.locZ, 15, 0.2D, 0.2D, 0.2D, 0.0D, new int[0]);
         this.S();
      }

      return true;
   }
}
