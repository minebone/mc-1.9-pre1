package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.class_ahw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_anl extends Block {
   public class_anl(Material var1, MaterialMapColor var2) {
      super(var1, var2);
   }

   public boolean g(IBlockData var1) {
      return true;
   }

   public int b(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return 15;
   }
}
