package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.EntityCreature;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_tj;

public class class_tf extends class_tj {
   private EntityCreature a;
   private double b;
   private double c;
   private double d;
   private double e;
   private World f;

   public class_tf(EntityCreature var1, double var2) {
      this.a = var1;
      this.e = var2;
      this.f = var1.world;
      this.a(1);
   }

   public boolean a() {
      if(!this.f.B()) {
         return false;
      } else if(!this.a.aG()) {
         return false;
      } else if(!this.f.h(new BlockPosition(this.a.locX, this.a.bk().b, this.a.locZ))) {
         return false;
      } else if(this.a.a(EnumInventorySlot.HEAD) != null) {
         return false;
      } else {
         Vec3D var1 = this.f();
         if(var1 == null) {
            return false;
         } else {
            this.b = var1.b;
            this.c = var1.c;
            this.d = var1.d;
            return true;
         }
      }
   }

   public boolean b() {
      return !this.a.x().n();
   }

   public void c() {
      this.a.x().a(this.b, this.c, this.d, this.e);
   }

   private Vec3D f() {
      Random var1 = this.a.bE();
      BlockPosition var2 = new BlockPosition(this.a.locX, this.a.bk().b, this.a.locZ);

      for(int var3 = 0; var3 < 10; ++var3) {
         BlockPosition var4 = var2.a(var1.nextInt(20) - 10, var1.nextInt(6) - 3, var1.nextInt(20) - 10);
         if(!this.f.h(var4) && this.a.a(var4) < 0.0F) {
            return new Vec3D((double)var4.p(), (double)var4.q(), (double)var4.r());
         }
      }

      return null;
   }
}
