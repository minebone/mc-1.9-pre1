package net.minecraft.server;

import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.Blocks;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Enchantment;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EntityWitch;
import net.minecraft.server.EnumColor;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.MerchantRecipe;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.PathfinderGoalRestrictOpenDoor;
import net.minecraft.server.ScoreboardTeam;
import net.minecraft.server.ScoreboardTeamBase;
import net.minecraft.server.StatisticList;
import net.minecraft.server.Village;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agn;
import net.minecraft.server.class_ago;
import net.minecraft.server.class_ahe;
import net.minecraft.server.class_ahg;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_ou;
import net.minecraft.server.class_qk;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_qu;
import net.minecraft.server.class_rn;
import net.minecraft.server.class_rw;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sz;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_tl;
import net.minecraft.server.class_tm;
import net.minecraft.server.class_to;
import net.minecraft.server.class_tp;
import net.minecraft.server.class_tq;
import net.minecraft.server.class_ts;
import net.minecraft.server.class_tv;
import net.minecraft.server.class_ua;
import net.minecraft.server.class_uc;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_un;
import net.minecraft.server.class_up;
import net.minecraft.server.class_uq;
import net.minecraft.server.class_vd;
import net.minecraft.server.class_xz;
import net.minecraft.server.class_yc;
import net.minecraft.server.class_yk;
import net.minecraft.server.class_ys;
import net.minecraft.server.class_yz;
import net.minecraft.server.class_zc;

public class class_zd extends class_rn implements class_ahe, class_zc {
   private static final class_ke bw = DataWatcher.a(class_zd.class, class_kg.b);
   private int bx;
   private boolean by;
   private boolean bz;
   Village bv;
   private EntityHuman bA;
   private class_ahg bB;
   private int bC;
   private boolean bD;
   private boolean bE;
   private int bF;
   private String bG;
   private int bH;
   private int bI;
   private boolean bJ;
   private boolean bK;
   private class_qu bL;
   private static final class_zd.class_f_in_class_zd[][][][] bM = new class_zd.class_f_in_class_zd[][][][]{{{{new class_zd.class_a_in_class_zd(Items.Q, new class_zd.class_g_in_class_zd(18, 22)), new class_zd.class_a_in_class_zd(Items.cc, new class_zd.class_g_in_class_zd(15, 19)), new class_zd.class_a_in_class_zd(Items.cb, new class_zd.class_g_in_class_zd(15, 19)), new class_zd.class_e_in_class_zd(Items.R, new class_zd.class_g_in_class_zd(-4, -2))}, {new class_zd.class_a_in_class_zd(Item.a(Blocks.aU), new class_zd.class_g_in_class_zd(8, 13)), new class_zd.class_e_in_class_zd(Items.ck, new class_zd.class_g_in_class_zd(-3, -2))}, {new class_zd.class_a_in_class_zd(Item.a(Blocks.bk), new class_zd.class_g_in_class_zd(7, 12)), new class_zd.class_e_in_class_zd(Items.e, new class_zd.class_g_in_class_zd(-5, -7))}, {new class_zd.class_e_in_class_zd(Items.bj, new class_zd.class_g_in_class_zd(-6, -10)), new class_zd.class_e_in_class_zd(Items.bg, new class_zd.class_g_in_class_zd(1, 1))}}, {{new class_zd.class_a_in_class_zd(Items.H, new class_zd.class_g_in_class_zd(15, 20)), new class_zd.class_a_in_class_zd(Items.j, new class_zd.class_g_in_class_zd(16, 24)), new class_zd.class_d_in_class_zd(Items.bb, new class_zd.class_g_in_class_zd(6, 6), Items.bc, new class_zd.class_g_in_class_zd(6, 6))}, {new class_zd.class_c_in_class_zd(Items.aY, new class_zd.class_g_in_class_zd(7, 8))}}, {{new class_zd.class_a_in_class_zd(Item.a(Blocks.L), new class_zd.class_g_in_class_zd(16, 22)), new class_zd.class_e_in_class_zd(Items.bl, new class_zd.class_g_in_class_zd(3, 4))}, {new class_zd.class_e_in_class_zd(new ItemStack(Item.a(Blocks.L)), new class_zd.class_g_in_class_zd(1, 2)), new class_zd.class_e_in_class_zd(new ItemStack(Item.a(Blocks.L), 1, 1), new class_zd.class_g_in_class_zd(1, 2)), new class_zd.class_e_in_class_zd(new ItemStack(Item.a(Blocks.L), 1, 2), new class_zd.class_g_in_class_zd(1, 2)), new class_zd.class_e_in_class_zd(new ItemStack(Item.a(Blocks.L), 1, 3), new class_zd.class_g_in_class_zd(1, 2)), new class_zd.class_e_in_class_zd(new ItemStack(Item.a(Blocks.L), 1, 4), new class_zd.class_g_in_class_zd(1, 2)), new class_zd.class_e_in_class_zd(new ItemStack(Item.a(Blocks.L), 1, 5), new class_zd.class_g_in_class_zd(1, 2)), new class_zd.class_e_in_class_zd(new ItemStack(Item.a(Blocks.L), 1, 6), new class_zd.class_g_in_class_zd(1, 2)), new class_zd.class_e_in_class_zd(new ItemStack(Item.a(Blocks.L), 1, 7), new class_zd.class_g_in_class_zd(1, 2)), new class_zd.class_e_in_class_zd(new ItemStack(Item.a(Blocks.L), 1, 8), new class_zd.class_g_in_class_zd(1, 2)), new class_zd.class_e_in_class_zd(new ItemStack(Item.a(Blocks.L), 1, 9), new class_zd.class_g_in_class_zd(1, 2)), new class_zd.class_e_in_class_zd(new ItemStack(Item.a(Blocks.L), 1, 10), new class_zd.class_g_in_class_zd(1, 2)), new class_zd.class_e_in_class_zd(new ItemStack(Item.a(Blocks.L), 1, 11), new class_zd.class_g_in_class_zd(1, 2)), new class_zd.class_e_in_class_zd(new ItemStack(Item.a(Blocks.L), 1, 12), new class_zd.class_g_in_class_zd(1, 2)), new class_zd.class_e_in_class_zd(new ItemStack(Item.a(Blocks.L), 1, 13), new class_zd.class_g_in_class_zd(1, 2)), new class_zd.class_e_in_class_zd(new ItemStack(Item.a(Blocks.L), 1, 14), new class_zd.class_g_in_class_zd(1, 2)), new class_zd.class_e_in_class_zd(new ItemStack(Item.a(Blocks.L), 1, 15), new class_zd.class_g_in_class_zd(1, 2))}}, {{new class_zd.class_a_in_class_zd(Items.H, new class_zd.class_g_in_class_zd(15, 20)), new class_zd.class_e_in_class_zd(Items.g, new class_zd.class_g_in_class_zd(-12, -8))}, {new class_zd.class_e_in_class_zd(Items.f, new class_zd.class_g_in_class_zd(2, 3)), new class_zd.class_d_in_class_zd(Item.a(Blocks.n), new class_zd.class_g_in_class_zd(10, 10), Items.am, new class_zd.class_g_in_class_zd(6, 10))}}}, {{{new class_zd.class_a_in_class_zd(Items.aR, new class_zd.class_g_in_class_zd(24, 36)), new class_zd.class_b_in_class_zd()}, {new class_zd.class_a_in_class_zd(Items.aS, new class_zd.class_g_in_class_zd(8, 10)), new class_zd.class_e_in_class_zd(Items.aX, new class_zd.class_g_in_class_zd(10, 12)), new class_zd.class_e_in_class_zd(Item.a(Blocks.X), new class_zd.class_g_in_class_zd(3, 4))}, {new class_zd.class_a_in_class_zd(Items.bX, new class_zd.class_g_in_class_zd(2, 2)), new class_zd.class_e_in_class_zd(Items.aZ, new class_zd.class_g_in_class_zd(10, 12)), new class_zd.class_e_in_class_zd(Item.a(Blocks.w), new class_zd.class_g_in_class_zd(-5, -3))}, {new class_zd.class_b_in_class_zd()}, {new class_zd.class_b_in_class_zd()}, {new class_zd.class_e_in_class_zd(Items.cy, new class_zd.class_g_in_class_zd(20, 22))}}}, {{{new class_zd.class_a_in_class_zd(Items.bA, new class_zd.class_g_in_class_zd(36, 40)), new class_zd.class_a_in_class_zd(Items.m, new class_zd.class_g_in_class_zd(8, 10))}, {new class_zd.class_e_in_class_zd(Items.aE, new class_zd.class_g_in_class_zd(-4, -1)), new class_zd.class_e_in_class_zd(new ItemStack(Items.bd, 1, EnumColor.BLUE.b()), new class_zd.class_g_in_class_zd(-2, -1))}, {new class_zd.class_e_in_class_zd(Items.bB, new class_zd.class_g_in_class_zd(4, 7)), new class_zd.class_e_in_class_zd(Item.a(Blocks.aX), new class_zd.class_g_in_class_zd(-3, -1))}, {new class_zd.class_e_in_class_zd(Items.bU, new class_zd.class_g_in_class_zd(3, 11))}}}, {{{new class_zd.class_a_in_class_zd(Items.j, new class_zd.class_g_in_class_zd(16, 24)), new class_zd.class_e_in_class_zd(Items.aa, new class_zd.class_g_in_class_zd(4, 6))}, {new class_zd.class_a_in_class_zd(Items.l, new class_zd.class_g_in_class_zd(7, 9)), new class_zd.class_e_in_class_zd(Items.ab, new class_zd.class_g_in_class_zd(10, 14))}, {new class_zd.class_a_in_class_zd(Items.k, new class_zd.class_g_in_class_zd(3, 4)), new class_zd.class_c_in_class_zd(Items.af, new class_zd.class_g_in_class_zd(16, 19))}, {new class_zd.class_e_in_class_zd(Items.Z, new class_zd.class_g_in_class_zd(5, 7)), new class_zd.class_e_in_class_zd(Items.Y, new class_zd.class_g_in_class_zd(9, 11)), new class_zd.class_e_in_class_zd(Items.W, new class_zd.class_g_in_class_zd(5, 7)), new class_zd.class_e_in_class_zd(Items.X, new class_zd.class_g_in_class_zd(11, 15))}}, {{new class_zd.class_a_in_class_zd(Items.j, new class_zd.class_g_in_class_zd(16, 24)), new class_zd.class_e_in_class_zd(Items.c, new class_zd.class_g_in_class_zd(6, 8))}, {new class_zd.class_a_in_class_zd(Items.l, new class_zd.class_g_in_class_zd(7, 9)), new class_zd.class_c_in_class_zd(Items.n, new class_zd.class_g_in_class_zd(9, 10))}, {new class_zd.class_a_in_class_zd(Items.k, new class_zd.class_g_in_class_zd(3, 4)), new class_zd.class_c_in_class_zd(Items.w, new class_zd.class_g_in_class_zd(12, 15)), new class_zd.class_c_in_class_zd(Items.z, new class_zd.class_g_in_class_zd(9, 12))}}, {{new class_zd.class_a_in_class_zd(Items.j, new class_zd.class_g_in_class_zd(16, 24)), new class_zd.class_c_in_class_zd(Items.a, new class_zd.class_g_in_class_zd(5, 7))}, {new class_zd.class_a_in_class_zd(Items.l, new class_zd.class_g_in_class_zd(7, 9)), new class_zd.class_c_in_class_zd(Items.b, new class_zd.class_g_in_class_zd(9, 11))}, {new class_zd.class_a_in_class_zd(Items.k, new class_zd.class_g_in_class_zd(3, 4)), new class_zd.class_c_in_class_zd(Items.y, new class_zd.class_g_in_class_zd(12, 15))}}}, {{{new class_zd.class_a_in_class_zd(Items.an, new class_zd.class_g_in_class_zd(14, 18)), new class_zd.class_a_in_class_zd(Items.br, new class_zd.class_g_in_class_zd(14, 18))}, {new class_zd.class_a_in_class_zd(Items.j, new class_zd.class_g_in_class_zd(16, 24)), new class_zd.class_e_in_class_zd(Items.ao, new class_zd.class_g_in_class_zd(-7, -5)), new class_zd.class_e_in_class_zd(Items.bs, new class_zd.class_g_in_class_zd(-8, -6))}}, {{new class_zd.class_a_in_class_zd(Items.aM, new class_zd.class_g_in_class_zd(9, 12)), new class_zd.class_e_in_class_zd(Items.U, new class_zd.class_g_in_class_zd(2, 4))}, {new class_zd.class_c_in_class_zd(Items.T, new class_zd.class_g_in_class_zd(7, 12))}, {new class_zd.class_e_in_class_zd(Items.aC, new class_zd.class_g_in_class_zd(8, 10))}}}};

   public class_zd(World var1) {
      this(var1, 0);
   }

   public class_zd(World var1, int var2) {
      super(var1);
      this.bL = new class_qu("Items", false, 8);
      this.l(var2);
      this.a(0.6F, 1.95F);
      ((class_vd)this.x()).a(true);
      this.l(true);
   }

   protected void r() {
      this.bp.a(0, new class_tg(this));
      this.bp.a(1, new class_sz(this, class_yz.class, 8.0F, 0.6D, 0.6D));
      this.bp.a(1, new class_up(this));
      this.bp.a(1, new class_tp(this));
      this.bp.a(2, new class_ts(this));
      this.bp.a(3, new PathfinderGoalRestrictOpenDoor(this));
      this.bp.a(4, new class_ua(this, true));
      this.bp.a(5, new class_tv(this, 0.6D));
      this.bp.a(6, new class_tq(this));
      this.bp.a(7, new class_un(this));
      this.bp.a(9, new class_tm(this, EntityHuman.class, 3.0F, 1.0F));
      this.bp.a(9, new class_uq(this));
      this.bp.a(9, new class_uf(this, 0.6D));
      this.bp.a(10, new class_to(this, EntityInsentient.class, 8.0F));
   }

   private void dj() {
      if(!this.bK) {
         this.bK = true;
         if(this.m_()) {
            this.bp.a(8, new class_uc(this, 0.32D));
         } else if(this.cZ() == 0) {
            this.bp.a(6, new class_tl(this, 0.6D));
         }

      }
   }

   protected void o() {
      if(this.cZ() == 0) {
         this.bp.a(8, new class_tl(this, 0.6D));
      }

      super.o();
   }

   protected void bz() {
      super.bz();
      this.a((class_sk)class_ys.d).a(0.5D);
   }

   protected void M() {
      if(--this.bx <= 0) {
         BlockPosition var1 = new BlockPosition(this);
         this.world.ai().a(var1);
         this.bx = 70 + this.random.nextInt(50);
         this.bv = this.world.ai().a(var1, 32);
         if(this.bv == null) {
            this.cX();
         } else {
            BlockPosition var2 = this.bv.a();
            this.a(var2, this.bv.b());
            if(this.bJ) {
               this.bJ = false;
               this.bv.b(5);
            }
         }
      }

      if(!this.dc() && this.bC > 0) {
         --this.bC;
         if(this.bC <= 0) {
            if(this.bD) {
               Iterator var3 = this.bB.iterator();

               while(var3.hasNext()) {
                  MerchantRecipe var4 = (MerchantRecipe)var3.next();
                  if(var4.h()) {
                     var4.a(this.random.nextInt(6) + this.random.nextInt(6) + 2);
                  }
               }

               this.dk();
               this.bD = false;
               if(this.bv != null && this.bG != null) {
                  this.world.a((Entity)this, (byte)14);
                  this.bv.a(this.bG, 1);
               }
            }

            this.c(new MobEffect(MobEffectList.j, 200, 0));
         }
      }

      super.M();
   }

   public boolean a(EntityHuman var1, EnumHand var2, ItemStack var3) {
      boolean var4 = var3 != null && var3.b() == Items.bT;
      if(!var4 && this.at() && !this.dc() && !this.m_()) {
         if(!this.world.E && (this.bB == null || !this.bB.isEmpty())) {
            this.a_(var1);
            var1.a((class_ahe)this);
         }

         var1.b(StatisticList.H);
         return true;
      } else {
         return super.a(var1, var2, var3);
      }
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)bw, (Object)Integer.valueOf(0));
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("Profession", this.cZ());
      var1.a("Riches", this.bF);
      var1.a("Career", this.bH);
      var1.a("CareerLevel", this.bI);
      var1.a("Willing", this.bE);
      if(this.bB != null) {
         var1.a((String)"Offers", (NBTTag)this.bB.a());
      }

      NBTTagList var2 = new NBTTagList();

      for(int var3 = 0; var3 < this.bL.u_(); ++var3) {
         ItemStack var4 = this.bL.a(var3);
         if(var4 != null) {
            var2.a((NBTTag)var4.b(new NBTTagCompound()));
         }
      }

      var1.a((String)"Inventory", (NBTTag)var2);
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.l(var1.h("Profession"));
      this.bF = var1.h("Riches");
      this.bH = var1.h("Career");
      this.bI = var1.h("CareerLevel");
      this.bE = var1.p("Willing");
      if(var1.b("Offers", 10)) {
         NBTTagCompound var2 = var1.o("Offers");
         this.bB = new class_ahg(var2);
      }

      NBTTagList var5 = var1.c("Inventory", 10);

      for(int var3 = 0; var3 < var5.c(); ++var3) {
         ItemStack var4 = ItemStack.a(var5.b(var3));
         if(var4 != null) {
            this.bL.a(var4);
         }
      }

      this.l(true);
      this.dj();
   }

   protected boolean K() {
      return false;
   }

   protected class_nf G() {
      return this.dc()?class_ng.gn:class_ng.gj;
   }

   protected class_nf bQ() {
      return class_ng.gl;
   }

   protected class_nf bR() {
      return class_ng.gk;
   }

   public void l(int var1) {
      this.datawatcher.b(bw, Integer.valueOf(var1));
   }

   public int cZ() {
      return Math.max(((Integer)this.datawatcher.a(bw)).intValue() % 5, 0);
   }

   public boolean da() {
      return this.by;
   }

   public void o(boolean var1) {
      this.by = var1;
   }

   public void p(boolean var1) {
      this.bz = var1;
   }

   public boolean db() {
      return this.bz;
   }

   public void a(class_rz var1) {
      super.a((class_rz)var1);
      if(this.bv != null && var1 != null) {
         this.bv.a(var1);
         if(var1 instanceof EntityHuman) {
            byte var2 = -1;
            if(this.m_()) {
               var2 = -3;
            }

            this.bv.a(var1.h_(), var2);
            if(this.at()) {
               this.world.a((Entity)this, (byte)13);
            }
         }
      }

   }

   public void a(DamageSource var1) {
      if(this.bv != null) {
         Entity var2 = var1.j();
         if(var2 != null) {
            if(var2 instanceof EntityHuman) {
               this.bv.a(var2.h_(), -2);
            } else if(var2 instanceof class_yk) {
               this.bv.h();
            }
         } else {
            EntityHuman var3 = this.world.a(this, 16.0D);
            if(var3 != null) {
               this.bv.h();
            }
         }
      }

      super.a((DamageSource)var1);
   }

   public void a_(EntityHuman var1) {
      this.bA = var1;
   }

   public EntityHuman t_() {
      return this.bA;
   }

   public boolean dc() {
      return this.bA != null;
   }

   public boolean q(boolean var1) {
      if(!this.bE && var1 && this.df()) {
         boolean var2 = false;

         for(int var3 = 0; var3 < this.bL.u_(); ++var3) {
            ItemStack var4 = this.bL.a(var3);
            if(var4 != null) {
               if(var4.b() == Items.R && var4.b >= 3) {
                  var2 = true;
                  this.bL.a(var3, 3);
               } else if((var4.b() == Items.cc || var4.b() == Items.cb) && var4.b >= 12) {
                  var2 = true;
                  this.bL.a(var3, 12);
               }
            }

            if(var2) {
               this.world.a((Entity)this, (byte)18);
               this.bE = true;
               break;
            }
         }
      }

      return this.bE;
   }

   public void r(boolean var1) {
      this.bE = var1;
   }

   public void a(MerchantRecipe var1) {
      var1.g();
      this.a_ = -this.C();
      this.a(class_ng.go, this.cc(), this.cd());
      int var2 = 3 + this.random.nextInt(4);
      if(var1.e() == 1 || this.random.nextInt(5) == 0) {
         this.bC = 40;
         this.bD = true;
         this.bE = true;
         if(this.bA != null) {
            this.bG = this.bA.h_();
         } else {
            this.bG = null;
         }

         var2 += 5;
      }

      if(var1.a().b() == Items.bY) {
         this.bF += var1.a().b;
      }

      if(var1.j()) {
         this.world.a((Entity)(new class_rw(this.world, this.locX, this.locY + 0.5D, this.locZ, var2)));
      }

   }

   public void a(ItemStack var1) {
      if(!this.world.E && this.a_ > -this.C() + 20) {
         this.a_ = -this.C();
         if(var1 != null) {
            this.a(class_ng.go, this.cc(), this.cd());
         } else {
            this.a(class_ng.gm, this.cc(), this.cd());
         }
      }

   }

   public class_ahg b_(EntityHuman var1) {
      if(this.bB == null) {
         this.dk();
      }

      return this.bB;
   }

   private void dk() {
      class_zd.class_f_in_class_zd[][][] var1 = bM[this.cZ()];
      if(this.bH != 0 && this.bI != 0) {
         ++this.bI;
      } else {
         this.bH = this.random.nextInt(var1.length) + 1;
         this.bI = 1;
      }

      if(this.bB == null) {
         this.bB = new class_ahg();
      }

      int var2 = this.bH - 1;
      int var3 = this.bI - 1;
      class_zd.class_f_in_class_zd[][] var4 = var1[var2];
      if(var3 >= 0 && var3 < var4.length) {
         class_zd.class_f_in_class_zd[] var5 = var4[var3];
         class_zd.class_f_in_class_zd[] var6 = var5;
         int var7 = var5.length;

         for(int var8 = 0; var8 < var7; ++var8) {
            class_zd.class_f_in_class_zd var9 = var6[var8];
            var9.a(this.bB, this.random);
         }
      }

   }

   public IChatBaseComponent i_() {
      ScoreboardTeamBase var1 = this.aN();
      String var2 = this.be();
      if(var2 != null && !var2.isEmpty()) {
         ChatComponentText var5 = new ChatComponentText(ScoreboardTeam.a(var1, var2));
         var5.b().a(this.bj());
         var5.b().a(this.getUniqueId().toString());
         return var5;
      } else {
         if(this.bB == null) {
            this.dk();
         }

         String var3 = null;
         switch(this.cZ()) {
         case 0:
            if(this.bH == 1) {
               var3 = "farmer";
            } else if(this.bH == 2) {
               var3 = "fisherman";
            } else if(this.bH == 3) {
               var3 = "shepherd";
            } else if(this.bH == 4) {
               var3 = "fletcher";
            }
            break;
         case 1:
            var3 = "librarian";
            break;
         case 2:
            var3 = "cleric";
            break;
         case 3:
            if(this.bH == 1) {
               var3 = "armor";
            } else if(this.bH == 2) {
               var3 = "weapon";
            } else if(this.bH == 3) {
               var3 = "tool";
            }
            break;
         case 4:
            if(this.bH == 1) {
               var3 = "butcher";
            } else if(this.bH == 2) {
               var3 = "leather";
            }
         }

         if(var3 != null) {
            ChatMessage var4 = new ChatMessage("entity.Villager." + var3, new Object[0]);
            var4.b().a(this.bj());
            var4.b().a(this.getUniqueId().toString());
            if(var1 != null) {
               var4.b().a(var1.m());
            }

            return var4;
         } else {
            return super.i_();
         }
      }
   }

   public float bm() {
      return this.m_()?0.81F:1.62F;
   }

   public class_sc a(class_qk var1, class_sc var2) {
      var2 = super.a(var1, var2);
      this.l(this.world.r.nextInt(5));
      this.dj();
      return var2;
   }

   public void dd() {
      this.bJ = true;
   }

   public class_zd b(class_rn var1) {
      class_zd var2 = new class_zd(this.world);
      var2.a(this.world.D(new BlockPosition(var2)), (class_sc)null);
      return var2;
   }

   public boolean a(EntityHuman var1) {
      return false;
   }

   public void a(class_xz var1) {
      if(!this.world.E && !this.dead) {
         EntityWitch var2 = new EntityWitch(this.world);
         var2.b(this.locX, this.locY, this.locZ, this.yaw, this.pitch);
         var2.a(this.world.D(new BlockPosition(var2)), (class_sc)null);
         var2.m(this.cR());
         if(this.o_()) {
            var2.c(this.be());
            var2.i(this.bf());
         }

         this.world.a((Entity)var2);
         this.S();
      }
   }

   public class_qu de() {
      return this.bL;
   }

   protected void a(class_yc var1) {
      ItemStack var2 = var1.k();
      Item var3 = var2.b();
      if(this.a(var3)) {
         ItemStack var4 = this.bL.a(var2);
         if(var4 == null) {
            var1.S();
         } else {
            var2.b = var4.b;
         }
      }

   }

   private boolean a(Item var1) {
      return var1 == Items.R || var1 == Items.cc || var1 == Items.cb || var1 == Items.Q || var1 == Items.P || var1 == Items.cV || var1 == Items.cU;
   }

   public boolean df() {
      return this.m(1);
   }

   public boolean dg() {
      return this.m(2);
   }

   public boolean dh() {
      boolean var1 = this.cZ() == 0;
      return var1?!this.m(5):!this.m(1);
   }

   private boolean m(int var1) {
      boolean var2 = this.cZ() == 0;

      for(int var3 = 0; var3 < this.bL.u_(); ++var3) {
         ItemStack var4 = this.bL.a(var3);
         if(var4 != null) {
            if(var4.b() == Items.R && var4.b >= 3 * var1 || var4.b() == Items.cc && var4.b >= 12 * var1 || var4.b() == Items.cb && var4.b >= 12 * var1 || var4.b() == Items.cV && var4.b >= 12 * var1) {
               return true;
            }

            if(var2 && var4.b() == Items.Q && var4.b >= 9 * var1) {
               return true;
            }
         }
      }

      return false;
   }

   public boolean di() {
      for(int var1 = 0; var1 < this.bL.u_(); ++var1) {
         ItemStack var2 = this.bL.a(var1);
         if(var2 != null && (var2.b() == Items.P || var2.b() == Items.cc || var2.b() == Items.cb || var2.b() == Items.cU)) {
            return true;
         }
      }

      return false;
   }

   public boolean c(int var1, ItemStack var2) {
      if(super.c(var1, var2)) {
         return true;
      } else {
         int var3 = var1 - 300;
         if(var3 >= 0 && var3 < this.bL.u_()) {
            this.bL.a(var3, var2);
            return true;
         } else {
            return false;
         }
      }
   }

   // $FF: synthetic method
   public class_rn a(class_rn var1) {
      return this.b(var1);
   }

   static class class_d_in_class_zd implements class_zd.class_f_in_class_zd {
      public ItemStack a;
      public class_zd.class_g_in_class_zd b;
      public ItemStack c;
      public class_zd.class_g_in_class_zd d;

      public class_d_in_class_zd(Item var1, class_zd.class_g_in_class_zd var2, Item var3, class_zd.class_g_in_class_zd var4) {
         this.a = new ItemStack(var1);
         this.b = var2;
         this.c = new ItemStack(var3);
         this.d = var4;
      }

      public void a(class_ahg var1, Random var2) {
         int var3 = 1;
         if(this.b != null) {
            var3 = this.b.a(var2);
         }

         int var4 = 1;
         if(this.d != null) {
            var4 = this.d.a(var2);
         }

         var1.add(new MerchantRecipe(new ItemStack(this.a.b(), var3, this.a.i()), new ItemStack(Items.bY), new ItemStack(this.c.b(), var4, this.c.i())));
      }
   }

   static class class_b_in_class_zd implements class_zd.class_f_in_class_zd {
      public void a(class_ahg var1, Random var2) {
         Enchantment var3 = (Enchantment)Enchantment.b.a(var2);
         int var4 = MathHelper.a(var2, var3.d(), var3.b());
         ItemStack var5 = Items.cn.a(new class_ago(var3, var4));
         int var6 = 2 + var2.nextInt(5 + var4 * 10) + 3 * var4;
         if(var3.e()) {
            var6 *= 2;
         }

         if(var6 > 64) {
            var6 = 64;
         }

         var1.add(new MerchantRecipe(new ItemStack(Items.aS), new ItemStack(Items.bY, var6), var5));
      }
   }

   static class class_c_in_class_zd implements class_zd.class_f_in_class_zd {
      public ItemStack a;
      public class_zd.class_g_in_class_zd b;

      public class_c_in_class_zd(Item var1, class_zd.class_g_in_class_zd var2) {
         this.a = new ItemStack(var1);
         this.b = var2;
      }

      public void a(class_ahg var1, Random var2) {
         int var3 = 1;
         if(this.b != null) {
            var3 = this.b.a(var2);
         }

         ItemStack var4 = new ItemStack(Items.bY, var3, 0);
         ItemStack var5 = new ItemStack(this.a.b(), 1, this.a.i());
         var5 = class_agn.a(var2, var5, 5 + var2.nextInt(15), false);
         var1.add(new MerchantRecipe(var4, var5));
      }
   }

   static class class_e_in_class_zd implements class_zd.class_f_in_class_zd {
      public ItemStack a;
      public class_zd.class_g_in_class_zd b;

      public class_e_in_class_zd(Item var1, class_zd.class_g_in_class_zd var2) {
         this.a = new ItemStack(var1);
         this.b = var2;
      }

      public class_e_in_class_zd(ItemStack var1, class_zd.class_g_in_class_zd var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a(class_ahg var1, Random var2) {
         int var3 = 1;
         if(this.b != null) {
            var3 = this.b.a(var2);
         }

         ItemStack var4;
         ItemStack var5;
         if(var3 < 0) {
            var4 = new ItemStack(Items.bY);
            var5 = new ItemStack(this.a.b(), -var3, this.a.i());
         } else {
            var4 = new ItemStack(Items.bY, var3, 0);
            var5 = new ItemStack(this.a.b(), 1, this.a.i());
         }

         var1.add(new MerchantRecipe(var4, var5));
      }
   }

   static class class_a_in_class_zd implements class_zd.class_f_in_class_zd {
      public Item a;
      public class_zd.class_g_in_class_zd b;

      public class_a_in_class_zd(Item var1, class_zd.class_g_in_class_zd var2) {
         this.a = var1;
         this.b = var2;
      }

      public void a(class_ahg var1, Random var2) {
         int var3 = 1;
         if(this.b != null) {
            var3 = this.b.a(var2);
         }

         var1.add(new MerchantRecipe(new ItemStack(this.a, var3, 0), Items.bY));
      }
   }

   interface class_f_in_class_zd {
      void a(class_ahg var1, Random var2);
   }

   static class class_g_in_class_zd extends class_ou {
      public class_g_in_class_zd(int var1, int var2) {
         super(Integer.valueOf(var1), Integer.valueOf(var2));
      }

      public int a(Random var1) {
         return ((Integer)this.a()).intValue() >= ((Integer)this.b()).intValue()?((Integer)this.a()).intValue():((Integer)this.a()).intValue() + var1.nextInt(((Integer)this.b()).intValue() - ((Integer)this.a()).intValue() + 1);
      }
   }
}
