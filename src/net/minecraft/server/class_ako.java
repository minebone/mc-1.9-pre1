package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ajx;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_ako extends class_ajx {
   protected static final AxisAlignedBB a = new AxisAlignedBB(0.09999999403953552D, 0.0D, 0.09999999403953552D, 0.8999999761581421D, 0.800000011920929D, 0.8999999761581421D);

   protected class_ako() {
      super(Material.l);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return a;
   }

   public MaterialMapColor r(IBlockData var1) {
      return MaterialMapColor.o;
   }

   protected boolean i(IBlockData var1) {
      return var1.getBlock() == Blocks.m || var1.getBlock() == Blocks.cz || var1.getBlock() == Blocks.cu || var1.getBlock() == Blocks.d;
   }

   public boolean a(class_ahw var1, BlockPosition var2) {
      return true;
   }

   public int a(Random var1) {
      return var1.nextInt(3);
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Items.A;
   }

   public void a(World var1, EntityHuman var2, BlockPosition var3, IBlockData var4, TileEntity var5, ItemStack var6) {
      if(!var1.E && var6 != null && var6.b() == Items.bl) {
         var2.b(StatisticList.a((Block)this));
         a(var1, var3, new ItemStack(Blocks.I, 1, 0));
      } else {
         super.a(var1, var2, var3, var4, var5, var6);
      }

   }
}
