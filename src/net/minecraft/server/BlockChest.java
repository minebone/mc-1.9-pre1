package net.minecraft.server;

import java.util.Iterator;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Container;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_amf;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aoa;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_apw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arn;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_qe;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_qi;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_qr;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_wa;

public class BlockChest extends class_ajm {
   public static final class_arn a = class_amf.D;
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.0625D, 0.0D, 0.0D, 0.9375D, 0.875D, 0.9375D);
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.0625D, 0.0D, 0.0625D, 0.9375D, 0.875D, 1.0D);
   protected static final AxisAlignedBB d = new AxisAlignedBB(0.0D, 0.0D, 0.0625D, 0.9375D, 0.875D, 0.9375D);
   protected static final AxisAlignedBB e = new AxisAlignedBB(0.0625D, 0.0D, 0.0625D, 1.0D, 0.875D, 0.9375D);
   protected static final AxisAlignedBB f = new AxisAlignedBB(0.0625D, 0.0D, 0.0625D, 0.9375D, 0.875D, 0.9375D);
   public final BlockChest.class_a_in_class_akd g;

   protected BlockChest(BlockChest.class_a_in_class_akd var1) {
      super(Material.d);
      this.w(this.A.b().set(a, EnumDirection.NORTH));
      this.g = var1;
      this.a(var1 == BlockChest.class_a_in_class_akd.TRAP?CreativeModeTab.d:CreativeModeTab.c);
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.ENTITYBLOCK_ANIMATED;
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return var2.getType(var3.c()).getBlock() == this?b:(var2.getType(var3.d()).getBlock() == this?c:(var2.getType(var3.e()).getBlock() == this?d:(var2.getType(var3.f()).getBlock() == this?e:f)));
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      this.e(var1, var2, var3);
      Iterator var4 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

      while(var4.hasNext()) {
         EnumDirection var5 = (EnumDirection)var4.next();
         BlockPosition var6 = var2.a(var5);
         IBlockData var7 = var1.getType(var6);
         if(var7.getBlock() == this) {
            this.e(var1, var6, var7);
         }
      }

   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return this.u().set(a, var8.bh());
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, class_rz var4, ItemStack var5) {
      EnumDirection var6 = EnumDirection.b(MathHelper.c((double)(var4.yaw * 4.0F / 360.0F) + 0.5D) & 3).d();
      var3 = var3.set(a, var6);
      BlockPosition var7 = var2.c();
      BlockPosition var8 = var2.d();
      BlockPosition var9 = var2.e();
      BlockPosition var10 = var2.f();
      boolean var11 = this == var1.getType(var7).getBlock();
      boolean var12 = this == var1.getType(var8).getBlock();
      boolean var13 = this == var1.getType(var9).getBlock();
      boolean var14 = this == var1.getType(var10).getBlock();
      if(!var11 && !var12 && !var13 && !var14) {
         var1.a((BlockPosition)var2, (IBlockData)var3, 3);
      } else if(var6.k() == EnumDirection.class_a_in_class_cq.X && (var11 || var12)) {
         if(var11) {
            var1.a((BlockPosition)var7, (IBlockData)var3, 3);
         } else {
            var1.a((BlockPosition)var8, (IBlockData)var3, 3);
         }

         var1.a((BlockPosition)var2, (IBlockData)var3, 3);
      } else if(var6.k() == EnumDirection.class_a_in_class_cq.Z && (var13 || var14)) {
         if(var13) {
            var1.a((BlockPosition)var9, (IBlockData)var3, 3);
         } else {
            var1.a((BlockPosition)var10, (IBlockData)var3, 3);
         }

         var1.a((BlockPosition)var2, (IBlockData)var3, 3);
      }

      if(var5.s()) {
         TileEntity var15 = var1.r(var2);
         if(var15 instanceof class_apw) {
            ((class_apw)var15).a(var5.q());
         }
      }

   }

   public IBlockData e(World var1, BlockPosition var2, IBlockData var3) {
      if(var1.E) {
         return var3;
      } else {
         IBlockData var4 = var1.getType(var2.c());
         IBlockData var5 = var1.getType(var2.d());
         IBlockData var6 = var1.getType(var2.e());
         IBlockData var7 = var1.getType(var2.f());
         EnumDirection var8 = (EnumDirection)var3.get(a);
         if(var4.getBlock() != this && var5.getBlock() != this) {
            boolean var15 = var4.b();
            boolean var16 = var5.b();
            if(var6.getBlock() == this || var7.getBlock() == this) {
               BlockPosition var17 = var6.getBlock() == this?var2.e():var2.f();
               IBlockData var18 = var1.getType(var17.c());
               IBlockData var13 = var1.getType(var17.d());
               var8 = EnumDirection.SOUTH;
               EnumDirection var14;
               if(var6.getBlock() == this) {
                  var14 = (EnumDirection)var6.get(a);
               } else {
                  var14 = (EnumDirection)var7.get(a);
               }

               if(var14 == EnumDirection.NORTH) {
                  var8 = EnumDirection.NORTH;
               }

               if((var15 || var18.b()) && !var16 && !var13.b()) {
                  var8 = EnumDirection.SOUTH;
               }

               if((var16 || var13.b()) && !var15 && !var18.b()) {
                  var8 = EnumDirection.NORTH;
               }
            }
         } else {
            BlockPosition var9 = var4.getBlock() == this?var2.c():var2.d();
            IBlockData var10 = var1.getType(var9.e());
            IBlockData var11 = var1.getType(var9.f());
            var8 = EnumDirection.EAST;
            EnumDirection var12;
            if(var4.getBlock() == this) {
               var12 = (EnumDirection)var4.get(a);
            } else {
               var12 = (EnumDirection)var5.get(a);
            }

            if(var12 == EnumDirection.WEST) {
               var8 = EnumDirection.WEST;
            }

            if((var6.b() || var10.b()) && !var7.b() && !var11.b()) {
               var8 = EnumDirection.EAST;
            }

            if((var7.b() || var11.b()) && !var6.b() && !var10.b()) {
               var8 = EnumDirection.WEST;
            }
         }

         var3 = var3.set(a, var8);
         var1.a((BlockPosition)var2, (IBlockData)var3, 3);
         return var3;
      }
   }

   public IBlockData f(World var1, BlockPosition var2, IBlockData var3) {
      EnumDirection var4 = null;
      Iterator var5 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

      while(var5.hasNext()) {
         EnumDirection var6 = (EnumDirection)var5.next();
         IBlockData var7 = var1.getType(var2.a(var6));
         if(var7.getBlock() == this) {
            return var3;
         }

         if(var7.b()) {
            if(var4 != null) {
               var4 = null;
               break;
            }

            var4 = var6;
         }
      }

      if(var4 != null) {
         return var3.set(a, var4.d());
      } else {
         EnumDirection var8 = (EnumDirection)var3.get(a);
         if(var1.getType(var2.a(var8)).b()) {
            var8 = var8.d();
         }

         if(var1.getType(var2.a(var8)).b()) {
            var8 = var8.e();
         }

         if(var1.getType(var2.a(var8)).b()) {
            var8 = var8.d();
         }

         return var3.set(a, var8);
      }
   }

   public boolean a(World var1, BlockPosition var2) {
      int var3 = 0;
      BlockPosition var4 = var2.e();
      BlockPosition var5 = var2.f();
      BlockPosition var6 = var2.c();
      BlockPosition var7 = var2.d();
      if(var1.getType(var4).getBlock() == this) {
         if(this.d(var1, var4)) {
            return false;
         }

         ++var3;
      }

      if(var1.getType(var5).getBlock() == this) {
         if(this.d(var1, var5)) {
            return false;
         }

         ++var3;
      }

      if(var1.getType(var6).getBlock() == this) {
         if(this.d(var1, var6)) {
            return false;
         }

         ++var3;
      }

      if(var1.getType(var7).getBlock() == this) {
         if(this.d(var1, var7)) {
            return false;
         }

         ++var3;
      }

      return var3 <= 1;
   }

   private boolean d(World var1, BlockPosition var2) {
      if(var1.getType(var2).getBlock() != this) {
         return false;
      } else {
         Iterator var3 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

         EnumDirection var4;
         do {
            if(!var3.hasNext()) {
               return false;
            }

            var4 = (EnumDirection)var3.next();
         } while(var1.getType(var2.a(var4)).getBlock() != this);

         return true;
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      super.a(var1, var2, var3, var4);
      TileEntity var5 = var1.r(var2);
      if(var5 instanceof class_apw) {
         var5.A();
      }

   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      TileEntity var4 = var1.r(var2);
      if(var4 instanceof IInventory) {
         class_qi.a(var1, var2, (IInventory)var4);
         var1.f(var2, this);
      }

      super.b(var1, var2, var3);
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(var1.E) {
         return true;
      } else {
         class_qr var11 = this.c(var1, var2);
         if(var11 != null) {
            var4.openContainer((IInventory)var11);
            if(this.g == BlockChest.class_a_in_class_akd.BASIC) {
               var4.b(StatisticList.ac);
            } else if(this.g == BlockChest.class_a_in_class_akd.TRAP) {
               var4.b(StatisticList.W);
            }
         }

         return true;
      }
   }

   public class_qr c(World var1, BlockPosition var2) {
      TileEntity var3 = var1.r(var2);
      if(!(var3 instanceof class_apw)) {
         return null;
      } else {
         Object var4 = (class_apw)var3;
         if(this.e(var1, var2)) {
            return null;
         } else {
            Iterator var5 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

            while(true) {
               while(true) {
                  EnumDirection var6;
                  TileEntity var9;
                  do {
                     BlockPosition var7;
                     Block var8;
                     do {
                        if(!var5.hasNext()) {
                           return (class_qr)var4;
                        }

                        var6 = (EnumDirection)var5.next();
                        var7 = var2.a(var6);
                        var8 = var1.getType(var7).getBlock();
                     } while(var8 != this);

                     if(this.e(var1, var7)) {
                        return null;
                     }

                     var9 = var1.r(var7);
                  } while(!(var9 instanceof class_apw));

                  if(var6 != EnumDirection.WEST && var6 != EnumDirection.NORTH) {
                     var4 = new class_qe("container.chestDouble", (class_qr)var4, (class_apw)var9);
                  } else {
                     var4 = new class_qe("container.chestDouble", (class_apw)var9, (class_qr)var4);
                  }
               }
            }
         }
      }
   }

   public TileEntity a(World var1, int var2) {
      return new class_apw();
   }

   public boolean g(IBlockData var1) {
      return this.g == BlockChest.class_a_in_class_akd.TRAP;
   }

   public int b(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      if(!var1.m()) {
         return 0;
      } else {
         int var5 = 0;
         TileEntity var6 = var2.r(var3);
         if(var6 instanceof class_apw) {
            var5 = ((class_apw)var6).l;
         }

         return MathHelper.a(var5, 0, 15);
      }
   }

   public int c(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return var4 == EnumDirection.UP?var1.a(var2, var3, var4):0;
   }

   private boolean e(World var1, BlockPosition var2) {
      return this.i(var1, var2) || this.j(var1, var2);
   }

   private boolean i(World var1, BlockPosition var2) {
      return var1.getType(var2.a()).l();
   }

   private boolean j(World var1, BlockPosition var2) {
      Iterator var3 = var1.a(class_wa.class, new AxisAlignedBB((double)var2.p(), (double)(var2.q() + 1), (double)var2.r(), (double)(var2.p() + 1), (double)(var2.q() + 2), (double)(var2.r() + 1))).iterator();

      class_wa var5;
      do {
         if(!var3.hasNext()) {
            return false;
         }

         Entity var4 = (Entity)var3.next();
         var5 = (class_wa)var4;
      } while(!var5.db());

      return true;
   }

   public boolean v(IBlockData var1) {
      return true;
   }

   public int d(IBlockData var1, World var2, BlockPosition var3) {
      return Container.b((IInventory)this.c(var2, var3));
   }

   public IBlockData a(int var1) {
      EnumDirection var2 = EnumDirection.a(var1);
      if(var2.k() == EnumDirection.class_a_in_class_cq.Y) {
         var2 = EnumDirection.NORTH;
      }

      return this.u().set(a, var2);
   }

   public int e(IBlockData var1) {
      return ((EnumDirection)var1.get(a)).a();
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(a, var2.a((EnumDirection)var1.get(a)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(a)));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   public static enum class_a_in_class_akd {
      BASIC,
      TRAP;
   }
}
