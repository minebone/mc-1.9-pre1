package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EnumColor;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;

public class class_ang extends Block {
   public class_ang() {
      this(Material.e.r());
   }

   public class_ang(MaterialMapColor var1) {
      super(Material.e, var1);
      this.a(CreativeModeTab.b);
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return this == Blocks.q?Items.j:(this == Blocks.ag?Items.k:(this == Blocks.x?Items.bd:(this == Blocks.bP?Items.bY:(this == Blocks.co?Items.cq:Item.a((Block)this)))));
   }

   public int a(Random var1) {
      return this == Blocks.x?4 + var1.nextInt(5):1;
   }

   public int a(int var1, Random var2) {
      if(var1 > 0 && Item.a((Block)this) != this.a((IBlockData)this.t().a().iterator().next(), var2, var1)) {
         int var3 = var2.nextInt(var1 + 2) - 1;
         if(var3 < 0) {
            var3 = 0;
         }

         return this.a(var2) * (var3 + 1);
      } else {
         return this.a(var2);
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, float var4, int var5) {
      super.a(var1, var2, var3, var4, var5);
      if(this.a(var3, var1.r, var5) != Item.a((Block)this)) {
         int var6 = 0;
         if(this == Blocks.q) {
            var6 = MathHelper.a((Random)var1.r, 0, 2);
         } else if(this == Blocks.ag) {
            var6 = MathHelper.a((Random)var1.r, 3, 7);
         } else if(this == Blocks.bP) {
            var6 = MathHelper.a((Random)var1.r, 3, 7);
         } else if(this == Blocks.x) {
            var6 = MathHelper.a((Random)var1.r, 2, 5);
         } else if(this == Blocks.co) {
            var6 = MathHelper.a((Random)var1.r, 2, 5);
         }

         this.b(var1, var2, var6);
      }

   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(this);
   }

   public int d(IBlockData var1) {
      return this == Blocks.x?EnumColor.BLUE.b():0;
   }
}
