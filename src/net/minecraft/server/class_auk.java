package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_auc;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;

public class class_auk extends class_auc {
   public boolean b(World var1, Random var2, BlockPosition var3) {
      while(var1.d(var3) && var3.q() > 2) {
         var3 = var3.b();
      }

      if(var1.getType(var3).getBlock() != Blocks.aJ) {
         return false;
      } else {
         var3 = var3.b(var2.nextInt(4));
         int var4 = var2.nextInt(4) + 7;
         int var5 = var4 / 4 + var2.nextInt(2);
         if(var5 > 1 && var2.nextInt(60) == 0) {
            var3 = var3.b(10 + var2.nextInt(30));
         }

         int var6;
         int var8;
         for(var6 = 0; var6 < var4; ++var6) {
            float var7 = (1.0F - (float)var6 / (float)var4) * (float)var5;
            var8 = MathHelper.f(var7);

            for(int var9 = -var8; var9 <= var8; ++var9) {
               float var10 = (float)MathHelper.a(var9) - 0.25F;

               for(int var11 = -var8; var11 <= var8; ++var11) {
                  float var12 = (float)MathHelper.a(var11) - 0.25F;
                  if((var9 == 0 && var11 == 0 || var10 * var10 + var12 * var12 <= var7 * var7) && (var9 != -var8 && var9 != var8 && var11 != -var8 && var11 != var8 || var2.nextFloat() <= 0.75F)) {
                     IBlockData var13 = var1.getType(var3.a(var9, var6, var11));
                     Block var14 = var13.getBlock();
                     if(var13.getMaterial() == Material.a || var14 == Blocks.d || var14 == Blocks.aJ || var14 == Blocks.aI) {
                        this.a(var1, var3.a(var9, var6, var11), Blocks.cB.u());
                     }

                     if(var6 != 0 && var8 > 1) {
                        var13 = var1.getType(var3.a(var9, -var6, var11));
                        var14 = var13.getBlock();
                        if(var13.getMaterial() == Material.a || var14 == Blocks.d || var14 == Blocks.aJ || var14 == Blocks.aI) {
                           this.a(var1, var3.a(var9, -var6, var11), Blocks.cB.u());
                        }
                     }
                  }
               }
            }
         }

         var6 = var5 - 1;
         if(var6 < 0) {
            var6 = 0;
         } else if(var6 > 1) {
            var6 = 1;
         }

         for(int var15 = -var6; var15 <= var6; ++var15) {
            for(var8 = -var6; var8 <= var6; ++var8) {
               BlockPosition var16 = var3.a(var15, -1, var8);
               int var17 = 50;
               if(Math.abs(var15) == 1 && Math.abs(var8) == 1) {
                  var17 = var2.nextInt(5);
               }

               while(var16.q() > 50) {
                  IBlockData var18 = var1.getType(var16);
                  Block var19 = var18.getBlock();
                  if(var18.getMaterial() != Material.a && var19 != Blocks.d && var19 != Blocks.aJ && var19 != Blocks.aI && var19 != Blocks.cB) {
                     break;
                  }

                  this.a(var1, var16, Blocks.cB.u());
                  var16 = var16.b();
                  --var17;
                  if(var17 <= 0) {
                     var16 = var16.c(var2.nextInt(5) + 1);
                     var17 = var2.nextInt(5);
                  }
               }
            }
         }

         return true;
      }
   }
}
