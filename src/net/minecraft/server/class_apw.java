package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Container;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.TileEntity;
import net.minecraft.server.class_aba;
import net.minecraft.server.ItemStack;
import net.minecraft.server.BlockChest;
import net.minecraft.server.class_aql;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_ky;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.class_qe;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_qg;

public class class_apw extends class_aql implements class_ky, IInventory {
   private ItemStack[] o = new ItemStack[27];
   public boolean a;
   public class_apw f;
   public class_apw g;
   public class_apw h;
   public class_apw i;
   public float j;
   public float k;
   public int l;
   private int p;
   private BlockChest.class_a_in_class_akd q;
   private String r;

   public class_apw() {
   }

   public class_apw(BlockChest.class_a_in_class_akd var1) {
      this.q = var1;
   }

   public int u_() {
      return 27;
   }

   public ItemStack a(int var1) {
      this.d((EntityHuman)null);
      return this.o[var1];
   }

   public ItemStack a(int var1, int var2) {
      this.d((EntityHuman)null);
      ItemStack var3 = class_qg.a(this.o, var1, var2);
      if(var3 != null) {
         this.v_();
      }

      return var3;
   }

   public ItemStack b(int var1) {
      this.d((EntityHuman)null);
      return class_qg.a(this.o, var1);
   }

   public void a(int var1, ItemStack var2) {
      this.d((EntityHuman)null);
      this.o[var1] = var2;
      if(var2 != null && var2.b > this.w_()) {
         var2.b = this.w_();
      }

      this.v_();
   }

   public String h_() {
      return this.o_()?this.r:"container.chest";
   }

   public boolean o_() {
      return this.r != null && !this.r.isEmpty();
   }

   public void a(String var1) {
      this.r = var1;
   }

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      this.o = new ItemStack[this.u_()];
      if(var2.b("CustomName", 8)) {
         this.r = var2.l("CustomName");
      }

      if(!this.b((NBTTagCompound)var2)) {
         NBTTagList var3 = var2.c("Items", 10);

         for(int var4 = 0; var4 < var3.c(); ++var4) {
            NBTTagCompound var5 = var3.b(var4);
            int var6 = var5.f("Slot") & 255;
            if(var6 >= 0 && var6 < this.o.length) {
               this.o[var6] = ItemStack.a(var5);
            }
         }
      }

   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      if(!this.c(var1)) {
         NBTTagList var2 = new NBTTagList();

         for(int var3 = 0; var3 < this.o.length; ++var3) {
            if(this.o[var3] != null) {
               NBTTagCompound var4 = new NBTTagCompound();
               var4.a("Slot", (byte)var3);
               this.o[var3].b(var4);
               var2.a((NBTTag)var4);
            }
         }

         var1.a((String)"Items", (NBTTag)var2);
      }

      if(this.o_()) {
         var1.a("CustomName", this.r);
      }

   }

   public int w_() {
      return 64;
   }

   public boolean a(EntityHuman var1) {
      return this.b.r(this.c) != this?false:var1.e((double)this.c.p() + 0.5D, (double)this.c.q() + 0.5D, (double)this.c.r() + 0.5D) <= 64.0D;
   }

   public void A() {
      super.A();
      this.a = false;
   }

   private void a(class_apw var1, EnumDirection var2) {
      if(var1.x()) {
         this.a = false;
      } else if(this.a) {
         switch(class_apw.SyntheticClass_1.a[var2.ordinal()]) {
         case 1:
            if(this.f != var1) {
               this.a = false;
            }
            break;
         case 2:
            if(this.i != var1) {
               this.a = false;
            }
            break;
         case 3:
            if(this.g != var1) {
               this.a = false;
            }
            break;
         case 4:
            if(this.h != var1) {
               this.a = false;
            }
         }
      }

   }

   public void m() {
      if(!this.a) {
         this.a = true;
         this.h = this.a(EnumDirection.WEST);
         this.g = this.a(EnumDirection.EAST);
         this.f = this.a(EnumDirection.NORTH);
         this.i = this.a(EnumDirection.SOUTH);
      }
   }

   protected class_apw a(EnumDirection var1) {
      BlockPosition var2 = this.c.a(var1);
      if(this.b(var2)) {
         TileEntity var3 = this.b.r(var2);
         if(var3 instanceof class_apw) {
            class_apw var4 = (class_apw)var3;
            var4.a(this, var1.d());
            return var4;
         }
      }

      return null;
   }

   private boolean b(BlockPosition var1) {
      if(this.b == null) {
         return false;
      } else {
         Block var2 = this.b.getType(var1).getBlock();
         return var2 instanceof BlockChest && ((BlockChest)var2).g == this.o();
      }
   }

   public void c() {
      this.m();
      int var1 = this.c.p();
      int var2 = this.c.q();
      int var3 = this.c.r();
      ++this.p;
      float var4;
      if(!this.b.E && this.l != 0 && (this.p + var1 + var2 + var3) % 200 == 0) {
         this.l = 0;
         var4 = 5.0F;
         List var5 = this.b.a(EntityHuman.class, new AxisAlignedBB((double)((float)var1 - var4), (double)((float)var2 - var4), (double)((float)var3 - var4), (double)((float)(var1 + 1) + var4), (double)((float)(var2 + 1) + var4), (double)((float)(var3 + 1) + var4)));
         Iterator var6 = var5.iterator();

         label93:
         while(true) {
            IInventory var8;
            do {
               EntityHuman var7;
               do {
                  if(!var6.hasNext()) {
                     break label93;
                  }

                  var7 = (EntityHuman)var6.next();
               } while(!(var7.bt instanceof class_aba));

               var8 = ((class_aba)var7.bt).e();
            } while(var8 != this && (!(var8 instanceof class_qe) || !((class_qe)var8).a((IInventory)this)));

            ++this.l;
         }
      }

      this.k = this.j;
      var4 = 0.1F;
      double var14;
      if(this.l > 0 && this.j == 0.0F && this.f == null && this.h == null) {
         double var11 = (double)var1 + 0.5D;
         var14 = (double)var3 + 0.5D;
         if(this.i != null) {
            var14 += 0.5D;
         }

         if(this.g != null) {
            var11 += 0.5D;
         }

         this.b.a((EntityHuman)null, var11, (double)var2 + 0.5D, var14, class_ng.V, EnumSoundCategory.BLOCKS, 0.5F, this.b.r.nextFloat() * 0.1F + 0.9F);
      }

      if(this.l == 0 && this.j > 0.0F || this.l > 0 && this.j < 1.0F) {
         float var12 = this.j;
         if(this.l > 0) {
            this.j += var4;
         } else {
            this.j -= var4;
         }

         if(this.j > 1.0F) {
            this.j = 1.0F;
         }

         float var13 = 0.5F;
         if(this.j < var13 && var12 >= var13 && this.f == null && this.h == null) {
            var14 = (double)var1 + 0.5D;
            double var9 = (double)var3 + 0.5D;
            if(this.i != null) {
               var9 += 0.5D;
            }

            if(this.g != null) {
               var14 += 0.5D;
            }

            this.b.a((EntityHuman)null, var14, (double)var2 + 0.5D, var9, class_ng.T, EnumSoundCategory.BLOCKS, 0.5F, this.b.r.nextFloat() * 0.1F + 0.9F);
         }

         if(this.j < 0.0F) {
            this.j = 0.0F;
         }
      }

   }

   public boolean c(int var1, int var2) {
      if(var1 == 1) {
         this.l = var2;
         return true;
      } else {
         return super.c(var1, var2);
      }
   }

   public void b(EntityHuman var1) {
      if(!var1.y()) {
         if(this.l < 0) {
            this.l = 0;
         }

         ++this.l;
         this.b.c(this.c, this.w(), 1, this.l);
         this.b.d(this.c, this.w());
         this.b.d(this.c.b(), this.w());
      }

   }

   public void c(EntityHuman var1) {
      if(!var1.y() && this.w() instanceof BlockChest) {
         --this.l;
         this.b.c(this.c, this.w(), 1, this.l);
         this.b.d(this.c, this.w());
         this.b.d(this.c.b(), this.w());
      }

   }

   public boolean b(int var1, ItemStack var2) {
      return true;
   }

   public void y() {
      super.y();
      this.A();
      this.m();
   }

   public BlockChest.class_a_in_class_akd o() {
      if(this.q == null) {
         if(this.b == null || !(this.w() instanceof BlockChest)) {
            return BlockChest.class_a_in_class_akd.BASIC;
         }

         this.q = ((BlockChest)this.w()).g;
      }

      return this.q;
   }

   public String k() {
      return "minecraft:chest";
   }

   public Container a(PlayerInventory var1, EntityHuman var2) {
      this.d(var2);
      return new class_aba(var1, this, var2);
   }

   public int c_(int var1) {
      return 0;
   }

   public void b(int var1, int var2) {
   }

   public int g() {
      return 0;
   }

   public void l() {
      this.d((EntityHuman)null);

      for(int var1 = 0; var1 < this.o.length; ++var1) {
         this.o[var1] = null;
      }

   }

   public void a(class_kk var1, long var2) {
      this.m = var1;
      this.n = var2;
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumDirection.values().length];

      static {
         try {
            a[EnumDirection.NORTH.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.SOUTH.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.EAST.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
