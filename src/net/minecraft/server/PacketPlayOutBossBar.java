package net.minecraft.server;

import java.io.IOException;
import java.util.UUID;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketListenerPlayOut;
import net.minecraft.server.BossBar;

/*TODO: new packet*/
public class PacketPlayOutBossBar implements Packet {
	
   private UUID a;
   private PacketPlayOutBossBar.class_a_in_class_fv b;
   private IChatBaseComponent c;
   private float d;
   private BossBar.EnumBossBarColor e;
   private BossBar.EnumBossBarType f;
   private boolean g;
   private boolean h;
   private boolean i;

   public PacketPlayOutBossBar() {
   }

   public PacketPlayOutBossBar(PacketPlayOutBossBar.class_a_in_class_fv var1, BossBar var2) {
      this.b = var1;
      this.a = var2.d();
      this.c = var2.e();
      this.d = var2.f();
      this.e = var2.g();
      this.f = var2.h();
      this.g = var2.i();
      this.h = var2.j();
      this.i = var2.k();
   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.i();
      this.b = (PacketPlayOutBossBar.class_a_in_class_fv)var1.a(PacketPlayOutBossBar.class_a_in_class_fv.class);
      switch(PacketPlayOutBossBar.SyntheticClass_1.a[this.b.ordinal()]) {
      case 1:
         this.c = var1.f();
         this.d = var1.readFloat();
         this.e = (BossBar.EnumBossBarColor)var1.a(BossBar.EnumBossBarColor.class);
         this.f = (BossBar.EnumBossBarType)var1.a(BossBar.EnumBossBarType.class);
         this.a(var1.readUnsignedByte());
      case 2:
      default:
         break;
      case 3:
         this.d = var1.readFloat();
         break;
      case 4:
         this.c = var1.f();
         break;
      case 5:
         this.e = (BossBar.EnumBossBarColor)var1.a(BossBar.EnumBossBarColor.class);
         this.f = (BossBar.EnumBossBarType)var1.a(BossBar.EnumBossBarType.class);
         break;
      case 6:
         this.a(var1.readUnsignedByte());
      }

   }

   private void a(int var1) {
      this.g = (var1 & 1) > 0;
      this.h = (var1 & 2) > 0;
      this.i = (var1 & 2) > 0;
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.a(this.a);
      var1.a((Enum)this.b);
      switch(PacketPlayOutBossBar.SyntheticClass_1.a[this.b.ordinal()]) {
      case 1:
         var1.a(this.c);
         var1.writeFloat(this.d);
         var1.a((Enum)this.e);
         var1.a((Enum)this.f);
         var1.writeByte(this.j());
      case 2:
      default:
         break;
      case 3:
         var1.writeFloat(this.d);
         break;
      case 4:
         var1.a(this.c);
         break;
      case 5:
         var1.a((Enum)this.e);
         var1.a((Enum)this.f);
         break;
      case 6:
         var1.writeByte(this.j());
      }

   }

   private int j() {
      int var1 = 0;
      if(this.g) {
         var1 |= 1;
      }

      if(this.h) {
         var1 |= 2;
      }

      if(this.i) {
         var1 |= 2;
      }

      return var1;
   }

   public void a(PacketListenerPlayOut var1) {
      var1.a(this);
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketListenerPlayOut)var1);
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[PacketPlayOutBossBar.class_a_in_class_fv.values().length];

      static {
         try {
            a[PacketPlayOutBossBar.class_a_in_class_fv.ADD.ordinal()] = 1;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[PacketPlayOutBossBar.class_a_in_class_fv.REMOVE.ordinal()] = 2;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[PacketPlayOutBossBar.class_a_in_class_fv.UPDATE_PCT.ordinal()] = 3;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[PacketPlayOutBossBar.class_a_in_class_fv.UPDATE_NAME.ordinal()] = 4;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[PacketPlayOutBossBar.class_a_in_class_fv.UPDATE_STYLE.ordinal()] = 5;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[PacketPlayOutBossBar.class_a_in_class_fv.UPDATE_PROPERTIES.ordinal()] = 6;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum class_a_in_class_fv {
      ADD,
      REMOVE,
      UPDATE_PCT,
      UPDATE_NAME,
      UPDATE_STYLE,
      UPDATE_PROPERTIES;
   }
}
