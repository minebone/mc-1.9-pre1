package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_akr;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.class_axg;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_rz;

public class class_ald extends class_akr {
   protected static final AxisAlignedBB a = new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 1.0D, 0.625D);
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.375D, 0.375D, 0.0D, 0.625D, 0.625D, 1.0D);
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.0D, 0.375D, 0.375D, 1.0D, 0.625D, 0.625D);

   protected class_ald() {
      super(Material.q);
      this.w(this.A.b().set(H, EnumDirection.UP));
      this.a(CreativeModeTab.c);
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(H, var2.a((EnumDirection)var1.get(H)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.set(H, var2.b((EnumDirection)var1.get(H)));
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      switch(class_ald.SyntheticClass_1.a[((EnumDirection)var1.get(H)).k().ordinal()]) {
      case 1:
      default:
         return c;
      case 2:
         return b;
      case 3:
         return a;
      }
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean a(World var1, BlockPosition var2) {
      return true;
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      IBlockData var9 = var1.getType(var2.a(var3.d()));
      if(var9.getBlock() == Blocks.cQ) {
         EnumDirection var10 = (EnumDirection)var9.get(H);
         if(var10 == var3) {
            return this.u().set(H, var3.d());
         }
      }

      return this.u().set(H, var3);
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
   }

   public IBlockData a(int var1) {
      IBlockData var2 = this.u();
      var2 = var2.set(H, EnumDirection.a(var1));
      return var2;
   }

   public int e(IBlockData var1) {
      return ((EnumDirection)var1.get(H)).a();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{H});
   }

   public class_axg h(IBlockData var1) {
      return class_axg.NORMAL;
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumDirection.class_a_in_class_cq.values().length];

      static {
         try {
            a[EnumDirection.class_a_in_class_cq.X.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.class_a_in_class_cq.Z.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.class_a_in_class_cq.Y.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
