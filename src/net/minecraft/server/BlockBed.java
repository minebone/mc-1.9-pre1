package net.minecraft.server;

import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_amf;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.class_axg;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_or;
import net.minecraft.server.EnumHand;

public class BlockBed extends class_amf {
   public static final BlockStateEnum a = BlockStateEnum.a("part", BlockBed.EnumBedPart.class);
   public static final class_arm b = class_arm.a("occupied");
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.5625D, 1.0D);

   public BlockBed() {
      super(Material.n);
      this.w(this.A.b().set(a, BlockBed.EnumBedPart.FOOT).set(b, Boolean.valueOf(false)));
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(var1.E) {
         return true;
      } else {
         if(var3.get(a) != BlockBed.EnumBedPart.HEAD) {
            var2 = var2.a((EnumDirection)var3.get(D));
            var3 = var1.getType(var2);
            if(var3.getBlock() != this) {
               return true;
            }
         }

         if(var1.s.e() && var1.b(var2) != class_aik.j) {
            if(((Boolean)var3.get(b)).booleanValue()) {
               EntityHuman var12 = this.c(var1, var2);
               if(var12 != null) {
                  var4.b((IChatBaseComponent)(new ChatMessage("tile.bed.occupied", new Object[0])));
                  return true;
               }

               var3 = var3.set(b, Boolean.valueOf(false));
               var1.a((BlockPosition)var2, (IBlockData)var3, 4);
            }

            EntityHuman.EnumBedResult var13 = var4.a(var2);
            if(var13 == EntityHuman.EnumBedResult.OK) {
               var3 = var3.set(b, Boolean.valueOf(true));
               var1.a((BlockPosition)var2, (IBlockData)var3, 4);
               return true;
            } else {
               if(var13 == EntityHuman.EnumBedResult.NOT_POSSIBLE_NOW) {
                  var4.b((IChatBaseComponent)(new ChatMessage("tile.bed.noSleep", new Object[0])));
               } else if(var13 == EntityHuman.EnumBedResult.NOT_SAFE) {
                  var4.b((IChatBaseComponent)(new ChatMessage("tile.bed.notSafe", new Object[0])));
               }

               return true;
            }
         } else {
            var1.g(var2);
            BlockPosition var11 = var2.a(((EnumDirection)var3.get(D)).d());
            if(var1.getType(var11).getBlock() == this) {
               var1.g(var11);
            }

            var1.a((Entity)null, (double)var2.p() + 0.5D, (double)var2.q() + 0.5D, (double)var2.r() + 0.5D, 5.0F, true, true);
            return true;
         }
      }
   }

   private EntityHuman c(World var1, BlockPosition var2) {
      Iterator var3 = var1.i.iterator();

      EntityHuman var4;
      do {
         if(!var3.hasNext()) {
            return null;
         }

         var4 = (EntityHuman)var3.next();
      } while(!var4.ck() || !var4.bG.equals(var2));

      return var4;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      EnumDirection var5 = (EnumDirection)var3.get(D);
      if(var3.get(a) == BlockBed.EnumBedPart.HEAD) {
         if(var1.getType(var2.a(var5.d())).getBlock() != this) {
            var1.g(var2);
         }
      } else if(var1.getType(var2.a(var5)).getBlock() != this) {
         var1.g(var2);
         if(!var1.E) {
            this.b(var1, var2, var3, 0);
         }
      }

   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return var1.get(a) == BlockBed.EnumBedPart.HEAD?null:Items.bh;
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return c;
   }

   public static BlockPosition a(World var0, BlockPosition var1, int var2) {
      EnumDirection var3 = (EnumDirection)var0.getType(var1).get(D);
      int var4 = var1.p();
      int var5 = var1.q();
      int var6 = var1.r();

      for(int var7 = 0; var7 <= 1; ++var7) {
         int var8 = var4 - var3.g() * var7 - 1;
         int var9 = var6 - var3.i() * var7 - 1;
         int var10 = var8 + 2;
         int var11 = var9 + 2;

         for(int var12 = var8; var12 <= var10; ++var12) {
            for(int var13 = var9; var13 <= var11; ++var13) {
               BlockPosition var14 = new BlockPosition(var12, var5, var13);
               if(b(var0, var14)) {
                  if(var2 <= 0) {
                     return var14;
                  }

                  --var2;
               }
            }
         }
      }

      return null;
   }

   protected static boolean b(World var0, BlockPosition var1) {
      return var0.getType(var1.b()).q() && !var0.getType(var1).getMaterial().a() && !var0.getType(var1.a()).getMaterial().a();
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, float var4, int var5) {
      if(var3.get(a) == BlockBed.EnumBedPart.FOOT) {
         super.a(var1, var2, var3, var4, 0);
      }

   }

   public class_axg h(IBlockData var1) {
      return class_axg.DESTROY;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Items.bh);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4) {
      if(var4.abilities.d && var3.get(a) == BlockBed.EnumBedPart.HEAD) {
         BlockPosition var5 = var2.a(((EnumDirection)var3.get(D)).d());
         if(var1.getType(var5).getBlock() == this) {
            var1.g(var5);
         }
      }

   }

   public IBlockData a(int var1) {
      EnumDirection var2 = EnumDirection.b(var1);
      return (var1 & 8) > 0?this.u().set(a, BlockBed.EnumBedPart.HEAD).set(D, var2).set(b, Boolean.valueOf((var1 & 4) > 0)):this.u().set(a, BlockBed.EnumBedPart.FOOT).set(D, var2);
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      if(var1.get(a) == BlockBed.EnumBedPart.FOOT) {
         IBlockData var4 = var2.getType(var3.a((EnumDirection)var1.get(D)));
         if(var4.getBlock() == this) {
            var1 = var1.set(b, var4.get(b));
         }
      }

      return var1;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(D, var2.a((EnumDirection)var1.get(D)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(D)));
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((EnumDirection)var1.get(D)).b();
      if(var1.get(a) == BlockBed.EnumBedPart.HEAD) {
         var3 |= 8;
         if(((Boolean)var1.get(b)).booleanValue()) {
            var3 |= 4;
         }
      }

      return var3;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{D, a, b});
   }

   public static enum EnumBedPart implements class_or {
      HEAD("head"),
      FOOT("foot");

      private final String c;

      private EnumBedPart(String var3) {
         this.c = var3;
      }

      public String toString() {
         return this.c;
      }

      public String m() {
         return this.c;
      }
   }
}
