package net.minecraft.server;

import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MerchantRecipe;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahe;
import net.minecraft.server.class_ahg;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_qg;

public class InventoryMerchant implements IInventory {
   private final class_ahe a;
   private ItemStack[] b = new ItemStack[3];
   private final EntityHuman c;
   private MerchantRecipe d;
   private int e;

   public InventoryMerchant(EntityHuman var1, class_ahe var2) {
      this.c = var1;
      this.a = var2;
   }

   public int u_() {
      return this.b.length;
   }

   public ItemStack a(int var1) {
      return this.b[var1];
   }

   public ItemStack a(int var1, int var2) {
      if(var1 == 2 && this.b[var1] != null) {
         return class_qg.a(this.b, var1, this.b[var1].b);
      } else {
         ItemStack var3 = class_qg.a(this.b, var1, var2);
         if(var3 != null && this.e(var1)) {
            this.h();
         }

         return var3;
      }
   }

   private boolean e(int var1) {
      return var1 == 0 || var1 == 1;
   }

   public ItemStack b(int var1) {
      return class_qg.a(this.b, var1);
   }

   public void a(int var1, ItemStack var2) {
      this.b[var1] = var2;
      if(var2 != null && var2.b > this.w_()) {
         var2.b = this.w_();
      }

      if(this.e(var1)) {
         this.h();
      }

   }

   public String h_() {
      return "mob.villager";
   }

   public boolean o_() {
      return false;
   }

   public IChatBaseComponent i_() {
      return (IChatBaseComponent)(this.o_()?new ChatComponentText(this.h_()):new ChatMessage(this.h_(), new Object[0]));
   }

   public int w_() {
      return 64;
   }

   public boolean a(EntityHuman var1) {
      return this.a.t_() == var1;
   }

   public void b(EntityHuman var1) {
   }

   public void c(EntityHuman var1) {
   }

   public boolean b(int var1, ItemStack var2) {
      return true;
   }

   public void v_() {
      this.h();
   }

   public void h() {
      this.d = null;
      ItemStack var1 = this.b[0];
      ItemStack var2 = this.b[1];
      if(var1 == null) {
         var1 = var2;
         var2 = null;
      }

      if(var1 == null) {
         this.a(2, (ItemStack)null);
      } else {
         class_ahg var3 = this.a.b_(this.c);
         if(var3 != null) {
            MerchantRecipe var4 = var3.a(var1, var2, this.e);
            if(var4 != null && !var4.h()) {
               this.d = var4;
               this.a(2, var4.d().k());
            } else if(var2 != null) {
               var4 = var3.a(var2, var1, this.e);
               if(var4 != null && !var4.h()) {
                  this.d = var4;
                  this.a(2, var4.d().k());
               } else {
                  this.a(2, (ItemStack)null);
               }
            } else {
               this.a(2, (ItemStack)null);
            }
         }
      }

      this.a.a(this.a(2));
   }

   public MerchantRecipe i() {
      return this.d;
   }

   public void d(int var1) {
      this.e = var1;
      this.h();
   }

   public int c_(int var1) {
      return 0;
   }

   public void b(int var1, int var2) {
   }

   public int g() {
      return 0;
   }

   public void l() {
      for(int var1 = 0; var1 < this.b.length; ++var1) {
         this.b[var1] = null;
      }

   }
}
