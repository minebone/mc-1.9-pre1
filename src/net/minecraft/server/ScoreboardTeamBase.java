package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Collection;
import java.util.Map;
import net.minecraft.server.EnumChatFormat;

public abstract class ScoreboardTeamBase {
   public boolean a(ScoreboardTeamBase var1) {
      return var1 == null?false:this == var1;
   }

   public abstract String b();

   public abstract String d(String var1);

   public abstract boolean g();

   public abstract EnumChatFormat m();

   public abstract Collection d();

   public abstract ScoreboardTeamBase.EnumNameTagVisibility j();

   public abstract ScoreboardTeamBase.class_a_in_class_bbq k();

   public static enum class_a_in_class_bbq {
      ALWAYS("always", 0),
      NEVER("never", 1),
      HIDE_FOR_OTHER_TEAMS("pushOtherTeams", 2),
      HIDE_FOR_OWN_TEAM("pushOwnTeam", 3);

      private static Map g = Maps.newHashMap();
      public final String e;
      public final int f;

      public static String[] a() {
         return (String[])g.keySet().toArray(new String[g.size()]);
      }

      public static ScoreboardTeamBase.class_a_in_class_bbq a(String var0) {
         return (ScoreboardTeamBase.class_a_in_class_bbq)g.get(var0);
      }

      private class_a_in_class_bbq(String var3, int var4) {
         this.e = var3;
         this.f = var4;
      }

      static {
         ScoreboardTeamBase.class_a_in_class_bbq[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            ScoreboardTeamBase.class_a_in_class_bbq var3 = var0[var2];
            g.put(var3.e, var3);
         }

      }
   }

   public static enum EnumNameTagVisibility {
      ALWAYS("always", 0),
      NEVER("never", 1),
      HIDE_FOR_OTHER_TEAMS("hideForOtherTeams", 2),
      HIDE_FOR_OWN_TEAM("hideForOwnTeam", 3);

      private static Map g = Maps.newHashMap();
      public final String e;
      public final int f;

      public static String[] a() {
         return (String[])g.keySet().toArray(new String[g.size()]);
      }

      public static ScoreboardTeamBase.EnumNameTagVisibility a(String var0) {
         return (ScoreboardTeamBase.EnumNameTagVisibility)g.get(var0);
      }

      private EnumNameTagVisibility(String var3, int var4) {
         this.e = var3;
         this.f = var4;
      }

      static {
         ScoreboardTeamBase.EnumNameTagVisibility[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            ScoreboardTeamBase.EnumNameTagVisibility var3 = var0[var2];
            g.put(var3.e, var3);
         }

      }
   }
}
