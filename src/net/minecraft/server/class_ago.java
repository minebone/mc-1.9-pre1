package net.minecraft.server;

import net.minecraft.server.Enchantment;
import net.minecraft.server.class_ov;

public class class_ago extends class_ov.class_a_in_class_ov {
   public final Enchantment b;
   public final int c;

   public class_ago(Enchantment var1, int var2) {
      super(var1.c().a());
      this.b = var1;
      this.c = var2;
   }
}
