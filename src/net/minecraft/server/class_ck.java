package net.minecraft.server;

import net.minecraft.server.TileEntity;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_cu;

public interface class_ck extends class_cu {
   double a();

   double b();

   double c();

   BlockPosition d();

   int f();

   TileEntity h();
}
