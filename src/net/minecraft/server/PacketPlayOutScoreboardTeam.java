package net.minecraft.server;

import com.google.common.collect.Lists;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.ScoreboardTeam;
import net.minecraft.server.ScoreboardTeamBase;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketListenerPlayOut;

public class PacketPlayOutScoreboardTeam implements Packet {
   private String a = "";
   private String b = "";
   private String c = "";
   private String d = "";
   private String e;
   private String f;
   private int g;
   private Collection h;
   private int i;
   private int j;

   public PacketPlayOutScoreboardTeam() {
      this.e = ScoreboardTeamBase.EnumNameTagVisibility.ALWAYS.e;
      this.f = ScoreboardTeamBase.class_a_in_class_bbq.ALWAYS.e;
      this.g = -1;
      this.h = Lists.newArrayList();
   }

   public PacketPlayOutScoreboardTeam(ScoreboardTeam var1, int var2) {
      this.e = ScoreboardTeamBase.EnumNameTagVisibility.ALWAYS.e;
      this.f = ScoreboardTeamBase.class_a_in_class_bbq.ALWAYS.e;
      this.g = -1;
      this.h = Lists.newArrayList();
      this.a = var1.b();
      this.i = var2;
      if(var2 == 0 || var2 == 2) {
         this.b = var1.c();
         this.c = var1.e();
         this.d = var1.f();
         this.j = var1.l();
         this.e = var1.i().e;
         this.f = var1.k().e;
         this.g = var1.m().b();
      }

      if(var2 == 0) {
         this.h.addAll(var1.d());
      }

   }

   public PacketPlayOutScoreboardTeam(ScoreboardTeam var1, Collection var2, int var3) {
      this.e = ScoreboardTeamBase.EnumNameTagVisibility.ALWAYS.e;
      this.f = ScoreboardTeamBase.class_a_in_class_bbq.ALWAYS.e;
      this.g = -1;
      this.h = Lists.newArrayList();
      if(var3 != 3 && var3 != 4) {
         throw new IllegalArgumentException("Method must be join or leave for player constructor");
      } else if(var2 != null && !var2.isEmpty()) {
         this.i = var3;
         this.a = var1.b();
         this.h.addAll(var2);
      } else {
         throw new IllegalArgumentException("Players cannot be null/empty");
      }
   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.c(16);
      this.i = var1.readByte();
      if(this.i == 0 || this.i == 2) {
         this.b = var1.c(32);
         this.c = var1.c(16);
         this.d = var1.c(16);
         this.j = var1.readByte();
         this.e = var1.c(32);
         this.f = var1.c(32);
         this.g = var1.readByte();
      }

      if(this.i == 0 || this.i == 3 || this.i == 4) {
         int var2 = var1.readVarInt();

         for(int var3 = 0; var3 < var2; ++var3) {
            this.h.add(var1.c(40));
         }
      }

   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.a(this.a);
      var1.writeByte(this.i);
      if(this.i == 0 || this.i == 2) {
         var1.a(this.b);
         var1.a(this.c);
         var1.a(this.d);
         var1.writeByte(this.j);
         var1.a(this.e);
         var1.a(this.f);
         var1.writeByte(this.g);
      }

      if(this.i == 0 || this.i == 3 || this.i == 4) {
         var1.writeVarInt(this.h.size());
         Iterator var2 = this.h.iterator();

         while(var2.hasNext()) {
            String var3 = (String)var2.next();
            var1.a(var3);
         }
      }

   }

   public void a(PacketListenerPlayOut var1) {
      var1.a(this);
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketListenerPlayOut)var1);
   }
}
