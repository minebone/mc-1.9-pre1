package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.World;
import net.minecraft.server.class_ate;

public class class_aiw extends BiomeBase {
   public class_aiw(BiomeBase.class_a_in_class_aif var1) {
      super(var1);
      this.v.clear();
   }

   public BiomeBase.EnumTemperature h() {
      return BiomeBase.EnumTemperature.OCEAN;
   }

   public void a(World var1, Random var2, class_ate var3, int var4, int var5, double var6) {
      super.a(var1, var2, var3, var4, var5, var6);
   }
}
