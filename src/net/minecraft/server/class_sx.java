package net.minecraft.server;

import net.minecraft.server.EntityInsentient;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ayl;
import net.minecraft.server.class_ayr;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_ys;

public class class_sx {
   protected final EntityInsentient a;
   protected double b;
   protected double c;
   protected double d;
   protected double e;
   protected float f;
   protected float g;
   protected class_sx.class_a_in_class_sx h = class_sx.class_a_in_class_sx.WAIT;

   public class_sx(EntityInsentient var1) {
      this.a = var1;
   }

   public boolean a() {
      return this.h == class_sx.class_a_in_class_sx.MOVE_TO;
   }

   public double b() {
      return this.e;
   }

   public void a(double var1, double var3, double var5, double var7) {
      this.b = var1;
      this.c = var3;
      this.d = var5;
      this.e = var7;
      this.h = class_sx.class_a_in_class_sx.MOVE_TO;
   }

   public void a(float var1, float var2) {
      this.h = class_sx.class_a_in_class_sx.STRAFE;
      this.f = var1;
      this.g = var2;
      this.e = 0.25D;
   }

   public void a(class_sx var1) {
      this.h = var1.h;
      this.b = var1.b;
      this.c = var1.c;
      this.d = var1.d;
      this.e = Math.max(var1.e, 1.0D);
      this.f = var1.f;
      this.g = var1.g;
   }

   public void c() {
      float var9;
      if(this.h == class_sx.class_a_in_class_sx.STRAFE) {
         float var1 = (float)this.a.a((class_sk)class_ys.d).e();
         float var2 = (float)this.e * var1;
         float var3 = this.f;
         float var4 = this.g;
         float var5 = MathHelper.c(var3 * var3 + var4 * var4);
         if(var5 < 1.0F) {
            var5 = 1.0F;
         }

         var5 = var2 / var5;
         var3 *= var5;
         var4 *= var5;
         float var6 = MathHelper.a(this.a.yaw * 0.017453292F);
         float var7 = MathHelper.b(this.a.yaw * 0.017453292F);
         float var8 = var3 * var7 - var4 * var6;
         var9 = var4 * var7 + var3 * var6;
         if(class_ayr.a((class_ahw)this.a.world, MathHelper.c(this.a.locX + (double)var8), MathHelper.c(this.a.locY), MathHelper.c(this.a.locZ + (double)var9)) != class_ayl.WALKABLE) {
            this.f = 1.0F;
            this.g = 0.0F;
            var2 = var1;
         }

         this.a.l(var2);
         this.a.o(this.f);
         this.a.p(this.g);
         this.h = class_sx.class_a_in_class_sx.WAIT;
      } else if(this.h == class_sx.class_a_in_class_sx.MOVE_TO) {
         this.h = class_sx.class_a_in_class_sx.WAIT;
         double var10 = this.b - this.a.locX;
         double var11 = this.d - this.a.locZ;
         double var12 = this.c - this.a.locY;
         double var13 = var10 * var10 + var12 * var12 + var11 * var11;
         if(var13 < 2.500000277905201E-7D) {
            this.a.o(0.0F);
            return;
         }

         var9 = (float)(MathHelper.b(var11, var10) * 57.2957763671875D) - 90.0F;
         this.a.yaw = this.a(this.a.yaw, var9, 90.0F);
         this.a.l((float)(this.e * this.a.a((class_sk)class_ys.d).e()));
         if(var12 > (double)this.a.P && var10 * var10 + var11 * var11 < 1.0D) {
            this.a.w().a();
         }
      } else {
         this.a.o(0.0F);
      }

   }

   protected float a(float var1, float var2, float var3) {
      float var4 = MathHelper.g(var2 - var1);
      if(var4 > var3) {
         var4 = var3;
      }

      if(var4 < -var3) {
         var4 = -var3;
      }

      float var5 = var1 + var4;
      if(var5 < 0.0F) {
         var5 += 360.0F;
      } else if(var5 > 360.0F) {
         var5 -= 360.0F;
      }

      return var5;
   }

   public double d() {
      return this.b;
   }

   public double e() {
      return this.c;
   }

   public double f() {
      return this.d;
   }

   public static enum class_a_in_class_sx {
      WAIT,
      MOVE_TO,
      STRAFE;
   }
}
