package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BlockVine;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_auc;
import net.minecraft.server.BlockPosition;

public class class_avg extends class_auc {
   public boolean b(World var1, Random var2, BlockPosition var3) {
      for(; var3.q() < 128; var3 = var3.a()) {
         if(var1.d(var3)) {
            EnumDirection[] var4 = EnumDirection.EnumDirectionLimit.HORIZONTAL.a();
            int var5 = var4.length;

            for(int var6 = 0; var6 < var5; ++var6) {
               EnumDirection var7 = var4[var6];
               if(Blocks.bn.b(var1, var3, var7)) {
                  IBlockData var8 = Blocks.bn.u().set(BlockVine.b, Boolean.valueOf(var7 == EnumDirection.NORTH)).set(BlockVine.c, Boolean.valueOf(var7 == EnumDirection.EAST)).set(BlockVine.d, Boolean.valueOf(var7 == EnumDirection.SOUTH)).set(BlockVine.e, Boolean.valueOf(var7 == EnumDirection.WEST));
                  var1.a((BlockPosition)var3, (IBlockData)var8, 2);
                  break;
               }
            }
         } else {
            var3 = var3.a(var2.nextInt(4) - var2.nextInt(4), 0, var2.nextInt(4) - var2.nextInt(4));
         }
      }

      return true;
   }
}
