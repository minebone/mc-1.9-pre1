package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockDirt;
import net.minecraft.server.BlockFlowers;
import net.minecraft.server.BlockLongGrass;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aju;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_alu extends Block implements class_aju {
   public static final class_arm a = class_arm.a("snowy");

   protected class_alu() {
      super(Material.b);
      this.w(this.A.b().set(a, Boolean.valueOf(false)));
      this.a(true);
      this.a(CreativeModeTab.b);
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      Block var4 = var2.getType(var3.a()).getBlock();
      return var1.set(a, Boolean.valueOf(var4 == Blocks.aJ || var4 == Blocks.aH));
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(!var1.E) {
         if(var1.k(var2.a()) < 4 && var1.getType(var2.a()).c() > 2) {
            var1.a(var2, Blocks.d.u());
         } else {
            if(var1.k(var2.a()) >= 9) {
               for(int var5 = 0; var5 < 4; ++var5) {
                  BlockPosition var6 = var2.a(var4.nextInt(3) - 1, var4.nextInt(5) - 3, var4.nextInt(3) - 1);
                  if(var6.q() >= 0 && var6.q() < 256 && !var1.e(var6)) {
                     return;
                  }

                  IBlockData var7 = var1.getType(var6.a());
                  IBlockData var8 = var1.getType(var6);
                  if(var8.getBlock() == Blocks.d && var8.get(BlockDirt.a) == BlockDirt.EnumDirtVariant.DIRT && var1.k(var6.a()) >= 4 && var7.c() <= 2) {
                     var1.a(var6, Blocks.c.u());
                  }
               }
            }

         }
      }
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Blocks.d.a(Blocks.d.u().set(BlockDirt.a, BlockDirt.EnumDirtVariant.DIRT), var2, var3);
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, boolean var4) {
      return true;
   }

   public boolean a(World var1, Random var2, BlockPosition var3, IBlockData var4) {
      return true;
   }

   public void b(World var1, Random var2, BlockPosition var3, IBlockData var4) {
      BlockPosition var5 = var3.a();

      label38:
      for(int var6 = 0; var6 < 128; ++var6) {
         BlockPosition var7 = var5;

         for(int var8 = 0; var8 < var6 / 16; ++var8) {
            var7 = var7.a(var2.nextInt(3) - 1, (var2.nextInt(3) - 1) * var2.nextInt(3) / 2, var2.nextInt(3) - 1);
            if(var1.getType(var7.b()).getBlock() != Blocks.c || var1.getType(var7).l()) {
               continue label38;
            }
         }

         if(var1.getType(var7).getBlock().x == Material.a) {
            if(var2.nextInt(8) == 0) {
               BlockFlowers.EnumFlowerVarient var11 = var1.b(var7).a(var2, var7);
               BlockFlowers var9 = var11.a().a();
               IBlockData var10 = var9.u().set(var9.g(), var11);
               if(var9.f(var1, var7, var10)) {
                  var1.a((BlockPosition)var7, (IBlockData)var10, 3);
               }
            } else {
               IBlockData var12 = Blocks.H.u().set(BlockLongGrass.a, BlockLongGrass.EnumTallGrassType.GRASS);
               if(Blocks.H.f(var1, var7, var12)) {
                  var1.a((BlockPosition)var7, (IBlockData)var12, 3);
               }
            }
         }
      }

   }

   public int e(IBlockData var1) {
      return 0;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }
}
