package net.minecraft.server;

import net.minecraft.server.BiomeBase;
import net.minecraft.server.IntCache;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_air;
import net.minecraft.server.class_ais;
import net.minecraft.server.class_axu;

public class class_ayb extends class_axu {
   public class_ayb(long var1, class_axu var3) {
      super(var1);
      this.a = var3;
   }

   public int[] a(int var1, int var2, int var3, int var4) {
      int[] var5 = this.a.a(var1 - 1, var2 - 1, var3 + 2, var4 + 2);
      int[] var6 = IntCache.a(var3 * var4);

      for(int var7 = 0; var7 < var4; ++var7) {
         for(int var8 = 0; var8 < var3; ++var8) {
            this.a((long)(var8 + var1), (long)(var7 + var2));
            int var9 = var5[var8 + 1 + (var7 + 1) * (var3 + 2)];
            BiomeBase var10 = BiomeBase.b(var9);
            int var11;
            int var12;
            int var13;
            int var14;
            if(var9 == BiomeBase.a(class_aik.p)) {
               var11 = var5[var8 + 1 + (var7 + 1 - 1) * (var3 + 2)];
               var12 = var5[var8 + 1 + 1 + (var7 + 1) * (var3 + 2)];
               var13 = var5[var8 + 1 - 1 + (var7 + 1) * (var3 + 2)];
               var14 = var5[var8 + 1 + (var7 + 1 + 1) * (var3 + 2)];
               if(var11 != BiomeBase.a(class_aik.a) && var12 != BiomeBase.a(class_aik.a) && var13 != BiomeBase.a(class_aik.a) && var14 != BiomeBase.a(class_aik.a)) {
                  var6[var8 + var7 * var3] = var9;
               } else {
                  var6[var8 + var7 * var3] = BiomeBase.a(class_aik.q);
               }
            } else if(var10 != null && var10.g() == class_air.class) {
               var11 = var5[var8 + 1 + (var7 + 1 - 1) * (var3 + 2)];
               var12 = var5[var8 + 1 + 1 + (var7 + 1) * (var3 + 2)];
               var13 = var5[var8 + 1 - 1 + (var7 + 1) * (var3 + 2)];
               var14 = var5[var8 + 1 + (var7 + 1 + 1) * (var3 + 2)];
               if(this.c(var11) && this.c(var12) && this.c(var13) && this.c(var14)) {
                  if(!b(var11) && !b(var12) && !b(var13) && !b(var14)) {
                     var6[var8 + var7 * var3] = var9;
                  } else {
                     var6[var8 + var7 * var3] = BiomeBase.a(class_aik.r);
                  }
               } else {
                  var6[var8 + var7 * var3] = BiomeBase.a(class_aik.y);
               }
            } else if(var9 != BiomeBase.a(class_aik.e) && var9 != BiomeBase.a(class_aik.J) && var9 != BiomeBase.a(class_aik.v)) {
               if(var10 != null && var10.p()) {
                  this.a(var5, var6, var8, var7, var3, var9, BiomeBase.a(class_aik.B));
               } else if(var9 != BiomeBase.a(class_aik.M) && var9 != BiomeBase.a(class_aik.N)) {
                  if(var9 != BiomeBase.a(class_aik.a) && var9 != BiomeBase.a(class_aik.z) && var9 != BiomeBase.a(class_aik.i) && var9 != BiomeBase.a(class_aik.h)) {
                     var11 = var5[var8 + 1 + (var7 + 1 - 1) * (var3 + 2)];
                     var12 = var5[var8 + 1 + 1 + (var7 + 1) * (var3 + 2)];
                     var13 = var5[var8 + 1 - 1 + (var7 + 1) * (var3 + 2)];
                     var14 = var5[var8 + 1 + (var7 + 1 + 1) * (var3 + 2)];
                     if(!b(var11) && !b(var12) && !b(var13) && !b(var14)) {
                        var6[var8 + var7 * var3] = var9;
                     } else {
                        var6[var8 + var7 * var3] = BiomeBase.a(class_aik.r);
                     }
                  } else {
                     var6[var8 + var7 * var3] = var9;
                  }
               } else {
                  var11 = var5[var8 + 1 + (var7 + 1 - 1) * (var3 + 2)];
                  var12 = var5[var8 + 1 + 1 + (var7 + 1) * (var3 + 2)];
                  var13 = var5[var8 + 1 - 1 + (var7 + 1) * (var3 + 2)];
                  var14 = var5[var8 + 1 + (var7 + 1 + 1) * (var3 + 2)];
                  if(!b(var11) && !b(var12) && !b(var13) && !b(var14)) {
                     if(this.d(var11) && this.d(var12) && this.d(var13) && this.d(var14)) {
                        var6[var8 + var7 * var3] = var9;
                     } else {
                        var6[var8 + var7 * var3] = BiomeBase.a(class_aik.d);
                     }
                  } else {
                     var6[var8 + var7 * var3] = var9;
                  }
               }
            } else {
               this.a(var5, var6, var8, var7, var3, var9, BiomeBase.a(class_aik.A));
            }
         }
      }

      return var6;
   }

   private void a(int[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7) {
      if(b(var6)) {
         var2[var3 + var4 * var5] = var6;
      } else {
         int var8 = var1[var3 + 1 + (var4 + 1 - 1) * (var5 + 2)];
         int var9 = var1[var3 + 1 + 1 + (var4 + 1) * (var5 + 2)];
         int var10 = var1[var3 + 1 - 1 + (var4 + 1) * (var5 + 2)];
         int var11 = var1[var3 + 1 + (var4 + 1 + 1) * (var5 + 2)];
         if(!b(var8) && !b(var9) && !b(var10) && !b(var11)) {
            var2[var3 + var4 * var5] = var6;
         } else {
            var2[var3 + var4 * var5] = var7;
         }

      }
   }

   private boolean c(int var1) {
      return BiomeBase.b(var1) != null && BiomeBase.b(var1).g() == class_air.class?true:var1 == BiomeBase.a(class_aik.y) || var1 == BiomeBase.a(class_aik.w) || var1 == BiomeBase.a(class_aik.x) || var1 == BiomeBase.a(class_aik.f) || var1 == BiomeBase.a(class_aik.g) || b(var1);
   }

   private boolean d(int var1) {
      return BiomeBase.b(var1) instanceof class_ais;
   }
}
