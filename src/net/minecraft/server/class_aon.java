package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_aon extends Block {
   protected static final AxisAlignedBB a = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.875D, 1.0D);

   public class_aon() {
      super(Material.p, MaterialMapColor.B);
      this.a(CreativeModeTab.b);
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return a;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Entity var4) {
      var4.motX *= 0.4D;
      var4.motZ *= 0.4D;
   }
}
