package net.minecraft.server;

import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockLeaves;
import net.minecraft.server.BlockVine;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.class_akh;
import net.minecraft.server.class_ane;
import net.minecraft.server.class_anf;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.class_ato;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_avf extends class_ato {
   private static final IBlockData a = Blocks.r.u().set(class_anf.b, BlockWood.EnumLogVariant.OAK);
   private static final IBlockData b = Blocks.t.u().set(class_ane.e, BlockWood.EnumLogVariant.OAK).set(BlockLeaves.b, Boolean.valueOf(false));
   private final int c;
   private final boolean d;
   private final IBlockData e;
   private final IBlockData f;

   public class_avf(boolean var1) {
      this(var1, 4, a, b, false);
   }

   public class_avf(boolean var1, int var2, IBlockData var3, IBlockData var4, boolean var5) {
      super(var1);
      this.c = var2;
      this.e = var3;
      this.f = var4;
      this.d = var5;
   }

   public boolean b(World var1, Random var2, BlockPosition var3) {
      int var4 = var2.nextInt(3) + this.c;
      boolean var5 = true;
      if(var3.q() >= 1 && var3.q() + var4 + 1 <= 256) {
         byte var7;
         int var9;
         int var10;
         for(int var6 = var3.q(); var6 <= var3.q() + 1 + var4; ++var6) {
            var7 = 1;
            if(var6 == var3.q()) {
               var7 = 0;
            }

            if(var6 >= var3.q() + 1 + var4 - 2) {
               var7 = 2;
            }

            BlockPosition.class_a_in_class_cj var8 = new BlockPosition.class_a_in_class_cj();

            for(var9 = var3.p() - var7; var9 <= var3.p() + var7 && var5; ++var9) {
               for(var10 = var3.r() - var7; var10 <= var3.r() + var7 && var5; ++var10) {
                  if(var6 >= 0 && var6 < 256) {
                     if(!this.a(var1.getType(var8.c(var9, var6, var10)).getBlock())) {
                        var5 = false;
                     }
                  } else {
                     var5 = false;
                  }
               }
            }
         }

         if(!var5) {
            return false;
         } else {
            Block var19 = var1.getType(var3.b()).getBlock();
            if((var19 == Blocks.c || var19 == Blocks.d || var19 == Blocks.ak) && var3.q() < 256 - var4 - 1) {
               this.a(var1, var3.b());
               var7 = 3;
               byte var20 = 0;

               int var11;
               int var13;
               int var14;
               BlockPosition var16;
               for(var9 = var3.q() - var7 + var4; var9 <= var3.q() + var4; ++var9) {
                  var10 = var9 - (var3.q() + var4);
                  var11 = var20 + 1 - var10 / 2;

                  for(int var12 = var3.p() - var11; var12 <= var3.p() + var11; ++var12) {
                     var13 = var12 - var3.p();

                     for(var14 = var3.r() - var11; var14 <= var3.r() + var11; ++var14) {
                        int var15 = var14 - var3.r();
                        if(Math.abs(var13) != var11 || Math.abs(var15) != var11 || var2.nextInt(2) != 0 && var10 != 0) {
                           var16 = new BlockPosition(var12, var9, var14);
                           Material var17 = var1.getType(var16).getMaterial();
                           if(var17 == Material.a || var17 == Material.j || var17 == Material.l) {
                              this.a(var1, var16, this.f);
                           }
                        }
                     }
                  }
               }

               for(var9 = 0; var9 < var4; ++var9) {
                  Material var21 = var1.getType(var3.b(var9)).getMaterial();
                  if(var21 == Material.a || var21 == Material.j || var21 == Material.l) {
                     this.a(var1, var3.b(var9), this.e);
                     if(this.d && var9 > 0) {
                        if(var2.nextInt(3) > 0 && var1.d(var3.a(-1, var9, 0))) {
                           this.a(var1, var3.a(-1, var9, 0), BlockVine.c);
                        }

                        if(var2.nextInt(3) > 0 && var1.d(var3.a(1, var9, 0))) {
                           this.a(var1, var3.a(1, var9, 0), BlockVine.e);
                        }

                        if(var2.nextInt(3) > 0 && var1.d(var3.a(0, var9, -1))) {
                           this.a(var1, var3.a(0, var9, -1), BlockVine.d);
                        }

                        if(var2.nextInt(3) > 0 && var1.d(var3.a(0, var9, 1))) {
                           this.a(var1, var3.a(0, var9, 1), BlockVine.b);
                        }
                     }
                  }
               }

               if(this.d) {
                  for(var9 = var3.q() - 3 + var4; var9 <= var3.q() + var4; ++var9) {
                     var10 = var9 - (var3.q() + var4);
                     var11 = 2 - var10 / 2;
                     BlockPosition.class_a_in_class_cj var24 = new BlockPosition.class_a_in_class_cj();

                     for(var13 = var3.p() - var11; var13 <= var3.p() + var11; ++var13) {
                        for(var14 = var3.r() - var11; var14 <= var3.r() + var11; ++var14) {
                           var24.c(var13, var9, var14);
                           if(var1.getType(var24).getMaterial() == Material.j) {
                              BlockPosition var26 = var24.e();
                              var16 = var24.f();
                              BlockPosition var27 = var24.c();
                              BlockPosition var18 = var24.d();
                              if(var2.nextInt(4) == 0 && var1.getType(var26).getMaterial() == Material.a) {
                                 this.b(var1, var26, BlockVine.c);
                              }

                              if(var2.nextInt(4) == 0 && var1.getType(var16).getMaterial() == Material.a) {
                                 this.b(var1, var16, BlockVine.e);
                              }

                              if(var2.nextInt(4) == 0 && var1.getType(var27).getMaterial() == Material.a) {
                                 this.b(var1, var27, BlockVine.d);
                              }

                              if(var2.nextInt(4) == 0 && var1.getType(var18).getMaterial() == Material.a) {
                                 this.b(var1, var18, BlockVine.b);
                              }
                           }
                        }
                     }
                  }

                  if(var2.nextInt(5) == 0 && var4 > 5) {
                     for(var9 = 0; var9 < 2; ++var9) {
                        Iterator var22 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

                        while(var22.hasNext()) {
                           EnumDirection var23 = (EnumDirection)var22.next();
                           if(var2.nextInt(4 - var9) == 0) {
                              EnumDirection var25 = var23.d();
                              this.a(var1, var2.nextInt(3), var3.a(var25.g(), var4 - 5 + var9, var25.i()), var23);
                           }
                        }
                     }
                  }
               }

               return true;
            } else {
               return false;
            }
         }
      } else {
         return false;
      }
   }

   private void a(World var1, int var2, BlockPosition var3, EnumDirection var4) {
      this.a(var1, var3, Blocks.bN.u().set(class_akh.a, Integer.valueOf(var2)).set(class_akh.D, var4));
   }

   private void a(World var1, BlockPosition var2, class_arm var3) {
      this.a(var1, var2, Blocks.bn.u().set(var3, Boolean.valueOf(true)));
   }

   private void b(World var1, BlockPosition var2, class_arm var3) {
      this.a(var1, var2, var3);
      int var4 = 4;

      for(var2 = var2.b(); var1.getType(var2).getMaterial() == Material.a && var4 > 0; --var4) {
         this.a(var1, var2, var3);
         var2 = var2.b();
      }

   }
}
