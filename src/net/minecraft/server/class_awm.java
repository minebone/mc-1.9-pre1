package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.StructureBoundingBox;
import net.minecraft.server.class_ahm;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;

public class class_awm {
   private class_amq a;
   private class_aod b;
   private boolean c;
   private Block d;
   private class_ahm e;
   private StructureBoundingBox f;
   private boolean g;

   public class_awm() {
      this(class_amq.NONE, class_aod.NONE, false, (Block)null, (StructureBoundingBox)null);
   }

   public class_awm(class_amq var1, class_aod var2, boolean var3, Block var4, StructureBoundingBox var5) {
      this.b = var2;
      this.a = var1;
      this.c = var3;
      this.d = var4;
      this.e = null;
      this.f = var5;
      this.g = true;
   }

   public class_awm a() {
      return (new class_awm(this.a, this.b, this.c, this.d, this.f)).a(this.e).b(this.g);
   }

   public class_awm a(class_amq var1) {
      this.a = var1;
      return this;
   }

   public class_awm a(class_aod var1) {
      this.b = var1;
      return this;
   }

   public class_awm a(boolean var1) {
      this.c = var1;
      return this;
   }

   public class_awm a(Block var1) {
      this.d = var1;
      return this;
   }

   public class_awm a(class_ahm var1) {
      this.e = var1;
      return this;
   }

   public class_awm a(StructureBoundingBox var1) {
      this.f = var1;
      return this;
   }

   public class_amq b() {
      return this.a;
   }

   public class_awm b(boolean var1) {
      this.g = var1;
      return this;
   }

   public class_aod c() {
      return this.b;
   }

   public boolean e() {
      return this.c;
   }

   public Block f() {
      return this.d;
   }

   public StructureBoundingBox g() {
      if(this.f == null && this.e != null) {
         this.i();
      }

      return this.f;
   }

   public boolean h() {
      return this.g;
   }

   void i() {
      this.f = this.b(this.e);
   }

   private StructureBoundingBox b(class_ahm var1) {
      if(var1 == null) {
         return null;
      } else {
         int var2 = var1.a * 16;
         int var3 = var1.b * 16;
         return new StructureBoundingBox(var2, 0, var3, var2 + 16 - 1, 255, var3 + 16 - 1);
      }
   }
}
