package net.minecraft.server;

import java.util.UUID;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.class_ox;

public class class_pl implements class_ox {
   public int a() {
      return 108;
   }

   public NBTTagCompound a(NBTTagCompound var1) {
      if(var1.b("UUID", 8)) {
         var1.a("UUID", UUID.fromString(var1.l("UUID")));
      }

      return var1;
   }
}
