package net.minecraft.server;

import com.google.common.base.Predicate;
import java.util.Iterator;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ali;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arn;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_rz;

public class class_ape extends Block {
   public static final class_arn a = class_arn.a("facing", new Predicate() {
      public boolean a(EnumDirection var1) {
         return var1 != EnumDirection.DOWN;
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((EnumDirection)var1);
      }
   });
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.4000000059604645D, 0.0D, 0.4000000059604645D, 0.6000000238418579D, 0.6000000238418579D, 0.6000000238418579D);
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.3499999940395355D, 0.20000000298023224D, 0.699999988079071D, 0.6499999761581421D, 0.800000011920929D, 1.0D);
   protected static final AxisAlignedBB d = new AxisAlignedBB(0.3499999940395355D, 0.20000000298023224D, 0.0D, 0.6499999761581421D, 0.800000011920929D, 0.30000001192092896D);
   protected static final AxisAlignedBB e = new AxisAlignedBB(0.699999988079071D, 0.20000000298023224D, 0.3499999940395355D, 1.0D, 0.800000011920929D, 0.6499999761581421D);
   protected static final AxisAlignedBB f = new AxisAlignedBB(0.0D, 0.20000000298023224D, 0.3499999940395355D, 0.30000001192092896D, 0.800000011920929D, 0.6499999761581421D);

   protected class_ape() {
      super(Material.q);
      this.w(this.A.b().set(a, EnumDirection.UP));
      this.a(true);
      this.a(CreativeModeTab.c);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      switch(class_ape.SyntheticClass_1.a[((EnumDirection)var1.get(a)).ordinal()]) {
      case 1:
         return f;
      case 2:
         return e;
      case 3:
         return d;
      case 4:
         return c;
      default:
         return b;
      }
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   private boolean b(World var1, BlockPosition var2) {
      if(var1.getType(var2).q()) {
         return true;
      } else {
         Block var3 = var1.getType(var2).getBlock();
         return var3 instanceof class_ali || var3 == Blocks.w || var3 == Blocks.bZ || var3 == Blocks.cG;
      }
   }

   public boolean a(World var1, BlockPosition var2) {
      Iterator var3 = a.c().iterator();

      EnumDirection var4;
      do {
         if(!var3.hasNext()) {
            return false;
         }

         var4 = (EnumDirection)var3.next();
      } while(!this.a(var1, var2, var4));

      return true;
   }

   private boolean a(World var1, BlockPosition var2, EnumDirection var3) {
      BlockPosition var4 = var2.a(var3.d());
      boolean var5 = var3.k().c();
      return var5 && var1.d(var4, true) || var3.equals(EnumDirection.UP) && this.b(var1, var4);
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      if(this.a(var1, var2, var3)) {
         return this.u().set(a, var3);
      } else {
         Iterator var9 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

         EnumDirection var10;
         do {
            if(!var9.hasNext()) {
               return this.u();
            }

            var10 = (EnumDirection)var9.next();
         } while(!var1.d(var2.a(var10.d()), true));

         return this.u().set(a, var10);
      }
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      this.f(var1, var2, var3);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      this.e(var1, var2, var3);
   }

   protected boolean e(World var1, BlockPosition var2, IBlockData var3) {
      if(!this.f(var1, var2, var3)) {
         return true;
      } else {
         EnumDirection var4 = (EnumDirection)var3.get(a);
         EnumDirection.class_a_in_class_cq var5 = var4.k();
         EnumDirection var6 = var4.d();
         boolean var7 = false;
         if(var5.c() && !var1.d(var2.a(var6), true)) {
            var7 = true;
         } else if(var5.b() && !this.b(var1, var2.a(var6))) {
            var7 = true;
         }

         if(var7) {
            this.b(var1, var2, var3, 0);
            var1.g(var2);
            return true;
         } else {
            return false;
         }
      }
   }

   protected boolean f(World var1, BlockPosition var2, IBlockData var3) {
      if(var3.getBlock() == this && this.a(var1, var2, (EnumDirection)var3.get(a))) {
         return true;
      } else {
         if(var1.getType(var2).getBlock() == this) {
            this.b(var1, var2, var3, 0);
            var1.g(var2);
         }

         return false;
      }
   }

   public IBlockData a(int var1) {
      IBlockData var2 = this.u();
      switch(var1) {
      case 1:
         var2 = var2.set(a, EnumDirection.EAST);
         break;
      case 2:
         var2 = var2.set(a, EnumDirection.WEST);
         break;
      case 3:
         var2 = var2.set(a, EnumDirection.SOUTH);
         break;
      case 4:
         var2 = var2.set(a, EnumDirection.NORTH);
         break;
      case 5:
      default:
         var2 = var2.set(a, EnumDirection.UP);
      }

      return var2;
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3;
      switch(class_ape.SyntheticClass_1.a[((EnumDirection)var1.get(a)).ordinal()]) {
      case 1:
         var3 = var2 | 1;
         break;
      case 2:
         var3 = var2 | 2;
         break;
      case 3:
         var3 = var2 | 3;
         break;
      case 4:
         var3 = var2 | 4;
         break;
      case 5:
      case 6:
      default:
         var3 = var2 | 5;
      }

      return var3;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(a, var2.a((EnumDirection)var1.get(a)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(a)));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumDirection.values().length];

      static {
         try {
            a[EnumDirection.EAST.ordinal()] = 1;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 2;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[EnumDirection.SOUTH.ordinal()] = 3;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.NORTH.ordinal()] = 4;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.DOWN.ordinal()] = 5;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.UP.ordinal()] = 6;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
