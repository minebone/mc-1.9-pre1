package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_akf extends Block {
   public static final class_arm a = class_arm.a("north");
   public static final class_arm b = class_arm.a("east");
   public static final class_arm c = class_arm.a("south");
   public static final class_arm d = class_arm.a("west");
   public static final class_arm e = class_arm.a("up");
   public static final class_arm f = class_arm.a("down");

   protected class_akf() {
      super(Material.k);
      this.a(CreativeModeTab.c);
      this.w(this.A.b().set(a, Boolean.valueOf(false)).set(b, Boolean.valueOf(false)).set(c, Boolean.valueOf(false)).set(d, Boolean.valueOf(false)).set(e, Boolean.valueOf(false)).set(f, Boolean.valueOf(false)));
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      Block var4 = var2.getType(var3.b()).getBlock();
      Block var5 = var2.getType(var3.a()).getBlock();
      Block var6 = var2.getType(var3.c()).getBlock();
      Block var7 = var2.getType(var3.f()).getBlock();
      Block var8 = var2.getType(var3.d()).getBlock();
      Block var9 = var2.getType(var3.e()).getBlock();
      return var1.set(f, Boolean.valueOf(var4 == this || var4 == Blocks.cS || var4 == Blocks.bH)).set(e, Boolean.valueOf(var5 == this || var5 == Blocks.cS)).set(a, Boolean.valueOf(var6 == this || var6 == Blocks.cS)).set(b, Boolean.valueOf(var7 == this || var7 == Blocks.cS)).set(c, Boolean.valueOf(var8 == this || var8 == Blocks.cS)).set(d, Boolean.valueOf(var9 == this || var9 == Blocks.cS));
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      var1 = var1.b(var2, var3);
      float var4 = 0.1875F;
      float var5 = ((Boolean)var1.get(d)).booleanValue()?0.0F:0.1875F;
      float var6 = ((Boolean)var1.get(f)).booleanValue()?0.0F:0.1875F;
      float var7 = ((Boolean)var1.get(a)).booleanValue()?0.0F:0.1875F;
      float var8 = ((Boolean)var1.get(b)).booleanValue()?1.0F:0.8125F;
      float var9 = ((Boolean)var1.get(e)).booleanValue()?1.0F:0.8125F;
      float var10 = ((Boolean)var1.get(c)).booleanValue()?1.0F:0.8125F;
      return new AxisAlignedBB((double)var5, (double)var6, (double)var7, (double)var8, (double)var9, (double)var10);
   }

   public void a(IBlockData var1, World var2, BlockPosition var3, AxisAlignedBB var4, List var5, Entity var6) {
      var1 = var1.b(var2, var3);
      float var7 = 0.1875F;
      float var8 = 0.8125F;
      a(var3, var4, var5, new AxisAlignedBB(0.1875D, 0.1875D, 0.1875D, 0.8125D, 0.8125D, 0.8125D));
      if(((Boolean)var1.get(d)).booleanValue()) {
         a(var3, var4, var5, new AxisAlignedBB(0.0D, 0.1875D, 0.1875D, 0.1875D, 0.8125D, 0.8125D));
      }

      if(((Boolean)var1.get(b)).booleanValue()) {
         a(var3, var4, var5, new AxisAlignedBB(0.8125D, 0.1875D, 0.1875D, 1.0D, 0.8125D, 0.8125D));
      }

      if(((Boolean)var1.get(e)).booleanValue()) {
         a(var3, var4, var5, new AxisAlignedBB(0.1875D, 0.8125D, 0.1875D, 0.8125D, 1.0D, 0.8125D));
      }

      if(((Boolean)var1.get(f)).booleanValue()) {
         a(var3, var4, var5, new AxisAlignedBB(0.1875D, 0.0D, 0.1875D, 0.8125D, 0.1875D, 0.8125D));
      }

      if(((Boolean)var1.get(a)).booleanValue()) {
         a(var3, var4, var5, new AxisAlignedBB(0.1875D, 0.1875D, 0.0D, 0.8125D, 0.8125D, 0.1875D));
      }

      if(((Boolean)var1.get(c)).booleanValue()) {
         a(var3, var4, var5, new AxisAlignedBB(0.1875D, 0.1875D, 0.8125D, 0.8125D, 0.8125D, 1.0D));
      }

   }

   public int e(IBlockData var1) {
      return 0;
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(!this.b(var1, var2)) {
         var1.b(var2, true);
      }

   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Items.cS;
   }

   public int a(Random var1) {
      return var1.nextInt(2);
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean a(World var1, BlockPosition var2) {
      return super.a(var1, var2)?this.b(var1, var2):false;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!this.b(var1, var2)) {
         var1.a((BlockPosition)var2, (Block)this, 1);
      }

   }

   public boolean b(World var1, BlockPosition var2) {
      boolean var3 = var1.d(var2.a());
      boolean var4 = var1.d(var2.b());
      Iterator var5 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

      Block var9;
      do {
         BlockPosition var7;
         Block var8;
         do {
            if(!var5.hasNext()) {
               Block var10 = var1.getType(var2.b()).getBlock();
               return var10 == this || var10 == Blocks.bH;
            }

            EnumDirection var6 = (EnumDirection)var5.next();
            var7 = var2.a(var6);
            var8 = var1.getType(var7).getBlock();
         } while(var8 != this);

         if(!var3 && !var4) {
            return false;
         }

         var9 = var1.getType(var7.b()).getBlock();
      } while(var9 != this && var9 != Blocks.bH);

      return true;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b, c, d, e, f});
   }

   public boolean b(class_ahw var1, BlockPosition var2) {
      return false;
   }
}
