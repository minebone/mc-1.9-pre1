package net.minecraft.server;

import com.google.common.base.Predicate;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockHopper;
import net.minecraft.server.Container;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.class_abi;
import net.minecraft.server.ItemStack;
import net.minecraft.server.BlockChest;
import net.minecraft.server.class_apw;
import net.minecraft.server.class_aqg;
import net.minecraft.server.class_aql;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ky;
import net.minecraft.server.MathHelper;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_qg;
import net.minecraft.server.class_qx;
import net.minecraft.server.class_ru;
import net.minecraft.server.class_yc;

public class TileEntityHopper extends class_aql implements class_aqg, class_ky {
   private ItemStack[] a = new ItemStack[5];
   private String f;
   private int g = -1;

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      this.a = new ItemStack[this.u_()];
      if(var2.b("CustomName", 8)) {
         this.f = var2.l("CustomName");
      }

      this.g = var2.h("TransferCooldown");
      if(!this.b((NBTTagCompound)var2)) {
         NBTTagList var3 = var2.c("Items", 10);

         for(int var4 = 0; var4 < var3.c(); ++var4) {
            NBTTagCompound var5 = var3.b(var4);
            byte var6 = var5.f("Slot");
            if(var6 >= 0 && var6 < this.a.length) {
               this.a[var6] = ItemStack.a(var5);
            }
         }
      }

   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      if(!this.c(var1)) {
         NBTTagList var2 = new NBTTagList();

         for(int var3 = 0; var3 < this.a.length; ++var3) {
            if(this.a[var3] != null) {
               NBTTagCompound var4 = new NBTTagCompound();
               var4.a("Slot", (byte)var3);
               this.a[var3].b(var4);
               var2.a((NBTTag)var4);
            }
         }

         var1.a((String)"Items", (NBTTag)var2);
      }

      var1.a("TransferCooldown", this.g);
      if(this.o_()) {
         var1.a("CustomName", this.f);
      }

   }

   public int u_() {
      return this.a.length;
   }

   public ItemStack a(int var1) {
      this.d((EntityHuman)null);
      return this.a[var1];
   }

   public ItemStack a(int var1, int var2) {
      this.d((EntityHuman)null);
      return class_qg.a(this.a, var1, var2);
   }

   public ItemStack b(int var1) {
      this.d((EntityHuman)null);
      return class_qg.a(this.a, var1);
   }

   public void a(int var1, ItemStack var2) {
      this.d((EntityHuman)null);
      this.a[var1] = var2;
      if(var2 != null && var2.b > this.w_()) {
         var2.b = this.w_();
      }

   }

   public String h_() {
      return this.o_()?this.f:"container.hopper";
   }

   public boolean o_() {
      return this.f != null && !this.f.isEmpty();
   }

   public void a(String var1) {
      this.f = var1;
   }

   public int w_() {
      return 64;
   }

   public boolean a(EntityHuman var1) {
      return this.b.r(this.c) != this?false:var1.e((double)this.c.p() + 0.5D, (double)this.c.q() + 0.5D, (double)this.c.r() + 0.5D) <= 64.0D;
   }

   public void b(EntityHuman var1) {
   }

   public void c(EntityHuman var1) {
   }

   public boolean b(int var1, ItemStack var2) {
      return true;
   }

   public void c() {
      if(this.b != null && !this.b.E) {
         --this.g;
         if(!this.o()) {
            this.d(0);
            this.m();
         }

      }
   }

   public boolean m() {
      if(this.b != null && !this.b.E) {
         if(!this.o() && BlockHopper.f(this.u())) {
            boolean var1 = false;
            if(!this.q()) {
               var1 = this.H();
            }

            if(!this.r()) {
               var1 = a((class_aqg)this) || var1;
            }

            if(var1) {
               this.d(8);
               this.v_();
               return true;
            }
         }

         return false;
      } else {
         return false;
      }
   }

   private boolean q() {
      ItemStack[] var1 = this.a;
      int var2 = var1.length;

      for(int var3 = 0; var3 < var2; ++var3) {
         ItemStack var4 = var1[var3];
         if(var4 != null) {
            return false;
         }
      }

      return true;
   }

   private boolean r() {
      ItemStack[] var1 = this.a;
      int var2 = var1.length;

      for(int var3 = 0; var3 < var2; ++var3) {
         ItemStack var4 = var1[var3];
         if(var4 == null || var4.b != var4.c()) {
            return false;
         }
      }

      return true;
   }

   private boolean H() {
      IInventory var1 = this.I();
      if(var1 == null) {
         return false;
      } else {
         EnumDirection var2 = BlockHopper.e(this.u()).d();
         if(this.a(var1, var2)) {
            return false;
         } else {
            for(int var3 = 0; var3 < this.u_(); ++var3) {
               if(this.a(var3) != null) {
                  ItemStack var4 = this.a(var3).k();
                  ItemStack var5 = a(var1, this.a(var3, 1), var2);
                  if(var5 == null || var5.b == 0) {
                     var1.v_();
                     return true;
                  }

                  this.a(var3, var4);
               }
            }

            return false;
         }
      }
   }

   private boolean a(IInventory var1, EnumDirection var2) {
      if(var1 instanceof class_qx) {
         class_qx var7 = (class_qx)var1;
         int[] var8 = var7.a(var2);

         for(int var9 = 0; var9 < var8.length; ++var9) {
            ItemStack var6 = var7.a(var8[var9]);
            if(var6 == null || var6.b != var6.c()) {
               return false;
            }
         }
      } else {
         int var3 = var1.u_();

         for(int var4 = 0; var4 < var3; ++var4) {
            ItemStack var5 = var1.a(var4);
            if(var5 == null || var5.b != var5.c()) {
               return false;
            }
         }
      }

      return true;
   }

   private static boolean b(IInventory var0, EnumDirection var1) {
      if(var0 instanceof class_qx) {
         class_qx var2 = (class_qx)var0;
         int[] var3 = var2.a(var1);

         for(int var4 = 0; var4 < var3.length; ++var4) {
            if(var2.a(var3[var4]) != null) {
               return false;
            }
         }
      } else {
         int var5 = var0.u_();

         for(int var6 = 0; var6 < var5; ++var6) {
            if(var0.a(var6) != null) {
               return false;
            }
         }
      }

      return true;
   }

   public static boolean a(class_aqg var0) {
      IInventory var1 = b(var0);
      if(var1 != null) {
         EnumDirection var2 = EnumDirection.DOWN;
         if(b(var1, var2)) {
            return false;
         }

         if(var1 instanceof class_qx) {
            class_qx var3 = (class_qx)var1;
            int[] var4 = var3.a(var2);

            for(int var5 = 0; var5 < var4.length; ++var5) {
               if(a(var0, var1, var4[var5], var2)) {
                  return true;
               }
            }
         } else {
            int var7 = var1.u_();

            for(int var9 = 0; var9 < var7; ++var9) {
               if(a(var0, var1, var9, var2)) {
                  return true;
               }
            }
         }
      } else {
         Iterator var6 = a(var0.D(), var0.E(), var0.F(), var0.G()).iterator();

         while(var6.hasNext()) {
            class_yc var8 = (class_yc)var6.next();
            if(a((IInventory)var0, (class_yc)var8)) {
               return true;
            }
         }
      }

      return false;
   }

   private static boolean a(class_aqg var0, IInventory var1, int var2, EnumDirection var3) {
      ItemStack var4 = var1.a(var2);
      if(var4 != null && b(var1, var4, var2, var3)) {
         ItemStack var5 = var4.k();
         ItemStack var6 = a(var0, var1.a(var2, 1), (EnumDirection)null);
         if(var6 == null || var6.b == 0) {
            var1.v_();
            return true;
         }

         var1.a(var2, var5);
      }

      return false;
   }

   public static boolean a(IInventory var0, class_yc var1) {
      boolean var2 = false;
      if(var1 == null) {
         return false;
      } else {
         ItemStack var3 = var1.k().k();
         ItemStack var4 = a(var0, var3, (EnumDirection)null);
         if(var4 != null && var4.b != 0) {
            var1.a(var4);
         } else {
            var2 = true;
            var1.S();
         }

         return var2;
      }
   }

   public static ItemStack a(IInventory var0, ItemStack var1, EnumDirection var2) {
      if(var0 instanceof class_qx && var2 != null) {
         class_qx var6 = (class_qx)var0;
         int[] var7 = var6.a(var2);

         for(int var5 = 0; var5 < var7.length && var1 != null && var1.b > 0; ++var5) {
            var1 = c(var0, var1, var7[var5], var2);
         }
      } else {
         int var3 = var0.u_();

         for(int var4 = 0; var4 < var3 && var1 != null && var1.b > 0; ++var4) {
            var1 = c(var0, var1, var4, var2);
         }
      }

      if(var1 != null && var1.b == 0) {
         var1 = null;
      }

      return var1;
   }

   private static boolean a(IInventory var0, ItemStack var1, int var2, EnumDirection var3) {
      return !var0.b(var2, var1)?false:!(var0 instanceof class_qx) || ((class_qx)var0).a(var2, var1, var3);
   }

   private static boolean b(IInventory var0, ItemStack var1, int var2, EnumDirection var3) {
      return !(var0 instanceof class_qx) || ((class_qx)var0).b(var2, var1, var3);
   }

   private static ItemStack c(IInventory var0, ItemStack var1, int var2, EnumDirection var3) {
      ItemStack var4 = var0.a(var2);
      if(a(var0, var1, var2, var3)) {
         boolean var5 = false;
         if(var4 == null) {
            var0.a(var2, var1);
            var1 = null;
            var5 = true;
         } else if(a(var4, var1)) {
            int var6 = var1.c() - var4.b;
            int var7 = Math.min(var1.b, var6);
            var1.b -= var7;
            var4.b += var7;
            var5 = var7 > 0;
         }

         if(var5) {
            if(var0 instanceof TileEntityHopper) {
               TileEntityHopper var8 = (TileEntityHopper)var0;
               if(var8.p()) {
                  var8.d(8);
               }

               var0.v_();
            }

            var0.v_();
         }
      }

      return var1;
   }

   private IInventory I() {
      EnumDirection var1 = BlockHopper.e(this.u());
      return b(this.D(), this.E() + (double)var1.g(), this.F() + (double)var1.h(), this.G() + (double)var1.i());
   }

   public static IInventory b(class_aqg var0) {
      return b(var0.D(), var0.E(), var0.F() + 1.0D, var0.G());
   }

   public static List a(World var0, double var1, double var3, double var5) {
      return var0.a(class_yc.class, new AxisAlignedBB(var1 - 0.5D, var3, var5 - 0.5D, var1 + 0.5D, var3 + 1.5D, var5 + 0.5D), class_ru.a);
   }

   public static IInventory b(World var0, double var1, double var3, double var5) {
      Object var7 = null;
      int var8 = MathHelper.c(var1);
      int var9 = MathHelper.c(var3);
      int var10 = MathHelper.c(var5);
      BlockPosition var11 = new BlockPosition(var8, var9, var10);
      Block var12 = var0.getType(var11).getBlock();
      if(var12.m()) {
         TileEntity var13 = var0.r(var11);
         if(var13 instanceof IInventory) {
            var7 = (IInventory)var13;
            if(var7 instanceof class_apw && var12 instanceof BlockChest) {
               var7 = ((BlockChest)var12).c(var0, var11);
            }
         }
      }

      if(var7 == null) {
         List var14 = var0.a((Entity)null, (AxisAlignedBB)(new AxisAlignedBB(var1 - 0.5D, var3 - 0.5D, var5 - 0.5D, var1 + 0.5D, var3 + 0.5D, var5 + 0.5D)), (Predicate)class_ru.c);
         if(!var14.isEmpty()) {
            var7 = (IInventory)var14.get(var0.r.nextInt(var14.size()));
         }
      }

      return (IInventory)var7;
   }

   private static boolean a(ItemStack var0, ItemStack var1) {
      return var0.b() != var1.b()?false:(var0.i() != var1.i()?false:(var0.b > var0.c()?false:ItemStack.a(var0, var1)));
   }

   public double E() {
      return (double)this.c.p() + 0.5D;
   }

   public double F() {
      return (double)this.c.q() + 0.5D;
   }

   public double G() {
      return (double)this.c.r() + 0.5D;
   }

   public void d(int var1) {
      this.g = var1;
   }

   public boolean o() {
      return this.g > 0;
   }

   public boolean p() {
      return this.g <= 1;
   }

   public String k() {
      return "minecraft:hopper";
   }

   public Container a(PlayerInventory var1, EntityHuman var2) {
      this.d(var2);
      return new class_abi(var1, this, var2);
   }

   public int c_(int var1) {
      return 0;
   }

   public void b(int var1, int var2) {
   }

   public int g() {
      return 0;
   }

   public void l() {
      this.d((EntityHuman)null);

      for(int var1 = 0; var1 < this.a.length; ++var1) {
         this.a[var1] = null;
      }

   }
}
