package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.StructureBoundingBox;
import net.minecraft.server.TileEntity;
import net.minecraft.server.UtilColor;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_ahm;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_apa;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_awl;
import net.minecraft.server.class_awm;
import net.minecraft.server.class_awn;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.BaseBlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutTileEntityData;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_or;

public class class_aqo extends TileEntity {
   private String a = "";
   private String f = "";
   private String g = "";
   private BlockPosition h = new BlockPosition(1, 1, 1);
   private BlockPosition i = BlockPosition.a;
   private class_amq j = class_amq.NONE;
   private class_aod k = class_aod.NONE;
   private class_aqo.class_a_in_class_aqo l = class_aqo.class_a_in_class_aqo.DATA;
   private boolean m;

   public void a(NBTTagCompound var1) {
      super.a(var1);
      var1.a("name", this.a);
      var1.a("author", this.f);
      var1.a("metadata", this.g);
      var1.a("posX", this.h.p());
      var1.a("posY", this.h.q());
      var1.a("posZ", this.h.r());
      var1.a("sizeX", this.i.p());
      var1.a("sizeY", this.i.q());
      var1.a("sizeZ", this.i.r());
      var1.a("rotation", this.k.toString());
      var1.a("mirror", this.j.toString());
      var1.a("mode", this.l.toString());
      var1.a("ignoreEntities", this.m);
   }

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      this.a = var2.l("name");
      this.f = var2.l("author");
      this.g = var2.l("metadata");
      this.h = new BlockPosition(var2.h("posX"), var2.h("posY"), var2.h("posZ"));
      this.i = new BlockPosition(var2.h("sizeX"), var2.h("sizeY"), var2.h("sizeZ"));

      try {
         this.k = class_aod.valueOf(var2.l("rotation"));
      } catch (IllegalArgumentException var6) {
         this.k = class_aod.NONE;
      }

      try {
         this.j = class_amq.valueOf(var2.l("mirror"));
      } catch (IllegalArgumentException var5) {
         this.j = class_amq.NONE;
      }

      try {
         this.l = class_aqo.class_a_in_class_aqo.valueOf(var2.l("mode"));
      } catch (IllegalArgumentException var4) {
         this.l = class_aqo.class_a_in_class_aqo.DATA;
      }

      this.m = var2.p("ignoreEntities");
   }

   public Packet D_() {
      NBTTagCompound var1 = new NBTTagCompound();
      this.a(var1);
      return new PacketPlayOutTileEntityData(this.c, 7, var1);
   }

   public void a(String var1) {
      this.a = var1;
   }

   public void b(BlockPosition var1) {
      this.h = var1;
   }

   public void c(BlockPosition var1) {
      this.i = var1;
   }

   public void a(class_amq var1) {
      this.j = var1;
   }

   public void a(class_aod var1) {
      this.k = var1;
   }

   public void b(String var1) {
      this.g = var1;
   }

   public void a(class_aqo.class_a_in_class_aqo var1) {
      this.l = var1;
      IBlockData var2 = this.b.getType(this.v());
      if(var2.getBlock() == Blocks.df) {
         this.b.a((BlockPosition)this.v(), (IBlockData)var2.set(class_apa.a, var1), 2);
      }

   }

   public void a(boolean var1) {
      this.m = var1;
   }

   public boolean l() {
      if(this.l != class_aqo.class_a_in_class_aqo.SAVE) {
         return false;
      } else {
         BlockPosition var1 = this.v();
         boolean var2 = true;
         BlockPosition var3 = new BlockPosition(var1.p() - 128, 0, var1.r() - 128);
         BlockPosition var4 = new BlockPosition(var1.p() + 128, 255, var1.r() + 128);
         List var5 = this.a(var3, var4);
         List var6 = this.a(var5);
         if(var6.size() < 1) {
            return false;
         } else {
            StructureBoundingBox var7 = this.a(var1, var6);
            if(var7.d - var7.a > 1 && var7.e - var7.b > 1 && var7.f - var7.c > 1) {
               this.h = new BlockPosition(var7.a - var1.p() + 1, var7.b - var1.q() + 1, var7.c - var1.r() + 1);
               this.i = new BlockPosition(var7.d - var7.a - 1, var7.e - var7.b - 1, var7.f - var7.c - 1);
               this.v_();
               IBlockData var8 = this.b.getType(var1);
               this.b.a(var1, var8, var8, 3);
               return true;
            } else {
               return false;
            }
         }
      }
   }

   private List a(List var1) {
      Iterable var2 = Iterables.filter(var1, (Predicate)(new Predicate() {
         public boolean a(class_aqo var1) {
            return var1.l == class_aqo.class_a_in_class_aqo.CORNER && class_aqo.this.a.equals(var1.a);
         }

         // $FF: synthetic method
         public boolean apply(Object var1) {
            return this.a((class_aqo)var1);
         }
      }));
      return Lists.newArrayList(var2);
   }

   private List a(BlockPosition var1, BlockPosition var2) {
      ArrayList var3 = Lists.newArrayList();
      Iterator var4 = BlockPosition.b(var1, var2).iterator();

      while(var4.hasNext()) {
         BlockPosition.class_a_in_class_cj var5 = (BlockPosition.class_a_in_class_cj)var4.next();
         IBlockData var6 = this.b.getType(var5);
         if(var6.getBlock() == Blocks.df) {
            TileEntity var7 = this.b.r(var5);
            if(var7 != null && var7 instanceof class_aqo) {
               var3.add((class_aqo)var7);
            }
         }
      }

      return var3;
   }

   private StructureBoundingBox a(BlockPosition var1, List var2) {
      StructureBoundingBox var3;
      if(var2.size() > 1) {
         BlockPosition var4 = ((class_aqo)var2.get(0)).v();
         var3 = new StructureBoundingBox(var4, var4);
      } else {
         var3 = new StructureBoundingBox(var1, var1);
      }

      Iterator var7 = var2.iterator();

      while(var7.hasNext()) {
         class_aqo var5 = (class_aqo)var7.next();
         BlockPosition var6 = var5.v();
         if(var6.p() < var3.a) {
            var3.a = var6.p();
         } else if(var6.p() > var3.d) {
            var3.d = var6.p();
         }

         if(var6.q() < var3.b) {
            var3.b = var6.q();
         } else if(var6.q() > var3.e) {
            var3.e = var6.q();
         }

         if(var6.r() < var3.c) {
            var3.c = var6.r();
         } else if(var6.r() > var3.f) {
            var3.f = var6.r();
         }
      }

      return var3;
   }

   public boolean m() {
      if(this.l == class_aqo.class_a_in_class_aqo.SAVE && !this.b.E) {
         BlockPosition var1 = this.v().a((BaseBlockPosition)this.h);
         WorldServer var2 = (WorldServer)this.b;
         MinecraftServer var3 = this.b.u();
         class_awl var4 = var2.y();
         class_awn var5 = var4.a(var3, new class_kk(this.a));
         var5.a(this.b, var1, this.i, !this.m, Blocks.cv);
         var5.a(this.f);
         var4.c(var3, new class_kk(this.a));
         return true;
      } else {
         return false;
      }
   }

   public boolean n() {
      if(this.l == class_aqo.class_a_in_class_aqo.LOAD && !this.b.E) {
         BlockPosition var1 = this.v().a((BaseBlockPosition)this.h);
         WorldServer var2 = (WorldServer)this.b;
         MinecraftServer var3 = this.b.u();
         class_awl var4 = var2.y();
         class_awn var5 = var4.a(var3, new class_kk(this.a));
         if(!UtilColor.b(var5.b())) {
            this.f = var5.b();
         }

         if(!this.i.equals(var5.a())) {
            this.i = var5.a();
            return false;
         } else {
            BlockPosition var6 = var5.a(this.k);
            Iterator var7 = this.b.b((Entity)null, (AxisAlignedBB)(new AxisAlignedBB(var1, var6.a((BaseBlockPosition)var1).a(-1, -1, -1)))).iterator();

            while(var7.hasNext()) {
               Entity var8 = (Entity)var7.next();
               this.b.f(var8);
            }

            class_awm var9 = (new class_awm()).a(this.j).a(this.k).a(this.m).a((class_ahm)null).a((Block)null).b(false);
            var5.a(this.b, var1, var9);
            return true;
         }
      } else {
         return false;
      }
   }

   public static enum class_a_in_class_aqo implements class_or {
      SAVE("save", 0),
      LOAD("load", 1),
      CORNER("corner", 2),
      DATA("data", 3);

      private static final class_aqo.class_a_in_class_aqo[] e = new class_aqo.class_a_in_class_aqo[values().length];
      private final String f;
      private final int g;

      private class_a_in_class_aqo(String var3, int var4) {
         this.f = var3;
         this.g = var4;
      }

      public String m() {
         return this.f;
      }

      public int a() {
         return this.g;
      }

      public static class_aqo.class_a_in_class_aqo a(int var0) {
         if(var0 < 0 || var0 >= e.length) {
            var0 = 0;
         }

         return e[var0];
      }

      static {
         class_aqo.class_a_in_class_aqo[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            class_aqo.class_a_in_class_aqo var3 = var0[var2];
            e[var3.a()] = var3;
         }

      }
   }
}
