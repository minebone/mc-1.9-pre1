package net.minecraft.server;

import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.class_ox;

public class class_pk implements class_ox {
   public int a() {
      return 135;
   }

   public NBTTagCompound a(NBTTagCompound var1) {
      while(var1.b("Riding", 10)) {
         NBTTagCompound var2 = this.b(var1);
         this.a(var1, var2);
         var1 = var2;
      }

      return var1;
   }

   protected void a(NBTTagCompound var1, NBTTagCompound var2) {
      NBTTagList var3 = new NBTTagList();
      var3.a((NBTTag)var1);
      var2.a((String)"Passengers", (NBTTag)var3);
   }

   protected NBTTagCompound b(NBTTagCompound var1) {
      NBTTagCompound var2 = var1.o("Riding");
      var1.q("Riding");
      return var2;
   }
}
