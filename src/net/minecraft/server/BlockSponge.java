package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;
import net.minecraft.server.class_ou;

public class BlockSponge extends Block {
   public static final class_arm a = class_arm.a("wet");

   protected BlockSponge() {
      super(Material.m);
      this.w(this.A.b().set(a, Boolean.valueOf(false)));
      this.a(CreativeModeTab.b);
   }

   public String c() {
      return class_di.a(this.a() + ".dry.name");
   }

   public int d(IBlockData var1) {
      return ((Boolean)var1.get(a)).booleanValue()?1:0;
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      this.e(var1, var2, var3);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      this.e(var1, var2, var3);
      super.a(var1, var2, var3, var4);
   }

   protected void e(World var1, BlockPosition var2, IBlockData var3) {
      if(!((Boolean)var3.get(a)).booleanValue() && this.b(var1, var2)) {
         var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Boolean.valueOf(true)), 2);
         var1.b(2001, var2, Block.a((Block)Blocks.j));
      }

   }

   private boolean b(World var1, BlockPosition var2) {
      LinkedList var3 = Lists.newLinkedList();
      ArrayList var4 = Lists.newArrayList();
      var3.add(new class_ou(var2, Integer.valueOf(0)));
      int var5 = 0;

      BlockPosition var7;
      while(!var3.isEmpty()) {
         class_ou var6 = (class_ou)var3.poll();
         var7 = (BlockPosition)var6.a();
         int var8 = ((Integer)var6.b()).intValue();
         EnumDirection[] var9 = EnumDirection.values();
         int var10 = var9.length;

         for(int var11 = 0; var11 < var10; ++var11) {
            EnumDirection var12 = var9[var11];
            BlockPosition var13 = var7.a(var12);
            if(var1.getType(var13).getMaterial() == Material.h) {
               var1.a((BlockPosition)var13, (IBlockData)Blocks.AIR.u(), 2);
               var4.add(var13);
               ++var5;
               if(var8 < 6) {
                  var3.add(new class_ou(var13, Integer.valueOf(var8 + 1)));
               }
            }
         }

         if(var5 > 64) {
            break;
         }
      }

      Iterator var14 = var4.iterator();

      while(var14.hasNext()) {
         var7 = (BlockPosition)var14.next();
         var1.d(var7, Blocks.AIR);
      }

      return var5 > 0;
   }

   public IBlockData a(int var1) {
      return this.u().set(a, Boolean.valueOf((var1 & 1) == 1));
   }

   public int e(IBlockData var1) {
      return ((Boolean)var1.get(a)).booleanValue()?1:0;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }
}
