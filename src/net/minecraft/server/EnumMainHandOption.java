package net.minecraft.server;

import net.minecraft.server.ChatMessage;
import net.minecraft.server.IChatBaseComponent;

public enum EnumMainHandOption {
   LEFT(new ChatMessage("options.mainHand.left", new Object[0])),
   RIGHT(new ChatMessage("options.mainHand.right", new Object[0]));

   private final IChatBaseComponent c;

   private EnumMainHandOption(IChatBaseComponent var3) {
      this.c = var3;
   }

   public String toString() {
      return this.c.c();
   }
}
