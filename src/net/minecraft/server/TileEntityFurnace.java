package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Container;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.ItemTool;
import net.minecraft.server.Items;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.class_abf;
import net.minecraft.server.class_abg;
import net.minecraft.server.class_acb;
import net.minecraft.server.class_adm;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aew;
import net.minecraft.server.class_afp;
import net.minecraft.server.class_alr;
import net.minecraft.server.class_aqi;
import net.minecraft.server.Material;
import net.minecraft.server.class_ky;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_qg;
import net.minecraft.server.class_qx;

public class TileEntityFurnace extends class_aqi implements class_ky, class_qx {
   private static final int[] a = new int[]{0};
   private static final int[] f = new int[]{2, 1};
   private static final int[] g = new int[]{1};
   private ItemStack[] h = new ItemStack[3];
   private int i;
   private int j;
   private int k;
   private int l;
   private String m;

   public int u_() {
      return this.h.length;
   }

   public ItemStack a(int var1) {
      return this.h[var1];
   }

   public ItemStack a(int var1, int var2) {
      return class_qg.a(this.h, var1, var2);
   }

   public ItemStack b(int var1) {
      return class_qg.a(this.h, var1);
   }

   public void a(int var1, ItemStack var2) {
      boolean var3 = var2 != null && var2.a(this.h[var1]) && ItemStack.a(var2, this.h[var1]);
      this.h[var1] = var2;
      if(var2 != null && var2.b > this.w_()) {
         var2.b = this.w_();
      }

      if(var1 == 0 && !var3) {
         this.l = this.a(var2);
         this.k = 0;
         this.v_();
      }

   }

   public String h_() {
      return this.o_()?this.m:"container.furnace";
   }

   public boolean o_() {
      return this.m != null && !this.m.isEmpty();
   }

   public void a(String var1) {
      this.m = var1;
   }

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      NBTTagList var3 = var2.c("Items", 10);
      this.h = new ItemStack[this.u_()];

      for(int var4 = 0; var4 < var3.c(); ++var4) {
         NBTTagCompound var5 = var3.b(var4);
         byte var6 = var5.f("Slot");
         if(var6 >= 0 && var6 < this.h.length) {
            this.h[var6] = ItemStack.a(var5);
         }
      }

      this.i = var2.g("BurnTime");
      this.k = var2.g("CookTime");
      this.l = var2.g("CookTimeTotal");
      this.j = b(this.h[1]);
      if(var2.b("CustomName", 8)) {
         this.m = var2.l("CustomName");
      }

   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      var1.a("BurnTime", (short)this.i);
      var1.a("CookTime", (short)this.k);
      var1.a("CookTimeTotal", (short)this.l);
      NBTTagList var2 = new NBTTagList();

      for(int var3 = 0; var3 < this.h.length; ++var3) {
         if(this.h[var3] != null) {
            NBTTagCompound var4 = new NBTTagCompound();
            var4.a("Slot", (byte)var3);
            this.h[var3].b(var4);
            var2.a((NBTTag)var4);
         }
      }

      var1.a((String)"Items", (NBTTag)var2);
      if(this.o_()) {
         var1.a("CustomName", this.m);
      }

   }

   public int w_() {
      return 64;
   }

   public boolean m() {
      return this.i > 0;
   }

   public void c() {
      boolean var1 = this.m();
      boolean var2 = false;
      if(this.m()) {
         --this.i;
      }

      if(!this.b.E) {
         if(!this.m() && (this.h[1] == null || this.h[0] == null)) {
            if(!this.m() && this.k > 0) {
               this.k = MathHelper.a(this.k - 2, 0, this.l);
            }
         } else {
            if(!this.m() && this.o()) {
               this.j = this.i = b(this.h[1]);
               if(this.m()) {
                  var2 = true;
                  if(this.h[1] != null) {
                     --this.h[1].b;
                     if(this.h[1].b == 0) {
                        Item var3 = this.h[1].b().q();
                        this.h[1] = var3 != null?new ItemStack(var3):null;
                     }
                  }
               }
            }

            if(this.m() && this.o()) {
               ++this.k;
               if(this.k == this.l) {
                  this.k = 0;
                  this.l = this.a(this.h[0]);
                  this.n();
                  var2 = true;
               }
            } else {
               this.k = 0;
            }
         }

         if(var1 != this.m()) {
            var2 = true;
            class_alr.a(this.m(), this.b, this.c);
         }
      }

      if(var2) {
         this.v_();
      }

   }

   public int a(ItemStack var1) {
      return 200;
   }

   private boolean o() {
      if(this.h[0] == null) {
         return false;
      } else {
         ItemStack var1 = class_afp.a().a(this.h[0]);
         return var1 == null?false:(this.h[2] == null?true:(!this.h[2].a(var1)?false:(this.h[2].b < this.w_() && this.h[2].b < this.h[2].c()?true:this.h[2].b < var1.c())));
      }
   }

   public void n() {
      if(this.o()) {
         ItemStack var1 = class_afp.a().a(this.h[0]);
         if(this.h[2] == null) {
            this.h[2] = var1.k();
         } else if(this.h[2].b() == var1.b()) {
            ++this.h[2].b;
         }

         if(this.h[0].b() == Item.a(Blocks.v) && this.h[0].i() == 1 && this.h[1] != null && this.h[1].b() == Items.ay) {
            this.h[1] = new ItemStack(Items.az);
         }

         --this.h[0].b;
         if(this.h[0].b <= 0) {
            this.h[0] = null;
         }

      }
   }

   public static int b(ItemStack var0) {
      if(var0 == null) {
         return 0;
      } else {
         Item var1 = var0.b();
         if(var1 instanceof class_acb && Block.a(var1) != Blocks.AIR) {
            Block var2 = Block.a(var1);
            if(var2 == Blocks.bM) {
               return 150;
            }

            if(var2.u().getMaterial() == Material.d) {
               return 300;
            }

            if(var2 == Blocks.cA) {
               return 16000;
            }
         }

         return var1 instanceof ItemTool && ((ItemTool)var1).h().equals("WOOD")?200:(var1 instanceof class_aew && ((class_aew)var1).h().equals("WOOD")?200:(var1 instanceof class_adm && ((class_adm)var1).g().equals("WOOD")?200:(var1 == Items.A?100:(var1 == Items.j?1600:(var1 == Items.aA?20000:(var1 == Item.a(Blocks.g)?100:(var1 == Items.bC?2400:0)))))));
      }
   }

   public static boolean c(ItemStack var0) {
      return b(var0) > 0;
   }

   public boolean a(EntityHuman var1) {
      return this.b.r(this.c) != this?false:var1.e((double)this.c.p() + 0.5D, (double)this.c.q() + 0.5D, (double)this.c.r() + 0.5D) <= 64.0D;
   }

   public void b(EntityHuman var1) {
   }

   public void c(EntityHuman var1) {
   }

   public boolean b(int var1, ItemStack var2) {
      if(var1 == 2) {
         return false;
      } else if(var1 != 1) {
         return true;
      } else {
         ItemStack var3 = this.h[1];
         return c(var2) || class_abf.d_(var2) && (var3 == null || var3.b() != Items.ay);
      }
   }

   public int[] a(EnumDirection var1) {
      return var1 == EnumDirection.DOWN?f:(var1 == EnumDirection.UP?a:g);
   }

   public boolean a(int var1, ItemStack var2, EnumDirection var3) {
      return this.b(var1, var2);
   }

   public boolean b(int var1, ItemStack var2, EnumDirection var3) {
      if(var3 == EnumDirection.DOWN && var1 == 1) {
         Item var4 = var2.b();
         if(var4 != Items.az && var4 != Items.ay) {
            return false;
         }
      }

      return true;
   }

   public String k() {
      return "minecraft:furnace";
   }

   public Container a(PlayerInventory var1, EntityHuman var2) {
      return new class_abg(var1, this);
   }

   public int c_(int var1) {
      switch(var1) {
      case 0:
         return this.i;
      case 1:
         return this.j;
      case 2:
         return this.k;
      case 3:
         return this.l;
      default:
         return 0;
      }
   }

   public void b(int var1, int var2) {
      switch(var1) {
      case 0:
         this.i = var2;
         break;
      case 1:
         this.j = var2;
         break;
      case 2:
         this.k = var2;
         break;
      case 3:
         this.l = var2;
      }

   }

   public int g() {
      return 4;
   }

   public void l() {
      for(int var1 = 0; var1 < this.h.length; ++var1) {
         this.h[var1] = null;
      }

   }
}
