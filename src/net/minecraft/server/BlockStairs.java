package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aho;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_amf;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_aoo;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arn;
import net.minecraft.server.IBlockState;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_or;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;

public class BlockStairs extends Block {
   public static final class_arn a = class_amf.D;
   public static final BlockStateEnum b = BlockStateEnum.a("half", BlockStairs.class_a_in_class_aos.class);
   public static final BlockStateEnum c = BlockStateEnum.a("shape", BlockStairs.EnumStairShape.class);
   protected static final AxisAlignedBB d = new AxisAlignedBB(0.0D, 0.5D, 0.0D, 1.0D, 1.0D, 1.0D);
   protected static final AxisAlignedBB e = new AxisAlignedBB(0.0D, 0.5D, 0.0D, 0.5D, 1.0D, 1.0D);
   protected static final AxisAlignedBB f = new AxisAlignedBB(0.5D, 0.5D, 0.0D, 1.0D, 1.0D, 1.0D);
   protected static final AxisAlignedBB g = new AxisAlignedBB(0.0D, 0.5D, 0.0D, 1.0D, 1.0D, 0.5D);
   protected static final AxisAlignedBB B = new AxisAlignedBB(0.0D, 0.5D, 0.5D, 1.0D, 1.0D, 1.0D);
   protected static final AxisAlignedBB C = new AxisAlignedBB(0.0D, 0.5D, 0.0D, 0.5D, 1.0D, 0.5D);
   protected static final AxisAlignedBB D = new AxisAlignedBB(0.5D, 0.5D, 0.0D, 1.0D, 1.0D, 0.5D);
   protected static final AxisAlignedBB E = new AxisAlignedBB(0.0D, 0.5D, 0.5D, 0.5D, 1.0D, 1.0D);
   protected static final AxisAlignedBB F = new AxisAlignedBB(0.5D, 0.5D, 0.5D, 1.0D, 1.0D, 1.0D);
   protected static final AxisAlignedBB G = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.5D, 1.0D);
   protected static final AxisAlignedBB H = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.5D, 0.5D, 1.0D);
   protected static final AxisAlignedBB I = new AxisAlignedBB(0.5D, 0.0D, 0.0D, 1.0D, 0.5D, 1.0D);
   protected static final AxisAlignedBB J = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.5D, 0.5D);
   protected static final AxisAlignedBB K = new AxisAlignedBB(0.0D, 0.0D, 0.5D, 1.0D, 0.5D, 1.0D);
   protected static final AxisAlignedBB L = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.5D, 0.5D, 0.5D);
   protected static final AxisAlignedBB M = new AxisAlignedBB(0.5D, 0.0D, 0.0D, 1.0D, 0.5D, 0.5D);
   protected static final AxisAlignedBB N = new AxisAlignedBB(0.0D, 0.0D, 0.5D, 0.5D, 0.5D, 1.0D);
   protected static final AxisAlignedBB O = new AxisAlignedBB(0.5D, 0.0D, 0.5D, 1.0D, 0.5D, 1.0D);
   private final Block P;
   private final IBlockData Q;

   protected BlockStairs(IBlockData var1) {
      super(var1.getBlock().x);
      this.w(this.A.b().set(a, EnumDirection.NORTH).set(b, BlockStairs.class_a_in_class_aos.BOTTOM).set(c, BlockStairs.EnumStairShape.STRAIGHT));
      this.P = var1.getBlock();
      this.Q = var1;
      this.c(this.P.q);
      this.b(this.P.r / 3.0F);
      this.a((class_aoo)this.P.v);
      this.d(255);
      this.a((CreativeModeTab)CreativeModeTab.b);
   }

   public void a(IBlockData var1, World var2, BlockPosition var3, AxisAlignedBB var4, List var5, Entity var6) {
      var1 = this.b((IBlockData)var1, (class_ahw)var2, (BlockPosition)var3);
      Iterator var7 = x(var1).iterator();

      while(var7.hasNext()) {
         AxisAlignedBB var8 = (AxisAlignedBB)var7.next();
         a(var3, var4, var5, var8);
      }

   }

   private static List x(IBlockData var0) {
      ArrayList var1 = Lists.newArrayList();
      boolean var2 = var0.get(b) == BlockStairs.class_a_in_class_aos.TOP;
      var1.add(var2?d:G);
      BlockStairs.EnumStairShape var3 = (BlockStairs.EnumStairShape)var0.get(c);
      if(var3 == BlockStairs.EnumStairShape.STRAIGHT || var3 == BlockStairs.EnumStairShape.INNER_LEFT || var3 == BlockStairs.EnumStairShape.INNER_RIGHT) {
         var1.add(y(var0));
      }

      if(var3 != BlockStairs.EnumStairShape.STRAIGHT) {
         var1.add(z(var0));
      }

      return var1;
   }

   private static AxisAlignedBB y(IBlockData var0) {
      boolean var1 = var0.get(b) == BlockStairs.class_a_in_class_aos.TOP;
      switch(BlockStairs.SyntheticClass_1.a[((EnumDirection)var0.get(a)).ordinal()]) {
      case 1:
      default:
         return var1?J:g;
      case 2:
         return var1?K:B;
      case 3:
         return var1?H:e;
      case 4:
         return var1?I:f;
      }
   }

   private static AxisAlignedBB z(IBlockData var0) {
      EnumDirection var1 = (EnumDirection)var0.get(a);
      EnumDirection var2;
      switch(BlockStairs.SyntheticClass_1.b[((BlockStairs.EnumStairShape)var0.get(c)).ordinal()]) {
      case 1:
      default:
         var2 = var1;
         break;
      case 2:
         var2 = var1.e();
         break;
      case 3:
         var2 = var1.d();
         break;
      case 4:
         var2 = var1.f();
      }

      boolean var3 = var0.get(b) == BlockStairs.class_a_in_class_aos.TOP;
      switch(BlockStairs.SyntheticClass_1.a[var2.ordinal()]) {
      case 1:
      default:
         return var3?L:C;
      case 2:
         return var3?O:F;
      case 3:
         return var3?N:E;
      case 4:
         return var3?M:D;
      }
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public void a(World var1, BlockPosition var2, EntityHuman var3) {
      this.P.a(var1, var2, var3);
   }

   public void d(World var1, BlockPosition var2, IBlockData var3) {
      this.P.d(var1, var2, var3);
   }

   public float a(Entity var1) {
      return this.P.a(var1);
   }

   public int a(World var1) {
      return this.P.a(var1);
   }

   public Vec3D a(World var1, BlockPosition var2, Entity var3, Vec3D var4) {
      return this.P.a(var1, var2, var3, var4);
   }

   public boolean n() {
      return this.P.n();
   }

   public boolean a(IBlockData var1, boolean var2) {
      return this.P.a(var1, var2);
   }

   public boolean a(World var1, BlockPosition var2) {
      return this.P.a(var1, var2);
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      this.a(var1, var2, this.Q, Blocks.AIR);
      this.P.c(var1, var2, this.Q);
   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      this.P.b(var1, var2, this.Q);
   }

   public void a(World var1, BlockPosition var2, Entity var3) {
      this.P.a(var1, var2, var3);
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      this.P.b(var1, var2, var3, var4);
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      return this.P.a(var1, var2, this.Q, var4, var5, var6, EnumDirection.DOWN, 0.0F, 0.0F, 0.0F);
   }

   public void a(World var1, BlockPosition var2, class_aho var3) {
      this.P.a(var1, var2, var3);
   }

   public boolean k(IBlockData var1) {
      return var1.get(b) == BlockStairs.class_a_in_class_aos.TOP;
   }

   public MaterialMapColor r(IBlockData var1) {
      return this.P.r(this.Q);
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      IBlockData var9 = super.a(var1, var2, var3, var4, var5, var6, var7, var8);
      var9 = var9.set(a, var8.bh()).set(c, BlockStairs.EnumStairShape.STRAIGHT);
      return var3 != EnumDirection.DOWN && (var3 == EnumDirection.UP || (double)var5 <= 0.5D)?var9.set(b, BlockStairs.class_a_in_class_aos.BOTTOM):var9.set(b, BlockStairs.class_a_in_class_aos.TOP);
   }

   public MovingObjectPosition a(IBlockData var1, World var2, BlockPosition var3, Vec3D var4, Vec3D var5) {
      ArrayList var6 = Lists.newArrayList();
      Iterator var7 = x(this.b((IBlockData)var1, (class_ahw)var2, (BlockPosition)var3)).iterator();

      while(var7.hasNext()) {
         AxisAlignedBB var8 = (AxisAlignedBB)var7.next();
         var6.add(this.a(var3, var4, var5, var8));
      }

      MovingObjectPosition var14 = null;
      double var15 = 0.0D;
      Iterator var10 = var6.iterator();

      while(var10.hasNext()) {
         MovingObjectPosition var11 = (MovingObjectPosition)var10.next();
         if(var11 != null) {
            double var12 = var11.c.g(var5);
            if(var12 > var15) {
               var14 = var11;
               var15 = var12;
            }
         }
      }

      return var14;
   }

   public IBlockData a(int var1) {
      IBlockData var2 = this.u().set(b, (var1 & 4) > 0?BlockStairs.class_a_in_class_aos.TOP:BlockStairs.class_a_in_class_aos.BOTTOM);
      var2 = var2.set(a, EnumDirection.a(5 - (var1 & 3)));
      return var2;
   }

   public int e(IBlockData var1) {
      int var2 = 0;
      if(var1.get(b) == BlockStairs.class_a_in_class_aos.TOP) {
         var2 |= 4;
      }

      var2 |= 5 - ((EnumDirection)var1.get(a)).a();
      return var2;
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return var1.set(c, d(var1, var2, var3));
   }

   private static BlockStairs.EnumStairShape d(IBlockData var0, class_ahw var1, BlockPosition var2) {
      EnumDirection var3 = (EnumDirection)var0.get(a);
      IBlockData var4 = var1.getType(var2.a(var3));
      if(i(var4) && var0.get(b) == var4.get(b)) {
         EnumDirection var5 = (EnumDirection)var4.get(a);
         if(var5.k() != ((EnumDirection)var0.get(a)).k() && d(var0, var1, var2, var5.d())) {
            if(var5 == var3.f()) {
               return BlockStairs.EnumStairShape.OUTER_LEFT;
            }

            return BlockStairs.EnumStairShape.OUTER_RIGHT;
         }
      }

      IBlockData var7 = var1.getType(var2.a(var3.d()));
      if(i(var7) && var0.get(b) == var7.get(b)) {
         EnumDirection var6 = (EnumDirection)var7.get(a);
         if(var6.k() != ((EnumDirection)var0.get(a)).k() && d(var0, var1, var2, var6)) {
            if(var6 == var3.f()) {
               return BlockStairs.EnumStairShape.INNER_LEFT;
            }

            return BlockStairs.EnumStairShape.INNER_RIGHT;
         }
      }

      return BlockStairs.EnumStairShape.STRAIGHT;
   }

   private static boolean d(IBlockData var0, class_ahw var1, BlockPosition var2, EnumDirection var3) {
      IBlockData var4 = var1.getType(var2.a(var3));
      return !i(var4) || var4.get(a) != var0.get(a) || var4.get(b) != var0.get(b);
   }

   public static boolean i(IBlockData var0) {
      return var0.getBlock() instanceof BlockStairs;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(a, var2.a((EnumDirection)var1.get(a)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      EnumDirection var3 = (EnumDirection)var1.get(a);
      BlockStairs.EnumStairShape var4 = (BlockStairs.EnumStairShape)var1.get(c);
      switch(BlockStairs.SyntheticClass_1.c[var2.ordinal()]) {
      case 1:
         if(var3.k() == EnumDirection.class_a_in_class_cq.Z) {
            switch(BlockStairs.SyntheticClass_1.b[var4.ordinal()]) {
            case 1:
               return var1.a(class_aod.CLOCKWISE_180).set(c, BlockStairs.EnumStairShape.OUTER_RIGHT);
            case 2:
               return var1.a(class_aod.CLOCKWISE_180).set(c, BlockStairs.EnumStairShape.OUTER_LEFT);
            case 3:
               return var1.a(class_aod.CLOCKWISE_180).set(c, BlockStairs.EnumStairShape.INNER_LEFT);
            case 4:
               return var1.a(class_aod.CLOCKWISE_180).set(c, BlockStairs.EnumStairShape.INNER_RIGHT);
            default:
               return var1.a(class_aod.CLOCKWISE_180);
            }
         }
         break;
      case 2:
         if(var3.k() == EnumDirection.class_a_in_class_cq.X) {
            switch(BlockStairs.SyntheticClass_1.b[var4.ordinal()]) {
            case 1:
               return var1.a(class_aod.CLOCKWISE_180).set(c, BlockStairs.EnumStairShape.OUTER_RIGHT);
            case 2:
               return var1.a(class_aod.CLOCKWISE_180).set(c, BlockStairs.EnumStairShape.OUTER_LEFT);
            case 3:
               return var1.a(class_aod.CLOCKWISE_180).set(c, BlockStairs.EnumStairShape.INNER_RIGHT);
            case 4:
               return var1.a(class_aod.CLOCKWISE_180).set(c, BlockStairs.EnumStairShape.INNER_LEFT);
            case 5:
               return var1.a(class_aod.CLOCKWISE_180);
            }
         }
      }

      return super.a(var1, var2);
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b, c});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b;
      // $FF: synthetic field
      static final int[] c = new int[class_amq.values().length];

      static {
         try {
            c[class_amq.LEFT_RIGHT.ordinal()] = 1;
         } catch (NoSuchFieldError var11) {
            ;
         }

         try {
            c[class_amq.FRONT_BACK.ordinal()] = 2;
         } catch (NoSuchFieldError var10) {
            ;
         }

         b = new int[BlockStairs.EnumStairShape.values().length];

         try {
            b[BlockStairs.EnumStairShape.OUTER_LEFT.ordinal()] = 1;
         } catch (NoSuchFieldError var9) {
            ;
         }

         try {
            b[BlockStairs.EnumStairShape.OUTER_RIGHT.ordinal()] = 2;
         } catch (NoSuchFieldError var8) {
            ;
         }

         try {
            b[BlockStairs.EnumStairShape.INNER_RIGHT.ordinal()] = 3;
         } catch (NoSuchFieldError var7) {
            ;
         }

         try {
            b[BlockStairs.EnumStairShape.INNER_LEFT.ordinal()] = 4;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            b[BlockStairs.EnumStairShape.STRAIGHT.ordinal()] = 5;
         } catch (NoSuchFieldError var5) {
            ;
         }

         a = new int[EnumDirection.values().length];

         try {
            a[EnumDirection.NORTH.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.SOUTH.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.EAST.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum EnumStairShape implements class_or {
      STRAIGHT("straight"),
      INNER_LEFT("inner_left"),
      INNER_RIGHT("inner_right"),
      OUTER_LEFT("outer_left"),
      OUTER_RIGHT("outer_right");

      private final String f;

      private EnumStairShape(String var3) {
         this.f = var3;
      }

      public String toString() {
         return this.f;
      }

      public String m() {
         return this.f;
      }
   }

   public static enum class_a_in_class_aos implements class_or {
      TOP("top"),
      BOTTOM("bottom");

      private final String c;

      private class_a_in_class_aos(String var3) {
         this.c = var3;
      }

      public String toString() {
         return this.c;
      }

      public String m() {
         return this.c;
      }
   }
}
