package net.minecraft.server;

import com.google.common.collect.Sets;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.Block;
import net.minecraft.server.BlockMinecartTrackAbstract;
import net.minecraft.server.Blocks;
import net.minecraft.server.Chunk;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EnumCreatureType;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_ahm;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_lu;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_ov;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_sd;

public final class class_ahz {
   private static final int a = (int)Math.pow(17.0D, 2.0D);
   private final Set b = Sets.newHashSet();

   public int a(WorldServer var1, boolean var2, boolean var3, boolean var4) {
      if(!var2 && !var3) {
         return 0;
      } else {
         this.b.clear();
         int var5 = 0;
         Iterator var6 = var1.i.iterator();

         while(true) {
            EntityHuman var7;
            int var9;
            int var12;
            do {
               if(!var6.hasNext()) {
                  int var37 = 0;
                  BlockPosition var38 = var1.R();
                  EnumCreatureType[] var39 = EnumCreatureType.values();
                  var9 = var39.length;

                  label137:
                  for(int var40 = 0; var40 < var9; ++var40) {
                     EnumCreatureType var41 = var39[var40];
                     if((!var41.d() || var3) && (var41.d() || var2) && (!var41.e() || var4)) {
                        var12 = var1.a((Class)var41.a());
                        int var42 = var41.b() * var5 / a;
                        if(var12 <= var42) {
                           BlockPosition.class_a_in_class_cj var43 = new BlockPosition.class_a_in_class_cj();
                           Iterator var44 = this.b.iterator();

                           label134:
                           while(true) {
                              int var18;
                              int var19;
                              int var20;
                              IBlockData var21;
                              do {
                                 if(!var44.hasNext()) {
                                    continue label137;
                                 }

                                 class_ahm var16 = (class_ahm)var44.next();
                                 BlockPosition var17 = a(var1, var16.a, var16.b);
                                 var18 = var17.p();
                                 var19 = var17.q();
                                 var20 = var17.r();
                                 var21 = var1.getType(var17);
                              } while(var21.l());

                              int var22 = 0;

                              for(int var23 = 0; var23 < 3; ++var23) {
                                 int var24 = var18;
                                 int var25 = var19;
                                 int var26 = var20;
                                 byte var27 = 6;
                                 BiomeBase.BiomeMeta var28 = null;
                                 class_sc var29 = null;
                                 int var30 = MathHelper.f(Math.random() * 4.0D);

                                 for(int var31 = 0; var31 < var30; ++var31) {
                                    var24 += var1.r.nextInt(var27) - var1.r.nextInt(var27);
                                    var25 += var1.r.nextInt(1) - var1.r.nextInt(1);
                                    var26 += var1.r.nextInt(var27) - var1.r.nextInt(var27);
                                    var43.c(var24, var25, var26);
                                    float var32 = (float)var24 + 0.5F;
                                    float var33 = (float)var26 + 0.5F;
                                    if(!var1.a((double)var32, (double)var25, (double)var33, 24.0D) && var38.e((double)var32, (double)var25, (double)var33) >= 576.0D) {
                                       if(var28 == null) {
                                          var28 = var1.a((EnumCreatureType)var41, (BlockPosition)var43);
                                          if(var28 == null) {
                                             break;
                                          }
                                       }

                                       if(var1.a(var41, var28, var43) && a(class_sd.a(var28.b), var1, var43)) {
                                          EntityInsentient var34;
                                          try {
                                             var34 = (EntityInsentient)var28.b.getConstructor(new Class[]{World.class}).newInstance(new Object[]{var1});
                                          } catch (Exception var36) {
                                             var36.printStackTrace();
                                             return var37;
                                          }

                                          var34.b((double)var32, (double)var25, (double)var33, var1.r.nextFloat() * 360.0F, 0.0F);
                                          if(var34.cF() && var34.cG()) {
                                             var29 = var34.a(var1.D(new BlockPosition(var34)), var29);
                                             if(var34.cG()) {
                                                ++var22;
                                                var1.a((Entity)var34);
                                             } else {
                                                var34.S();
                                             }

                                             if(var22 >= var34.cJ()) {
                                                continue label134;
                                             }
                                          }

                                          var37 += var22;
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }

                  return var37;
               }

               var7 = (EntityHuman)var6.next();
            } while(var7.y());

            int var8 = MathHelper.c(var7.locX / 16.0D);
            var9 = MathHelper.c(var7.locZ / 16.0D);
            byte var10 = 8;

            for(int var11 = -var10; var11 <= var10; ++var11) {
               for(var12 = -var10; var12 <= var10; ++var12) {
                  boolean var13 = var11 == -var10 || var11 == var10 || var12 == -var10 || var12 == var10;
                  class_ahm var14 = new class_ahm(var11 + var8, var12 + var9);
                  if(!this.b.contains(var14)) {
                     ++var5;
                     if(!var13 && var1.aj().a(var14)) {
                        class_lu var15 = var1.w().b(var14.a, var14.b);
                        if(var15 != null && var15.e()) {
                           this.b.add(var14);
                        }
                     }
                  }
               }
            }
         }
      }
   }

   protected static BlockPosition a(World var0, int var1, int var2) {
      Chunk var3 = var0.a(var1, var2);
      int var4 = var1 * 16 + var0.r.nextInt(16);
      int var5 = var2 * 16 + var0.r.nextInt(16);
      int var6 = MathHelper.c(var3.e(new BlockPosition(var4, 0, var5)) + 1, 16);
      int var7 = var0.r.nextInt(var6 > 0?var6:var3.g() + 16 - 1);
      return new BlockPosition(var4, var7, var5);
   }

   public static boolean a(IBlockData var0) {
      return var0.k()?false:(var0.m()?false:(var0.getMaterial().d()?false:!BlockMinecartTrackAbstract.i(var0)));
   }

   public static boolean a(EntityInsentient.EnumEntityPositionType var0, World var1, BlockPosition var2) {
      if(!var1.aj().a(var2)) {
         return false;
      } else {
         IBlockData var3 = var1.getType(var2);
         if(var0 == EntityInsentient.EnumEntityPositionType.IN_WATER) {
            return var3.getMaterial().d() && var1.getType(var2.b()).getMaterial().d() && !var1.getType(var2.a()).l();
         } else {
            BlockPosition var4 = var2.b();
            if(!var1.getType(var4).q()) {
               return false;
            } else {
               Block var5 = var1.getType(var4).getBlock();
               boolean var6 = var5 != Blocks.h && var5 != Blocks.cv;
               return var6 && a(var3) && a(var1.getType(var2.a()));
            }
         }
      }
   }

   public static void a(World var0, BiomeBase var1, int var2, int var3, int var4, int var5, Random var6) {
      List var7 = var1.a(EnumCreatureType.CREATURE);
      if(!var7.isEmpty()) {
         while(var6.nextFloat() < var1.f()) {
            BiomeBase.BiomeMeta var8 = (BiomeBase.BiomeMeta)class_ov.a(var0.r, var7);
            int var9 = var8.c + var6.nextInt(1 + var8.d - var8.c);
            class_sc var10 = null;
            int var11 = var2 + var6.nextInt(var4);
            int var12 = var3 + var6.nextInt(var5);
            int var13 = var11;
            int var14 = var12;

            for(int var15 = 0; var15 < var9; ++var15) {
               boolean var16 = false;

               for(int var17 = 0; !var16 && var17 < 4; ++var17) {
                  BlockPosition var18 = var0.q(new BlockPosition(var11, 0, var12));
                  if(a(EntityInsentient.EnumEntityPositionType.ON_GROUND, var0, var18)) {
                     EntityInsentient var19;
                     try {
                        var19 = (EntityInsentient)var8.b.getConstructor(new Class[]{World.class}).newInstance(new Object[]{var0});
                     } catch (Exception var21) {
                        var21.printStackTrace();
                        continue;
                     }

                     var19.b((double)((float)var11 + 0.5F), (double)var18.q(), (double)((float)var12 + 0.5F), var6.nextFloat() * 360.0F, 0.0F);
                     var0.a((Entity)var19);
                     var10 = var19.a(var0.D(new BlockPosition(var19)), var10);
                     var16 = true;
                  }

                  var11 += var6.nextInt(5) - var6.nextInt(5);

                  for(var12 += var6.nextInt(5) - var6.nextInt(5); var11 < var2 || var11 >= var2 + var4 || var12 < var3 || var12 >= var3 + var4; var12 = var14 + var6.nextInt(5) - var6.nextInt(5)) {
                     var11 = var13 + var6.nextInt(5) - var6.nextInt(5);
                  }
               }
            }
         }

      }
   }
}
