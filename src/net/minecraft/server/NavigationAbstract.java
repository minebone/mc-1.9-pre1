package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.class_aib;
import net.minecraft.server.class_ayn;
import net.minecraft.server.class_ayo;
import net.minecraft.server.class_ayp;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sl;
import net.minecraft.server.class_ys;

public abstract class NavigationAbstract {
   private static int f = 20;
   protected EntityInsentient a;
   protected World b;
   protected class_ayo c;
   protected double d;
   private final class_sl g;
   private int h;
   private int i;
   private Vec3D j = Vec3D.a;
   private Vec3D k = Vec3D.a;
   private long l = 0L;
   private long m = 0L;
   private double n;
   private float o = 0.5F;
   private boolean p;
   private long q;
   protected class_ayn e;
   private BlockPosition r;
   private final class_ayp s;

   public NavigationAbstract(EntityInsentient var1, World var2) {
      this.a = var1;
      this.b = var2;
      this.g = var1.a((class_sk)class_ys.b);
      this.s = this.a();
      this.b.C().a(this);
   }

   protected abstract class_ayp a();

   public void a(double var1) {
      this.d = var1;
   }

   public float h() {
      return (float)this.g.e();
   }

   public boolean i() {
      return this.p;
   }

   public void j() {
      if(this.b.P() - this.q > (long)f) {
         if(this.r != null) {
            this.c = null;
            this.c = this.a(this.r);
            this.q = this.b.P();
            this.p = false;
         }
      } else {
         this.p = true;
      }

   }

   public final class_ayo a(double var1, double var3, double var5) {
      return this.a(new BlockPosition(MathHelper.c(var1), (int)var3, MathHelper.c(var5)));
   }

   public class_ayo a(BlockPosition var1) {
      if(!this.b()) {
         return null;
      } else if(this.c != null && !this.c.b() && var1.equals(this.r)) {
         return this.c;
      } else {
         this.r = var1;
         float var2 = this.h();
         this.b.C.a("pathfind");
         BlockPosition var3 = new BlockPosition(this.a);
         int var4 = (int)(var2 + 8.0F);
         class_aib var5 = new class_aib(this.b, var3.a(-var4, -var4, -var4), var3.a(var4, var4, var4), 0);
         class_ayo var6 = this.s.a(var5, this.a, (BlockPosition)this.r, var2);
         this.b.C.b();
         return var6;
      }
   }

   public class_ayo a(Entity var1) {
      if(!this.b()) {
         return null;
      } else {
         BlockPosition var2 = new BlockPosition(var1);
         if(this.c != null && !this.c.b() && var2.equals(this.r)) {
            return this.c;
         } else {
            this.r = var2;
            float var3 = this.h();
            this.b.C.a("pathfind");
            BlockPosition var4 = (new BlockPosition(this.a)).a();
            int var5 = (int)(var3 + 16.0F);
            class_aib var6 = new class_aib(this.b, var4.a(-var5, -var5, -var5), var4.a(var5, var5, var5), 0);
            class_ayo var7 = this.s.a(var6, this.a, (Entity)var1, var3);
            this.b.C.b();
            return var7;
         }
      }
   }

   public boolean a(double var1, double var3, double var5, double var7) {
      class_ayo var9 = this.a((double)MathHelper.c(var1), (double)((int)var3), (double)MathHelper.c(var5));
      return this.a(var9, var7);
   }

   public boolean a(Entity var1, double var2) {
      class_ayo var4 = this.a(var1);
      return var4 != null?this.a(var4, var2):false;
   }

   public boolean a(class_ayo var1, double var2) {
      if(var1 == null) {
         this.c = null;
         return false;
      } else {
         if(!var1.a(this.c)) {
            this.c = var1;
         }

         this.d();
         if(this.c.d() == 0) {
            return false;
         } else {
            this.d = var2;
            Vec3D var4 = this.c();
            this.i = this.h;
            this.j = var4;
            return true;
         }
      }
   }

   public class_ayo k() {
      return this.c;
   }

   public void l() {
      ++this.h;
      if(this.p) {
         this.j();
      }

      if(!this.n()) {
         Vec3D var1;
         if(this.b()) {
            this.m();
         } else if(this.c != null && this.c.e() < this.c.d()) {
            var1 = this.c();
            Vec3D var2 = this.c.a(this.a, this.c.e());
            if(var1.c > var2.c && !this.a.onGround && MathHelper.c(var1.b) == MathHelper.c(var2.b) && MathHelper.c(var1.d) == MathHelper.c(var2.d)) {
               this.c.c(this.c.e() + 1);
            }
         }

         if(!this.n()) {
            var1 = this.c.a((Entity)this.a);
            if(var1 != null) {
               BlockPosition var4 = (new BlockPosition(var1)).b();
               AxisAlignedBB var3 = this.b.getType(var4).c(this.b, var4);
               var1 = var1.a(0.0D, 1.0D - var3.e, 0.0D);
               this.a.u().a(var1.b, var1.c, var1.d, this.d);
            }
         }
      }
   }

   protected void m() {
      Vec3D var1 = this.c();
      int var2 = this.c.d();

      for(int var3 = this.c.e(); var3 < this.c.d(); ++var3) {
         if((double)this.c.a(var3).b != Math.floor(var1.c)) {
            var2 = var3;
            break;
         }
      }

      this.o = this.a.width > 0.75F?this.a.width / 2.0F:0.75F - this.a.width / 2.0F;
      Vec3D var8 = this.c.f();
      if(MathHelper.e((float)(this.a.locX - (var8.b + 0.5D))) < this.o && MathHelper.e((float)(this.a.locZ - (var8.d + 0.5D))) < this.o) {
         this.c.c(this.c.e() + 1);
      }

      int var4 = MathHelper.f(this.a.width);
      int var5 = (int)this.a.length + 1;
      int var6 = var4;

      for(int var7 = var2 - 1; var7 >= this.c.e(); --var7) {
         if(this.a(var1, this.c.a(this.a, var7), var4, var5, var6)) {
            this.c.c(var7);
            break;
         }
      }

      this.a(var1);
   }

   protected void a(Vec3D var1) {
      if(this.h - this.i > 100) {
         if(var1.g(this.j) < 2.25D) {
            this.o();
         }

         this.i = this.h;
         this.j = var1;
      }

      if(this.c != null && !this.c.b()) {
         Vec3D var2 = this.c.f();
         if(!var2.equals(this.k)) {
            this.k = var2;
            double var3 = var1.f(this.k);
            this.n = this.a.cj() > 0.0F?var3 / (double)this.a.cj() * 1000.0D:0.0D;
         } else {
            this.l += System.currentTimeMillis() - this.m;
         }

         if(this.n > 0.0D && (double)this.l > this.n * 3.0D) {
            this.k = Vec3D.a;
            this.l = 0L;
            this.n = 0.0D;
            this.o();
         }

         this.m = System.currentTimeMillis();
      }

   }

   public boolean n() {
      return this.c == null || this.c.b();
   }

   public void o() {
      this.c = null;
   }

   protected abstract Vec3D c();

   protected abstract boolean b();

   protected boolean p() {
      return this.a.ah() || this.a.am();
   }

   protected void d() {
   }

   protected abstract boolean a(Vec3D var1, Vec3D var2, int var3, int var4, int var5);

   public boolean b(BlockPosition var1) {
      return this.b.getType(var1.b()).b();
   }
}
