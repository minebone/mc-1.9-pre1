package net.minecraft.server;

import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.WorldSettings;
import net.minecraft.server.class_bz;
import net.minecraft.server.ICommandListener;

public class CommandPublish extends CommandAbstract {
   public String c() {
      return "publish";
   }

   public String b(ICommandListener var1) {
      return "commands.publish.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      String var4 = var1.a(WorldSettings.EnumGamemode.SURVIVAL, false);
      if(var4 != null) {
         a(var2, this, "commands.publish.started", new Object[]{var4});
      } else {
         a(var2, this, "commands.publish.failed", new Object[0]);
      }

   }
}
