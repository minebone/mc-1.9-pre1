package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockRepeater;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_akq;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_or;

public class BlockRedstoneWire extends Block {
   public static final BlockStateEnum a = BlockStateEnum.a("north", BlockRedstoneWire.EnumRedstoneWireConnection.class);
   public static final BlockStateEnum b = BlockStateEnum.a("east", BlockRedstoneWire.EnumRedstoneWireConnection.class);
   public static final BlockStateEnum c = BlockStateEnum.a("south", BlockRedstoneWire.EnumRedstoneWireConnection.class);
   public static final BlockStateEnum d = BlockStateEnum.a("west", BlockRedstoneWire.EnumRedstoneWireConnection.class);
   public static final BlockStateInteger e = BlockStateInteger.a("power", 0, 15);
   protected static final AxisAlignedBB[] f = new AxisAlignedBB[]{new AxisAlignedBB(0.1875D, 0.0D, 0.1875D, 0.8125D, 0.0625D, 0.8125D), new AxisAlignedBB(0.1875D, 0.0D, 0.1875D, 0.8125D, 0.0625D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.1875D, 0.8125D, 0.0625D, 0.8125D), new AxisAlignedBB(0.0D, 0.0D, 0.1875D, 0.8125D, 0.0625D, 1.0D), new AxisAlignedBB(0.1875D, 0.0D, 0.0D, 0.8125D, 0.0625D, 0.8125D), new AxisAlignedBB(0.1875D, 0.0D, 0.0D, 0.8125D, 0.0625D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.8125D, 0.0625D, 0.8125D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.8125D, 0.0625D, 1.0D), new AxisAlignedBB(0.1875D, 0.0D, 0.1875D, 1.0D, 0.0625D, 0.8125D), new AxisAlignedBB(0.1875D, 0.0D, 0.1875D, 1.0D, 0.0625D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.1875D, 1.0D, 0.0625D, 0.8125D), new AxisAlignedBB(0.0D, 0.0D, 0.1875D, 1.0D, 0.0625D, 1.0D), new AxisAlignedBB(0.1875D, 0.0D, 0.0D, 1.0D, 0.0625D, 0.8125D), new AxisAlignedBB(0.1875D, 0.0D, 0.0D, 1.0D, 0.0625D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.0625D, 0.8125D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.0625D, 1.0D)};
   private boolean g = true;
   private final Set B = Sets.newHashSet();

   public BlockRedstoneWire() {
      super(Material.q);
      this.w(this.A.b().set(a, BlockRedstoneWire.EnumRedstoneWireConnection.NONE).set(b, BlockRedstoneWire.EnumRedstoneWireConnection.NONE).set(c, BlockRedstoneWire.EnumRedstoneWireConnection.NONE).set(d, BlockRedstoneWire.EnumRedstoneWireConnection.NONE).set(e, Integer.valueOf(0)));
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return f[x(var1.b(var2, var3))];
   }

   private static int x(IBlockData var0) {
      int var1 = 0;
      boolean var2 = var0.get(a) != BlockRedstoneWire.EnumRedstoneWireConnection.NONE;
      boolean var3 = var0.get(b) != BlockRedstoneWire.EnumRedstoneWireConnection.NONE;
      boolean var4 = var0.get(c) != BlockRedstoneWire.EnumRedstoneWireConnection.NONE;
      boolean var5 = var0.get(d) != BlockRedstoneWire.EnumRedstoneWireConnection.NONE;
      if(var2 || var4 && !var2 && !var3 && !var5) {
         var1 |= 1 << EnumDirection.NORTH.b();
      }

      if(var3 || var5 && !var2 && !var3 && !var4) {
         var1 |= 1 << EnumDirection.EAST.b();
      }

      if(var4 || var2 && !var3 && !var4 && !var5) {
         var1 |= 1 << EnumDirection.SOUTH.b();
      }

      if(var5 || var3 && !var2 && !var4 && !var5) {
         var1 |= 1 << EnumDirection.WEST.b();
      }

      return var1;
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      var1 = var1.set(d, this.b(var2, var3, EnumDirection.WEST));
      var1 = var1.set(b, this.b(var2, var3, EnumDirection.EAST));
      var1 = var1.set(a, this.b(var2, var3, EnumDirection.NORTH));
      var1 = var1.set(c, this.b(var2, var3, EnumDirection.SOUTH));
      return var1;
   }

   private BlockRedstoneWire.EnumRedstoneWireConnection b(class_ahw var1, BlockPosition var2, EnumDirection var3) {
      BlockPosition var4 = var2.a(var3);
      IBlockData var5 = var1.getType(var2.a(var3));
      if(!a(var1.getType(var4), var3) && (var5.l() || !i(var1.getType(var4.b())))) {
         IBlockData var6 = var1.getType(var2.a());
         if(!var6.l()) {
            boolean var7 = var1.getType(var4).q() || var1.getType(var4).getBlock() == Blocks.aX;
            if(var7 && i(var1.getType(var4.a()))) {
               if(var5.k()) {
                  return BlockRedstoneWire.EnumRedstoneWireConnection.UP;
               }

               return BlockRedstoneWire.EnumRedstoneWireConnection.SIDE;
            }
         }

         return BlockRedstoneWire.EnumRedstoneWireConnection.NONE;
      } else {
         return BlockRedstoneWire.EnumRedstoneWireConnection.SIDE;
      }
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean a(World var1, BlockPosition var2) {
      return var1.getType(var2.b()).q() || var1.getType(var2.b()).getBlock() == Blocks.aX;
   }

   private IBlockData e(World var1, BlockPosition var2, IBlockData var3) {
      var3 = this.a(var1, var2, var2, var3);
      ArrayList var4 = Lists.newArrayList((Iterable)this.B);
      this.B.clear();
      Iterator var5 = var4.iterator();

      while(var5.hasNext()) {
         BlockPosition var6 = (BlockPosition)var5.next();
         var1.d(var6, this);
      }

      return var3;
   }

   private IBlockData a(World var1, BlockPosition var2, BlockPosition var3, IBlockData var4) {
      IBlockData var5 = var4;
      int var6 = ((Integer)var4.get(e)).intValue();
      byte var7 = 0;
      int var14 = this.a((World)var1, (BlockPosition)var3, var7);
      this.g = false;
      int var8 = var1.z(var2);
      this.g = true;
      if(var8 > 0 && var8 > var14 - 1) {
         var14 = var8;
      }

      int var9 = 0;
      Iterator var10 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

      while(true) {
         while(var10.hasNext()) {
            EnumDirection var11 = (EnumDirection)var10.next();
            BlockPosition var12 = var2.a(var11);
            boolean var13 = var12.p() != var3.p() || var12.r() != var3.r();
            if(var13) {
               var9 = this.a(var1, var12, var9);
            }

            if(var1.getType(var12).l() && !var1.getType(var2.a()).l()) {
               if(var13 && var2.q() >= var3.q()) {
                  var9 = this.a(var1, var12.a(), var9);
               }
            } else if(!var1.getType(var12).l() && var13 && var2.q() <= var3.q()) {
               var9 = this.a(var1, var12.b(), var9);
            }
         }

         if(var9 > var14) {
            var14 = var9 - 1;
         } else if(var14 > 0) {
            --var14;
         } else {
            var14 = 0;
         }

         if(var8 > var14 - 1) {
            var14 = var8;
         }

         if(var6 != var14) {
            var4 = var4.set(e, Integer.valueOf(var14));
            if(var1.getType(var2) == var5) {
               var1.a((BlockPosition)var2, (IBlockData)var4, 2);
            }

            this.B.add(var2);
            EnumDirection[] var15 = EnumDirection.values();
            int var16 = var15.length;

            for(int var17 = 0; var17 < var16; ++var17) {
               EnumDirection var18 = var15[var17];
               this.B.add(var2.a(var18));
            }
         }

         return var4;
      }
   }

   private void b(World var1, BlockPosition var2) {
      if(var1.getType(var2).getBlock() == this) {
         var1.d(var2, this);
         EnumDirection[] var3 = EnumDirection.values();
         int var4 = var3.length;

         for(int var5 = 0; var5 < var4; ++var5) {
            EnumDirection var6 = var3[var5];
            var1.d(var2.a(var6), this);
         }

      }
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      if(!var1.E) {
         this.e(var1, var2, var3);
         Iterator var4 = EnumDirection.EnumDirectionLimit.VERTICAL.iterator();

         EnumDirection var5;
         while(var4.hasNext()) {
            var5 = (EnumDirection)var4.next();
            var1.d(var2.a(var5), this);
         }

         var4 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

         while(var4.hasNext()) {
            var5 = (EnumDirection)var4.next();
            this.b(var1, var2.a(var5));
         }

         var4 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

         while(var4.hasNext()) {
            var5 = (EnumDirection)var4.next();
            BlockPosition var6 = var2.a(var5);
            if(var1.getType(var6).l()) {
               this.b(var1, var6.a());
            } else {
               this.b(var1, var6.b());
            }
         }

      }
   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      super.b(var1, var2, var3);
      if(!var1.E) {
         EnumDirection[] var4 = EnumDirection.values();
         int var5 = var4.length;

         for(int var6 = 0; var6 < var5; ++var6) {
            EnumDirection var7 = var4[var6];
            var1.d(var2.a(var7), this);
         }

         this.e(var1, var2, var3);
         Iterator var8 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

         EnumDirection var9;
         while(var8.hasNext()) {
            var9 = (EnumDirection)var8.next();
            this.b(var1, var2.a(var9));
         }

         var8 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

         while(var8.hasNext()) {
            var9 = (EnumDirection)var8.next();
            BlockPosition var10 = var2.a(var9);
            if(var1.getType(var10).l()) {
               this.b(var1, var10.a());
            } else {
               this.b(var1, var10.b());
            }
         }

      }
   }

   private int a(World var1, BlockPosition var2, int var3) {
      if(var1.getType(var2).getBlock() != this) {
         return var3;
      } else {
         int var4 = ((Integer)var1.getType(var2).get(e)).intValue();
         return var4 > var3?var4:var3;
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!var1.E) {
         if(this.a(var1, var2)) {
            this.e(var1, var2, var3);
         } else {
            this.b(var1, var2, var3, 0);
            var1.g(var2);
         }

      }
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Items.aE;
   }

   public int c(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return !this.g?0:var1.a(var2, var3, var4);
   }

   public int b(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      if(!this.g) {
         return 0;
      } else {
         int var5 = ((Integer)var1.get(e)).intValue();
         if(var5 == 0) {
            return 0;
         } else if(var4 == EnumDirection.UP) {
            return var5;
         } else {
            EnumSet var6 = EnumSet.noneOf(EnumDirection.class);
            Iterator var7 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

            while(var7.hasNext()) {
               EnumDirection var8 = (EnumDirection)var7.next();
               if(this.c(var2, var3, var8)) {
                  var6.add(var8);
               }
            }

            if(var4.k().c() && var6.isEmpty()) {
               return var5;
            } else if(var6.contains(var4) && !var6.contains(var4.f()) && !var6.contains(var4.e())) {
               return var5;
            } else {
               return 0;
            }
         }
      }
   }

   private boolean c(class_ahw var1, BlockPosition var2, EnumDirection var3) {
      BlockPosition var4 = var2.a(var3);
      IBlockData var5 = var1.getType(var4);
      boolean var6 = var5.l();
      boolean var7 = var1.getType(var2.a()).l();
      return !var7 && var6 && c(var1, var4.a())?true:(a(var5, var3)?true:(var5.getBlock() == Blocks.bc && var5.get(class_akq.D) == var3?true:!var6 && c(var1, var4.b())));
   }

   protected static boolean c(class_ahw var0, BlockPosition var1) {
      return i(var0.getType(var1));
   }

   protected static boolean i(IBlockData var0) {
      return a((IBlockData)var0, (EnumDirection)null);
   }

   protected static boolean a(IBlockData var0, EnumDirection var1) {
      Block var2 = var0.getBlock();
      if(var2 == Blocks.af) {
         return true;
      } else if(Blocks.bb.C(var0)) {
         EnumDirection var3 = (EnumDirection)var0.get(BlockRepeater.D);
         return var3 == var1 || var3.d() == var1;
      } else {
         return var0.m() && var1 != null;
      }
   }

   public boolean g(IBlockData var1) {
      return this.g;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Items.aE);
   }

   public IBlockData a(int var1) {
      return this.u().set(e, Integer.valueOf(var1));
   }

   public int e(IBlockData var1) {
      return ((Integer)var1.get(e)).intValue();
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      switch(BlockRedstoneWire.SyntheticClass_1.a[var2.ordinal()]) {
      case 1:
         return var1.set(a, var1.get(c)).set(b, var1.get(d)).set(c, var1.get(a)).set(d, var1.get(b));
      case 2:
         return var1.set(a, var1.get(b)).set(b, var1.get(c)).set(c, var1.get(d)).set(d, var1.get(a));
      case 3:
         return var1.set(a, var1.get(d)).set(b, var1.get(a)).set(c, var1.get(b)).set(d, var1.get(c));
      default:
         return var1;
      }
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      switch(BlockRedstoneWire.SyntheticClass_1.b[var2.ordinal()]) {
      case 1:
         return var1.set(a, var1.get(c)).set(c, var1.get(a));
      case 2:
         return var1.set(b, var1.get(d)).set(d, var1.get(b));
      default:
         return super.a(var1, var2);
      }
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b, c, d, e});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b = new int[class_amq.values().length];

      static {
         try {
            b[class_amq.LEFT_RIGHT.ordinal()] = 1;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            b[class_amq.FRONT_BACK.ordinal()] = 2;
         } catch (NoSuchFieldError var4) {
            ;
         }

         a = new int[class_aod.values().length];

         try {
            a[class_aod.CLOCKWISE_180.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[class_aod.CLOCKWISE_90.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   static enum EnumRedstoneWireConnection implements class_or {
      UP("up"),
      SIDE("side"),
      NONE("none");

      private final String d;

      private EnumRedstoneWireConnection(String var3) {
         this.d = var3;
      }

      public String toString() {
         return this.m();
      }

      public String m() {
         return this.d;
      }
   }
}
