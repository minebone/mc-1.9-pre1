package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumMonsterType;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_to;
import net.minecraft.server.class_tr;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_uu;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_yp;
import net.minecraft.server.class_ys;

public class class_yj extends class_yp {
   private int a = 0;
   private boolean b = false;

   public class_yj(World var1) {
      super(var1);
      this.b_ = 3;
      this.a(0.4F, 0.3F);
   }

   protected void r() {
      this.bp.a(1, new class_tg(this));
      this.bp.a(2, new class_tr(this, 1.0D, false));
      this.bp.a(3, new class_uf(this, 1.0D));
      this.bp.a(7, new class_to(this, EntityHuman.class, 8.0F));
      this.bp.a(8, new class_ue(this));
      this.bq.a(1, new class_uu(this, true, new Class[0]));
      this.bq.a(2, new class_ux(this, EntityHuman.class, true));
   }

   public float bm() {
      return 0.1F;
   }

   protected void bz() {
      super.bz();
      this.a(class_ys.a).a(8.0D);
      this.a(class_ys.d).a(0.25D);
      this.a(class_ys.e).a(2.0D);
   }

   protected boolean ad() {
      return false;
   }

   protected class_nf G() {
      return class_ng.aY;
   }

   protected class_nf bQ() {
      return class_ng.ba;
   }

   protected class_nf bR() {
      return class_ng.aZ;
   }

   protected void a(BlockPosition var1, Block var2) {
      this.a(class_ng.bb, 0.15F, 1.0F);
   }

   protected class_kk J() {
      return class_azs.ag;
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.a = var1.h("Lifetime");
      this.b = var1.p("PlayerSpawned");
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("Lifetime", this.a);
      var1.a("PlayerSpawned", this.b);
   }

   public void m() {
      this.aM = this.yaw;
      super.m();
   }

   public double aw() {
      return 0.3D;
   }

   public boolean o() {
      return this.b;
   }

   public void a(boolean var1) {
      this.b = var1;
   }

   public void n() {
      super.n();
      if(this.world.E) {
         for(int var1 = 0; var1 < 2; ++var1) {
            this.world.a(EnumParticle.PORTAL, this.locX + (this.random.nextDouble() - 0.5D) * (double)this.width, this.locY + this.random.nextDouble() * (double)this.length, this.locZ + (this.random.nextDouble() - 0.5D) * (double)this.width, (this.random.nextDouble() - 0.5D) * 2.0D, -this.random.nextDouble(), (this.random.nextDouble() - 0.5D) * 2.0D, new int[0]);
         }
      } else {
         if(!this.cN()) {
            ++this.a;
         }

         if(this.a >= 2400) {
            this.S();
         }
      }

   }

   protected boolean s_() {
      return true;
   }

   public boolean cF() {
      if(super.cF()) {
         EntityHuman var1 = this.world.a(this, 5.0D);
         return var1 == null;
      } else {
         return false;
      }
   }

   public EnumMonsterType bZ() {
      return EnumMonsterType.ARTHROPOD;
   }
}
