package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Container;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.class_abd;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aqi;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_qg;

public class TileEntityDispenser extends class_aqi implements IInventory {
   private static final Random f = new Random();
   private ItemStack[] g = new ItemStack[9];
   protected String a;

   public int u_() {
      return 9;
   }

   public ItemStack a(int var1) {
      return this.g[var1];
   }

   public ItemStack a(int var1, int var2) {
      ItemStack var3 = class_qg.a(this.g, var1, var2);
      if(var3 != null) {
         this.v_();
      }

      return var3;
   }

   public ItemStack b(int var1) {
      return class_qg.a(this.g, var1);
   }

   public int m() {
      int var1 = -1;
      int var2 = 1;

      for(int var3 = 0; var3 < this.g.length; ++var3) {
         if(this.g[var3] != null && f.nextInt(var2++) == 0) {
            var1 = var3;
         }
      }

      return var1;
   }

   public void a(int var1, ItemStack var2) {
      this.g[var1] = var2;
      if(var2 != null && var2.b > this.w_()) {
         var2.b = this.w_();
      }

      this.v_();
   }

   public int a(ItemStack var1) {
      for(int var2 = 0; var2 < this.g.length; ++var2) {
         if(this.g[var2] == null || this.g[var2].b() == null) {
            this.a(var2, var1);
            return var2;
         }
      }

      return -1;
   }

   public String h_() {
      return this.o_()?this.a:"container.dispenser";
   }

   public void a(String var1) {
      this.a = var1;
   }

   public boolean o_() {
      return this.a != null;
   }

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      NBTTagList var3 = var2.c("Items", 10);
      this.g = new ItemStack[this.u_()];

      for(int var4 = 0; var4 < var3.c(); ++var4) {
         NBTTagCompound var5 = var3.b(var4);
         int var6 = var5.f("Slot") & 255;
         if(var6 >= 0 && var6 < this.g.length) {
            this.g[var6] = ItemStack.a(var5);
         }
      }

      if(var2.b("CustomName", 8)) {
         this.a = var2.l("CustomName");
      }

   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      NBTTagList var2 = new NBTTagList();

      for(int var3 = 0; var3 < this.g.length; ++var3) {
         if(this.g[var3] != null) {
            NBTTagCompound var4 = new NBTTagCompound();
            var4.a("Slot", (byte)var3);
            this.g[var3].b(var4);
            var2.a((NBTTag)var4);
         }
      }

      var1.a((String)"Items", (NBTTag)var2);
      if(this.o_()) {
         var1.a("CustomName", this.a);
      }

   }

   public int w_() {
      return 64;
   }

   public boolean a(EntityHuman var1) {
      return this.b.r(this.c) != this?false:var1.e((double)this.c.p() + 0.5D, (double)this.c.q() + 0.5D, (double)this.c.r() + 0.5D) <= 64.0D;
   }

   public void b(EntityHuman var1) {
   }

   public void c(EntityHuman var1) {
   }

   public boolean b(int var1, ItemStack var2) {
      return true;
   }

   public String k() {
      return "minecraft:dispenser";
   }

   public Container a(PlayerInventory var1, EntityHuman var2) {
      return new class_abd(var1, this);
   }

   public int c_(int var1) {
      return 0;
   }

   public void b(int var1, int var2) {
   }

   public int g() {
      return 0;
   }

   public void l() {
      for(int var1 = 0; var1 < this.g.length; ++var1) {
         this.g[var1] = null;
      }

   }
}
