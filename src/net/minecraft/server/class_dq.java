package net.minecraft.server;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import net.minecraft.server.NBTReadLimiter;
import net.minecraft.server.NBTTag;

public class class_dq extends NBTTag {
   void a(DataInput var1, int var2, NBTReadLimiter var3) throws IOException {
      var3.a(64L);
   }

   void a(DataOutput var1) throws IOException {
   }

   public byte a() {
      return (byte)0;
   }

   public String toString() {
      return "END";
   }

   public NBTTag b() {
      return new class_dq();
   }
}
