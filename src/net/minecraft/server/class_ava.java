package net.minecraft.server;

import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.World;
import net.minecraft.server.class_auc;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_ws;

public class class_ava extends class_auc {
   private boolean a = false;
   private class_ava.class_a_in_class_ava b = null;
   private BlockPosition c;

   public void a(class_ava.class_a_in_class_ava var1) {
      this.b = var1;
   }

   public void a(boolean var1) {
      this.a = var1;
   }

   public boolean b(World var1, Random var2, BlockPosition var3) {
      if(this.b == null) {
         throw new IllegalStateException("Decoration requires priming with a spike");
      } else {
         int var4 = this.b.c();
         Iterator var5 = BlockPosition.b(new BlockPosition(var3.p() - var4, 0, var3.r() - var4), new BlockPosition(var3.p() + var4, this.b.d() + 10, var3.r() + var4)).iterator();

         while(true) {
            while(var5.hasNext()) {
               BlockPosition.class_a_in_class_cj var6 = (BlockPosition.class_a_in_class_cj)var5.next();
               if(var6.e((double)var3.p(), (double)var6.q(), (double)var3.r()) <= (double)(var4 * var4 + 1) && var6.q() < this.b.d()) {
                  this.a(var1, var6, Blocks.Z.u());
               } else if(var6.q() > 65) {
                  this.a(var1, var6, Blocks.AIR.u());
               }
            }

            if(this.b.e()) {
               for(int var7 = -2; var7 <= 2; ++var7) {
                  for(int var9 = -2; var9 <= 2; ++var9) {
                     if(MathHelper.a(var7) == 2 || MathHelper.a(var9) == 2) {
                        this.a(var1, new BlockPosition(var3.p() + var7, this.b.d(), var3.r() + var9), Blocks.bi.u());
                        this.a(var1, new BlockPosition(var3.p() + var7, this.b.d() + 1, var3.r() + var9), Blocks.bi.u());
                        this.a(var1, new BlockPosition(var3.p() + var7, this.b.d() + 2, var3.r() + var9), Blocks.bi.u());
                     }

                     this.a(var1, new BlockPosition(var3.p() + var7, this.b.d() + 3, var3.r() + var9), Blocks.bi.u());
                  }
               }
            }

            class_ws var8 = new class_ws(var1);
            var8.a(this.c);
            var8.h(this.a);
            var8.b((double)((float)var3.p() + 0.5F), (double)(this.b.d() + 1), (double)((float)var3.r() + 0.5F), var2.nextFloat() * 360.0F, 0.0F);
            var1.a((Entity)var8);
            this.a(var1, new BlockPosition(var3.p(), this.b.d(), var3.r()), Blocks.h.u());
            return true;
         }
      }
   }

   public void a(BlockPosition var1) {
      this.c = var1;
   }

   public static class class_a_in_class_ava {
      private final int a;
      private final int b;
      private final int c;
      private final int d;
      private final boolean e;
      private final AxisAlignedBB f;

      public class_a_in_class_ava(int var1, int var2, int var3, int var4, boolean var5) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
         this.e = var5;
         this.f = new AxisAlignedBB((double)(var1 - var3), 0.0D, (double)(var2 - var3), (double)(var1 + var3), 256.0D, (double)(var2 + var3));
      }

      public boolean a(BlockPosition var1) {
         int var2 = this.a - this.c;
         int var3 = this.b - this.c;
         return var1.p() == (var2 & -16) && var1.r() == (var3 & -16);
      }

      public int a() {
         return this.a;
      }

      public int b() {
         return this.b;
      }

      public int c() {
         return this.c;
      }

      public int d() {
         return this.d;
      }

      public boolean e() {
         return this.e;
      }

      public AxisAlignedBB f() {
         return this.f;
      }
   }
}
