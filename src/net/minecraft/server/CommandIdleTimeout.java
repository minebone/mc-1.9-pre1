package net.minecraft.server;

import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.ICommandListener;

public class CommandIdleTimeout extends CommandAbstract {
   public String c() {
      return "setidletimeout";
   }

   public int a() {
      return 3;
   }

   public String b(ICommandListener var1) {
      return "commands.setidletimeout.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length != 1) {
         throw new class_cf("commands.setidletimeout.usage", new Object[0]);
      } else {
         int var4 = a(var3[0], 0);
         var1.d(var4);
         a(var2, this, "commands.setidletimeout.success", new Object[]{Integer.valueOf(var4)});
      }
   }
}
