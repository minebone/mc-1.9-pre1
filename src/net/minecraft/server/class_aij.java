package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.CrashReport;
import net.minecraft.server.CrashReportSystemDetails;
import net.minecraft.server.IntCache;
import net.minecraft.server.WorldData;
import net.minecraft.server.WorldType;
import net.minecraft.server.class_aig;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_axu;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_e;

public class class_aij {
   private class_axu a;
   private class_axu b;
   private final class_aig c;
   private final List d;

   protected class_aij() {
      this.c = new class_aig(this);
      this.d = Lists.newArrayList((Object[])(new BiomeBase[]{class_aik.f, class_aik.c, class_aik.g, class_aik.u, class_aik.t, class_aik.w, class_aik.x}));
   }

   private class_aij(long var1, WorldType var3, String var4) {
      this();
      class_axu[] var5 = class_axu.a(var1, var3, var4);
      this.a = var5[0];
      this.b = var5[1];
   }

   public class_aij(WorldData var1) {
      this(var1.a(), var1.t(), var1.A());
   }

   public List a() {
      return this.d;
   }

   public BiomeBase a(BlockPosition var1) {
      return this.a(var1, (BiomeBase)null);
   }

   public BiomeBase a(BlockPosition var1, BiomeBase var2) {
      return this.c.a(var1.p(), var1.r(), var2);
   }

   public float a(float var1, int var2) {
      return var1;
   }

   public BiomeBase[] a(BiomeBase[] var1, int var2, int var3, int var4, int var5) {
      IntCache.a();
      if(var1 == null || var1.length < var4 * var5) {
         var1 = new BiomeBase[var4 * var5];
      }

      int[] var6 = this.a.a(var2, var3, var4, var5);

      try {
         for(int var7 = 0; var7 < var4 * var5; ++var7) {
            var1[var7] = BiomeBase.a(var6[var7], class_aik.b);
         }

         return var1;
      } catch (Throwable var10) {
         CrashReport var8 = CrashReport.a(var10, "Invalid Biome id");
         CrashReportSystemDetails var9 = var8.a("RawBiomeBlock");
         var9.a((String)"biomes[] size", (Object)Integer.valueOf(var1.length));
         var9.a((String)"x", (Object)Integer.valueOf(var2));
         var9.a((String)"z", (Object)Integer.valueOf(var3));
         var9.a((String)"w", (Object)Integer.valueOf(var4));
         var9.a((String)"h", (Object)Integer.valueOf(var5));
         throw new class_e(var8);
      }
   }

   public BiomeBase[] b(BiomeBase[] var1, int var2, int var3, int var4, int var5) {
      return this.a(var1, var2, var3, var4, var5, true);
   }

   public BiomeBase[] a(BiomeBase[] var1, int var2, int var3, int var4, int var5, boolean var6) {
      IntCache.a();
      if(var1 == null || var1.length < var4 * var5) {
         var1 = new BiomeBase[var4 * var5];
      }

      if(var6 && var4 == 16 && var5 == 16 && (var2 & 15) == 0 && (var3 & 15) == 0) {
         BiomeBase[] var9 = this.c.b(var2, var3);
         System.arraycopy(var9, 0, var1, 0, var4 * var5);
         return var1;
      } else {
         int[] var7 = this.b.a(var2, var3, var4, var5);

         for(int var8 = 0; var8 < var4 * var5; ++var8) {
            var1[var8] = BiomeBase.a(var7[var8], class_aik.b);
         }

         return var1;
      }
   }

   public boolean a(int var1, int var2, int var3, List var4) {
      IntCache.a();
      int var5 = var1 - var3 >> 2;
      int var6 = var2 - var3 >> 2;
      int var7 = var1 + var3 >> 2;
      int var8 = var2 + var3 >> 2;
      int var9 = var7 - var5 + 1;
      int var10 = var8 - var6 + 1;
      int[] var11 = this.a.a(var5, var6, var9, var10);

      try {
         for(int var12 = 0; var12 < var9 * var10; ++var12) {
            BiomeBase var16 = BiomeBase.b(var11[var12]);
            if(!var4.contains(var16)) {
               return false;
            }
         }

         return true;
      } catch (Throwable var15) {
         CrashReport var13 = CrashReport.a(var15, "Invalid Biome id");
         CrashReportSystemDetails var14 = var13.a("Layer");
         var14.a((String)"Layer", (Object)this.a.toString());
         var14.a((String)"x", (Object)Integer.valueOf(var1));
         var14.a((String)"z", (Object)Integer.valueOf(var2));
         var14.a((String)"radius", (Object)Integer.valueOf(var3));
         var14.a((String)"allowed", (Object)var4);
         throw new class_e(var13);
      }
   }

   public BlockPosition a(int var1, int var2, int var3, List var4, Random var5) {
      IntCache.a();
      int var6 = var1 - var3 >> 2;
      int var7 = var2 - var3 >> 2;
      int var8 = var1 + var3 >> 2;
      int var9 = var2 + var3 >> 2;
      int var10 = var8 - var6 + 1;
      int var11 = var9 - var7 + 1;
      int[] var12 = this.a.a(var6, var7, var10, var11);
      BlockPosition var13 = null;
      int var14 = 0;

      for(int var15 = 0; var15 < var10 * var11; ++var15) {
         int var16 = var6 + var15 % var10 << 2;
         int var17 = var7 + var15 / var10 << 2;
         BiomeBase var18 = BiomeBase.b(var12[var15]);
         if(var4.contains(var18) && (var13 == null || var5.nextInt(var14 + 1) == 0)) {
            var13 = new BlockPosition(var16, 0, var17);
            ++var14;
         }
      }

      return var13;
   }

   public void b() {
      this.c.a();
   }
}
