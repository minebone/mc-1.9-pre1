package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockDoor;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.class_aym;
import net.minecraft.server.class_ayo;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_vd;

public abstract class PathfinderGoalDoorInteract extends class_tj {
   protected EntityInsentient a;
   protected BlockPosition b = BlockPosition.a;
   protected BlockDoor c;
   boolean d;
   float e;
   float f;

   public PathfinderGoalDoorInteract(EntityInsentient var1) {
      this.a = var1;
      if(!(var1.x() instanceof class_vd)) {
         throw new IllegalArgumentException("Unsupported mob type for DoorInteractGoal");
      }
   }

   public boolean a() {
      if(!this.a.positionChanged) {
         return false;
      } else {
         class_vd var1 = (class_vd)this.a.x();
         class_ayo var2 = var1.k();
         if(var2 != null && !var2.b() && var1.f()) {
            for(int var3 = 0; var3 < Math.min(var2.e() + 2, var2.d()); ++var3) {
               class_aym var4 = var2.a(var3);
               this.b = new BlockPosition(var4.a, var4.b + 1, var4.c);
               if(this.a.e((double)this.b.p(), this.a.locY, (double)this.b.r()) <= 2.25D) {
                  this.c = this.a(this.b);
                  if(this.c != null) {
                     return true;
                  }
               }
            }

            this.b = (new BlockPosition(this.a)).a();
            this.c = this.a(this.b);
            return this.c != null;
         } else {
            return false;
         }
      }
   }

   public boolean b() {
      return !this.d;
   }

   public void c() {
      this.d = false;
      this.e = (float)((double)((float)this.b.p() + 0.5F) - this.a.locX);
      this.f = (float)((double)((float)this.b.r() + 0.5F) - this.a.locZ);
   }

   public void e() {
      float var1 = (float)((double)((float)this.b.p() + 0.5F) - this.a.locX);
      float var2 = (float)((double)((float)this.b.r() + 0.5F) - this.a.locZ);
      float var3 = this.e * var1 + this.f * var2;
      if(var3 < 0.0F) {
         this.d = true;
      }

   }

   private BlockDoor a(BlockPosition var1) {
      IBlockData var2 = this.a.world.getType(var1);
      Block var3 = var2.getBlock();
      return var3 instanceof BlockDoor && var2.getMaterial() == Material.d?(BlockDoor)var3:null;
   }
}
