package net.minecraft.server;

import net.minecraft.server.Item;
import net.minecraft.server.ItemArmor;
import net.minecraft.server.ItemTool;
import net.minecraft.server.class_acg;
import net.minecraft.server.class_adh;
import net.minecraft.server.class_aew;
import net.minecraft.server.EnumInventorySlot;

public enum class_agm {
   ALL,
   ARMOR,
   ARMOR_FEET,
   ARMOR_LEGS,
   ARMOR_CHEST,
   ARMOR_HEAD,
   WEAPON,
   DIGGER,
   FISHING_ROD,
   BREAKABLE,
   BOW;

   public boolean a(Item var1) {
      if(this == ALL) {
         return true;
      } else if(this == BREAKABLE && var1.m()) {
         return true;
      } else if(var1 instanceof ItemArmor) {
         if(this == ARMOR) {
            return true;
         } else {
            ItemArmor var2 = (ItemArmor)var1;
            return var2.c == EnumInventorySlot.HEAD?this == ARMOR_HEAD:(var2.c == EnumInventorySlot.LEGS?this == ARMOR_LEGS:(var2.c == EnumInventorySlot.CHEST?this == ARMOR_CHEST:(var2.c == EnumInventorySlot.FEET?this == ARMOR_FEET:false)));
         }
      } else {
         return var1 instanceof class_aew?this == WEAPON:(var1 instanceof ItemTool?this == DIGGER:(var1 instanceof class_acg?this == BOW:(var1 instanceof class_adh?this == FISHING_ROD:false)));
      }
   }
}
