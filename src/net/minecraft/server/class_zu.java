package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityFireball;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.World;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_rz;

public class class_zu extends EntityFireball {
   public class_zu(World var1) {
      super(var1);
      this.a(0.3125F, 0.3125F);
   }

   public class_zu(World var1, class_rz var2, double var3, double var5, double var7) {
      super(var1, var2, var3, var5, var7);
      this.a(0.3125F, 0.3125F);
   }

   public class_zu(World var1, double var2, double var4, double var6, double var8, double var10, double var12) {
      super(var1, var2, var4, var6, var8, var10, var12);
      this.a(0.3125F, 0.3125F);
   }

   protected void a(MovingObjectPosition var1) {
      if(!this.world.E) {
         boolean var2;
         if(var1.d != null) {
            if(!var1.d.af()) {
               var2 = var1.d.a(DamageSource.a((EntityFireball)this, (Entity)this.a), 5.0F);
               if(var2) {
                  this.a(this.a, var1.d);
                  var1.d.g(5);
               }
            }
         } else {
            var2 = true;
            if(this.a != null && this.a instanceof EntityInsentient) {
               var2 = this.world.U().b("mobGriefing");
            }

            if(var2) {
               BlockPosition var3 = var1.a().a(var1.b);
               if(this.world.d(var3)) {
                  this.world.a(var3, Blocks.ab.u());
               }
            }
         }

         this.S();
      }

   }

   public boolean ao() {
      return false;
   }

   public boolean a(DamageSource var1, float var2) {
      return false;
   }
}
