package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Entity;
import net.minecraft.server.Village;
import net.minecraft.server.World;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_rn;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_zd;

public class class_tq extends class_tj {
   private class_zd b;
   private class_zd c;
   private World d;
   private int e;
   Village a;

   public class_tq(class_zd var1) {
      this.b = var1;
      this.d = var1.world;
      this.a(3);
   }

   public boolean a() {
      if(this.b.l() != 0) {
         return false;
      } else if(this.b.bE().nextInt(500) != 0) {
         return false;
      } else {
         this.a = this.d.ai().a(new BlockPosition(this.b), 0);
         if(this.a == null) {
            return false;
         } else if(this.f() && this.b.q(true)) {
            Entity var1 = this.d.a((Class)class_zd.class, (AxisAlignedBB)this.b.bk().b(8.0D, 3.0D, 8.0D), (Entity)this.b);
            if(var1 == null) {
               return false;
            } else {
               this.c = (class_zd)var1;
               return this.c.l() == 0 && this.c.q(true);
            }
         } else {
            return false;
         }
      }
   }

   public void c() {
      this.e = 300;
      this.b.o(true);
   }

   public void d() {
      this.a = null;
      this.c = null;
      this.b.o(false);
   }

   public boolean b() {
      return this.e >= 0 && this.f() && this.b.l() == 0 && this.b.q(false);
   }

   public void e() {
      --this.e;
      this.b.t().a(this.c, 10.0F, 30.0F);
      if(this.b.h(this.c) > 2.25D) {
         this.b.x().a((Entity)this.c, 0.25D);
      } else if(this.e == 0 && this.c.da()) {
         this.i();
      }

      if(this.b.bE().nextInt(35) == 0) {
         this.d.a((Entity)this.b, (byte)12);
      }

   }

   private boolean f() {
      if(!this.a.i()) {
         return false;
      } else {
         int var1 = (int)((double)((float)this.a.c()) * 0.35D);
         return this.a.e() < var1;
      }
   }

   private void i() {
      class_zd var1 = this.b.b((class_rn)this.c);
      this.c.b_(6000);
      this.b.b_(6000);
      this.c.r(false);
      this.b.r(false);
      var1.b_(-24000);
      var1.b(this.b.locX, this.b.locY, this.b.locZ, 0.0F, 0.0F);
      this.d.a((Entity)var1);
      this.d.a((Entity)var1, (byte)12);
   }
}
