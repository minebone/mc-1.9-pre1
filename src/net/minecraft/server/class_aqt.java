package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockPistonExtension;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.TileEntity;
import net.minecraft.server.TileEntityPiston;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_akr;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_aoo;
import net.minecraft.server.class_aqv;
import net.minecraft.server.class_aqx;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.class_axg;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rz;

public class class_aqt extends class_akr {
   public static final class_arm a = class_arm.a("extended");
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.75D, 1.0D, 1.0D);
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.25D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
   protected static final AxisAlignedBB d = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.75D);
   protected static final AxisAlignedBB e = new AxisAlignedBB(0.0D, 0.0D, 0.25D, 1.0D, 1.0D, 1.0D);
   protected static final AxisAlignedBB f = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.75D, 1.0D);
   protected static final AxisAlignedBB g = new AxisAlignedBB(0.0D, 0.25D, 0.0D, 1.0D, 1.0D, 1.0D);
   private final boolean B;

   public class_aqt(boolean var1) {
      super(Material.H);
      this.w(this.A.b().set(H, EnumDirection.NORTH).set(a, Boolean.valueOf(false)));
      this.B = var1;
      this.a(class_aoo.d);
      this.c(0.5F);
      this.a(CreativeModeTab.d);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      if(((Boolean)var1.get(a)).booleanValue()) {
         switch(class_aqt.SyntheticClass_1.a[((EnumDirection)var1.get(H)).ordinal()]) {
         case 1:
            return g;
         case 2:
         default:
            return f;
         case 3:
            return e;
         case 4:
            return d;
         case 5:
            return c;
         case 6:
            return b;
         }
      } else {
         return j;
      }
   }

   public boolean k(IBlockData var1) {
      return !((Boolean)var1.get(a)).booleanValue() || var1.get(H) == EnumDirection.DOWN;
   }

   public void a(IBlockData var1, World var2, BlockPosition var3, AxisAlignedBB var4, List var5, Entity var6) {
      a(var3, var4, var5, var1.c(var2, var3));
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, class_rz var4, ItemStack var5) {
      var1.a((BlockPosition)var2, (IBlockData)var3.set(H, a(var2, var4)), 2);
      if(!var1.E) {
         this.e(var1, var2, var3);
      }

   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!var1.E) {
         this.e(var1, var2, var3);
      }

   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      if(!var1.E && var1.r(var2) == null) {
         this.e(var1, var2, var3);
      }

   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return this.u().set(H, a(var2, var8)).set(a, Boolean.valueOf(false));
   }

   private void e(World var1, BlockPosition var2, IBlockData var3) {
      EnumDirection var4 = (EnumDirection)var3.get(H);
      boolean var5 = this.a(var1, var2, var4);
      if(var5 && !((Boolean)var3.get(a)).booleanValue()) {
         if((new class_aqx(var1, var2, var4, true)).a()) {
            var1.c(var2, this, 0, var4.a());
         }
      } else if(!var5 && ((Boolean)var3.get(a)).booleanValue()) {
         var1.c(var2, this, 1, var4.a());
      }

   }

   private boolean a(World var1, BlockPosition var2, EnumDirection var3) {
      EnumDirection[] var4 = EnumDirection.values();
      int var5 = var4.length;

      int var6;
      for(var6 = 0; var6 < var5; ++var6) {
         EnumDirection var7 = var4[var6];
         if(var7 != var3 && var1.b(var2.a(var7), var7)) {
            return true;
         }
      }

      if(var1.b(var2, EnumDirection.DOWN)) {
         return true;
      } else {
         BlockPosition var9 = var2.a();
         EnumDirection[] var10 = EnumDirection.values();
         var6 = var10.length;

         for(int var11 = 0; var11 < var6; ++var11) {
            EnumDirection var8 = var10[var11];
            if(var8 != EnumDirection.DOWN && var1.b(var9.a(var8), var8)) {
               return true;
            }
         }

         return false;
      }
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, int var4, int var5) {
      EnumDirection var6 = (EnumDirection)var3.get(H);
      if(!var1.E) {
         boolean var7 = this.a(var1, var2, var6);
         if(var7 && var4 == 1) {
            var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Boolean.valueOf(true)), 2);
            return false;
         }

         if(!var7 && var4 == 0) {
            return false;
         }
      }

      if(var4 == 0) {
         if(!this.a(var1, var2, var6, true)) {
            return false;
         }

         var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Boolean.valueOf(true)), 2);
         var1.a((EntityHuman)null, var2, class_ng.dR, EnumSoundCategory.BLOCKS, 0.5F, var1.r.nextFloat() * 0.25F + 0.6F);
      } else if(var4 == 1) {
         TileEntity var14 = var1.r(var2.a(var6));
         if(var14 instanceof TileEntityPiston) {
            ((TileEntityPiston)var14).h();
         }

         var1.a((BlockPosition)var2, (IBlockData)Blocks.M.u().set(class_aqv.a, var6).set(class_aqv.b, this.B?BlockPistonExtension.EnumPistonType.STICKY:BlockPistonExtension.EnumPistonType.DEFAULT), 3);
         var1.a(var2, class_aqv.a(this.a(var5), var6, false, true));
         if(this.B) {
            BlockPosition var8 = var2.a(var6.g() * 2, var6.h() * 2, var6.i() * 2);
            IBlockData var9 = var1.getType(var8);
            Block var10 = var9.getBlock();
            boolean var11 = false;
            if(var10 == Blocks.M) {
               TileEntity var12 = var1.r(var8);
               if(var12 instanceof TileEntityPiston) {
                  TileEntityPiston var13 = (TileEntityPiston)var12;
                  if(var13.e() == var6 && var13.d()) {
                     var13.h();
                     var11 = true;
                  }
               }
            }

            if(!var11 && var9.getMaterial() != Material.a && a(var9, var1, var8, var6.d(), false) && (var9.o() == class_axg.NORMAL || var10 == Blocks.J || var10 == Blocks.F)) {
               this.a(var1, var2, var6, false);
            }
         } else {
            var1.g(var2.a(var6));
         }

         var1.a((EntityHuman)null, var2, class_ng.dQ, EnumSoundCategory.BLOCKS, 0.5F, var1.r.nextFloat() * 0.15F + 0.6F);
      }

      return true;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public static EnumDirection e(int var0) {
      int var1 = var0 & 7;
      return var1 > 5?null:EnumDirection.a(var1);
   }

   public static EnumDirection a(BlockPosition var0, class_rz var1) {
      if(MathHelper.e((float)var1.locX - (float)var0.p()) < 2.0F && MathHelper.e((float)var1.locZ - (float)var0.r()) < 2.0F) {
         double var2 = var1.locY + (double)var1.bm();
         if(var2 - (double)var0.q() > 2.0D) {
            return EnumDirection.UP;
         }

         if((double)var0.q() - var2 > 0.0D) {
            return EnumDirection.DOWN;
         }
      }

      return var1.bh().d();
   }

   public static boolean a(IBlockData var0, World var1, BlockPosition var2, EnumDirection var3, boolean var4) {
      Block var5 = var0.getBlock();
      if(var5 == Blocks.Z) {
         return false;
      } else if(!var1.aj().a(var2)) {
         return false;
      } else if(var2.q() < 0 || var3 == EnumDirection.DOWN && var2.q() == 0) {
         return false;
      } else if(var2.q() > var1.Y() - 1 || var3 == EnumDirection.UP && var2.q() == var1.Y() - 1) {
         return false;
      } else {
         if(var5 != Blocks.J && var5 != Blocks.F) {
            if(var0.b(var1, var2) == -1.0F) {
               return false;
            }

            if(var0.o() == class_axg.BLOCK) {
               return false;
            }

            if(var0.o() == class_axg.DESTROY) {
               return var4;
            }
         } else if(((Boolean)var0.get(a)).booleanValue()) {
            return false;
         }

         return !var5.m();
      }
   }

   private boolean a(World var1, BlockPosition var2, EnumDirection var3, boolean var4) {
      if(!var4) {
         var1.g(var2.a(var3));
      }

      class_aqx var5 = new class_aqx(var1, var2, var3, var4);
      if(!var5.a()) {
         return false;
      } else {
         List var6 = var5.c();
         ArrayList var7 = Lists.newArrayList();

         for(int var8 = 0; var8 < var6.size(); ++var8) {
            BlockPosition var9 = (BlockPosition)var6.get(var8);
            var7.add(var1.getType(var9).b(var1, var9));
         }

         List var16 = var5.d();
         int var17 = var6.size() + var16.size();
         IBlockData[] var10 = new IBlockData[var17];
         EnumDirection var11 = var4?var3:var3.d();

         int var12;
         BlockPosition var13;
         IBlockData var14;
         for(var12 = var16.size() - 1; var12 >= 0; --var12) {
            var13 = (BlockPosition)var16.get(var12);
            var14 = var1.getType(var13);
            var14.getBlock().b(var1, var13, var14, 0);
            var1.g(var13);
            --var17;
            var10[var17] = var14;
         }

         for(var12 = var6.size() - 1; var12 >= 0; --var12) {
            var13 = (BlockPosition)var6.get(var12);
            var14 = var1.getType(var13);
            var1.a((BlockPosition)var13, (IBlockData)Blocks.AIR.u(), 4);
            var13 = var13.a(var11);
            var1.a((BlockPosition)var13, (IBlockData)Blocks.M.u().set(H, var3), 4);
            var1.a(var13, class_aqv.a((IBlockData)var7.get(var12), var3, var4, false));
            --var17;
            var10[var17] = var14;
         }

         BlockPosition var18 = var2.a(var3);
         if(var4) {
            BlockPistonExtension.EnumPistonType var19 = this.B?BlockPistonExtension.EnumPistonType.STICKY:BlockPistonExtension.EnumPistonType.DEFAULT;
            var14 = Blocks.K.u().set(BlockPistonExtension.H, var3).set(BlockPistonExtension.a, var19);
            IBlockData var15 = Blocks.M.u().set(class_aqv.a, var3).set(class_aqv.b, this.B?BlockPistonExtension.EnumPistonType.STICKY:BlockPistonExtension.EnumPistonType.DEFAULT);
            var1.a((BlockPosition)var18, (IBlockData)var15, 4);
            var1.a(var18, class_aqv.a(var14, var3, true, false));
         }

         int var20;
         for(var20 = var16.size() - 1; var20 >= 0; --var20) {
            var1.d((BlockPosition)var16.get(var20), var10[var17++].getBlock());
         }

         for(var20 = var6.size() - 1; var20 >= 0; --var20) {
            var1.d((BlockPosition)var6.get(var20), var10[var17++].getBlock());
         }

         if(var4) {
            var1.d(var18, Blocks.K);
            var1.d(var2, this);
         }

         return true;
      }
   }

   public IBlockData a(int var1) {
      return this.u().set(H, e(var1)).set(a, Boolean.valueOf((var1 & 8) > 0));
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((EnumDirection)var1.get(H)).a();
      if(((Boolean)var1.get(a)).booleanValue()) {
         var3 |= 8;
      }

      return var3;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(H, var2.a((EnumDirection)var1.get(H)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(H)));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{H, a});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumDirection.values().length];

      static {
         try {
            a[EnumDirection.DOWN.ordinal()] = 1;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[EnumDirection.UP.ordinal()] = 2;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[EnumDirection.NORTH.ordinal()] = 3;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.SOUTH.ordinal()] = 4;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 5;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.EAST.ordinal()] = 6;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
