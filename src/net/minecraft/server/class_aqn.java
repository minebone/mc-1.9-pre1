package net.minecraft.server;

import com.google.common.collect.Iterables;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import com.mojang.authlib.properties.Property;
import java.util.UUID;
import net.minecraft.server.GameProfileSerializer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.UserCache;
import net.minecraft.server.UtilColor;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutTileEntityData;
import net.minecraft.server.class_ky;

public class class_aqn extends TileEntity implements class_ky {
   private int a;
   private int f;
   private GameProfile g = null;
   private int h;
   private boolean i;
   private static UserCache j;
   private static MinecraftSessionService k;

   public static void a(UserCache var0) {
      j = var0;
   }

   public static void a(MinecraftSessionService var0) {
      k = var0;
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      var1.a("SkullType", (byte)(this.a & 255));
      var1.a("Rot", (byte)(this.f & 255));
      if(this.g != null) {
         NBTTagCompound var2 = new NBTTagCompound();
         GameProfileSerializer.a(var2, this.g);
         var1.a((String)"Owner", (NBTTag)var2);
      }

   }

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      this.a = var2.f("SkullType");
      this.f = var2.f("Rot");
      if(this.a == 3) {
         if(var2.b("Owner", 10)) {
            this.g = GameProfileSerializer.a(var2.o("Owner"));
         } else if(var2.b("ExtraType", 8)) {
            String var3 = var2.l("ExtraType");
            if(!UtilColor.b(var3)) {
               this.g = new GameProfile((UUID)null, var3);
               this.g();
            }
         }
      }

   }

   public void c() {
      if(this.a == 5) {
         if(this.b.y(this.c)) {
            this.i = true;
            ++this.h;
         } else {
            this.i = false;
         }
      }

   }

   public GameProfile b() {
      return this.g;
   }

   public Packet D_() {
      NBTTagCompound var1 = new NBTTagCompound();
      this.a(var1);
      return new PacketPlayOutTileEntityData(this.c, 4, var1);
   }

   public void a(int var1) {
      this.a = var1;
      this.g = null;
   }

   public void a(GameProfile var1) {
      this.a = 3;
      this.g = var1;
      this.g();
   }

   private void g() {
      this.g = b(this.g);
      this.v_();
   }

   public static GameProfile b(GameProfile var0) {
      if(var0 != null && !UtilColor.b(var0.getName())) {
         if(var0.isComplete() && var0.getProperties().containsKey("textures")) {
            return var0;
         } else if(j != null && k != null) {
            GameProfile var1 = j.a(var0.getName());
            if(var1 == null) {
               return var0;
            } else {
               Property var2 = (Property)Iterables.getFirst(var1.getProperties().get("textures"), (Object)null);
               if(var2 == null) {
                  var1 = k.fillProfileProperties(var1, true);
               }

               return var1;
            }
         } else {
            return var0;
         }
      } else {
         return var0;
      }
   }

   public int d() {
      return this.a;
   }

   public void b(int var1) {
      this.f = var1;
   }
}
