package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockLeaves;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.class_ane;
import net.minecraft.server.class_anf;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ato;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_aut extends class_ato {
   private static final IBlockData a = Blocks.r.u().set(class_anf.b, BlockWood.EnumLogVariant.SPRUCE);
   private static final IBlockData b = Blocks.t.u().set(class_ane.e, BlockWood.EnumLogVariant.SPRUCE).set(BlockLeaves.b, Boolean.valueOf(false));

   public class_aut() {
      super(false);
   }

   public boolean b(World var1, Random var2, BlockPosition var3) {
      int var4 = var2.nextInt(5) + 7;
      int var5 = var4 - var2.nextInt(2) - 3;
      int var6 = var4 - var5;
      int var7 = 1 + var2.nextInt(var6 + 1);
      boolean var8 = true;
      if(var3.q() >= 1 && var3.q() + var4 + 1 <= 256) {
         int var12;
         int var13;
         int var18;
         for(int var9 = var3.q(); var9 <= var3.q() + 1 + var4 && var8; ++var9) {
            boolean var10 = true;
            if(var9 - var3.q() < var5) {
               var18 = 0;
            } else {
               var18 = var7;
            }

            BlockPosition.class_a_in_class_cj var11 = new BlockPosition.class_a_in_class_cj();

            for(var12 = var3.p() - var18; var12 <= var3.p() + var18 && var8; ++var12) {
               for(var13 = var3.r() - var18; var13 <= var3.r() + var18 && var8; ++var13) {
                  if(var9 >= 0 && var9 < 256) {
                     if(!this.a(var1.getType(var11.c(var12, var9, var13)).getBlock())) {
                        var8 = false;
                     }
                  } else {
                     var8 = false;
                  }
               }
            }
         }

         if(!var8) {
            return false;
         } else {
            Block var17 = var1.getType(var3.b()).getBlock();
            if((var17 == Blocks.c || var17 == Blocks.d) && var3.q() < 256 - var4 - 1) {
               this.a(var1, var3.b());
               var18 = 0;

               int var19;
               for(var19 = var3.q() + var4; var19 >= var3.q() + var5; --var19) {
                  for(var12 = var3.p() - var18; var12 <= var3.p() + var18; ++var12) {
                     var13 = var12 - var3.p();

                     for(int var14 = var3.r() - var18; var14 <= var3.r() + var18; ++var14) {
                        int var15 = var14 - var3.r();
                        if(Math.abs(var13) != var18 || Math.abs(var15) != var18 || var18 <= 0) {
                           BlockPosition var16 = new BlockPosition(var12, var19, var14);
                           if(!var1.getType(var16).b()) {
                              this.a(var1, var16, b);
                           }
                        }
                     }
                  }

                  if(var18 >= 1 && var19 == var3.q() + var5 + 1) {
                     --var18;
                  } else if(var18 < var7) {
                     ++var18;
                  }
               }

               for(var19 = 0; var19 < var4 - 1; ++var19) {
                  Material var20 = var1.getType(var3.b(var19)).getMaterial();
                  if(var20 == Material.a || var20 == Material.j) {
                     this.a(var1, var3.b(var19), a);
                  }
               }

               return true;
            } else {
               return false;
            }
         }
      } else {
         return false;
      }
   }
}
