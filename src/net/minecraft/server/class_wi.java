package net.minecraft.server;

import com.google.common.base.Predicate;
import java.util.UUID;
import net.minecraft.server.Block;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityAnimal;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityTameableAnimal;
import net.minecraft.server.EnumColor;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.PathfinderGoalFollowOwner;
import net.minecraft.server.World;
import net.minecraft.server.class_adj;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rn;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_ta;
import net.minecraft.server.class_tc;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_tn;
import net.minecraft.server.class_to;
import net.minecraft.server.class_tr;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_ul;
import net.minecraft.server.class_uu;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_uy;
import net.minecraft.server.class_uz;
import net.minecraft.server.class_va;
import net.minecraft.server.class_wc;
import net.minecraft.server.class_wd;
import net.minecraft.server.class_wj;
import net.minecraft.server.class_yh;
import net.minecraft.server.class_yl;
import net.minecraft.server.class_ys;
import net.minecraft.server.class_yv;
import net.minecraft.server.class_zl;

public class class_wi extends EntityTameableAnimal {
   private static final class_ke bz = DataWatcher.a(class_wi.class, class_kg.c);
   private static final class_ke bA = DataWatcher.a(class_wi.class, class_kg.h);
   private static final class_ke bB = DataWatcher.a(class_wi.class, class_kg.b);
   private float bC;
   private float bD;
   private boolean bE;
   private boolean bF;
   private float bG;
   private float bH;

   public class_wi(World var1) {
      super(var1);
      this.a(0.6F, 0.85F);
      this.p(false);
   }

   protected void r() {
      this.bp.a(1, new class_tg(this));
      this.bp.a(2, this.bx = new class_ul(this));
      this.bp.a(3, new class_tn(this, 0.4F));
      this.bp.a(4, new class_tr(this, 1.0D, true));
      this.bp.a(5, new PathfinderGoalFollowOwner(this, 1.0D, 10.0F, 2.0F));
      this.bp.a(6, new class_tc(this, 1.0D));
      this.bp.a(7, new class_uf(this, 1.0D));
      this.bp.a(8, new class_ta(this, 8.0F));
      this.bp.a(9, new class_to(this, EntityHuman.class, 8.0F));
      this.bp.a(9, new class_ue(this));
      this.bq.a(1, new class_uz(this));
      this.bq.a(2, new class_va(this));
      this.bq.a(3, new class_uu(this, true, new Class[0]));
      this.bq.a(4, new class_uy(this, EntityAnimal.class, false, new Predicate() {
         public boolean a(Entity var1) {
            return var1 instanceof class_wd || var1 instanceof class_wc;
         }

         // $FF: synthetic method
         public boolean apply(Object var1) {
            return this.a((Entity)var1);
         }
      }));
      this.bq.a(5, new class_ux(this, class_yv.class, false));
   }

   protected void bz() {
      super.bz();
      this.a((class_sk)class_ys.d).a(0.30000001192092896D);
      if(this.cZ()) {
         this.a((class_sk)class_ys.a).a(20.0D);
      } else {
         this.a((class_sk)class_ys.a).a(8.0D);
      }

      this.bY().b(class_ys.e).a(2.0D);
   }

   public void c(class_rz var1) {
      super.c(var1);
      if(var1 == null) {
         this.r(false);
      } else if(!this.cZ()) {
         this.r(true);
      }

   }

   protected void M() {
      this.datawatcher.b(bz, Float.valueOf(this.bP()));
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)bz, (Object)Float.valueOf(this.bP()));
      this.datawatcher.a((class_ke)bA, (Object)Boolean.valueOf(false));
      this.datawatcher.a((class_ke)bB, (Object)Integer.valueOf(EnumColor.RED.b()));
   }

   protected void a(BlockPosition var1, Block var2) {
      this.a(class_ng.gK, 0.15F, 1.0F);
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("Angry", this.dj());
      var1.a("CollarColor", (byte)this.dk().b());
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.r(var1.p("Angry"));
      if(var1.b("CollarColor", 99)) {
         this.a(EnumColor.a(var1.f("CollarColor")));
      }

   }

   protected class_nf G() {
      return this.dj()?class_ng.gG:(this.random.nextInt(3) == 0?(this.cZ() && ((Float)this.datawatcher.a(bz)).floatValue() < 10.0F?class_ng.gL:class_ng.gI):class_ng.gE);
   }

   protected class_nf bQ() {
      return class_ng.gH;
   }

   protected class_nf bR() {
      return class_ng.gF;
   }

   protected float cc() {
      return 0.4F;
   }

   protected class_kk J() {
      return class_azs.I;
   }

   public void n() {
      super.n();
      if(!this.world.E && this.bE && !this.bF && !this.cT() && this.onGround) {
         this.bF = true;
         this.bG = 0.0F;
         this.bH = 0.0F;
         this.world.a((Entity)this, (byte)8);
      }

      if(!this.world.E && this.A() == null && this.dj()) {
         this.r(false);
      }

   }

   public void m() {
      super.m();
      this.bD = this.bC;
      if(this.dl()) {
         this.bC += (1.0F - this.bC) * 0.4F;
      } else {
         this.bC += (0.0F - this.bC) * 0.4F;
      }

      if(this.ag()) {
         this.bE = true;
         this.bF = false;
         this.bG = 0.0F;
         this.bH = 0.0F;
      } else if((this.bE || this.bF) && this.bF) {
         if(this.bG == 0.0F) {
            this.a(class_ng.gJ, this.cc(), (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
         }

         this.bH = this.bG;
         this.bG += 0.05F;
         if(this.bH >= 2.0F) {
            this.bE = false;
            this.bF = false;
            this.bH = 0.0F;
            this.bG = 0.0F;
         }

         if(this.bG > 0.4F) {
            float var1 = (float)this.bk().b;
            int var2 = (int)(MathHelper.a((this.bG - 0.4F) * 3.1415927F) * 7.0F);

            for(int var3 = 0; var3 < var2; ++var3) {
               float var4 = (this.random.nextFloat() * 2.0F - 1.0F) * this.width * 0.5F;
               float var5 = (this.random.nextFloat() * 2.0F - 1.0F) * this.width * 0.5F;
               this.world.a(EnumParticle.WATER_SPLASH, this.locX + (double)var4, (double)(var1 + 0.8F), this.locZ + (double)var5, this.motX, this.motY, this.motZ, new int[0]);
            }
         }
      }

   }

   public float bm() {
      return this.length * 0.8F;
   }

   public int cD() {
      return this.db()?20:super.cD();
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b((DamageSource)var1)) {
         return false;
      } else {
         Entity var3 = var1.j();
         if(this.bx != null) {
            this.bx.a(false);
         }

         if(var3 != null && !(var3 instanceof EntityHuman) && !(var3 instanceof class_zl)) {
            var2 = (var2 + 1.0F) / 2.0F;
         }

         return super.a(var1, var2);
      }
   }

   public boolean B(Entity var1) {
      boolean var2 = var1.a(DamageSource.a((class_rz)this), (float)((int)this.a((class_sk)class_ys.e).e()));
      if(var2) {
         this.a((class_rz)this, (Entity)var1);
      }

      return var2;
   }

   public void p(boolean var1) {
      super.p(var1);
      if(var1) {
         this.a((class_sk)class_ys.a).a(20.0D);
      } else {
         this.a((class_sk)class_ys.a).a(8.0D);
      }

      this.a((class_sk)class_ys.e).a(4.0D);
   }

   public boolean a(EntityHuman var1, EnumHand var2, ItemStack var3) {
      if(this.cZ()) {
         if(var3 != null) {
            if(var3.b() instanceof class_adj) {
               class_adj var4 = (class_adj)var3.b();
               if(var4.g() && ((Float)this.datawatcher.a(bz)).floatValue() < 20.0F) {
                  if(!var1.abilities.d) {
                     --var3.b;
                  }

                  this.b((float)var4.h(var3));
                  return true;
               }
            } else if(var3.b() == Items.bd) {
               EnumColor var5 = EnumColor.a(var3.i());
               if(var5 != this.dk()) {
                  this.a(var5);
                  if(!var1.abilities.d) {
                     --var3.b;
                  }

                  return true;
               }
            }
         }

         if(this.d(var1) && !this.world.E && !this.e(var3)) {
            this.bx.a(!this.db());
            this.bc = false;
            this.h.o();
            this.c((class_rz)null);
         }
      } else if(var3 != null && var3.b() == Items.be && !this.dj()) {
         if(!var1.abilities.d) {
            --var3.b;
         }

         if(!this.world.E) {
            if(this.random.nextInt(3) == 0) {
               this.p(true);
               this.h.o();
               this.c((class_rz)null);
               this.bx.a(true);
               this.c(20.0F);
               this.b((UUID)var1.getUniqueId());
               this.o(true);
               this.world.a((Entity)this, (byte)7);
            } else {
               this.o(false);
               this.world.a((Entity)this, (byte)6);
            }
         }

         return true;
      }

      return super.a(var1, var2, var3);
   }

   public boolean e(ItemStack var1) {
      return var1 == null?false:(!(var1.b() instanceof class_adj)?false:((class_adj)var1.b()).g());
   }

   public int cJ() {
      return 8;
   }

   public boolean dj() {
      return (((Byte)this.datawatcher.a(bv)).byteValue() & 2) != 0;
   }

   public void r(boolean var1) {
      byte var2 = ((Byte)this.datawatcher.a(bv)).byteValue();
      if(var1) {
         this.datawatcher.b(bv, Byte.valueOf((byte)(var2 | 2)));
      } else {
         this.datawatcher.b(bv, Byte.valueOf((byte)(var2 & -3)));
      }

   }

   public EnumColor dk() {
      return EnumColor.a(((Integer)this.datawatcher.a(bB)).intValue() & 15);
   }

   public void a(EnumColor var1) {
      this.datawatcher.b(bB, Integer.valueOf(var1.b()));
   }

   public class_wi b(class_rn var1) {
      class_wi var2 = new class_wi(this.world);
      UUID var3 = this.b();
      if(var3 != null) {
         var2.b((UUID)var3);
         var2.p(true);
      }

      return var2;
   }

   public void s(boolean var1) {
      this.datawatcher.b(bA, Boolean.valueOf(var1));
   }

   public boolean a(EntityAnimal var1) {
      if(var1 == this) {
         return false;
      } else if(!this.cZ()) {
         return false;
      } else if(!(var1 instanceof class_wi)) {
         return false;
      } else {
         class_wi var2 = (class_wi)var1;
         return !var2.cZ()?false:(var2.db()?false:this.df() && var2.df());
      }
   }

   public boolean dl() {
      return ((Boolean)this.datawatcher.a(bA)).booleanValue();
   }

   protected boolean K() {
      return !this.cZ() && this.ticksLived > 2400;
   }

   public boolean a(class_rz var1, class_rz var2) {
      if(!(var1 instanceof class_yh) && !(var1 instanceof class_yl)) {
         if(var1 instanceof class_wi) {
            class_wi var3 = (class_wi)var1;
            if(var3.cZ() && var3.dc() == var2) {
               return false;
            }
         }

         return var1 instanceof EntityHuman && var2 instanceof EntityHuman && !((EntityHuman)var2).a((EntityHuman)var1)?false:!(var1 instanceof class_wj) || !((class_wj)var1).dc();
      } else {
         return false;
      }
   }

   public boolean a(EntityHuman var1) {
      return !this.dj() && super.a(var1);
   }

   // $FF: synthetic method
   public class_rn a(class_rn var1) {
      return this.b(var1);
   }
}
