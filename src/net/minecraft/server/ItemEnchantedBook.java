package net.minecraft.server;

import net.minecraft.server.Enchantment;
import net.minecraft.server.EnumItemRarity;
import net.minecraft.server.Item;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ago;

public class ItemEnchantedBook extends Item {
   public boolean g_(ItemStack var1) {
      return false;
   }

   public EnumItemRarity g(ItemStack var1) {
      return !this.h(var1).c_()?EnumItemRarity.UNCOMMON:super.g(var1);
   }

   public NBTTagList h(ItemStack var1) {
      NBTTagCompound var2 = var1.o();
      return var2 != null && var2.b("StoredEnchantments", 9)?(NBTTagList)var2.c("StoredEnchantments"):new NBTTagList();
   }

   public void a(ItemStack var1, class_ago var2) {
      NBTTagList var3 = this.h(var1);
      boolean var4 = true;

      for(int var5 = 0; var5 < var3.c(); ++var5) {
         NBTTagCompound var6 = var3.b(var5);
         if(Enchantment.c(var6.g("id")) == var2.b) {
            if(var6.g("lvl") < var2.c) {
               var6.a("lvl", (short)var2.c);
            }

            var4 = false;
            break;
         }
      }

      if(var4) {
         NBTTagCompound var7 = new NBTTagCompound();
         var7.a("id", (short)Enchantment.b(var2.b));
         var7.a("lvl", (short)var2.c);
         var3.a((NBTTag)var7);
      }

      if(!var1.n()) {
         var1.d(new NBTTagCompound());
      }

      var1.o().a((String)"StoredEnchantments", (NBTTag)var3);
   }

   public ItemStack a(class_ago var1) {
      ItemStack var2 = new ItemStack(this);
      this.a(var2, var1);
      return var2;
   }
}
