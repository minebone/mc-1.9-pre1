package net.minecraft.server;

import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.class_ox;

public class class_pe implements class_ox {
   public int a() {
      return 147;
   }

   public NBTTagCompound a(NBTTagCompound var1) {
      if("ArmorStand".equals(var1.l("id")) && var1.p("Silent") && !var1.p("Marker")) {
         var1.q("Silent");
      }

      return var1;
   }
}
