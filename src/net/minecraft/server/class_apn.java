package net.minecraft.server;

import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.class_ajn;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;

public class class_apn extends class_ajn {
   public static final BlockStateInteger d = BlockStateInteger.a("power", 0, 15);
   private final int e;

   protected class_apn(Material var1, int var2) {
      this(var1, var2, var1.r());
   }

   protected class_apn(Material var1, int var2, MaterialMapColor var3) {
      super(var1, var3);
      this.w(this.A.b().set(d, Integer.valueOf(0)));
      this.e = var2;
   }

   protected int e(World var1, BlockPosition var2) {
      int var3 = Math.min(var1.a(Entity.class, c.a(var2)).size(), this.e);
      if(var3 > 0) {
         float var4 = (float)Math.min(this.e, var3) / (float)this.e;
         return MathHelper.f(var4 * 15.0F);
      } else {
         return 0;
      }
   }

   protected void b(World var1, BlockPosition var2) {
      var1.a((EntityHuman)null, var2, class_ng.dp, EnumSoundCategory.BLOCKS, 0.3F, 0.90000004F);
   }

   protected void c(World var1, BlockPosition var2) {
      var1.a((EntityHuman)null, var2, class_ng.do1, EnumSoundCategory.BLOCKS, 0.3F, 0.75F);
   }

   protected int i(IBlockData var1) {
      return ((Integer)var1.get(d)).intValue();
   }

   protected IBlockData a(IBlockData var1, int var2) {
      return var1.set(d, Integer.valueOf(var2));
   }

   public int a(World var1) {
      return 10;
   }

   public IBlockData a(int var1) {
      return this.u().set(d, Integer.valueOf(var1));
   }

   public int e(IBlockData var1) {
      return ((Integer)var1.get(d)).intValue();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{d});
   }
}
