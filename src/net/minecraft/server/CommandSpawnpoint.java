package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;

public class CommandSpawnpoint extends CommandAbstract {
   public String c() {
      return "spawnpoint";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.spawnpoint.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length > 1 && var3.length < 4) {
         throw new class_cf("commands.spawnpoint.usage", new Object[0]);
      } else {
         EntityPlayer var4 = var3.length > 0?a(var1, var2, var3[0]):a(var2);
         BlockPosition var5 = var3.length > 3?a(var2, var3, 1, true):var4.c();
         if(var4.world != null) {
            var4.a((BlockPosition)var5, true);
            a(var2, this, "commands.spawnpoint.success", new Object[]{var4.h_(), Integer.valueOf(var5.p()), Integer.valueOf(var5.q()), Integer.valueOf(var5.r())});
         }

      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, var1.J()):(var3.length > 1 && var3.length <= 4?a(var3, 1, var4):Collections.emptyList());
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 0;
   }
}
