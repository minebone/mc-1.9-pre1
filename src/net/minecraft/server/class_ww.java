package net.minecraft.server;

import net.minecraft.server.DamageSource;
import net.minecraft.server.class_wr;
import net.minecraft.server.class_wt;
import net.minecraft.server.class_wv;
import net.minecraft.server.class_zl;

public abstract class class_ww extends class_wv {
   public class_ww(class_wt var1) {
      super(var1);
   }

   public boolean a() {
      return true;
   }

   public float a(class_wr var1, DamageSource var2, float var3) {
      if(var2.i() instanceof class_zl) {
         var2.i().g(1);
         return 0.0F;
      } else {
         return super.a(var1, var2, var3);
      }
   }
}
