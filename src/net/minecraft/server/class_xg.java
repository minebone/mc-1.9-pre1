package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Vec3D;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_wt;
import net.minecraft.server.class_ww;
import net.minecraft.server.class_wx;
import net.minecraft.server.class_xj;

public class class_xg extends class_ww {
   private int b;

   public class_xg(class_wt var1) {
      super(var1);
   }

   public void c() {
      ++this.b;
      EntityHuman var1 = this.a.world.a((Entity)this.a, 20.0D, 10.0D);
      if(var1 != null) {
         if(this.b > 25) {
            this.a.cT().a(class_xj.h);
         } else {
            Vec3D var2 = (new Vec3D(var1.locX - this.a.locX, 0.0D, var1.locZ - this.a.locZ)).a();
            Vec3D var3 = (new Vec3D((double)MathHelper.a(this.a.yaw * 0.017453292F), 0.0D, (double)(-MathHelper.b(this.a.yaw * 0.017453292F)))).a();
            float var4 = (float)var3.b(var2);
            float var5 = (float)(Math.acos((double)var4) * 57.2957763671875D) + 0.5F;
            if(var5 < 0.0F || var5 > 10.0F) {
               double var6 = var1.locX - this.a.bu.locX;
               double var8 = var1.locZ - this.a.bu.locZ;
               double var10 = MathHelper.a(MathHelper.g(180.0D - MathHelper.b(var6, var8) * 57.2957763671875D - (double)this.a.yaw), -100.0D, 100.0D);
               this.a.bf *= 0.8F;
               float var12 = MathHelper.a(var6 * var6 + var8 * var8) + 1.0F;
               float var13 = var12;
               if(var12 > 40.0F) {
                  var12 = 40.0F;
               }

               this.a.bf = (float)((double)this.a.bf + var10 * (double)(0.7F / var12 / var13));
               this.a.yaw += this.a.bf;
            }
         }
      } else if(this.b >= 100) {
         var1 = this.a.world.a((Entity)this.a, 150.0D, 150.0D);
         this.a.cT().a(class_xj.e);
         if(var1 != null) {
            this.a.cT().a(class_xj.i);
            ((class_wx)this.a.cT().b(class_xj.i)).a(new Vec3D(var1.locX, var1.locY, var1.locZ));
         }
      }

   }

   public void d() {
      this.b = 0;
   }

   public class_xj i() {
      return class_xj.g;
   }
}
