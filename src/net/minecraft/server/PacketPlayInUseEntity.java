package net.minecraft.server;

import java.io.IOException;
import net.minecraft.server.Entity;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayInListener;
import net.minecraft.server.EnumHand;

public class PacketPlayInUseEntity implements Packet {
   private int a;
   private PacketPlayInUseEntity.EnumEntityUseAction b;
   private Vec3D c;
   private EnumHand d;

   public PacketPlayInUseEntity() {
   }

   public PacketPlayInUseEntity(Entity var1) {
      this.a = var1.getId();
      this.b = PacketPlayInUseEntity.EnumEntityUseAction.ATTACK;
   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.readVarInt();
      this.b = (PacketPlayInUseEntity.EnumEntityUseAction)var1.a(PacketPlayInUseEntity.EnumEntityUseAction.class);
      if(this.b == PacketPlayInUseEntity.EnumEntityUseAction.INTERACT_AT) {
         this.c = new Vec3D((double)var1.readFloat(), (double)var1.readFloat(), (double)var1.readFloat());
      }

      if(this.b == PacketPlayInUseEntity.EnumEntityUseAction.INTERACT || this.b == PacketPlayInUseEntity.EnumEntityUseAction.INTERACT_AT) {
         this.d = (EnumHand)var1.a(EnumHand.class);
      }

   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.writeVarInt(this.a);
      var1.a((Enum)this.b);
      if(this.b == PacketPlayInUseEntity.EnumEntityUseAction.INTERACT_AT) {
         var1.writeFloat((float)this.c.b);
         var1.writeFloat((float)this.c.c);
         var1.writeFloat((float)this.c.d);
      }

      if(this.b == PacketPlayInUseEntity.EnumEntityUseAction.INTERACT || this.b == PacketPlayInUseEntity.EnumEntityUseAction.INTERACT_AT) {
         var1.a((Enum)this.d);
      }

   }

   public void a(PacketPlayInListener var1) {
      var1.a(this);
   }

   public Entity a(World var1) {
      return var1.a(this.a);
   }

   public PacketPlayInUseEntity.EnumEntityUseAction a() {
      return this.b;
   }

   public EnumHand b() {
      return this.d;
   }

   public Vec3D c() {
      return this.c;
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketPlayInListener)var1);
   }

   public static enum EnumEntityUseAction {
      INTERACT,
      ATTACK,
      INTERACT_AT;
   }
}
