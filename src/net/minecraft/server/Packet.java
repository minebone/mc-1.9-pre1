package net.minecraft.server;

import java.io.IOException;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketListener;

public interface Packet {
	
   void decode(PacketDataSerializer var1) throws IOException;

   void encode(PacketDataSerializer var1) throws IOException;

   void handle(PacketListener var1);
}
