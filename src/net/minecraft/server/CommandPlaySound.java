package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.Vec3D;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutNamedSoundEffect;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.class_nf;
import net.minecraft.server.EnumSoundCategory;

public class CommandPlaySound extends CommandAbstract {
   public String c() {
      return "playsound";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.playsound.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 2) {
         throw new class_cf(this.b(var2), new Object[0]);
      } else {
         byte var4 = 0;
         int var33 = var4 + 1;
         String var5 = var3[var4];
         EnumSoundCategory var6 = EnumSoundCategory.a(var3[var33++]);
         EntityPlayer var7 = a(var1, var2, var3[var33++]);
         Vec3D var8 = var2.d();
         double var9 = var8.b;
         if(var3.length > var33) {
            var9 = b(var9, var3[var33++], true);
         }

         double var11 = var8.c;
         if(var3.length > var33) {
            var11 = b(var11, var3[var33++], 0, 0, false);
         }

         double var13 = var8.d;
         if(var3.length > var33) {
            var13 = b(var13, var3[var33++], true);
         }

         double var15 = 1.0D;
         if(var3.length > var33) {
            var15 = a(var3[var33++], 0.0D, 3.4028234663852886E38D);
         }

         double var17 = 1.0D;
         if(var3.length > var33) {
            var17 = a(var3[var33++], 0.0D, 2.0D);
         }

         double var19 = 0.0D;
         if(var3.length > var33) {
            var19 = a(var3[var33], 0.0D, 1.0D);
         }

         double var21 = var15 > 1.0D?var15 * 16.0D:16.0D;
         double var23 = var7.f(var9, var11, var13);
         if(var23 > var21) {
            if(var19 <= 0.0D) {
               throw new class_bz("commands.playsound.playerTooFar", new Object[]{var7.h_()});
            }

            double var25 = var9 - var7.locX;
            double var27 = var11 - var7.locY;
            double var29 = var13 - var7.locZ;
            double var31 = Math.sqrt(var25 * var25 + var27 * var27 + var29 * var29);
            if(var31 > 0.0D) {
               var9 = var7.locX + var25 / var31 * 2.0D;
               var11 = var7.locY + var27 / var31 * 2.0D;
               var13 = var7.locZ + var29 / var31 * 2.0D;
            }

            var15 = var19;
         }

         var7.a.a((Packet)(new PacketPlayOutNamedSoundEffect(var5, var6, var9, var11, var13, (float)var15, (float)var17)));
         a(var2, this, "commands.playsound.success", new Object[]{var5, var7.h_()});
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, class_nf.a.c()):(var3.length == 2?a(var3, EnumSoundCategory.b()):(var3.length == 3?a(var3, var1.J()):(var3.length > 3 && var3.length <= 6?a(var3, 2, var4):Collections.emptyList())));
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 2;
   }
}
