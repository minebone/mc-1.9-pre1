package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cb;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;

public class CommandDifficulty extends CommandAbstract {
   public String c() {
      return "difficulty";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.difficulty.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length <= 0) {
         throw new class_cf("commands.difficulty.usage", new Object[0]);
      } else {
         EnumDifficulty var4 = this.e(var3[0]);
         var1.a(var4);
         a(var2, this, "commands.difficulty.success", new Object[]{new ChatMessage(var4.b(), new Object[0])});
      }
   }

   protected EnumDifficulty e(String var1) throws class_cb {
      return !var1.equalsIgnoreCase("peaceful") && !var1.equalsIgnoreCase("p")?(!var1.equalsIgnoreCase("easy") && !var1.equalsIgnoreCase("e")?(!var1.equalsIgnoreCase("normal") && !var1.equalsIgnoreCase("n")?(!var1.equalsIgnoreCase("hard") && !var1.equalsIgnoreCase("h")?EnumDifficulty.a(a(var1, 0, 3)):EnumDifficulty.HARD):EnumDifficulty.NORMAL):EnumDifficulty.EASY):EnumDifficulty.PEACEFUL;
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, new String[]{"peaceful", "easy", "normal", "hard"}):Collections.emptyList();
   }
}
