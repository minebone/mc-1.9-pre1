package net.minecraft.server;

import java.util.Calendar;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_vs;
import net.minecraft.server.class_ys;

public class class_vt extends class_vs {
   private static final class_ke a = DataWatcher.a(class_vt.class, class_kg.a);
   private BlockPosition b;

   public class_vt(World var1) {
      super(var1);
      this.a(0.5F, 0.9F);
      this.a(true);
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)a, (Object)Byte.valueOf((byte)0));
   }

   protected float cc() {
      return 0.1F;
   }

   protected float cd() {
      return super.cd() * 0.95F;
   }

   protected class_nf G() {
      return this.o() && this.random.nextInt(4) != 0?null:class_ng.w;
   }

   protected class_nf bQ() {
      return class_ng.y;
   }

   protected class_nf bR() {
      return class_ng.x;
   }

   public boolean ap() {
      return false;
   }

   protected void C(Entity var1) {
   }

   protected void cm() {
   }

   protected void bz() {
      super.bz();
      this.a((class_sk)class_ys.a).a(6.0D);
   }

   public boolean o() {
      return (((Byte)this.datawatcher.a(a)).byteValue() & 1) != 0;
   }

   public void a(boolean var1) {
      byte var2 = ((Byte)this.datawatcher.a(a)).byteValue();
      if(var1) {
         this.datawatcher.b(a, Byte.valueOf((byte)(var2 | 1)));
      } else {
         this.datawatcher.b(a, Byte.valueOf((byte)(var2 & -2)));
      }

   }

   public void m() {
      super.m();
      if(this.o()) {
         this.motX = this.motY = this.motZ = 0.0D;
         this.locY = (double)MathHelper.c(this.locY) + 1.0D - (double)this.length;
      } else {
         this.motY *= 0.6000000238418579D;
      }

   }

   protected void M() {
      super.M();
      BlockPosition var1 = new BlockPosition(this);
      BlockPosition var2 = var1.a();
      if(this.o()) {
         if(!this.world.getType(var2).l()) {
            this.a(false);
            this.world.a((EntityHuman)null, 1025, var1, 0);
         } else {
            if(this.random.nextInt(200) == 0) {
               this.aO = (float)this.random.nextInt(360);
            }

            if(this.world.b(this, 4.0D) != null) {
               this.a(false);
               this.world.a((EntityHuman)null, 1025, var1, 0);
            }
         }
      } else {
         if(this.b != null && (!this.world.d(this.b) || this.b.q() < 1)) {
            this.b = null;
         }

         if(this.b == null || this.random.nextInt(30) == 0 || this.b.e((double)((int)this.locX), (double)((int)this.locY), (double)((int)this.locZ)) < 4.0D) {
            this.b = new BlockPosition((int)this.locX + this.random.nextInt(7) - this.random.nextInt(7), (int)this.locY + this.random.nextInt(6) - 2, (int)this.locZ + this.random.nextInt(7) - this.random.nextInt(7));
         }

         double var3 = (double)this.b.p() + 0.5D - this.locX;
         double var5 = (double)this.b.q() + 0.1D - this.locY;
         double var7 = (double)this.b.r() + 0.5D - this.locZ;
         this.motX += (Math.signum(var3) * 0.5D - this.motX) * 0.10000000149011612D;
         this.motY += (Math.signum(var5) * 0.699999988079071D - this.motY) * 0.10000000149011612D;
         this.motZ += (Math.signum(var7) * 0.5D - this.motZ) * 0.10000000149011612D;
         float var9 = (float)(MathHelper.b(this.motZ, this.motX) * 57.2957763671875D) - 90.0F;
         float var10 = MathHelper.g(var9 - this.yaw);
         this.be = 0.5F;
         this.yaw += var10;
         if(this.random.nextInt(100) == 0 && this.world.getType(var2).l()) {
            this.a(true);
         }
      }

   }

   protected boolean ad() {
      return false;
   }

   public void e(float var1, float var2) {
   }

   protected void a(double var1, boolean var3, IBlockData var4, BlockPosition var5) {
   }

   public boolean aZ() {
      return true;
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b(var1)) {
         return false;
      } else {
         if(!this.world.E && this.o()) {
            this.a(false);
         }

         return super.a(var1, var2);
      }
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.datawatcher.b(a, Byte.valueOf(var1.f("BatFlags")));
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("BatFlags", ((Byte)this.datawatcher.a(a)).byteValue());
   }

   public boolean cF() {
      BlockPosition var1 = new BlockPosition(this.locX, this.bk().b, this.locZ);
      if(var1.q() >= this.world.K()) {
         return false;
      } else {
         int var2 = this.world.k(var1);
         byte var3 = 4;
         if(this.a(this.world.ac())) {
            var3 = 7;
         } else if(this.random.nextBoolean()) {
            return false;
         }

         return var2 > this.random.nextInt(var3)?false:super.cF();
      }
   }

   private boolean a(Calendar var1) {
      return var1.get(2) + 1 == 10 && var1.get(5) >= 20 || var1.get(2) + 1 == 11 && var1.get(5) <= 3;
   }

   public float bm() {
      return this.length / 2.0F;
   }

   protected class_kk J() {
      return class_azs.ab;
   }
}
