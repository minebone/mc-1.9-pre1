package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.class_alg;
import net.minecraft.server.IBlockData;

public class class_alw extends class_alg {
   public Item a(IBlockData var1, Random var2, int var3) {
      if(var3 > 3) {
         var3 = 3;
      }

      return var2.nextInt(10 - var3 * 3) == 0?Items.am:Item.a((Block)this);
   }

   public MaterialMapColor r(IBlockData var1) {
      return MaterialMapColor.m;
   }
}
