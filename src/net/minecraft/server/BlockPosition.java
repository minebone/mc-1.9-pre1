package net.minecraft.server;

import com.google.common.collect.AbstractIterator;
import com.google.common.collect.Lists;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Vec3D;
import net.minecraft.server.BaseBlockPosition;
import net.minecraft.server.MathHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BlockPosition extends BaseBlockPosition {
   private static final Logger c = LogManager.getLogger();
   public static final BlockPosition a = new BlockPosition(0, 0, 0);
   private static final int d = 1 + MathHelper.e(MathHelper.c(30000000));
   private static final int e = d;
   private static final int f = 64 - d - e;
   private static final int g = 0 + e;
   private static final int h = g + f;
   private static final long i = (1L << d) - 1L;
   private static final long j = (1L << f) - 1L;
   private static final long k = (1L << e) - 1L;

   public BlockPosition(int var1, int var2, int var3) {
      super(var1, var2, var3);
   }

   public BlockPosition(double var1, double var3, double var5) {
      super(var1, var3, var5);
   }

   public BlockPosition(Entity var1) {
      this(var1.locX, var1.locY, var1.locZ);
   }

   public BlockPosition(Vec3D var1) {
      this(var1.b, var1.c, var1.d);
   }

   public BlockPosition(BaseBlockPosition var1) {
      this(var1.p(), var1.q(), var1.r());
   }

   public BlockPosition a(double var1, double var3, double var5) {
      return var1 == 0.0D && var3 == 0.0D && var5 == 0.0D?this:new BlockPosition((double)this.p() + var1, (double)this.q() + var3, (double)this.r() + var5);
   }

   public BlockPosition a(int var1, int var2, int var3) {
      return var1 == 0 && var2 == 0 && var3 == 0?this:new BlockPosition(this.p() + var1, this.q() + var2, this.r() + var3);
   }

   public BlockPosition a(BaseBlockPosition var1) {
      return var1.p() == 0 && var1.q() == 0 && var1.r() == 0?this:new BlockPosition(this.p() + var1.p(), this.q() + var1.q(), this.r() + var1.r());
   }

   public BlockPosition b(BaseBlockPosition var1) {
      return var1.p() == 0 && var1.q() == 0 && var1.r() == 0?this:new BlockPosition(this.p() - var1.p(), this.q() - var1.q(), this.r() - var1.r());
   }

   public BlockPosition a() {
      return this.b(1);
   }

   public BlockPosition b(int var1) {
      return this.a(EnumDirection.UP, var1);
   }

   public BlockPosition b() {
      return this.c(1);
   }

   public BlockPosition c(int var1) {
      return this.a(EnumDirection.DOWN, var1);
   }

   public BlockPosition c() {
      return this.d(1);
   }

   public BlockPosition d(int var1) {
      return this.a(EnumDirection.NORTH, var1);
   }

   public BlockPosition d() {
      return this.e(1);
   }

   public BlockPosition e(int var1) {
      return this.a(EnumDirection.SOUTH, var1);
   }

   public BlockPosition e() {
      return this.f(1);
   }

   public BlockPosition f(int var1) {
      return this.a(EnumDirection.WEST, var1);
   }

   public BlockPosition f() {
      return this.g(1);
   }

   public BlockPosition g(int var1) {
      return this.a(EnumDirection.EAST, var1);
   }

   public BlockPosition a(EnumDirection var1) {
      return this.a(var1, 1);
   }

   public BlockPosition a(EnumDirection var1, int var2) {
      return var2 == 0?this:new BlockPosition(this.p() + var1.g() * var2, this.q() + var1.h() * var2, this.r() + var1.i() * var2);
   }

   public BlockPosition c(BaseBlockPosition var1) {
      return new BlockPosition(this.q() * var1.r() - this.r() * var1.q(), this.r() * var1.p() - this.p() * var1.r(), this.p() * var1.q() - this.q() * var1.p());
   }

   public long g() {
      return ((long)this.p() & i) << h | ((long)this.q() & j) << g | ((long)this.r() & k) << 0;
   }

   public static BlockPosition a(long var0) {
      int var2 = (int)(var0 << 64 - h - d >> 64 - d);
      int var3 = (int)(var0 << 64 - g - f >> 64 - f);
      int var4 = (int)(var0 << 64 - e >> 64 - e);
      return new BlockPosition(var2, var3, var4);
   }

   public static Iterable a(BlockPosition var0, BlockPosition var1) {
      final BlockPosition var2 = new BlockPosition(Math.min(var0.p(), var1.p()), Math.min(var0.q(), var1.q()), Math.min(var0.r(), var1.r()));
      final BlockPosition var3 = new BlockPosition(Math.max(var0.p(), var1.p()), Math.max(var0.q(), var1.q()), Math.max(var0.r(), var1.r()));
      return new Iterable() {
         public Iterator iterator() {
            return new AbstractIterator() {
               private BlockPosition b = null;

               protected BlockPosition a() {
                  if(this.b == null) {
                     this.b = var2;
                     return this.b;
                  } else if(this.b.equals(var3)) {
                     return (BlockPosition)this.endOfData();
                  } else {
                     int var1 = this.b.p();
                     int var2x = this.b.q();
                     int var3x = this.b.r();
                     if(var1 < var3.p()) {
                        ++var1;
                     } else if(var2x < var3.q()) {
                        var1 = var2.p();
                        ++var2x;
                     } else if(var3x < var3.r()) {
                        var1 = var2.p();
                        var2x = var2.q();
                        ++var3x;
                     }

                     this.b = new BlockPosition(var1, var2x, var3x);
                     return this.b;
                  }
               }

               // $FF: synthetic method
               protected Object computeNext() {
                  return this.a();
               }
            };
         }
      };
   }

   public BlockPosition h() {
      return this;
   }

   public static Iterable b(BlockPosition var0, BlockPosition var1) {
      final BlockPosition var2 = new BlockPosition(Math.min(var0.p(), var1.p()), Math.min(var0.q(), var1.q()), Math.min(var0.r(), var1.r()));
      final BlockPosition var3 = new BlockPosition(Math.max(var0.p(), var1.p()), Math.max(var0.q(), var1.q()), Math.max(var0.r(), var1.r()));
      return new Iterable() {
         public Iterator iterator() {
            return new AbstractIterator() {
               private BlockPosition.class_a_in_class_cj b = null;

               protected BlockPosition.class_a_in_class_cj a() {
                  if(this.b == null) {
                     this.b = new BlockPosition.class_a_in_class_cj(var2.p(), var2.q(), var2.r());
                     return this.b;
                  } else if(this.b.equals(var3)) {
                     return (BlockPosition.class_a_in_class_cj)this.endOfData();
                  } else {
                     int var1 = this.b.p();
                     int var2x = this.b.q();
                     int var3x = this.b.r();
                     if(var1 < var3.p()) {
                        ++var1;
                     } else if(var2x < var3.q()) {
                        var1 = var2.p();
                        ++var2x;
                     } else if(var3x < var3.r()) {
                        var1 = var2.p();
                        var2x = var2.q();
                        ++var3x;
                     }

                     this.b.c = var1;
                     this.b.d = var2x;
                     this.b.e = var3x;
                     return this.b;
                  }
               }

               // $FF: synthetic method
               protected Object computeNext() {
                  return this.a();
               }
            };
         }
      };
   }

   // $FF: synthetic method
   public BaseBlockPosition d(BaseBlockPosition var1) {
      return this.c(var1);
   }

   public static final class class_b_in_class_cj extends BlockPosition {
      private int c;
      private int d;
      private int e;
      private boolean f;
      private static final List g = Lists.newArrayList();

      private class_b_in_class_cj(int var1, int var2, int var3) {
         super(0, 0, 0);
         this.c = var1;
         this.d = var2;
         this.e = var3;
      }

      public static BlockPosition.class_b_in_class_cj s() {
         return c(0, 0, 0);
      }

      public static BlockPosition.class_b_in_class_cj c(double var0, double var2, double var4) {
         return c(MathHelper.c(var0), MathHelper.c(var2), MathHelper.c(var4));
      }

      public static BlockPosition.class_b_in_class_cj c(int var0, int var1, int var2) {
         List var3 = g;
         synchronized(g) {
            if(!g.isEmpty()) {
               BlockPosition.class_b_in_class_cj var4 = (BlockPosition.class_b_in_class_cj)g.remove(g.size() - 1);
               if(var4 != null && var4.f) {
                  var4.f = false;
                  var4.d(var0, var1, var2);
                  return var4;
               }
            }
         }

         return new BlockPosition.class_b_in_class_cj(var0, var1, var2);
      }

      public void t() {
         List var1 = g;
         synchronized(g) {
            if(g.size() < 100) {
               g.add(this);
            }

            this.f = true;
         }
      }

      public int p() {
         return this.c;
      }

      public int q() {
         return this.d;
      }

      public int r() {
         return this.e;
      }

      public BlockPosition.class_b_in_class_cj d(int var1, int var2, int var3) {
         if(this.f) {
            BlockPosition.c.error("PooledMutableBlockPosition modified after it was released.", new Throwable());
            this.f = false;
         }

         this.c = var1;
         this.d = var2;
         this.e = var3;
         return this;
      }

      public BlockPosition.class_b_in_class_cj d(double var1, double var3, double var5) {
         return this.d(MathHelper.c(var1), MathHelper.c(var3), MathHelper.c(var5));
      }

      public BlockPosition.class_b_in_class_cj h(BaseBlockPosition var1) {
         return this.d(var1.p(), var1.q(), var1.r());
      }

      public BlockPosition.class_b_in_class_cj c(EnumDirection var1) {
         return this.d(this.c + var1.g(), this.d + var1.h(), this.e + var1.i());
      }

      // $FF: synthetic method
      public BaseBlockPosition d(BaseBlockPosition var1) {
         return super.c(var1);
      }
   }

   public static final class class_a_in_class_cj extends BlockPosition {
      private int c;
      private int d;
      private int e;

      public class_a_in_class_cj() {
         this(0, 0, 0);
      }

      public class_a_in_class_cj(BlockPosition var1) {
         this(var1.p(), var1.q(), var1.r());
      }

      public class_a_in_class_cj(int var1, int var2, int var3) {
         super(0, 0, 0);
         this.c = var1;
         this.d = var2;
         this.e = var3;
      }

      public int p() {
         return this.c;
      }

      public int q() {
         return this.d;
      }

      public int r() {
         return this.e;
      }

      public BlockPosition.class_a_in_class_cj c(int var1, int var2, int var3) {
         this.c = var1;
         this.d = var2;
         this.e = var3;
         return this;
      }

      public void c(EnumDirection var1) {
         this.c += var1.g();
         this.d += var1.h();
         this.e += var1.i();
      }

      public void p(int var1) {
         this.d = var1;
      }

      public BlockPosition h() {
         return new BlockPosition(this);
      }

      // $FF: synthetic method
      public BaseBlockPosition d(BaseBlockPosition var1) {
         return super.c(var1);
      }
   }
}
