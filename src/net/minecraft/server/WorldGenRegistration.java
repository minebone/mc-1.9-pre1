package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import net.minecraft.server.BlockDoubleStepAbstract;
import net.minecraft.server.BlockFlowerPot;
import net.minecraft.server.BlockLever;
import net.minecraft.server.BlockRepeater;
import net.minecraft.server.BlockSandStone;
import net.minecraft.server.BlockSmoothBrick;
import net.minecraft.server.BlockStairs;
import net.minecraft.server.BlockVine;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityWitch;
import net.minecraft.server.EnumColor;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Items;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.StructureBoundingBox;
import net.minecraft.server.StructurePiece;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.WorldGenFactory;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_apg;
import net.minecraft.server.class_aph;
import net.minecraft.server.class_apw;
import net.minecraft.server.class_aqt;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_awl;
import net.minecraft.server.class_awm;
import net.minecraft.server.class_awn;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.BaseBlockPosition;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_ow;
import net.minecraft.server.class_sc;

public class WorldGenRegistration {
   public static void a() {
      WorldGenFactory.a(WorldGenRegistration.WorldGenPyramidPiece.class, "TeDP");
      WorldGenFactory.a(WorldGenRegistration.WorldGenJungleTemple.class, "TeJP");
      WorldGenFactory.a(WorldGenRegistration.class_e_in_class_avz.class, "TeSH");
      WorldGenFactory.a(WorldGenRegistration.class_b_in_class_avz.class, "Iglu");
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
   }

   public static class class_b_in_class_avz extends WorldGenRegistration.WorldGenScatteredPiece {
      private static final class_kk e = new class_kk("igloo/igloo_top");
      private static final class_kk f = new class_kk("igloo/igloo_middle");
      private static final class_kk g = new class_kk("igloo/igloo_bottom");

      public class_b_in_class_avz() {
      }

      public class_b_in_class_avz(Random var1, int var2, int var3) {
         super(var1, var2, 64, var3, 7, 5, 8);
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(!this.a(var1, var3, -1)) {
            return false;
         } else {
            StructureBoundingBox var4 = this.c();
            BlockPosition var5 = new BlockPosition(var4.a, var4.b, var4.c);
            class_aod[] var6 = class_aod.values();
            MinecraftServer var7 = var1.u();
            class_awl var8 = var1.S().h();
            class_awm var9 = new class_awm(class_amq.NONE, var6[var2.nextInt(var6.length)], false, Blocks.cv, var4);
            class_awn var10 = var8.a(var7, e);
            var10.a(var1, var5, var9);
            if(var2.nextDouble() < 0.5D) {
               class_awn var11 = var8.a(var7, f);
               class_awn var12 = var8.a(var7, g);
               int var13 = var2.nextInt(8) + 4;

               for(int var14 = 0; var14 < var13; ++var14) {
                  BlockPosition var15 = var10.a(var9, new BlockPosition(3, -1 - var14 * 3, 5), var9, new BlockPosition(1, 2, 1));
                  var11.a(var1, var5.a((BaseBlockPosition)var15), var9);
               }

               BlockPosition var21 = var5.a((BaseBlockPosition)var10.a(var9, new BlockPosition(3, -1 - var13 * 3, 5), var9, new BlockPosition(3, 5, 7)));
               var12.a(var1, var21, var9);
               Map var22 = var12.a(var21, var9);
               Iterator var16 = var22.entrySet().iterator();

               while(var16.hasNext()) {
                  Entry var17 = (Entry)var16.next();
                  if("chest".equals(var17.getValue())) {
                     BlockPosition var18 = (BlockPosition)var17.getKey();
                     var1.a((BlockPosition)var18, (IBlockData)Blocks.AIR.u(), 3);
                     TileEntity var19 = var1.r(var18.b());
                     if(var19 instanceof class_apw) {
                        ((class_apw)var19).a(class_azs.m, var2.nextLong());
                     }
                  }
               }
            } else {
               BlockPosition var20 = class_awn.a(var9, new BlockPosition(3, 0, 5));
               var1.a((BlockPosition)var5.a((BaseBlockPosition)var20), (IBlockData)Blocks.aJ.u(), 3);
            }

            return true;
         }
      }
   }

   public static class class_e_in_class_avz extends WorldGenRegistration.WorldGenScatteredPiece {
      private boolean e;

      public class_e_in_class_avz() {
      }

      public class_e_in_class_avz(Random var1, int var2, int var3) {
         super(var1, var2, 64, var3, 7, 7, 9);
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("Witch", this.e);
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.e = var1.p("Witch");
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(!this.a(var1, var3, 0)) {
            return false;
         } else {
            this.a(var1, var3, 1, 1, 1, 5, 1, 7, Blocks.f.a(BlockWood.EnumLogVariant.SPRUCE.a()), Blocks.f.a(BlockWood.EnumLogVariant.SPRUCE.a()), false);
            this.a(var1, var3, 1, 4, 2, 5, 4, 7, Blocks.f.a(BlockWood.EnumLogVariant.SPRUCE.a()), Blocks.f.a(BlockWood.EnumLogVariant.SPRUCE.a()), false);
            this.a(var1, var3, 2, 1, 0, 4, 1, 0, Blocks.f.a(BlockWood.EnumLogVariant.SPRUCE.a()), Blocks.f.a(BlockWood.EnumLogVariant.SPRUCE.a()), false);
            this.a(var1, var3, 2, 2, 2, 3, 3, 2, Blocks.f.a(BlockWood.EnumLogVariant.SPRUCE.a()), Blocks.f.a(BlockWood.EnumLogVariant.SPRUCE.a()), false);
            this.a(var1, var3, 1, 2, 3, 1, 3, 6, Blocks.f.a(BlockWood.EnumLogVariant.SPRUCE.a()), Blocks.f.a(BlockWood.EnumLogVariant.SPRUCE.a()), false);
            this.a(var1, var3, 5, 2, 3, 5, 3, 6, Blocks.f.a(BlockWood.EnumLogVariant.SPRUCE.a()), Blocks.f.a(BlockWood.EnumLogVariant.SPRUCE.a()), false);
            this.a(var1, var3, 2, 2, 7, 4, 3, 7, Blocks.f.a(BlockWood.EnumLogVariant.SPRUCE.a()), Blocks.f.a(BlockWood.EnumLogVariant.SPRUCE.a()), false);
            this.a(var1, var3, 1, 0, 2, 1, 3, 2, Blocks.r.u(), Blocks.r.u(), false);
            this.a(var1, var3, 5, 0, 2, 5, 3, 2, Blocks.r.u(), Blocks.r.u(), false);
            this.a(var1, var3, 1, 0, 7, 1, 3, 7, Blocks.r.u(), Blocks.r.u(), false);
            this.a(var1, var3, 5, 0, 7, 5, 3, 7, Blocks.r.u(), Blocks.r.u(), false);
            this.a(var1, Blocks.aO.u(), 2, 3, 2, var3);
            this.a(var1, Blocks.aO.u(), 3, 3, 7, var3);
            this.a(var1, Blocks.AIR.u(), 1, 3, 4, var3);
            this.a(var1, Blocks.AIR.u(), 5, 3, 4, var3);
            this.a(var1, Blocks.AIR.u(), 5, 3, 5, var3);
            this.a(var1, Blocks.ca.u().set(BlockFlowerPot.b, BlockFlowerPot.EnumFlowerPotContents.MUSHROOM_RED), 1, 3, 5, var3);
            this.a(var1, Blocks.ai.u(), 3, 2, 6, var3);
            this.a(var1, Blocks.bE.u(), 4, 2, 6, var3);
            this.a(var1, Blocks.aO.u(), 1, 2, 1, var3);
            this.a(var1, Blocks.aO.u(), 5, 2, 1, var3);
            IBlockData var4 = Blocks.bU.u().set(BlockStairs.a, EnumDirection.NORTH);
            IBlockData var5 = Blocks.bU.u().set(BlockStairs.a, EnumDirection.EAST);
            IBlockData var6 = Blocks.bU.u().set(BlockStairs.a, EnumDirection.WEST);
            IBlockData var7 = Blocks.bU.u().set(BlockStairs.a, EnumDirection.SOUTH);
            this.a(var1, var3, 0, 4, 1, 6, 4, 1, var4, var4, false);
            this.a(var1, var3, 0, 4, 2, 0, 4, 7, var5, var5, false);
            this.a(var1, var3, 6, 4, 2, 6, 4, 7, var6, var6, false);
            this.a(var1, var3, 0, 4, 8, 6, 4, 8, var7, var7, false);

            int var8;
            int var9;
            for(var8 = 2; var8 <= 7; var8 += 5) {
               for(var9 = 1; var9 <= 5; var9 += 4) {
                  this.b(var1, Blocks.r.u(), var9, -1, var8, var3);
               }
            }

            if(!this.e) {
               var8 = this.a(2, 5);
               var9 = this.d(2);
               int var10 = this.b(2, 5);
               if(var3.b((BaseBlockPosition)(new BlockPosition(var8, var9, var10)))) {
                  this.e = true;
                  EntityWitch var11 = new EntityWitch(var1);
                  var11.b((double)var8 + 0.5D, (double)var9, (double)var10 + 0.5D, 0.0F, 0.0F);
                  var11.a(var1.D(new BlockPosition(var8, var9, var10)), (class_sc)null);
                  var1.a((Entity)var11);
               }
            }

            return true;
         }
      }
   }

   public static class WorldGenJungleTemple extends WorldGenRegistration.WorldGenScatteredPiece {
      private boolean e;
      private boolean f;
      private boolean g;
      private boolean h;
      private static final List i = Lists.newArrayList((Object[])(new class_ow[]{new class_ow(Items.g, 0, 2, 7, 30)}));
      private static WorldGenRegistration.WorldGenJungleTemple.class_c_in_class_avz$class_a_in_class_c_in_class_avz j = new WorldGenRegistration.WorldGenJungleTemple.class_c_in_class_avz$class_a_in_class_c_in_class_avz();

      public WorldGenJungleTemple() {
      }

      public WorldGenJungleTemple(Random var1, int var2, int var3) {
         super(var1, var2, 64, var3, 12, 10, 15);
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("placedMainChest", this.e);
         var1.a("placedHiddenChest", this.f);
         var1.a("placedTrap1", this.g);
         var1.a("placedTrap2", this.h);
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.e = var1.p("placedMainChest");
         this.f = var1.p("placedHiddenChest");
         this.g = var1.p("placedTrap1");
         this.h = var1.p("placedTrap2");
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(!this.a(var1, var3, 0)) {
            return false;
         } else {
            this.a(var1, var3, 0, -4, 0, this.a - 1, 0, this.c - 1, false, var2, j);
            this.a(var1, var3, 2, 1, 2, 9, 2, 2, false, var2, j);
            this.a(var1, var3, 2, 1, 12, 9, 2, 12, false, var2, j);
            this.a(var1, var3, 2, 1, 3, 2, 2, 11, false, var2, j);
            this.a(var1, var3, 9, 1, 3, 9, 2, 11, false, var2, j);
            this.a(var1, var3, 1, 3, 1, 10, 6, 1, false, var2, j);
            this.a(var1, var3, 1, 3, 13, 10, 6, 13, false, var2, j);
            this.a(var1, var3, 1, 3, 2, 1, 6, 12, false, var2, j);
            this.a(var1, var3, 10, 3, 2, 10, 6, 12, false, var2, j);
            this.a(var1, var3, 2, 3, 2, 9, 3, 12, false, var2, j);
            this.a(var1, var3, 2, 6, 2, 9, 6, 12, false, var2, j);
            this.a(var1, var3, 3, 7, 3, 8, 7, 11, false, var2, j);
            this.a(var1, var3, 4, 8, 4, 7, 8, 10, false, var2, j);
            this.a(var1, var3, 3, 1, 3, 8, 2, 11);
            this.a(var1, var3, 4, 3, 6, 7, 3, 9);
            this.a(var1, var3, 2, 4, 2, 9, 5, 12);
            this.a(var1, var3, 4, 6, 5, 7, 6, 9);
            this.a(var1, var3, 5, 7, 6, 6, 7, 8);
            this.a(var1, var3, 5, 1, 2, 6, 2, 2);
            this.a(var1, var3, 5, 2, 12, 6, 2, 12);
            this.a(var1, var3, 5, 5, 1, 6, 5, 1);
            this.a(var1, var3, 5, 5, 13, 6, 5, 13);
            this.a(var1, Blocks.AIR.u(), 1, 5, 5, var3);
            this.a(var1, Blocks.AIR.u(), 10, 5, 5, var3);
            this.a(var1, Blocks.AIR.u(), 1, 5, 9, var3);
            this.a(var1, Blocks.AIR.u(), 10, 5, 9, var3);

            int var4;
            for(var4 = 0; var4 <= 14; var4 += 14) {
               this.a(var1, var3, 2, 4, var4, 2, 5, var4, false, var2, j);
               this.a(var1, var3, 4, 4, var4, 4, 5, var4, false, var2, j);
               this.a(var1, var3, 7, 4, var4, 7, 5, var4, false, var2, j);
               this.a(var1, var3, 9, 4, var4, 9, 5, var4, false, var2, j);
            }

            this.a(var1, var3, 5, 6, 0, 6, 6, 0, false, var2, j);

            for(var4 = 0; var4 <= 11; var4 += 11) {
               for(int var5 = 2; var5 <= 12; var5 += 2) {
                  this.a(var1, var3, var4, 4, var5, var4, 5, var5, false, var2, j);
               }

               this.a(var1, var3, var4, 6, 5, var4, 6, 5, false, var2, j);
               this.a(var1, var3, var4, 6, 9, var4, 6, 9, false, var2, j);
            }

            this.a(var1, var3, 2, 7, 2, 2, 9, 2, false, var2, j);
            this.a(var1, var3, 9, 7, 2, 9, 9, 2, false, var2, j);
            this.a(var1, var3, 2, 7, 12, 2, 9, 12, false, var2, j);
            this.a(var1, var3, 9, 7, 12, 9, 9, 12, false, var2, j);
            this.a(var1, var3, 4, 9, 4, 4, 9, 4, false, var2, j);
            this.a(var1, var3, 7, 9, 4, 7, 9, 4, false, var2, j);
            this.a(var1, var3, 4, 9, 10, 4, 9, 10, false, var2, j);
            this.a(var1, var3, 7, 9, 10, 7, 9, 10, false, var2, j);
            this.a(var1, var3, 5, 9, 7, 6, 9, 7, false, var2, j);
            IBlockData var9 = Blocks.aw.u().set(BlockStairs.a, EnumDirection.EAST);
            IBlockData var10 = Blocks.aw.u().set(BlockStairs.a, EnumDirection.WEST);
            IBlockData var6 = Blocks.aw.u().set(BlockStairs.a, EnumDirection.SOUTH);
            IBlockData var7 = Blocks.aw.u().set(BlockStairs.a, EnumDirection.NORTH);
            this.a(var1, var7, 5, 9, 6, var3);
            this.a(var1, var7, 6, 9, 6, var3);
            this.a(var1, var6, 5, 9, 8, var3);
            this.a(var1, var6, 6, 9, 8, var3);
            this.a(var1, var7, 4, 0, 0, var3);
            this.a(var1, var7, 5, 0, 0, var3);
            this.a(var1, var7, 6, 0, 0, var3);
            this.a(var1, var7, 7, 0, 0, var3);
            this.a(var1, var7, 4, 1, 8, var3);
            this.a(var1, var7, 4, 2, 9, var3);
            this.a(var1, var7, 4, 3, 10, var3);
            this.a(var1, var7, 7, 1, 8, var3);
            this.a(var1, var7, 7, 2, 9, var3);
            this.a(var1, var7, 7, 3, 10, var3);
            this.a(var1, var3, 4, 1, 9, 4, 1, 9, false, var2, j);
            this.a(var1, var3, 7, 1, 9, 7, 1, 9, false, var2, j);
            this.a(var1, var3, 4, 1, 10, 7, 2, 10, false, var2, j);
            this.a(var1, var3, 5, 4, 5, 6, 4, 5, false, var2, j);
            this.a(var1, var9, 4, 4, 5, var3);
            this.a(var1, var10, 7, 4, 5, var3);

            int var8;
            for(var8 = 0; var8 < 4; ++var8) {
               this.a(var1, var6, 5, 0 - var8, 6 + var8, var3);
               this.a(var1, var6, 6, 0 - var8, 6 + var8, var3);
               this.a(var1, var3, 5, 0 - var8, 7 + var8, 6, 0 - var8, 9 + var8);
            }

            this.a(var1, var3, 1, -3, 12, 10, -1, 13);
            this.a(var1, var3, 1, -3, 1, 3, -1, 13);
            this.a(var1, var3, 1, -3, 1, 9, -1, 5);

            for(var8 = 1; var8 <= 13; var8 += 2) {
               this.a(var1, var3, 1, -3, var8, 1, -2, var8, false, var2, j);
            }

            for(var8 = 2; var8 <= 12; var8 += 2) {
               this.a(var1, var3, 1, -1, var8, 3, -1, var8, false, var2, j);
            }

            this.a(var1, var3, 2, -2, 1, 5, -2, 1, false, var2, j);
            this.a(var1, var3, 7, -2, 1, 9, -2, 1, false, var2, j);
            this.a(var1, var3, 6, -3, 1, 6, -3, 1, false, var2, j);
            this.a(var1, var3, 6, -1, 1, 6, -1, 1, false, var2, j);
            this.a(var1, Blocks.bR.u().set(class_aph.a, EnumDirection.EAST).set(class_aph.c, Boolean.valueOf(true)), 1, -3, 8, var3);
            this.a(var1, Blocks.bR.u().set(class_aph.a, EnumDirection.WEST).set(class_aph.c, Boolean.valueOf(true)), 4, -3, 8, var3);
            this.a(var1, Blocks.bS.u().set(class_apg.b, Boolean.valueOf(true)), 2, -3, 8, var3);
            this.a(var1, Blocks.bS.u().set(class_apg.b, Boolean.valueOf(true)), 3, -3, 8, var3);
            this.a(var1, Blocks.af.u(), 5, -3, 7, var3);
            this.a(var1, Blocks.af.u(), 5, -3, 6, var3);
            this.a(var1, Blocks.af.u(), 5, -3, 5, var3);
            this.a(var1, Blocks.af.u(), 5, -3, 4, var3);
            this.a(var1, Blocks.af.u(), 5, -3, 3, var3);
            this.a(var1, Blocks.af.u(), 5, -3, 2, var3);
            this.a(var1, Blocks.af.u(), 5, -3, 1, var3);
            this.a(var1, Blocks.af.u(), 4, -3, 1, var3);
            this.a(var1, Blocks.Y.u(), 3, -3, 1, var3);
            if(!this.g) {
               this.g = this.a(var1, var3, var2, 3, -2, 1, EnumDirection.NORTH, i, 2);
            }

            this.a(var1, Blocks.bn.u().set(BlockVine.d, Boolean.valueOf(true)), 3, -2, 2, var3);
            this.a(var1, Blocks.bR.u().set(class_aph.a, EnumDirection.NORTH).set(class_aph.c, Boolean.valueOf(true)), 7, -3, 1, var3);
            this.a(var1, Blocks.bR.u().set(class_aph.a, EnumDirection.SOUTH).set(class_aph.c, Boolean.valueOf(true)), 7, -3, 5, var3);
            this.a(var1, Blocks.bS.u().set(class_apg.b, Boolean.valueOf(true)), 7, -3, 2, var3);
            this.a(var1, Blocks.bS.u().set(class_apg.b, Boolean.valueOf(true)), 7, -3, 3, var3);
            this.a(var1, Blocks.bS.u().set(class_apg.b, Boolean.valueOf(true)), 7, -3, 4, var3);
            this.a(var1, Blocks.af.u(), 8, -3, 6, var3);
            this.a(var1, Blocks.af.u(), 9, -3, 6, var3);
            this.a(var1, Blocks.af.u(), 9, -3, 5, var3);
            this.a(var1, Blocks.Y.u(), 9, -3, 4, var3);
            this.a(var1, Blocks.af.u(), 9, -2, 4, var3);
            if(!this.h) {
               this.h = this.a(var1, var3, var2, 9, -2, 3, EnumDirection.WEST, i, 2);
            }

            this.a(var1, Blocks.bn.u().set(BlockVine.c, Boolean.valueOf(true)), 8, -1, 3, var3);
            this.a(var1, Blocks.bn.u().set(BlockVine.c, Boolean.valueOf(true)), 8, -2, 3, var3);
            if(!this.e) {
               this.e = this.a(var1, var3, var2, 8, -3, 3, class_azs.l);
            }

            this.a(var1, Blocks.Y.u(), 9, -3, 2, var3);
            this.a(var1, Blocks.Y.u(), 8, -3, 1, var3);
            this.a(var1, Blocks.Y.u(), 4, -3, 5, var3);
            this.a(var1, Blocks.Y.u(), 5, -2, 5, var3);
            this.a(var1, Blocks.Y.u(), 5, -1, 5, var3);
            this.a(var1, Blocks.Y.u(), 6, -3, 5, var3);
            this.a(var1, Blocks.Y.u(), 7, -2, 5, var3);
            this.a(var1, Blocks.Y.u(), 7, -1, 5, var3);
            this.a(var1, Blocks.Y.u(), 8, -3, 5, var3);
            this.a(var1, var3, 9, -1, 1, 9, -1, 5, false, var2, j);
            this.a(var1, var3, 8, -3, 8, 10, -1, 10);
            this.a(var1, Blocks.bf.a(BlockSmoothBrick.e), 8, -2, 11, var3);
            this.a(var1, Blocks.bf.a(BlockSmoothBrick.e), 9, -2, 11, var3);
            this.a(var1, Blocks.bf.a(BlockSmoothBrick.e), 10, -2, 11, var3);
            IBlockData var11 = Blocks.ay.u().set(BlockLever.a, BlockLever.EnumLeverPosition.NORTH);
            this.a(var1, var11, 8, -2, 12, var3);
            this.a(var1, var11, 9, -2, 12, var3);
            this.a(var1, var11, 10, -2, 12, var3);
            this.a(var1, var3, 8, -3, 8, 8, -3, 10, false, var2, j);
            this.a(var1, var3, 10, -3, 8, 10, -3, 10, false, var2, j);
            this.a(var1, Blocks.Y.u(), 10, -2, 9, var3);
            this.a(var1, Blocks.af.u(), 8, -2, 9, var3);
            this.a(var1, Blocks.af.u(), 8, -2, 10, var3);
            this.a(var1, Blocks.af.u(), 10, -1, 9, var3);
            this.a(var1, Blocks.F.u().set(class_aqt.H, EnumDirection.UP), 9, -2, 8, var3);
            this.a(var1, Blocks.F.u().set(class_aqt.H, EnumDirection.WEST), 10, -2, 8, var3);
            this.a(var1, Blocks.F.u().set(class_aqt.H, EnumDirection.WEST), 10, -1, 8, var3);
            this.a(var1, Blocks.bb.u().set(BlockRepeater.D, EnumDirection.NORTH), 10, -2, 10, var3);
            if(!this.f) {
               this.f = this.a(var1, var3, var2, 9, -3, 10, class_azs.l);
            }

            return true;
         }
      }

      static class class_c_in_class_avz$class_a_in_class_c_in_class_avz extends StructurePiece.class_a_in_class_awf {
         private class_c_in_class_avz$class_a_in_class_c_in_class_avz() {
         }

         public void a(Random var1, int var2, int var3, int var4, boolean var5) {
            if(var1.nextFloat() < 0.4F) {
               this.a = Blocks.e.u();
            } else {
               this.a = Blocks.Y.u();
            }

         }

         // $FF: synthetic method
         class_c_in_class_avz$class_a_in_class_c_in_class_avz(WorldGenRegistration.SyntheticClass_1 var1) {
            this();
         }
      }
   }

   public static class WorldGenPyramidPiece extends WorldGenRegistration.WorldGenScatteredPiece {
      private boolean[] e = new boolean[4];

      public WorldGenPyramidPiece() {
      }

      public WorldGenPyramidPiece(Random var1, int var2, int var3) {
         super(var1, var2, 64, var3, 21, 15, 21);
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("hasPlacedChest0", this.e[0]);
         var1.a("hasPlacedChest1", this.e[1]);
         var1.a("hasPlacedChest2", this.e[2]);
         var1.a("hasPlacedChest3", this.e[3]);
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.e[0] = var1.p("hasPlacedChest0");
         this.e[1] = var1.p("hasPlacedChest1");
         this.e[2] = var1.p("hasPlacedChest2");
         this.e[3] = var1.p("hasPlacedChest3");
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         this.a(var1, var3, 0, -4, 0, this.a - 1, 0, this.c - 1, Blocks.A.u(), Blocks.A.u(), false);

         int var4;
         for(var4 = 1; var4 <= 9; ++var4) {
            this.a(var1, var3, var4, var4, var4, this.a - 1 - var4, var4, this.c - 1 - var4, Blocks.A.u(), Blocks.A.u(), false);
            this.a(var1, var3, var4 + 1, var4, var4 + 1, this.a - 2 - var4, var4, this.c - 2 - var4, Blocks.AIR.u(), Blocks.AIR.u(), false);
         }

         for(var4 = 0; var4 < this.a; ++var4) {
            for(int var5 = 0; var5 < this.c; ++var5) {
               byte var6 = -5;
               this.b(var1, Blocks.A.u(), var4, var6, var5, var3);
            }
         }

         IBlockData var14 = Blocks.bO.u().set(BlockStairs.a, EnumDirection.NORTH);
         IBlockData var15 = Blocks.bO.u().set(BlockStairs.a, EnumDirection.SOUTH);
         IBlockData var16 = Blocks.bO.u().set(BlockStairs.a, EnumDirection.EAST);
         IBlockData var7 = Blocks.bO.u().set(BlockStairs.a, EnumDirection.WEST);
         int var8 = ~EnumColor.ORANGE.b() & 15;
         int var9 = ~EnumColor.BLUE.b() & 15;
         this.a(var1, var3, 0, 0, 0, 4, 9, 4, Blocks.A.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 1, 10, 1, 3, 10, 3, Blocks.A.u(), Blocks.A.u(), false);
         this.a(var1, var14, 2, 10, 0, var3);
         this.a(var1, var15, 2, 10, 4, var3);
         this.a(var1, var16, 0, 10, 2, var3);
         this.a(var1, var7, 4, 10, 2, var3);
         this.a(var1, var3, this.a - 5, 0, 0, this.a - 1, 9, 4, Blocks.A.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, this.a - 4, 10, 1, this.a - 2, 10, 3, Blocks.A.u(), Blocks.A.u(), false);
         this.a(var1, var14, this.a - 3, 10, 0, var3);
         this.a(var1, var15, this.a - 3, 10, 4, var3);
         this.a(var1, var16, this.a - 5, 10, 2, var3);
         this.a(var1, var7, this.a - 1, 10, 2, var3);
         this.a(var1, var3, 8, 0, 0, 12, 4, 4, Blocks.A.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 9, 1, 0, 11, 3, 4, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), 9, 1, 1, var3);
         this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), 9, 2, 1, var3);
         this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), 9, 3, 1, var3);
         this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), 10, 3, 1, var3);
         this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), 11, 3, 1, var3);
         this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), 11, 2, 1, var3);
         this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), 11, 1, 1, var3);
         this.a(var1, var3, 4, 1, 1, 8, 3, 3, Blocks.A.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 4, 1, 2, 8, 2, 2, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 12, 1, 1, 16, 3, 3, Blocks.A.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 12, 1, 2, 16, 2, 2, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 5, 4, 5, this.a - 6, 4, this.c - 6, Blocks.A.u(), Blocks.A.u(), false);
         this.a(var1, var3, 9, 4, 9, 11, 4, 11, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 8, 1, 8, 8, 3, 8, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), false);
         this.a(var1, var3, 12, 1, 8, 12, 3, 8, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), false);
         this.a(var1, var3, 8, 1, 12, 8, 3, 12, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), false);
         this.a(var1, var3, 12, 1, 12, 12, 3, 12, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), false);
         this.a(var1, var3, 1, 1, 5, 4, 4, 11, Blocks.A.u(), Blocks.A.u(), false);
         this.a(var1, var3, this.a - 5, 1, 5, this.a - 2, 4, 11, Blocks.A.u(), Blocks.A.u(), false);
         this.a(var1, var3, 6, 7, 9, 6, 7, 11, Blocks.A.u(), Blocks.A.u(), false);
         this.a(var1, var3, this.a - 7, 7, 9, this.a - 7, 7, 11, Blocks.A.u(), Blocks.A.u(), false);
         this.a(var1, var3, 5, 5, 9, 5, 7, 11, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), false);
         this.a(var1, var3, this.a - 6, 5, 9, this.a - 6, 7, 11, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), false);
         this.a(var1, Blocks.AIR.u(), 5, 5, 10, var3);
         this.a(var1, Blocks.AIR.u(), 5, 6, 10, var3);
         this.a(var1, Blocks.AIR.u(), 6, 6, 10, var3);
         this.a(var1, Blocks.AIR.u(), this.a - 6, 5, 10, var3);
         this.a(var1, Blocks.AIR.u(), this.a - 6, 6, 10, var3);
         this.a(var1, Blocks.AIR.u(), this.a - 7, 6, 10, var3);
         this.a(var1, var3, 2, 4, 4, 2, 6, 4, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, this.a - 3, 4, 4, this.a - 3, 6, 4, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var14, 2, 4, 5, var3);
         this.a(var1, var14, 2, 3, 4, var3);
         this.a(var1, var14, this.a - 3, 4, 5, var3);
         this.a(var1, var14, this.a - 3, 3, 4, var3);
         this.a(var1, var3, 1, 1, 3, 2, 2, 3, Blocks.A.u(), Blocks.A.u(), false);
         this.a(var1, var3, this.a - 3, 1, 3, this.a - 2, 2, 3, Blocks.A.u(), Blocks.A.u(), false);
         this.a(var1, Blocks.A.u(), 1, 1, 2, var3);
         this.a(var1, Blocks.A.u(), this.a - 2, 1, 2, var3);
         this.a(var1, Blocks.U.a(BlockDoubleStepAbstract.EnumStoneSlabVariant.SAND.a()), 1, 2, 2, var3);
         this.a(var1, Blocks.U.a(BlockDoubleStepAbstract.EnumStoneSlabVariant.SAND.a()), this.a - 2, 2, 2, var3);
         this.a(var1, var7, 2, 1, 2, var3);
         this.a(var1, var16, this.a - 3, 1, 2, var3);
         this.a(var1, var3, 4, 3, 5, 4, 3, 18, Blocks.A.u(), Blocks.A.u(), false);
         this.a(var1, var3, this.a - 5, 3, 5, this.a - 5, 3, 17, Blocks.A.u(), Blocks.A.u(), false);
         this.a(var1, var3, 3, 1, 5, 4, 2, 16, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, this.a - 6, 1, 5, this.a - 5, 2, 16, Blocks.AIR.u(), Blocks.AIR.u(), false);

         int var10;
         for(var10 = 5; var10 <= 17; var10 += 2) {
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), 4, 1, var10, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.CHISELED.a()), 4, 2, var10, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), this.a - 5, 1, var10, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.CHISELED.a()), this.a - 5, 2, var10, var3);
         }

         this.a(var1, Blocks.cu.a(var8), 10, 0, 7, var3);
         this.a(var1, Blocks.cu.a(var8), 10, 0, 8, var3);
         this.a(var1, Blocks.cu.a(var8), 9, 0, 9, var3);
         this.a(var1, Blocks.cu.a(var8), 11, 0, 9, var3);
         this.a(var1, Blocks.cu.a(var8), 8, 0, 10, var3);
         this.a(var1, Blocks.cu.a(var8), 12, 0, 10, var3);
         this.a(var1, Blocks.cu.a(var8), 7, 0, 10, var3);
         this.a(var1, Blocks.cu.a(var8), 13, 0, 10, var3);
         this.a(var1, Blocks.cu.a(var8), 9, 0, 11, var3);
         this.a(var1, Blocks.cu.a(var8), 11, 0, 11, var3);
         this.a(var1, Blocks.cu.a(var8), 10, 0, 12, var3);
         this.a(var1, Blocks.cu.a(var8), 10, 0, 13, var3);
         this.a(var1, Blocks.cu.a(var9), 10, 0, 10, var3);

         for(var10 = 0; var10 <= this.a - 1; var10 += this.a - 1) {
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10, 2, 1, var3);
            this.a(var1, Blocks.cu.a(var8), var10, 2, 2, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10, 2, 3, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10, 3, 1, var3);
            this.a(var1, Blocks.cu.a(var8), var10, 3, 2, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10, 3, 3, var3);
            this.a(var1, Blocks.cu.a(var8), var10, 4, 1, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.CHISELED.a()), var10, 4, 2, var3);
            this.a(var1, Blocks.cu.a(var8), var10, 4, 3, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10, 5, 1, var3);
            this.a(var1, Blocks.cu.a(var8), var10, 5, 2, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10, 5, 3, var3);
            this.a(var1, Blocks.cu.a(var8), var10, 6, 1, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.CHISELED.a()), var10, 6, 2, var3);
            this.a(var1, Blocks.cu.a(var8), var10, 6, 3, var3);
            this.a(var1, Blocks.cu.a(var8), var10, 7, 1, var3);
            this.a(var1, Blocks.cu.a(var8), var10, 7, 2, var3);
            this.a(var1, Blocks.cu.a(var8), var10, 7, 3, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10, 8, 1, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10, 8, 2, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10, 8, 3, var3);
         }

         for(var10 = 2; var10 <= this.a - 3; var10 += this.a - 3 - 2) {
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10 - 1, 2, 0, var3);
            this.a(var1, Blocks.cu.a(var8), var10, 2, 0, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10 + 1, 2, 0, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10 - 1, 3, 0, var3);
            this.a(var1, Blocks.cu.a(var8), var10, 3, 0, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10 + 1, 3, 0, var3);
            this.a(var1, Blocks.cu.a(var8), var10 - 1, 4, 0, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.CHISELED.a()), var10, 4, 0, var3);
            this.a(var1, Blocks.cu.a(var8), var10 + 1, 4, 0, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10 - 1, 5, 0, var3);
            this.a(var1, Blocks.cu.a(var8), var10, 5, 0, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10 + 1, 5, 0, var3);
            this.a(var1, Blocks.cu.a(var8), var10 - 1, 6, 0, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.CHISELED.a()), var10, 6, 0, var3);
            this.a(var1, Blocks.cu.a(var8), var10 + 1, 6, 0, var3);
            this.a(var1, Blocks.cu.a(var8), var10 - 1, 7, 0, var3);
            this.a(var1, Blocks.cu.a(var8), var10, 7, 0, var3);
            this.a(var1, Blocks.cu.a(var8), var10 + 1, 7, 0, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10 - 1, 8, 0, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10, 8, 0, var3);
            this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), var10 + 1, 8, 0, var3);
         }

         this.a(var1, var3, 8, 4, 0, 12, 6, 0, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), false);
         this.a(var1, Blocks.AIR.u(), 8, 6, 0, var3);
         this.a(var1, Blocks.AIR.u(), 12, 6, 0, var3);
         this.a(var1, Blocks.cu.a(var8), 9, 5, 0, var3);
         this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.CHISELED.a()), 10, 5, 0, var3);
         this.a(var1, Blocks.cu.a(var8), 11, 5, 0, var3);
         this.a(var1, var3, 8, -14, 8, 12, -11, 12, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), false);
         this.a(var1, var3, 8, -10, 8, 12, -10, 12, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.CHISELED.a()), Blocks.A.a(BlockSandStone.EnumSandstoneVariant.CHISELED.a()), false);
         this.a(var1, var3, 8, -9, 8, 12, -9, 12, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), false);
         this.a(var1, var3, 8, -8, 8, 12, -1, 12, Blocks.A.u(), Blocks.A.u(), false);
         this.a(var1, var3, 9, -11, 9, 11, -1, 11, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, Blocks.az.u(), 10, -11, 10, var3);
         this.a(var1, var3, 9, -13, 9, 11, -13, 11, Blocks.W.u(), Blocks.AIR.u(), false);
         this.a(var1, Blocks.AIR.u(), 8, -11, 10, var3);
         this.a(var1, Blocks.AIR.u(), 8, -10, 10, var3);
         this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.CHISELED.a()), 7, -10, 10, var3);
         this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), 7, -11, 10, var3);
         this.a(var1, Blocks.AIR.u(), 12, -11, 10, var3);
         this.a(var1, Blocks.AIR.u(), 12, -10, 10, var3);
         this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.CHISELED.a()), 13, -10, 10, var3);
         this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), 13, -11, 10, var3);
         this.a(var1, Blocks.AIR.u(), 10, -11, 8, var3);
         this.a(var1, Blocks.AIR.u(), 10, -10, 8, var3);
         this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.CHISELED.a()), 10, -10, 7, var3);
         this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), 10, -11, 7, var3);
         this.a(var1, Blocks.AIR.u(), 10, -11, 12, var3);
         this.a(var1, Blocks.AIR.u(), 10, -10, 12, var3);
         this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.CHISELED.a()), 10, -10, 13, var3);
         this.a(var1, Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), 10, -11, 13, var3);
         Iterator var17 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

         while(var17.hasNext()) {
            EnumDirection var11 = (EnumDirection)var17.next();
            if(!this.e[var11.b()]) {
               int var12 = var11.g() * 2;
               int var13 = var11.i() * 2;
               this.e[var11.b()] = this.a(var1, var3, var2, 10 + var12, -11, 10 + var13, class_azs.k);
            }
         }

         return true;
      }
   }

   abstract static class WorldGenScatteredPiece extends StructurePiece {
      protected int a;
      protected int b;
      protected int c;
      protected int d = -1;

      public WorldGenScatteredPiece() {
      }

      protected WorldGenScatteredPiece(Random var1, int var2, int var3, int var4, int var5, int var6, int var7) {
         super(0);
         this.a = var5;
         this.b = var6;
         this.c = var7;
         this.a(EnumDirection.EnumDirectionLimit.HORIZONTAL.a(var1));
         if(this.e().k() == EnumDirection.class_a_in_class_cq.Z) {
            this.l = new StructureBoundingBox(var2, var3, var4, var2 + var5 - 1, var3 + var6 - 1, var4 + var7 - 1);
         } else {
            this.l = new StructureBoundingBox(var2, var3, var4, var2 + var7 - 1, var3 + var6 - 1, var4 + var5 - 1);
         }

      }

      protected void a(NBTTagCompound var1) {
         var1.a("Width", this.a);
         var1.a("Height", this.b);
         var1.a("Depth", this.c);
         var1.a("HPos", this.d);
      }

      protected void b(NBTTagCompound var1) {
         this.a = var1.h("Width");
         this.b = var1.h("Height");
         this.c = var1.h("Depth");
         this.d = var1.h("HPos");
      }

      protected boolean a(World var1, StructureBoundingBox var2, int var3) {
         if(this.d >= 0) {
            return true;
         } else {
            int var4 = 0;
            int var5 = 0;
            BlockPosition.class_a_in_class_cj var6 = new BlockPosition.class_a_in_class_cj();

            for(int var7 = this.l.c; var7 <= this.l.f; ++var7) {
               for(int var8 = this.l.a; var8 <= this.l.d; ++var8) {
                  var6.c(var8, 64, var7);
                  if(var2.b((BaseBlockPosition)var6)) {
                     var4 += Math.max(var1.q(var6).q(), var1.s.i());
                     ++var5;
                  }
               }
            }

            if(var5 == 0) {
               return false;
            } else {
               this.d = var4 / var5;
               this.l.a(0, this.d - this.l.b + var3, 0);
               return true;
            }
         }
      }
   }
}
