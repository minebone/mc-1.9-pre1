package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.Entity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.class_apx;
import net.minecraft.server.class_aqm;
import net.minecraft.server.class_bbk;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;

public class CommandStats extends CommandAbstract {
   public String c() {
      return "stats";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.stats.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 1) {
         throw new class_cf("commands.stats.usage", new Object[0]);
      } else {
         boolean var4;
         if(var3[0].equals("entity")) {
            var4 = false;
         } else {
            if(!var3[0].equals("block")) {
               throw new class_cf("commands.stats.usage", new Object[0]);
            }

            var4 = true;
         }

         byte var5;
         if(var4) {
            if(var3.length < 5) {
               throw new class_cf("commands.stats.block.usage", new Object[0]);
            }

            var5 = 4;
         } else {
            if(var3.length < 3) {
               throw new class_cf("commands.stats.entity.usage", new Object[0]);
            }

            var5 = 2;
         }

         int var12 = var5 + 1;
         String var6 = var3[var5];
         if("set".equals(var6)) {
            if(var3.length < var12 + 3) {
               if(var12 == 5) {
                  throw new class_cf("commands.stats.block.set.usage", new Object[0]);
               }

               throw new class_cf("commands.stats.entity.set.usage", new Object[0]);
            }
         } else {
            if(!"clear".equals(var6)) {
               throw new class_cf("commands.stats.usage", new Object[0]);
            }

            if(var3.length < var12 + 1) {
               if(var12 == 5) {
                  throw new class_cf("commands.stats.block.clear.usage", new Object[0]);
               }

               throw new class_cf("commands.stats.entity.clear.usage", new Object[0]);
            }
         }

         CommandObjectiveExecutor.EnumCommandResult var7 = CommandObjectiveExecutor.EnumCommandResult.a(var3[var12++]);
         if(var7 == null) {
            throw new class_bz("commands.stats.failed", new Object[0]);
         } else {
            World var8 = var2.e();
            CommandObjectiveExecutor var9;
            BlockPosition var10;
            TileEntity var11;
            if(var4) {
               var10 = a(var2, var3, 1, false);
               var11 = var8.r(var10);
               if(var11 == null) {
                  throw new class_bz("commands.stats.noCompatibleBlock", new Object[]{Integer.valueOf(var10.p()), Integer.valueOf(var10.q()), Integer.valueOf(var10.r())});
               }

               if(var11 instanceof class_apx) {
                  var9 = ((class_apx)var11).c();
               } else {
                  if(!(var11 instanceof class_aqm)) {
                     throw new class_bz("commands.stats.noCompatibleBlock", new Object[]{Integer.valueOf(var10.p()), Integer.valueOf(var10.q()), Integer.valueOf(var10.r())});
                  }

                  var9 = ((class_aqm)var11).d();
               }
            } else {
               Entity var13 = b(var1, var2, var3[1]);
               var9 = var13.bo();
            }

            if("set".equals(var6)) {
               String var14 = var3[var12++];
               String var15 = var3[var12];
               if(var14.isEmpty() || var15.isEmpty()) {
                  throw new class_bz("commands.stats.failed", new Object[0]);
               }

               CommandObjectiveExecutor.a(var9, var7, var14, var15);
               a(var2, this, "commands.stats.success", new Object[]{var7.b(), var15, var14});
            } else if("clear".equals(var6)) {
               CommandObjectiveExecutor.a(var9, var7, (String)null, (String)null);
               a(var2, this, "commands.stats.cleared", new Object[]{var7.b()});
            }

            if(var4) {
               var10 = a(var2, var3, 1, false);
               var11 = var8.r(var10);
               var11.v_();
            }

         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, new String[]{"entity", "block"}):(var3.length == 2 && var3[0].equals("entity")?a(var3, var1.J()):(var3.length >= 2 && var3.length <= 4 && var3[0].equals("block")?a(var3, 1, var4):(var3.length == 3 && var3[0].equals("entity") || var3.length == 5 && var3[0].equals("block")?a(var3, new String[]{"set", "clear"}):((var3.length != 4 || !var3[0].equals("entity")) && (var3.length != 6 || !var3[0].equals("block"))?((var3.length != 6 || !var3[0].equals("entity")) && (var3.length != 8 || !var3[0].equals("block"))?Collections.emptyList():a(var3, this.a(var1))):a(var3, CommandObjectiveExecutor.EnumCommandResult.c())))));
   }

   protected List a(MinecraftServer var1) {
      Collection var2 = var1.a(0).ad().c();
      ArrayList var3 = Lists.newArrayList();
      Iterator var4 = var2.iterator();

      while(var4.hasNext()) {
         class_bbk var5 = (class_bbk)var4.next();
         if(!var5.c().b()) {
            var3.add(var5.b());
         }
      }

      return var3;
   }

   public boolean b(String[] var1, int var2) {
      return var1.length > 0 && var1[0].equals("entity") && var2 == 1;
   }
}
