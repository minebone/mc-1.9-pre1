package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.World;
import net.minecraft.server.class_azs;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.class_ys;
import net.minecraft.server.class_yw;

public class class_yo extends class_yw {
   public class_yo(World var1) {
      super(var1);
      this.fireProof = true;
   }

   protected void bz() {
      super.bz();
      this.a(class_ys.d).a(0.20000000298023224D);
   }

   public boolean cF() {
      return this.world.ae() != EnumDifficulty.PEACEFUL;
   }

   public boolean cG() {
      return this.world.a((AxisAlignedBB)this.bk(), (Entity)this) && this.world.a((Entity)this, (AxisAlignedBB)this.bk()).isEmpty() && !this.world.e(this.bk());
   }

   protected void a(int var1) {
      super.a(var1);
      this.a(class_ys.g).a((double)(var1 * 3));
   }

   public float e(float var1) {
      return 1.0F;
   }

   protected EnumParticle o() {
      return EnumParticle.FLAME;
   }

   protected class_yw cT() {
      return new class_yo(this.world);
   }

   protected class_kk J() {
      return this.db() > 1?class_azs.ad:class_azs.a;
   }

   public boolean aG() {
      return false;
   }

   protected int cU() {
      return super.cU() * 4;
   }

   protected void cV() {
      this.a *= 0.9F;
   }

   protected void cg() {
      this.motY = (double)(0.42F + (float)this.db() * 0.1F);
      this.ai = true;
   }

   protected void ci() {
      this.motY = (double)(0.22F + (float)this.db() * 0.05F);
      this.ai = true;
   }

   public void e(float var1, float var2) {
   }

   protected boolean cW() {
      return true;
   }

   protected int cX() {
      return super.cX() + 2;
   }

   protected class_nf bQ() {
      return this.db() > 1?class_ng.dh:class_ng.fv;
   }

   protected class_nf bR() {
      return this.db() > 1?class_ng.dg:class_ng.fu;
   }

   protected class_nf cY() {
      return this.db() > 1?class_ng.dj:class_ng.fw;
   }

   protected class_nf cZ() {
      return class_ng.di;
   }

   protected boolean da() {
      return true;
   }
}
