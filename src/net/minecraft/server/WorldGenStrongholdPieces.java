package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.minecraft.server.BlockDoor;
import net.minecraft.server.BlockDoubleStepAbstract;
import net.minecraft.server.BlockMonsterEggs;
import net.minecraft.server.BlockSmoothBrick;
import net.minecraft.server.BlockStairs;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.StructureBoundingBox;
import net.minecraft.server.StructurePiece;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.WorldGenFactory;
import net.minecraft.server.class_ajy;
import net.minecraft.server.class_alc;
import net.minecraft.server.class_amj;
import net.minecraft.server.class_ape;
import net.minecraft.server.class_aqj;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.BaseBlockPosition;

public class WorldGenStrongholdPieces {
   private static final WorldGenStrongholdPieces.class_f_in_class_awb[] b = new WorldGenStrongholdPieces.class_f_in_class_awb[]{new WorldGenStrongholdPieces.class_f_in_class_awb(WorldGenStrongholdPieces.WorldGenStrongholdStairs.class, 40, 0), new WorldGenStrongholdPieces.class_f_in_class_awb(WorldGenStrongholdPieces.class_h_in_class_awb.class, 5, 5), new WorldGenStrongholdPieces.class_f_in_class_awb(WorldGenStrongholdPieces.class_d_in_class_awb.class, 20, 0), new WorldGenStrongholdPieces.class_f_in_class_awb(WorldGenStrongholdPieces.class_i_in_class_awb.class, 20, 0), new WorldGenStrongholdPieces.class_f_in_class_awb(WorldGenStrongholdPieces.class_j_in_class_awb.class, 10, 6), new WorldGenStrongholdPieces.class_f_in_class_awb(WorldGenStrongholdPieces.class_o_in_class_awb.class, 5, 5), new WorldGenStrongholdPieces.class_f_in_class_awb(WorldGenStrongholdPieces.WorldGenStrongholdStairs2.class, 5, 5), new WorldGenStrongholdPieces.class_f_in_class_awb(WorldGenStrongholdPieces.WorldGenStrongholdCrossing.class, 5, 4), new WorldGenStrongholdPieces.class_f_in_class_awb(WorldGenStrongholdPieces.class_a_in_class_awb.class, 5, 4), new WorldGenStrongholdPieces.class_f_in_class_awb(WorldGenStrongholdPieces.WorldGenStrongholdLibrary.class, 10, 2) {
      public boolean a(int var1) {
         return super.a(var1) && var1 > 4;
      }
   }, new WorldGenStrongholdPieces.class_f_in_class_awb(WorldGenStrongholdPieces.class_g_in_class_awb.class, 20, 1) {
      public boolean a(int var1) {
         return super.a(var1) && var1 > 5;
      }
   }};
   private static List c;
   private static Class d;
   static int a;
   private static final WorldGenStrongholdPieces.class_k_in_class_awb e = new WorldGenStrongholdPieces.class_k_in_class_awb(null);

   public static void a() {
      WorldGenFactory.a(WorldGenStrongholdPieces.class_a_in_class_awb.class, "SHCC");
      WorldGenFactory.a(WorldGenStrongholdPieces.class_b_in_class_awb.class, "SHFC");
      WorldGenFactory.a(WorldGenStrongholdPieces.WorldGenStrongholdCrossing.class, "SH5C");
      WorldGenFactory.a(WorldGenStrongholdPieces.class_d_in_class_awb.class, "SHLT");
      WorldGenFactory.a(WorldGenStrongholdPieces.WorldGenStrongholdLibrary.class, "SHLi");
      WorldGenFactory.a(WorldGenStrongholdPieces.class_g_in_class_awb.class, "SHPR");
      WorldGenFactory.a(WorldGenStrongholdPieces.class_h_in_class_awb.class, "SHPH");
      WorldGenFactory.a(WorldGenStrongholdPieces.class_i_in_class_awb.class, "SHRT");
      WorldGenFactory.a(WorldGenStrongholdPieces.class_j_in_class_awb.class, "SHRC");
      WorldGenFactory.a(WorldGenStrongholdPieces.WorldGenStrongholdStairs2.class, "SHSD");
      WorldGenFactory.a(WorldGenStrongholdPieces.class_m_in_class_awb.class, "SHStart");
      WorldGenFactory.a(WorldGenStrongholdPieces.WorldGenStrongholdStairs.class, "SHS");
      WorldGenFactory.a(WorldGenStrongholdPieces.class_o_in_class_awb.class, "SHSSD");
   }

   public static void b() {
      c = Lists.newArrayList();
      WorldGenStrongholdPieces.class_f_in_class_awb[] var0 = b;
      int var1 = var0.length;

      for(int var2 = 0; var2 < var1; ++var2) {
         WorldGenStrongholdPieces.class_f_in_class_awb var3 = var0[var2];
         var3.c = 0;
         c.add(var3);
      }

      d = null;
   }

   private static boolean d() {
      boolean var0 = false;
      a = 0;

      WorldGenStrongholdPieces.class_f_in_class_awb var2;
      for(Iterator var1 = c.iterator(); var1.hasNext(); a += var2.b) {
         var2 = (WorldGenStrongholdPieces.class_f_in_class_awb)var1.next();
         if(var2.d > 0 && var2.c < var2.d) {
            var0 = true;
         }
      }

      return var0;
   }

   private static WorldGenStrongholdPieces.WorldGenStrongholdPiece a(Class var0, List var1, Random var2, int var3, int var4, int var5, EnumDirection var6, int var7) {
      Object var8 = null;
      if(var0 == WorldGenStrongholdPieces.WorldGenStrongholdStairs.class) {
         var8 = WorldGenStrongholdPieces.WorldGenStrongholdStairs.a(var1, var2, var3, var4, var5, var6, var7);
      } else if(var0 == WorldGenStrongholdPieces.class_h_in_class_awb.class) {
         var8 = WorldGenStrongholdPieces.class_h_in_class_awb.a(var1, var2, var3, var4, var5, var6, var7);
      } else if(var0 == WorldGenStrongholdPieces.class_d_in_class_awb.class) {
         var8 = WorldGenStrongholdPieces.class_d_in_class_awb.a(var1, var2, var3, var4, var5, var6, var7);
      } else if(var0 == WorldGenStrongholdPieces.class_i_in_class_awb.class) {
         var8 = WorldGenStrongholdPieces.class_i_in_class_awb.a(var1, var2, var3, var4, var5, var6, var7);
      } else if(var0 == WorldGenStrongholdPieces.class_j_in_class_awb.class) {
         var8 = WorldGenStrongholdPieces.class_j_in_class_awb.a(var1, var2, var3, var4, var5, var6, var7);
      } else if(var0 == WorldGenStrongholdPieces.class_o_in_class_awb.class) {
         var8 = WorldGenStrongholdPieces.class_o_in_class_awb.a(var1, var2, var3, var4, var5, var6, var7);
      } else if(var0 == WorldGenStrongholdPieces.WorldGenStrongholdStairs2.class) {
         var8 = WorldGenStrongholdPieces.WorldGenStrongholdStairs2.a(var1, var2, var3, var4, var5, var6, var7);
      } else if(var0 == WorldGenStrongholdPieces.WorldGenStrongholdCrossing.class) {
         var8 = WorldGenStrongholdPieces.WorldGenStrongholdCrossing.a(var1, var2, var3, var4, var5, var6, var7);
      } else if(var0 == WorldGenStrongholdPieces.class_a_in_class_awb.class) {
         var8 = WorldGenStrongholdPieces.class_a_in_class_awb.a(var1, var2, var3, var4, var5, var6, var7);
      } else if(var0 == WorldGenStrongholdPieces.WorldGenStrongholdLibrary.class) {
         var8 = WorldGenStrongholdPieces.WorldGenStrongholdLibrary.a(var1, var2, var3, var4, var5, var6, var7);
      } else if(var0 == WorldGenStrongholdPieces.class_g_in_class_awb.class) {
         var8 = WorldGenStrongholdPieces.class_g_in_class_awb.a(var1, var2, var3, var4, var5, var6, var7);
      }

      return (WorldGenStrongholdPieces.WorldGenStrongholdPiece)var8;
   }

   private static WorldGenStrongholdPieces.WorldGenStrongholdPiece b(WorldGenStrongholdPieces.class_m_in_class_awb var0, List var1, Random var2, int var3, int var4, int var5, EnumDirection var6, int var7) {
      if(!d()) {
         return null;
      } else {
         if(d != null) {
            WorldGenStrongholdPieces.WorldGenStrongholdPiece var8 = a(d, var1, var2, var3, var4, var5, var6, var7);
            d = null;
            if(var8 != null) {
               return var8;
            }
         }

         int var13 = 0;

         while(var13 < 5) {
            ++var13;
            int var9 = var2.nextInt(a);
            Iterator var10 = c.iterator();

            while(var10.hasNext()) {
               WorldGenStrongholdPieces.class_f_in_class_awb var11 = (WorldGenStrongholdPieces.class_f_in_class_awb)var10.next();
               var9 -= var11.b;
               if(var9 < 0) {
                  if(!var11.a(var7) || var11 == var0.a) {
                     break;
                  }

                  WorldGenStrongholdPieces.WorldGenStrongholdPiece var12 = a(var11.a, var1, var2, var3, var4, var5, var6, var7);
                  if(var12 != null) {
                     ++var11.c;
                     var0.a = var11;
                     if(!var11.a()) {
                        c.remove(var11);
                     }

                     return var12;
                  }
               }
            }
         }

         StructureBoundingBox var14 = WorldGenStrongholdPieces.class_b_in_class_awb.a(var1, var2, var3, var4, var5, var6);
         if(var14 != null && var14.b > 1) {
            return new WorldGenStrongholdPieces.class_b_in_class_awb(var7, var2, var14, var6);
         } else {
            return null;
         }
      }
   }

   private static StructurePiece c(WorldGenStrongholdPieces.class_m_in_class_awb var0, List var1, Random var2, int var3, int var4, int var5, EnumDirection var6, int var7) {
      if(var7 > 50) {
         return null;
      } else if(Math.abs(var3 - var0.c().a) <= 112 && Math.abs(var5 - var0.c().c) <= 112) {
         WorldGenStrongholdPieces.WorldGenStrongholdPiece var8 = b(var0, var1, var2, var3, var4, var5, var6, var7 + 1);
         if(var8 != null) {
            var1.add(var8);
            var0.c.add(var8);
         }

         return var8;
      } else {
         return null;
      }
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b = new int[EnumDirection.values().length];

      static {
         try {
            b[EnumDirection.NORTH.ordinal()] = 1;
         } catch (NoSuchFieldError var8) {
            ;
         }

         try {
            b[EnumDirection.SOUTH.ordinal()] = 2;
         } catch (NoSuchFieldError var7) {
            ;
         }

         try {
            b[EnumDirection.WEST.ordinal()] = 3;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            b[EnumDirection.EAST.ordinal()] = 4;
         } catch (NoSuchFieldError var5) {
            ;
         }

         a = new int[WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType.values().length];

         try {
            a[WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType.OPENING.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType.WOOD_DOOR.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType.GRATES.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType.IRON_DOOR.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   static class class_k_in_class_awb extends StructurePiece.class_a_in_class_awf {
      private class_k_in_class_awb() {
      }

      public void a(Random var1, int var2, int var3, int var4, boolean var5) {
         if(var5) {
            float var6 = var1.nextFloat();
            if(var6 < 0.2F) {
               this.a = Blocks.bf.a(BlockSmoothBrick.d);
            } else if(var6 < 0.5F) {
               this.a = Blocks.bf.a(BlockSmoothBrick.c);
            } else if(var6 < 0.55F) {
               this.a = Blocks.be.a(BlockMonsterEggs.EnumMonsterEggVarient.c.a());
            } else {
               this.a = Blocks.bf.u();
            }
         } else {
            this.a = Blocks.AIR.u();
         }

      }

      // $FF: synthetic method
      class_k_in_class_awb(Object var1) {
         this();
      }
   }

   public static class class_g_in_class_awb extends WorldGenStrongholdPieces.WorldGenStrongholdPiece {
      private boolean a;

      public class_g_in_class_awb() {
      }

      public class_g_in_class_awb(int var1, Random var2, StructureBoundingBox var3, EnumDirection var4) {
         super(var1);
         this.a(var4);
         this.l = var3;
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("Mob", this.a);
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.a = var1.p("Mob");
      }

      public void a(StructurePiece var1, List var2, Random var3) {
         if(var1 != null) {
            ((WorldGenStrongholdPieces.class_m_in_class_awb)var1).b = this;
         }

      }

      public static WorldGenStrongholdPieces.class_g_in_class_awb a(List var0, Random var1, int var2, int var3, int var4, EnumDirection var5, int var6) {
         StructureBoundingBox var7 = StructureBoundingBox.a(var2, var3, var4, -4, -1, 0, 11, 8, 16, var5);
         return a(var7) && StructurePiece.a(var0, var7) == null?new WorldGenStrongholdPieces.class_g_in_class_awb(var6, var1, var7, var5):null;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         this.a(var1, var3, 0, 0, 0, 10, 7, 15, false, var2, WorldGenStrongholdPieces.e);
         this.a(var1, var2, var3, WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType.GRATES, 4, 1, 0);
         byte var4 = 6;
         this.a(var1, var3, 1, var4, 1, 1, var4, 14, false, var2, WorldGenStrongholdPieces.e);
         this.a(var1, var3, 9, var4, 1, 9, var4, 14, false, var2, WorldGenStrongholdPieces.e);
         this.a(var1, var3, 2, var4, 1, 8, var4, 2, false, var2, WorldGenStrongholdPieces.e);
         this.a(var1, var3, 2, var4, 14, 8, var4, 14, false, var2, WorldGenStrongholdPieces.e);
         this.a(var1, var3, 1, 1, 1, 2, 1, 4, false, var2, WorldGenStrongholdPieces.e);
         this.a(var1, var3, 8, 1, 1, 9, 1, 4, false, var2, WorldGenStrongholdPieces.e);
         this.a(var1, var3, 1, 1, 1, 1, 1, 3, Blocks.k.u(), Blocks.k.u(), false);
         this.a(var1, var3, 9, 1, 1, 9, 1, 3, Blocks.k.u(), Blocks.k.u(), false);
         this.a(var1, var3, 3, 1, 8, 7, 1, 12, false, var2, WorldGenStrongholdPieces.e);
         this.a(var1, var3, 4, 1, 9, 6, 1, 11, Blocks.k.u(), Blocks.k.u(), false);

         int var5;
         for(var5 = 3; var5 < 14; var5 += 2) {
            this.a(var1, var3, 0, 3, var5, 0, 4, var5, Blocks.bi.u(), Blocks.bi.u(), false);
            this.a(var1, var3, 10, 3, var5, 10, 4, var5, Blocks.bi.u(), Blocks.bi.u(), false);
         }

         for(var5 = 2; var5 < 9; var5 += 2) {
            this.a(var1, var3, var5, 3, 15, var5, 4, 15, Blocks.bi.u(), Blocks.bi.u(), false);
         }

         IBlockData var15 = Blocks.bv.u().set(BlockStairs.a, EnumDirection.NORTH);
         this.a(var1, var3, 4, 1, 5, 6, 1, 7, false, var2, WorldGenStrongholdPieces.e);
         this.a(var1, var3, 4, 2, 6, 6, 2, 7, false, var2, WorldGenStrongholdPieces.e);
         this.a(var1, var3, 4, 3, 7, 6, 3, 7, false, var2, WorldGenStrongholdPieces.e);

         for(int var6 = 4; var6 <= 6; ++var6) {
            this.a(var1, var15, var6, 1, 4, var3);
            this.a(var1, var15, var6, 2, 5, var3);
            this.a(var1, var15, var6, 3, 6, var3);
         }

         IBlockData var16 = Blocks.bG.u().set(class_alc.a, EnumDirection.NORTH);
         IBlockData var7 = Blocks.bG.u().set(class_alc.a, EnumDirection.SOUTH);
         IBlockData var8 = Blocks.bG.u().set(class_alc.a, EnumDirection.EAST);
         IBlockData var9 = Blocks.bG.u().set(class_alc.a, EnumDirection.WEST);
         boolean var10 = true;
         boolean[] var11 = new boolean[12];

         for(int var12 = 0; var12 < var11.length; ++var12) {
            var11[var12] = var2.nextFloat() > 0.9F;
            var10 &= var11[var12];
         }

         this.a(var1, var16.set(class_alc.b, Boolean.valueOf(var11[0])), 4, 3, 8, var3);
         this.a(var1, var16.set(class_alc.b, Boolean.valueOf(var11[1])), 5, 3, 8, var3);
         this.a(var1, var16.set(class_alc.b, Boolean.valueOf(var11[2])), 6, 3, 8, var3);
         this.a(var1, var7.set(class_alc.b, Boolean.valueOf(var11[3])), 4, 3, 12, var3);
         this.a(var1, var7.set(class_alc.b, Boolean.valueOf(var11[4])), 5, 3, 12, var3);
         this.a(var1, var7.set(class_alc.b, Boolean.valueOf(var11[5])), 6, 3, 12, var3);
         this.a(var1, var8.set(class_alc.b, Boolean.valueOf(var11[6])), 3, 3, 9, var3);
         this.a(var1, var8.set(class_alc.b, Boolean.valueOf(var11[7])), 3, 3, 10, var3);
         this.a(var1, var8.set(class_alc.b, Boolean.valueOf(var11[8])), 3, 3, 11, var3);
         this.a(var1, var9.set(class_alc.b, Boolean.valueOf(var11[9])), 7, 3, 9, var3);
         this.a(var1, var9.set(class_alc.b, Boolean.valueOf(var11[10])), 7, 3, 10, var3);
         this.a(var1, var9.set(class_alc.b, Boolean.valueOf(var11[11])), 7, 3, 11, var3);
         if(var10) {
            IBlockData var17 = Blocks.bF.u();
            this.a(var1, var17, 4, 3, 9, var3);
            this.a(var1, var17, 5, 3, 9, var3);
            this.a(var1, var17, 6, 3, 9, var3);
            this.a(var1, var17, 4, 3, 10, var3);
            this.a(var1, var17, 5, 3, 10, var3);
            this.a(var1, var17, 6, 3, 10, var3);
            this.a(var1, var17, 4, 3, 11, var3);
            this.a(var1, var17, 5, 3, 11, var3);
            this.a(var1, var17, 6, 3, 11, var3);
         }

         if(!this.a) {
            int var14 = this.d(3);
            BlockPosition var18 = new BlockPosition(this.a(5, 6), var14, this.b(5, 6));
            if(var3.b((BaseBlockPosition)var18)) {
               this.a = true;
               var1.a((BlockPosition)var18, (IBlockData)Blocks.ac.u(), 2);
               TileEntity var13 = var1.r(var18);
               if(var13 instanceof class_aqj) {
                  ((class_aqj)var13).b().a("Silverfish");
               }
            }
         }

         return true;
      }
   }

   public static class WorldGenStrongholdCrossing extends WorldGenStrongholdPieces.WorldGenStrongholdPiece {
      private boolean a;
      private boolean b;
      private boolean c;
      private boolean e;

      public WorldGenStrongholdCrossing() {
      }

      public WorldGenStrongholdCrossing(int var1, Random var2, StructureBoundingBox var3, EnumDirection var4) {
         super(var1);
         this.a(var4);
         this.d = this.a(var2);
         this.l = var3;
         this.a = var2.nextBoolean();
         this.b = var2.nextBoolean();
         this.c = var2.nextBoolean();
         this.e = var2.nextInt(3) > 0;
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("leftLow", this.a);
         var1.a("leftHigh", this.b);
         var1.a("rightLow", this.c);
         var1.a("rightHigh", this.e);
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.a = var1.p("leftLow");
         this.b = var1.p("leftHigh");
         this.c = var1.p("rightLow");
         this.e = var1.p("rightHigh");
      }

      public void a(StructurePiece var1, List var2, Random var3) {
         int var4 = 3;
         int var5 = 5;
         EnumDirection var6 = this.e();
         if(var6 == EnumDirection.WEST || var6 == EnumDirection.NORTH) {
            var4 = 8 - var4;
            var5 = 8 - var5;
         }

         this.a((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, 5, 1);
         if(this.a) {
            this.b((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, var4, 1);
         }

         if(this.b) {
            this.b((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, var5, 7);
         }

         if(this.c) {
            this.c((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, var4, 1);
         }

         if(this.e) {
            this.c((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, var5, 7);
         }

      }

      public static WorldGenStrongholdPieces.WorldGenStrongholdCrossing a(List var0, Random var1, int var2, int var3, int var4, EnumDirection var5, int var6) {
         StructureBoundingBox var7 = StructureBoundingBox.a(var2, var3, var4, -4, -3, 0, 10, 9, 11, var5);
         return a(var7) && StructurePiece.a(var0, var7) == null?new WorldGenStrongholdPieces.WorldGenStrongholdCrossing(var6, var1, var7, var5):null;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.a(var1, var3)) {
            return false;
         } else {
            this.a(var1, var3, 0, 0, 0, 9, 8, 10, true, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var2, var3, this.d, 4, 3, 0);
            if(this.a) {
               this.a(var1, var3, 0, 3, 1, 0, 5, 3, Blocks.AIR.u(), Blocks.AIR.u(), false);
            }

            if(this.c) {
               this.a(var1, var3, 9, 3, 1, 9, 5, 3, Blocks.AIR.u(), Blocks.AIR.u(), false);
            }

            if(this.b) {
               this.a(var1, var3, 0, 5, 7, 0, 7, 9, Blocks.AIR.u(), Blocks.AIR.u(), false);
            }

            if(this.e) {
               this.a(var1, var3, 9, 5, 7, 9, 7, 9, Blocks.AIR.u(), Blocks.AIR.u(), false);
            }

            this.a(var1, var3, 5, 1, 10, 7, 3, 10, Blocks.AIR.u(), Blocks.AIR.u(), false);
            this.a(var1, var3, 1, 2, 1, 8, 2, 6, false, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var3, 4, 1, 5, 4, 4, 9, false, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var3, 8, 1, 5, 8, 4, 9, false, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var3, 1, 4, 7, 3, 4, 9, false, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var3, 1, 3, 5, 3, 3, 6, false, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var3, 1, 3, 4, 3, 3, 4, Blocks.U.u(), Blocks.U.u(), false);
            this.a(var1, var3, 1, 4, 6, 3, 4, 6, Blocks.U.u(), Blocks.U.u(), false);
            this.a(var1, var3, 5, 1, 7, 7, 1, 8, false, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var3, 5, 1, 9, 7, 1, 9, Blocks.U.u(), Blocks.U.u(), false);
            this.a(var1, var3, 5, 2, 7, 7, 2, 7, Blocks.U.u(), Blocks.U.u(), false);
            this.a(var1, var3, 4, 5, 7, 4, 5, 9, Blocks.U.u(), Blocks.U.u(), false);
            this.a(var1, var3, 8, 5, 7, 8, 5, 9, Blocks.U.u(), Blocks.U.u(), false);
            this.a(var1, var3, 5, 5, 7, 7, 5, 9, Blocks.T.u(), Blocks.T.u(), false);
            this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.SOUTH), 6, 5, 6, var3);
            return true;
         }
      }
   }

   public static class WorldGenStrongholdLibrary extends WorldGenStrongholdPieces.WorldGenStrongholdPiece {
      private boolean a;

      public WorldGenStrongholdLibrary() {
      }

      public WorldGenStrongholdLibrary(int var1, Random var2, StructureBoundingBox var3, EnumDirection var4) {
         super(var1);
         this.a(var4);
         this.d = this.a(var2);
         this.l = var3;
         this.a = var3.d() > 6;
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("Tall", this.a);
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.a = var1.p("Tall");
      }

      public static WorldGenStrongholdPieces.WorldGenStrongholdLibrary a(List var0, Random var1, int var2, int var3, int var4, EnumDirection var5, int var6) {
         StructureBoundingBox var7 = StructureBoundingBox.a(var2, var3, var4, -4, -1, 0, 14, 11, 15, var5);
         if(!a(var7) || StructurePiece.a(var0, var7) != null) {
            var7 = StructureBoundingBox.a(var2, var3, var4, -4, -1, 0, 14, 6, 15, var5);
            if(!a(var7) || StructurePiece.a(var0, var7) != null) {
               return null;
            }
         }

         return new WorldGenStrongholdPieces.WorldGenStrongholdLibrary(var6, var1, var7, var5);
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.a(var1, var3)) {
            return false;
         } else {
            byte var4 = 11;
            if(!this.a) {
               var4 = 6;
            }

            this.a(var1, var3, 0, 0, 0, 13, var4 - 1, 14, true, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var2, var3, this.d, 4, 1, 0);
            this.a(var1, var3, var2, 0.07F, 2, 1, 1, 11, 4, 13, Blocks.G.u(), Blocks.G.u(), false);
            boolean var5 = true;
            boolean var6 = true;

            int var7;
            for(var7 = 1; var7 <= 13; ++var7) {
               if((var7 - 1) % 4 == 0) {
                  this.a(var1, var3, 1, 1, var7, 1, 4, var7, Blocks.f.u(), Blocks.f.u(), false);
                  this.a(var1, var3, 12, 1, var7, 12, 4, var7, Blocks.f.u(), Blocks.f.u(), false);
                  this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.EAST), 2, 3, var7, var3);
                  this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.WEST), 11, 3, var7, var3);
                  if(this.a) {
                     this.a(var1, var3, 1, 6, var7, 1, 9, var7, Blocks.f.u(), Blocks.f.u(), false);
                     this.a(var1, var3, 12, 6, var7, 12, 9, var7, Blocks.f.u(), Blocks.f.u(), false);
                  }
               } else {
                  this.a(var1, var3, 1, 1, var7, 1, 4, var7, Blocks.X.u(), Blocks.X.u(), false);
                  this.a(var1, var3, 12, 1, var7, 12, 4, var7, Blocks.X.u(), Blocks.X.u(), false);
                  if(this.a) {
                     this.a(var1, var3, 1, 6, var7, 1, 9, var7, Blocks.X.u(), Blocks.X.u(), false);
                     this.a(var1, var3, 12, 6, var7, 12, 9, var7, Blocks.X.u(), Blocks.X.u(), false);
                  }
               }
            }

            for(var7 = 3; var7 < 12; var7 += 2) {
               this.a(var1, var3, 3, 1, var7, 4, 3, var7, Blocks.X.u(), Blocks.X.u(), false);
               this.a(var1, var3, 6, 1, var7, 7, 3, var7, Blocks.X.u(), Blocks.X.u(), false);
               this.a(var1, var3, 9, 1, var7, 10, 3, var7, Blocks.X.u(), Blocks.X.u(), false);
            }

            if(this.a) {
               this.a(var1, var3, 1, 5, 1, 3, 5, 13, Blocks.f.u(), Blocks.f.u(), false);
               this.a(var1, var3, 10, 5, 1, 12, 5, 13, Blocks.f.u(), Blocks.f.u(), false);
               this.a(var1, var3, 4, 5, 1, 9, 5, 2, Blocks.f.u(), Blocks.f.u(), false);
               this.a(var1, var3, 4, 5, 12, 9, 5, 13, Blocks.f.u(), Blocks.f.u(), false);
               this.a(var1, Blocks.f.u(), 9, 5, 11, var3);
               this.a(var1, Blocks.f.u(), 8, 5, 11, var3);
               this.a(var1, Blocks.f.u(), 9, 5, 10, var3);
               this.a(var1, var3, 3, 6, 2, 3, 6, 12, Blocks.aO.u(), Blocks.aO.u(), false);
               this.a(var1, var3, 10, 6, 2, 10, 6, 10, Blocks.aO.u(), Blocks.aO.u(), false);
               this.a(var1, var3, 4, 6, 2, 9, 6, 2, Blocks.aO.u(), Blocks.aO.u(), false);
               this.a(var1, var3, 4, 6, 12, 8, 6, 12, Blocks.aO.u(), Blocks.aO.u(), false);
               this.a(var1, Blocks.aO.u(), 9, 6, 11, var3);
               this.a(var1, Blocks.aO.u(), 8, 6, 11, var3);
               this.a(var1, Blocks.aO.u(), 9, 6, 10, var3);
               IBlockData var11 = Blocks.au.u().set(class_amj.a, EnumDirection.SOUTH);
               this.a(var1, var11, 10, 1, 13, var3);
               this.a(var1, var11, 10, 2, 13, var3);
               this.a(var1, var11, 10, 3, 13, var3);
               this.a(var1, var11, 10, 4, 13, var3);
               this.a(var1, var11, 10, 5, 13, var3);
               this.a(var1, var11, 10, 6, 13, var3);
               this.a(var1, var11, 10, 7, 13, var3);
               byte var8 = 7;
               byte var9 = 7;
               this.a(var1, Blocks.aO.u(), var8 - 1, 9, var9, var3);
               this.a(var1, Blocks.aO.u(), var8, 9, var9, var3);
               this.a(var1, Blocks.aO.u(), var8 - 1, 8, var9, var3);
               this.a(var1, Blocks.aO.u(), var8, 8, var9, var3);
               this.a(var1, Blocks.aO.u(), var8 - 1, 7, var9, var3);
               this.a(var1, Blocks.aO.u(), var8, 7, var9, var3);
               this.a(var1, Blocks.aO.u(), var8 - 2, 7, var9, var3);
               this.a(var1, Blocks.aO.u(), var8 + 1, 7, var9, var3);
               this.a(var1, Blocks.aO.u(), var8 - 1, 7, var9 - 1, var3);
               this.a(var1, Blocks.aO.u(), var8 - 1, 7, var9 + 1, var3);
               this.a(var1, Blocks.aO.u(), var8, 7, var9 - 1, var3);
               this.a(var1, Blocks.aO.u(), var8, 7, var9 + 1, var3);
               IBlockData var10 = Blocks.aa.u().set(class_ape.a, EnumDirection.UP);
               this.a(var1, var10, var8 - 2, 8, var9, var3);
               this.a(var1, var10, var8 + 1, 8, var9, var3);
               this.a(var1, var10, var8 - 1, 8, var9 - 1, var3);
               this.a(var1, var10, var8 - 1, 8, var9 + 1, var3);
               this.a(var1, var10, var8, 8, var9 - 1, var3);
               this.a(var1, var10, var8, 8, var9 + 1, var3);
            }

            this.a(var1, var3, var2, 3, 3, 5, class_azs.h);
            if(this.a) {
               this.a(var1, Blocks.AIR.u(), 12, 9, 1, var3);
               this.a(var1, var3, var2, 12, 8, 1, class_azs.h);
            }

            return true;
         }
      }
   }

   public static class class_h_in_class_awb extends WorldGenStrongholdPieces.WorldGenStrongholdPiece {
      public class_h_in_class_awb() {
      }

      public class_h_in_class_awb(int var1, Random var2, StructureBoundingBox var3, EnumDirection var4) {
         super(var1);
         this.a(var4);
         this.d = this.a(var2);
         this.l = var3;
      }

      public void a(StructurePiece var1, List var2, Random var3) {
         this.a((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, 1, 1);
      }

      public static WorldGenStrongholdPieces.class_h_in_class_awb a(List var0, Random var1, int var2, int var3, int var4, EnumDirection var5, int var6) {
         StructureBoundingBox var7 = StructureBoundingBox.a(var2, var3, var4, -1, -1, 0, 9, 5, 11, var5);
         return a(var7) && StructurePiece.a(var0, var7) == null?new WorldGenStrongholdPieces.class_h_in_class_awb(var6, var1, var7, var5):null;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.a(var1, var3)) {
            return false;
         } else {
            this.a(var1, var3, 0, 0, 0, 8, 4, 10, true, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var2, var3, this.d, 1, 1, 0);
            this.a(var1, var3, 1, 1, 10, 3, 3, 10, Blocks.AIR.u(), Blocks.AIR.u(), false);
            this.a(var1, var3, 4, 1, 1, 4, 3, 1, false, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var3, 4, 1, 3, 4, 3, 3, false, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var3, 4, 1, 7, 4, 3, 7, false, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var3, 4, 1, 9, 4, 3, 9, false, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var3, 4, 1, 4, 4, 3, 6, Blocks.bi.u(), Blocks.bi.u(), false);
            this.a(var1, var3, 5, 1, 5, 7, 3, 5, Blocks.bi.u(), Blocks.bi.u(), false);
            this.a(var1, Blocks.bi.u(), 4, 3, 2, var3);
            this.a(var1, Blocks.bi.u(), 4, 3, 8, var3);
            IBlockData var4 = Blocks.aA.u().set(BlockDoor.a, EnumDirection.WEST);
            IBlockData var5 = Blocks.aA.u().set(BlockDoor.a, EnumDirection.WEST).set(BlockDoor.e, BlockDoor.class_a_in_class_aku.UPPER);
            this.a(var1, var4, 4, 1, 2, var3);
            this.a(var1, var5, 4, 2, 2, var3);
            this.a(var1, var4, 4, 1, 8, var3);
            this.a(var1, var5, 4, 2, 8, var3);
            return true;
         }
      }
   }

   public static class class_j_in_class_awb extends WorldGenStrongholdPieces.WorldGenStrongholdPiece {
      protected int a;

      public class_j_in_class_awb() {
      }

      public class_j_in_class_awb(int var1, Random var2, StructureBoundingBox var3, EnumDirection var4) {
         super(var1);
         this.a(var4);
         this.d = this.a(var2);
         this.l = var3;
         this.a = var2.nextInt(5);
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("Type", this.a);
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.a = var1.h("Type");
      }

      public void a(StructurePiece var1, List var2, Random var3) {
         this.a((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, 4, 1);
         this.b((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, 1, 4);
         this.c((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, 1, 4);
      }

      public static WorldGenStrongholdPieces.class_j_in_class_awb a(List var0, Random var1, int var2, int var3, int var4, EnumDirection var5, int var6) {
         StructureBoundingBox var7 = StructureBoundingBox.a(var2, var3, var4, -4, -1, 0, 11, 7, 11, var5);
         return a(var7) && StructurePiece.a(var0, var7) == null?new WorldGenStrongholdPieces.class_j_in_class_awb(var6, var1, var7, var5):null;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.a(var1, var3)) {
            return false;
         } else {
            this.a(var1, var3, 0, 0, 0, 10, 6, 10, true, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var2, var3, this.d, 4, 1, 0);
            this.a(var1, var3, 4, 1, 10, 6, 3, 10, Blocks.AIR.u(), Blocks.AIR.u(), false);
            this.a(var1, var3, 0, 1, 4, 0, 3, 6, Blocks.AIR.u(), Blocks.AIR.u(), false);
            this.a(var1, var3, 10, 1, 4, 10, 3, 6, Blocks.AIR.u(), Blocks.AIR.u(), false);
            int var4;
            switch(this.a) {
            case 0:
               this.a(var1, Blocks.bf.u(), 5, 1, 5, var3);
               this.a(var1, Blocks.bf.u(), 5, 2, 5, var3);
               this.a(var1, Blocks.bf.u(), 5, 3, 5, var3);
               this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.WEST), 4, 3, 5, var3);
               this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.EAST), 6, 3, 5, var3);
               this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.SOUTH), 5, 3, 4, var3);
               this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.NORTH), 5, 3, 6, var3);
               this.a(var1, Blocks.U.u(), 4, 1, 4, var3);
               this.a(var1, Blocks.U.u(), 4, 1, 5, var3);
               this.a(var1, Blocks.U.u(), 4, 1, 6, var3);
               this.a(var1, Blocks.U.u(), 6, 1, 4, var3);
               this.a(var1, Blocks.U.u(), 6, 1, 5, var3);
               this.a(var1, Blocks.U.u(), 6, 1, 6, var3);
               this.a(var1, Blocks.U.u(), 5, 1, 4, var3);
               this.a(var1, Blocks.U.u(), 5, 1, 6, var3);
               break;
            case 1:
               for(var4 = 0; var4 < 5; ++var4) {
                  this.a(var1, Blocks.bf.u(), 3, 1, 3 + var4, var3);
                  this.a(var1, Blocks.bf.u(), 7, 1, 3 + var4, var3);
                  this.a(var1, Blocks.bf.u(), 3 + var4, 1, 3, var3);
                  this.a(var1, Blocks.bf.u(), 3 + var4, 1, 7, var3);
               }

               this.a(var1, Blocks.bf.u(), 5, 1, 5, var3);
               this.a(var1, Blocks.bf.u(), 5, 2, 5, var3);
               this.a(var1, Blocks.bf.u(), 5, 3, 5, var3);
               this.a(var1, Blocks.i.u(), 5, 4, 5, var3);
               break;
            case 2:
               for(var4 = 1; var4 <= 9; ++var4) {
                  this.a(var1, Blocks.e.u(), 1, 3, var4, var3);
                  this.a(var1, Blocks.e.u(), 9, 3, var4, var3);
               }

               for(var4 = 1; var4 <= 9; ++var4) {
                  this.a(var1, Blocks.e.u(), var4, 3, 1, var3);
                  this.a(var1, Blocks.e.u(), var4, 3, 9, var3);
               }

               this.a(var1, Blocks.e.u(), 5, 1, 4, var3);
               this.a(var1, Blocks.e.u(), 5, 1, 6, var3);
               this.a(var1, Blocks.e.u(), 5, 3, 4, var3);
               this.a(var1, Blocks.e.u(), 5, 3, 6, var3);
               this.a(var1, Blocks.e.u(), 4, 1, 5, var3);
               this.a(var1, Blocks.e.u(), 6, 1, 5, var3);
               this.a(var1, Blocks.e.u(), 4, 3, 5, var3);
               this.a(var1, Blocks.e.u(), 6, 3, 5, var3);

               for(var4 = 1; var4 <= 3; ++var4) {
                  this.a(var1, Blocks.e.u(), 4, var4, 4, var3);
                  this.a(var1, Blocks.e.u(), 6, var4, 4, var3);
                  this.a(var1, Blocks.e.u(), 4, var4, 6, var3);
                  this.a(var1, Blocks.e.u(), 6, var4, 6, var3);
               }

               this.a(var1, Blocks.aa.u(), 5, 3, 5, var3);

               for(var4 = 2; var4 <= 8; ++var4) {
                  this.a(var1, Blocks.f.u(), 2, 3, var4, var3);
                  this.a(var1, Blocks.f.u(), 3, 3, var4, var3);
                  if(var4 <= 3 || var4 >= 7) {
                     this.a(var1, Blocks.f.u(), 4, 3, var4, var3);
                     this.a(var1, Blocks.f.u(), 5, 3, var4, var3);
                     this.a(var1, Blocks.f.u(), 6, 3, var4, var3);
                  }

                  this.a(var1, Blocks.f.u(), 7, 3, var4, var3);
                  this.a(var1, Blocks.f.u(), 8, 3, var4, var3);
               }

               IBlockData var5 = Blocks.au.u().set(class_amj.a, EnumDirection.WEST);
               this.a(var1, var5, 9, 1, 3, var3);
               this.a(var1, var5, 9, 2, 3, var3);
               this.a(var1, var5, 9, 3, 3, var3);
               this.a(var1, var3, var2, 3, 4, 8, class_azs.i);
            }

            return true;
         }
      }
   }

   public static class class_i_in_class_awb extends WorldGenStrongholdPieces.class_d_in_class_awb {
      public void a(StructurePiece var1, List var2, Random var3) {
         EnumDirection var4 = this.e();
         if(var4 != EnumDirection.NORTH && var4 != EnumDirection.EAST) {
            this.b((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, 1, 1);
         } else {
            this.c((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, 1, 1);
         }

      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.a(var1, var3)) {
            return false;
         } else {
            this.a(var1, var3, 0, 0, 0, 4, 4, 4, true, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var2, var3, this.d, 1, 1, 0);
            EnumDirection var4 = this.e();
            if(var4 != EnumDirection.NORTH && var4 != EnumDirection.EAST) {
               this.a(var1, var3, 0, 1, 1, 0, 3, 3, Blocks.AIR.u(), Blocks.AIR.u(), false);
            } else {
               this.a(var1, var3, 4, 1, 1, 4, 3, 3, Blocks.AIR.u(), Blocks.AIR.u(), false);
            }

            return true;
         }
      }
   }

   public static class class_d_in_class_awb extends WorldGenStrongholdPieces.WorldGenStrongholdPiece {
      public class_d_in_class_awb() {
      }

      public class_d_in_class_awb(int var1, Random var2, StructureBoundingBox var3, EnumDirection var4) {
         super(var1);
         this.a(var4);
         this.d = this.a(var2);
         this.l = var3;
      }

      public void a(StructurePiece var1, List var2, Random var3) {
         EnumDirection var4 = this.e();
         if(var4 != EnumDirection.NORTH && var4 != EnumDirection.EAST) {
            this.c((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, 1, 1);
         } else {
            this.b((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, 1, 1);
         }

      }

      public static WorldGenStrongholdPieces.class_d_in_class_awb a(List var0, Random var1, int var2, int var3, int var4, EnumDirection var5, int var6) {
         StructureBoundingBox var7 = StructureBoundingBox.a(var2, var3, var4, -1, -1, 0, 5, 5, 5, var5);
         return a(var7) && StructurePiece.a(var0, var7) == null?new WorldGenStrongholdPieces.class_d_in_class_awb(var6, var1, var7, var5):null;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.a(var1, var3)) {
            return false;
         } else {
            this.a(var1, var3, 0, 0, 0, 4, 4, 4, true, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var2, var3, this.d, 1, 1, 0);
            EnumDirection var4 = this.e();
            if(var4 != EnumDirection.NORTH && var4 != EnumDirection.EAST) {
               this.a(var1, var3, 4, 1, 1, 4, 3, 3, Blocks.AIR.u(), Blocks.AIR.u(), false);
            } else {
               this.a(var1, var3, 0, 1, 1, 0, 3, 3, Blocks.AIR.u(), Blocks.AIR.u(), false);
            }

            return true;
         }
      }
   }

   public static class class_o_in_class_awb extends WorldGenStrongholdPieces.WorldGenStrongholdPiece {
      public class_o_in_class_awb() {
      }

      public class_o_in_class_awb(int var1, Random var2, StructureBoundingBox var3, EnumDirection var4) {
         super(var1);
         this.a(var4);
         this.d = this.a(var2);
         this.l = var3;
      }

      public void a(StructurePiece var1, List var2, Random var3) {
         this.a((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, 1, 1);
      }

      public static WorldGenStrongholdPieces.class_o_in_class_awb a(List var0, Random var1, int var2, int var3, int var4, EnumDirection var5, int var6) {
         StructureBoundingBox var7 = StructureBoundingBox.a(var2, var3, var4, -1, -7, 0, 5, 11, 8, var5);
         return a(var7) && StructurePiece.a(var0, var7) == null?new WorldGenStrongholdPieces.class_o_in_class_awb(var6, var1, var7, var5):null;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.a(var1, var3)) {
            return false;
         } else {
            this.a(var1, var3, 0, 0, 0, 4, 10, 7, true, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var2, var3, this.d, 1, 7, 0);
            this.a(var1, var2, var3, WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType.OPENING, 1, 1, 7);
            IBlockData var4 = Blocks.aw.u().set(BlockStairs.a, EnumDirection.SOUTH);

            for(int var5 = 0; var5 < 6; ++var5) {
               this.a(var1, var4, 1, 6 - var5, 1 + var5, var3);
               this.a(var1, var4, 2, 6 - var5, 1 + var5, var3);
               this.a(var1, var4, 3, 6 - var5, 1 + var5, var3);
               if(var5 < 5) {
                  this.a(var1, Blocks.bf.u(), 1, 5 - var5, 1 + var5, var3);
                  this.a(var1, Blocks.bf.u(), 2, 5 - var5, 1 + var5, var3);
                  this.a(var1, Blocks.bf.u(), 3, 5 - var5, 1 + var5, var3);
               }
            }

            return true;
         }
      }
   }

   public static class class_a_in_class_awb extends WorldGenStrongholdPieces.WorldGenStrongholdPiece {
      private boolean a;

      public class_a_in_class_awb() {
      }

      public class_a_in_class_awb(int var1, Random var2, StructureBoundingBox var3, EnumDirection var4) {
         super(var1);
         this.a(var4);
         this.d = this.a(var2);
         this.l = var3;
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("Chest", this.a);
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.a = var1.p("Chest");
      }

      public void a(StructurePiece var1, List var2, Random var3) {
         this.a((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, 1, 1);
      }

      public static WorldGenStrongholdPieces.class_a_in_class_awb a(List var0, Random var1, int var2, int var3, int var4, EnumDirection var5, int var6) {
         StructureBoundingBox var7 = StructureBoundingBox.a(var2, var3, var4, -1, -1, 0, 5, 5, 7, var5);
         return a(var7) && StructurePiece.a(var0, var7) == null?new WorldGenStrongholdPieces.class_a_in_class_awb(var6, var1, var7, var5):null;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.a(var1, var3)) {
            return false;
         } else {
            this.a(var1, var3, 0, 0, 0, 4, 4, 6, true, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var2, var3, this.d, 1, 1, 0);
            this.a(var1, var2, var3, WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType.OPENING, 1, 1, 6);
            this.a(var1, var3, 3, 1, 2, 3, 1, 4, Blocks.bf.u(), Blocks.bf.u(), false);
            this.a(var1, Blocks.U.a(BlockDoubleStepAbstract.EnumStoneSlabVariant.SMOOTHBRICK.a()), 3, 1, 1, var3);
            this.a(var1, Blocks.U.a(BlockDoubleStepAbstract.EnumStoneSlabVariant.SMOOTHBRICK.a()), 3, 1, 5, var3);
            this.a(var1, Blocks.U.a(BlockDoubleStepAbstract.EnumStoneSlabVariant.SMOOTHBRICK.a()), 3, 2, 2, var3);
            this.a(var1, Blocks.U.a(BlockDoubleStepAbstract.EnumStoneSlabVariant.SMOOTHBRICK.a()), 3, 2, 4, var3);

            for(int var4 = 2; var4 <= 4; ++var4) {
               this.a(var1, Blocks.U.a(BlockDoubleStepAbstract.EnumStoneSlabVariant.SMOOTHBRICK.a()), 2, 1, var4, var3);
            }

            if(!this.a && var3.b((BaseBlockPosition)(new BlockPosition(this.a(3, 3), this.d(2), this.b(3, 3))))) {
               this.a = true;
               this.a(var1, var3, var2, 3, 2, 3, class_azs.j);
            }

            return true;
         }
      }
   }

   public static class WorldGenStrongholdStairs extends WorldGenStrongholdPieces.WorldGenStrongholdPiece {
      private boolean a;
      private boolean b;

      public WorldGenStrongholdStairs() {
      }

      public WorldGenStrongholdStairs(int var1, Random var2, StructureBoundingBox var3, EnumDirection var4) {
         super(var1);
         this.a(var4);
         this.d = this.a(var2);
         this.l = var3;
         this.a = var2.nextInt(2) == 0;
         this.b = var2.nextInt(2) == 0;
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("Left", this.a);
         var1.a("Right", this.b);
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.a = var1.p("Left");
         this.b = var1.p("Right");
      }

      public void a(StructurePiece var1, List var2, Random var3) {
         this.a((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, 1, 1);
         if(this.a) {
            this.b((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, 1, 2);
         }

         if(this.b) {
            this.c((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, 1, 2);
         }

      }

      public static WorldGenStrongholdPieces.WorldGenStrongholdStairs a(List var0, Random var1, int var2, int var3, int var4, EnumDirection var5, int var6) {
         StructureBoundingBox var7 = StructureBoundingBox.a(var2, var3, var4, -1, -1, 0, 5, 5, 7, var5);
         return a(var7) && StructurePiece.a(var0, var7) == null?new WorldGenStrongholdPieces.WorldGenStrongholdStairs(var6, var1, var7, var5):null;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.a(var1, var3)) {
            return false;
         } else {
            this.a(var1, var3, 0, 0, 0, 4, 4, 6, true, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var2, var3, this.d, 1, 1, 0);
            this.a(var1, var2, var3, WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType.OPENING, 1, 1, 6);
            IBlockData var4 = Blocks.aa.u().set(class_ape.a, EnumDirection.EAST);
            IBlockData var5 = Blocks.aa.u().set(class_ape.a, EnumDirection.WEST);
            this.a(var1, var3, var2, 0.1F, 1, 2, 1, var4);
            this.a(var1, var3, var2, 0.1F, 3, 2, 1, var5);
            this.a(var1, var3, var2, 0.1F, 1, 2, 5, var4);
            this.a(var1, var3, var2, 0.1F, 3, 2, 5, var5);
            if(this.a) {
               this.a(var1, var3, 0, 1, 2, 0, 3, 4, Blocks.AIR.u(), Blocks.AIR.u(), false);
            }

            if(this.b) {
               this.a(var1, var3, 4, 1, 2, 4, 3, 4, Blocks.AIR.u(), Blocks.AIR.u(), false);
            }

            return true;
         }
      }
   }

   public static class class_m_in_class_awb extends WorldGenStrongholdPieces.WorldGenStrongholdStairs2 {
      public WorldGenStrongholdPieces.class_f_in_class_awb a;
      public WorldGenStrongholdPieces.class_g_in_class_awb b;
      public List c = Lists.newArrayList();

      public class_m_in_class_awb() {
      }

      public class_m_in_class_awb(int var1, Random var2, int var3, int var4) {
         super(0, var2, var3, var4);
      }

      public BlockPosition a() {
         return this.b != null?this.b.a():super.a();
      }
   }

   public static class WorldGenStrongholdStairs2 extends WorldGenStrongholdPieces.WorldGenStrongholdPiece {
      private boolean a;

      public WorldGenStrongholdStairs2() {
      }

      public WorldGenStrongholdStairs2(int var1, Random var2, int var3, int var4) {
         super(var1);
         this.a = true;
         this.a(EnumDirection.EnumDirectionLimit.HORIZONTAL.a(var2));
         this.d = WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType.OPENING;
         if(this.e().k() == EnumDirection.class_a_in_class_cq.Z) {
            this.l = new StructureBoundingBox(var3, 64, var4, var3 + 5 - 1, 74, var4 + 5 - 1);
         } else {
            this.l = new StructureBoundingBox(var3, 64, var4, var3 + 5 - 1, 74, var4 + 5 - 1);
         }

      }

      public WorldGenStrongholdStairs2(int var1, Random var2, StructureBoundingBox var3, EnumDirection var4) {
         super(var1);
         this.a = false;
         this.a(var4);
         this.d = this.a(var2);
         this.l = var3;
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("Source", this.a);
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.a = var1.p("Source");
      }

      public void a(StructurePiece var1, List var2, Random var3) {
         if(this.a) {
            WorldGenStrongholdPieces.d = WorldGenStrongholdPieces.WorldGenStrongholdCrossing.class;
         }

         this.a((WorldGenStrongholdPieces.class_m_in_class_awb)var1, var2, var3, 1, 1);
      }

      public static WorldGenStrongholdPieces.WorldGenStrongholdStairs2 a(List var0, Random var1, int var2, int var3, int var4, EnumDirection var5, int var6) {
         StructureBoundingBox var7 = StructureBoundingBox.a(var2, var3, var4, -1, -7, 0, 5, 11, 5, var5);
         return a(var7) && StructurePiece.a(var0, var7) == null?new WorldGenStrongholdPieces.WorldGenStrongholdStairs2(var6, var1, var7, var5):null;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.a(var1, var3)) {
            return false;
         } else {
            this.a(var1, var3, 0, 0, 0, 4, 10, 4, true, var2, WorldGenStrongholdPieces.e);
            this.a(var1, var2, var3, this.d, 1, 7, 0);
            this.a(var1, var2, var3, WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType.OPENING, 1, 1, 4);
            this.a(var1, Blocks.bf.u(), 2, 6, 1, var3);
            this.a(var1, Blocks.bf.u(), 1, 5, 1, var3);
            this.a(var1, Blocks.U.a(BlockDoubleStepAbstract.EnumStoneSlabVariant.STONE.a()), 1, 6, 1, var3);
            this.a(var1, Blocks.bf.u(), 1, 5, 2, var3);
            this.a(var1, Blocks.bf.u(), 1, 4, 3, var3);
            this.a(var1, Blocks.U.a(BlockDoubleStepAbstract.EnumStoneSlabVariant.STONE.a()), 1, 5, 3, var3);
            this.a(var1, Blocks.bf.u(), 2, 4, 3, var3);
            this.a(var1, Blocks.bf.u(), 3, 3, 3, var3);
            this.a(var1, Blocks.U.a(BlockDoubleStepAbstract.EnumStoneSlabVariant.STONE.a()), 3, 4, 3, var3);
            this.a(var1, Blocks.bf.u(), 3, 3, 2, var3);
            this.a(var1, Blocks.bf.u(), 3, 2, 1, var3);
            this.a(var1, Blocks.U.a(BlockDoubleStepAbstract.EnumStoneSlabVariant.STONE.a()), 3, 3, 1, var3);
            this.a(var1, Blocks.bf.u(), 2, 2, 1, var3);
            this.a(var1, Blocks.bf.u(), 1, 1, 1, var3);
            this.a(var1, Blocks.U.a(BlockDoubleStepAbstract.EnumStoneSlabVariant.STONE.a()), 1, 2, 1, var3);
            this.a(var1, Blocks.bf.u(), 1, 1, 2, var3);
            this.a(var1, Blocks.U.a(BlockDoubleStepAbstract.EnumStoneSlabVariant.STONE.a()), 1, 1, 3, var3);
            return true;
         }
      }
   }

   public static class class_b_in_class_awb extends WorldGenStrongholdPieces.WorldGenStrongholdPiece {
      private int a;

      public class_b_in_class_awb() {
      }

      public class_b_in_class_awb(int var1, Random var2, StructureBoundingBox var3, EnumDirection var4) {
         super(var1);
         this.a(var4);
         this.l = var3;
         this.a = var4 != EnumDirection.NORTH && var4 != EnumDirection.SOUTH?var3.c():var3.e();
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("Steps", this.a);
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.a = var1.h("Steps");
      }

      public static StructureBoundingBox a(List var0, Random var1, int var2, int var3, int var4, EnumDirection var5) {
         boolean var6 = true;
         StructureBoundingBox var7 = StructureBoundingBox.a(var2, var3, var4, -1, -1, 0, 5, 5, 4, var5);
         StructurePiece var8 = StructurePiece.a(var0, var7);
         if(var8 == null) {
            return null;
         } else {
            if(var8.c().b == var7.b) {
               for(int var9 = 3; var9 >= 1; --var9) {
                  var7 = StructureBoundingBox.a(var2, var3, var4, -1, -1, 0, 5, 5, var9 - 1, var5);
                  if(!var8.c().a(var7)) {
                     return StructureBoundingBox.a(var2, var3, var4, -1, -1, 0, 5, 5, var9, var5);
                  }
               }
            }

            return null;
         }
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.a(var1, var3)) {
            return false;
         } else {
            for(int var4 = 0; var4 < this.a; ++var4) {
               this.a(var1, Blocks.bf.u(), 0, 0, var4, var3);
               this.a(var1, Blocks.bf.u(), 1, 0, var4, var3);
               this.a(var1, Blocks.bf.u(), 2, 0, var4, var3);
               this.a(var1, Blocks.bf.u(), 3, 0, var4, var3);
               this.a(var1, Blocks.bf.u(), 4, 0, var4, var3);

               for(int var5 = 1; var5 <= 3; ++var5) {
                  this.a(var1, Blocks.bf.u(), 0, var5, var4, var3);
                  this.a(var1, Blocks.AIR.u(), 1, var5, var4, var3);
                  this.a(var1, Blocks.AIR.u(), 2, var5, var4, var3);
                  this.a(var1, Blocks.AIR.u(), 3, var5, var4, var3);
                  this.a(var1, Blocks.bf.u(), 4, var5, var4, var3);
               }

               this.a(var1, Blocks.bf.u(), 0, 4, var4, var3);
               this.a(var1, Blocks.bf.u(), 1, 4, var4, var3);
               this.a(var1, Blocks.bf.u(), 2, 4, var4, var3);
               this.a(var1, Blocks.bf.u(), 3, 4, var4, var3);
               this.a(var1, Blocks.bf.u(), 4, 4, var4, var3);
            }

            return true;
         }
      }
   }

   abstract static class WorldGenStrongholdPiece extends StructurePiece {
      protected WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType d = WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType.OPENING;

      public WorldGenStrongholdPiece() {
      }

      protected WorldGenStrongholdPiece(int var1) {
         super(var1);
      }

      protected void a(NBTTagCompound var1) {
         var1.a("EntryDoor", this.d.name());
      }

      protected void b(NBTTagCompound var1) {
         this.d = WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType.valueOf(var1.l("EntryDoor"));
      }

      protected void a(World var1, Random var2, StructureBoundingBox var3, WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType var4, int var5, int var6, int var7) {
         switch(WorldGenStrongholdPieces.SyntheticClass_1.a[var4.ordinal()]) {
         case 1:
         default:
            this.a(var1, var3, var5, var6, var7, var5 + 3 - 1, var6 + 3 - 1, var7, Blocks.AIR.u(), Blocks.AIR.u(), false);
            break;
         case 2:
            this.a(var1, Blocks.bf.u(), var5, var6, var7, var3);
            this.a(var1, Blocks.bf.u(), var5, var6 + 1, var7, var3);
            this.a(var1, Blocks.bf.u(), var5, var6 + 2, var7, var3);
            this.a(var1, Blocks.bf.u(), var5 + 1, var6 + 2, var7, var3);
            this.a(var1, Blocks.bf.u(), var5 + 2, var6 + 2, var7, var3);
            this.a(var1, Blocks.bf.u(), var5 + 2, var6 + 1, var7, var3);
            this.a(var1, Blocks.bf.u(), var5 + 2, var6, var7, var3);
            this.a(var1, Blocks.ao.u(), var5 + 1, var6, var7, var3);
            this.a(var1, Blocks.ao.u().set(BlockDoor.e, BlockDoor.class_a_in_class_aku.UPPER), var5 + 1, var6 + 1, var7, var3);
            break;
         case 3:
            this.a(var1, Blocks.AIR.u(), var5 + 1, var6, var7, var3);
            this.a(var1, Blocks.AIR.u(), var5 + 1, var6 + 1, var7, var3);
            this.a(var1, Blocks.bi.u(), var5, var6, var7, var3);
            this.a(var1, Blocks.bi.u(), var5, var6 + 1, var7, var3);
            this.a(var1, Blocks.bi.u(), var5, var6 + 2, var7, var3);
            this.a(var1, Blocks.bi.u(), var5 + 1, var6 + 2, var7, var3);
            this.a(var1, Blocks.bi.u(), var5 + 2, var6 + 2, var7, var3);
            this.a(var1, Blocks.bi.u(), var5 + 2, var6 + 1, var7, var3);
            this.a(var1, Blocks.bi.u(), var5 + 2, var6, var7, var3);
            break;
         case 4:
            this.a(var1, Blocks.bf.u(), var5, var6, var7, var3);
            this.a(var1, Blocks.bf.u(), var5, var6 + 1, var7, var3);
            this.a(var1, Blocks.bf.u(), var5, var6 + 2, var7, var3);
            this.a(var1, Blocks.bf.u(), var5 + 1, var6 + 2, var7, var3);
            this.a(var1, Blocks.bf.u(), var5 + 2, var6 + 2, var7, var3);
            this.a(var1, Blocks.bf.u(), var5 + 2, var6 + 1, var7, var3);
            this.a(var1, Blocks.bf.u(), var5 + 2, var6, var7, var3);
            this.a(var1, Blocks.aA.u(), var5 + 1, var6, var7, var3);
            this.a(var1, Blocks.aA.u().set(BlockDoor.e, BlockDoor.class_a_in_class_aku.UPPER), var5 + 1, var6 + 1, var7, var3);
            this.a(var1, Blocks.aG.u().set(class_ajy.H, EnumDirection.NORTH), var5 + 2, var6 + 1, var7 + 1, var3);
            this.a(var1, Blocks.aG.u().set(class_ajy.H, EnumDirection.SOUTH), var5 + 2, var6 + 1, var7 - 1, var3);
         }

      }

      protected WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType a(Random var1) {
         int var2 = var1.nextInt(5);
         switch(var2) {
         case 0:
         case 1:
         default:
            return WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType.OPENING;
         case 2:
            return WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType.WOOD_DOOR;
         case 3:
            return WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType.GRATES;
         case 4:
            return WorldGenStrongholdPieces.WorldGenStrongholdPiece.WorldGenStrongholdPiece$WorldGenStrongholdDoorType.IRON_DOOR;
         }
      }

      protected StructurePiece a(WorldGenStrongholdPieces.class_m_in_class_awb var1, List var2, Random var3, int var4, int var5) {
         EnumDirection var6 = this.e();
         if(var6 != null) {
            switch(WorldGenStrongholdPieces.SyntheticClass_1.b[var6.ordinal()]) {
            case 1:
               return WorldGenStrongholdPieces.c(var1, var2, var3, this.l.a + var4, this.l.b + var5, this.l.c - 1, var6, this.d());
            case 2:
               return WorldGenStrongholdPieces.c(var1, var2, var3, this.l.a + var4, this.l.b + var5, this.l.f + 1, var6, this.d());
            case 3:
               return WorldGenStrongholdPieces.c(var1, var2, var3, this.l.a - 1, this.l.b + var5, this.l.c + var4, var6, this.d());
            case 4:
               return WorldGenStrongholdPieces.c(var1, var2, var3, this.l.d + 1, this.l.b + var5, this.l.c + var4, var6, this.d());
            }
         }

         return null;
      }

      protected StructurePiece b(WorldGenStrongholdPieces.class_m_in_class_awb var1, List var2, Random var3, int var4, int var5) {
         EnumDirection var6 = this.e();
         if(var6 != null) {
            switch(WorldGenStrongholdPieces.SyntheticClass_1.b[var6.ordinal()]) {
            case 1:
               return WorldGenStrongholdPieces.c(var1, var2, var3, this.l.a - 1, this.l.b + var4, this.l.c + var5, EnumDirection.WEST, this.d());
            case 2:
               return WorldGenStrongholdPieces.c(var1, var2, var3, this.l.a - 1, this.l.b + var4, this.l.c + var5, EnumDirection.WEST, this.d());
            case 3:
               return WorldGenStrongholdPieces.c(var1, var2, var3, this.l.a + var5, this.l.b + var4, this.l.c - 1, EnumDirection.NORTH, this.d());
            case 4:
               return WorldGenStrongholdPieces.c(var1, var2, var3, this.l.a + var5, this.l.b + var4, this.l.c - 1, EnumDirection.NORTH, this.d());
            }
         }

         return null;
      }

      protected StructurePiece c(WorldGenStrongholdPieces.class_m_in_class_awb var1, List var2, Random var3, int var4, int var5) {
         EnumDirection var6 = this.e();
         if(var6 != null) {
            switch(WorldGenStrongholdPieces.SyntheticClass_1.b[var6.ordinal()]) {
            case 1:
               return WorldGenStrongholdPieces.c(var1, var2, var3, this.l.d + 1, this.l.b + var4, this.l.c + var5, EnumDirection.EAST, this.d());
            case 2:
               return WorldGenStrongholdPieces.c(var1, var2, var3, this.l.d + 1, this.l.b + var4, this.l.c + var5, EnumDirection.EAST, this.d());
            case 3:
               return WorldGenStrongholdPieces.c(var1, var2, var3, this.l.a + var5, this.l.b + var4, this.l.f + 1, EnumDirection.SOUTH, this.d());
            case 4:
               return WorldGenStrongholdPieces.c(var1, var2, var3, this.l.a + var5, this.l.b + var4, this.l.f + 1, EnumDirection.SOUTH, this.d());
            }
         }

         return null;
      }

      protected static boolean a(StructureBoundingBox var0) {
         return var0 != null && var0.b > 10;
      }

      public static enum WorldGenStrongholdPiece$WorldGenStrongholdDoorType {
         OPENING,
         WOOD_DOOR,
         GRATES,
         IRON_DOOR;
      }
   }

   static class class_f_in_class_awb {
      public Class a;
      public final int b;
      public int c;
      public int d;

      public class_f_in_class_awb(Class var1, int var2, int var3) {
         this.a = var1;
         this.b = var2;
         this.d = var3;
      }

      public boolean a(int var1) {
         return this.d == 0 || this.c < this.d;
      }

      public boolean a() {
         return this.d == 0 || this.c < this.d;
      }
   }
}
