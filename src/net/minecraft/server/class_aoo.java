package net.minecraft.server;

import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;

public class class_aoo {
   public static final class_aoo a = new class_aoo(1.0F, 1.0F, class_ng.gQ, class_ng.gY, class_ng.gV, class_ng.gU, class_ng.gT);
   public static final class_aoo b = new class_aoo(1.0F, 1.0F, class_ng.bW, class_ng.ca, class_ng.bZ, class_ng.bY, class_ng.bX);
   public static final class_aoo c = new class_aoo(1.0F, 1.0F, class_ng.bR, class_ng.bV, class_ng.bU, class_ng.bT, class_ng.bS);
   public static final class_aoo d = new class_aoo(1.0F, 1.0F, class_ng.fT, class_ng.gb, class_ng.fY, class_ng.fX, class_ng.fW);
   public static final class_aoo e = new class_aoo(1.0F, 1.5F, class_ng.dk, class_ng.dq, class_ng.dn, class_ng.dm, class_ng.dl);
   public static final class_aoo f = new class_aoo(1.0F, 1.0F, class_ng.bM, class_ng.bQ, class_ng.bP, class_ng.bO, class_ng.bN);
   public static final class_aoo g = new class_aoo(1.0F, 1.0F, class_ng.ae, class_ng.ai, class_ng.ah, class_ng.ag, class_ng.af);
   public static final class_aoo h = new class_aoo(1.0F, 1.0F, class_ng.eB, class_ng.eF, class_ng.eE, class_ng.eD, class_ng.eC);
   public static final class_aoo i = new class_aoo(1.0F, 1.0F, class_ng.fF, class_ng.fJ, class_ng.fI, class_ng.fH, class_ng.fG);
   public static final class_aoo j = new class_aoo(1.0F, 1.0F, class_ng.cS, class_ng.cW, class_ng.cV, class_ng.cU, class_ng.cT);
   public static final class_aoo k = new class_aoo(0.3F, 1.0F, class_ng.b, class_ng.h, class_ng.g, class_ng.e, class_ng.d);
   public static final class_aoo l = new class_aoo(1.0F, 1.0F, class_ng.fl, class_ng.ft, class_ng.fr, class_ng.fo, class_ng.fn);
   public final float m;
   public final float n;
   private final class_nf o;
   private final class_nf p;
   private final class_nf q;
   private final class_nf r;
   private final class_nf s;

   public class_aoo(float var1, float var2, class_nf var3, class_nf var4, class_nf var5, class_nf var6, class_nf var7) {
      this.m = var1;
      this.n = var2;
      this.o = var3;
      this.p = var4;
      this.q = var5;
      this.r = var6;
      this.s = var7;
   }

   public float a() {
      return this.m;
   }

   public float b() {
      return this.n;
   }

   public class_nf d() {
      return this.p;
   }

   public class_nf e() {
      return this.q;
   }

   public class_nf g() {
      return this.s;
   }
}
