package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Item;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aly;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public abstract class class_app extends class_aly {
   public static final BlockStateEnum d = BlockStateEnum.a("variant", BlockWood.EnumLogVariant.class);

   public class_app() {
      super(Material.d);
      IBlockData var1 = this.A.b();
      if(!this.e()) {
         var1 = var1.set(a, class_aly.class_a_in_class_aly.BOTTOM);
      }

      this.w(var1.set(d, BlockWood.EnumLogVariant.OAK));
      this.a(CreativeModeTab.b);
   }

   public MaterialMapColor r(IBlockData var1) {
      return ((BlockWood.EnumLogVariant)var1.get(d)).c();
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Item.a((Block)Blocks.bM);
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Blocks.bM, 1, ((BlockWood.EnumLogVariant)var3.get(d)).a());
   }

   public String e(int var1) {
      return super.a() + "." + BlockWood.EnumLogVariant.a(var1).d();
   }

   public IBlockState g() {
      return d;
   }

   public Comparable a(ItemStack var1) {
      return BlockWood.EnumLogVariant.a(var1.i() & 7);
   }

   public IBlockData a(int var1) {
      IBlockData var2 = this.u().set(d, BlockWood.EnumLogVariant.a(var1 & 7));
      if(!this.e()) {
         var2 = var2.set(a, (var1 & 8) == 0?class_aly.class_a_in_class_aly.BOTTOM:class_aly.class_a_in_class_aly.TOP);
      }

      return var2;
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((BlockWood.EnumLogVariant)var1.get(d)).a();
      if(!this.e() && var1.get(a) == class_aly.class_a_in_class_aly.TOP) {
         var3 |= 8;
      }

      return var3;
   }

   protected BlockStateList b() {
      return this.e()?new BlockStateList(this, new IBlockState[]{d}):new BlockStateList(this, new IBlockState[]{a, d});
   }

   public int d(IBlockData var1) {
      return ((BlockWood.EnumLogVariant)var1.get(d)).a();
   }
}
