package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.BlockFlowers;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ate;
import net.minecraft.server.class_ato;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_yw;

public class class_ajb extends BiomeBase {
   protected static final IBlockData y = Blocks.bx.u();

   protected class_ajb(BiomeBase.class_a_in_class_aif var1) {
      super(var1);
      this.t.z = 2;
      this.t.A = 1;
      this.t.C = 1;
      this.t.D = 8;
      this.t.E = 10;
      this.t.I = 1;
      this.t.y = 4;
      this.t.H = 0;
      this.t.G = 0;
      this.t.B = 5;
      this.u.add(new BiomeBase.BiomeMeta(class_yw.class, 1, 1, 1));
   }

   public class_ato a(Random var1) {
      return p;
   }

   public BlockFlowers.EnumFlowerVarient a(Random var1, BlockPosition var2) {
      return BlockFlowers.EnumFlowerVarient.BLUE_ORCHID;
   }

   public void a(World var1, Random var2, class_ate var3, int var4, int var5, double var6) {
      double var8 = l.a((double)var4 * 0.25D, (double)var5 * 0.25D);
      if(var8 > 0.0D) {
         int var10 = var4 & 15;
         int var11 = var5 & 15;

         for(int var12 = 255; var12 >= 0; --var12) {
            if(var3.a(var11, var12, var10).getMaterial() != Material.a) {
               if(var12 == 62 && var3.a(var11, var12, var10).getBlock() != Blocks.j) {
                  var3.a(var11, var12, var10, h);
                  if(var8 < 0.12D) {
                     var3.a(var11, var12 + 1, var10, y);
                  }
               }
               break;
            }
         }
      }

      this.b(var1, var2, var3, var4, var5, var6);
   }
}
