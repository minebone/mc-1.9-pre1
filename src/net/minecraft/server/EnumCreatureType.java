package net.minecraft.server;

import net.minecraft.server.EntityAnimal;
import net.minecraft.server.Material;
import net.minecraft.server.class_vs;
import net.minecraft.server.class_wh;
import net.minecraft.server.class_yk;

public enum EnumCreatureType {
   MONSTER(class_yk.class, 70, Material.a, false, false),
   CREATURE(EntityAnimal.class, 10, Material.a, true, true),
   AMBIENT(class_vs.class, 15, Material.a, true, false),
   WATER_CREATURE(class_wh.class, 5, Material.h, true, false);

   private final Class e;
   private final int f;
   private final Material g;
   private final boolean h;
   private final boolean i;

   private EnumCreatureType(Class var3, int var4, Material var5, boolean var6, boolean var7) {
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.i = var7;
   }

   public Class a() {
      return this.e;
   }

   public int b() {
      return this.f;
   }

   public boolean d() {
      return this.h;
   }

   public boolean e() {
      return this.i;
   }
}
