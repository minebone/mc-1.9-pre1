package net.minecraft.server;

import com.google.common.base.Predicate;
import java.util.List;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;
import net.minecraft.server.class_ro;
import net.minecraft.server.class_wt;

public class class_acf extends Item {
   public class_acf() {
      this.a(CreativeModeTab.k);
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      List var5 = var2.a(class_ro.class, var3.bk().g(2.0D), new Predicate() {
         public boolean a(class_ro var1) {
            return var1 != null && var1.at() && var1.w() instanceof class_wt;
         }

         // $FF: synthetic method
         public boolean apply(Object var1) {
            return this.a((class_ro)var1);
         }
      });
      if(!var5.isEmpty()) {
         class_ro var8 = (class_ro)var5.get(0);
         var8.a(var8.j() - 0.5F);
         var2.a((EntityHuman)null, var3.locX, var3.locY, var3.locZ, class_ng.I, EnumSoundCategory.NEUTRAL, 1.0F, 1.0F);
         return new class_qo(EnumResult.SUCCESS, this.a(var1, var3, new ItemStack(Items.bK)));
      } else {
         MovingObjectPosition var6 = this.a(var2, var3, true);
         if(var6 == null) {
            return new class_qo(EnumResult.PASS, var1);
         } else {
            if(var6.a == MovingObjectPosition.EnumMovingObjectType.BLOCK) {
               BlockPosition var7 = var6.a();
               if(!var2.a(var3, var7) || !var3.a(var7.a(var6.b), var6.b, var1)) {
                  return new class_qo(EnumResult.PASS, var1);
               }

               if(var2.getType(var7).getMaterial() == Material.h) {
                  var2.a(var3, var3.locX, var3.locY, var3.locZ, class_ng.H, EnumSoundCategory.NEUTRAL, 1.0F, 1.0F);
                  return new class_qo(EnumResult.SUCCESS, this.a(var1, var3, new ItemStack(Items.bG)));
               }
            }

            return new class_qo(EnumResult.PASS, var1);
         }
      }
   }

   protected ItemStack a(ItemStack var1, EntityHuman var2, ItemStack var3) {
      --var1.b;
      var2.b(StatisticList.b((Item)this));
      if(var1.b <= 0) {
         return var3;
      } else {
         if(!var2.br.c(var3)) {
            var2.a(var3, false);
         }

         return var1;
      }
   }
}
