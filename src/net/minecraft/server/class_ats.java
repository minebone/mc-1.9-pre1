package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Blocks;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.class_apw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_auc;
import net.minecraft.server.Material;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;

public class class_ats extends class_auc {
   public boolean b(World var1, Random var2, BlockPosition var3) {
      IBlockData var4;
      while(((var4 = var1.getType(var3)).getMaterial() == Material.a || var4.getMaterial() == Material.j) && var3.q() > 1) {
         var3 = var3.b();
      }

      if(var3.q() < 1) {
         return false;
      } else {
         var3 = var3.a();

         for(int var5 = 0; var5 < 4; ++var5) {
            BlockPosition var6 = var3.a(var2.nextInt(4) - var2.nextInt(4), var2.nextInt(3) - var2.nextInt(3), var2.nextInt(4) - var2.nextInt(4));
            if(var1.d(var6) && var1.getType(var6.b()).q()) {
               var1.a((BlockPosition)var6, (IBlockData)Blocks.ae.u(), 2);
               TileEntity var7 = var1.r(var6);
               if(var7 instanceof class_apw) {
                  ((class_apw)var7).a(class_azs.b, var2.nextLong());
               }

               BlockPosition var8 = var6.f();
               BlockPosition var9 = var6.e();
               BlockPosition var10 = var6.c();
               BlockPosition var11 = var6.d();
               if(var1.d(var9) && var1.getType(var9.b()).q()) {
                  var1.a((BlockPosition)var9, (IBlockData)Blocks.aa.u(), 2);
               }

               if(var1.d(var8) && var1.getType(var8.b()).q()) {
                  var1.a((BlockPosition)var8, (IBlockData)Blocks.aa.u(), 2);
               }

               if(var1.d(var10) && var1.getType(var10.b()).q()) {
                  var1.a((BlockPosition)var10, (IBlockData)Blocks.aa.u(), 2);
               }

               if(var1.d(var11) && var1.getType(var11.b()).q()) {
                  var1.a((BlockPosition)var11, (IBlockData)Blocks.aa.u(), 2);
               }

               return true;
            }
         }

         return false;
      }
   }
}
