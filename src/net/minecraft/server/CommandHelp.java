package net.minecraft.server;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import net.minecraft.server.ChatClickable;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumChatFormat;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_ahi;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cb;
import net.minecraft.server.class_ce;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_k;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.MathHelper;

public class CommandHelp extends CommandAbstract {
   private static String[] a = new String[]{"Yolo", "/achievement take achievement.understandCommands @p", "Ask for help on twitter", "/deop @p", "Scoreboard deleted, commands blocked", "Contact helpdesk for help", "/testfornoob @p", "/trigger warning", "Oh my god, it\'s full of stats", "/kill @p[name=!Searge]", "Have you tried turning it off and on again?", "Sorry, no help today"};
   private Random b = new Random();

   public String c() {
      return "help";
   }

   public int a() {
      return 0;
   }

   public String b(ICommandListener var1) {
      return "commands.help.usage";
   }

   public List b() {
      return Arrays.asList(new String[]{"?"});
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var2 instanceof class_ahi) {
         var2.a((new ChatComponentText("Searge says: ")).a(a[this.b.nextInt(a.length) % a.length]));
      } else {
         List var4 = this.a(var2, var1);
         boolean var5 = true;
         int var6 = (var4.size() - 1) / 7;
         boolean var7 = false;

         int var14;
         try {
            var14 = var3.length == 0?0:a(var3[0], 1, var6 + 1) - 1;
         } catch (class_cb var13) {
            Map var9 = this.a(var1);
            class_k var10 = (class_k)var9.get(var3[0]);
            if(var10 != null) {
               throw new class_cf(var10.b(var2), new Object[0]);
            }

            if(MathHelper.a(var3[0], -1) != -1) {
               throw var13;
            }

            throw new class_ce();
         }

         int var8 = Math.min((var14 + 1) * 7, var4.size());
         ChatMessage var15 = new ChatMessage("commands.help.header", new Object[]{Integer.valueOf(var14 + 1), Integer.valueOf(var6 + 1)});
         var15.b().a(EnumChatFormat.DARK_GREEN);
         var2.a(var15);

         for(int var16 = var14 * 7; var16 < var8; ++var16) {
            class_k var11 = (class_k)var4.get(var16);
            ChatMessage var12 = new ChatMessage(var11.b(var2), new Object[0]);
            var12.b().a(new ChatClickable(ChatClickable.class_a_in_class_et.SUGGEST_COMMAND, "/" + var11.c() + " "));
            var2.a(var12);
         }

         if(var14 == 0 && var2 instanceof EntityHuman) {
            ChatMessage var17 = new ChatMessage("commands.help.footer", new Object[0]);
            var17.b().a(EnumChatFormat.GREEN);
            var2.a(var17);
         }

      }
   }

   protected List a(ICommandListener var1, MinecraftServer var2) {
      List var3 = var2.N().a(var1);
      Collections.sort(var3);
      return var3;
   }

   protected Map a(MinecraftServer var1) {
      return var1.N().b();
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      if(var3.length == 1) {
         Set var5 = this.a(var1).keySet();
         return a(var3, (String[])var5.toArray(new String[var5.size()]));
      } else {
         return Collections.emptyList();
      }
   }
}
