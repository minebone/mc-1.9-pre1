package net.minecraft.server;

import com.google.common.base.Optional;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;

public class class_zp extends Entity {
   private static final class_ke a = DataWatcher.a(class_zp.class, class_kg.f);
   private int b;
   private int c;

   public class_zp(World var1) {
      super(var1);
      this.a(0.25F, 0.25F);
   }

   protected void i() {
      this.datawatcher.a((class_ke)a, (Object)Optional.absent());
   }

   public class_zp(World var1, double var2, double var4, double var6, ItemStack var8) {
      super(var1);
      this.b = 0;
      this.a(0.25F, 0.25F);
      this.b(var2, var4, var6);
      int var9 = 1;
      if(var8 != null && var8.n()) {
         this.datawatcher.b(a, Optional.of(var8));
         NBTTagCompound var10 = var8.o();
         NBTTagCompound var11 = var10.o("Fireworks");
         var9 += var11.f("Flight");
      }

      this.motX = this.random.nextGaussian() * 0.001D;
      this.motZ = this.random.nextGaussian() * 0.001D;
      this.motY = 0.05D;
      this.c = 10 * var9 + this.random.nextInt(6) + this.random.nextInt(7);
   }

   public void m() {
      this.M = this.locX;
      this.N = this.locY;
      this.O = this.locZ;
      super.m();
      this.motX *= 1.15D;
      this.motZ *= 1.15D;
      this.motY += 0.04D;
      this.d(this.motX, this.motY, this.motZ);
      float var1 = MathHelper.a(this.motX * this.motX + this.motZ * this.motZ);
      this.yaw = (float)(MathHelper.b(this.motX, this.motZ) * 57.2957763671875D);

      for(this.pitch = (float)(MathHelper.b(this.motY, (double)var1) * 57.2957763671875D); this.pitch - this.lastPitch < -180.0F; this.lastPitch -= 360.0F) {
         ;
      }

      while(this.pitch - this.lastPitch >= 180.0F) {
         this.lastPitch += 360.0F;
      }

      while(this.yaw - this.lastYaw < -180.0F) {
         this.lastYaw -= 360.0F;
      }

      while(this.yaw - this.lastYaw >= 180.0F) {
         this.lastYaw += 360.0F;
      }

      this.pitch = this.lastPitch + (this.pitch - this.lastPitch) * 0.2F;
      this.yaw = this.lastYaw + (this.yaw - this.lastYaw) * 0.2F;
      if(this.b == 0 && !this.ac()) {
         this.world.a((EntityHuman)null, this.locX, this.locY, this.locZ, class_ng.bo, EnumSoundCategory.AMBIENT, 3.0F, 1.0F);
      }

      ++this.b;
      if(this.world.E && this.b % 2 < 2) {
         this.world.a(EnumParticle.FIREWORKS_SPARK, this.locX, this.locY - 0.3D, this.locZ, this.random.nextGaussian() * 0.05D, -this.motY * 0.5D, this.random.nextGaussian() * 0.05D, new int[0]);
      }

      if(!this.world.E && this.b > this.c) {
         this.world.a((Entity)this, (byte)17);
         this.S();
      }

   }

   public void b(NBTTagCompound var1) {
      var1.a("Life", this.b);
      var1.a("LifeTime", this.c);
      ItemStack var2 = (ItemStack)((Optional)this.datawatcher.a(a)).orNull();
      if(var2 != null) {
         NBTTagCompound var3 = new NBTTagCompound();
         var2.b(var3);
         var1.a((String)"FireworksItem", (NBTTag)var3);
      }

   }

   public void a(NBTTagCompound var1) {
      this.b = var1.h("Life");
      this.c = var1.h("LifeTime");
      NBTTagCompound var2 = var1.o("FireworksItem");
      if(var2 != null) {
         ItemStack var3 = ItemStack.a(var2);
         if(var3 != null) {
            this.datawatcher.b(a, Optional.of(var3));
         }
      }

   }

   public float e(float var1) {
      return super.e(var1);
   }

   public boolean aS() {
      return false;
   }
}
