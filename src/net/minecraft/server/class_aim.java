package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.BlockMonsterEggs;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ate;
import net.minecraft.server.class_ato;
import net.minecraft.server.class_auc;
import net.minecraft.server.class_aus;
import net.minecraft.server.class_avc;
import net.minecraft.server.BlockPosition;

public class class_aim extends BiomeBase {
   private class_auc y = new class_aus(Blocks.be.u().set(BlockMonsterEggs.a, BlockMonsterEggs.EnumMonsterEggVarient.a), 9);
   private class_avc z = new class_avc(false);
   private final class_aim.class_a_in_class_aim A;

   protected class_aim(class_aim.class_a_in_class_aim var1, BiomeBase.class_a_in_class_aif var2) {
      super(var2);
      if(var1 == class_aim.class_a_in_class_aim.EXTRA_TREES) {
         this.t.z = 3;
      }

      this.A = var1;
   }

   public class_ato a(Random var1) {
      return (class_ato)(var1.nextInt(3) > 0?this.z:super.a(var1));
   }

   public void a(World var1, Random var2, BlockPosition var3) {
      super.a(var1, var2, var3);
      int var4 = 3 + var2.nextInt(6);

      int var5;
      int var6;
      int var7;
      for(var5 = 0; var5 < var4; ++var5) {
         var6 = var2.nextInt(16);
         var7 = var2.nextInt(28) + 4;
         int var8 = var2.nextInt(16);
         BlockPosition var9 = var3.a(var6, var7, var8);
         if(var1.getType(var9).getBlock() == Blocks.b) {
            var1.a((BlockPosition)var9, (IBlockData)Blocks.bP.u(), 2);
         }
      }

      for(var4 = 0; var4 < 7; ++var4) {
         var5 = var2.nextInt(16);
         var6 = var2.nextInt(64);
         var7 = var2.nextInt(16);
         this.y.b(var1, var2, var3.a(var5, var6, var7));
      }

   }

   public void a(World var1, Random var2, class_ate var3, int var4, int var5, double var6) {
      this.r = Blocks.c.u();
      this.s = Blocks.d.u();
      if((var6 < -1.0D || var6 > 2.0D) && this.A == class_aim.class_a_in_class_aim.MUTATED) {
         this.r = Blocks.n.u();
         this.s = Blocks.n.u();
      } else if(var6 > 1.0D && this.A != class_aim.class_a_in_class_aim.EXTRA_TREES) {
         this.r = Blocks.b.u();
         this.s = Blocks.b.u();
      }

      this.b(var1, var2, var3, var4, var5, var6);
   }

   public static enum class_a_in_class_aim {
      NORMAL,
      EXTRA_TREES,
      MUTATED;
   }
}
