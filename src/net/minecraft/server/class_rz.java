package net.minecraft.server;

import com.google.common.base.Objects;
import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import net.minecraft.server.AttributeMapBase;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CombatTracker;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityCreature;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityTracker;
import net.minecraft.server.EnumAnimation;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumMonsterType;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Item;
import net.minecraft.server.ItemArmor;
import net.minecraft.server.Items;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.MobEffectType;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.EntityBoat;
import net.minecraft.server.class_acw;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aff;
import net.minecraft.server.class_agn;
import net.minecraft.server.class_agp;
import net.minecraft.server.class_ags;
import net.minecraft.server.class_amj;
import net.minecraft.server.class_aoo;
import net.minecraft.server.class_apf;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutAnimation;
import net.minecraft.server.PacketPlayOutEntityEquipment;
import net.minecraft.server.PacketPlayOutCollect;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_qz;
import net.minecraft.server.class_rc;
import net.minecraft.server.class_ru;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rw;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sl;
import net.minecraft.server.class_sq;
import net.minecraft.server.class_wi;
import net.minecraft.server.class_yc;
import net.minecraft.server.class_ys;
import net.minecraft.server.class_zl;

public abstract class class_rz extends Entity {
   private static final UUID a = UUID.fromString("662A6B8D-DA3E-4C1C-8813-96EA6097278D");
   private static final AttributeModifier b = (new AttributeModifier(a, "Sprinting speed boost", 0.30000001192092896D, 2)).a(false);
   protected static final class_ke as = DataWatcher.a(class_rz.class, class_kg.a);
   private static final class_ke c = DataWatcher.a(class_rz.class, class_kg.c);
   private static final class_ke f = DataWatcher.a(class_rz.class, class_kg.b);
   private static final class_ke g = DataWatcher.a(class_rz.class, class_kg.h);
   private static final class_ke h = DataWatcher.a(class_rz.class, class_kg.b);
   private AttributeMapBase bp;
   private final CombatTracker bq = new CombatTracker(this);
   private final Map br = Maps.newHashMap();
   private final ItemStack[] bs = new ItemStack[2];
   private final ItemStack[] bt = new ItemStack[4];
   public boolean at;
   public EnumHand au;
   public int av;
   public int aw;
   public int ax;
   public int ay;
   public float az;
   public int aA;
   public float aB;
   public float aC;
   protected int aD;
   public float aE;
   public float aF;
   public float aG;
   public int aH = 20;
   public float aI;
   public float aJ;
   public float aK;
   public float aL;
   public float aM;
   public float aN;
   public float aO;
   public float aP;
   public float aQ = 0.02F;
   protected EntityHuman aR;
   protected int aS;
   protected boolean aT;
   protected int aU;
   protected float aV;
   protected float aW;
   protected float aX;
   protected float aY;
   protected float aZ;
   protected int ba;
   protected float bb;
   protected boolean bc;
   public float bd;
   public float be;
   public float bf;
   protected int bg;
   protected double bh;
   protected double bi;
   protected double bj;
   protected double bk;
   protected double bl;
   private boolean bu = true;
   private class_rz bv;
   private int bw;
   private class_rz bx;
   private int by;
   private float bz;
   private int bA;
   private float bB;
   protected ItemStack bm;
   protected int bn;
   protected int bo;
   private BlockPosition bC;

   public void P() {
      this.a(DamageSource.k, Float.MAX_VALUE);
   }

   public class_rz(World var1) {
      super(var1);
      this.bz();
      this.c(this.bV());
      this.i = true;
      this.aL = (float)((Math.random() + 1.0D) * 0.009999999776482582D);
      this.b(this.locX, this.locY, this.locZ);
      this.aK = (float)Math.random() * 12398.0F;
      this.yaw = (float)(Math.random() * 6.2831854820251465D);
      this.aO = this.yaw;
      this.P = 0.6F;
   }

   protected void i() {
      this.datawatcher.a((class_ke)as, (Object)Byte.valueOf((byte)0));
      this.datawatcher.a((class_ke)f, (Object)Integer.valueOf(0));
      this.datawatcher.a((class_ke)g, (Object)Boolean.valueOf(false));
      this.datawatcher.a((class_ke)h, (Object)Integer.valueOf(0));
      this.datawatcher.a((class_ke)c, (Object)Float.valueOf(1.0F));
   }

   protected void bz() {
      this.bY().b(class_ys.a);
      this.bY().b(class_ys.c);
      this.bY().b(class_ys.d);
      this.bY().b(class_ys.g);
   }

   protected void a(double var1, boolean var3, IBlockData var4, BlockPosition var5) {
      if(!this.ah()) {
         this.ai();
      }

      if(!this.world.E && this.fallDistance > 3.0F && var3) {
         float var6 = (float)MathHelper.f(this.fallDistance - 3.0F);
         if(var4.getMaterial() != Material.a) {
            double var7 = Math.min((double)(0.2F + var6 / 15.0F), 2.5D);
            int var9 = (int)(150.0D * var7);
            ((WorldServer)this.world).a(EnumParticle.BLOCK_DUST, this.locX, this.locY, this.locZ, var9, 0.0D, 0.0D, 0.0D, 0.15000000596046448D, new int[]{Block.j(var4)});
         }
      }

      super.a(var1, var3, var4, var5);
   }

   public boolean bA() {
      return false;
   }

   public void T() {
      this.aB = this.aC;
      super.T();
      this.world.C.a("livingEntityBaseTick");
      boolean var1 = this instanceof EntityHuman;
      if(this.at()) {
         if(this.au()) {
            this.a(DamageSource.e, 1.0F);
         } else if(var1 && !this.world.aj().a(this.bk())) {
            double var2 = this.world.aj().a((Entity)this) + this.world.aj().m();
            if(var2 < 0.0D) {
               this.a(DamageSource.e, (float)Math.max(1, MathHelper.c(-var2 * this.world.aj().n())));
            }
         }
      }

      if(this.af() || this.world.E) {
         this.W();
      }

      boolean var7 = var1 && ((EntityHuman)this).abilities.a;
      if(this.at()) {
         if(!this.a((Material)Material.h)) {
            this.j(300);
         } else {
            if(!this.bA() && !this.a(MobEffectList.m) && !var7) {
               this.j(this.d(this.aO()));
               if(this.aO() == -20) {
                  this.j(0);

                  for(int var3 = 0; var3 < 8; ++var3) {
                     float var4 = this.random.nextFloat() - this.random.nextFloat();
                     float var5 = this.random.nextFloat() - this.random.nextFloat();
                     float var6 = this.random.nextFloat() - this.random.nextFloat();
                     this.world.a(EnumParticle.WATER_BUBBLE, this.locX + (double)var4, this.locY + (double)var5, this.locZ + (double)var6, this.motX, this.motY, this.motZ, new int[0]);
                  }

                  this.a(DamageSource.f, 2.0F);
               }
            }

            if(!this.world.E && this.aH() && this.bx() instanceof class_rz) {
               this.p();
            }
         }

         if(!this.world.E) {
            BlockPosition var8 = new BlockPosition(this);
            if(!Objects.equal(this.bC, var8)) {
               this.bC = var8;
               this.b(var8);
            }
         }
      }

      if(this.at() && this.ag()) {
         this.W();
      }

      this.aI = this.aJ;
      if(this.ax > 0) {
         --this.ax;
      }

      if(this.noDamageTicks > 0 && !(this instanceof EntityPlayer)) {
         --this.noDamageTicks;
      }

      if(this.bP() <= 0.0F) {
         this.bB();
      }

      if(this.aS > 0) {
         --this.aS;
      } else {
         this.aR = null;
      }

      if(this.bx != null && !this.bx.at()) {
         this.bx = null;
      }

      if(this.bv != null) {
         if(!this.bv.at()) {
            this.a((class_rz)null);
         } else if(this.ticksLived - this.bw > 100) {
            this.a((class_rz)null);
         }
      }

      this.bK();
      this.aY = this.aX;
      this.aN = this.aM;
      this.aP = this.aO;
      this.lastYaw = this.yaw;
      this.lastPitch = this.pitch;
      this.world.C.b();
   }

   protected void b(BlockPosition var1) {
      int var2 = class_agn.a(class_agp.j, this);
      if(var2 > 0) {
         class_ags.a(this, this.world, var1, var2);
      }

   }

   public boolean m_() {
      return false;
   }

   protected void bB() {
      ++this.aA;
      if(this.aA == 20) {
         int var1;
         if(!this.world.E && (this.bD() || this.aS > 0 && this.bC() && this.world.U().b("doMobLoot"))) {
            var1 = this.b(this.aR);

            while(var1 > 0) {
               int var2 = class_rw.a(var1);
               var1 -= var2;
               this.world.a((Entity)(new class_rw(this.world, this.locX, this.locY, this.locZ, var2)));
            }
         }

         this.S();

         for(var1 = 0; var1 < 20; ++var1) {
            double var8 = this.random.nextGaussian() * 0.02D;
            double var4 = this.random.nextGaussian() * 0.02D;
            double var6 = this.random.nextGaussian() * 0.02D;
            this.world.a(EnumParticle.EXPLOSION_NORMAL, this.locX + (double)(this.random.nextFloat() * this.width * 2.0F) - (double)this.width, this.locY + (double)(this.random.nextFloat() * this.length), this.locZ + (double)(this.random.nextFloat() * this.width * 2.0F) - (double)this.width, var8, var4, var6, new int[0]);
         }
      }

   }

   protected boolean bC() {
      return !this.m_();
   }

   protected int d(int var1) {
      int var2 = class_agn.c(this);
      return var2 > 0 && this.random.nextInt(var2 + 1) > 0?var1:var1 - 1;
   }

   protected int b(EntityHuman var1) {
      return 0;
   }

   protected boolean bD() {
      return false;
   }

   public Random bE() {
      return this.random;
   }

   public class_rz bF() {
      return this.bv;
   }

   public int bG() {
      return this.bw;
   }

   public void a(class_rz var1) {
      this.bv = var1;
      this.bw = this.ticksLived;
   }

   public class_rz bH() {
      return this.bx;
   }

   public int bI() {
      return this.by;
   }

   public void z(Entity var1) {
      if(var1 instanceof class_rz) {
         this.bx = (class_rz)var1;
      } else {
         this.bx = null;
      }

      this.by = this.ticksLived;
   }

   public int bJ() {
      return this.aU;
   }

   protected void a_(ItemStack var1) {
      if(var1 != null) {
         class_nf var2 = class_ng.p;
         Item var3 = var1.b();
         if(var3 instanceof ItemArmor) {
            var2 = ((ItemArmor)var3).d().b();
         }

         this.a(var2, 1.0F, 1.0F);
      }
   }

   public void b(NBTTagCompound var1) {
      var1.a("Health", this.bP());
      var1.a("HurtTime", (short)this.ax);
      var1.a("HurtByTimestamp", this.bw);
      var1.a("DeathTime", (short)this.aA);
      var1.a("AbsorptionAmount", this.co());
      EnumInventorySlot[] var2 = EnumInventorySlot.values();
      int var3 = var2.length;

      int var4;
      EnumInventorySlot var5;
      ItemStack var6;
      for(var4 = 0; var4 < var3; ++var4) {
         var5 = var2[var4];
         var6 = this.a(var5);
         if(var6 != null) {
            this.bY().a(var6.a(var5));
         }
      }

      var1.a((String)"Attributes", (NBTTag)class_ys.a(this.bY()));
      var2 = EnumInventorySlot.values();
      var3 = var2.length;

      for(var4 = 0; var4 < var3; ++var4) {
         var5 = var2[var4];
         var6 = this.a(var5);
         if(var6 != null) {
            this.bY().b(var6.a(var5));
         }
      }

      if(!this.br.isEmpty()) {
         NBTTagList var7 = new NBTTagList();
         Iterator var8 = this.br.values().iterator();

         while(var8.hasNext()) {
            MobEffect var9 = (MobEffect)var8.next();
            var7.a((NBTTag)var9.a(new NBTTagCompound()));
         }

         var1.a((String)"ActiveEffects", (NBTTag)var7);
      }

   }

   public void a(NBTTagCompound var1) {
      this.n(var1.j("AbsorptionAmount"));
      if(var1.b("Attributes", 9) && this.world != null && !this.world.E) {
         class_ys.a(this.bY(), var1.c("Attributes", 10));
      }

      if(var1.b("ActiveEffects", 9)) {
         NBTTagList var2 = var1.c("ActiveEffects", 10);

         for(int var3 = 0; var3 < var2.c(); ++var3) {
            NBTTagCompound var4 = var2.b(var3);
            MobEffect var5 = MobEffect.b(var4);
            if(var5 != null) {
               this.br.put(var5.a(), var5);
            }
         }
      }

      if(var1.b("Health", 99)) {
         this.c(var1.j("Health"));
      }

      this.ax = var1.g("HurtTime");
      this.aA = var1.g("DeathTime");
      this.bw = var1.h("HurtByTimestamp");
      if(var1.b("Team", 8)) {
         String var6 = var1.l("Team");
         this.world.ad().a(this.getUniqueId().toString(), var6);
      }

   }

   protected void bK() {
      Iterator var1 = this.br.keySet().iterator();

      while(var1.hasNext()) {
         MobEffectType var2 = (MobEffectType)var1.next();
         MobEffect var3 = (MobEffect)this.br.get(var2);
         if(!var3.a(this)) {
            if(!this.world.E) {
               var1.remove();
               this.b(var3);
            }
         } else if(var3.b() % 600 == 0) {
            this.a(var3, false);
         }
      }

      if(this.bu) {
         if(!this.world.E) {
            this.F();
         }

         this.bu = false;
      }

      int var11 = ((Integer)this.datawatcher.a(f)).intValue();
      boolean var12 = ((Boolean)this.datawatcher.a(g)).booleanValue();
      if(var11 > 0) {
         boolean var4 = false;
         if(!this.aM()) {
            var4 = this.random.nextBoolean();
         } else {
            var4 = this.random.nextInt(15) == 0;
         }

         if(var12) {
            var4 &= this.random.nextInt(5) == 0;
         }

         if(var4 && var11 > 0) {
            double var5 = (double)(var11 >> 16 & 255) / 255.0D;
            double var7 = (double)(var11 >> 8 & 255) / 255.0D;
            double var9 = (double)(var11 >> 0 & 255) / 255.0D;
            this.world.a(var12?EnumParticle.SPELL_MOB_AMBIENT:EnumParticle.SPELL_MOB, this.locX + (this.random.nextDouble() - 0.5D) * (double)this.width, this.locY + this.random.nextDouble() * (double)this.length, this.locZ + (this.random.nextDouble() - 0.5D) * (double)this.width, var5, var7, var9, new int[0]);
         }
      }

   }

   protected void F() {
      if(this.br.isEmpty()) {
         this.bL();
         this.g(false);
      } else {
         Collection var1 = this.br.values();
         this.datawatcher.b(g, Boolean.valueOf(a(var1)));
         this.datawatcher.b(f, Integer.valueOf(class_aff.a(var1)));
         this.g(this.a(MobEffectList.n));
      }

   }

   public static boolean a(Collection var0) {
      Iterator var1 = var0.iterator();

      MobEffect var2;
      do {
         if(!var1.hasNext()) {
            return true;
         }

         var2 = (MobEffect)var1.next();
      } while(var2.d());

      return false;
   }

   protected void bL() {
      this.datawatcher.b(g, Boolean.valueOf(false));
      this.datawatcher.b(f, Integer.valueOf(0));
   }

   public void bM() {
      if(!this.world.E) {
         Iterator var1 = this.br.values().iterator();

         while(var1.hasNext()) {
            this.b((MobEffect)var1.next());
            var1.remove();
         }

      }
   }

   public Collection bN() {
      return this.br.values();
   }

   public boolean a(MobEffectType var1) {
      return this.br.containsKey(var1);
   }

   public MobEffect b(MobEffectType var1) {
      return (MobEffect)this.br.get(var1);
   }

   public void c(MobEffect var1) {
      if(this.d(var1)) {
         MobEffect var2 = (MobEffect)this.br.get(var1.a());
         if(var2 == null) {
            this.br.put(var1.a(), var1);
            this.a(var1);
         } else {
            var2.a(var1);
            this.a(var2, true);
         }

      }
   }

   public boolean d(MobEffect var1) {
      if(this.bZ() == EnumMonsterType.UNDEAD) {
         MobEffectType var2 = var1.a();
         if(var2 == MobEffectList.j || var2 == MobEffectList.s) {
            return false;
         }
      }

      return true;
   }

   public boolean bO() {
      return this.bZ() == EnumMonsterType.UNDEAD;
   }

   public MobEffect c(MobEffectType var1) {
      return (MobEffect)this.br.remove(var1);
   }

   public void d(MobEffectType var1) {
      MobEffect var2 = this.c(var1);
      if(var2 != null) {
         this.b(var2);
      }

   }

   protected void a(MobEffect var1) {
      this.bu = true;
      if(!this.world.E) {
         var1.a().b(this, this.bY(), var1.c());
      }

   }

   protected void a(MobEffect var1, boolean var2) {
      this.bu = true;
      if(var2 && !this.world.E) {
         MobEffectType var3 = var1.a();
         var3.a(this, this.bY(), var1.c());
         var3.b(this, this.bY(), var1.c());
      }

   }

   protected void b(MobEffect var1) {
      this.bu = true;
      if(!this.world.E) {
         var1.a().a(this, this.bY(), var1.c());
      }

   }

   public void b(float var1) {
      float var2 = this.bP();
      if(var2 > 0.0F) {
         this.c(var2 + var1);
      }

   }

   public final float bP() {
      return ((Float)this.datawatcher.a(c)).floatValue();
   }

   public void c(float var1) {
      this.datawatcher.b(c, Float.valueOf(MathHelper.a(var1, 0.0F, this.bV())));
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b((DamageSource)var1)) {
         return false;
      } else if(this.world.E) {
         return false;
      } else {
         this.aU = 0;
         if(this.bP() <= 0.0F) {
            return false;
         } else if(var1.o() && this.a(MobEffectList.l)) {
            return false;
         } else {
            if((var1 == DamageSource.o || var1 == DamageSource.p) && this.a(EnumInventorySlot.HEAD) != null) {
               this.a(EnumInventorySlot.HEAD).a((int)(var2 * 4.0F + this.random.nextFloat() * var2 * 2.0F), this);
               var2 *= 0.75F;
            }

            boolean var3 = false;
            if(var2 > 0.0F && this.d(var1)) {
               this.k(var2);
               if(var1.a()) {
                  var2 = 0.0F;
               } else {
                  var2 *= 0.33F;
                  if(var1.i() instanceof class_rz) {
                     ((class_rz)var1.i()).a(this, 0.5F, this.locX - var1.i().locX, this.locZ - var1.i().locZ);
                  }
               }

               var3 = true;
            }

            this.aF = 1.5F;
            boolean var4 = true;
            if((float)this.noDamageTicks > (float)this.aH / 2.0F) {
               if(var2 <= this.bb) {
                  return false;
               }

               this.d(var1, var2 - this.bb);
               this.bb = var2;
               var4 = false;
            } else {
               this.bb = var2;
               this.noDamageTicks = this.aH;
               this.d(var1, var2);
               this.ax = this.ay = 10;
            }

            this.az = 0.0F;
            Entity var5 = var1.j();
            if(var5 != null) {
               if(var5 instanceof class_rz) {
                  this.a((class_rz)var5);
               }

               if(var5 instanceof EntityHuman) {
                  this.aS = 100;
                  this.aR = (EntityHuman)var5;
               } else if(var5 instanceof class_wi) {
                  class_wi var6 = (class_wi)var5;
                  if(var6.cZ()) {
                     this.aS = 100;
                     this.aR = null;
                  }
               }
            }

            if(var4) {
               if(var3) {
                  this.world.a((Entity)this, (byte)29);
               } else if(var1 instanceof class_rc && ((class_rc)var1).x()) {
                  this.world.a((Entity)this, (byte)33);
               } else {
                  this.world.a((Entity)this, (byte)2);
               }

               if(var1 != DamageSource.f && (!var3 || var2 > 0.0F)) {
                  this.an();
               }

               if(var5 != null) {
                  double var10 = var5.locX - this.locX;

                  double var8;
                  for(var8 = var5.locZ - this.locZ; var10 * var10 + var8 * var8 < 1.0E-4D; var8 = (Math.random() - Math.random()) * 0.01D) {
                     var10 = (Math.random() - Math.random()) * 0.01D;
                  }

                  this.az = (float)(MathHelper.b(var8, var10) * 57.2957763671875D - (double)this.yaw);
                  this.a(var5, 0.4F, var10, var8);
               } else {
                  this.az = (float)((int)(Math.random() * 2.0D) * 180);
               }
            }

            if(this.bP() <= 0.0F) {
               class_nf var11 = this.bR();
               if(var4 && var11 != null) {
                  this.a(var11, this.cc(), this.cd());
               }

               this.a(var1);
            } else if(var4) {
               this.c(var1);
            }

            return !var3 || var2 > 0.0F;
         }
      }
   }

   protected void c(DamageSource var1) {
      class_nf var2 = this.bQ();
      if(var2 != null) {
         this.a(var2, this.cc(), this.cd());
      }

   }

   private boolean d(DamageSource var1) {
      if(!var1.e() && this.cz()) {
         Vec3D var2 = var1.v();
         if(var2 != null) {
            Vec3D var3 = this.f(1.0F);
            Vec3D var4 = var2.a(new Vec3D(this.locX, this.locY, this.locZ)).a();
            var4 = new Vec3D(var4.b, 0.0D, var4.d);
            if(var4.b(var3) < 0.0D) {
               return true;
            }
         }
      }

      return false;
   }

   public void b(ItemStack var1) {
      this.a(class_ng.cQ, 0.8F, 0.8F + this.world.r.nextFloat() * 0.4F);

      for(int var2 = 0; var2 < 5; ++var2) {
         Vec3D var3 = new Vec3D(((double)this.random.nextFloat() - 0.5D) * 0.1D, Math.random() * 0.1D + 0.1D, 0.0D);
         var3 = var3.a(-this.pitch * 0.017453292F);
         var3 = var3.b(-this.yaw * 0.017453292F);
         double var4 = (double)(-this.random.nextFloat()) * 0.6D - 0.3D;
         Vec3D var6 = new Vec3D(((double)this.random.nextFloat() - 0.5D) * 0.3D, var4, 0.6D);
         var6 = var6.a(-this.pitch * 0.017453292F);
         var6 = var6.b(-this.yaw * 0.017453292F);
         var6 = var6.b(this.locX, this.locY + (double)this.bm(), this.locZ);
         this.world.a(EnumParticle.ITEM_CRACK, var6.b, var6.c, var6.d, var3.b, var3.c + 0.05D, var3.d, new int[]{Item.a(var1.b())});
      }

   }

   public void a(DamageSource var1) {
      if(!this.aT) {
         Entity var2 = var1.j();
         class_rz var3 = this.bU();
         if(this.ba >= 0 && var3 != null) {
            var3.b(this, this.ba);
         }

         if(var2 != null) {
            var2.b(this);
         }

         this.aT = true;
         this.bT().g();
         if(!this.world.E) {
            int var4 = 0;
            if(var2 instanceof EntityHuman) {
               var4 = class_agn.h((class_rz)var2);
            }

            if(this.bC() && this.world.U().b("doMobLoot")) {
               boolean var5 = this.aS > 0;
               this.a(var5, var4, var1);
            }
         }

         this.world.a((Entity)this, (byte)3);
      }
   }

   protected void a(boolean var1, int var2, DamageSource var3) {
      this.b(var1, var2);
      this.a(var1, var2);
   }

   protected void a(boolean var1, int var2) {
   }

   public void a(Entity var1, float var2, double var3, double var5) {
      if(this.random.nextDouble() >= this.a(class_ys.c).e()) {
         this.ai = true;
         float var7 = MathHelper.a(var3 * var3 + var5 * var5);
         this.motX /= 2.0D;
         this.motZ /= 2.0D;
         this.motX -= var3 / (double)var7 * (double)var2;
         this.motZ -= var5 / (double)var7 * (double)var2;
         if(this.onGround) {
            this.motY /= 2.0D;
            this.motY += (double)var2;
            if(this.motY > 0.4000000059604645D) {
               this.motY = 0.4000000059604645D;
            }
         }

      }
   }

   protected class_nf bQ() {
      return class_ng.bD;
   }

   protected class_nf bR() {
      return class_ng.by;
   }

   protected class_nf e(int var1) {
      return var1 > 4?class_ng.bw:class_ng.bE;
   }

   protected void b(boolean var1, int var2) {
   }

   public boolean n_() {
      int var1 = MathHelper.c(this.locX);
      int var2 = MathHelper.c(this.bk().b);
      int var3 = MathHelper.c(this.locZ);
      if(this instanceof EntityHuman && ((EntityHuman)this).y()) {
         return false;
      } else {
         BlockPosition var4 = new BlockPosition(var1, var2, var3);
         IBlockData var5 = this.world.getType(var4);
         Block var6 = var5.getBlock();
         return var6 != Blocks.au && var6 != Blocks.bn?var6 instanceof class_apf && this.a(var4, var5):true;
      }
   }

   private boolean a(BlockPosition var1, IBlockData var2) {
      if(((Boolean)var2.get(class_apf.b)).booleanValue()) {
         IBlockData var3 = this.world.getType(var1.b());
         if(var3.getBlock() == Blocks.au && var3.get(class_amj.a) == var2.get(class_apf.a)) {
            return true;
         }
      }

      return false;
   }

   public boolean at() {
      return !this.dead && this.bP() > 0.0F;
   }

   public void e(float var1, float var2) {
      super.e(var1, var2);
      MobEffect var3 = this.b(MobEffectList.h);
      float var4 = var3 == null?0.0F:(float)(var3.c() + 1);
      int var5 = MathHelper.f((var1 - 3.0F - var4) * var2);
      if(var5 > 0) {
         this.a(this.e(var5), 1.0F, 1.0F);
         this.a(DamageSource.i, (float)var5);
         int var6 = MathHelper.c(this.locX);
         int var7 = MathHelper.c(this.locY - 0.20000000298023224D);
         int var8 = MathHelper.c(this.locZ);
         IBlockData var9 = this.world.getType(new BlockPosition(var6, var7, var8));
         if(var9.getMaterial() != Material.a) {
            class_aoo var10 = var9.getBlock().w();
            this.a(var10.g(), var10.a() * 0.5F, var10.b() * 0.75F);
         }
      }

   }

   public int bS() {
      class_sl var1 = this.a(class_ys.g);
      return MathHelper.c(var1.e());
   }

   protected void j(float var1) {
   }

   protected void k(float var1) {
   }

   protected float b(DamageSource var1, float var2) {
      if(!var1.e()) {
         this.j(var2);
         var2 = class_qz.a(var2, (float)this.bS());
      }

      return var2;
   }

   protected float c(DamageSource var1, float var2) {
      if(var1.h()) {
         return var2;
      } else {
         int var3;
         if(this.a(MobEffectList.k) && var1 != DamageSource.k) {
            var3 = (this.b(MobEffectList.k).c() + 1) * 5;
            int var4 = 25 - var3;
            float var5 = var2 * (float)var4;
            var2 = var5 / 25.0F;
         }

         if(var2 <= 0.0F) {
            return 0.0F;
         } else {
            var3 = class_agn.a(this.aE(), var1);
            if(var3 > 0) {
               var2 = class_qz.b(var2, (float)var3);
            }

            return var2;
         }
      }
   }

   protected void d(DamageSource var1, float var2) {
      if(!this.b((DamageSource)var1)) {
         var2 = this.b(var1, var2);
         var2 = this.c(var1, var2);
         float var3 = var2;
         var2 = Math.max(var2 - this.co(), 0.0F);
         this.n(this.co() - (var3 - var2));
         if(var2 != 0.0F) {
            float var4 = this.bP();
            this.c(var4 - var2);
            this.bT().a(var1, var4, var2);
            this.n(this.co() - var2);
         }
      }
   }

   public CombatTracker bT() {
      return this.bq;
   }

   public class_rz bU() {
      return (class_rz)(this.bq.c() != null?this.bq.c():(this.aR != null?this.aR:(this.bv != null?this.bv:null)));
   }

   public final float bV() {
      return (float)this.a(class_ys.a).e();
   }

   public final int bW() {
      return ((Integer)this.datawatcher.a(h)).intValue();
   }

   public final void k(int var1) {
      this.datawatcher.b(h, Integer.valueOf(var1));
   }

   private int o() {
      return this.a(MobEffectList.c)?6 - (1 + this.b(MobEffectList.c).c()):(this.a(MobEffectList.d)?6 + (1 + this.b(MobEffectList.d).c()) * 2:6);
   }

   public void a(EnumHand var1) {
      if(!this.at || this.av >= this.o() / 2 || this.av < 0) {
         this.av = -1;
         this.at = true;
         this.au = var1;
         if(this.world instanceof WorldServer) {
            ((WorldServer)this.world).v().a((Entity)this, (Packet)(new PacketPlayOutAnimation(this, var1 == EnumHand.MAIN_HAND?0:3)));
         }
      }

   }

   protected void X() {
      this.a(DamageSource.k, 4.0F);
   }

   protected void bX() {
      int var1 = this.o();
      if(this.at) {
         ++this.av;
         if(this.av >= var1) {
            this.av = 0;
            this.at = false;
         }
      } else {
         this.av = 0;
      }

      this.aC = (float)this.av / (float)var1;
   }

   public class_sl a(class_sk var1) {
      return this.bY().a(var1);
   }

   public AttributeMapBase bY() {
      if(this.bp == null) {
         this.bp = new class_sq();
      }

      return this.bp;
   }

   public EnumMonsterType bZ() {
      return EnumMonsterType.UNDEFINED;
   }

   public ItemStack ca() {
      return this.a(EnumInventorySlot.MAINHAND);
   }

   public ItemStack cb() {
      return this.a(EnumInventorySlot.OFFHAND);
   }

   public ItemStack b(EnumHand var1) {
      if(var1 == EnumHand.MAIN_HAND) {
         return this.a(EnumInventorySlot.MAINHAND);
      } else if(var1 == EnumHand.OFF_HAND) {
         return this.a(EnumInventorySlot.OFFHAND);
      } else {
         throw new IllegalArgumentException("Invalid hand " + var1);
      }
   }

   public void a(EnumHand var1, ItemStack var2) {
      if(var1 == EnumHand.MAIN_HAND) {
         this.a(EnumInventorySlot.MAINHAND, var2);
      } else {
         if(var1 != EnumHand.OFF_HAND) {
            throw new IllegalArgumentException("Invalid hand " + var1);
         }

         this.a(EnumInventorySlot.OFFHAND, var2);
      }

   }

   public abstract Iterable aE();

   public abstract ItemStack a(EnumInventorySlot var1);

   public abstract void a(EnumInventorySlot var1, ItemStack var2);

   public void e(boolean var1) {
      super.e(var1);
      class_sl var2 = this.a(class_ys.d);
      if(var2.a(a) != null) {
         var2.c(b);
      }

      if(var1) {
         var2.b(b);
      }

   }

   protected float cc() {
      return 1.0F;
   }

   protected float cd() {
      return this.m_()?(this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.5F:(this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F;
   }

   protected boolean ce() {
      return this.bP() <= 0.0F;
   }

   public void A(Entity var1) {
      if(var1 instanceof EntityBoat) {
         double var34 = (double)(this.width / 2.0F + var1.width / 2.0F) + 0.4D;
         float var4 = -MathHelper.a(-this.yaw * 0.017453292F - 3.1415927F);
         float var35 = -MathHelper.b(-this.yaw * 0.017453292F - 3.1415927F);
         double var6 = Math.abs(var4) > Math.abs(var35)?var34 / (double)Math.abs(var4):var34 / (double)Math.abs(var35);
         double var8 = this.locX + (double)var4 * var6;
         double var36 = this.locZ + (double)var35 * var6;
         this.b(var8, var1.locY + (double)var1.length + 0.001D, var36);
         if(this.world.b(this.bk())) {
            this.b(var8, var1.locY + (double)var1.length + 1.001D, var36);
            if(this.world.b(this.bk())) {
               this.b(var1.locX, var1.locY + (double)this.length + 0.001D, var1.locZ);
            }
         }
      } else {
         double var3 = var1.locX;
         double var5 = var1.bk().b + (double)var1.length;
         double var7 = var1.locZ;
         EnumDirection var9 = var1.bi();
         EnumDirection var10 = var9.e();
         int[][] var11 = new int[][]{{0, 1}, {0, -1}, {-1, 1}, {-1, -1}, {1, 1}, {1, -1}, {-1, 0}, {1, 0}, {0, 1}};
         double var12 = Math.floor(this.locX) + 0.5D;
         double var14 = Math.floor(this.locZ) + 0.5D;
         double var16 = this.bk().d - this.bk().a;
         double var18 = this.bk().f - this.bk().c;
         AxisAlignedBB var20 = new AxisAlignedBB(var12 - var16 / 2.0D, this.bk().b, var14 - var18 / 2.0D, var12 + var16 / 2.0D, this.bk().e, var14 + var18 / 2.0D);
         int[][] var21 = var11;
         int var22 = var11.length;

         for(int var23 = 0; var23 < var22; ++var23) {
            int[] var24 = var21[var23];
            double var25 = (double)(var9.g() * var24[0] + var10.g() * var24[1]);
            double var27 = (double)(var9.i() * var24[0] + var10.i() * var24[1]);
            double var29 = var12 + var25;
            double var31 = var14 + var27;
            AxisAlignedBB var2 = var20.c(var25, 1.0D, var27);
            if(!this.world.b(var2)) {
               if(this.world.getType(new BlockPosition(var29, this.locY, var31)).q()) {
                  this.a(var29, this.locY + 1.0D, var31);
                  return;
               }

               BlockPosition var33 = new BlockPosition(var29, this.locY - 1.0D, var31);
               if(this.world.getType(var33).q() || this.world.getType(var33).getMaterial() == Material.h) {
                  var3 = var29;
                  var5 = this.locY + 1.0D;
                  var7 = var31;
               }
            } else if(!this.world.b(var2.c(0.0D, 1.0D, 0.0D)) && this.world.getType(new BlockPosition(var29, this.locY + 1.0D, var31)).q()) {
               var3 = var29;
               var5 = this.locY + 2.0D;
               var7 = var31;
            }
         }

         this.a(var3, var5, var7);
      }
   }

   protected float cf() {
      return 0.42F;
   }

   protected void cg() {
      this.motY = (double)this.cf();
      if(this.a(MobEffectList.h)) {
         this.motY += (double)((float)(this.b(MobEffectList.h).c() + 1) * 0.1F);
      }

      if(this.aK()) {
         float var1 = this.yaw * 0.017453292F;
         this.motX -= (double)(MathHelper.a(var1) * 0.2F);
         this.motZ += (double)(MathHelper.b(var1) * 0.2F);
      }

      this.ai = true;
   }

   protected void ch() {
      this.motY += 0.03999999910593033D;
   }

   protected void ci() {
      this.motY += 0.03999999910593033D;
   }

   public void g(float var1, float var2) {
      double var3;
      float var7;
      double var20;
      if(this.cn() || this.bw()) {
         float var5;
         float var6;
         if(!this.ah() || this instanceof EntityHuman && ((EntityHuman)this).abilities.b) {
            if(!this.am() || this instanceof EntityHuman && ((EntityHuman)this).abilities.b) {
               if(this.cA()) {
                  if(this.motY > -0.5D) {
                     this.fallDistance = 1.0F;
                  }

                  Vec3D var17 = this.aA();
                  float var4 = this.pitch * 0.017453292F;
                  var20 = Math.sqrt(var17.b * var17.b + var17.d * var17.d);
                  double var21 = Math.sqrt(this.motX * this.motX + this.motZ * this.motZ);
                  double var9 = var17.b();
                  float var11 = MathHelper.b(var4);
                  var11 = (float)((double)var11 * (double)var11 * Math.min(1.0D, var9 / 0.4D));
                  this.motY += -0.08D + (double)var11 * 0.06D;
                  double var12;
                  if(this.motY < 0.0D && var20 > 0.0D) {
                     var12 = this.motY * -0.1D * (double)var11;
                     this.motY += var12;
                     this.motX += var17.b * var12 / var20;
                     this.motZ += var17.d * var12 / var20;
                  }

                  if(var4 < 0.0F) {
                     var12 = var21 * (double)(-MathHelper.a(var4)) * 0.04D;
                     this.motY += var12 * 3.2D;
                     this.motX -= var17.b * var12 / var20;
                     this.motZ -= var17.d * var12 / var20;
                  }

                  if(var20 > 0.0D) {
                     this.motX += (var17.b / var20 * var21 - this.motX) * 0.1D;
                     this.motZ += (var17.d / var20 * var21 - this.motZ) * 0.1D;
                  }

                  this.motX *= 0.9900000095367432D;
                  this.motY *= 0.9800000190734863D;
                  this.motZ *= 0.9900000095367432D;
                  this.d(this.motX, this.motY, this.motZ);
                  if(this.positionChanged && !this.world.E) {
                     var12 = Math.sqrt(this.motX * this.motX + this.motZ * this.motZ);
                     double var14 = var21 - var12;
                     float var16 = (float)(var14 * 10.0D - 3.0D);
                     if(var16 > 0.0F) {
                        this.a(this.e((int)var16), 1.0F, 1.0F);
                        this.a(DamageSource.j, var16);
                     }
                  }

                  if(this.onGround && !this.world.E) {
                     this.b(7, false);
                  }
               } else {
                  float var18 = 0.91F;
                  BlockPosition.class_b_in_class_cj var19 = BlockPosition.class_b_in_class_cj.c(this.locX, this.bk().b - 1.0D, this.locZ);
                  if(this.onGround) {
                     var18 = this.world.getType(var19).getBlock().z * 0.91F;
                  }

                  var5 = 0.16277136F / (var18 * var18 * var18);
                  if(this.onGround) {
                     var6 = this.cj() * var5;
                  } else {
                     var6 = this.aQ;
                  }

                  this.a(var1, var2, var6);
                  var18 = 0.91F;
                  if(this.onGround) {
                     var18 = this.world.getType(var19.d(this.locX, this.bk().b - 1.0D, this.locZ)).getBlock().z * 0.91F;
                  }

                  if(this.n_()) {
                     var7 = 0.15F;
                     this.motX = MathHelper.a(this.motX, (double)(-var7), (double)var7);
                     this.motZ = MathHelper.a(this.motZ, (double)(-var7), (double)var7);
                     this.fallDistance = 0.0F;
                     if(this.motY < -0.15D) {
                        this.motY = -0.15D;
                     }

                     boolean var8 = this.aJ() && this instanceof EntityHuman;
                     if(var8 && this.motY < 0.0D) {
                        this.motY = 0.0D;
                     }
                  }

                  this.d(this.motX, this.motY, this.motZ);
                  if(this.positionChanged && this.n_()) {
                     this.motY = 0.2D;
                  }

                  if(this.a(MobEffectList.y)) {
                     this.motY += (0.05D * (double)(this.b(MobEffectList.y).c() + 1) - this.motY) * 0.2D;
                  } else {
                     var19.d(this.locX, 0.0D, this.locZ);
                     if(this.world.E && (!this.world.e((BlockPosition)var19) || !this.world.f((BlockPosition)var19).p())) {
                        if(this.locY > 0.0D) {
                           this.motY = -0.1D;
                        } else {
                           this.motY = 0.0D;
                        }
                     } else {
                        this.motY -= 0.08D;
                     }
                  }

                  this.motY *= 0.9800000190734863D;
                  this.motX *= (double)var18;
                  this.motZ *= (double)var18;
                  var19.t();
               }
            } else {
               var3 = this.locY;
               this.a(var1, var2, 0.02F);
               this.d(this.motX, this.motY, this.motZ);
               this.motX *= 0.5D;
               this.motY *= 0.5D;
               this.motZ *= 0.5D;
               this.motY -= 0.02D;
               if(this.positionChanged && this.c(this.motX, this.motY + 0.6000000238418579D - this.locY + var3, this.motZ)) {
                  this.motY = 0.30000001192092896D;
               }
            }
         } else {
            var3 = this.locY;
            var5 = 0.8F;
            var6 = 0.02F;
            var7 = (float)class_agn.d(this);
            if(var7 > 3.0F) {
               var7 = 3.0F;
            }

            if(!this.onGround) {
               var7 *= 0.5F;
            }

            if(var7 > 0.0F) {
               var5 += (0.54600006F - var5) * var7 / 3.0F;
               var6 += (this.cj() - var6) * var7 / 3.0F;
            }

            this.a(var1, var2, var6);
            this.d(this.motX, this.motY, this.motZ);
            this.motX *= (double)var5;
            this.motY *= 0.800000011920929D;
            this.motZ *= (double)var5;
            this.motY -= 0.02D;
            if(this.positionChanged && this.c(this.motX, this.motY + 0.6000000238418579D - this.locY + var3, this.motZ)) {
               this.motY = 0.30000001192092896D;
            }
         }
      }

      this.aE = this.aF;
      var3 = this.locX - this.lastX;
      var20 = this.locZ - this.lastZ;
      var7 = MathHelper.a(var3 * var3 + var20 * var20) * 4.0F;
      if(var7 > 1.0F) {
         var7 = 1.0F;
      }

      this.aF += (var7 - this.aF) * 0.4F;
      this.aG += this.aF;
   }

   public float cj() {
      return this.bz;
   }

   public void l(float var1) {
      this.bz = var1;
   }

   public boolean B(Entity var1) {
      this.z(var1);
      return false;
   }

   public boolean ck() {
      return false;
   }

   public void m() {
      super.m();
      this.ct();
      if(!this.world.E) {
         int var1 = this.bW();
         if(var1 > 0) {
            if(this.aw <= 0) {
               this.aw = 20 * (30 - var1);
            }

            --this.aw;
            if(this.aw <= 0) {
               this.k(var1 - 1);
            }
         }

         EnumInventorySlot[] var2 = EnumInventorySlot.values();
         int var3 = var2.length;

         for(int var4 = 0; var4 < var3; ++var4) {
            EnumInventorySlot var5 = var2[var4];
            ItemStack var6;
            switch(class_rz.SyntheticClass_1.a[var5.a().ordinal()]) {
            case 1:
               var6 = this.bs[var5.b()];
               break;
            case 2:
               var6 = this.bt[var5.b()];
               break;
            default:
               continue;
            }

            ItemStack var7 = this.a(var5);
            if(!ItemStack.b(var7, var6)) {
               ((WorldServer)this.world).v().a((Entity)this, (Packet)(new PacketPlayOutEntityEquipment(this.getId(), var5, var7)));
               if(var6 != null) {
                  this.bY().a(var6.a(var5));
               }

               if(var7 != null) {
                  this.bY().b(var7.a(var5));
               }

               switch(class_rz.SyntheticClass_1.a[var5.a().ordinal()]) {
               case 1:
                  this.bs[var5.b()] = var7 == null?null:var7.k();
                  break;
               case 2:
                  this.bt[var5.b()] = var7 == null?null:var7.k();
               }
            }
         }

         if(this.ticksLived % 20 == 0) {
            this.bT().g();
         }

         if(!this.ar) {
            boolean var10 = this.a(MobEffectList.x);
            if(this.i(6) != var10) {
               this.b(6, var10);
            }
         }
      }

      this.n();
      double var9 = this.locX - this.lastX;
      double var11 = this.locZ - this.lastZ;
      float var12 = (float)(var9 * var9 + var11 * var11);
      float var13 = this.aM;
      float var14 = 0.0F;
      this.aV = this.aW;
      float var8 = 0.0F;
      if(var12 > 0.0025000002F) {
         var8 = 1.0F;
         var14 = (float)Math.sqrt((double)var12) * 3.0F;
         var13 = (float)MathHelper.b(var11, var9) * 57.295776F - 90.0F;
      }

      if(this.aC > 0.0F) {
         var13 = this.yaw;
      }

      if(!this.onGround) {
         var8 = 0.0F;
      }

      this.aW += (var8 - this.aW) * 0.3F;
      this.world.C.a("headTurn");
      var14 = this.h(var13, var14);
      this.world.C.b();
      this.world.C.a("rangeChecks");

      while(this.yaw - this.lastYaw < -180.0F) {
         this.lastYaw -= 360.0F;
      }

      while(this.yaw - this.lastYaw >= 180.0F) {
         this.lastYaw += 360.0F;
      }

      while(this.aM - this.aN < -180.0F) {
         this.aN -= 360.0F;
      }

      while(this.aM - this.aN >= 180.0F) {
         this.aN += 360.0F;
      }

      while(this.pitch - this.lastPitch < -180.0F) {
         this.lastPitch -= 360.0F;
      }

      while(this.pitch - this.lastPitch >= 180.0F) {
         this.lastPitch += 360.0F;
      }

      while(this.aO - this.aP < -180.0F) {
         this.aP -= 360.0F;
      }

      while(this.aO - this.aP >= 180.0F) {
         this.aP += 360.0F;
      }

      this.world.C.b();
      this.aX += var14;
      if(this.cA()) {
         ++this.bo;
      } else {
         this.bo = 0;
      }

   }

   protected float h(float var1, float var2) {
      float var3 = MathHelper.g(var1 - this.aM);
      this.aM += var3 * 0.3F;
      float var4 = MathHelper.g(this.yaw - this.aM);
      boolean var5 = var4 < -90.0F || var4 >= 90.0F;
      if(var4 < -75.0F) {
         var4 = -75.0F;
      }

      if(var4 >= 75.0F) {
         var4 = 75.0F;
      }

      this.aM = this.yaw - var4;
      if(var4 * var4 > 2500.0F) {
         this.aM += var4 * 0.2F;
      }

      if(var5) {
         var2 *= -1.0F;
      }

      return var2;
   }

   public void n() {
      if(this.bA > 0) {
         --this.bA;
      }

      if(this.bg > 0 && !this.bw()) {
         double var1 = this.locX + (this.bh - this.locX) / (double)this.bg;
         double var3 = this.locY + (this.bi - this.locY) / (double)this.bg;
         double var5 = this.locZ + (this.bj - this.locZ) / (double)this.bg;
         double var7 = MathHelper.g(this.bk - (double)this.yaw);
         this.yaw = (float)((double)this.yaw + var7 / (double)this.bg);
         this.pitch = (float)((double)this.pitch + (this.bl - (double)this.pitch) / (double)this.bg);
         --this.bg;
         this.b(var1, var3, var5);
         this.b(this.yaw, this.pitch);
      } else if(!this.cn()) {
         this.motX *= 0.98D;
         this.motY *= 0.98D;
         this.motZ *= 0.98D;
      }

      if(Math.abs(this.motX) < 0.003D) {
         this.motX = 0.0D;
      }

      if(Math.abs(this.motY) < 0.003D) {
         this.motY = 0.0D;
      }

      if(Math.abs(this.motZ) < 0.003D) {
         this.motZ = 0.0D;
      }

      this.world.C.a("ai");
      if(this.ce()) {
         this.bc = false;
         this.bd = 0.0F;
         this.be = 0.0F;
         this.bf = 0.0F;
      } else if(this.cn()) {
         this.world.C.a("newAi");
         this.cl();
         this.world.C.b();
      }

      this.world.C.b();
      this.world.C.a("jump");
      if(this.bc) {
         if(this.ah()) {
            this.ch();
         } else if(this.am()) {
            this.ci();
         } else if(this.onGround && this.bA == 0) {
            this.cg();
            this.bA = 10;
         }
      } else {
         this.bA = 0;
      }

      this.world.C.b();
      this.world.C.a("travel");
      this.bd *= 0.98F;
      this.be *= 0.98F;
      this.bf *= 0.9F;
      this.r();
      this.g(this.bd, this.be);
      this.world.C.b();
      this.world.C.a("push");
      this.cm();
      this.world.C.b();
   }

   private void r() {
      boolean var1 = this.i(7);
      if(var1 && !this.onGround && !this.aH()) {
         ItemStack var2 = this.a(EnumInventorySlot.CHEST);
         if(var2 != null && var2.b() == Items.cR && class_acw.d(var2)) {
            var1 = true;
            if(!this.world.E && (this.bo + 1) % 20 == 0) {
               var2.a(1, (class_rz)this);
            }
         } else {
            var1 = false;
         }
      } else {
         var1 = false;
      }

      if(!this.world.E) {
         this.b(7, var1);
      }

   }

   protected void cl() {
   }

   protected void cm() {
      List var1 = this.world.a((Entity)this, (AxisAlignedBB)this.bk(), (Predicate)class_ru.a(this));
      if(!var1.isEmpty()) {
         for(int var2 = 0; var2 < var1.size(); ++var2) {
            Entity var3 = (Entity)var1.get(var2);
            this.C(var3);
         }
      }

   }

   protected void C(Entity var1) {
      var1.i(this);
   }

   public void p() {
      Entity var1 = this.bx();
      super.p();
      if(var1 != null && var1 != this.bx() && !this.world.E) {
         this.A(var1);
      }

   }

   public void av() {
      super.av();
      this.aV = this.aW;
      this.aW = 0.0F;
      this.fallDistance = 0.0F;
   }

   public void k(boolean var1) {
      this.bc = var1;
   }

   public void a(Entity var1, int var2) {
      if(!var1.dead && !this.world.E) {
         EntityTracker var3 = ((WorldServer)this.world).v();
         if(var1 instanceof class_yc) {
            var3.a((Entity)var1, (Packet)(new PacketPlayOutCollect(var1.getId(), this.getId())));
         }

         if(var1 instanceof class_zl) {
            var3.a((Entity)var1, (Packet)(new PacketPlayOutCollect(var1.getId(), this.getId())));
         }

         if(var1 instanceof class_rw) {
            var3.a((Entity)var1, (Packet)(new PacketPlayOutCollect(var1.getId(), this.getId())));
         }
      }

   }

   public boolean D(Entity var1) {
      return this.world.a(new Vec3D(this.locX, this.locY + (double)this.bm(), this.locZ), new Vec3D(var1.locX, var1.locY + (double)var1.bm(), var1.locZ), false, true, false) == null;
   }

   public Vec3D aA() {
      return this.f(1.0F);
   }

   public Vec3D f(float var1) {
      if(var1 == 1.0F) {
         return this.f(this.pitch, this.aO);
      } else {
         float var2 = this.lastPitch + (this.pitch - this.lastPitch) * var1;
         float var3 = this.aP + (this.aO - this.aP) * var1;
         return this.f(var2, var3);
      }
   }

   public boolean cn() {
      return !this.world.E;
   }

   public boolean ao() {
      return !this.dead;
   }

   public boolean ap() {
      return !this.dead;
   }

   protected void an() {
      this.velocityChanged = this.random.nextDouble() >= this.a(class_ys.c).e();
   }

   public float aR() {
      return this.aO;
   }

   public void h(float var1) {
      this.aO = var1;
   }

   public void i(float var1) {
      this.aM = var1;
   }

   public float co() {
      return this.bB;
   }

   public void n(float var1) {
      if(var1 < 0.0F) {
         var1 = 0.0F;
      }

      this.bB = var1;
   }

   public void j() {
   }

   public void k() {
   }

   protected void cp() {
      this.bu = true;
   }

   public boolean cr() {
      return (((Byte)this.datawatcher.a(as)).byteValue() & 1) > 0;
   }

   public EnumHand cs() {
      return (((Byte)this.datawatcher.a(as)).byteValue() & 2) > 0?EnumHand.OFF_HAND:EnumHand.MAIN_HAND;
   }

   protected void ct() {
      if(this.cr()) {
         ItemStack var1 = this.b(this.cs());
         if(var1 == this.bm) {
            if(this.cv() <= 25 && this.cv() % 4 == 0) {
               this.a((ItemStack)this.bm, 5);
            }

            if(--this.bn == 0 && !this.world.E) {
               this.v();
            }
         } else {
            this.cy();
         }
      }

   }

   public void c(EnumHand var1) {
      ItemStack var2 = this.b(var1);
      if(var2 != null && !this.cr()) {
         this.bm = var2;
         this.bn = var2.l();
         if(!this.world.E) {
            int var3 = 1;
            if(var1 == EnumHand.OFF_HAND) {
               var3 |= 2;
            }

            this.datawatcher.b(as, Byte.valueOf((byte)var3));
         }

      }
   }

   public void a(class_ke var1) {
      super.a(var1);
      if(as.equals(var1) && this.world.E) {
         if(this.cr() && this.bm == null) {
            this.bm = this.b(this.cs());
            if(this.bm != null) {
               this.bn = this.bm.l();
            }
         } else if(!this.cr() && this.bm != null) {
            this.bm = null;
            this.bn = 0;
         }
      }

   }

   protected void a(ItemStack var1, int var2) {
      if(var1 != null && this.cr()) {
         if(var1.m() == EnumAnimation.DRINK) {
            this.a(class_ng.bz, 0.5F, this.world.r.nextFloat() * 0.1F + 0.9F);
         }

         if(var1.m() == EnumAnimation.EAT) {
            for(int var3 = 0; var3 < var2; ++var3) {
               Vec3D var4 = new Vec3D(((double)this.random.nextFloat() - 0.5D) * 0.1D, Math.random() * 0.1D + 0.1D, 0.0D);
               var4 = var4.a(-this.pitch * 0.017453292F);
               var4 = var4.b(-this.yaw * 0.017453292F);
               double var5 = (double)(-this.random.nextFloat()) * 0.6D - 0.3D;
               Vec3D var7 = new Vec3D(((double)this.random.nextFloat() - 0.5D) * 0.3D, var5, 0.6D);
               var7 = var7.a(-this.pitch * 0.017453292F);
               var7 = var7.b(-this.yaw * 0.017453292F);
               var7 = var7.b(this.locX, this.locY + (double)this.bm(), this.locZ);
               if(var1.f()) {
                  this.world.a(EnumParticle.ITEM_CRACK, var7.b, var7.c, var7.d, var4.b, var4.c + 0.05D, var4.d, new int[]{Item.a(var1.b()), var1.i()});
               } else {
                  this.world.a(EnumParticle.ITEM_CRACK, var7.b, var7.c, var7.d, var4.b, var4.c + 0.05D, var4.d, new int[]{Item.a(var1.b())});
               }
            }

            this.a(class_ng.bA, 0.5F + 0.5F * (float)this.random.nextInt(2), (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
         }

      }
   }

   protected void v() {
      if(this.bm != null && this.cr()) {
         this.a((ItemStack)this.bm, 16);
         ItemStack var1 = this.bm.a(this.world, this);
         if(var1 != null && var1.b == 0) {
            var1 = null;
         }

         this.a(this.cs(), var1);
         this.cy();
      }

   }

   public ItemStack cu() {
      return this.bm;
   }

   public int cv() {
      return this.bn;
   }

   public int cw() {
      return this.cr()?this.bm.l() - this.cv():0;
   }

   public void cx() {
      if(this.bm != null) {
         this.bm.a(this.world, this, this.cv());
      }

      this.cy();
   }

   public void cy() {
      if(!this.world.E) {
         this.datawatcher.b(as, Byte.valueOf((byte)0));
      }

      this.bm = null;
      this.bn = 0;
   }

   public boolean cz() {
      if(this.cr() && this.bm != null) {
         Item var1 = this.bm.b();
         return var1.f(this.bm) != EnumAnimation.BLOCK?false:var1.e(this.bm) - this.bn >= 5;
      } else {
         return false;
      }
   }

   public boolean cA() {
      return this.i(7);
   }

   public boolean k(double var1, double var3, double var5) {
      double var7 = this.locX;
      double var9 = this.locY;
      double var11 = this.locZ;
      this.locX = var1;
      this.locY = var3;
      this.locZ = var5;
      boolean var13 = false;
      BlockPosition var14 = new BlockPosition(this);
      World var15 = this.world;
      Random var16 = this.bE();
      if(var15.e(var14)) {
         boolean var17 = false;

         while(!var17 && var14.q() > 0) {
            BlockPosition var18 = var14.b();
            IBlockData var19 = var15.getType(var18);
            if(var19.getMaterial().c()) {
               var17 = true;
            } else {
               --this.locY;
               var14 = var18;
            }
         }

         if(var17) {
            this.a(this.locX, this.locY, this.locZ);
            if(var15.a((Entity)this, (AxisAlignedBB)this.bk()).isEmpty() && !var15.e(this.bk())) {
               var13 = true;
            }
         }
      }

      if(!var13) {
         this.a(var7, var9, var11);
         return false;
      } else {
         short var30 = 128;

         for(int var31 = 0; var31 < var30; ++var31) {
            double var32 = (double)var31 / ((double)var30 - 1.0D);
            float var21 = (var16.nextFloat() - 0.5F) * 0.2F;
            float var22 = (var16.nextFloat() - 0.5F) * 0.2F;
            float var23 = (var16.nextFloat() - 0.5F) * 0.2F;
            double var24 = var7 + (this.locX - var7) * var32 + (var16.nextDouble() - 0.5D) * (double)this.width * 2.0D;
            double var26 = var9 + (this.locY - var9) * var32 + var16.nextDouble() * (double)this.length;
            double var28 = var11 + (this.locZ - var11) * var32 + (var16.nextDouble() - 0.5D) * (double)this.width * 2.0D;
            var15.a(EnumParticle.PORTAL, var24, var26, var28, (double)var21, (double)var22, (double)var23, new int[0]);
         }

         if(this instanceof EntityCreature) {
            ((EntityCreature)this).x().o();
         }

         return true;
      }
   }

   public boolean cC() {
      return true;
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumInventorySlot.EnumSlotType.values().length];

      static {
         try {
            a[EnumInventorySlot.EnumSlotType.HAND.ordinal()] = 1;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumInventorySlot.EnumSlotType.ARMOR.ordinal()] = 2;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
