package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EnumColor;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_apq extends Block {
   public static final BlockStateEnum a = BlockStateEnum.a("color", EnumColor.class);
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.0625D, 1.0D);

   protected class_apq() {
      super(Material.r);
      this.w(this.A.b().set(a, EnumColor.WHITE));
      this.a(true);
      this.a(CreativeModeTab.c);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return b;
   }

   public MaterialMapColor r(IBlockData var1) {
      return ((EnumColor)var1.get(a)).e();
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean a(World var1, BlockPosition var2) {
      return super.a(var1, var2) && this.b(var1, var2);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      this.e(var1, var2, var3);
   }

   private boolean e(World var1, BlockPosition var2, IBlockData var3) {
      if(!this.b(var1, var2)) {
         this.b(var1, var2, var3, 0);
         var1.g(var2);
         return false;
      } else {
         return true;
      }
   }

   private boolean b(World var1, BlockPosition var2) {
      return !var1.d(var2.b());
   }

   public int d(IBlockData var1) {
      return ((EnumColor)var1.get(a)).a();
   }

   public IBlockData a(int var1) {
      return this.u().set(a, EnumColor.b(var1));
   }

   public int e(IBlockData var1) {
      return ((EnumColor)var1.get(a)).a();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }
}
