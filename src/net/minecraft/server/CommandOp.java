package net.minecraft.server;

import com.google.common.collect.Lists;
import com.mojang.authlib.GameProfile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;

public class CommandOp extends CommandAbstract {
   public String c() {
      return "op";
   }

   public int a() {
      return 3;
   }

   public String b(ICommandListener var1) {
      return "commands.op.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length == 1 && var3[0].length() > 0) {
         GameProfile var4 = var1.aA().a(var3[0]);
         if(var4 == null) {
            throw new class_bz("commands.op.failed", new Object[]{var3[0]});
         } else {
            var1.getPlayerList().a(var4);
            a(var2, this, "commands.op.success", new Object[]{var3[0]});
         }
      } else {
         throw new class_cf("commands.op.usage", new Object[0]);
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      if(var3.length == 1) {
         String var5 = var3[var3.length - 1];
         ArrayList var6 = Lists.newArrayList();
         GameProfile[] var7 = var1.K();
         int var8 = var7.length;

         for(int var9 = 0; var9 < var8; ++var9) {
            GameProfile var10 = var7[var9];
            if(!var1.getPlayerList().h(var10) && a(var5, var10.getName())) {
               var6.add(var10.getName());
            }
         }

         return var6;
      } else {
         return Collections.emptyList();
      }
   }
}
