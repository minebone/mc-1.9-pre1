package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.Items;
import net.minecraft.server.class_acg;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_yv;

public class class_uh extends class_tj {
   private final class_yv a;
   private final double b;
   private final int c;
   private final float d;
   private int e = -1;
   private int f;
   private boolean g;
   private boolean h;
   private int i = -1;

   public class_uh(class_yv var1, double var2, int var4, float var5) {
      this.a = var1;
      this.b = var2;
      this.c = var4;
      this.d = var5 * var5;
      this.a(3);
   }

   public boolean a() {
      return this.a.A() == null?false:this.f();
   }

   protected boolean f() {
      return this.a.ca() != null && this.a.ca().b() == Items.f;
   }

   public boolean b() {
      return (this.a() || !this.a.x().n()) && this.f();
   }

   public void c() {
      super.c();
      this.a.a(true);
   }

   public void d() {
      super.c();
      this.a.a(false);
      this.f = 0;
      this.e = -1;
      this.a.cy();
   }

   public void e() {
      class_rz var1 = this.a.A();
      if(var1 != null) {
         double var2 = this.a.e(var1.locX, var1.bk().b, var1.locZ);
         boolean var4 = this.a.y().a(var1);
         boolean var5 = this.f > 0;
         if(var4 != var5) {
            this.f = 0;
         }

         if(var4) {
            ++this.f;
         } else {
            --this.f;
         }

         if(var2 <= (double)this.d && this.f >= 20) {
            this.a.x().o();
            ++this.i;
         } else {
            this.a.x().a((Entity)var1, this.b);
            this.i = -1;
         }

         if(this.i >= 20) {
            if((double)this.a.bE().nextFloat() < 0.3D) {
               this.g = !this.g;
            }

            if((double)this.a.bE().nextFloat() < 0.3D) {
               this.h = !this.h;
            }

            this.i = 0;
         }

         if(this.i > -1) {
            if(var2 > (double)(this.d * 0.75F)) {
               this.h = false;
            } else if(var2 < (double)(this.d * 0.25F)) {
               this.h = true;
            }

            this.a.u().a(this.h?-0.5F:0.5F, this.g?0.5F:-0.5F);
            this.a.a(var1, 30.0F, 30.0F);
         } else {
            this.a.t().a(var1, 30.0F, 30.0F);
         }

         if(this.a.cr()) {
            if(!var4 && this.f < -60) {
               this.a.cy();
            } else if(var4) {
               int var6 = this.a.cw();
               if(var6 >= 20) {
                  this.a.cy();
                  this.a.a(var1, class_acg.b(var6));
                  this.e = this.c;
               }
            }
         } else if(--this.e <= 0 && this.f >= -60) {
            this.a.c(EnumHand.MAIN_HAND);
         }

      }
   }
}
