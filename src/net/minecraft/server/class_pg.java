package net.minecraft.server;

import com.google.common.collect.Sets;
import java.util.Set;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.class_ox;

public class class_pg implements class_ox {
   private static final Set a = Sets.newHashSet((Object[])(new String[]{"ArmorStand", "Bat", "Blaze", "CaveSpider", "Chicken", "Cow", "Creeper", "EnderDragon", "Enderman", "Endermite", "EntityHorse", "Ghast", "Giant", "Guardian", "LavaSlime", "MushroomCow", "Ozelot", "Pig", "PigZombie", "Rabbit", "Sheep", "Shulker", "Silverfish", "Skeleton", "Slime", "SnowMan", "Spider", "Squid", "Villager", "VillagerGolem", "Witch", "WitherBoss", "Wolf", "Zombie"}));

   public int a() {
      return 109;
   }

   public NBTTagCompound a(NBTTagCompound var1) {
      if(a.contains(var1.l("id"))) {
         float var2;
         if(var1.b("HealF", 99)) {
            var2 = var1.j("HealF");
            var1.q("HealF");
         } else {
            if(!var1.b("Health", 99)) {
               return var1;
            }

            var2 = var1.j("Heath");
         }

         var1.a("Health", var2);
      }

      return var1;
   }
}
