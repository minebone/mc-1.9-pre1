package net.minecraft.server;

import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumAnimation;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.class_abx;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_adq;
import net.minecraft.server.class_agn;
import net.minecraft.server.class_agp;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_zl;

public class class_acg extends Item {
   public class_acg() {
      this.j = 1;
      this.e(384);
      this.a(CreativeModeTab.j);
      this.a(new class_kk("pull"), new class_adq() {
      });
      this.a(new class_kk("pulling"), new class_adq() {
      });
   }

   private ItemStack a(EntityHuman var1) {
      if(this.h_(var1.b((EnumHand)EnumHand.OFF_HAND))) {
         return var1.b((EnumHand)EnumHand.OFF_HAND);
      } else if(this.h_(var1.b((EnumHand)EnumHand.MAIN_HAND))) {
         return var1.b((EnumHand)EnumHand.MAIN_HAND);
      } else {
         for(int var2 = 0; var2 < var1.br.u_(); ++var2) {
            ItemStack var3 = var1.br.a(var2);
            if(this.h_(var3)) {
               return var3;
            }
         }

         return null;
      }
   }

   protected boolean h_(ItemStack var1) {
      return var1 != null && var1.b() instanceof class_abx;
   }

   public void a(ItemStack var1, World var2, class_rz var3, int var4) {
      if(var3 instanceof EntityHuman) {
         EntityHuman var5 = (EntityHuman)var3;
         boolean var6 = var5.abilities.d || class_agn.a(class_agp.x, var1) > 0;
         ItemStack var7 = this.a(var5);
         if(var7 != null || var6) {
            if(var7 == null) {
               var7 = new ItemStack(Items.g);
            }

            int var8 = this.e(var1) - var4;
            float var9 = b(var8);
            if((double)var9 >= 0.1D) {
               boolean var10 = var6 && var7.b() == Items.g;
               if(!var2.E) {
                  class_abx var11 = (class_abx)((class_abx)(var7.b() instanceof class_abx?var7.b():Items.g));
                  class_zl var12 = var11.a(var2, var7, var5);
                  var12.a(var5, var5.pitch, var5.yaw, 0.0F, var9 * 3.0F, 1.0F);
                  if(var9 == 1.0F) {
                     var12.a(true);
                  }

                  int var13 = class_agn.a(class_agp.u, var1);
                  if(var13 > 0) {
                     var12.c(var12.k() + (double)var13 * 0.5D + 0.5D);
                  }

                  int var14 = class_agn.a(class_agp.v, var1);
                  if(var14 > 0) {
                     var12.a(var14);
                  }

                  if(class_agn.a(class_agp.w, var1) > 0) {
                     var12.g(100);
                  }

                  var1.a(1, (class_rz)var5);
                  if(var10) {
                     var12.c = class_zl.class_a_in_class_zl.CREATIVE_ONLY;
                  }

                  var2.a((Entity)var12);
               }

               var2.a((EntityHuman)null, var5.locX, var5.locY, var5.locZ, class_ng.v, EnumSoundCategory.NEUTRAL, 1.0F, 1.0F / (i.nextFloat() * 0.4F + 1.2F) + var9 * 0.5F);
               if(!var10) {
                  --var7.b;
                  if(var7.b == 0) {
                     var5.br.d(var7);
                  }
               }

               var5.b(StatisticList.b((Item)this));
            }
         }
      }
   }

   public static float b(int var0) {
      float var1 = (float)var0 / 20.0F;
      var1 = (var1 * var1 + var1 * 2.0F) / 3.0F;
      if(var1 > 1.0F) {
         var1 = 1.0F;
      }

      return var1;
   }

   public int e(ItemStack var1) {
      return 72000;
   }

   public EnumAnimation f(ItemStack var1) {
      return EnumAnimation.BOW;
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      boolean var5 = this.a(var3) != null;
      if(!var3.abilities.d && !var5) {
         return !var5?new class_qo(EnumResult.FAIL, var1):new class_qo(EnumResult.PASS, var1);
      } else {
         var3.c(var4);
         return new class_qo(EnumResult.SUCCESS, var1);
      }
   }

   public int c() {
      return 1;
   }
}
