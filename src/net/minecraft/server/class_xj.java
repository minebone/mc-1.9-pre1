package net.minecraft.server;

import java.lang.reflect.Constructor;
import java.util.Arrays;
import net.minecraft.server.class_wt;
import net.minecraft.server.class_wx;
import net.minecraft.server.class_wy;
import net.minecraft.server.class_wz;
import net.minecraft.server.class_xa;
import net.minecraft.server.class_xb;
import net.minecraft.server.class_xc;
import net.minecraft.server.class_xd;
import net.minecraft.server.class_xe;
import net.minecraft.server.class_xf;
import net.minecraft.server.class_xg;
import net.minecraft.server.class_xh;
import net.minecraft.server.class_xi;

public class class_xj {
   private static class_xj[] l = new class_xj[0];
   public static final class_xj a = a(class_wz.class, "HoldingPattern");
   public static final class_xj b = a(class_xh.class, "StrafePlayer");
   public static final class_xj c = a(class_xb.class, "LandingApproach");
   public static final class_xj d = a(class_xc.class, "Landing");
   public static final class_xj e = a(class_xi.class, "Takeoff");
   public static final class_xj f = a(class_xf.class, "SittingFlaming");
   public static final class_xj g = a(class_xg.class, "SittingScanning");
   public static final class_xj h = a(class_xe.class, "SittingAttacking");
   public static final class_xj i = a(class_wx.class, "ChargingPlayer");
   public static final class_xj j = a(class_wy.class, "Dying");
   public static final class_xj k = a(class_xa.class, "Hover");
   private final Class m;
   private final int n;
   private final String o;

   private class_xj(int var1, Class var2, String var3) {
      this.n = var1;
      this.m = var2;
      this.o = var3;
   }

   public class_xd a(class_wt var1) {
      try {
         Constructor var2 = this.a();
         return (class_xd)var2.newInstance(new Object[]{var1});
      } catch (Exception var3) {
         throw new Error(var3);
      }
   }

   protected Constructor a() throws NoSuchMethodException {
      return this.m.getConstructor(new Class[]{class_wt.class});
   }

   public int b() {
      return this.n;
   }

   public String toString() {
      return this.o + " (#" + this.n + ")";
   }

   public static class_xj a(int var0) {
      return var0 >= 0 && var0 < l.length?l[var0]:a;
   }

   public static int c() {
      return l.length;
   }

   private static class_xj a(Class var0, String var1) {
      class_xj var2 = new class_xj(l.length, var0, var1);
      l = (class_xj[])Arrays.copyOf(l, l.length + 1);
      l[var2.b()] = var2;
      return var2;
   }
}
