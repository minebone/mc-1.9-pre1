package net.minecraft.server;

import net.minecraft.server.EntityCreature;
import net.minecraft.server.Vec3D;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_vl;

public class class_tv extends class_tj {
   private EntityCreature a;
   private double b;
   private double c;
   private double d;
   private double e;

   public class_tv(EntityCreature var1, double var2) {
      this.a = var1;
      this.e = var2;
      this.a(1);
   }

   public boolean a() {
      if(this.a.cU()) {
         return false;
      } else {
         BlockPosition var1 = this.a.cV();
         Vec3D var2 = class_vl.a(this.a, 16, 7, new Vec3D((double)var1.p(), (double)var1.q(), (double)var1.r()));
         if(var2 == null) {
            return false;
         } else {
            this.b = var2.b;
            this.c = var2.c;
            this.d = var2.d;
            return true;
         }
      }
   }

   public boolean b() {
      return !this.a.x().n();
   }

   public void c() {
      this.a.x().a(this.b, this.c, this.d, this.e);
   }
}
