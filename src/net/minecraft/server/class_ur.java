package net.minecraft.server;

import net.minecraft.server.class_tr;
import net.minecraft.server.class_yz;

public class class_ur extends class_tr {
   private final class_yz h;
   private int i;

   public class_ur(class_yz var1, double var2, boolean var4) {
      super(var1, var2, var4);
      this.h = var1;
   }

   public void c() {
      super.c();
      this.i = 0;
   }

   public void d() {
      super.d();
      this.h.a(false);
   }

   public void e() {
      super.e();
      ++this.i;
      if(this.i >= 5 && this.c < 10) {
         this.h.a(true);
      } else {
         this.h.a(false);
      }

   }
}
