package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.class_ane;
import net.minecraft.server.class_anf;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ato;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_atq extends class_ato {
   private static final IBlockData a = Blocks.r.u().set(class_anf.b, BlockWood.EnumLogVariant.BIRCH);
   private static final IBlockData b = Blocks.t.u().set(class_ane.e, BlockWood.EnumLogVariant.BIRCH).set(class_ane.b, Boolean.valueOf(false));
   private boolean c;

   public class_atq(boolean var1, boolean var2) {
      super(var1);
      this.c = var2;
   }

   public boolean b(World var1, Random var2, BlockPosition var3) {
      int var4 = var2.nextInt(3) + 5;
      if(this.c) {
         var4 += var2.nextInt(7);
      }

      boolean var5 = true;
      if(var3.q() >= 1 && var3.q() + var4 + 1 <= 256) {
         int var9;
         int var10;
         for(int var6 = var3.q(); var6 <= var3.q() + 1 + var4; ++var6) {
            byte var7 = 1;
            if(var6 == var3.q()) {
               var7 = 0;
            }

            if(var6 >= var3.q() + 1 + var4 - 2) {
               var7 = 2;
            }

            BlockPosition.class_a_in_class_cj var8 = new BlockPosition.class_a_in_class_cj();

            for(var9 = var3.p() - var7; var9 <= var3.p() + var7 && var5; ++var9) {
               for(var10 = var3.r() - var7; var10 <= var3.r() + var7 && var5; ++var10) {
                  if(var6 >= 0 && var6 < 256) {
                     if(!this.a(var1.getType(var8.c(var9, var6, var10)).getBlock())) {
                        var5 = false;
                     }
                  } else {
                     var5 = false;
                  }
               }
            }
         }

         if(!var5) {
            return false;
         } else {
            Block var16 = var1.getType(var3.b()).getBlock();
            if((var16 == Blocks.c || var16 == Blocks.d || var16 == Blocks.ak) && var3.q() < 256 - var4 - 1) {
               this.a(var1, var3.b());

               int var17;
               for(var17 = var3.q() - 3 + var4; var17 <= var3.q() + var4; ++var17) {
                  int var18 = var17 - (var3.q() + var4);
                  var9 = 1 - var18 / 2;

                  for(var10 = var3.p() - var9; var10 <= var3.p() + var9; ++var10) {
                     int var11 = var10 - var3.p();

                     for(int var12 = var3.r() - var9; var12 <= var3.r() + var9; ++var12) {
                        int var13 = var12 - var3.r();
                        if(Math.abs(var11) != var9 || Math.abs(var13) != var9 || var2.nextInt(2) != 0 && var18 != 0) {
                           BlockPosition var14 = new BlockPosition(var10, var17, var12);
                           Material var15 = var1.getType(var14).getMaterial();
                           if(var15 == Material.a || var15 == Material.j) {
                              this.a(var1, var14, b);
                           }
                        }
                     }
                  }
               }

               for(var17 = 0; var17 < var4; ++var17) {
                  Material var19 = var1.getType(var3.b(var17)).getMaterial();
                  if(var19 == Material.a || var19 == Material.j) {
                     this.a(var1, var3.b(var17), a);
                  }
               }

               return true;
            } else {
               return false;
            }
         }
      } else {
         return false;
      }
   }
}
