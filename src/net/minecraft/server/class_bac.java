package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.Enchantment;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ago;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_bae;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_kk;
import net.minecraft.server.MathHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_bac extends class_bae {
   private static final Logger a = LogManager.getLogger();
   private final List b;

   public class_bac(class_baq[] var1, List var2) {
      super(var1);
      this.b = var2;
   }

   public ItemStack a(ItemStack var1, Random var2, class_azy var3) {
      Enchantment var4;
      if(this.b != null && !this.b.isEmpty()) {
         var4 = (Enchantment)this.b.get(var2.nextInt(this.b.size()));
      } else {
         ArrayList var5 = Lists.newArrayList();
         Iterator var6 = Enchantment.b.iterator();

         label33:
         while(true) {
            Enchantment var7;
            do {
               if(!var6.hasNext()) {
                  if(var5.isEmpty()) {
                     a.warn("Couldn\'t find a compatible enchantment for " + var1);
                     return var1;
                  }

                  var4 = (Enchantment)var5.get(var2.nextInt(var5.size()));
                  break label33;
               }

               var7 = (Enchantment)var6.next();
            } while(var1.b() != Items.aS && !var7.a(var1));

            var5.add(var7);
         }
      }

      int var8 = MathHelper.a(var2, var4.d(), var4.b());
      if(var1.b() == Items.aS) {
         var1.a((Item)Items.cn);
         Items.cn.a(var1, new class_ago(var4, var8));
      } else {
         var1.a(var4, var8);
      }

      return var1;
   }

   public static class class_a_in_class_bac extends class_bae.class_a_in_class_bae {
      public class_a_in_class_bac() {
         super(new class_kk("enchant_randomly"), class_bac.class);
      }

      public void a(JsonObject var1, class_bac var2, JsonSerializationContext var3) {
         if(var2.b != null && !var2.b.isEmpty()) {
            JsonArray var4 = new JsonArray();
            Iterator var5 = var2.b.iterator();

            while(var5.hasNext()) {
               Enchantment var6 = (Enchantment)var5.next();
               class_kk var7 = (class_kk)Enchantment.b.b(var6);
               if(var7 == null) {
                  throw new IllegalArgumentException("Don\'t know how to serialize enchantment " + var6);
               }

               var4.add(new JsonPrimitive(var7.toString()));
            }

            var1.add("enchantments", var4);
         }

      }

      public class_bac a(JsonObject var1, JsonDeserializationContext var2, class_baq[] var3) {
         ArrayList var4 = null;
         if(var1.has("enchantments")) {
            var4 = Lists.newArrayList();
            JsonArray var5 = ChatDeserializer.u(var1, "enchantments");
            Iterator var6 = var5.iterator();

            while(var6.hasNext()) {
               JsonElement var7 = (JsonElement)var6.next();
               String var8 = ChatDeserializer.a(var7, "enchantment");
               Enchantment var9 = (Enchantment)Enchantment.b.c(new class_kk(var8));
               if(var9 == null) {
                  throw new JsonSyntaxException("Unknown enchantment \'" + var8 + "\'");
               }

               var4.add(var9);
            }
         }

         return new class_bac(var3, var4);
      }

      // $FF: synthetic method
      public class_bae b(JsonObject var1, JsonDeserializationContext var2, class_baq[] var3) {
         return this.a(var1, var2, var3);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(JsonObject var1, class_bae var2, JsonSerializationContext var3) {
         this.a(var1, (class_bac)var2, var3);
      }
   }
}
