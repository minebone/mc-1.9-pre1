package net.minecraft.server;

import net.minecraft.server.Container;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.class_qs;

public interface class_qm extends class_qs {
   Container a(PlayerInventory var1, EntityHuman var2);

   String k();
}
