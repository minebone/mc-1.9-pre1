package net.minecraft.server;

import net.minecraft.server.DamageSource;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.class_adj;
import net.minecraft.server.ItemStack;

public class class_aar {
   private int a = 20;
   private float b = 5.0F;
   private float c;
   private int d;
   private int e = 20;

   public void a(int var1, float var2) {
      this.a = Math.min(var1 + this.a, 20);
      this.b = Math.min(this.b + (float)var1 * var2 * 2.0F, (float)this.a);
   }

   public void a(class_adj var1, ItemStack var2) {
      this.a(var1.h(var2), var1.i(var2));
   }

   public void a(EntityHuman var1) {
      EnumDifficulty var2 = var1.world.ae();
      this.e = this.a;
      if(this.c > 4.0F) {
         this.c -= 4.0F;
         if(this.b > 0.0F) {
            this.b = Math.max(this.b - 1.0F, 0.0F);
         } else if(var2 != EnumDifficulty.PEACEFUL) {
            this.a = Math.max(this.a - 1, 0);
         }
      }

      boolean var3 = var1.world.U().b("naturalRegeneration");
      if(var3 && this.b > 0.0F && var1.cS() && this.a >= 20) {
         ++this.d;
         if(this.d >= 10) {
            float var4 = Math.min(this.b, 4.0F);
            var1.b(var4 / 4.0F);
            this.a(var4);
            this.d = 0;
         }
      } else if(var3 && this.a >= 18 && var1.cS()) {
         ++this.d;
         if(this.d >= 80) {
            var1.b(1.0F);
            this.a(4.0F);
            this.d = 0;
         }
      } else if(this.a <= 0) {
         ++this.d;
         if(this.d >= 80) {
            if(var1.bP() > 10.0F || var2 == EnumDifficulty.HARD || var1.bP() > 1.0F && var2 == EnumDifficulty.NORMAL) {
               var1.a(DamageSource.g, 1.0F);
            }

            this.d = 0;
         }
      } else {
         this.d = 0;
      }

   }

   public void a(NBTTagCompound var1) {
      if(var1.b("foodLevel", 99)) {
         this.a = var1.h("foodLevel");
         this.d = var1.h("foodTickTimer");
         this.b = var1.j("foodSaturationLevel");
         this.c = var1.j("foodExhaustionLevel");
      }

   }

   public void b(NBTTagCompound var1) {
      var1.a("foodLevel", this.a);
      var1.a("foodTickTimer", this.d);
      var1.a("foodSaturationLevel", this.b);
      var1.a("foodExhaustionLevel", this.c);
   }

   public int a() {
      return this.a;
   }

   public boolean c() {
      return this.a < 20;
   }

   public void a(float var1) {
      this.c = Math.min(this.c + var1, 40.0F);
   }

   public float e() {
      return this.b;
   }

   public void a(int var1) {
      this.a = var1;
   }
}
