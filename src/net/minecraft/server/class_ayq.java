package net.minecraft.server;

import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.class_ahw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.class_ayl;
import net.minecraft.server.class_aym;
import net.minecraft.server.class_ayn;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;

public class class_ayq extends class_ayn {
   public void a(class_ahw var1, EntityInsentient var2) {
      super.a(var1, var2);
   }

   public void a() {
      super.a();
   }

   public class_aym b() {
      return this.a(MathHelper.c(this.b.bk().a), MathHelper.c(this.b.bk().b + 0.5D), MathHelper.c(this.b.bk().c));
   }

   public class_aym a(double var1, double var3, double var5) {
      return this.a(MathHelper.c(var1 - (double)(this.b.width / 2.0F)), MathHelper.c(var3 + 0.5D), MathHelper.c(var5 - (double)(this.b.width / 2.0F)));
   }

   public int a(class_aym[] var1, class_aym var2, class_aym var3, float var4) {
      int var5 = 0;
      EnumDirection[] var6 = EnumDirection.values();
      int var7 = var6.length;

      for(int var8 = 0; var8 < var7; ++var8) {
         EnumDirection var9 = var6[var8];
         class_aym var10 = this.b(var2.a + var9.g(), var2.b + var9.h(), var2.c + var9.i());
         if(var10 != null && !var10.i && var10.a(var3) < var4) {
            var1[var5++] = var10;
         }
      }

      return var5;
   }

   public class_ayl a(class_ahw var1, int var2, int var3, int var4, EntityInsentient var5, int var6, int var7, int var8, boolean var9, boolean var10) {
      return class_ayl.WATER;
   }

   private class_aym b(int var1, int var2, int var3) {
      class_ayl var4 = this.c(var1, var2, var3);
      return var4 == class_ayl.WATER?this.a(var1, var2, var3):null;
   }

   private class_ayl c(int var1, int var2, int var3) {
      BlockPosition.class_a_in_class_cj var4 = new BlockPosition.class_a_in_class_cj();

      for(int var5 = var1; var5 < var1 + this.d; ++var5) {
         for(int var6 = var2; var6 < var2 + this.e; ++var6) {
            for(int var7 = var3; var7 < var3 + this.f; ++var7) {
               IBlockData var8 = this.a.getType(var4.c(var5, var6, var7));
               if(var8.getMaterial() != Material.h) {
                  return class_ayl.BLOCKED;
               }
            }
         }
      }

      return class_ayl.WATER;
   }
}
