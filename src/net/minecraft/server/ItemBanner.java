package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumColor;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.TileEntityBanner;
import net.minecraft.server.World;
import net.minecraft.server.class_acb;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aot;
import net.minecraft.server.class_apk;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;

public class ItemBanner extends class_acb {
   public ItemBanner() {
      super(Blocks.cK);
      this.j = 16;
      this.a(CreativeModeTab.c);
      this.a(true);
      this.e(0);
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      IBlockData var10 = var3.getType(var4);
      boolean var11 = var10.getBlock().a((class_ahw)var3, (BlockPosition)var4);
      if(var6 == EnumDirection.DOWN || !var10.getMaterial().a() && !var11 || var11 && var6 != EnumDirection.UP) {
         return EnumResult.FAIL;
      } else {
         var4 = var4.a(var6);
         if(var2.a(var4, var6, var1) && Blocks.cK.a(var3, var4)) {
            if(var3.E) {
               return EnumResult.SUCCESS;
            } else {
               var4 = var11?var4.b():var4;
               if(var6 == EnumDirection.UP) {
                  int var12 = MathHelper.c((double)((var2.yaw + 180.0F) * 16.0F / 360.0F) + 0.5D) & 15;
                  var3.a((BlockPosition)var4, (IBlockData)Blocks.cK.u().set(class_aot.b, Integer.valueOf(var12)), 3);
               } else {
                  var3.a((BlockPosition)var4, (IBlockData)Blocks.cL.u().set(class_apk.b, var6), 3);
               }

               --var1.b;
               TileEntity var13 = var3.r(var4);
               if(var13 instanceof TileEntityBanner) {
                  ((TileEntityBanner)var13).a(var1);
               }

               return EnumResult.SUCCESS;
            }
         } else {
            return EnumResult.FAIL;
         }
      }
   }

   public String a(ItemStack var1) {
      String var2 = "item.banner.";
      EnumColor var3 = b(var1);
      var2 = var2 + var3.d() + ".name";
      return class_di.a(var2);
   }

   public static EnumColor b(ItemStack var0) {
      NBTTagCompound var1 = var0.a("BlockEntityTag", false);
      EnumColor var2 = null;
      if(var1 != null && var1.e("Base")) {
         var2 = EnumColor.a(var1.h("Base"));
      } else {
         var2 = EnumColor.a(var0.i());
      }

      return var2;
   }
}
