package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EnumColor;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.class_ajp;
import net.minecraft.server.class_ama;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_aoq extends class_ama {
   public static final BlockStateEnum a = BlockStateEnum.a("color", EnumColor.class);

   public class_aoq(Material var1) {
      super(var1, false);
      this.w(this.A.b().set(a, EnumColor.WHITE));
      this.a(CreativeModeTab.b);
   }

   public int d(IBlockData var1) {
      return ((EnumColor)var1.get(a)).a();
   }

   public MaterialMapColor r(IBlockData var1) {
      return ((EnumColor)var1.get(a)).e();
   }

   public int a(Random var1) {
      return 0;
   }

   protected boolean o() {
      return true;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public IBlockData a(int var1) {
      return this.u().set(a, EnumColor.b(var1));
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      if(!var1.E) {
         class_ajp.c(var1, var2);
      }

   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      if(!var1.E) {
         class_ajp.c(var1, var2);
      }

   }

   public int e(IBlockData var1) {
      return ((EnumColor)var1.get(a)).a();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }
}
