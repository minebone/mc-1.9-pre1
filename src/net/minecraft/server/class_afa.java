package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aex;
import net.minecraft.server.class_amn;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;

public class class_afa extends class_aex {
   public class_afa(Block var1) {
      super(var1, false);
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      MovingObjectPosition var5 = this.a(var2, var3, true);
      if(var5 == null) {
         return new class_qo(EnumResult.PASS, var1);
      } else {
         if(var5.a == MovingObjectPosition.EnumMovingObjectType.BLOCK) {
            BlockPosition var6 = var5.a();
            if(!var2.a(var3, var6) || !var3.a(var6.a(var5.b), var5.b, var1)) {
               return new class_qo(EnumResult.FAIL, var1);
            }

            BlockPosition var7 = var6.a();
            IBlockData var8 = var2.getType(var6);
            if(var8.getMaterial() == Material.h && ((Integer)var8.get(class_amn.b)).intValue() == 0 && var2.d(var7)) {
               var2.a((BlockPosition)var7, (IBlockData)Blocks.bx.u(), 11);
               if(!var3.abilities.d) {
                  --var1.b;
               }

               var3.b(StatisticList.b((Item)this));
               var2.a(var3, var6, class_ng.gp, EnumSoundCategory.BLOCKS, 1.0F, 1.0F);
               return new class_qo(EnumResult.SUCCESS, var1);
            }
         }

         return new class_qo(EnumResult.FAIL, var1);
      }
   }
}
