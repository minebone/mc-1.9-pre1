package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.EntityProjectile;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.World;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_rw;
import net.minecraft.server.class_rz;

public class class_aaa extends EntityProjectile {
   public class_aaa(World var1) {
      super(var1);
   }

   public class_aaa(World var1, class_rz var2) {
      super(var1, var2);
   }

   public class_aaa(World var1, double var2, double var4, double var6) {
      super(var1, var2, var4, var6);
   }

   protected float j() {
      return 0.07F;
   }

   protected void a(MovingObjectPosition var1) {
      if(!this.world.E) {
         this.world.b(2002, new BlockPosition(this), 0);
         int var2 = 3 + this.world.r.nextInt(5) + this.world.r.nextInt(5);

         while(var2 > 0) {
            int var3 = class_rw.a(var2);
            var2 -= var3;
            this.world.a((Entity)(new class_rw(this.world, this.locX, this.locY, this.locZ, var3)));
         }

         this.S();
      }

   }
}
