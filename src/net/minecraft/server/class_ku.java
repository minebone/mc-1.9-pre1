package net.minecraft.server;

public final class class_ku extends RuntimeException {
   public static final class_ku a = new class_ku();

   private class_ku() {
      this.setStackTrace(new StackTraceElement[0]);
   }

   public synchronized Throwable fillInStackTrace() {
      this.setStackTrace(new StackTraceElement[0]);
      return this;
   }
}
