package net.minecraft.server;

import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;

public class ItemCoal extends Item {
   public ItemCoal() {
      this.a(true);
      this.e(0);
      this.a(CreativeModeTab.l);
   }

   public String f_(ItemStack var1) {
      return var1.i() == 1?"item.charcoal":"item.coal";
   }
}
