package net.minecraft.server;

import com.google.common.collect.Maps;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTCompressedStreamTools;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.class_awn;
import net.minecraft.server.class_kk;
import org.apache.commons.io.IOUtils;

public class class_awl {
   private final Map a;
   private final String b;

   public class_awl() {
      this("structures");
   }

   public class_awl(String var1) {
      this.a = Maps.newHashMap();
      this.b = var1;
   }

   public class_awn a(MinecraftServer var1, class_kk var2) {
      String var3 = var2.a();
      if(this.a.containsKey(var3)) {
         return (class_awn)this.a.get(var3);
      } else {
         if(var1 != null) {
            this.b(var1, var2);
         } else {
            this.a(var2);
         }

         if(this.a.containsKey(var3)) {
            return (class_awn)this.a.get(var3);
         } else {
            class_awn var4 = new class_awn();
            this.a.put(var3, var4);
            return var4;
         }
      }
   }

   public boolean b(MinecraftServer var1, class_kk var2) {
      String var3 = var2.a();
      File var4 = var1.d(this.b);
      File var5 = new File(var4, var3 + ".nbt");
      if(!var5.exists()) {
         return this.a(var2);
      } else {
         FileInputStream var6 = null;

         boolean var8;
         try {
            var6 = new FileInputStream(var5);
            this.a((String)var3, (InputStream)var6);
            return true;
         } catch (Throwable var12) {
            var8 = false;
         } finally {
            IOUtils.closeQuietly((InputStream)var6);
         }

         return var8;
      }
   }

   private boolean a(class_kk var1) {
      String var2 = var1.b();
      String var3 = var1.a();
      InputStream var4 = null;

      boolean var6;
      try {
         var4 = MinecraftServer.class.getResourceAsStream("/assets/" + var2 + "/structures/" + var3 + ".nbt");
         this.a(var3, var4);
         return true;
      } catch (Throwable var10) {
         var6 = false;
      } finally {
         IOUtils.closeQuietly(var4);
      }

      return var6;
   }

   private void a(String var1, InputStream var2) throws IOException {
      NBTTagCompound var3 = NBTCompressedStreamTools.a(var2);
      class_awn var4 = new class_awn();
      var4.b(var3);
      this.a.put(var1, var4);
   }

   public boolean c(MinecraftServer var1, class_kk var2) {
      String var3 = var2.a();
      if(!this.a.containsKey(var3)) {
         return false;
      } else {
         File var4 = var1.d(this.b);
         if(!var4.exists()) {
            if(!var4.mkdirs()) {
               return false;
            }
         } else if(!var4.isDirectory()) {
            return false;
         }

         File var5 = new File(var4, var3 + ".nbt");
         NBTTagCompound var6 = new NBTTagCompound();
         class_awn var7 = (class_awn)this.a.get(var3);
         FileOutputStream var8 = null;

         boolean var10;
         try {
            var7.a(var6);
            var8 = new FileOutputStream(var5);
            NBTCompressedStreamTools.a((NBTTagCompound)var6, (OutputStream)var8);
            return true;
         } catch (Throwable var14) {
            var10 = false;
         } finally {
            IOUtils.closeQuietly((OutputStream)var8);
         }

         return var10;
      }
   }
}
