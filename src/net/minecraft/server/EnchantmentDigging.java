package net.minecraft.server;

import net.minecraft.server.Enchantment;
import net.minecraft.server.Items;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agm;
import net.minecraft.server.EnumInventorySlot;

public class EnchantmentDigging extends Enchantment {
   protected EnchantmentDigging(Enchantment.class_a_in_class_agl var1, EnumInventorySlot... var2) {
      super(var1, class_agm.DIGGER, var2);
      this.c("digging");
   }

   public int a(int var1) {
      return 1 + 10 * (var1 - 1);
   }

   public int b(int var1) {
      return super.a(var1) + 50;
   }

   public int b() {
      return 5;
   }

   public boolean a(ItemStack var1) {
      return var1.b() == Items.bl?true:super.a(var1);
   }
}
