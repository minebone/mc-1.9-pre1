package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MojangsonParser;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ec;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.class_yc;

public class class_aj extends CommandAbstract {
   public String c() {
      return "give";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.give.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 2) {
         throw new class_cf("commands.give.usage", new Object[0]);
      } else {
         EntityPlayer var4 = a(var1, var2, var3[0]);
         Item var5 = a(var2, var3[1]);
         int var6 = var3.length >= 3?a(var3[2], 1, 64):1;
         int var7 = var3.length >= 4?a(var3[3]):0;
         ItemStack var8 = new ItemStack(var5, var6, var7);
         if(var3.length >= 5) {
            String var9 = a(var2, var3, 4).c();

            try {
               var8.d(MojangsonParser.a(var9));
            } catch (class_ec var11) {
               throw new class_bz("commands.give.tagError", new Object[]{var11.getMessage()});
            }
         }

         boolean var12 = var4.br.c(var8);
         if(var12) {
            var4.world.a((EntityHuman)null, var4.locX, var4.locY, var4.locZ, class_ng.cR, EnumSoundCategory.PLAYERS, 0.2F, ((var4.bE().nextFloat() - var4.bE().nextFloat()) * 0.7F + 1.0F) * 2.0F);
            var4.bs.b();
         }

         class_yc var10;
         if(var12 && var8.b <= 0) {
            var8.b = 1;
            var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_ITEMS, var6);
            var10 = var4.a(var8, false);
            if(var10 != null) {
               var10.w();
            }
         } else {
            var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_ITEMS, var6 - var8.b);
            var10 = var4.a(var8, false);
            if(var10 != null) {
               var10.r();
               var10.d(var4.h_());
            }
         }

         a(var2, this, "commands.give.success", new Object[]{var8.B(), Integer.valueOf(var6), var4.h_()});
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, var1.J()):(var3.length == 2?a(var3, Item.f.c()):Collections.emptyList());
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 0;
   }
}
