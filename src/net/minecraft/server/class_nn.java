package net.minecraft.server;

import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.Item;
import net.minecraft.server.Statistic;

public class class_nn extends Statistic {
   private final Item a;

   public class_nn(String var1, String var2, IChatBaseComponent var3, Item var4) {
      super(var1 + var2, var3);
      this.a = var4;
   }
}
