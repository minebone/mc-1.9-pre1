package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.class_aab;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aec;
import net.minecraft.server.class_aff;
import net.minecraft.server.class_di;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;

public class class_aev extends class_aec {
   public String a(ItemStack var1) {
      return class_di.a(class_aff.c(var1).b("splash_potion.effect."));
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      if(!var3.abilities.d) {
         --var1.b;
      }

      var2.a((EntityHuman)null, var3.locX, var3.locY, var3.locZ, class_ng.fP, EnumSoundCategory.NEUTRAL, 0.5F, 0.4F / (i.nextFloat() * 0.4F + 0.8F));
      if(!var2.E) {
         class_aab var5 = new class_aab(var2, var3, var1);
         var5.a(var3, var3.pitch, var3.yaw, -20.0F, 0.5F, 1.0F);
         var2.a((Entity)var5);
      }

      var3.b(StatisticList.b((Item)this));
      return new class_qo(EnumResult.SUCCESS, var1);
   }
}
