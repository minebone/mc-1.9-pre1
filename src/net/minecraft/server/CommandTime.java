package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;

public class CommandTime extends CommandAbstract {
   public String c() {
      return "time";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.time.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length > 1) {
         int var4;
         if(var3[0].equals("set")) {
            if(var3[1].equals("day")) {
               var4 = 1000;
            } else if(var3[1].equals("night")) {
               var4 = 13000;
            } else {
               var4 = a(var3[1], 0);
            }

            this.a(var1, var4);
            a(var2, this, "commands.time.set", new Object[]{Integer.valueOf(var4)});
            return;
         }

         if(var3[0].equals("add")) {
            var4 = a(var3[1], 0);
            this.b(var1, var4);
            a(var2, this, "commands.time.added", new Object[]{Integer.valueOf(var4)});
            return;
         }

         if(var3[0].equals("query")) {
            if(var3[1].equals("daytime")) {
               var4 = (int)(var2.e().Q() % 24000L);
               var2.a(CommandObjectiveExecutor.EnumCommandResult.QUERY_RESULT, var4);
               a(var2, this, "commands.time.query", new Object[]{Integer.valueOf(var4)});
               return;
            }

            if(var3[1].equals("day")) {
               var4 = (int)(var2.e().Q() / 24000L % 2147483647L);
               var2.a(CommandObjectiveExecutor.EnumCommandResult.QUERY_RESULT, var4);
               a(var2, this, "commands.time.query", new Object[]{Integer.valueOf(var4)});
               return;
            }

            if(var3[1].equals("gametime")) {
               var4 = (int)(var2.e().P() % 2147483647L);
               var2.a(CommandObjectiveExecutor.EnumCommandResult.QUERY_RESULT, var4);
               a(var2, this, "commands.time.query", new Object[]{Integer.valueOf(var4)});
               return;
            }
         }
      }

      throw new class_cf("commands.time.usage", new Object[0]);
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, new String[]{"set", "add", "query"}):(var3.length == 2 && var3[0].equals("set")?a(var3, new String[]{"day", "night"}):(var3.length == 2 && var3[0].equals("query")?a(var3, new String[]{"daytime", "gametime", "day"}):Collections.emptyList()));
   }

   protected void a(MinecraftServer var1, int var2) {
      for(int var3 = 0; var3 < var1.d.length; ++var3) {
         var1.d[var3].b((long)var2);
      }

   }

   protected void b(MinecraftServer var1, int var2) {
      for(int var3 = 0; var3 < var1.d.length; ++var3) {
         WorldServer var4 = var1.d[var3];
         var4.b(var4.Q() + (long)var2);
      }

   }
}
