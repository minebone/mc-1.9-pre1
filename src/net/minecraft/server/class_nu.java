package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Map;
import net.minecraft.server.Achievement;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Statistic;
import net.minecraft.server.class_nr;
import net.minecraft.server.class_ns;

public class class_nu {
   protected final Map a = Maps.newConcurrentMap();

   public boolean a(Achievement var1) {
      return this.a((Statistic)var1) > 0;
   }

   public boolean b(Achievement var1) {
      return var1.c == null || this.a(var1.c);
   }

   public void b(EntityHuman var1, Statistic var2, int var3) {
      if(!var2.d() || this.b((Achievement)var2)) {
         this.a(var1, var2, this.a(var2) + var3);
      }
   }

   public void a(EntityHuman var1, Statistic var2, int var3) {
      class_nr var4 = (class_nr)this.a.get(var2);
      if(var4 == null) {
         var4 = new class_nr();
         this.a.put(var2, var4);
      }

      var4.a(var3);
   }

   public int a(Statistic var1) {
      class_nr var2 = (class_nr)this.a.get(var1);
      return var2 == null?0:var2.a();
   }

   public class_ns b(Statistic var1) {
      class_nr var2 = (class_nr)this.a.get(var1);
      return var2 != null?var2.b():null;
   }

   public class_ns a(Statistic var1, class_ns var2) {
      class_nr var3 = (class_nr)this.a.get(var1);
      if(var3 == null) {
         var3 = new class_nr();
         this.a.put(var1, var3);
      }

      var3.a(var2);
      return var2;
   }
}
