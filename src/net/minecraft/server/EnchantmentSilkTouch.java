package net.minecraft.server;

import net.minecraft.server.Enchantment;
import net.minecraft.server.class_agm;
import net.minecraft.server.class_agp;
import net.minecraft.server.EnumInventorySlot;

public class EnchantmentSilkTouch extends Enchantment {
   protected EnchantmentSilkTouch(Enchantment.class_a_in_class_agl var1, EnumInventorySlot... var2) {
      super(var1, class_agm.DIGGER, var2);
      this.c("untouching");
   }

   public int a(int var1) {
      return 15;
   }

   public int b(int var1) {
      return super.a(var1) + 50;
   }

   public int b() {
      return 1;
   }

   public boolean a(Enchantment var1) {
      return super.a(var1) && var1 != class_agp.t;
   }
}
