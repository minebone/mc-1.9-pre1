package net.minecraft.server;

import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.class_apw;
import net.minecraft.server.class_aqj;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_auc;
import net.minecraft.server.Material;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WorldGenDungeons extends class_auc {
   private static final Logger a = LogManager.getLogger();
   private static final String[] b = new String[]{"Skeleton", "Zombie", "Zombie", "Spider"};

   public boolean b(World var1, Random var2, BlockPosition var3) {
      boolean var4 = true;
      int var5 = var2.nextInt(2) + 2;
      int var6 = -var5 - 1;
      int var7 = var5 + 1;
      boolean var8 = true;
      boolean var9 = true;
      int var10 = var2.nextInt(2) + 2;
      int var11 = -var10 - 1;
      int var12 = var10 + 1;
      int var13 = 0;

      int var14;
      int var15;
      int var16;
      BlockPosition var17;
      for(var14 = var6; var14 <= var7; ++var14) {
         for(var15 = -1; var15 <= 4; ++var15) {
            for(var16 = var11; var16 <= var12; ++var16) {
               var17 = var3.a(var14, var15, var16);
               Material var18 = var1.getType(var17).getMaterial();
               boolean var19 = var18.a();
               if(var15 == -1 && !var19) {
                  return false;
               }

               if(var15 == 4 && !var19) {
                  return false;
               }

               if((var14 == var6 || var14 == var7 || var16 == var11 || var16 == var12) && var15 == 0 && var1.d(var17) && var1.d(var17.a())) {
                  ++var13;
               }
            }
         }
      }

      if(var13 >= 1 && var13 <= 5) {
         for(var14 = var6; var14 <= var7; ++var14) {
            for(var15 = 3; var15 >= -1; --var15) {
               for(var16 = var11; var16 <= var12; ++var16) {
                  var17 = var3.a(var14, var15, var16);
                  if(var14 != var6 && var15 != -1 && var16 != var11 && var14 != var7 && var15 != 4 && var16 != var12) {
                     if(var1.getType(var17).getBlock() != Blocks.ae) {
                        var1.g(var17);
                     }
                  } else if(var17.q() >= 0 && !var1.getType(var17.b()).getMaterial().a()) {
                     var1.g(var17);
                  } else if(var1.getType(var17).getMaterial().a() && var1.getType(var17).getBlock() != Blocks.ae) {
                     if(var15 == -1 && var2.nextInt(4) != 0) {
                        var1.a((BlockPosition)var17, (IBlockData)Blocks.Y.u(), 2);
                     } else {
                        var1.a((BlockPosition)var17, (IBlockData)Blocks.e.u(), 2);
                     }
                  }
               }
            }
         }

         for(var14 = 0; var14 < 2; ++var14) {
            for(var15 = 0; var15 < 3; ++var15) {
               var16 = var3.p() + var2.nextInt(var5 * 2 + 1) - var5;
               int var24 = var3.q();
               int var25 = var3.r() + var2.nextInt(var10 * 2 + 1) - var10;
               BlockPosition var26 = new BlockPosition(var16, var24, var25);
               if(var1.d(var26)) {
                  int var20 = 0;
                  Iterator var21 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

                  while(var21.hasNext()) {
                     EnumDirection var22 = (EnumDirection)var21.next();
                     if(var1.getType(var26.a(var22)).getMaterial().a()) {
                        ++var20;
                     }
                  }

                  if(var20 == 1) {
                     var1.a((BlockPosition)var26, (IBlockData)Blocks.ae.f(var1, var26, Blocks.ae.u()), 2);
                     TileEntity var27 = var1.r(var26);
                     if(var27 instanceof class_apw) {
                        ((class_apw)var27).a(class_azs.d, var2.nextLong());
                     }
                     break;
                  }
               }
            }
         }

         var1.a((BlockPosition)var3, (IBlockData)Blocks.ac.u(), 2);
         TileEntity var23 = var1.r(var3);
         if(var23 instanceof class_aqj) {
            ((class_aqj)var23).b().a(this.a(var2));
         } else {
            a.error("Failed to fetch mob spawner entity at (" + var3.p() + ", " + var3.q() + ", " + var3.r() + ")");
         }

         return true;
      } else {
         return false;
      }
   }

   private String a(Random var1) {
      return b[var1.nextInt(b.length)];
   }
}
