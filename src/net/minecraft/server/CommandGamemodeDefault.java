package net.minecraft.server;

import java.util.Iterator;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.WorldSettings;
import net.minecraft.server.class_ah;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;

public class CommandGamemodeDefault extends class_ah {
   public String c() {
      return "defaultgamemode";
   }

   public String b(ICommandListener var1) {
      return "commands.defaultgamemode.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length <= 0) {
         throw new class_cf("commands.defaultgamemode.usage", new Object[0]);
      } else {
         WorldSettings.EnumGamemode var4 = this.c(var2, var3[0]);
         this.a(var4, var1);
         a(var2, this, "commands.defaultgamemode.success", new Object[]{new ChatMessage("gameMode." + var4.b(), new Object[0])});
      }
   }

   protected void a(WorldSettings.EnumGamemode var1, MinecraftServer var2) {
      var2.a(var1);
      if(var2.at()) {
         Iterator var3 = var2.getPlayerList().v().iterator();

         while(var3.hasNext()) {
            EntityPlayer var4 = (EntityPlayer)var3.next();
            var4.a(var1);
         }
      }

   }
}
