package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.TileEntity;
import net.minecraft.server.TileEntityEnchantTable;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_aoa;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_qm;
import net.minecraft.server.class_rz;

public class class_akz extends class_ajm {
   protected static final AxisAlignedBB a = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.75D, 1.0D);

   protected class_akz() {
      super(Material.e, MaterialMapColor.D);
      this.d(0);
      this.a(CreativeModeTab.c);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return a;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.MODEL;
   }

   public TileEntity a(World var1, int var2) {
      return new TileEntityEnchantTable();
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(var1.E) {
         return true;
      } else {
         TileEntity var11 = var1.r(var2);
         if(var11 instanceof TileEntityEnchantTable) {
            var4.a((class_qm)((TileEntityEnchantTable)var11));
         }

         return true;
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, class_rz var4, ItemStack var5) {
      super.a(var1, var2, var3, var4, var5);
      if(var5.s()) {
         TileEntity var6 = var1.r(var2);
         if(var6 instanceof TileEntityEnchantTable) {
            ((TileEntityEnchantTable)var6).a(var5.q());
         }
      }

   }
}
