package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.BiomeDecorator;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;

public class class_ajg extends BiomeDecorator {
   public void a(World var1, Random var2, BiomeBase var3, BlockPosition var4) {
      BlockPosition var5 = var1.R();
      boolean var6 = true;
      double var7 = var5.k(var4.a(8, var5.q(), 8));
      if(var7 <= 1024.0D) {
         BlockPosition var9 = new BlockPosition(var5.p() - 16, var5.q() - 1, var5.r() - 16);
         BlockPosition var10 = new BlockPosition(var5.p() + 16, var5.q() - 1, var5.r() + 16);
         BlockPosition.class_a_in_class_cj var11 = new BlockPosition.class_a_in_class_cj(var9);

         for(int var12 = var4.r(); var12 < var4.r() + 16; ++var12) {
            for(int var13 = var4.p(); var13 < var4.p() + 16; ++var13) {
               if(var12 >= var9.r() && var12 <= var10.r() && var13 >= var9.p() && var13 <= var10.p()) {
                  var11.c(var13, var11.q(), var12);
                  if(var5.p() == var13 && var5.r() == var12) {
                     var1.a((BlockPosition)var11, (IBlockData)Blocks.e.u(), 2);
                  } else {
                     var1.a((BlockPosition)var11, (IBlockData)Blocks.b.u(), 2);
                  }
               }
            }
         }

      }
   }
}
