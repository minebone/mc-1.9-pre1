package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.ItemStack;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_qu;
import net.minecraft.server.class_tm;
import net.minecraft.server.class_yc;
import net.minecraft.server.class_zd;

public class class_uq extends class_tm {
   private int e;
   private class_zd f;

   public class_uq(class_zd var1) {
      super(var1, class_zd.class, 3.0F, 0.02F);
      this.f = var1;
   }

   public void c() {
      super.c();
      if(this.f.dg() && this.b instanceof class_zd && ((class_zd)this.b).dh()) {
         this.e = 10;
      } else {
         this.e = 0;
      }

   }

   public void e() {
      super.e();
      if(this.e > 0) {
         --this.e;
         if(this.e == 0) {
            class_qu var1 = this.f.de();

            for(int var2 = 0; var2 < var1.u_(); ++var2) {
               ItemStack var3 = var1.a(var2);
               ItemStack var4 = null;
               if(var3 != null) {
                  Item var5 = var3.b();
                  int var6;
                  if((var5 == Items.R || var5 == Items.cc || var5 == Items.cb || var5 == Items.cV) && var3.b > 3) {
                     var6 = var3.b / 2;
                     var3.b -= var6;
                     var4 = new ItemStack(var5, var6, var3.i());
                  } else if(var5 == Items.Q && var3.b > 5) {
                     var6 = var3.b / 2 / 3 * 3;
                     int var7 = var6 / 3;
                     var3.b -= var6;
                     var4 = new ItemStack(Items.R, var7, 0);
                  }

                  if(var3.b <= 0) {
                     var1.a(var2, (ItemStack)null);
                  }
               }

               if(var4 != null) {
                  double var11 = this.f.locY - 0.30000001192092896D + (double)this.f.bm();
                  class_yc var12 = new class_yc(this.f.world, this.f.locX, var11, this.f.locZ, var4);
                  float var8 = 0.3F;
                  float var9 = this.f.aO;
                  float var10 = this.f.pitch;
                  var12.motX = (double)(-MathHelper.a(var9 * 0.017453292F) * MathHelper.b(var10 * 0.017453292F) * var8);
                  var12.motZ = (double)(MathHelper.b(var9 * 0.017453292F) * MathHelper.b(var10 * 0.017453292F) * var8);
                  var12.motY = (double)(-MathHelper.a(var10 * 0.017453292F) * var8 + 0.1F);
                  var12.q();
                  this.f.world.a((Entity)var12);
                  break;
               }
            }
         }
      }

   }
}
