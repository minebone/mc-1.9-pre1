package net.minecraft.server;

import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityCreature;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.EnumSkyBlock;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.class_aby;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agn;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_yk;
import net.minecraft.server.class_ys;

public abstract class class_yp extends EntityCreature implements class_yk {
   public class_yp(World var1) {
      super(var1);
      this.b_ = 5;
   }

   public EnumSoundCategory by() {
      return EnumSoundCategory.HOSTILE;
   }

   public void n() {
      this.bX();
      float var1 = this.e(1.0F);
      if(var1 > 0.5F) {
         this.aU += 2;
      }

      super.n();
   }

   public void m() {
      super.m();
      if(!this.world.E && this.world.ae() == EnumDifficulty.PEACEFUL) {
         this.S();
      }

   }

   protected class_nf Z() {
      return class_ng.cC;
   }

   protected class_nf aa() {
      return class_ng.cB;
   }

   public boolean a(DamageSource var1, float var2) {
      return this.b(var1)?false:super.a(var1, var2);
   }

   protected class_nf bQ() {
      return class_ng.cz;
   }

   protected class_nf bR() {
      return class_ng.cy;
   }

   protected class_nf e(int var1) {
      return var1 > 4?class_ng.cx:class_ng.cA;
   }

   public boolean B(Entity var1) {
      float var2 = (float)this.a(class_ys.e).e();
      int var3 = 0;
      if(var1 instanceof class_rz) {
         var2 += class_agn.a(this.ca(), ((class_rz)var1).bZ());
         var3 += class_agn.a((class_rz)this);
      }

      boolean var4 = var1.a(DamageSource.a((class_rz)this), var2);
      if(var4) {
         if(var3 > 0 && var1 instanceof class_rz) {
            ((class_rz)var1).a(this, (float)var3 * 0.5F, (double)MathHelper.a(this.yaw * 0.017453292F), (double)(-MathHelper.b(this.yaw * 0.017453292F)));
            this.motX *= 0.6D;
            this.motZ *= 0.6D;
         }

         int var5 = class_agn.b(this);
         if(var5 > 0) {
            var1.g(var5 * 4);
         }

         if(var1 instanceof EntityHuman) {
            EntityHuman var6 = (EntityHuman)var1;
            ItemStack var7 = this.ca();
            ItemStack var8 = var6.cr()?var6.cu():null;
            if(var7 != null && var8 != null && var7.b() instanceof class_aby && var8.b() == Items.cQ) {
               float var9 = 0.25F + (float)class_agn.e(this) * 0.05F;
               if(this.random.nextFloat() < var9) {
                  var6.cZ().a(Items.cQ, 100);
                  this.world.a((Entity)var6, (byte)30);
               }
            }
         }

         this.a(this, var1);
      }

      return var4;
   }

   public float a(BlockPosition var1) {
      return 0.5F - this.world.n(var1);
   }

   protected boolean s_() {
      BlockPosition var1 = new BlockPosition(this.locX, this.bk().b, this.locZ);
      if(this.world.b(EnumSkyBlock.SKY, var1) > this.random.nextInt(32)) {
         return false;
      } else {
         int var2 = this.world.k(var1);
         if(this.world.V()) {
            int var3 = this.world.af();
            this.world.c(10);
            var2 = this.world.k(var1);
            this.world.c(var3);
         }

         return var2 <= this.random.nextInt(8);
      }
   }

   public boolean cF() {
      return this.world.ae() != EnumDifficulty.PEACEFUL && this.s_() && super.cF();
   }

   protected void bz() {
      super.bz();
      this.bY().b(class_ys.e);
   }

   protected boolean bC() {
      return true;
   }
}
