package net.minecraft.server;

import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BlockDirt;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aju;
import net.minecraft.server.class_ajx;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_aui;
import net.minecraft.server.BlockPosition;

public class class_amt extends class_ajx implements class_aju {
   protected static final AxisAlignedBB a = new AxisAlignedBB(0.30000001192092896D, 0.0D, 0.30000001192092896D, 0.699999988079071D, 0.4000000059604645D, 0.699999988079071D);

   protected class_amt() {
      this.a(true);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return a;
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(var4.nextInt(25) == 0) {
         int var5 = 5;
         boolean var6 = true;
         Iterator var7 = BlockPosition.b(var2.a(-4, -1, -4), var2.a(4, 1, 4)).iterator();

         while(var7.hasNext()) {
            BlockPosition var8 = (BlockPosition)var7.next();
            if(var1.getType(var8).getBlock() == this) {
               --var5;
               if(var5 <= 0) {
                  return;
               }
            }
         }

         BlockPosition var9 = var2.a(var4.nextInt(3) - 1, var4.nextInt(2) - var4.nextInt(2), var4.nextInt(3) - 1);

         for(int var10 = 0; var10 < 4; ++var10) {
            if(var1.d(var9) && this.f(var1, var9, this.u())) {
               var2 = var9;
            }

            var9 = var2.a(var4.nextInt(3) - 1, var4.nextInt(2) - var4.nextInt(2), var4.nextInt(3) - 1);
         }

         if(var1.d(var9) && this.f(var1, var9, this.u())) {
            var1.a((BlockPosition)var9, (IBlockData)this.u(), 2);
         }
      }

   }

   public boolean a(World var1, BlockPosition var2) {
      return super.a(var1, var2) && this.f(var1, var2, this.u());
   }

   protected boolean i(IBlockData var1) {
      return var1.b();
   }

   public boolean f(World var1, BlockPosition var2, IBlockData var3) {
      if(var2.q() >= 0 && var2.q() < 256) {
         IBlockData var4 = var1.getType(var2.b());
         return var4.getBlock() == Blocks.bw?true:(var4.getBlock() == Blocks.d && var4.get(BlockDirt.a) == BlockDirt.EnumDirtVariant.PODZOL?true:var1.j(var2) < 13 && this.i(var4));
      } else {
         return false;
      }
   }

   public boolean c(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      var1.g(var2);
      class_aui var5 = null;
      if(this == Blocks.P) {
         var5 = new class_aui(Blocks.bg);
      } else if(this == Blocks.Q) {
         var5 = new class_aui(Blocks.bh);
      }

      if(var5 != null && var5.b(var1, var4, var2)) {
         return true;
      } else {
         var1.a((BlockPosition)var2, (IBlockData)var3, 3);
         return false;
      }
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, boolean var4) {
      return true;
   }

   public boolean a(World var1, Random var2, BlockPosition var3, IBlockData var4) {
      return (double)var2.nextFloat() < 0.4D;
   }

   public void b(World var1, Random var2, BlockPosition var3, IBlockData var4) {
      this.c(var1, var3, var4, var2);
   }
}
