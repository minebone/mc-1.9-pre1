package net.minecraft.server;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatHoverable;
import net.minecraft.server.EnumChatFormat;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.IScoreboardCriteria;
import net.minecraft.server.StatisticList;
import net.minecraft.server.class_bbw;
import net.minecraft.server.class_nq;

public class Statistic {
   public final String e;
   private final IChatBaseComponent a;
   public boolean f;
   private final class_nq b;
   private final IScoreboardCriteria c;
   private Class d;
   private static NumberFormat k = NumberFormat.getIntegerInstance(Locale.US);
   public static class_nq g = new class_nq() {
   };
   private static DecimalFormat l = new DecimalFormat("########0.00");
   public static class_nq h = new class_nq() {
   };
   public static class_nq i = new class_nq() {
   };
   public static class_nq j = new class_nq() {
   };

   public Statistic(String var1, IChatBaseComponent var2, class_nq var3) {
      this.e = var1;
      this.a = var2;
      this.b = var3;
      this.c = new class_bbw(this);
      IScoreboardCriteria.a.put(this.c.a(), this.c);
   }

   public Statistic(String var1, IChatBaseComponent var2) {
      this(var1, var2, g);
   }

   public Statistic i() {
      this.f = true;
      return this;
   }

   public Statistic h() {
      if(StatisticList.a.containsKey(this.e)) {
         throw new RuntimeException("Duplicate stat id: \"" + ((Statistic)StatisticList.a.get(this.e)).a + "\" and \"" + this.a + "\" at id " + this.e);
      } else {
         StatisticList.b.add(this);
         StatisticList.a.put(this.e, this);
         return this;
      }
   }

   public boolean d() {
      return false;
   }

   public IChatBaseComponent e() {
      IChatBaseComponent var1 = this.a.f();
      var1.b().a(EnumChatFormat.GRAY);
      var1.b().a(new ChatHoverable(ChatHoverable.EnumHoverAction.SHOW_ACHIEVEMENT, new ChatComponentText(this.e)));
      return var1;
   }

   public IChatBaseComponent j() {
      IChatBaseComponent var1 = this.e();
      IChatBaseComponent var2 = (new ChatComponentText("[")).a(var1).a("]");
      var2.a(var1.b());
      return var2;
   }

   public boolean equals(Object var1) {
      if(this == var1) {
         return true;
      } else if(var1 != null && this.getClass() == var1.getClass()) {
         Statistic var2 = (Statistic)var1;
         return this.e.equals(var2.e);
      } else {
         return false;
      }
   }

   public int hashCode() {
      return this.e.hashCode();
   }

   public String toString() {
      return "Stat{id=" + this.e + ", nameId=" + this.a + ", awardLocallyOnly=" + this.f + ", formatter=" + this.b + ", objectiveCriteria=" + this.c + '}';
   }

   public IScoreboardCriteria k() {
      return this.c;
   }

   public Class l() {
      return this.d;
   }

   public Statistic b(Class var1) {
      this.d = var1;
      return this;
   }
}
