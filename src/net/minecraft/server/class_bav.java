package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import java.util.Random;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.class_agn;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_rz;

public class class_bav implements class_baq {
   private final float a;
   private final float b;

   public class_bav(float var1, float var2) {
      this.a = var1;
      this.b = var2;
   }

   public boolean a(Random var1, class_azy var2) {
      int var3 = 0;
      if(var2.c() instanceof class_rz) {
         var3 = class_agn.h((class_rz)var2.c());
      }

      return var1.nextFloat() < this.a + (float)var3 * this.b;
   }

   public static class class_a_in_class_bav extends class_baq.class_a_in_class_baq {
      protected class_a_in_class_bav() {
         super(new class_kk("random_chance_with_looting"), class_bav.class);
      }

      public void a(JsonObject var1, class_bav var2, JsonSerializationContext var3) {
         var1.addProperty("chance", (Number)Float.valueOf(var2.a));
         var1.addProperty("looting_multiplier", (Number)Float.valueOf(var2.b));
      }

      public class_bav a(JsonObject var1, JsonDeserializationContext var2) {
         return new class_bav(ChatDeserializer.l(var1, "chance"), ChatDeserializer.l(var1, "looting_multiplier"));
      }

      // $FF: synthetic method
      public class_baq b(JsonObject var1, JsonDeserializationContext var2) {
         return this.a(var1, var2);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(JsonObject var1, class_baq var2, JsonSerializationContext var3) {
         this.a(var1, (class_bav)var2, var3);
      }
   }
}
