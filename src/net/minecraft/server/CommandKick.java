package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.ExceptionPlayerNotFound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;

public class CommandKick extends CommandAbstract {
   public String c() {
      return "kick";
   }

   public int a() {
      return 3;
   }

   public String b(ICommandListener var1) {
      return "commands.kick.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length > 0 && var3[0].length() > 1) {
         EntityPlayer var4 = var1.getPlayerList().a(var3[0]);
         String var5 = "Kicked by an operator.";
         boolean var6 = false;
         if(var4 == null) {
            throw new ExceptionPlayerNotFound();
         } else {
            if(var3.length >= 2) {
               var5 = a(var2, var3, 1).c();
               var6 = true;
            }

            var4.a.c(var5);
            if(var6) {
               a(var2, this, "commands.kick.success.reason", new Object[]{var4.h_(), var5});
            } else {
               a(var2, this, "commands.kick.success", new Object[]{var4.h_()});
            }

         }
      } else {
         throw new class_cf("commands.kick.usage", new Object[0]);
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length >= 1?a(var3, var1.J()):Collections.emptyList();
   }
}
