package net.minecraft.server;

import java.util.Iterator;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_ahu;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutBlockBreakAnimation;
import net.minecraft.server.PacketPlayOutWorldEvent;
import net.minecraft.server.PacketPlayOutNamedSoundSomething;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.class_nf;
import net.minecraft.server.EnumSoundCategory;

public class class_lq implements class_ahu {
   private MinecraftServer a;
   private WorldServer b;

   public class_lq(MinecraftServer var1, WorldServer var2) {
      this.a = var1;
      this.b = var2;
   }

   public void a(int var1, boolean var2, double var3, double var5, double var7, double var9, double var11, double var13, int... var15) {
   }

   public void a(Entity var1) {
      this.b.v().a(var1);
      if(var1 instanceof EntityPlayer) {
         this.b.s.a((EntityPlayer)var1);
      }

   }

   public void b(Entity var1) {
      this.b.v().b(var1);
      this.b.ad().a(var1);
      if(var1 instanceof EntityPlayer) {
         this.b.s.b((EntityPlayer)var1);
      }

   }

   public void a(EntityHuman var1, class_nf var2, EnumSoundCategory var3, double var4, double var6, double var8, float var10, float var11) {
      this.a.getPlayerList().a(var1, var4, var6, var8, var10 > 1.0F?(double)(16.0F * var10):16.0D, this.b.s.p().a(), new PacketPlayOutNamedSoundSomething(var2, var3, var4, var6, var8, var10, var11));
   }

   public void a(int var1, int var2, int var3, int var4, int var5, int var6) {
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, IBlockData var4, int var5) {
      this.b.w().a(var2);
   }

   public void a(BlockPosition var1) {
   }

   public void a(class_nf var1, BlockPosition var2) {
   }

   public void a(EntityHuman var1, int var2, BlockPosition var3, int var4) {
      this.a.getPlayerList().a(var1, (double)var3.p(), (double)var3.q(), (double)var3.r(), 64.0D, this.b.s.p().a(), new PacketPlayOutWorldEvent(var2, var3, var4, false));
   }

   public void a(int var1, BlockPosition var2, int var3) {
      this.a.getPlayerList().sendAll((Packet)(new PacketPlayOutWorldEvent(var1, var2, var3, true)));
   }

   public void b(int var1, BlockPosition var2, int var3) {
      Iterator var4 = this.a.getPlayerList().v().iterator();

      while(var4.hasNext()) {
         EntityPlayer var5 = (EntityPlayer)var4.next();
         if(var5 != null && var5.world == this.b && var5.getId() != var1) {
            double var6 = (double)var2.p() - var5.locX;
            double var8 = (double)var2.q() - var5.locY;
            double var10 = (double)var2.r() - var5.locZ;
            if(var6 * var6 + var8 * var8 + var10 * var10 < 1024.0D) {
               var5.a.a((Packet)(new PacketPlayOutBlockBreakAnimation(var1, var2, var3)));
            }
         }
      }

   }
}
