package net.minecraft.server;

import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.ICommandListener;

public class CommandSaveOff extends CommandAbstract {
   public String c() {
      return "save-off";
   }

   public String b(ICommandListener var1) {
      return "commands.save-off.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      boolean var4 = false;

      for(int var5 = 0; var5 < var1.d.length; ++var5) {
         if(var1.d[var5] != null) {
            WorldServer var6 = var1.d[var5];
            if(!var6.b) {
               var6.b = true;
               var4 = true;
            }
         }
      }

      if(var4) {
         a(var2, this, "commands.save.disabled", new Object[0]);
      } else {
         throw new class_bz("commands.save-off.alreadyOff", new Object[0]);
      }
   }
}
