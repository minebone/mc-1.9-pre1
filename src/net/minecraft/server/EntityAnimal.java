package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rn;
import net.minecraft.server.class_rp;

public abstract class EntityAnimal extends class_rn implements class_rp {
   protected Block by = Blocks.c;
   private int bv;
   private EntityHuman bw;

   public EntityAnimal(World var1) {
      super(var1);
   }

   protected void M() {
      if(this.l() != 0) {
         this.bv = 0;
      }

      super.M();
   }

   public void n() {
      super.n();
      if(this.l() != 0) {
         this.bv = 0;
      }

      if(this.bv > 0) {
         --this.bv;
         if(this.bv % 10 == 0) {
            double var1 = this.random.nextGaussian() * 0.02D;
            double var3 = this.random.nextGaussian() * 0.02D;
            double var5 = this.random.nextGaussian() * 0.02D;
            this.world.a(EnumParticle.HEART, this.locX + (double)(this.random.nextFloat() * this.width * 2.0F) - (double)this.width, this.locY + 0.5D + (double)(this.random.nextFloat() * this.length), this.locZ + (double)(this.random.nextFloat() * this.width * 2.0F) - (double)this.width, var1, var3, var5, new int[0]);
         }
      }

   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b((DamageSource)var1)) {
         return false;
      } else {
         this.bv = 0;
         return super.a(var1, var2);
      }
   }

   public float a(BlockPosition var1) {
      return this.world.getType(var1.b()).getBlock() == Blocks.c?10.0F:this.world.n(var1) - 0.5F;
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("InLove", this.bv);
   }

   public double aw() {
      return 0.29D;
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.bv = var1.h("InLove");
   }

   public boolean cF() {
      int var1 = MathHelper.c(this.locX);
      int var2 = MathHelper.c(this.bk().b);
      int var3 = MathHelper.c(this.locZ);
      BlockPosition var4 = new BlockPosition(var1, var2, var3);
      return this.world.getType(var4.b()).getBlock() == this.by && this.world.j(var4) > 8 && super.cF();
   }

   public int C() {
      return 120;
   }

   protected boolean K() {
      return false;
   }

   protected int b(EntityHuman var1) {
      return 1 + this.world.r.nextInt(3);
   }

   public boolean e(ItemStack var1) {
      return var1 == null?false:var1.b() == Items.Q;
   }

   public boolean a(EntityHuman var1, EnumHand var2, ItemStack var3) {
      if(var3 != null) {
         if(this.e(var3) && this.l() == 0 && this.bv <= 0) {
            this.a(var1, var3);
            this.c(var1);
            return true;
         }

         if(this.m_() && this.e(var3)) {
            this.a(var1, var3);
            this.a((int)((float)(-this.l() / 20) * 0.1F), true);
            return true;
         }
      }

      return super.a(var1, var2, var3);
   }

   protected void a(EntityHuman var1, ItemStack var2) {
      if(!var1.abilities.d) {
         --var2.b;
      }

   }

   public void c(EntityHuman var1) {
      this.bv = 600;
      this.bw = var1;
      this.world.a((Entity)this, (byte)18);
   }

   public EntityHuman de() {
      return this.bw;
   }

   public boolean df() {
      return this.bv > 0;
   }

   public void dg() {
      this.bv = 0;
   }

   public boolean a(EntityAnimal var1) {
      return var1 == this?false:(var1.getClass() != this.getClass()?false:this.df() && var1.df());
   }
}
