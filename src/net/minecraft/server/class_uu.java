package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.EntityCreature;
import net.minecraft.server.EntityTameableAnimal;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_vb;

public class class_uu extends class_vb {
   private final boolean a;
   private int b;
   private final Class[] c;

   public class_uu(EntityCreature var1, boolean var2, Class... var3) {
      super(var1, true);
      this.a = var2;
      this.c = var3;
      this.a(1);
   }

   public boolean a() {
      int var1 = this.e.bG();
      class_rz var2 = this.e.bF();
      return var1 != this.b && var2 != null && this.a(var2, false);
   }

   public void c() {
      this.e.c(this.e.bF());
      this.g = this.e.A();
      this.b = this.e.bG();
      this.h = 300;
      if(this.a) {
         double var1 = this.f();
         List var3 = this.e.world.a(this.e.getClass(), (new AxisAlignedBB(this.e.locX, this.e.locY, this.e.locZ, this.e.locX + 1.0D, this.e.locY + 1.0D, this.e.locZ + 1.0D)).b(var1, 10.0D, var1));
         Iterator var4 = var3.iterator();

         label55:
         while(true) {
            EntityCreature var5;
            do {
               do {
                  do {
                     do {
                        if(!var4.hasNext()) {
                           break label55;
                        }

                        var5 = (EntityCreature)var4.next();
                     } while(this.e == var5);
                  } while(var5.A() != null);
               } while(this.e instanceof EntityTameableAnimal && ((EntityTameableAnimal)this.e).dc() != ((EntityTameableAnimal)var5).dc());
            } while(var5.r(this.e.bF()));

            boolean var6 = false;
            Class[] var7 = this.c;
            int var8 = var7.length;

            for(int var9 = 0; var9 < var8; ++var9) {
               Class var10 = var7[var9];
               if(var5.getClass() == var10) {
                  var6 = true;
                  break;
               }
            }

            if(!var6) {
               this.a(var5, this.e.bF());
            }
         }
      }

      super.c();
   }

   protected void a(EntityCreature var1, class_rz var2) {
      var1.c(var2);
   }
}
