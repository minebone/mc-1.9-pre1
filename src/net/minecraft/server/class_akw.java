package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityFallingBlock;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_alg;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;

public class class_akw extends Block {
   protected static final AxisAlignedBB a = new AxisAlignedBB(0.0625D, 0.0D, 0.0625D, 0.9375D, 1.0D, 0.9375D);

   public class_akw() {
      super(Material.D, MaterialMapColor.E);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return a;
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      var1.a((BlockPosition)var2, (Block)this, this.a(var1));
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      var1.a((BlockPosition)var2, (Block)this, this.a(var1));
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      this.b(var1, var2);
   }

   private void b(World var1, BlockPosition var2) {
      if(class_alg.i(var1.getType(var2.b())) && var2.q() >= 0) {
         byte var3 = 32;
         if(!class_alg.f && var1.a(var2.a(-var3, -var3, -var3), var2.a(var3, var3, var3))) {
            var1.a((Entity)(new EntityFallingBlock(var1, (double)((float)var2.p() + 0.5F), (double)var2.q(), (double)((float)var2.r() + 0.5F), this.u())));
         } else {
            var1.g(var2);

            BlockPosition var4;
            for(var4 = var2; class_alg.i(var1.getType(var4)) && var4.q() > 0; var4 = var4.b()) {
               ;
            }

            if(var4.q() > 0) {
               var1.a((BlockPosition)var4, (IBlockData)this.u(), 2);
            }
         }

      }
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      this.c(var1, var2);
      return true;
   }

   public void a(World var1, BlockPosition var2, EntityHuman var3) {
      this.c(var1, var2);
   }

   private void c(World var1, BlockPosition var2) {
      IBlockData var3 = var1.getType(var2);
      if(var3.getBlock() == this) {
         for(int var4 = 0; var4 < 1000; ++var4) {
            BlockPosition var5 = var2.a(var1.r.nextInt(16) - var1.r.nextInt(16), var1.r.nextInt(8) - var1.r.nextInt(8), var1.r.nextInt(16) - var1.r.nextInt(16));
            if(var1.getType(var5).getBlock().x == Material.a) {
               if(var1.E) {
                  for(int var6 = 0; var6 < 128; ++var6) {
                     double var7 = var1.r.nextDouble();
                     float var9 = (var1.r.nextFloat() - 0.5F) * 0.2F;
                     float var10 = (var1.r.nextFloat() - 0.5F) * 0.2F;
                     float var11 = (var1.r.nextFloat() - 0.5F) * 0.2F;
                     double var12 = (double)var5.p() + (double)(var2.p() - var5.p()) * var7 + (var1.r.nextDouble() - 0.5D) + 0.5D;
                     double var14 = (double)var5.q() + (double)(var2.q() - var5.q()) * var7 + var1.r.nextDouble() - 0.5D;
                     double var16 = (double)var5.r() + (double)(var2.r() - var5.r()) * var7 + (var1.r.nextDouble() - 0.5D) + 0.5D;
                     var1.a(EnumParticle.PORTAL, var12, var14, var16, (double)var9, (double)var10, (double)var11, new int[0]);
                  }
               } else {
                  var1.a((BlockPosition)var5, (IBlockData)var3, 2);
                  var1.g(var2);
               }

               return;
            }
         }

      }
   }

   public int a(World var1) {
      return 5;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }
}
