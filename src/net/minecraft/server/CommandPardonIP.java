package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandBanIp;
import net.minecraft.server.ExceptionInvalidSyntax;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;

public class CommandPardonIP extends CommandAbstract {
   public String c() {
      return "pardon-ip";
   }

   public int a() {
      return 3;
   }

   public boolean a(MinecraftServer var1, ICommandListener var2) {
      return var1.getPlayerList().i().b() && super.a(var1, var2);
   }

   public String b(ICommandListener var1) {
      return "commands.unbanip.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length == 1 && var3[0].length() > 1) {
         Matcher var4 = CommandBanIp.a.matcher(var3[0]);
         if(var4.matches()) {
            var1.getPlayerList().i().c(var3[0]);
            a(var2, this, "commands.unbanip.success", new Object[]{var3[0]});
         } else {
            throw new ExceptionInvalidSyntax("commands.unbanip.invalid", new Object[0]);
         }
      } else {
         throw new class_cf("commands.unbanip.usage", new Object[0]);
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, var1.getPlayerList().i().a()):Collections.emptyList();
   }
}
