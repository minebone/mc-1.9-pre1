package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import net.minecraft.server.DamageSource;
import net.minecraft.server.EnchantmentArrowDamage;
import net.minecraft.server.EnchantmentArrowKnockback;
import net.minecraft.server.EnchantmentDepthStrider;
import net.minecraft.server.EnchantmentDigging;
import net.minecraft.server.EnchantmentDurability;
import net.minecraft.server.EnchantmentFlameArrows;
import net.minecraft.server.EnchantmentInfiniteArrows;
import net.minecraft.server.EnchantmentLootBonus;
import net.minecraft.server.EnchantmentLure;
import net.minecraft.server.EnchantmentOxygen;
import net.minecraft.server.EnchantmentSilkTouch;
import net.minecraft.server.EnchantmentWaterWorker;
import net.minecraft.server.EnchantmentWeaponDamage;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumMonsterType;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agm;
import net.minecraft.server.class_agq;
import net.minecraft.server.class_ags;
import net.minecraft.server.class_agt;
import net.minecraft.server.class_agv;
import net.minecraft.server.class_agx;
import net.minecraft.server.class_agy;
import net.minecraft.server.class_cx;
import net.minecraft.server.class_di;
import net.minecraft.server.class_kk;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;

public abstract class Enchantment {
   public static final class_cx b = new class_cx();
   private final EnumInventorySlot[] a;
   private final Enchantment.class_a_in_class_agl e;
   public class_agm c;
   protected String d;

   public static Enchantment c(int var0) {
      return (Enchantment)b.a(var0);
   }

   public static int b(Enchantment var0) {
      return b.a(var0);
   }

   public static Enchantment b(String var0) {
      return (Enchantment)b.c(new class_kk(var0));
   }

   protected Enchantment(Enchantment.class_a_in_class_agl var1, class_agm var2, EnumInventorySlot[] var3) {
      this.e = var1;
      this.c = var2;
      this.a = var3;
   }

   public Iterable a(class_rz var1) {
      ArrayList var2 = Lists.newArrayList();
      EnumInventorySlot[] var3 = this.a;
      int var4 = var3.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         EnumInventorySlot var6 = var3[var5];
         ItemStack var7 = var1.a(var6);
         if(var7 != null) {
            var2.add(var7);
         }
      }

      return var2.size() > 0?var2:null;
   }

   public Enchantment.class_a_in_class_agl c() {
      return this.e;
   }

   public int d() {
      return 1;
   }

   public int b() {
      return 1;
   }

   public int a(int var1) {
      return 1 + var1 * 10;
   }

   public int b(int var1) {
      return this.a(var1) + 5;
   }

   public int a(int var1, DamageSource var2) {
      return 0;
   }

   public float a(int var1, EnumMonsterType var2) {
      return 0.0F;
   }

   public boolean a(Enchantment var1) {
      return this != var1;
   }

   public Enchantment c(String var1) {
      this.d = var1;
      return this;
   }

   public String a() {
      return "enchantment." + this.d;
   }

   public String d(int var1) {
      String var2 = class_di.a(this.a());
      return var1 == 1 && this.b() == 1?var2:var2 + " " + class_di.a("enchantment.level." + var1);
   }

   public boolean a(ItemStack var1) {
      return this.c.a(var1.b());
   }

   public void a(class_rz var1, Entity var2, int var3) {
   }

   public void b(class_rz var1, Entity var2, int var3) {
   }

   public boolean e() {
      return false;
   }

   public static void f() {
      EnumInventorySlot[] var0 = new EnumInventorySlot[]{EnumInventorySlot.HEAD, EnumInventorySlot.CHEST, EnumInventorySlot.LEGS, EnumInventorySlot.FEET};
      b.a(0, new class_kk("protection"), new class_agx(Enchantment.class_a_in_class_agl.COMMON, class_agx.class_a_in_class_agx.ALL, var0));
      b.a(1, new class_kk("fire_protection"), new class_agx(Enchantment.class_a_in_class_agl.UNCOMMON, class_agx.class_a_in_class_agx.FIRE, var0));
      b.a(2, new class_kk("feather_falling"), new class_agx(Enchantment.class_a_in_class_agl.UNCOMMON, class_agx.class_a_in_class_agx.FALL, var0));
      b.a(3, new class_kk("blast_protection"), new class_agx(Enchantment.class_a_in_class_agl.RARE, class_agx.class_a_in_class_agx.EXPLOSION, var0));
      b.a(4, new class_kk("projectile_protection"), new class_agx(Enchantment.class_a_in_class_agl.UNCOMMON, class_agx.class_a_in_class_agx.PROJECTILE, var0));
      b.a(5, new class_kk("respiration"), new EnchantmentOxygen(Enchantment.class_a_in_class_agl.RARE, var0));
      b.a(6, new class_kk("aqua_affinity"), new EnchantmentWaterWorker(Enchantment.class_a_in_class_agl.RARE, var0));
      b.a(7, new class_kk("thorns"), new class_agy(Enchantment.class_a_in_class_agl.VERY_RARE, var0));
      b.a(8, new class_kk("depth_strider"), new EnchantmentDepthStrider(Enchantment.class_a_in_class_agl.RARE, var0));
      b.a(9, new class_kk("frost_walker"), new class_ags(Enchantment.class_a_in_class_agl.RARE, new EnumInventorySlot[]{EnumInventorySlot.FEET}));
      b.a(16, new class_kk("sharpness"), new EnchantmentWeaponDamage(Enchantment.class_a_in_class_agl.COMMON, 0, new EnumInventorySlot[]{EnumInventorySlot.MAINHAND}));
      b.a(17, new class_kk("smite"), new EnchantmentWeaponDamage(Enchantment.class_a_in_class_agl.UNCOMMON, 1, new EnumInventorySlot[]{EnumInventorySlot.MAINHAND}));
      b.a(18, new class_kk("bane_of_arthropods"), new EnchantmentWeaponDamage(Enchantment.class_a_in_class_agl.UNCOMMON, 2, new EnumInventorySlot[]{EnumInventorySlot.MAINHAND}));
      b.a(19, new class_kk("knockback"), new class_agt(Enchantment.class_a_in_class_agl.UNCOMMON, new EnumInventorySlot[]{EnumInventorySlot.MAINHAND}));
      b.a(20, new class_kk("fire_aspect"), new class_agq(Enchantment.class_a_in_class_agl.RARE, new EnumInventorySlot[]{EnumInventorySlot.MAINHAND}));
      b.a(21, new class_kk("looting"), new EnchantmentLootBonus(Enchantment.class_a_in_class_agl.RARE, class_agm.WEAPON, new EnumInventorySlot[]{EnumInventorySlot.MAINHAND}));
      b.a(32, new class_kk("efficiency"), new EnchantmentDigging(Enchantment.class_a_in_class_agl.COMMON, new EnumInventorySlot[]{EnumInventorySlot.MAINHAND}));
      b.a(33, new class_kk("silk_touch"), new EnchantmentSilkTouch(Enchantment.class_a_in_class_agl.VERY_RARE, new EnumInventorySlot[]{EnumInventorySlot.MAINHAND}));
      b.a(34, new class_kk("unbreaking"), new EnchantmentDurability(Enchantment.class_a_in_class_agl.UNCOMMON, new EnumInventorySlot[]{EnumInventorySlot.MAINHAND}));
      b.a(35, new class_kk("fortune"), new EnchantmentLootBonus(Enchantment.class_a_in_class_agl.RARE, class_agm.DIGGER, new EnumInventorySlot[]{EnumInventorySlot.MAINHAND}));
      b.a(48, new class_kk("power"), new EnchantmentArrowDamage(Enchantment.class_a_in_class_agl.COMMON, new EnumInventorySlot[]{EnumInventorySlot.MAINHAND}));
      b.a(49, new class_kk("punch"), new EnchantmentArrowKnockback(Enchantment.class_a_in_class_agl.RARE, new EnumInventorySlot[]{EnumInventorySlot.MAINHAND}));
      b.a(50, new class_kk("flame"), new EnchantmentFlameArrows(Enchantment.class_a_in_class_agl.RARE, new EnumInventorySlot[]{EnumInventorySlot.MAINHAND}));
      b.a(51, new class_kk("infinity"), new EnchantmentInfiniteArrows(Enchantment.class_a_in_class_agl.VERY_RARE, new EnumInventorySlot[]{EnumInventorySlot.MAINHAND}));
      b.a(61, new class_kk("luck_of_the_sea"), new EnchantmentLootBonus(Enchantment.class_a_in_class_agl.RARE, class_agm.FISHING_ROD, new EnumInventorySlot[]{EnumInventorySlot.MAINHAND}));
      b.a(62, new class_kk("lure"), new EnchantmentLure(Enchantment.class_a_in_class_agl.RARE, class_agm.FISHING_ROD, new EnumInventorySlot[]{EnumInventorySlot.MAINHAND}));
      b.a(70, new class_kk("mending"), new class_agv(Enchantment.class_a_in_class_agl.RARE, EnumInventorySlot.values()));
   }

   public static enum class_a_in_class_agl {
      COMMON(10),
      UNCOMMON(5),
      RARE(2),
      VERY_RARE(1);

      private final int e;

      private class_a_in_class_agl(int var3) {
         this.e = var3;
      }

      public int a() {
         return this.e;
      }
   }
}
