package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.World;
import net.minecraft.server.class_ajx;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_auc;
import net.minecraft.server.BlockPosition;

public class class_att extends class_auc {
   private class_ajx a;

   public class_att(class_ajx var1) {
      this.a = var1;
   }

   public boolean b(World var1, Random var2, BlockPosition var3) {
      for(int var4 = 0; var4 < 64; ++var4) {
         BlockPosition var5 = var3.a(var2.nextInt(8) - var2.nextInt(8), var2.nextInt(4) - var2.nextInt(4), var2.nextInt(8) - var2.nextInt(8));
         if(var1.d(var5) && (!var1.s.isNotOverworld() || var5.q() < 255) && this.a.f(var1, var5, this.a.u())) {
            var1.a((BlockPosition)var5, (IBlockData)this.a.u(), 2);
         }
      }

      return true;
   }
}
