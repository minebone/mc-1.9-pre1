package net.minecraft.server;

import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_rz;

public class BlockVine extends Block {
   public static final class_arm a = class_arm.a("up");
   public static final class_arm b = class_arm.a("north");
   public static final class_arm c = class_arm.a("east");
   public static final class_arm d = class_arm.a("south");
   public static final class_arm e = class_arm.a("west");
   public static final class_arm[] f = new class_arm[]{a, b, d, e, c};
   protected static final AxisAlignedBB g = new AxisAlignedBB(0.0D, 0.9375D, 0.0D, 1.0D, 1.0D, 1.0D);
   protected static final AxisAlignedBB B = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.0625D, 1.0D, 1.0D);
   protected static final AxisAlignedBB C = new AxisAlignedBB(0.9375D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
   protected static final AxisAlignedBB D = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.0625D);
   protected static final AxisAlignedBB E = new AxisAlignedBB(0.0D, 0.0D, 0.9375D, 1.0D, 1.0D, 1.0D);

   public BlockVine() {
      super(Material.l);
      this.w(this.A.b().set(a, Boolean.valueOf(false)).set(b, Boolean.valueOf(false)).set(c, Boolean.valueOf(false)).set(d, Boolean.valueOf(false)).set(e, Boolean.valueOf(false)));
      this.a(true);
      this.a((CreativeModeTab)CreativeModeTab.c);
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      var1 = var1.b(var2, var3);
      int var4 = 0;
      AxisAlignedBB var5 = j;
      if(((Boolean)var1.get(a)).booleanValue()) {
         var5 = g;
         ++var4;
      }

      if(((Boolean)var1.get(b)).booleanValue()) {
         var5 = D;
         ++var4;
      }

      if(((Boolean)var1.get(c)).booleanValue()) {
         var5 = C;
         ++var4;
      }

      if(((Boolean)var1.get(d)).booleanValue()) {
         var5 = E;
         ++var4;
      }

      if(((Boolean)var1.get(e)).booleanValue()) {
         var5 = B;
         ++var4;
      }

      return var4 == 1?var5:j;
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return var1.set(a, Boolean.valueOf(var2.getType(var3.a()).k()));
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean a(class_ahw var1, BlockPosition var2) {
      return true;
   }

   public boolean b(World var1, BlockPosition var2, EnumDirection var3) {
      switch(BlockVine.SyntheticClass_1.a[var3.ordinal()]) {
      case 1:
         return this.x(var1.getType(var2.a()));
      case 2:
      case 3:
      case 4:
      case 5:
         return this.x(var1.getType(var2.a(var3.d())));
      default:
         return false;
      }
   }

   private boolean x(IBlockData var1) {
      return var1.h() && var1.getMaterial().c();
   }

   private boolean e(World var1, BlockPosition var2, IBlockData var3) {
      IBlockData var4 = var3;
      Iterator var5 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

      while(true) {
         class_arm var7;
         IBlockData var8;
         do {
            EnumDirection var6;
            do {
               do {
                  if(!var5.hasNext()) {
                     if(i(var3) == 0) {
                        return false;
                     }

                     if(var4 != var3) {
                        var1.a((BlockPosition)var2, (IBlockData)var3, 2);
                     }

                     return true;
                  }

                  var6 = (EnumDirection)var5.next();
                  var7 = a(var6);
               } while(!((Boolean)var3.get(var7)).booleanValue());
            } while(this.x(var1.getType(var2.a(var6))));

            var8 = var1.getType(var2.a());
         } while(var8.getBlock() == this && ((Boolean)var8.get(var7)).booleanValue());

         var3 = var3.set(var7, Boolean.valueOf(false));
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!var1.E && !this.e(var1, var2, var3)) {
         this.b(var1, var2, var3, 0);
         var1.g(var2);
      }

   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(!var1.E) {
         if(var1.r.nextInt(4) == 0) {
            byte var5 = 4;
            int var6 = 5;
            boolean var7 = false;

            label191:
            for(int var8 = -var5; var8 <= var5; ++var8) {
               for(int var9 = -var5; var9 <= var5; ++var9) {
                  for(int var10 = -1; var10 <= 1; ++var10) {
                     if(var1.getType(var2.a(var8, var10, var9)).getBlock() == this) {
                        --var6;
                        if(var6 <= 0) {
                           var7 = true;
                           break label191;
                        }
                     }
                  }
               }
            }

            EnumDirection var19 = EnumDirection.a(var4);
            BlockPosition var20 = var2.a();
            if(var19 == EnumDirection.UP && var2.q() < 255 && var1.d(var20)) {
               if(!var7) {
                  IBlockData var22 = var3;
                  Iterator var23 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

                  while(true) {
                     EnumDirection var24;
                     do {
                        if(!var23.hasNext()) {
                           if(((Boolean)var22.get(b)).booleanValue() || ((Boolean)var22.get(c)).booleanValue() || ((Boolean)var22.get(d)).booleanValue() || ((Boolean)var22.get(e)).booleanValue()) {
                              var1.a((BlockPosition)var20, (IBlockData)var22, 2);
                           }

                           return;
                        }

                        var24 = (EnumDirection)var23.next();
                     } while(!var4.nextBoolean() && this.x(var1.getType(var20.a(var24))));

                     var22 = var22.set(a(var24), Boolean.valueOf(false));
                  }
               }
            } else {
               IBlockData var11;
               Block var12;
               BlockPosition var21;
               if(var19.k().c() && !((Boolean)var3.get(a(var19))).booleanValue()) {
                  if(!var7) {
                     var21 = var2.a(var19);
                     var11 = var1.getType(var21);
                     var12 = var11.getBlock();
                     if(var12.x == Material.a) {
                        EnumDirection var25 = var19.e();
                        EnumDirection var26 = var19.f();
                        boolean var27 = ((Boolean)var3.get(a(var25))).booleanValue();
                        boolean var28 = ((Boolean)var3.get(a(var26))).booleanValue();
                        BlockPosition var17 = var21.a(var25);
                        BlockPosition var18 = var21.a(var26);
                        if(var27 && this.x(var1.getType(var17))) {
                           var1.a((BlockPosition)var21, (IBlockData)this.u().set(a(var25), Boolean.valueOf(true)), 2);
                        } else if(var28 && this.x(var1.getType(var18))) {
                           var1.a((BlockPosition)var21, (IBlockData)this.u().set(a(var26), Boolean.valueOf(true)), 2);
                        } else if(var27 && var1.d(var17) && this.x(var1.getType(var2.a(var25)))) {
                           var1.a((BlockPosition)var17, (IBlockData)this.u().set(a(var19.d()), Boolean.valueOf(true)), 2);
                        } else if(var28 && var1.d(var18) && this.x(var1.getType(var2.a(var26)))) {
                           var1.a((BlockPosition)var18, (IBlockData)this.u().set(a(var19.d()), Boolean.valueOf(true)), 2);
                        } else if(this.x(var1.getType(var21.a()))) {
                           var1.a((BlockPosition)var21, (IBlockData)this.u(), 2);
                        }
                     } else if(var12.x.k() && var11.h()) {
                        var1.a((BlockPosition)var2, (IBlockData)var3.set(a(var19), Boolean.valueOf(true)), 2);
                     }

                  }
               } else {
                  if(var2.q() > 1) {
                     var21 = var2.b();
                     var11 = var1.getType(var21);
                     var12 = var11.getBlock();
                     IBlockData var13;
                     Iterator var14;
                     EnumDirection var15;
                     if(var12.x == Material.a) {
                        var13 = var3;
                        var14 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

                        while(var14.hasNext()) {
                           var15 = (EnumDirection)var14.next();
                           if(var4.nextBoolean()) {
                              var13 = var13.set(a(var15), Boolean.valueOf(false));
                           }
                        }

                        if(((Boolean)var13.get(b)).booleanValue() || ((Boolean)var13.get(c)).booleanValue() || ((Boolean)var13.get(d)).booleanValue() || ((Boolean)var13.get(e)).booleanValue()) {
                           var1.a((BlockPosition)var21, (IBlockData)var13, 2);
                        }
                     } else if(var12 == this) {
                        var13 = var11;
                        var14 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

                        while(var14.hasNext()) {
                           var15 = (EnumDirection)var14.next();
                           class_arm var16 = a(var15);
                           if(var4.nextBoolean() && ((Boolean)var3.get(var16)).booleanValue()) {
                              var13 = var13.set(var16, Boolean.valueOf(true));
                           }
                        }

                        if(((Boolean)var13.get(b)).booleanValue() || ((Boolean)var13.get(c)).booleanValue() || ((Boolean)var13.get(d)).booleanValue() || ((Boolean)var13.get(e)).booleanValue()) {
                           var1.a((BlockPosition)var21, (IBlockData)var13, 2);
                        }
                     }
                  }

               }
            }
         }
      }
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      IBlockData var9 = this.u().set(a, Boolean.valueOf(false)).set(b, Boolean.valueOf(false)).set(c, Boolean.valueOf(false)).set(d, Boolean.valueOf(false)).set(e, Boolean.valueOf(false));
      return var3.k().c()?var9.set(a(var3.d()), Boolean.valueOf(true)):var9;
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return null;
   }

   public int a(Random var1) {
      return 0;
   }

   public void a(World var1, EntityHuman var2, BlockPosition var3, IBlockData var4, TileEntity var5, ItemStack var6) {
      if(!var1.E && var6 != null && var6.b() == Items.bl) {
         var2.b(StatisticList.a((Block)this));
         a((World)var1, (BlockPosition)var3, (ItemStack)(new ItemStack(Blocks.bn, 1, 0)));
      } else {
         super.a(var1, var2, var3, var4, var5, var6);
      }

   }

   public IBlockData a(int var1) {
      return this.u().set(d, Boolean.valueOf((var1 & 1) > 0)).set(e, Boolean.valueOf((var1 & 2) > 0)).set(b, Boolean.valueOf((var1 & 4) > 0)).set(c, Boolean.valueOf((var1 & 8) > 0));
   }

   public int e(IBlockData var1) {
      int var2 = 0;
      if(((Boolean)var1.get(d)).booleanValue()) {
         var2 |= 1;
      }

      if(((Boolean)var1.get(e)).booleanValue()) {
         var2 |= 2;
      }

      if(((Boolean)var1.get(b)).booleanValue()) {
         var2 |= 4;
      }

      if(((Boolean)var1.get(c)).booleanValue()) {
         var2 |= 8;
      }

      return var2;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b, c, d, e});
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      switch(BlockVine.SyntheticClass_1.b[var2.ordinal()]) {
      case 1:
         return var1.set(b, var1.get(d)).set(c, var1.get(e)).set(d, var1.get(b)).set(e, var1.get(c));
      case 2:
         return var1.set(b, var1.get(c)).set(c, var1.get(d)).set(d, var1.get(e)).set(e, var1.get(b));
      case 3:
         return var1.set(b, var1.get(e)).set(c, var1.get(b)).set(d, var1.get(c)).set(e, var1.get(d));
      default:
         return var1;
      }
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      switch(BlockVine.SyntheticClass_1.c[var2.ordinal()]) {
      case 1:
         return var1.set(b, var1.get(d)).set(d, var1.get(b));
      case 2:
         return var1.set(c, var1.get(e)).set(e, var1.get(c));
      default:
         return super.a(var1, var2);
      }
   }

   public static class_arm a(EnumDirection var0) {
      switch(BlockVine.SyntheticClass_1.a[var0.ordinal()]) {
      case 1:
         return a;
      case 2:
         return b;
      case 3:
         return d;
      case 4:
         return c;
      case 5:
         return e;
      default:
         throw new IllegalArgumentException(var0 + " is an invalid choice");
      }
   }

   public static int i(IBlockData var0) {
      int var1 = 0;
      class_arm[] var2 = f;
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         class_arm var5 = var2[var4];
         if(((Boolean)var0.get(var5)).booleanValue()) {
            ++var1;
         }
      }

      return var1;
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b;
      // $FF: synthetic field
      static final int[] c = new int[class_amq.values().length];

      static {
         try {
            c[class_amq.LEFT_RIGHT.ordinal()] = 1;
         } catch (NoSuchFieldError var10) {
            ;
         }

         try {
            c[class_amq.FRONT_BACK.ordinal()] = 2;
         } catch (NoSuchFieldError var9) {
            ;
         }

         b = new int[class_aod.values().length];

         try {
            b[class_aod.CLOCKWISE_180.ordinal()] = 1;
         } catch (NoSuchFieldError var8) {
            ;
         }

         try {
            b[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var7) {
            ;
         }

         try {
            b[class_aod.CLOCKWISE_90.ordinal()] = 3;
         } catch (NoSuchFieldError var6) {
            ;
         }

         a = new int[EnumDirection.values().length];

         try {
            a[EnumDirection.UP.ordinal()] = 1;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[EnumDirection.NORTH.ordinal()] = 2;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.SOUTH.ordinal()] = 3;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.EAST.ordinal()] = 4;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 5;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
