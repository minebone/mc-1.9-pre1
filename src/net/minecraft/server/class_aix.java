package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.BlockFlowers;
import net.minecraft.server.BlockTallPlant;
import net.minecraft.server.World;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_wj;

public class class_aix extends BiomeBase {
   protected boolean y;

   protected class_aix(boolean var1, BiomeBase.class_a_in_class_aif var2) {
      super(var2);
      this.y = var1;
      this.v.add(new BiomeBase.BiomeMeta(class_wj.class, 5, 2, 6));
      this.t.z = -999;
      this.t.A = 4;
      this.t.B = 10;
   }

   public BlockFlowers.EnumFlowerVarient a(Random var1, BlockPosition var2) {
      double var3 = l.a((double)var2.p() / 200.0D, (double)var2.r() / 200.0D);
      int var5;
      if(var3 < -0.8D) {
         var5 = var1.nextInt(4);
         switch(var5) {
         case 0:
            return BlockFlowers.EnumFlowerVarient.ORANGE_TULIP;
         case 1:
            return BlockFlowers.EnumFlowerVarient.RED_TULIP;
         case 2:
            return BlockFlowers.EnumFlowerVarient.PINK_TULIP;
         case 3:
         default:
            return BlockFlowers.EnumFlowerVarient.WHITE_TULIP;
         }
      } else if(var1.nextInt(3) > 0) {
         var5 = var1.nextInt(3);
         return var5 == 0?BlockFlowers.EnumFlowerVarient.POPPY:(var5 == 1?BlockFlowers.EnumFlowerVarient.HOUSTONIA:BlockFlowers.EnumFlowerVarient.OXEYE_DAISY);
      } else {
         return BlockFlowers.EnumFlowerVarient.DANDELION;
      }
   }

   public void a(World var1, Random var2, BlockPosition var3) {
      double var4 = l.a((double)(var3.p() + 8) / 200.0D, (double)(var3.r() + 8) / 200.0D);
      int var6;
      int var7;
      int var8;
      int var9;
      if(var4 < -0.8D) {
         this.t.A = 15;
         this.t.B = 5;
      } else {
         this.t.A = 4;
         this.t.B = 10;
         m.a(BlockTallPlant.EnumTallFlowerVariants.GRASS);

         for(var6 = 0; var6 < 7; ++var6) {
            var7 = var2.nextInt(16) + 8;
            var8 = var2.nextInt(16) + 8;
            var9 = var2.nextInt(var1.l(var3.a(var7, 0, var8)).q() + 32);
            m.b(var1, var2, var3.a(var7, var9, var8));
         }
      }

      if(this.y) {
         m.a(BlockTallPlant.EnumTallFlowerVariants.SUNFLOWER);

         for(var6 = 0; var6 < 10; ++var6) {
            var7 = var2.nextInt(16) + 8;
            var8 = var2.nextInt(16) + 8;
            var9 = var2.nextInt(var1.l(var3.a(var7, 0, var8)).q() + 32);
            m.b(var1, var2, var3.a(var7, var9, var8));
         }
      }

      super.a(var1, var2, var3);
   }
}
