package net.minecraft.server;

import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.class_ox;

public class class_pj implements class_ox {
   public int a() {
      return 113;
   }

   public NBTTagCompound a(NBTTagCompound var1) {
      NBTTagList var2;
      if(var1.b("HandDropChances", 9)) {
         var2 = var1.c("HandDropChances", 5);
         if(var2.c() == 2 && var2.f(0) == 0.0F && var2.f(1) == 0.0F) {
            var1.q("HandDropChances");
         }
      }

      if(var1.b("ArmorDropChances", 9)) {
         var2 = var1.c("ArmorDropChances", 5);
         if(var2.c() == 4 && var2.f(0) == 0.0F && var2.f(1) == 0.0F && var2.f(2) == 0.0F && var2.f(3) == 0.0F) {
            var1.q("ArmorDropChances");
         }
      }

      return var1;
   }
}
