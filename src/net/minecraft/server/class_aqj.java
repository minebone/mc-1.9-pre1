package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.class_ahj;
import net.minecraft.server.class_aic;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutTileEntityData;
import net.minecraft.server.class_ky;

public class class_aqj extends TileEntity implements class_ky {
   private final class_ahj a = new class_ahj() {
      public void a(int var1) {
         class_aqj.this.b.c(class_aqj.this.c, Blocks.ac, var1, 0);
      }

      public World a() {
         return class_aqj.this.b;
      }

      public BlockPosition b() {
         return class_aqj.this.c;
      }

      public void a(class_aic var1) {
         super.a(var1);
         if(this.a() != null) {
            IBlockData var2 = this.a().getType(this.b());
            this.a().a(class_aqj.this.c, var2, var2, 4);
         }

      }
   };

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      this.a.a(var2);
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.a.b(var1);
   }

   public void c() {
      this.a.c();
   }

   public Packet D_() {
      NBTTagCompound var1 = new NBTTagCompound();
      this.a(var1);
      var1.q("SpawnPotentials");
      return new PacketPlayOutTileEntityData(this.c, 1, var1);
   }

   public boolean c(int var1, int var2) {
      return this.a.b(var1)?true:super.c(var1, var2);
   }

   public boolean B() {
      return true;
   }

   public class_ahj b() {
      return this.a;
   }
}
