package net.minecraft.server;

import net.minecraft.server.AchievementList;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumItemRarity;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.Statistic;
import net.minecraft.server.World;
import net.minecraft.server.class_adj;
import net.minecraft.server.ItemStack;

public class class_adk extends class_adj {
   public class_adk(int var1, float var2, boolean var3) {
      super(var1, var2, var3);
      this.a(true);
   }

   public EnumItemRarity g(ItemStack var1) {
      return var1.i() == 0?EnumItemRarity.RARE:EnumItemRarity.EPIC;
   }

   protected void a(ItemStack var1, World var2, EntityHuman var3) {
      if(!var2.E) {
         if(var1.i() > 0) {
            var3.b((Statistic)AchievementList.M);
            var3.c(new MobEffect(MobEffectList.j, 400, 1));
            var3.c(new MobEffect(MobEffectList.k, 6000, 0));
            var3.c(new MobEffect(MobEffectList.l, 6000, 0));
            var3.c(new MobEffect(MobEffectList.v, 2400, 3));
         } else {
            var3.c(new MobEffect(MobEffectList.j, 100, 1));
            var3.c(new MobEffect(MobEffectList.v, 2400, 0));
         }
      }

   }
}
