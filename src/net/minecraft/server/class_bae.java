package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import java.util.Random;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_kk;

public abstract class class_bae {
   private final class_baq[] a;

   protected class_bae(class_baq[] var1) {
      this.a = var1;
   }

   public abstract ItemStack a(ItemStack var1, Random var2, class_azy var3);

   public class_baq[] a() {
      return this.a;
   }

   public abstract static class class_a_in_class_bae {
      private final class_kk a;
      private final Class b;

      protected class_a_in_class_bae(class_kk var1, Class var2) {
         this.a = var1;
         this.b = var2;
      }

      public class_kk a() {
         return this.a;
      }

      public Class b() {
         return this.b;
      }

      public abstract void a(JsonObject var1, class_bae var2, JsonSerializationContext var3);

      public abstract class_bae b(JsonObject var1, JsonDeserializationContext var2, class_baq[] var3);
   }
}
