package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_amf;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;

public class BlockFenceGate extends class_amf {
   public static final class_arm a = class_arm.a("open");
   public static final class_arm b = class_arm.a("powered");
   public static final class_arm c = class_arm.a("in_wall");
   protected static final AxisAlignedBB d = new AxisAlignedBB(0.0D, 0.0D, 0.375D, 1.0D, 1.0D, 0.625D);
   protected static final AxisAlignedBB e = new AxisAlignedBB(0.375D, 0.0D, 0.0D, 0.625D, 1.0D, 1.0D);
   protected static final AxisAlignedBB f = new AxisAlignedBB(0.0D, 0.0D, 0.375D, 1.0D, 0.8125D, 0.625D);
   protected static final AxisAlignedBB g = new AxisAlignedBB(0.375D, 0.0D, 0.0D, 0.625D, 0.8125D, 1.0D);
   protected static final AxisAlignedBB B = new AxisAlignedBB(0.0D, 0.0D, 0.375D, 1.0D, 1.5D, 0.625D);
   protected static final AxisAlignedBB C = new AxisAlignedBB(0.375D, 0.0D, 0.0D, 0.625D, 1.5D, 1.0D);

   public BlockFenceGate(BlockWood.EnumLogVariant var1) {
      super(Material.d, var1.c());
      this.w(this.A.b().set(a, Boolean.valueOf(false)).set(b, Boolean.valueOf(false)).set(c, Boolean.valueOf(false)));
      this.a(CreativeModeTab.d);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      var1 = this.b(var1, var2, var3);
      return ((Boolean)var1.get(c)).booleanValue()?(((EnumDirection)var1.get(D)).k() == EnumDirection.class_a_in_class_cq.X?g:f):(((EnumDirection)var1.get(D)).k() == EnumDirection.class_a_in_class_cq.X?e:d);
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      EnumDirection.class_a_in_class_cq var4 = ((EnumDirection)var1.get(D)).k();
      if(var4 == EnumDirection.class_a_in_class_cq.Z && (var2.getType(var3.e()).getBlock() == Blocks.bZ || var2.getType(var3.f()).getBlock() == Blocks.bZ) || var4 == EnumDirection.class_a_in_class_cq.X && (var2.getType(var3.c()).getBlock() == Blocks.bZ || var2.getType(var3.d()).getBlock() == Blocks.bZ)) {
         var1 = var1.set(c, Boolean.valueOf(true));
      }

      return var1;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(D, var2.a((EnumDirection)var1.get(D)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(D)));
   }

   public boolean a(World var1, BlockPosition var2) {
      return var1.getType(var2.b()).getMaterial().a()?super.a(var1, var2):false;
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return ((Boolean)var1.get(a)).booleanValue()?k:(((EnumDirection)var1.get(D)).k() == EnumDirection.class_a_in_class_cq.Z?B:C);
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(class_ahw var1, BlockPosition var2) {
      return ((Boolean)var1.getType(var2).get(a)).booleanValue();
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return this.u().set(D, var8.bh()).set(a, Boolean.valueOf(false)).set(b, Boolean.valueOf(false)).set(c, Boolean.valueOf(false));
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(((Boolean)var3.get(a)).booleanValue()) {
         var3 = var3.set(a, Boolean.valueOf(false));
         var1.a((BlockPosition)var2, (IBlockData)var3, 10);
      } else {
         EnumDirection var11 = EnumDirection.a((double)var4.yaw);
         if(var3.get(D) == var11.d()) {
            var3 = var3.set(D, var11);
         }

         var3 = var3.set(a, Boolean.valueOf(true));
         var1.a((BlockPosition)var2, (IBlockData)var3, 10);
      }

      var1.a(var4, ((Boolean)var3.get(a)).booleanValue()?1008:1014, var2, 0);
      return true;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!var1.E) {
         boolean var5 = var1.y(var2);
         if(var5 || var4.u().m()) {
            if(var5 && !((Boolean)var3.get(a)).booleanValue() && !((Boolean)var3.get(b)).booleanValue()) {
               var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Boolean.valueOf(true)).set(b, Boolean.valueOf(true)), 2);
               var1.a((EntityHuman)null, 1008, var2, 0);
            } else if(!var5 && ((Boolean)var3.get(a)).booleanValue() && ((Boolean)var3.get(b)).booleanValue()) {
               var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Boolean.valueOf(false)).set(b, Boolean.valueOf(false)), 2);
               var1.a((EntityHuman)null, 1014, var2, 0);
            } else if(var5 != ((Boolean)var3.get(b)).booleanValue()) {
               var1.a((BlockPosition)var2, (IBlockData)var3.set(b, Boolean.valueOf(var5)), 2);
            }
         }

      }
   }

   public IBlockData a(int var1) {
      return this.u().set(D, EnumDirection.b(var1)).set(a, Boolean.valueOf((var1 & 4) != 0)).set(b, Boolean.valueOf((var1 & 8) != 0));
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((EnumDirection)var1.get(D)).b();
      if(((Boolean)var1.get(b)).booleanValue()) {
         var3 |= 8;
      }

      if(((Boolean)var1.get(a)).booleanValue()) {
         var3 |= 4;
      }

      return var3;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{D, a, b, c});
   }
}
