package net.minecraft.server;

import net.minecraft.server.MathHelper;

public class class_qz {
   public static float a(float var0, float var1) {
      float var2 = MathHelper.a(var1 - var0 * 0.5F, var1 * 0.2F, 20.0F);
      float var3 = var0 * (1.0F - var2 / 25.0F);
      return var3;
   }

   public static float b(float var0, float var1) {
      float var2 = MathHelper.a(var1, 0.0F, 20.0F);
      return var0 * (1.0F - var2 / 25.0F);
   }
}
