package net.minecraft.server;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityCreature;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Items;
import net.minecraft.server.ItemStack;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.class_ru;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_vb;
import net.minecraft.server.class_yh;
import net.minecraft.server.class_yv;
import net.minecraft.server.class_yz;

public class class_ux extends class_vb {
   protected final Class a;
   private final int i;
   protected final class_ux.class_a_in_class_ux b;
   protected final Predicate c;
   protected class_rz d;

   public class_ux(EntityCreature var1, Class var2, boolean var3) {
      this(var1, var2, var3, false);
   }

   public class_ux(EntityCreature var1, Class var2, boolean var3, boolean var4) {
      this(var1, var2, 10, var3, var4, (Predicate)null);
   }

   public class_ux(EntityCreature var1, Class var2, int var3, boolean var4, boolean var5, final Predicate var6) {
      super(var1, var4, var5);
      this.a = var2;
      this.i = var3;
      this.b = new class_ux.class_a_in_class_ux(var1);
      this.a(1);
      this.c = new Predicate() {
         public boolean a(class_rz var1) {
            return var1 == null?false:(var6 != null && !var6.apply(var1)?false:(!class_ru.e.apply(var1)?false:class_ux.this.a(var1, false)));
         }

         // $FF: synthetic method
         public boolean apply(Object var1) {
            return this.a((class_rz)var1);
         }
      };
   }

   public boolean a() {
      if(this.i > 0 && this.e.bE().nextInt(this.i) != 0) {
         return false;
      } else if(this.a != EntityHuman.class && this.a != EntityPlayer.class) {
         List var1 = this.e.world.a(this.a, this.a(this.f()), this.c);
         if(var1.isEmpty()) {
            return false;
         } else {
            Collections.sort(var1, this.b);
            this.d = (class_rz)var1.get(0);
            return true;
         }
      } else {
         this.d = this.e.world.a(this.e.locX, this.e.locY + (double)this.e.bm(), this.e.locZ, this.f(), this.f(), new Function() {
            public Double a(EntityHuman var1) {
               ItemStack var2 = var1.a(EnumInventorySlot.HEAD);
               if(var2 != null && var2.b() == Items.ch) {
                  int var3 = var2.h();
                  boolean var4 = class_ux.this.e instanceof class_yv && ((class_yv)class_ux.this.e).da() == 0 && var3 == 0;
                  boolean var5 = class_ux.this.e instanceof class_yz && var3 == 2;
                  boolean var6 = class_ux.this.e instanceof class_yh && var3 == 4;
                  if(var4 || var5 || var6) {
                     return Double.valueOf(0.5D);
                  }
               }

               return Double.valueOf(1.0D);
            }

            // $FF: synthetic method
            public Object apply(Object var1) {
               return this.a((EntityHuman)var1);
            }
         }, this.c);
         return this.d != null;
      }
   }

   protected AxisAlignedBB a(double var1) {
      return this.e.bk().b(var1, 4.0D, var1);
   }

   public void c() {
      this.e.c(this.d);
      super.c();
   }

   public static class class_a_in_class_ux implements Comparator {
      private final Entity a;

      public class_a_in_class_ux(Entity var1) {
         this.a = var1;
      }

      public int a(Entity var1, Entity var2) {
         double var3 = this.a.h(var1);
         double var5 = this.a.h(var2);
         return var3 < var5?-1:(var3 > var5?1:0);
      }

      // $FF: synthetic method
      public int compare(Object var1, Object var2) {
         return this.a((Entity)var1, (Entity)var2);
      }
   }
}
