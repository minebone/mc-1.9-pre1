package net.minecraft.server;

import com.google.gson.JsonParseException;
import java.util.Collections;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ev;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;

public class class_bn extends CommandAbstract {
   public String c() {
      return "tellraw";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.tellraw.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 2) {
         throw new class_cf("commands.tellraw.usage", new Object[0]);
      } else {
         EntityPlayer var4 = a(var1, var2, var3[0]);
         String var5 = a(var3, 1);

         try {
            IChatBaseComponent var6 = IChatBaseComponent.ChatSerializer.a(var5);
            var4.a((IChatBaseComponent)class_ev.a(var2, var6, var4));
         } catch (JsonParseException var7) {
            throw a(var7);
         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, var1.J()):Collections.emptyList();
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 0;
   }
}
