package net.minecraft.server;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityMinecartAbstract;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.EnumHand;

public class class_aam extends EntityMinecartAbstract {
   public class_aam(World var1) {
      super(var1);
   }

   public class_aam(World var1, double var2, double var4, double var6) {
      super(var1, var2, var4, var6);
   }

   public boolean a(EntityHuman var1, ItemStack var2, EnumHand var3) {
      if(var1.aJ()) {
         return false;
      } else if(this.aI()) {
         return true;
      } else {
         if(!this.world.E) {
            var1.m(this);
         }

         return true;
      }
   }

   public void a(int var1, int var2, int var3, boolean var4) {
      if(var4) {
         if(this.aI()) {
            this.ay();
         }

         if(this.t() == 0) {
            this.e(-this.u());
            this.d(10);
            this.a(50.0F);
            this.an();
         }
      }

   }

   public EntityMinecartAbstract.EnumMinecartType v() {
      return EntityMinecartAbstract.EnumMinecartType.RIDEABLE;
   }
}
