package net.minecraft.server;

import com.google.common.base.Predicate;
import net.minecraft.server.EntityTameableAnimal;
import net.minecraft.server.class_ux;

public class class_uy extends class_ux {
   private EntityTameableAnimal i;

   public class_uy(EntityTameableAnimal var1, Class var2, boolean var3, Predicate var4) {
      super(var1, var2, 10, var3, false, var4);
      this.i = var1;
   }

   public boolean a() {
      return !this.i.cZ() && super.a();
   }
}
