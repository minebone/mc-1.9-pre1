package net.minecraft.server;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_wi;

public class class_ta extends class_tj {
   private class_wi a;
   private EntityHuman b;
   private World c;
   private float d;
   private int e;

   public class_ta(class_wi var1, float var2) {
      this.a = var1;
      this.c = var1.world;
      this.d = var2;
      this.a(2);
   }

   public boolean a() {
      this.b = this.c.a(this.a, (double)this.d);
      return this.b == null?false:this.a(this.b);
   }

   public boolean b() {
      return !this.b.at()?false:(this.a.h(this.b) > (double)(this.d * this.d)?false:this.e > 0 && this.a(this.b));
   }

   public void c() {
      this.a.s(true);
      this.e = 40 + this.a.bE().nextInt(40);
   }

   public void d() {
      this.a.s(false);
      this.b = null;
   }

   public void e() {
      this.a.t().a(this.b.locX, this.b.locY + (double)this.b.bm(), this.b.locZ, 10.0F, (float)this.a.cD());
      --this.e;
   }

   private boolean a(EntityHuman var1) {
      EnumHand[] var2 = EnumHand.values();
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         EnumHand var5 = var2[var4];
         ItemStack var6 = var1.b((EnumHand)var5);
         if(var6 != null) {
            if(this.a.cZ() && var6.b() == Items.be) {
               return true;
            }

            if(this.a.e(var6)) {
               return true;
            }
         }
      }

      return false;
   }
}
