package net.minecraft.server;

import net.minecraft.server.EntityInsentient;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_vd;

public class class_tg extends class_tj {
   private EntityInsentient a;

   public class_tg(EntityInsentient var1) {
      this.a = var1;
      this.a(4);
      ((class_vd)var1.x()).c(true);
   }

   public boolean a() {
      return this.a.ah() || this.a.am();
   }

   public void e() {
      if(this.a.bE().nextFloat() < 0.8F) {
         this.a.w().a();
      }

   }
}
