package net.minecraft.server;

import java.util.WeakHashMap;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.NavigationAbstract;
import net.minecraft.server.World;
import net.minecraft.server.class_ahu;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_aym;
import net.minecraft.server.class_ayo;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_nf;
import net.minecraft.server.EnumSoundCategory;

public class class_vf implements class_ahu {
   private static final Object a = new Object();
   private final WeakHashMap b = new WeakHashMap();

   public void a(NavigationAbstract var1) {
      this.b.put(var1, a);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, IBlockData var4, int var5) {
      if(this.a(var1, var2, var3, var4)) {
         NavigationAbstract[] var6 = (NavigationAbstract[])this.b.keySet().toArray(new NavigationAbstract[0]);
         NavigationAbstract[] var7 = var6;
         int var8 = var6.length;

         for(int var9 = 0; var9 < var8; ++var9) {
            NavigationAbstract var10 = var7[var9];
            if(var10 != null && !var10.i()) {
               class_ayo var11 = var10.k();
               if(var11 != null && !var11.b() && var11.d() != 0) {
                  class_aym var12 = var10.c.c();
                  double var13 = var2.e(((double)var12.a + var10.a.locX) / 2.0D, ((double)var12.b + var10.a.locY) / 2.0D, ((double)var12.c + var10.a.locZ) / 2.0D);
                  int var15 = (var11.d() - var11.e()) * (var11.d() - var11.e());
                  if(var13 < (double)var15) {
                     var10.j();
                  }
               }
            }
         }

      }
   }

   protected boolean a(World var1, BlockPosition var2, IBlockData var3, IBlockData var4) {
      AxisAlignedBB var5 = var3.d(var1, var2);
      AxisAlignedBB var6 = var4.d(var1, var2);
      return var5 != var6 && (var5 == null || !var5.equals(var6));
   }

   public void a(BlockPosition var1) {
   }

   public void a(int var1, int var2, int var3, int var4, int var5, int var6) {
   }

   public void a(EntityHuman var1, class_nf var2, EnumSoundCategory var3, double var4, double var6, double var8, float var10, float var11) {
   }

   public void a(int var1, boolean var2, double var3, double var5, double var7, double var9, double var11, double var13, int... var15) {
   }

   public void a(Entity var1) {
   }

   public void b(Entity var1) {
   }

   public void a(class_nf var1, BlockPosition var2) {
   }

   public void a(int var1, BlockPosition var2, int var3) {
   }

   public void a(EntityHuman var1, int var2, BlockPosition var3, int var4) {
   }

   public void b(int var1, BlockPosition var2, int var3) {
   }
}
