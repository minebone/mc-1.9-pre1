package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.PacketPlayOutGameStateChange;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agn;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_ru;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_yi;
import net.minecraft.server.class_zr;

public abstract class class_zl extends Entity implements class_zr {
   private static final Predicate f = Predicates.and(new Predicate[]{class_ru.e, class_ru.a, new Predicate() {
      public boolean a(Entity var1) {
         return var1.ao();
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((Entity)var1);
      }
   }});
   private static final class_ke g = DataWatcher.a(class_zl.class, class_kg.a);
   private int h;
   private int as;
   private int at;
   private Block au;
   private int av;
   protected boolean a;
   protected int b;
   public class_zl.class_a_in_class_zl c;
   public int d;
   public Entity e;
   private int aw;
   private int ax;
   private double ay;
   private int az;

   public class_zl(World var1) {
      super(var1);
      this.h = -1;
      this.as = -1;
      this.at = -1;
      this.c = class_zl.class_a_in_class_zl.DISALLOWED;
      this.ay = 2.0D;
      this.a(0.5F, 0.5F);
   }

   public class_zl(World var1, double var2, double var4, double var6) {
      this(var1);
      this.b(var2, var4, var6);
   }

   public class_zl(World var1, class_rz var2) {
      this(var1, var2.locX, var2.locY + (double)var2.bm() - 0.10000000149011612D, var2.locZ);
      this.e = var2;
      if(var2 instanceof EntityHuman) {
         this.c = class_zl.class_a_in_class_zl.ALLOWED;
      }

   }

   protected void i() {
      this.datawatcher.a((class_ke)g, (Object)Byte.valueOf((byte)0));
   }

   public void a(Entity var1, float var2, float var3, float var4, float var5, float var6) {
      float var7 = -MathHelper.a(var3 * 0.017453292F) * MathHelper.b(var2 * 0.017453292F);
      float var8 = -MathHelper.a(var2 * 0.017453292F);
      float var9 = MathHelper.b(var3 * 0.017453292F) * MathHelper.b(var2 * 0.017453292F);
      this.c((double)var7, (double)var8, (double)var9, var5, var6);
      this.motX += var1.motX;
      this.motZ += var1.motZ;
      if(!var1.onGround) {
         this.motY += var1.motY;
      }

   }

   public void c(double var1, double var3, double var5, float var7, float var8) {
      float var9 = MathHelper.a(var1 * var1 + var3 * var3 + var5 * var5);
      var1 /= (double)var9;
      var3 /= (double)var9;
      var5 /= (double)var9;
      var1 += this.random.nextGaussian() * 0.007499999832361937D * (double)var8;
      var3 += this.random.nextGaussian() * 0.007499999832361937D * (double)var8;
      var5 += this.random.nextGaussian() * 0.007499999832361937D * (double)var8;
      var1 *= (double)var7;
      var3 *= (double)var7;
      var5 *= (double)var7;
      this.motX = var1;
      this.motY = var3;
      this.motZ = var5;
      float var10 = MathHelper.a(var1 * var1 + var5 * var5);
      this.lastYaw = this.yaw = (float)(MathHelper.b(var1, var5) * 57.2957763671875D);
      this.lastPitch = this.pitch = (float)(MathHelper.b(var3, (double)var10) * 57.2957763671875D);
      this.aw = 0;
   }

   public void m() {
      super.m();
      if(this.lastPitch == 0.0F && this.lastYaw == 0.0F) {
         float var1 = MathHelper.a(this.motX * this.motX + this.motZ * this.motZ);
         this.lastYaw = this.yaw = (float)(MathHelper.b(this.motX, this.motZ) * 57.2957763671875D);
         this.lastPitch = this.pitch = (float)(MathHelper.b(this.motY, (double)var1) * 57.2957763671875D);
      }

      BlockPosition var13 = new BlockPosition(this.h, this.as, this.at);
      IBlockData var2 = this.world.getType(var13);
      Block var3 = var2.getBlock();
      if(var2.getMaterial() != Material.a) {
         AxisAlignedBB var4 = var2.d(this.world, var13);
         if(var4 != Block.k && var4.a(var13).a(new Vec3D(this.locX, this.locY, this.locZ))) {
            this.a = true;
         }
      }

      if(this.d > 0) {
         --this.d;
      }

      if(this.a) {
         int var15 = var3.e(var2);
         if(var3 == this.au && var15 == this.av) {
            ++this.aw;
            if(this.aw >= 1200) {
               this.S();
            }
         } else {
            this.a = false;
            this.motX *= (double)(this.random.nextFloat() * 0.2F);
            this.motY *= (double)(this.random.nextFloat() * 0.2F);
            this.motZ *= (double)(this.random.nextFloat() * 0.2F);
            this.aw = 0;
            this.ax = 0;
         }

         ++this.b;
      } else {
         this.b = 0;
         ++this.ax;
         Vec3D var14 = new Vec3D(this.locX, this.locY, this.locZ);
         Vec3D var5 = new Vec3D(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
         MovingObjectPosition var6 = this.world.a(var14, var5, false, true, false);
         var14 = new Vec3D(this.locX, this.locY, this.locZ);
         var5 = new Vec3D(this.locX + this.motX, this.locY + this.motY, this.locZ + this.motZ);
         if(var6 != null) {
            var5 = new Vec3D(var6.c.b, var6.c.c, var6.c.d);
         }

         Entity var7 = this.a(var14, var5);
         if(var7 != null) {
            var6 = new MovingObjectPosition(var7);
         }

         if(var6 != null && var6.d != null && var6.d instanceof EntityHuman) {
            EntityHuman var8 = (EntityHuman)var6.d;
            if(this.e instanceof EntityHuman && !((EntityHuman)this.e).a(var8)) {
               var6 = null;
            }
         }

         if(var6 != null) {
            this.a(var6);
         }

         if(this.l()) {
            for(int var16 = 0; var16 < 4; ++var16) {
               this.world.a(EnumParticle.CRIT, this.locX + this.motX * (double)var16 / 4.0D, this.locY + this.motY * (double)var16 / 4.0D, this.locZ + this.motZ * (double)var16 / 4.0D, -this.motX, -this.motY + 0.2D, -this.motZ, new int[0]);
            }
         }

         this.locX += this.motX;
         this.locY += this.motY;
         this.locZ += this.motZ;
         float var17 = MathHelper.a(this.motX * this.motX + this.motZ * this.motZ);
         this.yaw = (float)(MathHelper.b(this.motX, this.motZ) * 57.2957763671875D);

         for(this.pitch = (float)(MathHelper.b(this.motY, (double)var17) * 57.2957763671875D); this.pitch - this.lastPitch < -180.0F; this.lastPitch -= 360.0F) {
            ;
         }

         while(this.pitch - this.lastPitch >= 180.0F) {
            this.lastPitch += 360.0F;
         }

         while(this.yaw - this.lastYaw < -180.0F) {
            this.lastYaw -= 360.0F;
         }

         while(this.yaw - this.lastYaw >= 180.0F) {
            this.lastYaw += 360.0F;
         }

         this.pitch = this.lastPitch + (this.pitch - this.lastPitch) * 0.2F;
         this.yaw = this.lastYaw + (this.yaw - this.lastYaw) * 0.2F;
         float var9 = 0.99F;
         float var10 = 0.05F;
         if(this.ah()) {
            for(int var11 = 0; var11 < 4; ++var11) {
               float var12 = 0.25F;
               this.world.a(EnumParticle.WATER_BUBBLE, this.locX - this.motX * (double)var12, this.locY - this.motY * (double)var12, this.locZ - this.motZ * (double)var12, this.motX, this.motY, this.motZ, new int[0]);
            }

            var9 = 0.6F;
         }

         if(this.ag()) {
            this.W();
         }

         this.motX *= (double)var9;
         this.motY *= (double)var9;
         this.motZ *= (double)var9;
         this.motY -= (double)var10;
         this.b(this.locX, this.locY, this.locZ);
         this.ab();
      }
   }

   protected void a(MovingObjectPosition var1) {
      Entity var2 = var1.d;
      if(var2 != null) {
         float var3 = MathHelper.a(this.motX * this.motX + this.motY * this.motY + this.motZ * this.motZ);
         int var4 = MathHelper.f((double)var3 * this.ay);
         if(this.l()) {
            var4 += this.random.nextInt(var4 / 2 + 2);
         }

         DamageSource var5;
         if(this.e == null) {
            var5 = DamageSource.a((class_zl)this, (Entity)this);
         } else {
            var5 = DamageSource.a(this, this.e);
         }

         if(this.aG() && !(var2 instanceof class_yi)) {
            var2.g(5);
         }

         if(var2.a(var5, (float)var4)) {
            if(var2 instanceof class_rz) {
               class_rz var6 = (class_rz)var2;
               if(!this.world.E) {
                  var6.k(var6.bW() + 1);
               }

               if(this.az > 0) {
                  float var7 = MathHelper.a(this.motX * this.motX + this.motZ * this.motZ);
                  if(var7 > 0.0F) {
                     var6.g(this.motX * (double)this.az * 0.6000000238418579D / (double)var7, 0.1D, this.motZ * (double)this.az * 0.6000000238418579D / (double)var7);
                  }
               }

               if(this.e instanceof class_rz) {
                  class_agn.a(var6, this.e);
                  class_agn.b((class_rz)((class_rz)this.e), (Entity)var6);
               }

               this.a(var6);
               if(this.e != null && var6 != this.e && var6 instanceof EntityHuman && this.e instanceof EntityPlayer) {
                  ((EntityPlayer)this.e).a.a((Packet)(new PacketPlayOutGameStateChange(6, 0.0F)));
               }
            }

            this.a(class_ng.t, 1.0F, 1.2F / (this.random.nextFloat() * 0.2F + 0.9F));
            if(!(var2 instanceof class_yi)) {
               this.S();
            }
         } else {
            this.motX *= -0.10000000149011612D;
            this.motY *= -0.10000000149011612D;
            this.motZ *= -0.10000000149011612D;
            this.yaw += 180.0F;
            this.lastYaw += 180.0F;
            this.ax = 0;
            if(!this.world.E && this.motX * this.motX + this.motY * this.motY + this.motZ * this.motZ < 0.0010000000474974513D) {
               if(this.c == class_zl.class_a_in_class_zl.ALLOWED) {
                  this.a(this.j(), 0.1F);
               }

               this.S();
            }
         }
      } else {
         BlockPosition var8 = var1.a();
         this.h = var8.p();
         this.as = var8.q();
         this.at = var8.r();
         IBlockData var9 = this.world.getType(var8);
         this.au = var9.getBlock();
         this.av = this.au.e(var9);
         this.motX = (double)((float)(var1.c.b - this.locX));
         this.motY = (double)((float)(var1.c.c - this.locY));
         this.motZ = (double)((float)(var1.c.d - this.locZ));
         float var10 = MathHelper.a(this.motX * this.motX + this.motY * this.motY + this.motZ * this.motZ);
         this.locX -= this.motX / (double)var10 * 0.05000000074505806D;
         this.locY -= this.motY / (double)var10 * 0.05000000074505806D;
         this.locZ -= this.motZ / (double)var10 * 0.05000000074505806D;
         this.a(class_ng.t, 1.0F, 1.2F / (this.random.nextFloat() * 0.2F + 0.9F));
         this.a = true;
         this.d = 7;
         this.a(false);
         if(var9.getMaterial() != Material.a) {
            this.au.a((World)this.world, (BlockPosition)var8, (IBlockData)var9, (Entity)this);
         }
      }

   }

   protected void a(class_rz var1) {
   }

   protected Entity a(Vec3D var1, Vec3D var2) {
      Entity var3 = null;
      List var4 = this.world.a((Entity)this, (AxisAlignedBB)this.bk().a(this.motX, this.motY, this.motZ).g(1.0D), (Predicate)f);
      double var5 = 0.0D;

      for(int var7 = 0; var7 < var4.size(); ++var7) {
         Entity var8 = (Entity)var4.get(var7);
         if(var8 != this.e || this.ax >= 5) {
            AxisAlignedBB var9 = var8.bk().g(0.30000001192092896D);
            MovingObjectPosition var10 = var9.a(var1, var2);
            if(var10 != null) {
               double var11 = var1.g(var10.c);
               if(var11 < var5 || var5 == 0.0D) {
                  var3 = var8;
                  var5 = var11;
               }
            }
         }
      }

      return var3;
   }

   public void b(NBTTagCompound var1) {
      var1.a("xTile", this.h);
      var1.a("yTile", this.as);
      var1.a("zTile", this.at);
      var1.a("life", (short)this.aw);
      class_kk var2 = (class_kk)Block.h.b(this.au);
      var1.a("inTile", var2 == null?"":var2.toString());
      var1.a("inData", (byte)this.av);
      var1.a("shake", (byte)this.d);
      var1.a("inGround", (byte)(this.a?1:0));
      var1.a("pickup", (byte)this.c.ordinal());
      var1.a("damage", this.ay);
   }

   public void a(NBTTagCompound var1) {
      this.h = var1.h("xTile");
      this.as = var1.h("yTile");
      this.at = var1.h("zTile");
      this.aw = var1.g("life");
      if(var1.b("inTile", 8)) {
         this.au = Block.b(var1.l("inTile"));
      } else {
         this.au = Block.b(var1.f("inTile") & 255);
      }

      this.av = var1.f("inData") & 255;
      this.d = var1.f("shake") & 255;
      this.a = var1.f("inGround") == 1;
      if(var1.b("damage", 99)) {
         this.ay = var1.k("damage");
      }

      if(var1.b("pickup", 99)) {
         this.c = class_zl.class_a_in_class_zl.a(var1.f("pickup"));
      } else if(var1.b("player", 99)) {
         this.c = var1.p("player")?class_zl.class_a_in_class_zl.ALLOWED:class_zl.class_a_in_class_zl.DISALLOWED;
      }

   }

   public void d(EntityHuman var1) {
      if(!this.world.E && this.a && this.d <= 0) {
         boolean var2 = this.c == class_zl.class_a_in_class_zl.ALLOWED || this.c == class_zl.class_a_in_class_zl.CREATIVE_ONLY && var1.abilities.d;
         if(this.c == class_zl.class_a_in_class_zl.ALLOWED && !var1.br.c(this.j())) {
            var2 = false;
         }

         if(var2) {
            this.a(class_ng.cR, 0.2F, ((this.random.nextFloat() - this.random.nextFloat()) * 0.7F + 1.0F) * 2.0F);
            var1.a(this, 1);
            this.S();
         }

      }
   }

   protected abstract ItemStack j();

   protected boolean ad() {
      return false;
   }

   public void c(double var1) {
      this.ay = var1;
   }

   public double k() {
      return this.ay;
   }

   public void a(int var1) {
      this.az = var1;
   }

   public boolean aS() {
      return false;
   }

   public float bm() {
      return 0.0F;
   }

   public void a(boolean var1) {
      byte var2 = ((Byte)this.datawatcher.a(g)).byteValue();
      if(var1) {
         this.datawatcher.b(g, Byte.valueOf((byte)(var2 | 1)));
      } else {
         this.datawatcher.b(g, Byte.valueOf((byte)(var2 & -2)));
      }

   }

   public boolean l() {
      byte var1 = ((Byte)this.datawatcher.a(g)).byteValue();
      return (var1 & 1) != 0;
   }

   public static enum class_a_in_class_zl {
      DISALLOWED,
      ALLOWED,
      CREATIVE_ONLY;

      public static class_zl.class_a_in_class_zl a(int var0) {
         if(var0 < 0 || var0 > values().length) {
            var0 = 0;
         }

         return values()[var0];
      }
   }
}
