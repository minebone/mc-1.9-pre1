package net.minecraft.server;

import com.google.common.base.Predicates;
import com.google.common.collect.Multimap;
import java.util.List;
import java.util.UUID;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_akt;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ck;
import net.minecraft.server.class_cn;
import net.minecraft.server.class_cr;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;
import net.minecraft.server.class_ru;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_ys;

public class ItemArmor extends Item {
   private static final int[] m = new int[]{13, 15, 16, 11};
   private static final UUID[] n = new UUID[]{UUID.fromString("845DB27C-C624-495F-8C9F-6020A9A58B6B"), UUID.fromString("D8499B04-0E66-4726-AB29-64469D734E0D"), UUID.fromString("9F3D476D-C118-4544-8365-64846904B48E"), UUID.fromString("2AD3F246-FEE1-4E67-B886-69FD380BB150")};
   public static final String[] a = new String[]{"minecraft:items/empty_armor_slot_boots", "minecraft:items/empty_armor_slot_leggings", "minecraft:items/empty_armor_slot_chestplate", "minecraft:items/empty_armor_slot_helmet"};
   public static final class_cr b = new class_cn() {
      protected ItemStack b(class_ck var1, ItemStack var2) {
         ItemStack var3 = ItemArmor.a(var1, var2);
         return var3 != null?var3:super.b(var1, var2);
      }
   };
   public final EnumInventorySlot c;
   public final int d;
   public final int e;
   private final ItemArmor.EnumArmorMaterial o;

   public static ItemStack a(class_ck var0, ItemStack var1) {
      BlockPosition var2 = var0.d().a(class_akt.e(var0.f()));
      int var3 = var2.p();
      int var4 = var2.q();
      int var5 = var2.r();
      AxisAlignedBB var6 = new AxisAlignedBB((double)var3, (double)var4, (double)var5, (double)(var3 + 1), (double)(var4 + 1), (double)(var5 + 1));
      List var7 = var0.i().a(class_rz.class, var6, Predicates.and(class_ru.e, new class_ru.class_a_in_class_ru(var1)));
      if(var7.isEmpty()) {
         return null;
      } else {
         class_rz var8 = (class_rz)var7.get(0);
         EnumInventorySlot var9 = EntityInsentient.d(var1);
         ItemStack var10 = var1.k();
         var10.b = 1;
         var8.a(var9, var10);
         if(var8 instanceof EntityInsentient) {
            ((EntityInsentient)var8).a(var9, 2.0F);
         }

         --var1.b;
         return var1;
      }
   }

   public ItemArmor(ItemArmor.EnumArmorMaterial var1, int var2, EnumInventorySlot var3) {
      this.o = var1;
      this.c = var3;
      this.e = var2;
      this.d = var1.b(var3);
      this.e(var1.a(var3));
      this.j = 1;
      this.a(CreativeModeTab.j);
      class_akt.c.a(this, b);
   }

   public int c() {
      return this.o.a();
   }

   public ItemArmor.EnumArmorMaterial d() {
      return this.o;
   }

   public boolean e_(ItemStack var1) {
      if(this.o != ItemArmor.EnumArmorMaterial.LEATHER) {
         return false;
      } else {
         NBTTagCompound var2 = var1.o();
         return var2 != null && var2.b("display", 10)?var2.o("display").b("color", 3):false;
      }
   }

   public int b(ItemStack var1) {
      if(this.o != ItemArmor.EnumArmorMaterial.LEATHER) {
         return 16777215;
      } else {
         NBTTagCompound var2 = var1.o();
         if(var2 != null) {
            NBTTagCompound var3 = var2.o("display");
            if(var3 != null && var3.b("color", 3)) {
               return var3.h("color");
            }
         }

         return 10511680;
      }
   }

   public void c(ItemStack var1) {
      if(this.o == ItemArmor.EnumArmorMaterial.LEATHER) {
         NBTTagCompound var2 = var1.o();
         if(var2 != null) {
            NBTTagCompound var3 = var2.o("display");
            if(var3.e("color")) {
               var3.q("color");
            }

         }
      }
   }

   public void a(ItemStack var1, int var2) {
      if(this.o != ItemArmor.EnumArmorMaterial.LEATHER) {
         throw new UnsupportedOperationException("Can\'t dye non-leather!");
      } else {
         NBTTagCompound var3 = var1.o();
         if(var3 == null) {
            var3 = new NBTTagCompound();
            var1.d(var3);
         }

         NBTTagCompound var4 = var3.o("display");
         if(!var3.b("display", 10)) {
            var3.a((String)"display", (NBTTag)var4);
         }

         var4.a("color", var2);
      }
   }

   public boolean a(ItemStack var1, ItemStack var2) {
      return this.o.c() == var2.b()?true:super.a(var1, var2);
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      EnumInventorySlot var5 = EntityInsentient.d(var1);
      ItemStack var6 = var3.a(var5);
      if(var6 == null) {
         var3.a(var5, var1.k());
         var1.b = 0;
         return new class_qo(EnumResult.SUCCESS, var1);
      } else {
         return new class_qo(EnumResult.FAIL, var1);
      }
   }

   public Multimap a(EnumInventorySlot var1) {
      Multimap var2 = super.a(var1);
      if(var1 == this.c) {
         var2.put(class_ys.g.a(), new AttributeModifier(n[var1.b()], "Armor modifier", (double)this.d, 0));
      }

      return var2;
   }

   public static enum EnumArmorMaterial {
      LEATHER("leather", 5, new int[]{1, 2, 3, 1}, 15, class_ng.s),
      CHAIN("chainmail", 15, new int[]{1, 4, 5, 2}, 12, class_ng.n),
      IRON("iron", 15, new int[]{2, 5, 6, 2}, 9, class_ng.r),
      GOLD("gold", 7, new int[]{1, 3, 5, 2}, 25, class_ng.q),
      DIAMOND("diamond", 33, new int[]{3, 6, 8, 3}, 10, class_ng.o);

      private final String f;
      private final int g;
      private final int[] h;
      private final int i;
      private final class_nf j;

      private EnumArmorMaterial(String var3, int var4, int[] var5, int var6, class_nf var7) {
         this.f = var3;
         this.g = var4;
         this.h = var5;
         this.i = var6;
         this.j = var7;
      }

      public int a(EnumInventorySlot var1) {
         return ItemArmor.m[var1.b()] * this.g;
      }

      public int b(EnumInventorySlot var1) {
         return this.h[var1.b()];
      }

      public int a() {
         return this.i;
      }

      public class_nf b() {
         return this.j;
      }

      public Item c() {
         return this == LEATHER?Items.aM:(this == CHAIN?Items.l:(this == GOLD?Items.m:(this == IRON?Items.l:(this == DIAMOND?Items.k:null))));
      }
   }
}
