package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_azs;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rn;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_vx;
import net.minecraft.server.class_yc;

public class class_vz extends class_vx {
   public class_vz(World var1) {
      super(var1);
      this.a(0.9F, 1.4F);
      this.by = Blocks.bw;
   }

   public boolean a(EntityHuman var1, EnumHand var2, ItemStack var3) {
      if(var3 != null && var3.b() == Items.B && this.l() >= 0 && !var1.abilities.d) {
         if(--var3.b == 0) {
            var1.a((EnumHand)var2, (ItemStack)(new ItemStack(Items.C)));
         } else if(!var1.br.c(new ItemStack(Items.C))) {
            var1.a(new ItemStack(Items.C), false);
         }

         return true;
      } else if(var3 != null && var3.b() == Items.bl && this.l() >= 0) {
         this.S();
         this.world.a(EnumParticle.EXPLOSION_LARGE, this.locX, this.locY + (double)(this.length / 2.0F), this.locZ, 0.0D, 0.0D, 0.0D, new int[0]);
         if(!this.world.E) {
            class_vx var4 = new class_vx(this.world);
            var4.b(this.locX, this.locY, this.locZ, this.yaw, this.pitch);
            var4.c(this.bP());
            var4.aM = this.aM;
            if(this.o_()) {
               var4.c(this.be());
            }

            this.world.a((Entity)var4);

            for(int var5 = 0; var5 < 5; ++var5) {
               this.world.a((Entity)(new class_yc(this.world, this.locX, this.locY + (double)this.length, this.locZ, new ItemStack(Blocks.Q))));
            }

            var3.a(1, (class_rz)var1);
            this.a(class_ng.dt, 1.0F, 1.0F);
         }

         return true;
      } else {
         return super.a(var1, var2, var3);
      }
   }

   public class_vz c(class_rn var1) {
      return new class_vz(this.world);
   }

   protected class_kk J() {
      return class_azs.H;
   }

   // $FF: synthetic method
   public class_vx b(class_rn var1) {
      return this.c(var1);
   }

   // $FF: synthetic method
   public class_rn a(class_rn var1) {
      return this.c(var1);
   }
}
