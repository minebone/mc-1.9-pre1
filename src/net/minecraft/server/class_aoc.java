package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_rz;

public class class_aoc extends Block {
   public static final BlockStateEnum c = BlockStateEnum.a("axis", EnumDirection.class_a_in_class_cq.class);

   protected class_aoc(Material var1) {
      super(var1, var1.r());
   }

   protected class_aoc(Material var1, MaterialMapColor var2) {
      super(var1, var2);
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      switch(class_aoc.SyntheticClass_1.b[var2.ordinal()]) {
      case 1:
      case 2:
         switch(class_aoc.SyntheticClass_1.a[((EnumDirection.class_a_in_class_cq)var1.get(c)).ordinal()]) {
         case 1:
            return var1.set(c, EnumDirection.class_a_in_class_cq.Z);
         case 2:
            return var1.set(c, EnumDirection.class_a_in_class_cq.X);
         default:
            return var1;
         }
      default:
         return var1;
      }
   }

   public IBlockData a(int var1) {
      EnumDirection.class_a_in_class_cq var2 = EnumDirection.class_a_in_class_cq.Y;
      int var3 = var1 & 12;
      if(var3 == 4) {
         var2 = EnumDirection.class_a_in_class_cq.X;
      } else if(var3 == 8) {
         var2 = EnumDirection.class_a_in_class_cq.Z;
      }

      return this.u().set(c, var2);
   }

   public int e(IBlockData var1) {
      int var2 = 0;
      EnumDirection.class_a_in_class_cq var3 = (EnumDirection.class_a_in_class_cq)var1.get(c);
      if(var3 == EnumDirection.class_a_in_class_cq.X) {
         var2 |= 4;
      } else if(var3 == EnumDirection.class_a_in_class_cq.Z) {
         var2 |= 8;
      }

      return var2;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{c});
   }

   protected ItemStack u(IBlockData var1) {
      return new ItemStack(Item.a((Block)this));
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return super.a(var1, var2, var3, var4, var5, var6, var7, var8).set(c, var3.k());
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b = new int[class_aod.values().length];

      static {
         try {
            b[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            b[class_aod.CLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         a = new int[EnumDirection.class_a_in_class_cq.values().length];

         try {
            a[EnumDirection.class_a_in_class_cq.X.ordinal()] = 1;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.class_a_in_class_cq.Z.ordinal()] = 2;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
