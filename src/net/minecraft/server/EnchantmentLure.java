package net.minecraft.server;

import net.minecraft.server.Enchantment;
import net.minecraft.server.class_agm;
import net.minecraft.server.EnumInventorySlot;

public class EnchantmentLure extends Enchantment {
   protected EnchantmentLure(Enchantment.class_a_in_class_agl var1, class_agm var2, EnumInventorySlot... var3) {
      super(var1, var2, var3);
      this.c("fishingSpeed");
   }

   public int a(int var1) {
      return 15 + (var1 - 1) * 9;
   }

   public int b(int var1) {
      return super.a(var1) + 50;
   }

   public int b() {
      return 3;
   }
}
