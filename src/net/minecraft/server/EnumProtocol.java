package net.minecraft.server;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Maps;
import java.util.Iterator;
import java.util.Map;
import net.minecraft.server.EnumProtocolDirection;
import net.minecraft.server.PacketPlayInBlockDig;
import net.minecraft.server.PacketPlayInClientCommand;
import net.minecraft.server.PacketPlayInCustomPayload;
import net.minecraft.server.PacketPlayInResourcePackStatus;
import net.minecraft.server.PacketPlayInUseEntity;
import net.minecraft.server.PacketPlayOutCombatEvent;
import net.minecraft.server.PacketPlayOutCustomPayload;
import net.minecraft.server.PacketPlayOutEntity;
import net.minecraft.server.PacketPlayOutGameStateChange;
import net.minecraft.server.PacketPlayOutPlayerInfo;
import net.minecraft.server.PacketPlayOutPosition;
import net.minecraft.server.PacketPlayOutResourcePackSend;
import net.minecraft.server.PacketPlayOutScoreboardScore;
import net.minecraft.server.PacketPlayOutScoreboardTeam;
import net.minecraft.server.PacketPlayOutTitle;
import net.minecraft.server.PacketPlayOutUpdateAttributes;
import net.minecraft.server.PacketPlayOutWorldBorder;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutSpawnEntity;
import net.minecraft.server.PacketPlayOutSpawnEntityExperienceOrb;
import net.minecraft.server.PacketPlayOutEntityWeather;
import net.minecraft.server.PacketPlayOutEntityLiving;
import net.minecraft.server.PacketPlayOutSpawnEntityPainting;
import net.minecraft.server.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.PacketPlayOutAnimation;
import net.minecraft.server.PacketPlayOutStatistic;
import net.minecraft.server.PacketPlayOutBlockBreakAnimation;
import net.minecraft.server.PacketPlayOutTileEntityData;
import net.minecraft.server.PacketPlayOutBlockAction;
import net.minecraft.server.PacketPlayOutBlockChange;
import net.minecraft.server.PacketPlayOutBossBar;
import net.minecraft.server.PacketPlayOutServerDifficulty;
import net.minecraft.server.PacketPlayOutTabComplete;
import net.minecraft.server.PacketPlayOutChat;
import net.minecraft.server.PacketPlayOutMultiBlockChange;
import net.minecraft.server.PacketPlayOutTransaction;
import net.minecraft.server.PacketPlayOutCloseWindow;
import net.minecraft.server.PacketPlayOutOpenWindow;
import net.minecraft.server.PacketPlayOutWindowItems;
import net.minecraft.server.PacketPlayOutWindowData;
import net.minecraft.server.PacketPlayOutSetSlot;
import net.minecraft.server.class_gg;
import net.minecraft.server.PacketPlayOutNamedSoundEffect;
import net.minecraft.server.PacketPlayOutKickDisconnect;
import net.minecraft.server.PacketPlayOutEntityStatus;
import net.minecraft.server.PacketPlayOutExplosion;
import net.minecraft.server.PacketPlayOutChunkUnload;
import net.minecraft.server.PacketPlayOutKeepAlive;
import net.minecraft.server.PacketPlayOutMapChunk;
import net.minecraft.server.PacketPlayOutWorldEvent;
import net.minecraft.server.PacketPlayOutWorldParticles;
import net.minecraft.server.PacketPlayOutLogin;
import net.minecraft.server.PacketPlayOutMap;
import net.minecraft.server.class_gv;
import net.minecraft.server.PacketPlayOutOpenSignEditor;
import net.minecraft.server.PacketPlayoutAbilities;
import net.minecraft.server.PacketPlayOutBed;
import net.minecraft.server.PacketPlayOutEntityDestroy;
import net.minecraft.server.PacketPlayOutRemoveEntityEffect;
import net.minecraft.server.PacketPlayOutRespawn;
import net.minecraft.server.PacketPlayOutEntityHeadRotation;
import net.minecraft.server.PacketPlayOutCamera;
import net.minecraft.server.PacketPlayOutHeldItemSlot;
import net.minecraft.server.PacketPlayOutScoreboardDisplayObjective;
import net.minecraft.server.PacketPlayOutEntityMetadata;
import net.minecraft.server.PacketPlayOutAttachEntity;
import net.minecraft.server.PacketPlayOutEntityVelocity;
import net.minecraft.server.PacketPlayOutEntityEquipment;
import net.minecraft.server.PacketPlayOutExperience;
import net.minecraft.server.PacketPlayOutUpdateHealth;
import net.minecraft.server.PacketPlayOutScoreboardObjective;
import net.minecraft.server.PacketPlayOutPlayerRefresh;
import net.minecraft.server.PacketPlayOutSpawnPosition;
import net.minecraft.server.PacketPlayOutUpdateTime;
import net.minecraft.server.PacketPlayOutUpdateSign;
import net.minecraft.server.PacketPlayOutNamedSoundSomething;
import net.minecraft.server.PacketPlayOutPlayerListHeaderFooter;
import net.minecraft.server.PacketPlayOutCollect;
import net.minecraft.server.PacketPlayOutEntityTeleport;
import net.minecraft.server.PacketPlayOutEntityEffect;
import net.minecraft.server.PacketPlayInTeleport;
import net.minecraft.server.class_ii;
import net.minecraft.server.class_ij;
import net.minecraft.server.class_il;
import net.minecraft.server.class_im;
import net.minecraft.server.class_in;
import net.minecraft.server.class_io;
import net.minecraft.server.class_ip;
import net.minecraft.server.class_is;
import net.minecraft.server.class_it;
import net.minecraft.server.PacketPlayInVehicleMove;
import net.minecraft.server.PacketPlayInSteer;
import net.minecraft.server.class_iw;
import net.minecraft.server.class_iy;
import net.minecraft.server.class_iz;
import net.minecraft.server.class_jb;
import net.minecraft.server.class_jc;
import net.minecraft.server.class_jd;
import net.minecraft.server.class_je;
import net.minecraft.server.class_jf;
import net.minecraft.server.class_jg;
import net.minecraft.server.class_jh;
import net.minecraft.server.PacketHandshakingInSetProtocol;
import net.minecraft.server.PacketLoginOutSuccess;
import net.minecraft.server.PacketLoginOutEncryptionBegin;
import net.minecraft.server.PacketLoginOutSetCompression;
import net.minecraft.server.PacketLoginOutDisconnect;
import net.minecraft.server.PacketLoginInStart;
import net.minecraft.server.PacketLoginInEncryptionBegin;
import net.minecraft.server.PacketStatusOutPong;
import net.minecraft.server.PacketStatusOutServerInfo;
import net.minecraft.server.PacketStatusInPing;
import net.minecraft.server.PacketStatusInStart;
import org.apache.logging.log4j.LogManager;

public enum EnumProtocol {
   HANDSHAKING(-1) {
      {
         this.addPacket(EnumProtocolDirection.SERVERBOUND, PacketHandshakingInSetProtocol.class);
      }
   },
   PLAY(0) {
      {
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutSpawnEntity.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutSpawnEntityExperienceOrb.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutEntityWeather.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutEntityLiving.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutSpawnEntityPainting.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutNamedEntitySpawn.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutAnimation.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutStatistic.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutBlockBreakAnimation.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutTileEntityData.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutBlockAction.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutBlockChange.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutBossBar.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutServerDifficulty.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutTabComplete.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutChat.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutMultiBlockChange.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutTransaction.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutCloseWindow.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutOpenWindow.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutWindowItems.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutWindowData.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutSetSlot.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, class_gg.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutCustomPayload.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutNamedSoundEffect.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutKickDisconnect.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutEntityStatus.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutExplosion.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutChunkUnload.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutGameStateChange.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutKeepAlive.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutMapChunk.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutWorldEvent.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutWorldParticles.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutLogin.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutMap.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutEntity.class_a_in_class_gu.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutEntity.class_b_in_class_gu.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutEntity.class_c_in_class_gu.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutEntity.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, class_gv.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutOpenSignEditor.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayoutAbilities.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutCombatEvent.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutPlayerInfo.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutPosition.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutBed.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutEntityDestroy.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutRemoveEntityEffect.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutResourcePackSend.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutRespawn.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutEntityHeadRotation.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutWorldBorder.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutCamera.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutHeldItemSlot.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutScoreboardDisplayObjective.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutEntityMetadata.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutAttachEntity.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutEntityVelocity.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutEntityEquipment.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutExperience.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutUpdateHealth.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutScoreboardObjective.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutPlayerRefresh.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutScoreboardTeam.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutScoreboardScore.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutSpawnPosition.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutUpdateTime.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutTitle.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutUpdateSign.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutNamedSoundSomething.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutPlayerListHeaderFooter.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutCollect.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutEntityTeleport.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutUpdateAttributes.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketPlayOutEntityEffect.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, PacketPlayInTeleport.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_ii.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_ij.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, PacketPlayInClientCommand.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_il.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_im.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_in.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_io.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_ip.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, PacketPlayInCustomPayload.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, PacketPlayInUseEntity.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_is.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_it.class_a_in_class_it.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_it.class_b_in_class_it.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_it.class_c_in_class_it.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_it.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, PacketPlayInVehicleMove.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, PacketPlayInSteer.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_iw.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, PacketPlayInBlockDig.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_iy.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_iz.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, PacketPlayInResourcePackStatus.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_jb.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_jc.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_jd.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_je.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_jf.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_jg.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, class_jh.class);
      }
   },
   STATUS(1) {
      {
         this.addPacket(EnumProtocolDirection.SERVERBOUND, PacketStatusInStart.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketStatusOutServerInfo.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, PacketStatusInPing.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketStatusOutPong.class);
      }
   },
   LOGIN(2) {
      {
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketLoginOutDisconnect.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketLoginOutEncryptionBegin.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketLoginOutSuccess.class);
         this.addPacket(EnumProtocolDirection.CLIENTBOUND, PacketLoginOutSetCompression.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, PacketLoginInStart.class);
         this.addPacket(EnumProtocolDirection.SERVERBOUND, PacketLoginInEncryptionBegin.class);
      }
   };

   private static int e = -1;
   private static int f = 2;
   private static final EnumProtocol[] g = new EnumProtocol[f - e + 1];
   private static final Map h = Maps.newHashMap();
   private final int i;
   private final Map j;

   private EnumProtocol(int var3) {
      this.j = Maps.newEnumMap(EnumProtocolDirection.class);
      this.i = var3;
   }

   protected EnumProtocol addPacket(EnumProtocolDirection var1, Class var2) {
      Object var3 = (BiMap)this.j.get(var1);
      if(var3 == null) {
         var3 = HashBiMap.create();
         this.j.put(var1, var3);
      }

      if(((BiMap)var3).containsValue(var2)) {
         String var4 = var1 + " packet " + var2 + " is already known to ID " + ((BiMap)var3).inverse().get(var2);
         LogManager.getLogger().fatal(var4);
         throw new IllegalArgumentException(var4);
      } else {
    	  if(var2.getSimpleName().contains("Out")) {
    		 //System.out.println(var2.getSimpleName() + " - " + var1.toString().toLowerCase() + " - " + ((BiMap)var3).size());
    	  }
         ((BiMap)var3).put(Integer.valueOf(((BiMap)var3).size()), var2);
         return this;
      }
   }

   public Integer a(EnumProtocolDirection var1, Packet var2) {
      return (Integer)((BiMap)this.j.get(var1)).inverse().get(var2.getClass());
   }

   public Packet a(EnumProtocolDirection var1, int var2) throws IllegalAccessException, InstantiationException {
      Class var3 = (Class)((BiMap)this.j.get(var1)).get(Integer.valueOf(var2));
      return var3 == null?null:(Packet)var3.newInstance();
   }

   public int a() {
      return this.i;
   }

   public static EnumProtocol a(int var0) {
      return var0 >= e && var0 <= f?g[var0 - e]:null;
   }

   public static EnumProtocol a(Packet var0) {
      return (EnumProtocol)h.get(var0.getClass());
   }

   // $FF: synthetic method
   EnumProtocol(int var3, Object var4) {
      this(var3);
   }

   static {
      EnumProtocol[] var0 = values();
      int var1 = var0.length;

      for(int var2 = 0; var2 < var1; ++var2) {
         EnumProtocol var3 = var0[var2];
         int var4 = var3.a();
         if(var4 < e || var4 > f) {
            throw new Error("Invalid protocol ID " + Integer.toString(var4));
         }

         g[var4 - e] = var3;
         Iterator var5 = var3.j.keySet().iterator();

         while(var5.hasNext()) {
            EnumProtocolDirection var6 = (EnumProtocolDirection)var5.next();

            Class var8;
            for(Iterator var7 = ((BiMap)var3.j.get(var6)).values().iterator(); var7.hasNext(); h.put(var8, var3)) {
               var8 = (Class)var7.next();
               if(h.containsKey(var8) && h.get(var8) != var3) {
                  throw new Error("Packet " + var8 + " is already assigned to protocol " + h.get(var8) + " - can\'t reassign to " + var3);
               }

               try {
                  var8.newInstance();
               } catch (Throwable var10) {
                  throw new Error("Packet " + var8 + " fails instantiation checks! " + var8);
               }
            }
         }
      }

   }
}
