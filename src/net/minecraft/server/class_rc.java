package net.minecraft.server;

import net.minecraft.server.ChatMessage;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.Vec3D;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_di;
import net.minecraft.server.class_rz;

public class class_rc extends DamageSource {
   protected Entity s;
   private boolean t = false;

   public class_rc(String var1, Entity var2) {
      super(var1);
      this.s = var2;
   }

   public class_rc w() {
      this.t = true;
      return this;
   }

   public boolean x() {
      return this.t;
   }

   public Entity j() {
      return this.s;
   }

   public IChatBaseComponent c(class_rz var1) {
      ItemStack var2 = this.s instanceof class_rz?((class_rz)this.s).ca():null;
      String var3 = "death.attack." + this.r;
      String var4 = var3 + ".item";
      return var2 != null && var2.s() && class_di.c(var4)?new ChatMessage(var4, new Object[]{var1.i_(), this.s.i_(), var2.B()}):new ChatMessage(var3, new Object[]{var1.i_(), this.s.i_()});
   }

   public boolean r() {
      return this.s != null && this.s instanceof class_rz && !(this.s instanceof EntityHuman);
   }

   public Vec3D v() {
      return new Vec3D(this.s.locX, this.s.locY, this.s.locZ);
   }
}
