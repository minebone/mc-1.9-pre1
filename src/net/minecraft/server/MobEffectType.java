package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;
import net.minecraft.server.AttributeMapBase;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.class_cx;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_rf;
import net.minecraft.server.class_rg;
import net.minecraft.server.class_rh;
import net.minecraft.server.class_ri;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sl;
import net.minecraft.server.class_ys;

public class MobEffectType {
   public static final class_cx b = new class_cx();
   private final Map a = Maps.newHashMap();
   private final boolean c;
   private final int d;
   private String e = "";
   private int f = -1;
   private double g;
   private boolean h;

   public static MobEffectType a(int var0) {
      return (MobEffectType)b.a(var0);
   }

   public static int a(MobEffectType var0) {
      return b.a(var0);
   }

   public static MobEffectType b(String var0) {
      return (MobEffectType)b.c(new class_kk(var0));
   }

   protected MobEffectType(boolean var1, int var2) {
      this.c = var1;
      if(var1) {
         this.g = 0.5D;
      } else {
         this.g = 1.0D;
      }

      this.d = var2;
   }

   protected MobEffectType b(int var1, int var2) {
      this.f = var1 + var2 * 8;
      return this;
   }

   public void a(class_rz var1, int var2) {
      if(this == MobEffectList.j) {
         if(var1.bP() < var1.bV()) {
            var1.b(1.0F);
         }
      } else if(this == MobEffectList.s) {
         if(var1.bP() > 1.0F) {
            var1.a(DamageSource.m, 1.0F);
         }
      } else if(this == MobEffectList.t) {
         var1.a(DamageSource.n, 1.0F);
      } else if(this == MobEffectList.q && var1 instanceof EntityHuman) {
         ((EntityHuman)var1).a(0.025F * (float)(var2 + 1));
      } else if(this == MobEffectList.w && var1 instanceof EntityHuman) {
         if(!var1.world.E) {
            ((EntityHuman)var1).cR().a(var2 + 1, 1.0F);
         }
      } else if((this != MobEffectList.f || var1.bO()) && (this != MobEffectList.g || !var1.bO())) {
         if(this == MobEffectList.g && !var1.bO() || this == MobEffectList.f && var1.bO()) {
            var1.a(DamageSource.m, (float)(6 << var2));
         }
      } else {
         var1.b((float)Math.max(4 << var2, 0));
      }

   }

   public void a(Entity var1, Entity var2, class_rz var3, int var4, double var5) {
      int var7;
      if((this != MobEffectList.f || var3.bO()) && (this != MobEffectList.g || !var3.bO())) {
         if(this == MobEffectList.g && !var3.bO() || this == MobEffectList.f && var3.bO()) {
            var7 = (int)(var5 * (double)(6 << var4) + 0.5D);
            if(var1 == null) {
               var3.a(DamageSource.m, (float)var7);
            } else {
               var3.a(DamageSource.b(var1, var2), (float)var7);
            }
         }
      } else {
         var7 = (int)(var5 * (double)(4 << var4) + 0.5D);
         var3.b((float)var7);
      }

   }

   public boolean a(int var1, int var2) {
      int var3;
      if(this == MobEffectList.j) {
         var3 = 50 >> var2;
         return var3 > 0?var1 % var3 == 0:true;
      } else if(this == MobEffectList.s) {
         var3 = 25 >> var2;
         return var3 > 0?var1 % var3 == 0:true;
      } else if(this == MobEffectList.t) {
         var3 = 40 >> var2;
         return var3 > 0?var1 % var3 == 0:true;
      } else {
         return this == MobEffectList.q;
      }
   }

   public boolean b() {
      return false;
   }

   public MobEffectType c(String var1) {
      this.e = var1;
      return this;
   }

   public String a() {
      return this.e;
   }

   protected MobEffectType a(double var1) {
      this.g = var1;
      return this;
   }

   public int g() {
      return this.d;
   }

   public MobEffectType a(class_sk var1, String var2, double var3, int var5) {
      AttributeModifier var6 = new AttributeModifier(UUID.fromString(var2), this.a(), var3, var5);
      this.a.put(var1, var6);
      return this;
   }

   public void a(class_rz var1, AttributeMapBase var2, int var3) {
      Iterator var4 = this.a.entrySet().iterator();

      while(var4.hasNext()) {
         Entry var5 = (Entry)var4.next();
         class_sl var6 = var2.a((class_sk)var5.getKey());
         if(var6 != null) {
            var6.c((AttributeModifier)var5.getValue());
         }
      }

   }

   public void b(class_rz var1, AttributeMapBase var2, int var3) {
      Iterator var4 = this.a.entrySet().iterator();

      while(var4.hasNext()) {
         Entry var5 = (Entry)var4.next();
         class_sl var6 = var2.a((class_sk)var5.getKey());
         if(var6 != null) {
            AttributeModifier var7 = (AttributeModifier)var5.getValue();
            var6.c(var7);
            var6.b(new AttributeModifier(var7.a(), this.a() + " " + var3, this.a(var3, var7), var7.c()));
         }
      }

   }

   public double a(int var1, AttributeModifier var2) {
      return var2.d() * (double)(var1 + 1);
   }

   public MobEffectType j() {
      this.h = true;
      return this;
   }

   public static void k() {
      b.a(1, new class_kk("speed"), (new MobEffectType(false, 8171462)).c("effect.moveSpeed").b(0, 0).a(class_ys.d, "91AEAA56-376B-4498-935B-2F7F68070635", 0.20000000298023224D, 2).j());
      b.a(2, new class_kk("slowness"), (new MobEffectType(true, 5926017)).c("effect.moveSlowdown").b(1, 0).a(class_ys.d, "7107DE5E-7CE8-4030-940E-514C1F160890", -0.15000000596046448D, 2));
      b.a(3, new class_kk("haste"), (new MobEffectType(false, 14270531)).c("effect.digSpeed").b(2, 0).a(1.5D).j().a(class_ys.f, "AF8B6E3F-3328-4C0A-AA36-5BA2BB9DBEF3", 0.10000000149011612D, 2));
      b.a(4, new class_kk("mining_fatigue"), (new MobEffectType(true, 4866583)).c("effect.digSlowDown").b(3, 0).a(class_ys.f, "55FCED67-E92A-486E-9800-B47F202C4386", -0.10000000149011612D, 2));
      b.a(5, new class_kk("strength"), (new class_rg(false, 9643043, 3.0D)).c("effect.damageBoost").b(4, 0).a(class_ys.e, "648D7064-6A60-4F59-8ABE-C2C23A6DD7A9", 0.0D, 0).j());
      b.a(6, new class_kk("instant_health"), (new class_ri(false, 16262179)).c("effect.heal").j());
      b.a(7, new class_kk("instant_damage"), (new class_ri(true, 4393481)).c("effect.harm").j());
      b.a(8, new class_kk("jump_boost"), (new MobEffectType(false, 2293580)).c("effect.jump").b(2, 1).j());
      b.a(9, new class_kk("nausea"), (new MobEffectType(true, 5578058)).c("effect.confusion").b(3, 1).a(0.25D));
      b.a(10, new class_kk("regeneration"), (new MobEffectType(false, 13458603)).c("effect.regeneration").b(7, 0).a(0.25D).j());
      b.a(11, new class_kk("resistance"), (new MobEffectType(false, 10044730)).c("effect.resistance").b(6, 1).j());
      b.a(12, new class_kk("fire_resistance"), (new MobEffectType(false, 14981690)).c("effect.fireResistance").b(7, 1).j());
      b.a(13, new class_kk("water_breathing"), (new MobEffectType(false, 3035801)).c("effect.waterBreathing").b(0, 2).j());
      b.a(14, new class_kk("invisibility"), (new MobEffectType(false, 8356754)).c("effect.invisibility").b(0, 1).j());
      b.a(15, new class_kk("blindness"), (new MobEffectType(true, 2039587)).c("effect.blindness").b(5, 1).a(0.25D));
      b.a(16, new class_kk("night_vision"), (new MobEffectType(false, 2039713)).c("effect.nightVision").b(4, 1).j());
      b.a(17, new class_kk("hunger"), (new MobEffectType(true, 5797459)).c("effect.hunger").b(1, 1));
      b.a(18, new class_kk("weakness"), (new class_rg(true, 4738376, -4.0D)).c("effect.weakness").b(5, 0).a(class_ys.e, "22653B89-116E-49DC-9B6B-9971489B5BE5", 0.0D, 0));
      b.a(19, new class_kk("poison"), (new MobEffectType(true, 5149489)).c("effect.poison").b(6, 0).a(0.25D));
      b.a(20, new class_kk("wither"), (new MobEffectType(true, 3484199)).c("effect.wither").b(1, 2).a(0.25D));
      b.a(21, new class_kk("health_boost"), (new class_rh(false, 16284963)).c("effect.healthBoost").b(7, 2).a(class_ys.a, "5D6F0BA2-1186-46AC-B896-C61C5CEE99CC", 4.0D, 0).j());
      b.a(22, new class_kk("absorption"), (new class_rf(false, 2445989)).c("effect.absorption").b(2, 2).j());
      b.a(23, new class_kk("saturation"), (new class_ri(false, 16262179)).c("effect.saturation").j());
      b.a(24, new class_kk("glowing"), (new MobEffectType(false, 9740385)).c("effect.glowing").b(4, 2));
      b.a(25, new class_kk("levitation"), (new MobEffectType(true, 13565951)).c("effect.levitation").b(3, 2));
      b.a(26, new class_kk("luck"), (new MobEffectType(false, 3381504)).c("effect.luck").b(5, 2).j().a(class_ys.h, "03C3C89D-7037-4B42-869F-B146BCB64D2E", 1.0D, 0));
      b.a(27, new class_kk("unluck"), (new MobEffectType(true, 12624973)).c("effect.unluck").b(6, 2).a(class_ys.h, "CC5AF142-2BD2-4215-B636-2605AED11727", -1.0D, 0));
   }
}
