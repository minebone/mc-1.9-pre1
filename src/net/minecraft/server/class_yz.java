package net.minecraft.server;

import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.AttributeRanged;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.EnumMonsterType;
import net.minecraft.server.Items;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.MobEffectType;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.PathfinderGoalMoveThroughVillage;
import net.minecraft.server.World;
import net.minecraft.server.EntityBoat;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_qk;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_ru;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sl;
import net.minecraft.server.class_tb;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_to;
import net.minecraft.server.class_tv;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_ur;
import net.minecraft.server.class_uu;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_vd;
import net.minecraft.server.class_vw;
import net.minecraft.server.class_wg;
import net.minecraft.server.class_yh;
import net.minecraft.server.class_yp;
import net.minecraft.server.class_yq;
import net.minecraft.server.class_ys;
import net.minecraft.server.class_zd;

public class class_yz extends class_yp {
   protected static final class_sk a = (new AttributeRanged((class_sk)null, "zombie.spawnReinforcements", 0.0D, 0.0D, 1.0D)).a("Spawn Reinforcements Chance");
   private static final UUID b = UUID.fromString("B9766B59-9566-4402-BC1F-2EE2A276D836");
   private static final AttributeModifier c = new AttributeModifier(b, "Baby speed boost", 0.5D, 1);
   private static final class_ke bv = DataWatcher.a(class_yz.class, class_kg.h);
   private static final class_ke bw = DataWatcher.a(class_yz.class, class_kg.b);
   private static final class_ke bx = DataWatcher.a(class_yz.class, class_kg.h);
   private static final class_ke by = DataWatcher.a(class_yz.class, class_kg.h);
   private final class_tb bz = new class_tb(this);
   private int bA;
   private boolean bB = false;
   private float bC = -1.0F;
   private float bD;

   public class_yz(World var1) {
      super(var1);
      this.a(0.6F, 1.95F);
   }

   protected void r() {
      this.bp.a(0, new class_tg(this));
      this.bp.a(2, new class_ur(this, 1.0D, false));
      this.bp.a(5, new class_tv(this, 1.0D));
      this.bp.a(7, new class_uf(this, 1.0D));
      this.bp.a(8, new class_to(this, EntityHuman.class, 8.0F));
      this.bp.a(8, new class_ue(this));
      this.o();
   }

   protected void o() {
      this.bp.a(6, new PathfinderGoalMoveThroughVillage(this, 1.0D, false));
      this.bq.a(1, new class_uu(this, true, new Class[]{class_yq.class}));
      this.bq.a(2, new class_ux(this, EntityHuman.class, true));
      this.bq.a(3, new class_ux(this, class_zd.class, false));
      this.bq.a(3, new class_ux(this, class_wg.class, true));
   }

   protected void bz() {
      super.bz();
      this.a((class_sk)class_ys.b).a(35.0D);
      this.a((class_sk)class_ys.d).a(0.23000000417232513D);
      this.a((class_sk)class_ys.e).a(3.0D);
      this.a((class_sk)class_ys.g).a(2.0D);
      this.bY().b(a).a(this.random.nextDouble() * 0.10000000149011612D);
   }

   protected void i() {
      super.i();
      this.Q().a((class_ke)bv, (Object)Boolean.valueOf(false));
      this.Q().a((class_ke)bw, (Object)Integer.valueOf(0));
      this.Q().a((class_ke)bx, (Object)Boolean.valueOf(false));
      this.Q().a((class_ke)by, (Object)Boolean.valueOf(false));
   }

   public void a(boolean var1) {
      this.Q().b(by, Boolean.valueOf(var1));
   }

   public boolean dc() {
      return this.bB;
   }

   public void o(boolean var1) {
      if(this.bB != var1) {
         this.bB = var1;
         ((class_vd)this.x()).a(var1);
         if(var1) {
            this.bp.a(1, this.bz);
         } else {
            this.bp.a((class_tj)this.bz);
         }
      }

   }

   public boolean m_() {
      return ((Boolean)this.Q().a(bv)).booleanValue();
   }

   protected int b(EntityHuman var1) {
      if(this.m_()) {
         this.b_ = (int)((float)this.b_ * 2.5F);
      }

      return super.b(var1);
   }

   public void p(boolean var1) {
      this.Q().b(bv, Boolean.valueOf(var1));
      if(this.world != null && !this.world.E) {
         class_sl var2 = this.a((class_sk)class_ys.d);
         var2.c(c);
         if(var1) {
            var2.b(c);
         }
      }

      this.q(var1);
   }

   public boolean dd() {
      return ((Integer)this.Q().a(bw)).intValue() > 0;
   }

   public int de() {
      return ((Integer)this.Q().a(bw)).intValue() - 1;
   }

   public void a(int var1) {
      this.Q().b(bw, Integer.valueOf(var1 + 1));
   }

   public void df() {
      this.Q().b(bw, Integer.valueOf(0));
   }

   public void a(class_ke var1) {
      if(bv.equals(var1)) {
         this.q(this.m_());
      }

      super.a(var1);
   }

   public void n() {
      if(this.world.B() && !this.world.E && !this.m_()) {
         float var1 = this.e(1.0F);
         BlockPosition var2 = this.bx() instanceof EntityBoat?(new BlockPosition(this.locX, (double)Math.round(this.locY), this.locZ)).a():new BlockPosition(this.locX, (double)Math.round(this.locY), this.locZ);
         if(var1 > 0.5F && this.random.nextFloat() * 30.0F < (var1 - 0.4F) * 2.0F && this.world.h(var2)) {
            boolean var3 = true;
            ItemStack var4 = this.a((EnumInventorySlot)EnumInventorySlot.HEAD);
            if(var4 != null) {
               if(var4.e()) {
                  var4.b(var4.h() + this.random.nextInt(2));
                  if(var4.h() >= var4.j()) {
                     this.b((ItemStack)var4);
                     this.a((EnumInventorySlot)EnumInventorySlot.HEAD, (ItemStack)null);
                  }
               }

               var3 = false;
            }

            if(var3) {
               this.g(8);
            }
         }
      }

      super.n();
   }

   public boolean a(DamageSource var1, float var2) {
      if(super.a(var1, var2)) {
         class_rz var3 = this.A();
         if(var3 == null && var1.j() instanceof class_rz) {
            var3 = (class_rz)var1.j();
         }

         if(var3 != null && this.world.ae() == EnumDifficulty.HARD && (double)this.random.nextFloat() < this.a((class_sk)a).e() && this.world.U().b("doMobSpawning")) {
            int var4 = MathHelper.c(this.locX);
            int var5 = MathHelper.c(this.locY);
            int var6 = MathHelper.c(this.locZ);
            class_yz var7 = new class_yz(this.world);

            for(int var8 = 0; var8 < 50; ++var8) {
               int var9 = var4 + MathHelper.a((Random)this.random, 7, 40) * MathHelper.a((Random)this.random, -1, 1);
               int var10 = var5 + MathHelper.a((Random)this.random, 7, 40) * MathHelper.a((Random)this.random, -1, 1);
               int var11 = var6 + MathHelper.a((Random)this.random, 7, 40) * MathHelper.a((Random)this.random, -1, 1);
               if(this.world.getType(new BlockPosition(var9, var10 - 1, var11)).q() && this.world.k(new BlockPosition(var9, var10, var11)) < 10) {
                  var7.b((double)var9, (double)var10, (double)var11);
                  if(!this.world.a((double)var9, (double)var10, (double)var11, 7.0D) && this.world.a((AxisAlignedBB)var7.bk(), (Entity)var7) && this.world.a((Entity)var7, (AxisAlignedBB)var7.bk()).isEmpty() && !this.world.e(var7.bk())) {
                     this.world.a((Entity)var7);
                     var7.c(var3);
                     var7.a((class_qk)this.world.D(new BlockPosition(var7)), (class_sc)null);
                     this.a((class_sk)a).b(new AttributeModifier("Zombie reinforcement caller charge", -0.05000000074505806D, 0));
                     var7.a((class_sk)a).b(new AttributeModifier("Zombie reinforcement callee charge", -0.05000000074505806D, 0));
                     break;
                  }
               }
            }
         }

         return true;
      } else {
         return false;
      }
   }

   public void m() {
      if(!this.world.E && this.dg()) {
         int var1 = this.di();
         this.bA -= var1;
         if(this.bA <= 0) {
            this.dh();
         }
      }

      super.m();
   }

   public boolean B(Entity var1) {
      boolean var2 = super.B(var1);
      if(var2) {
         int var3 = this.world.ae().a();
         if(this.ca() == null && this.aG() && this.random.nextFloat() < (float)var3 * 0.3F) {
            var1.g(2 * var3);
         }
      }

      return var2;
   }

   protected class_nf G() {
      return this.dd()?class_ng.hn:class_ng.gZ;
   }

   protected class_nf bQ() {
      return this.dd()?class_ng.hr:class_ng.hh;
   }

   protected class_nf bR() {
      return this.dd()?class_ng.hq:class_ng.hd;
   }

   protected void a(BlockPosition var1, Block var2) {
      this.a(this.dd()?class_ng.ht:class_ng.hm, 0.15F, 1.0F);
   }

   public EnumMonsterType bZ() {
      return EnumMonsterType.UNDEAD;
   }

   protected class_kk J() {
      return class_azs.ah;
   }

   protected void a(class_qk var1) {
      super.a(var1);
      if(this.random.nextFloat() < (this.world.ae() == EnumDifficulty.HARD?0.05F:0.01F)) {
         int var2 = this.random.nextInt(3);
         if(var2 == 0) {
            this.a((EnumInventorySlot)EnumInventorySlot.MAINHAND, (ItemStack)(new ItemStack(Items.n)));
         } else {
            this.a((EnumInventorySlot)EnumInventorySlot.MAINHAND, (ItemStack)(new ItemStack(Items.a)));
         }
      }

   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      if(this.m_()) {
         var1.a("IsBaby", true);
      }

      if(this.dd()) {
         var1.a("IsVillager", true);
         var1.a("VillagerProfession", this.de());
      }

      var1.a("ConversionTime", this.dg()?this.bA:-1);
      var1.a("CanBreakDoors", this.dc());
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      if(var1.p("IsBaby")) {
         this.p(true);
      }

      if(var1.p("IsVillager")) {
         if(var1.b("VillagerProfession", 99)) {
            this.a(var1.h("VillagerProfession"));
         } else {
            this.a(this.world.r.nextInt(5));
         }
      }

      if(var1.b("ConversionTime", 99) && var1.h("ConversionTime") > -1) {
         this.b(var1.h("ConversionTime"));
      }

      this.o(var1.p("CanBreakDoors"));
   }

   public void b(class_rz var1) {
      super.b(var1);
      if((this.world.ae() == EnumDifficulty.NORMAL || this.world.ae() == EnumDifficulty.HARD) && var1 instanceof class_zd) {
         if(this.world.ae() != EnumDifficulty.HARD && this.random.nextBoolean()) {
            return;
         }

         class_zd var2 = (class_zd)var1;
         class_yz var3 = new class_yz(this.world);
         var3.u(var1);
         this.world.e((Entity)var1);
         var3.a((class_qk)this.world.D(new BlockPosition(var3)), (class_sc)(new class_yz.class_a_in_class_yz(false, true)));
         var3.a(var2.cZ());
         var3.p(var1.m_());
         var3.m(var2.cR());
         if(var2.o_()) {
            var3.c(var2.be());
            var3.i(var2.bf());
         }

         this.world.a((Entity)var3);
         this.world.a((EntityHuman)null, 1026, new BlockPosition((int)this.locX, (int)this.locY, (int)this.locZ), 0);
      }

   }

   public float bm() {
      float var1 = 1.74F;
      if(this.m_()) {
         var1 = (float)((double)var1 - 0.81D);
      }

      return var1;
   }

   protected boolean c(ItemStack var1) {
      return var1.b() == Items.aW && this.m_() && this.aH()?false:super.c(var1);
   }

   public class_sc a(class_qk var1, class_sc var2) {
      Object var7 = super.a(var1, var2);
      float var3 = var1.c();
      this.l(this.random.nextFloat() < 0.55F * var3);
      if(var7 == null) {
         var7 = new class_yz.class_a_in_class_yz(this.world.r.nextFloat() < 0.05F, this.world.r.nextFloat() < 0.05F);
      }

      if(var7 instanceof class_yz.class_a_in_class_yz) {
         class_yz.class_a_in_class_yz var4 = (class_yz.class_a_in_class_yz)var7;
         if(var4.b) {
            this.a(this.random.nextInt(5));
         }

         if(var4.a) {
            this.p(true);
            if((double)this.world.r.nextFloat() < 0.05D) {
               List var5 = this.world.a(class_vw.class, this.bk().b(5.0D, 3.0D, 5.0D), class_ru.b);
               if(!var5.isEmpty()) {
                  class_vw var6 = (class_vw)var5.get(0);
                  var6.o(true);
                  this.m(var6);
               }
            } else if((double)this.world.r.nextFloat() < 0.05D) {
               class_vw var10 = new class_vw(this.world);
               var10.b(this.locX, this.locY, this.locZ, this.yaw, 0.0F);
               var10.a(var1, (class_sc)null);
               var10.o(true);
               this.world.a((Entity)var10);
               this.m(var10);
            }
         }
      }

      this.o(this.random.nextFloat() < var3 * 0.1F);
      this.a(var1);
      this.b((class_qk)var1);
      if(this.a((EnumInventorySlot)EnumInventorySlot.HEAD) == null) {
         Calendar var8 = this.world.ac();
         if(var8.get(2) + 1 == 10 && var8.get(5) == 31 && this.random.nextFloat() < 0.25F) {
            this.a((EnumInventorySlot)EnumInventorySlot.HEAD, (ItemStack)(new ItemStack(this.random.nextFloat() < 0.1F?Blocks.aZ:Blocks.aU)));
            this.bs[EnumInventorySlot.HEAD.b()] = 0.0F;
         }
      }

      this.a((class_sk)class_ys.c).b(new AttributeModifier("Random spawn bonus", this.random.nextDouble() * 0.05000000074505806D, 0));
      double var9 = this.random.nextDouble() * 1.5D * (double)var3;
      if(var9 > 1.0D) {
         this.a((class_sk)class_ys.b).b(new AttributeModifier("Random zombie-spawn bonus", var9, 2));
      }

      if(this.random.nextFloat() < var3 * 0.05F) {
         this.a((class_sk)a).b(new AttributeModifier("Leader zombie bonus", this.random.nextDouble() * 0.25D + 0.5D, 0));
         this.a((class_sk)class_ys.a).b(new AttributeModifier("Leader zombie bonus", this.random.nextDouble() * 3.0D + 1.0D, 2));
         this.o(true);
      }

      return (class_sc)var7;
   }

   public boolean a(EntityHuman var1, EnumHand var2, ItemStack var3) {
      if(var3 != null && var3.b() == Items.aq && var3.i() == 0 && this.dd() && this.a((MobEffectType)MobEffectList.r)) {
         if(!var1.abilities.d) {
            --var3.b;
         }

         if(!this.world.E) {
            this.b(this.random.nextInt(2401) + 3600);
         }

         return true;
      } else {
         return false;
      }
   }

   protected void b(int var1) {
      this.bA = var1;
      this.Q().b(bx, Boolean.valueOf(true));
      this.d(MobEffectList.r);
      this.c(new MobEffect(MobEffectList.e, var1, Math.min(this.world.ae().a() - 1, 0)));
      this.world.a((Entity)this, (byte)16);
   }

   protected boolean K() {
      return !this.dg();
   }

   public boolean dg() {
      return ((Boolean)this.Q().a(bx)).booleanValue();
   }

   protected void dh() {
      class_zd var1 = new class_zd(this.world);
      var1.u(this);
      var1.a(this.world.D(new BlockPosition(var1)), (class_sc)null);
      var1.dd();
      if(this.m_()) {
         var1.b_(-24000);
      }

      this.world.e((Entity)this);
      var1.m(this.cR());
      var1.l(this.de());
      if(this.o_()) {
         var1.c(this.be());
         var1.i(this.bf());
      }

      this.world.a((Entity)var1);
      var1.c(new MobEffect(MobEffectList.i, 200, 0));
      this.world.a((EntityHuman)null, 1027, new BlockPosition((int)this.locX, (int)this.locY, (int)this.locZ), 0);
   }

   protected int di() {
      int var1 = 1;
      if(this.random.nextFloat() < 0.01F) {
         int var2 = 0;
         BlockPosition.class_a_in_class_cj var3 = new BlockPosition.class_a_in_class_cj();

         for(int var4 = (int)this.locX - 4; var4 < (int)this.locX + 4 && var2 < 14; ++var4) {
            for(int var5 = (int)this.locY - 4; var5 < (int)this.locY + 4 && var2 < 14; ++var5) {
               for(int var6 = (int)this.locZ - 4; var6 < (int)this.locZ + 4 && var2 < 14; ++var6) {
                  Block var7 = this.world.getType(var3.c(var4, var5, var6)).getBlock();
                  if(var7 == Blocks.bi || var7 == Blocks.C) {
                     if(this.random.nextFloat() < 0.3F) {
                        ++var1;
                     }

                     ++var2;
                  }
               }
            }
         }
      }

      return var1;
   }

   public void q(boolean var1) {
      this.a(var1?0.5F:1.0F);
   }

   protected final void a(float var1, float var2) {
      boolean var3 = this.bC > 0.0F && this.bD > 0.0F;
      this.bC = var1;
      this.bD = var2;
      if(!var3) {
         this.a(1.0F);
      }

   }

   protected final void a(float var1) {
      super.a(this.bC * var1, this.bD * var1);
   }

   public double aw() {
      return this.m_()?0.0D:-0.35D;
   }

   public void a(DamageSource var1) {
      super.a(var1);
      if(var1.j() instanceof class_yh && !(this instanceof class_yq) && ((class_yh)var1.j()).o() && ((class_yh)var1.j()).dd()) {
         ((class_yh)var1.j()).de();
         this.a(new ItemStack(Items.ch, 1, 2), 0.0F);
      }

   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
   }

   class class_a_in_class_yz implements class_sc {
      public boolean a;
      public boolean b;

      private class_a_in_class_yz(boolean var2, boolean var3) {
         this.a = false;
         this.b = false;
         this.a = var2;
         this.b = var3;
      }

      // $FF: synthetic method
      class_a_in_class_yz(boolean var2, boolean var3, class_yz.SyntheticClass_1 var4) {
         this(var2, var3);
      }
   }
}
