package net.minecraft.server;

import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumAnimation;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;
import net.minecraft.server.class_rz;

public class class_adw extends Item {
   public class_adw() {
      this.d(1);
      this.a(CreativeModeTab.f);
   }

   public ItemStack a(ItemStack var1, World var2, class_rz var3) {
      if(var3 instanceof EntityHuman && !((EntityHuman)var3).abilities.d) {
         --var1.b;
      }

      if(!var2.E) {
         var3.bM();
      }

      if(var3 instanceof EntityHuman) {
         ((EntityHuman)var3).b(StatisticList.b((Item)this));
      }

      return var1.b <= 0?new ItemStack(Items.ay):var1;
   }

   public int e(ItemStack var1) {
      return 32;
   }

   public EnumAnimation f(ItemStack var1) {
      return EnumAnimation.DRINK;
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      var3.c(var4);
      return new class_qo(EnumResult.SUCCESS, var1);
   }
}
