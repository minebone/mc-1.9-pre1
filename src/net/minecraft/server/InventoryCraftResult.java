package net.minecraft.server;

import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_qg;

public class InventoryCraftResult implements IInventory {
   private ItemStack[] a = new ItemStack[1];

   public int u_() {
      return 1;
   }

   public ItemStack a(int var1) {
      return this.a[0];
   }

   public String h_() {
      return "Result";
   }

   public boolean o_() {
      return false;
   }

   public IChatBaseComponent i_() {
      return (IChatBaseComponent)(this.o_()?new ChatComponentText(this.h_()):new ChatMessage(this.h_(), new Object[0]));
   }

   public ItemStack a(int var1, int var2) {
      return class_qg.a(this.a, 0);
   }

   public ItemStack b(int var1) {
      return class_qg.a(this.a, 0);
   }

   public void a(int var1, ItemStack var2) {
      this.a[0] = var2;
   }

   public int w_() {
      return 64;
   }

   public void v_() {
   }

   public boolean a(EntityHuman var1) {
      return true;
   }

   public void b(EntityHuman var1) {
   }

   public void c(EntityHuman var1) {
   }

   public boolean b(int var1, ItemStack var2) {
      return true;
   }

   public int c_(int var1) {
      return 0;
   }

   public void b(int var1, int var2) {
   }

   public int g() {
      return 0;
   }

   public void l() {
      for(int var1 = 0; var1 < this.a.length; ++var1) {
         this.a[var1] = null;
      }

   }
}
