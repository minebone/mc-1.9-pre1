package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import net.minecraft.server.Block;
import net.minecraft.server.BlockLongGrass;
import net.minecraft.server.BlockStatePredicate;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_tj;

public class class_te extends class_tj {
   private static final Predicate b = BlockStatePredicate.a((Block)Blocks.H).a(BlockLongGrass.a, Predicates.equalTo(BlockLongGrass.EnumTallGrassType.GRASS));
   private EntityInsentient c;
   private World d;
   int a;

   public class_te(EntityInsentient var1) {
      this.c = var1;
      this.d = var1.world;
      this.a(7);
   }

   public boolean a() {
      if(this.c.bE().nextInt(this.c.m_()?50:1000) != 0) {
         return false;
      } else {
         BlockPosition var1 = new BlockPosition(this.c.locX, this.c.locY, this.c.locZ);
         return b.apply(this.d.getType(var1))?true:this.d.getType(var1.b()).getBlock() == Blocks.c;
      }
   }

   public void c() {
      this.a = 40;
      this.d.a((Entity)this.c, (byte)10);
      this.c.x().o();
   }

   public void d() {
      this.a = 0;
   }

   public boolean b() {
      return this.a > 0;
   }

   public int f() {
      return this.a;
   }

   public void e() {
      this.a = Math.max(0, this.a - 1);
      if(this.a == 4) {
         BlockPosition var1 = new BlockPosition(this.c.locX, this.c.locY, this.c.locZ);
         if(b.apply(this.d.getType(var1))) {
            if(this.d.U().b("mobGriefing")) {
               this.d.b(var1, false);
            }

            this.c.B();
         } else {
            BlockPosition var2 = var1.b();
            if(this.d.getType(var2).getBlock() == Blocks.c) {
               if(this.d.U().b("mobGriefing")) {
                  this.d.b(2001, var2, Block.a((Block)Blocks.c));
                  this.d.a((BlockPosition)var2, (IBlockData)Blocks.d.u(), 2);
               }

               this.c.B();
            }
         }

      }
   }
}
