package net.minecraft.server;

import com.google.common.collect.Multimap;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.Block;
import net.minecraft.server.BlockDirt;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_ys;

public class class_adm extends Item {
   private final float b;
   protected Item.class_a_in_class_adn a;

   public class_adm(Item.class_a_in_class_adn var1) {
      this.a = var1;
      this.j = 1;
      this.e(var1.a());
      this.a(CreativeModeTab.i);
      this.b = var1.c() + 1.0F;
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      if(!var2.a(var4.a(var6), var6, var1)) {
         return EnumResult.FAIL;
      } else {
         IBlockData var10 = var3.getType(var4);
         Block var11 = var10.getBlock();
         if(var6 != EnumDirection.DOWN && var3.getType(var4.a()).getMaterial() == Material.a) {
            if(var11 == Blocks.c || var11 == Blocks.da) {
               this.a(var1, var2, var3, var4, Blocks.ak.u());
               return EnumResult.SUCCESS;
            }

            if(var11 == Blocks.d) {
               switch(class_adm.SyntheticClass_1.a[((BlockDirt.EnumDirtVariant)var10.get(BlockDirt.a)).ordinal()]) {
               case 1:
                  this.a(var1, var2, var3, var4, Blocks.ak.u());
                  return EnumResult.SUCCESS;
               case 2:
                  this.a(var1, var2, var3, var4, Blocks.d.u().set(BlockDirt.a, BlockDirt.EnumDirtVariant.DIRT));
                  return EnumResult.SUCCESS;
               }
            }
         }

         return EnumResult.PASS;
      }
   }

   public boolean a(ItemStack var1, class_rz var2, class_rz var3) {
      var1.a(1, (class_rz)var3);
      return true;
   }

   protected void a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, IBlockData var5) {
      var3.a(var2, var4, class_ng.cj, EnumSoundCategory.BLOCKS, 1.0F, 1.0F);
      if(!var3.E) {
         var3.a((BlockPosition)var4, (IBlockData)var5, 11);
         var1.a(1, (class_rz)var2);
      }

   }

   public String g() {
      return this.a.toString();
   }

   public Multimap a(EnumInventorySlot var1) {
      Multimap var2 = super.a(var1);
      if(var1 == EnumInventorySlot.MAINHAND) {
         var2.put(class_ys.e.a(), new AttributeModifier(g, "Weapon modifier", 0.0D, 0));
         var2.put(class_ys.f.a(), new AttributeModifier(h, "Weapon modifier", (double)(this.b - 4.0F), 0));
      }

      return var2;
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[BlockDirt.EnumDirtVariant.values().length];

      static {
         try {
            a[BlockDirt.EnumDirtVariant.DIRT.ordinal()] = 1;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[BlockDirt.EnumDirtVariant.COARSE_DIRT.ordinal()] = 2;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
