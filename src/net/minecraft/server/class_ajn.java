package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ali;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.class_axg;
import net.minecraft.server.BlockPosition;

public abstract class class_ajn extends Block {
   protected static final AxisAlignedBB a = new AxisAlignedBB(0.0625D, 0.0D, 0.0625D, 0.9375D, 0.03125D, 0.9375D);
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.0625D, 0.0D, 0.0625D, 0.9375D, 0.0625D, 0.9375D);
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.125D, 0.0D, 0.125D, 0.875D, 0.25D, 0.875D);

   protected class_ajn(Material var1) {
      this(var1, var1.r());
   }

   protected class_ajn(Material var1, MaterialMapColor var2) {
      super(var1, var2);
      this.a(CreativeModeTab.d);
      this.a(true);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      boolean var4 = this.i(var1) > 0;
      return var4?a:b;
   }

   public int a(World var1) {
      return 20;
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(class_ahw var1, BlockPosition var2) {
      return true;
   }

   public boolean d() {
      return true;
   }

   public boolean a(World var1, BlockPosition var2) {
      return this.i(var1, var2.b());
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!this.i(var1, var2.b())) {
         this.b(var1, var2, var3, 0);
         var1.g(var2);
      }

   }

   private boolean i(World var1, BlockPosition var2) {
      return var1.getType(var2).q() || var1.getType(var2).getBlock() instanceof class_ali;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Random var4) {
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(!var1.E) {
         int var5 = this.i(var3);
         if(var5 > 0) {
            this.a(var1, var2, var3, var5);
         }

      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Entity var4) {
      if(!var1.E) {
         int var5 = this.i(var3);
         if(var5 == 0) {
            this.a(var1, var2, var3, var5);
         }

      }
   }

   protected void a(World var1, BlockPosition var2, IBlockData var3, int var4) {
      int var5 = this.e(var1, var2);
      boolean var6 = var4 > 0;
      boolean var7 = var5 > 0;
      if(var4 != var5) {
         var3 = this.a(var3, var5);
         var1.a((BlockPosition)var2, (IBlockData)var3, 2);
         this.d(var1, var2);
         var1.b(var2, var2);
      }

      if(!var7 && var6) {
         this.c(var1, var2);
      } else if(var7 && !var6) {
         this.b(var1, var2);
      }

      if(var7) {
         var1.a((BlockPosition)(new BlockPosition(var2)), (Block)this, this.a(var1));
      }

   }

   protected abstract void b(World var1, BlockPosition var2);

   protected abstract void c(World var1, BlockPosition var2);

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      if(this.i(var3) > 0) {
         this.d(var1, var2);
      }

      super.b(var1, var2, var3);
   }

   protected void d(World var1, BlockPosition var2) {
      var1.d(var2, this);
      var1.d(var2.b(), this);
   }

   public int b(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return this.i(var1);
   }

   public int c(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return var4 == EnumDirection.UP?this.i(var1):0;
   }

   public boolean g(IBlockData var1) {
      return true;
   }

   public class_axg h(IBlockData var1) {
      return class_axg.DESTROY;
   }

   protected abstract int e(World var1, BlockPosition var2);

   protected abstract int i(IBlockData var1);

   protected abstract IBlockData a(IBlockData var1, int var2);
}
