package net.minecraft.server;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.World;
import net.minecraft.server.class_ajy;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;

public class class_apo extends class_ajy {
   protected class_apo() {
      super(true);
   }

   protected void a(EntityHuman var1, World var2, BlockPosition var3) {
      var2.a(var1, var3, class_ng.gS, EnumSoundCategory.BLOCKS, 0.3F, 0.6F);
   }

   protected void b(World var1, BlockPosition var2) {
      var1.a((EntityHuman)null, var2, class_ng.gR, EnumSoundCategory.BLOCKS, 0.3F, 0.5F);
   }
}
