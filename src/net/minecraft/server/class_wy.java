package net.minecraft.server;

import net.minecraft.server.EnumParticle;
import net.minecraft.server.Vec3D;
import net.minecraft.server.class_aub;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_wt;
import net.minecraft.server.class_wv;
import net.minecraft.server.class_xj;

public class class_wy extends class_wv {
   private Vec3D b;
   private int c;

   public class_wy(class_wt var1) {
      super(var1);
   }

   public void b() {
      if(this.c++ % 10 == 0) {
         float var1 = (this.a.bE().nextFloat() - 0.5F) * 8.0F;
         float var2 = (this.a.bE().nextFloat() - 0.5F) * 4.0F;
         float var3 = (this.a.bE().nextFloat() - 0.5F) * 8.0F;
         this.a.world.a(EnumParticle.EXPLOSION_HUGE, this.a.locX + (double)var1, this.a.locY + 2.0D + (double)var2, this.a.locZ + (double)var3, 0.0D, 0.0D, 0.0D, new int[0]);
      }

   }

   public void c() {
      ++this.c;
      if(this.b == null) {
         BlockPosition var1 = this.a.world.l(class_aub.a);
         this.b = new Vec3D((double)var1.p(), (double)var1.q(), (double)var1.r());
      }

      double var3 = this.b.c(this.a.locX, this.a.locY, this.a.locZ);
      if(var3 >= 100.0D && var3 <= 22500.0D && !this.a.positionChanged && !this.a.B) {
         this.a.c(1.0F);
      } else {
         this.a.c(0.0F);
      }

   }

   public void d() {
      this.b = null;
      this.c = 0;
   }

   public float f() {
      return 3.0F;
   }

   public Vec3D g() {
      return this.b;
   }

   public class_xj i() {
      return class_xj.j;
   }
}
