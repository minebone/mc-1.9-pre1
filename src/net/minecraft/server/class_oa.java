package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterators;
import java.util.Iterator;
import net.minecraft.server.class_cs;
import net.minecraft.server.MathHelper;

public class class_oa implements class_cs, Iterable {
   private static final Object a = null;
   private Object[] b;
   private int[] c;
   private Object[] d;
   private int e;
   private int f;

   public class_oa(int var1) {
      var1 = (int)((float)var1 / 0.8F);
      this.b = (Object[])(new Object[var1]);
      this.c = new int[var1];
      this.d = (Object[])(new Object[var1]);
   }

   public int a(Object var1) {
      return this.c(this.b(var1, this.d(var1)));
   }

   public Object a(int var1) {
      return var1 >= 0 && var1 < this.d.length?this.d[var1]:null;
   }

   private int c(int var1) {
      return var1 == -1?-1:this.c[var1];
   }

   public int c(Object var1) {
      int var2 = this.c();
      this.a(var1, var2);
      return var2;
   }

   private int c() {
      while(this.e < this.d.length && this.d[this.e] != null) {
         ++this.e;
      }

      return this.e++;
   }

   private void d(int var1) {
      Object[] var2 = this.b;
      int[] var3 = this.c;
      this.b = (Object[])(new Object[var1]);
      this.c = new int[var1];
      this.d = (Object[])(new Object[var1]);
      this.e = 0;
      this.f = 0;

      for(int var4 = 0; var4 < var2.length; ++var4) {
         if(var2[var4] != null) {
            this.a(var2[var4], var3[var4]);
         }
      }

   }

   public void a(Object var1, int var2) {
      int var3 = Math.max(var2, ++this.f);
      int var4;
      if((float)var3 >= (float)this.b.length * 0.8F) {
         for(var4 = this.b.length << 1; var4 < var2; var4 <<= 1) {
            ;
         }

         this.d(var4);
      }

      var4 = this.e(this.d(var1));
      this.b[var4] = var1;
      this.c[var4] = var2;
      this.d[var2] = var1;
   }

   private int d(Object var1) {
      return (MathHelper.f(System.identityHashCode(var1)) & Integer.MAX_VALUE) % this.b.length;
   }

   private int b(Object var1, int var2) {
      int var3;
      for(var3 = var2; var3 < this.b.length; ++var3) {
         if(this.b[var3] == var1) {
            return var3;
         }

         if(this.b[var3] == a) {
            return -1;
         }
      }

      for(var3 = 0; var3 < var2; ++var3) {
         if(this.b[var3] == var1) {
            return var3;
         }

         if(this.b[var3] == a) {
            return -1;
         }
      }

      return -1;
   }

   private int e(int var1) {
      StringBuilder var2 = new StringBuilder("");

      int var3;
      for(var3 = var1; var3 < this.b.length; ++var3) {
         if(this.b[var3] == a) {
            return var3;
         }

         var2.append(var3).append(' ');
      }

      for(var3 = 0; var3 < var1; ++var3) {
         if(this.b[var3] == a) {
            return var3;
         }

         var2.append(var3).append(' ');
      }

      throw new RuntimeException("Overflowed :(");
   }

   public Iterator iterator() {
      return Iterators.filter(Iterators.forArray(this.d), (Predicate)Predicates.notNull());
   }

   public int b() {
      return this.f + 1;
   }
}
