package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import java.util.Random;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_bab;
import net.minecraft.server.class_bae;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_kk;
import net.minecraft.server.MathHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_baj extends class_bae {
   private static final Logger a = LogManager.getLogger();
   private final class_bab b;

   public class_baj(class_baq[] var1, class_bab var2) {
      super(var1);
      this.b = var2;
   }

   public ItemStack a(ItemStack var1, Random var2, class_azy var3) {
      if(var1.e()) {
         float var4 = 1.0F - this.b.b(var2);
         var1.b(MathHelper.d(var4 * (float)var1.j()));
      } else {
         a.warn("Couldn\'t set damage of loot item " + var1);
      }

      return var1;
   }

   public static class class_a_in_class_baj extends class_bae.class_a_in_class_bae {
      protected class_a_in_class_baj() {
         super(new class_kk("set_damage"), class_baj.class);
      }

      public void a(JsonObject var1, class_baj var2, JsonSerializationContext var3) {
         var1.add("damage", var3.serialize(var2.b));
      }

      public class_baj a(JsonObject var1, JsonDeserializationContext var2, class_baq[] var3) {
         return new class_baj(var3, (class_bab)ChatDeserializer.a(var1, "damage", var2, class_bab.class));
      }

      // $FF: synthetic method
      public class_bae b(JsonObject var1, JsonDeserializationContext var2, class_baq[] var3) {
         return this.a(var1, var2, var3);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(JsonObject var1, class_bae var2, JsonSerializationContext var3) {
         this.a(var1, (class_baj)var2, var3);
      }
   }
}
