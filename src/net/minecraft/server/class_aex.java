package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.class_acb;
import net.minecraft.server.ItemStack;

public class class_aex extends class_acb {
   private String[] b;

   public class_aex(Block var1, boolean var2) {
      super(var1);
      if(var2) {
         this.e(0);
         this.a(true);
      }

   }

   public int a(int var1) {
      return var1;
   }

   public class_aex a(String[] var1) {
      this.b = var1;
      return this;
   }

   public String f_(ItemStack var1) {
      if(this.b == null) {
         return super.f_(var1);
      } else {
         int var2 = var1.i();
         return var2 >= 0 && var2 < this.b.length?super.f_(var1) + "." + this.b[var2]:super.f_(var1);
      }
   }
}
