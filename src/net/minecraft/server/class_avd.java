package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockVine;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.class_ane;
import net.minecraft.server.class_anf;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.class_ato;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_avd extends class_ato {
   private static final IBlockData a = Blocks.r.u().set(class_anf.b, BlockWood.EnumLogVariant.OAK);
   private static final IBlockData b = Blocks.t.u().set(class_ane.e, BlockWood.EnumLogVariant.OAK).set(class_ane.b, Boolean.valueOf(false));

   public class_avd() {
      super(false);
   }

   public boolean b(World var1, Random var2, BlockPosition var3) {
      int var4;
      for(var4 = var2.nextInt(4) + 5; var1.getType(var3.b()).getMaterial() == Material.h; var3 = var3.b()) {
         ;
      }

      boolean var5 = true;
      if(var3.q() >= 1 && var3.q() + var4 + 1 <= 256) {
         int var9;
         int var10;
         for(int var6 = var3.q(); var6 <= var3.q() + 1 + var4; ++var6) {
            byte var7 = 1;
            if(var6 == var3.q()) {
               var7 = 0;
            }

            if(var6 >= var3.q() + 1 + var4 - 2) {
               var7 = 3;
            }

            BlockPosition.class_a_in_class_cj var8 = new BlockPosition.class_a_in_class_cj();

            for(var9 = var3.p() - var7; var9 <= var3.p() + var7 && var5; ++var9) {
               for(var10 = var3.r() - var7; var10 <= var3.r() + var7 && var5; ++var10) {
                  if(var6 >= 0 && var6 < 256) {
                     IBlockData var11 = var1.getType(var8.c(var9, var6, var10));
                     Block var12 = var11.getBlock();
                     if(var11.getMaterial() != Material.a && var11.getMaterial() != Material.j) {
                        if(var12 != Blocks.j && var12 != Blocks.i) {
                           var5 = false;
                        } else if(var6 > var3.q()) {
                           var5 = false;
                        }
                     }
                  } else {
                     var5 = false;
                  }
               }
            }
         }

         if(!var5) {
            return false;
         } else {
            Block var17 = var1.getType(var3.b()).getBlock();
            if((var17 == Blocks.c || var17 == Blocks.d) && var3.q() < 256 - var4 - 1) {
               this.a(var1, var3.b());

               BlockPosition var14;
               int var18;
               int var19;
               int var22;
               int var24;
               for(var18 = var3.q() - 3 + var4; var18 <= var3.q() + var4; ++var18) {
                  var19 = var18 - (var3.q() + var4);
                  var9 = 2 - var19 / 2;

                  for(var10 = var3.p() - var9; var10 <= var3.p() + var9; ++var10) {
                     var22 = var10 - var3.p();

                     for(var24 = var3.r() - var9; var24 <= var3.r() + var9; ++var24) {
                        int var13 = var24 - var3.r();
                        if(Math.abs(var22) != var9 || Math.abs(var13) != var9 || var2.nextInt(2) != 0 && var19 != 0) {
                           var14 = new BlockPosition(var10, var18, var24);
                           if(!var1.getType(var14).b()) {
                              this.a(var1, var14, b);
                           }
                        }
                     }
                  }
               }

               for(var18 = 0; var18 < var4; ++var18) {
                  IBlockData var20 = var1.getType(var3.b(var18));
                  Block var21 = var20.getBlock();
                  if(var20.getMaterial() == Material.a || var20.getMaterial() == Material.j || var21 == Blocks.i || var21 == Blocks.j) {
                     this.a(var1, var3.b(var18), a);
                  }
               }

               for(var18 = var3.q() - 3 + var4; var18 <= var3.q() + var4; ++var18) {
                  var19 = var18 - (var3.q() + var4);
                  var9 = 2 - var19 / 2;
                  BlockPosition.class_a_in_class_cj var23 = new BlockPosition.class_a_in_class_cj();

                  for(var22 = var3.p() - var9; var22 <= var3.p() + var9; ++var22) {
                     for(var24 = var3.r() - var9; var24 <= var3.r() + var9; ++var24) {
                        var23.c(var22, var18, var24);
                        if(var1.getType(var23).getMaterial() == Material.j) {
                           BlockPosition var25 = var23.e();
                           var14 = var23.f();
                           BlockPosition var15 = var23.c();
                           BlockPosition var16 = var23.d();
                           if(var2.nextInt(4) == 0 && var1.getType(var25).getMaterial() == Material.a) {
                              this.a(var1, var25, BlockVine.c);
                           }

                           if(var2.nextInt(4) == 0 && var1.getType(var14).getMaterial() == Material.a) {
                              this.a(var1, var14, BlockVine.e);
                           }

                           if(var2.nextInt(4) == 0 && var1.getType(var15).getMaterial() == Material.a) {
                              this.a(var1, var15, BlockVine.d);
                           }

                           if(var2.nextInt(4) == 0 && var1.getType(var16).getMaterial() == Material.a) {
                              this.a(var1, var16, BlockVine.b);
                           }
                        }
                     }
                  }
               }

               return true;
            } else {
               return false;
            }
         }
      } else {
         return false;
      }
   }

   private void a(World var1, BlockPosition var2, class_arm var3) {
      IBlockData var4 = Blocks.bn.u().set(var3, Boolean.valueOf(true));
      this.a(var1, var2, var4);
      int var5 = 4;

      for(var2 = var2.b(); var1.getType(var2).getMaterial() == Material.a && var5 > 0; --var5) {
         this.a(var1, var2, var4);
         var2 = var2.b();
      }

   }
}
