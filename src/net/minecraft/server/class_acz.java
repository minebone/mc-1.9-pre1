package net.minecraft.server;

import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_asz;
import net.minecraft.server.class_ata;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_ws;

public class class_acz extends Item {
   public class_acz() {
      this.c("end_crystal");
      this.a(CreativeModeTab.c);
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      IBlockData var10 = var3.getType(var4);
      if(var10.getBlock() != Blocks.Z && var10.getBlock() != Blocks.h) {
         return EnumResult.FAIL;
      } else {
         BlockPosition var11 = var4.a();
         if(!var2.a(var11, var6, var1)) {
            return EnumResult.FAIL;
         } else {
            BlockPosition var12 = var11.a();
            boolean var13 = !var3.d(var11) && !var3.getType(var11).getBlock().a((class_ahw)var3, (BlockPosition)var11);
            var13 |= !var3.d(var12) && !var3.getType(var12).getBlock().a((class_ahw)var3, (BlockPosition)var12);
            if(var13) {
               return EnumResult.FAIL;
            } else {
               double var14 = (double)var11.p();
               double var16 = (double)var11.q();
               double var18 = (double)var11.r();
               List var20 = var3.b((Entity)null, (AxisAlignedBB)(new AxisAlignedBB(var14, var16, var18, var14 + 1.0D, var16 + 2.0D, var18 + 1.0D)));
               if(!var20.isEmpty()) {
                  return EnumResult.FAIL;
               } else {
                  if(!var3.E) {
                     class_ws var21 = new class_ws(var3, (double)((float)var4.p() + 0.5F), (double)(var4.q() + 1), (double)((float)var4.r() + 0.5F));
                     var21.a(false);
                     var3.a((Entity)var21);
                     if(var3.s instanceof class_ata) {
                        class_asz var22 = ((class_ata)var3.s).s();
                        var22.e();
                     }
                  }

                  --var1.b;
                  return EnumResult.SUCCESS;
               }
            }
         }
      }
   }
}
