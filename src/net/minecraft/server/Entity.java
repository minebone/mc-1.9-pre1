package net.minecraft.server;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Callable;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockCobbleWall;
import net.minecraft.server.BlockFenceGate;
import net.minecraft.server.Blocks;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatHoverable;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.CrashReport;
import net.minecraft.server.CrashReportSystemDetails;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityTypes;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.Item;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagDouble;
import net.minecraft.server.NBTTagFloat;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.NBTTagString;
import net.minecraft.server.ScoreboardTeam;
import net.minecraft.server.ScoreboardTeamBase;
import net.minecraft.server.ShapeDetector;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.EntityBoat;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agn;
import net.minecraft.server.class_agx;
import net.minecraft.server.class_aho;
import net.minecraft.server.class_aia;
import net.minecraft.server.class_ali;
import net.minecraft.server.class_amn;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aoa;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_aoo;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.class_axg;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;
import net.minecraft.server.class_e;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_xz;
import net.minecraft.server.class_yc;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class Entity implements ICommandListener {
   private static final Logger a = LogManager.getLogger();
   private static final AxisAlignedBB b = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
   private static double c = 1.0D;
   private static int f;
   private int id;
   public boolean i;
   private final List h;
   protected int j;
   private Entity as;
   public boolean attachedToPlayer;
   public World world;
   public double lastX;
   public double lastY;
   public double lastZ;
   public double locX;
   public double locY;
   public double locZ;
   public double motX;
   public double motY;
   public double motZ;
   public float yaw;
   public float pitch;
   public float lastYaw;
   public float lastPitch;
   private AxisAlignedBB boundingBox;
   public boolean onGround;
   public boolean positionChanged;
   public boolean B;
   public boolean C;
   public boolean velocityChanged;
   protected boolean E;
   private boolean au;
   public boolean dead;
   public float width;
   public float length;
   public float I;
   public float J;
   public float K;
   public float fallDistance;
   private int av;
   public double M;
   public double N;
   public double O;
   public float P;
   public boolean noClip;
   public float R;
   protected Random random;
   public int ticksLived;
   public int maxFireTicks;
   private int fireTicks;
   protected boolean inWater;
   public int noDamageTicks;
   protected boolean justCreated;
   protected boolean fireProof;
   protected DataWatcher datawatcher;
   private static final class_ke ax = DataWatcher.a(Entity.class, class_kg.a);
   private static final class_ke ay = DataWatcher.a(Entity.class, class_kg.b);
   private static final class_ke az = DataWatcher.a(Entity.class, class_kg.d);
   private static final class_ke aA = DataWatcher.a(Entity.class, class_kg.h);
   private static final class_ke aB = DataWatcher.a(Entity.class, class_kg.h);
   public boolean aa;
   public int ab;
   public int ac;
   public int ad;
   public boolean ah;
   public boolean ai;
   public int portalCooldown;
   protected boolean ak;
   protected int al;
   public int dimension;
   protected BlockPosition an;
   protected Vec3D ao;
   protected EnumDirection ap;
   private boolean invlunerable;
   protected UUID uniqueID;
   private final CommandObjectiveExecutor aD;
   private final List aE;
   protected boolean ar;
   private final Set aF;
   private boolean aG;

   public Entity(World var1) {
      this.id = f++;
      this.h = Lists.newArrayList();
      this.boundingBox = b;
      this.width = 0.6F;
      this.length = 1.8F;
      this.av = 1;
      this.random = new Random();
      this.maxFireTicks = 1;
      this.justCreated = true;
      this.uniqueID = MathHelper.a(this.random);
      this.aD = new CommandObjectiveExecutor();
      this.aE = Lists.newArrayList();
      this.aF = Sets.newHashSet();
      this.world = var1;
      this.b(0.0D, 0.0D, 0.0D);
      if(var1 != null) {
         this.dimension = var1.s.p().a();
      }

      this.datawatcher = new DataWatcher(this);
      this.datawatcher.a((class_ke)ax, (Object)Byte.valueOf((byte)0));
      this.datawatcher.a((class_ke)ay, (Object)Integer.valueOf(300));
      this.datawatcher.a((class_ke)aA, (Object)Boolean.valueOf(false));
      this.datawatcher.a((class_ke)az, (Object)"");
      this.datawatcher.a((class_ke)aB, (Object)Boolean.valueOf(false));
      this.i();
   }

   public int getId() {
      return this.id;
   }

   public void f(int var1) {
      this.id = var1;
   }

   public Set O() {
      return this.aF;
   }

   public boolean a(String var1) {
      if(this.aF.size() >= 1024) {
         return false;
      } else {
         this.aF.add(var1);
         return true;
      }
   }

   public boolean b(String var1) {
      return this.aF.remove(var1);
   }

   public void P() {
      this.S();
   }

   protected abstract void i();

   public DataWatcher Q() {
      return this.datawatcher;
   }

   public boolean equals(Object var1) {
      return var1 instanceof Entity?((Entity)var1).id == this.id:false;
   }

   public int hashCode() {
      return this.id;
   }

   public void S() {
      this.dead = true;
   }

   public void b(boolean var1) {
   }

   protected void a(float var1, float var2) {
      if(var1 != this.width || var2 != this.length) {
         float var3 = this.width;
         this.width = var1;
         this.length = var2;
         AxisAlignedBB var4 = this.bk();
         this.a(new AxisAlignedBB(var4.a, var4.b, var4.c, var4.a + (double)this.width, var4.b + (double)this.length, var4.c + (double)this.width));
         if(this.width > var3 && !this.justCreated && !this.world.E) {
            this.d((double)(var3 - this.width), 0.0D, (double)(var3 - this.width));
         }
      }

   }

   protected void b(float var1, float var2) {
      this.yaw = var1 % 360.0F;
      this.pitch = var2 % 360.0F;
   }

   public void b(double var1, double var3, double var5) {
      this.locX = var1;
      this.locY = var3;
      this.locZ = var5;
      float var7 = this.width / 2.0F;
      float var8 = this.length;
      this.a(new AxisAlignedBB(var1 - (double)var7, var3, var5 - (double)var7, var1 + (double)var7, var3 + (double)var8, var5 + (double)var7));
   }

   public void m() {
      if(!this.world.E) {
         this.b(6, this.aL());
      }

      this.T();
   }

   public void T() {
      this.world.C.a("entityBaseTick");
      if(this.aH() && this.bx().dead) {
         this.p();
      }

      if(this.j > 0) {
         --this.j;
      }

      this.I = this.J;
      this.lastX = this.locX;
      this.lastY = this.locY;
      this.lastZ = this.locZ;
      this.lastPitch = this.pitch;
      this.lastYaw = this.yaw;
      if(!this.world.E && this.world instanceof WorldServer) {
         this.world.C.a("portal");
         if(this.ak) {
            MinecraftServer var1 = this.world.u();
            if(var1.E()) {
               if(!this.aH()) {
                  int var2 = this.U();
                  if(this.al++ >= var2) {
                     this.al = var2;
                     this.portalCooldown = this.aB();
                     byte var3;
                     if(this.world.s.p().a() == -1) {
                        var3 = 0;
                     } else {
                        var3 = -1;
                     }

                     this.c(var3);
                  }
               }

               this.ak = false;
            }
         } else {
            if(this.al > 0) {
               this.al -= 4;
            }

            if(this.al < 0) {
               this.al = 0;
            }
         }

         this.H();
         this.world.C.b();
      }

      this.ak();
      this.ai();
      if(this.world.E) {
         this.fireTicks = 0;
      } else if(this.fireTicks > 0) {
         if(this.fireProof) {
            this.fireTicks -= 4;
            if(this.fireTicks < 0) {
               this.fireTicks = 0;
            }
         } else {
            if(this.fireTicks % 20 == 0) {
               this.a(DamageSource.c, 1.0F);
            }

            --this.fireTicks;
         }
      }

      if(this.am()) {
         this.V();
         this.fallDistance *= 0.5F;
      }

      if(this.locY < -64.0D) {
         this.X();
      }

      if(!this.world.E) {
         this.b(0, this.fireTicks > 0);
      }

      this.justCreated = false;
      this.world.C.b();
   }

   protected void H() {
      if(this.portalCooldown > 0) {
         --this.portalCooldown;
      }

   }

   public int U() {
      return 1;
   }

   protected void V() {
      if(!this.fireProof) {
         this.a(DamageSource.d, 4.0F);
         this.g(15);
      }
   }

   public void g(int var1) {
      int var2 = var1 * 20;
      if(this instanceof class_rz) {
         var2 = class_agx.a((class_rz)this, var2);
      }

      if(this.fireTicks < var2) {
         this.fireTicks = var2;
      }

   }

   public void W() {
      this.fireTicks = 0;
   }

   protected void X() {
      this.S();
   }

   public boolean c(double var1, double var3, double var5) {
      AxisAlignedBB var7 = this.bk().c(var1, var3, var5);
      return this.b(var7);
   }

   private boolean b(AxisAlignedBB var1) {
      return this.world.a(this, var1).isEmpty() && !this.world.e(var1);
   }

   public void d(double var1, double var3, double var5) {
      if(this.noClip) {
         this.a(this.bk().c(var1, var3, var5));
         this.Y();
      } else {
         this.world.C.a("move");
         double var7 = this.locX;
         double var9 = this.locY;
         double var11 = this.locZ;
         if(this.E) {
            this.E = false;
            var1 *= 0.25D;
            var3 *= 0.05000000074505806D;
            var5 *= 0.25D;
            this.motX = 0.0D;
            this.motY = 0.0D;
            this.motZ = 0.0D;
         }

         double var13 = var1;
         double var15 = var3;
         double var17 = var5;
         boolean var19 = this.onGround && this.aJ() && this instanceof EntityHuman;
         if(var19) {
            double var20;
            for(var20 = 0.05D; var1 != 0.0D && this.world.a(this, this.bk().c(var1, -1.0D, 0.0D)).isEmpty(); var13 = var1) {
               if(var1 < var20 && var1 >= -var20) {
                  var1 = 0.0D;
               } else if(var1 > 0.0D) {
                  var1 -= var20;
               } else {
                  var1 += var20;
               }
            }

            for(; var5 != 0.0D && this.world.a(this, this.bk().c(0.0D, -1.0D, var5)).isEmpty(); var17 = var5) {
               if(var5 < var20 && var5 >= -var20) {
                  var5 = 0.0D;
               } else if(var5 > 0.0D) {
                  var5 -= var20;
               } else {
                  var5 += var20;
               }
            }

            for(; var1 != 0.0D && var5 != 0.0D && this.world.a(this, this.bk().c(var1, -1.0D, var5)).isEmpty(); var17 = var5) {
               if(var1 < var20 && var1 >= -var20) {
                  var1 = 0.0D;
               } else if(var1 > 0.0D) {
                  var1 -= var20;
               } else {
                  var1 += var20;
               }

               var13 = var1;
               if(var5 < var20 && var5 >= -var20) {
                  var5 = 0.0D;
               } else if(var5 > 0.0D) {
                  var5 -= var20;
               } else {
                  var5 += var20;
               }
            }
         }

         List var53 = this.world.a(this, this.bk().a(var1, var3, var5));
         AxisAlignedBB var21 = this.bk();
         int var22 = 0;

         int var23;
         for(var23 = var53.size(); var22 < var23; ++var22) {
            var3 = ((AxisAlignedBB)var53.get(var22)).b(this.bk(), var3);
         }

         this.a(this.bk().c(0.0D, var3, 0.0D));
         boolean var54 = this.onGround || var15 != var3 && var15 < 0.0D;
         var23 = 0;

         int var24;
         for(var24 = var53.size(); var23 < var24; ++var23) {
            var1 = ((AxisAlignedBB)var53.get(var23)).a(this.bk(), var1);
         }

         this.a(this.bk().c(var1, 0.0D, 0.0D));
         var23 = 0;

         for(var24 = var53.size(); var23 < var24; ++var23) {
            var5 = ((AxisAlignedBB)var53.get(var23)).c(this.bk(), var5);
         }

         this.a(this.bk().c(0.0D, 0.0D, var5));
         double var33;
         if(this.P > 0.0F && var54 && (var13 != var1 || var17 != var5)) {
            double var55 = var1;
            double var25 = var3;
            double var27 = var5;
            AxisAlignedBB var29 = this.bk();
            this.a(var21);
            var3 = (double)this.P;
            List var30 = this.world.a(this, this.bk().a(var13, var3, var17));
            AxisAlignedBB var31 = this.bk();
            AxisAlignedBB var32 = var31.a(var13, 0.0D, var17);
            var33 = var3;
            int var35 = 0;

            for(int var36 = var30.size(); var35 < var36; ++var35) {
               var33 = ((AxisAlignedBB)var30.get(var35)).b(var32, var33);
            }

            var31 = var31.c(0.0D, var33, 0.0D);
            double var66 = var13;
            int var37 = 0;

            for(int var38 = var30.size(); var37 < var38; ++var37) {
               var66 = ((AxisAlignedBB)var30.get(var37)).a(var31, var66);
            }

            var31 = var31.c(var66, 0.0D, 0.0D);
            double var68 = var17;
            int var39 = 0;

            for(int var40 = var30.size(); var39 < var40; ++var39) {
               var68 = ((AxisAlignedBB)var30.get(var39)).c(var31, var68);
            }

            var31 = var31.c(0.0D, 0.0D, var68);
            AxisAlignedBB var69 = this.bk();
            double var70 = var3;
            int var42 = 0;

            for(int var43 = var30.size(); var42 < var43; ++var42) {
               var70 = ((AxisAlignedBB)var30.get(var42)).b(var69, var70);
            }

            var69 = var69.c(0.0D, var70, 0.0D);
            double var71 = var13;
            int var44 = 0;

            for(int var45 = var30.size(); var44 < var45; ++var44) {
               var71 = ((AxisAlignedBB)var30.get(var44)).a(var69, var71);
            }

            var69 = var69.c(var71, 0.0D, 0.0D);
            double var72 = var17;
            int var46 = 0;

            for(int var47 = var30.size(); var46 < var47; ++var46) {
               var72 = ((AxisAlignedBB)var30.get(var46)).c(var69, var72);
            }

            var69 = var69.c(0.0D, 0.0D, var72);
            double var73 = var66 * var66 + var68 * var68;
            double var48 = var71 * var71 + var72 * var72;
            if(var73 > var48) {
               var1 = var66;
               var5 = var68;
               var3 = -var33;
               this.a(var31);
            } else {
               var1 = var71;
               var5 = var72;
               var3 = -var70;
               this.a(var69);
            }

            int var50 = 0;

            for(int var51 = var30.size(); var50 < var51; ++var50) {
               var3 = ((AxisAlignedBB)var30.get(var50)).b(this.bk(), var3);
            }

            this.a(this.bk().c(0.0D, var3, 0.0D));
            if(var55 * var55 + var27 * var27 >= var1 * var1 + var5 * var5) {
               var1 = var55;
               var3 = var25;
               var5 = var27;
               this.a(var29);
            }
         }

         this.world.C.b();
         this.world.C.a("rest");
         this.Y();
         this.positionChanged = var13 != var1 || var17 != var5;
         this.B = var15 != var3;
         this.onGround = this.B && var15 < 0.0D;
         this.C = this.positionChanged || this.B;
         var23 = MathHelper.c(this.locX);
         var24 = MathHelper.c(this.locY - 0.20000000298023224D);
         int var56 = MathHelper.c(this.locZ);
         BlockPosition var26 = new BlockPosition(var23, var24, var56);
         IBlockData var57 = this.world.getType(var26);
         if(var57.getMaterial() == Material.a) {
            BlockPosition var28 = var26.b();
            IBlockData var59 = this.world.getType(var28);
            Block var62 = var59.getBlock();
            if(var62 instanceof class_ali || var62 instanceof BlockCobbleWall || var62 instanceof BlockFenceGate) {
               var57 = var59;
               var26 = var28;
            }
         }

         this.a(var3, this.onGround, var57, var26);
         if(var13 != var1) {
            this.motX = 0.0D;
         }

         if(var17 != var5) {
            this.motZ = 0.0D;
         }

         Block var58 = var57.getBlock();
         if(var15 != var3) {
            var58.a(this.world, this);
         }

         if(this.ad() && !var19 && !this.aH()) {
            double var60 = this.locX - var7;
            double var64 = this.locY - var9;
            var33 = this.locZ - var11;
            if(var58 != Blocks.au) {
               var64 = 0.0D;
            }

            if(var58 != null && this.onGround) {
               var58.a(this.world, var26, this);
            }

            this.J = (float)((double)this.J + (double)MathHelper.a(var60 * var60 + var33 * var33) * 0.6D);
            this.K = (float)((double)this.K + (double)MathHelper.a(var60 * var60 + var64 * var64 + var33 * var33) * 0.6D);
            if(this.K > (float)this.av && var57.getMaterial() != Material.a) {
               this.av = (int)this.K + 1;
               if(this.ah()) {
                  float var67 = MathHelper.a(this.motX * this.motX * 0.20000000298023224D + this.motY * this.motY + this.motZ * this.motZ * 0.20000000298023224D) * 0.35F;
                  if(var67 > 1.0F) {
                     var67 = 1.0F;
                  }

                  this.a(this.Z(), var67, 1.0F + (this.random.nextFloat() - this.random.nextFloat()) * 0.4F);
               }

               this.a(var26, var58);
            }
         }

         try {
            this.ab();
         } catch (Throwable var52) {
            CrashReport var63 = CrashReport.a(var52, "Checking entity block collision");
            CrashReportSystemDetails var65 = var63.a("Entity being checked for collision");
            this.a(var65);
            throw new class_e(var63);
         }

         boolean var61 = this.ag();
         if(this.world.f(this.bk().h(0.001D))) {
            this.h(1);
            if(!var61) {
               ++this.fireTicks;
               if(this.fireTicks == 0) {
                  this.g(8);
               }
            }
         } else if(this.fireTicks <= 0) {
            this.fireTicks = -this.maxFireTicks;
         }

         if(var61 && this.fireTicks > 0) {
            this.a(class_ng.bC, 0.7F, 1.6F + (this.random.nextFloat() - this.random.nextFloat()) * 0.4F);
            this.fireTicks = -this.maxFireTicks;
         }

         this.world.C.b();
      }
   }

   public void Y() {
      AxisAlignedBB var1 = this.bk();
      this.locX = (var1.a + var1.d) / 2.0D;
      this.locY = var1.b;
      this.locZ = (var1.c + var1.f) / 2.0D;
   }

   protected class_nf Z() {
      return class_ng.bG;
   }

   protected class_nf aa() {
      return class_ng.bF;
   }

   protected void ab() {
      AxisAlignedBB var1 = this.bk();
      BlockPosition.class_b_in_class_cj var2 = BlockPosition.class_b_in_class_cj.c(var1.a + 0.001D, var1.b + 0.001D, var1.c + 0.001D);
      BlockPosition.class_b_in_class_cj var3 = BlockPosition.class_b_in_class_cj.c(var1.d - 0.001D, var1.e - 0.001D, var1.f - 0.001D);
      BlockPosition.class_b_in_class_cj var4 = BlockPosition.class_b_in_class_cj.s();
      if(this.world.a((BlockPosition)var2, (BlockPosition)var3)) {
         for(int var5 = var2.p(); var5 <= var3.p(); ++var5) {
            for(int var6 = var2.q(); var6 <= var3.q(); ++var6) {
               for(int var7 = var2.r(); var7 <= var3.r(); ++var7) {
                  var4.d(var5, var6, var7);
                  IBlockData var8 = this.world.getType(var4);

                  try {
                     var8.getBlock().a((World)this.world, (BlockPosition)var4, (IBlockData)var8, (Entity)this);
                  } catch (Throwable var12) {
                     CrashReport var10 = CrashReport.a(var12, "Colliding entity with block");
                     CrashReportSystemDetails var11 = var10.a("Block being collided with");
                     CrashReportSystemDetails.a(var11, var4, var8);
                     throw new class_e(var10);
                  }
               }
            }
         }
      }

      var2.t();
      var3.t();
      var4.t();
   }

   protected void a(BlockPosition var1, Block var2) {
      class_aoo var3 = var2.w();
      if(this.world.getType(var1.a()).getBlock() == Blocks.aH) {
         var3 = Blocks.aH.w();
         this.a(var3.d(), var3.a() * 0.15F, var3.b());
      } else if(!var2.u().getMaterial().d()) {
         this.a(var3.d(), var3.a() * 0.15F, var3.b());
      }

   }

   public void a(class_nf var1, float var2, float var3) {
      if(!this.ac()) {
         this.world.a((EntityHuman)null, this.locX, this.locY, this.locZ, var1, this.by(), var2, var3);
      }

   }

   public boolean ac() {
      return ((Boolean)this.datawatcher.a(aB)).booleanValue();
   }

   public void c(boolean var1) {
      this.datawatcher.b(aB, Boolean.valueOf(var1));
   }

   protected boolean ad() {
      return true;
   }

   protected void a(double var1, boolean var3, IBlockData var4, BlockPosition var5) {
      if(var3) {
         if(this.fallDistance > 0.0F) {
            var4.getBlock().a(this.world, var5, this, this.fallDistance);
         }

         this.fallDistance = 0.0F;
      } else if(var1 < 0.0D) {
         this.fallDistance = (float)((double)this.fallDistance - var1);
      }

   }

   public AxisAlignedBB ae() {
      return null;
   }

   protected void h(int var1) {
      if(!this.fireProof) {
         this.a(DamageSource.a, (float)var1);
      }

   }

   public final boolean af() {
      return this.fireProof;
   }

   public void e(float var1, float var2) {
      if(this.aI()) {
         Iterator var3 = this.bt().iterator();

         while(var3.hasNext()) {
            Entity var4 = (Entity)var3.next();
            var4.e(var1, var2);
         }
      }

   }

   public boolean ag() {
      if(this.inWater) {
         return true;
      } else {
         BlockPosition.class_b_in_class_cj var1 = BlockPosition.class_b_in_class_cj.c(this.locX, this.locY, this.locZ);
         if(!this.world.B(var1) && !this.world.B(var1.d(this.locX, this.locY + (double)this.length, this.locZ))) {
            var1.t();
            return false;
         } else {
            var1.t();
            return true;
         }
      }
   }

   public boolean ah() {
      return this.inWater;
   }

   public boolean ai() {
      if(this.bx() instanceof EntityBoat) {
         this.inWater = false;
      } else if(this.world.a(this.bk().b(0.0D, -0.4000000059604645D, 0.0D).h(0.001D), Material.h, this)) {
         if(!this.inWater && !this.justCreated) {
            this.aj();
         }

         this.fallDistance = 0.0F;
         this.inWater = true;
         this.fireTicks = 0;
      } else {
         this.inWater = false;
      }

      return this.inWater;
   }

   protected void aj() {
      float var1 = MathHelper.a(this.motX * this.motX * 0.20000000298023224D + this.motY * this.motY + this.motZ * this.motZ * 0.20000000298023224D) * 0.2F;
      if(var1 > 1.0F) {
         var1 = 1.0F;
      }

      this.a(this.aa(), var1, 1.0F + (this.random.nextFloat() - this.random.nextFloat()) * 0.4F);
      float var2 = (float)MathHelper.c(this.bk().b);

      int var3;
      float var4;
      float var5;
      for(var3 = 0; (float)var3 < 1.0F + this.width * 20.0F; ++var3) {
         var4 = (this.random.nextFloat() * 2.0F - 1.0F) * this.width;
         var5 = (this.random.nextFloat() * 2.0F - 1.0F) * this.width;
         this.world.a(EnumParticle.WATER_BUBBLE, this.locX + (double)var4, (double)(var2 + 1.0F), this.locZ + (double)var5, this.motX, this.motY - (double)(this.random.nextFloat() * 0.2F), this.motZ, new int[0]);
      }

      for(var3 = 0; (float)var3 < 1.0F + this.width * 20.0F; ++var3) {
         var4 = (this.random.nextFloat() * 2.0F - 1.0F) * this.width;
         var5 = (this.random.nextFloat() * 2.0F - 1.0F) * this.width;
         this.world.a(EnumParticle.WATER_SPLASH, this.locX + (double)var4, (double)(var2 + 1.0F), this.locZ + (double)var5, this.motX, this.motY, this.motZ, new int[0]);
      }

   }

   public void ak() {
      if(this.aK() && !this.ah()) {
         this.al();
      }

   }

   protected void al() {
      int var1 = MathHelper.c(this.locX);
      int var2 = MathHelper.c(this.locY - 0.20000000298023224D);
      int var3 = MathHelper.c(this.locZ);
      BlockPosition var4 = new BlockPosition(var1, var2, var3);
      IBlockData var5 = this.world.getType(var4);
      if(var5.i() != class_aoa.INVISIBLE) {
         this.world.a(EnumParticle.BLOCK_CRACK, this.locX + ((double)this.random.nextFloat() - 0.5D) * (double)this.width, this.bk().b + 0.1D, this.locZ + ((double)this.random.nextFloat() - 0.5D) * (double)this.width, -this.motX * 4.0D, 1.5D, -this.motZ * 4.0D, new int[]{Block.j(var5)});
      }

   }

   public boolean a(Material var1) {
      if(this.bx() instanceof EntityBoat) {
         return false;
      } else {
         double var2 = this.locY + (double)this.bm();
         BlockPosition var4 = new BlockPosition(this.locX, var2, this.locZ);
         IBlockData var5 = this.world.getType(var4);
         if(var5.getMaterial() == var1) {
            float var6 = class_amn.e(var5.getBlock().e(var5)) - 0.11111111F;
            float var7 = (float)(var4.q() + 1) - var6;
            boolean var8 = var2 < (double)var7;
            return !var8 && this instanceof EntityHuman?false:var8;
         } else {
            return false;
         }
      }
   }

   public boolean am() {
      return this.world.a(this.bk().b(-0.10000000149011612D, -0.4000000059604645D, -0.10000000149011612D), Material.i);
   }

   public void a(float var1, float var2, float var3) {
      float var4 = var1 * var1 + var2 * var2;
      if(var4 >= 1.0E-4F) {
         var4 = MathHelper.c(var4);
         if(var4 < 1.0F) {
            var4 = 1.0F;
         }

         var4 = var3 / var4;
         var1 *= var4;
         var2 *= var4;
         float var5 = MathHelper.a(this.yaw * 0.017453292F);
         float var6 = MathHelper.b(this.yaw * 0.017453292F);
         this.motX += (double)(var1 * var6 - var2 * var5);
         this.motZ += (double)(var2 * var6 + var1 * var5);
      }
   }

   public float e(float var1) {
      BlockPosition.class_a_in_class_cj var2 = new BlockPosition.class_a_in_class_cj(MathHelper.c(this.locX), 0, MathHelper.c(this.locZ));
      if(this.world.e((BlockPosition)var2)) {
         var2.p(MathHelper.c(this.locY + (double)this.bm()));
         return this.world.n(var2);
      } else {
         return 0.0F;
      }
   }

   public void a(World var1) {
      this.world = var1;
   }

   public void move(double var1, double var3, double var5, float var7, float var8) {
      this.lastX = this.locX = MathHelper.a(var1, -3.0E7D, 3.0E7D);
      this.lastY = this.locY = var3;
      this.lastZ = this.locZ = MathHelper.a(var5, -3.0E7D, 3.0E7D);
      var8 = MathHelper.a(var8, -90.0F, 90.0F);
      this.lastYaw = this.yaw = var7;
      this.lastPitch = this.pitch = var8;
      double var9 = (double)(this.lastYaw - var7);
      if(var9 < -180.0D) {
         this.lastYaw += 360.0F;
      }

      if(var9 >= 180.0D) {
         this.lastYaw -= 360.0F;
      }

      this.b(this.locX, this.locY, this.locZ);
      this.b(var7, var8);
   }

   public void a(BlockPosition var1, float var2, float var3) {
      this.b((double)var1.p() + 0.5D, (double)var1.q(), (double)var1.r() + 0.5D, var2, var3);
   }

   public void b(double var1, double var3, double var5, float var7, float var8) {
      this.M = this.lastX = this.locX = var1;
      this.N = this.lastY = this.locY = var3;
      this.O = this.lastZ = this.locZ = var5;
      this.yaw = var7;
      this.pitch = var8;
      this.b(this.locX, this.locY, this.locZ);
   }

   public float g(Entity var1) {
      float var2 = (float)(this.locX - var1.locX);
      float var3 = (float)(this.locY - var1.locY);
      float var4 = (float)(this.locZ - var1.locZ);
      return MathHelper.c(var2 * var2 + var3 * var3 + var4 * var4);
   }

   public double e(double var1, double var3, double var5) {
      double var7 = this.locX - var1;
      double var9 = this.locY - var3;
      double var11 = this.locZ - var5;
      return var7 * var7 + var9 * var9 + var11 * var11;
   }

   public double c(BlockPosition var1) {
      return var1.e(this.locX, this.locY, this.locZ);
   }

   public double d(BlockPosition var1) {
      return var1.f(this.locX, this.locY + 0.5D, this.locZ);
   }

   public double f(double var1, double var3, double var5) {
      double var7 = this.locX - var1;
      double var9 = this.locY - var3;
      double var11 = this.locZ - var5;
      return (double)MathHelper.a(var7 * var7 + var9 * var9 + var11 * var11);
   }

   public double h(Entity var1) {
      double var2 = this.locX - var1.locX;
      double var4 = this.locY - var1.locY;
      double var6 = this.locZ - var1.locZ;
      return var2 * var2 + var4 * var4 + var6 * var6;
   }

   public void d(EntityHuman var1) {
   }

   public void i(Entity var1) {
      if(!this.x(var1)) {
         if(!var1.noClip && !this.noClip) {
            double var2 = var1.locX - this.locX;
            double var4 = var1.locZ - this.locZ;
            double var6 = MathHelper.a(var2, var4);
            if(var6 >= 0.009999999776482582D) {
               var6 = (double)MathHelper.a(var6);
               var2 /= var6;
               var4 /= var6;
               double var8 = 1.0D / var6;
               if(var8 > 1.0D) {
                  var8 = 1.0D;
               }

               var2 *= var8;
               var4 *= var8;
               var2 *= 0.05000000074505806D;
               var4 *= 0.05000000074505806D;
               var2 *= (double)(1.0F - this.R);
               var4 *= (double)(1.0F - this.R);
               if(!this.aI()) {
                  this.g(-var2, 0.0D, -var4);
               }

               if(!var1.aI()) {
                  var1.g(var2, 0.0D, var4);
               }
            }

         }
      }
   }

   public void g(double var1, double var3, double var5) {
      this.motX += var1;
      this.motY += var3;
      this.motZ += var5;
      this.ai = true;
   }

   protected void an() {
      this.velocityChanged = true;
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b(var1)) {
         return false;
      } else {
         this.an();
         return false;
      }
   }

   public Vec3D f(float var1) {
      if(var1 == 1.0F) {
         return this.f(this.pitch, this.yaw);
      } else {
         float var2 = this.lastPitch + (this.pitch - this.lastPitch) * var1;
         float var3 = this.lastYaw + (this.yaw - this.lastYaw) * var1;
         return this.f(var2, var3);
      }
   }

   protected final Vec3D f(float var1, float var2) {
      float var3 = MathHelper.b(-var2 * 0.017453292F - 3.1415927F);
      float var4 = MathHelper.a(-var2 * 0.017453292F - 3.1415927F);
      float var5 = -MathHelper.b(-var1 * 0.017453292F);
      float var6 = MathHelper.a(-var1 * 0.017453292F);
      return new Vec3D((double)(var4 * var5), (double)var6, (double)(var3 * var5));
   }

   public boolean ao() {
      return false;
   }

   public boolean ap() {
      return false;
   }

   public void b(Entity var1, int var2) {
   }

   public boolean c(NBTTagCompound var1) {
      String var2 = this.ar();
      if(!this.dead && var2 != null) {
         var1.a("id", var2);
         this.e(var1);
         return true;
      } else {
         return false;
      }
   }

   public boolean d(NBTTagCompound var1) {
      String var2 = this.ar();
      if(!this.dead && var2 != null && !this.aH()) {
         var1.a("id", var2);
         this.e(var1);
         return true;
      } else {
         return false;
      }
   }

   public void e(NBTTagCompound var1) {
      try {
         var1.a((String)"Pos", (NBTTag)this.a(new double[]{this.locX, this.locY, this.locZ}));
         var1.a((String)"Motion", (NBTTag)this.a(new double[]{this.motX, this.motY, this.motZ}));
         var1.a((String)"Rotation", (NBTTag)this.a(new float[]{this.yaw, this.pitch}));
         var1.a("FallDistance", this.fallDistance);
         var1.a("Fire", (short)this.fireTicks);
         var1.a("Air", (short)this.aO());
         var1.a("OnGround", this.onGround);
         var1.a("Dimension", this.dimension);
         var1.a("Invulnerable", this.invlunerable);
         var1.a("PortalCooldown", this.portalCooldown);
         var1.a("UUID", this.getUniqueId());
         if(this.be() != null && !this.be().isEmpty()) {
            var1.a("CustomName", this.be());
         }

         if(this.bf()) {
            var1.a("CustomNameVisible", this.bf());
         }

         this.aD.b(var1);
         if(this.ac()) {
            var1.a("Silent", this.ac());
         }

         if(this.ar) {
            var1.a("Glowing", this.ar);
         }

         NBTTagList var2;
         Iterator var7;
         if(this.aF.size() > 0) {
            var2 = new NBTTagList();
            var7 = this.aF.iterator();

            while(var7.hasNext()) {
               String var8 = (String)var7.next();
               var2.a((NBTTag)(new NBTTagString(var8)));
            }

            var1.a((String)"Tags", (NBTTag)var2);
         }

         this.b(var1);
         if(this.aI()) {
            var2 = new NBTTagList();
            var7 = this.bt().iterator();

            while(var7.hasNext()) {
               Entity var9 = (Entity)var7.next();
               NBTTagCompound var5 = new NBTTagCompound();
               if(var9.c(var5)) {
                  var2.a((NBTTag)var5);
               }
            }

            if(!var2.c_()) {
               var1.a((String)"Passengers", (NBTTag)var2);
            }
         }

      } catch (Throwable var6) {
         CrashReport var3 = CrashReport.a(var6, "Saving entity NBT");
         CrashReportSystemDetails var4 = var3.a("Entity being saved");
         this.a(var4);
         throw new class_e(var3);
      }
   }

   public void f(NBTTagCompound var1) {
      try {
         NBTTagList var2 = var1.c("Pos", 6);
         NBTTagList var9 = var1.c("Motion", 6);
         NBTTagList var10 = var1.c("Rotation", 5);
         this.motX = var9.e(0);
         this.motY = var9.e(1);
         this.motZ = var9.e(2);
         if(Math.abs(this.motX) > 10.0D) {
            this.motX = 0.0D;
         }

         if(Math.abs(this.motY) > 10.0D) {
            this.motY = 0.0D;
         }

         if(Math.abs(this.motZ) > 10.0D) {
            this.motZ = 0.0D;
         }

         this.lastX = this.M = this.locX = var2.e(0);
         this.lastY = this.N = this.locY = var2.e(1);
         this.lastZ = this.O = this.locZ = var2.e(2);
         this.lastYaw = this.yaw = var10.f(0);
         this.lastPitch = this.pitch = var10.f(1);
         this.h(this.yaw);
         this.i(this.yaw);
         this.fallDistance = var1.j("FallDistance");
         this.fireTicks = var1.g("Fire");
         this.j(var1.g("Air"));
         this.onGround = var1.p("OnGround");
         if(var1.e("Dimension")) {
            this.dimension = var1.h("Dimension");
         }

         this.invlunerable = var1.p("Invulnerable");
         this.portalCooldown = var1.h("PortalCooldown");
         if(var1.b("UUID")) {
            this.uniqueID = var1.a("UUID");
         }

         this.b(this.locX, this.locY, this.locZ);
         this.b(this.yaw, this.pitch);
         if(var1.b("CustomName", 8)) {
            this.c(var1.l("CustomName"));
         }

         this.i(var1.p("CustomNameVisible"));
         this.aD.a(var1);
         this.c(var1.p("Silent"));
         this.f(var1.p("Glowing"));
         if(var1.b("Tags", 9)) {
            this.aF.clear();
            NBTTagList var5 = var1.c("Tags", 8);
            int var6 = Math.min(var5.c(), 1024);

            for(int var7 = 0; var7 < var6; ++var7) {
               this.aF.add(var5.g(var7));
            }
         }

         this.a(var1);
         if(this.aq()) {
            this.b(this.locX, this.locY, this.locZ);
         }

      } catch (Throwable var8) {
         CrashReport var3 = CrashReport.a(var8, "Loading entity NBT");
         CrashReportSystemDetails var4 = var3.a("Entity being loaded");
         this.a(var4);
         throw new class_e(var3);
      }
   }

   protected boolean aq() {
      return true;
   }

   protected final String ar() {
      return EntityTypes.b(this);
   }

   protected abstract void a(NBTTagCompound var1);

   protected abstract void b(NBTTagCompound var1);

   public void as() {
   }

   protected NBTTagList a(double... var1) {
      NBTTagList var2 = new NBTTagList();
      double[] var3 = var1;
      int var4 = var1.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         double var6 = var3[var5];
         var2.a((NBTTag)(new NBTTagDouble(var6)));
      }

      return var2;
   }

   protected NBTTagList a(float... var1) {
      NBTTagList var2 = new NBTTagList();
      float[] var3 = var1;
      int var4 = var1.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         float var6 = var3[var5];
         var2.a((NBTTag)(new NBTTagFloat(var6)));
      }

      return var2;
   }

   public class_yc a(Item var1, int var2) {
      return this.a(var1, var2, 0.0F);
   }

   public class_yc a(Item var1, int var2, float var3) {
      return this.a(new ItemStack(var1, var2, 0), var3);
   }

   public class_yc a(ItemStack var1, float var2) {
      if(var1.b != 0 && var1.b() != null) {
         class_yc var3 = new class_yc(this.world, this.locX, this.locY + (double)var2, this.locZ, var1);
         var3.q();
         this.world.a((Entity)var3);
         return var3;
      } else {
         return null;
      }
   }

   public boolean at() {
      return !this.dead;
   }

   public boolean au() {
      if(this.noClip) {
         return false;
      } else {
         BlockPosition.class_b_in_class_cj var1 = BlockPosition.class_b_in_class_cj.s();

         for(int var2 = 0; var2 < 8; ++var2) {
            int var3 = MathHelper.c(this.locY + (double)(((float)((var2 >> 0) % 2) - 0.5F) * 0.1F) + (double)this.bm());
            int var4 = MathHelper.c(this.locX + (double)(((float)((var2 >> 1) % 2) - 0.5F) * this.width * 0.8F));
            int var5 = MathHelper.c(this.locZ + (double)(((float)((var2 >> 2) % 2) - 0.5F) * this.width * 0.8F));
            if(var1.p() != var4 || var1.q() != var3 || var1.r() != var5) {
               var1.d(var4, var3, var5);
               if(this.world.getType(var1).getBlock().j()) {
                  var1.t();
                  return true;
               }
            }
         }

         var1.t();
         return false;
      }
   }

   public boolean a(EntityHuman var1, ItemStack var2, EnumHand var3) {
      return false;
   }

   public AxisAlignedBB j(Entity var1) {
      return null;
   }

   public void av() {
      Entity var1 = this.bx();
      if(this.aH() && var1.dead) {
         this.p();
      } else {
         this.motX = 0.0D;
         this.motY = 0.0D;
         this.motZ = 0.0D;
         this.m();
         if(this.aH()) {
            var1.k(this);
         }
      }
   }

   public void k(Entity var1) {
      if(this.w(var1)) {
         var1.b(this.locX, this.locY + this.ax() + var1.aw(), this.locZ);
      }
   }

   public double aw() {
      return 0.0D;
   }

   public double ax() {
      return (double)this.length * 0.75D;
   }

   public boolean m(Entity var1) {
      return this.a(var1, false);
   }

   public boolean a(Entity var1, boolean var2) {
      if(var2 || this.n(var1) && var1.q(this)) {
         if(this.aH()) {
            this.p();
         }

         this.as = var1;
         this.as.o(this);
         return true;
      } else {
         return false;
      }
   }

   protected boolean n(Entity var1) {
      return this.j <= 0;
   }

   public void ay() {
      for(int var1 = this.h.size() - 1; var1 >= 0; --var1) {
         ((Entity)this.h.get(var1)).p();
      }

   }

   public void p() {
      if(this.as != null) {
         Entity var1 = this.as;
         this.as = null;
         var1.p(this);
      }

   }

   protected void o(Entity var1) {
      if(var1.bx() != this) {
         throw new IllegalStateException("Use x.startRiding(y), not y.addPassenger(x)");
      } else {
         if(!this.world.E && var1 instanceof EntityHuman && !(this.bs() instanceof EntityHuman)) {
            this.h.add(0, var1);
         } else {
            this.h.add(var1);
         }

      }
   }

   protected void p(Entity var1) {
      if(var1.bx() == this) {
         throw new IllegalStateException("Use x.stopRiding(y), not y.removePassenger(x)");
      } else {
         this.h.remove(var1);
         var1.j = 60;
      }
   }

   protected boolean q(Entity var1) {
      return this.bt().size() < 1;
   }

   public float az() {
      return 0.0F;
   }

   public Vec3D aA() {
      return null;
   }

   public void e(BlockPosition var1) {
      if(this.portalCooldown > 0) {
         this.portalCooldown = this.aB();
      } else {
         if(!this.world.E && !var1.equals(this.an)) {
            this.an = new BlockPosition(var1);
            ShapeDetector.ShapeDetectorCollection var2 = Blocks.aY.c(this.world, this.an);
            double var3 = var2.b().k() == EnumDirection.class_a_in_class_cq.X?(double)var2.a().r():(double)var2.a().p();
            double var5 = var2.b().k() == EnumDirection.class_a_in_class_cq.X?this.locZ:this.locX;
            var5 = Math.abs(MathHelper.c(var5 - (double)(var2.b().e().c() == EnumDirection.EnumAxisDirection.NEGATIVE?1:0), var3, var3 - (double)var2.d()));
            double var7 = MathHelper.c(this.locY - 1.0D, (double)var2.a().q(), (double)(var2.a().q() - var2.e()));
            this.ao = new Vec3D(var5, var7, 0.0D);
            this.ap = var2.b();
         }

         this.ak = true;
      }
   }

   public int aB() {
      return 300;
   }

   public Iterable aD() {
      return this.aE;
   }

   public Iterable aE() {
      return this.aE;
   }

   public Iterable aF() {
      return Iterables.concat(this.aD(), this.aE());
   }

   public void a(EnumInventorySlot var1, ItemStack var2) {
   }

   public boolean aG() {
      boolean var1 = this.world != null && this.world.E;
      return !this.fireProof && (this.fireTicks > 0 || var1 && this.i(0));
   }

   public boolean aH() {
      return this.bx() != null;
   }

   public boolean aI() {
      return !this.bt().isEmpty();
   }

   public boolean aJ() {
      return this.i(1);
   }

   public void d(boolean var1) {
      this.b(1, var1);
   }

   public boolean aK() {
      return this.i(3);
   }

   public void e(boolean var1) {
      this.b(3, var1);
   }

   public boolean aL() {
      return this.ar || this.world.E && this.i(6);
   }

   public void f(boolean var1) {
      this.ar = var1;
      if(!this.world.E) {
         this.b(6, this.ar);
      }

   }

   public boolean aM() {
      return this.i(5);
   }

   public ScoreboardTeamBase aN() {
      return this.world.ad().g(this.getUniqueId().toString());
   }

   public boolean r(Entity var1) {
      return this.a(var1.aN());
   }

   public boolean a(ScoreboardTeamBase var1) {
      return this.aN() != null?this.aN().a(var1):false;
   }

   public void g(boolean var1) {
      this.b(5, var1);
   }

   protected boolean i(int var1) {
      return (((Byte)this.datawatcher.a(ax)).byteValue() & 1 << var1) != 0;
   }

   protected void b(int var1, boolean var2) {
      byte var3 = ((Byte)this.datawatcher.a(ax)).byteValue();
      if(var2) {
         this.datawatcher.b(ax, Byte.valueOf((byte)(var3 | 1 << var1)));
      } else {
         this.datawatcher.b(ax, Byte.valueOf((byte)(var3 & ~(1 << var1))));
      }

   }

   public int aO() {
      return ((Integer)this.datawatcher.a(ay)).intValue();
   }

   public void j(int var1) {
      this.datawatcher.b(ay, Integer.valueOf(var1));
   }

   public void a(class_xz var1) {
      this.a(DamageSource.b, 5.0F);
      ++this.fireTicks;
      if(this.fireTicks == 0) {
         this.g(8);
      }

   }

   public void b(class_rz var1) {
   }

   protected boolean j(double var1, double var3, double var5) {
      BlockPosition var7 = new BlockPosition(var1, var3, var5);
      double var8 = var1 - (double)var7.p();
      double var10 = var3 - (double)var7.q();
      double var12 = var5 - (double)var7.r();
      List var14 = this.world.a(this.bk());
      if(var14.isEmpty()) {
         return false;
      } else {
         EnumDirection var15 = EnumDirection.UP;
         double var16 = Double.MAX_VALUE;
         if(!this.world.t(var7.e()) && var8 < var16) {
            var16 = var8;
            var15 = EnumDirection.WEST;
         }

         if(!this.world.t(var7.f()) && 1.0D - var8 < var16) {
            var16 = 1.0D - var8;
            var15 = EnumDirection.EAST;
         }

         if(!this.world.t(var7.c()) && var12 < var16) {
            var16 = var12;
            var15 = EnumDirection.NORTH;
         }

         if(!this.world.t(var7.d()) && 1.0D - var12 < var16) {
            var16 = 1.0D - var12;
            var15 = EnumDirection.SOUTH;
         }

         if(!this.world.t(var7.a()) && 1.0D - var10 < var16) {
            var16 = 1.0D - var10;
            var15 = EnumDirection.UP;
         }

         float var18 = this.random.nextFloat() * 0.2F + 0.1F;
         float var19 = (float)var15.c().a();
         if(var15.k() == EnumDirection.class_a_in_class_cq.X) {
            this.motX += (double)(var19 * var18);
         } else if(var15.k() == EnumDirection.class_a_in_class_cq.Y) {
            this.motY += (double)(var19 * var18);
         } else if(var15.k() == EnumDirection.class_a_in_class_cq.Z) {
            this.motZ += (double)(var19 * var18);
         }

         return true;
      }
   }

   public void aP() {
      this.E = true;
      this.fallDistance = 0.0F;
   }

   public String h_() {
      if(this.o_()) {
         return this.be();
      } else {
         String var1 = EntityTypes.b(this);
         if(var1 == null) {
            var1 = "generic";
         }

         return class_di.a("entity." + var1 + ".name");
      }
   }

   public Entity[] aQ() {
      return null;
   }

   public boolean s(Entity var1) {
      return this == var1;
   }

   public float aR() {
      return 0.0F;
   }

   public void h(float var1) {
   }

   public void i(float var1) {
   }

   public boolean aS() {
      return true;
   }

   public boolean t(Entity var1) {
      return false;
   }

   public String toString() {
      return String.format("%s[\'%s\'/%d, l=\'%s\', x=%.2f, y=%.2f, z=%.2f]", new Object[]{this.getClass().getSimpleName(), this.h_(), Integer.valueOf(this.id), this.world == null?"~NULL~":this.world.T().j(), Double.valueOf(this.locX), Double.valueOf(this.locY), Double.valueOf(this.locZ)});
   }

   public boolean b(DamageSource var1) {
      return this.invlunerable && var1 != DamageSource.k && !var1.u();
   }

   public void h(boolean var1) {
      this.invlunerable = var1;
   }

   public void u(Entity var1) {
      this.b(var1.locX, var1.locY, var1.locZ, var1.yaw, var1.pitch);
   }

   private void a(Entity var1) {
      NBTTagCompound var2 = new NBTTagCompound();
      var1.e(var2);
      var2.q("Dimension");
      this.f(var2);
      this.portalCooldown = var1.portalCooldown;
      this.an = var1.an;
      this.ao = var1.ao;
      this.ap = var1.ap;
   }

   public Entity c(int var1) {
      if(!this.world.E && !this.dead) {
         this.world.C.a("changeDimension");
         MinecraftServer var2 = this.h();
         int var3 = this.dimension;
         WorldServer var4 = var2.a(var3);
         WorldServer var5 = var2.a(var1);
         this.dimension = var1;
         if(var3 == 1 && var1 == 1) {
            var5 = var2.a(0);
            this.dimension = 0;
         }

         this.world.e(this);
         this.dead = false;
         this.world.C.a("reposition");
         BlockPosition var6;
         if(var1 == 1) {
            var6 = var5.p();
         } else {
            double var7 = this.locX;
            double var9 = this.locZ;
            double var11 = 8.0D;
            if(var1 == -1) {
               var7 = MathHelper.a(var7 / var11, var5.aj().b() + 16.0D, var5.aj().d() - 16.0D);
               var9 = MathHelper.a(var9 / var11, var5.aj().c() + 16.0D, var5.aj().e() - 16.0D);
            } else if(var1 == 0) {
               var7 = MathHelper.a(var7 * var11, var5.aj().b() + 16.0D, var5.aj().d() - 16.0D);
               var9 = MathHelper.a(var9 * var11, var5.aj().c() + 16.0D, var5.aj().e() - 16.0D);
            }

            var7 = (double)MathHelper.a((int)var7, -29999872, 29999872);
            var9 = (double)MathHelper.a((int)var9, -29999872, 29999872);
            float var13 = this.yaw;
            this.b(var7, this.locY, var9, 90.0F, 0.0F);
            class_aia var14 = var5.x();
            var14.b(this, var13);
            var6 = new BlockPosition(this);
         }

         var4.a(this, false);
         this.world.C.c("reloading");
         Entity var16 = EntityTypes.a((String)EntityTypes.b(this), (World)var5);
         if(var16 != null) {
            var16.a(this);
            if(var3 == 1 && var1 == 1) {
               BlockPosition var8 = var5.q(var5.R());
               var16.a(var8, var16.yaw, var16.pitch);
            } else {
               var16.a(var6, var16.yaw, var16.pitch);
            }

            boolean var15 = var16.attachedToPlayer;
            var16.attachedToPlayer = true;
            var5.a(var16);
            var16.attachedToPlayer = var15;
            var5.a(var16, false);
         }

         this.dead = true;
         this.world.C.b();
         var4.m();
         var5.m();
         this.world.C.b();
         return var16;
      } else {
         return null;
      }
   }

   public boolean aU() {
      return true;
   }

   public float a(class_aho var1, World var2, BlockPosition var3, IBlockData var4) {
      return var4.getBlock().a(this);
   }

   public boolean a(class_aho var1, World var2, BlockPosition var3, IBlockData var4, float var5) {
      return true;
   }

   public int aV() {
      return 3;
   }

   public Vec3D aX() {
      return this.ao;
   }

   public EnumDirection aY() {
      return this.ap;
   }

   public boolean aZ() {
      return false;
   }

   public void a(CrashReportSystemDetails var1) {
      var1.a("Entity Type", new Callable() {
         public String a() throws Exception {
            return EntityTypes.b(Entity.this) + " (" + Entity.this.getClass().getCanonicalName() + ")";
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
      var1.a((String)"Entity ID", (Object)Integer.valueOf(this.id));
      var1.a("Entity Name", new Callable() {
         public String a() throws Exception {
            return Entity.this.h_();
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
      var1.a((String)"Entity\'s Exact location", (Object)String.format("%.2f, %.2f, %.2f", new Object[]{Double.valueOf(this.locX), Double.valueOf(this.locY), Double.valueOf(this.locZ)}));
      var1.a((String)"Entity\'s Block location", (Object)CrashReportSystemDetails.a(MathHelper.c(this.locX), MathHelper.c(this.locY), MathHelper.c(this.locZ)));
      var1.a((String)"Entity\'s Momentum", (Object)String.format("%.2f, %.2f, %.2f", new Object[]{Double.valueOf(this.motX), Double.valueOf(this.motY), Double.valueOf(this.motZ)}));
      var1.a("Entity\'s Passengers", new Callable() {
         public String a() throws Exception {
            return Entity.this.bt().toString();
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
      var1.a("Entity\'s Vehicle", new Callable() {
         public String a() throws Exception {
            return Entity.this.bx().toString();
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
   }

   public void a(UUID var1) {
      this.uniqueID = var1;
   }

   public UUID getUniqueId() {
      return this.uniqueID;
   }

   public boolean bc() {
      return true;
   }

   public IChatBaseComponent i_() {
      ChatComponentText var1 = new ChatComponentText(ScoreboardTeam.a(this.aN(), this.h_()));
      var1.b().a(this.bj());
      var1.b().a(this.getUniqueId().toString());
      return var1;
   }

   public void c(String var1) {
      this.datawatcher.b(az, var1);
   }

   public String be() {
      return (String)this.datawatcher.a(az);
   }

   public boolean o_() {
      return !((String)this.datawatcher.a(az)).isEmpty();
   }

   public void i(boolean var1) {
      this.datawatcher.b(aA, Boolean.valueOf(var1));
   }

   public boolean bf() {
      return ((Boolean)this.datawatcher.a(aA)).booleanValue();
   }

   public void a(double var1, double var3, double var5) {
      this.aG = true;
      this.b(var1, var3, var5, this.yaw, this.pitch);
      this.world.a(this, false);
   }

   public void a(class_ke var1) {
   }

   public EnumDirection bh() {
      return EnumDirection.b(MathHelper.c((double)(this.yaw * 4.0F / 360.0F) + 0.5D) & 3);
   }

   public EnumDirection bi() {
      return this.bh();
   }

   protected ChatHoverable bj() {
      NBTTagCompound var1 = new NBTTagCompound();
      String var2 = EntityTypes.b(this);
      var1.a("id", this.getUniqueId().toString());
      if(var2 != null) {
         var1.a("type", var2);
      }

      var1.a("name", this.h_());
      return new ChatHoverable(ChatHoverable.EnumHoverAction.SHOW_ENTITY, new ChatComponentText(var1.toString()));
   }

   public boolean a(EntityPlayer var1) {
      return true;
   }

   public AxisAlignedBB bk() {
      return this.boundingBox;
   }

   public void a(AxisAlignedBB var1) {
      this.boundingBox = var1;
   }

   public float bm() {
      return this.length * 0.85F;
   }

   public boolean bn() {
      return this.au;
   }

   public void j(boolean var1) {
      this.au = var1;
   }

   public boolean c(int var1, ItemStack var2) {
      return false;
   }

   public void a(IChatBaseComponent var1) {
   }

   public boolean a(int var1, String var2) {
      return true;
   }

   public BlockPosition c() {
      return new BlockPosition(this.locX, this.locY + 0.5D, this.locZ);
   }

   public Vec3D d() {
      return new Vec3D(this.locX, this.locY, this.locZ);
   }

   public World e() {
      return this.world;
   }

   public Entity f() {
      return this;
   }

   public boolean z_() {
      return false;
   }

   public void a(CommandObjectiveExecutor.EnumCommandResult var1, int var2) {
      if(this.world != null && !this.world.E) {
         this.aD.a(this.world.u(), this, var1, var2);
      }

   }

   public MinecraftServer h() {
      return this.world.u();
   }

   public CommandObjectiveExecutor bo() {
      return this.aD;
   }

   public void v(Entity var1) {
      this.aD.a(var1.bo());
   }

   public EnumResult a(EntityHuman var1, Vec3D var2, ItemStack var3, EnumHand var4) {
      return EnumResult.PASS;
   }

   public boolean bp() {
      return false;
   }

   protected void a(class_rz var1, Entity var2) {
      if(var2 instanceof class_rz) {
         class_agn.a((class_rz)((class_rz)var2), (Entity)var1);
      }

      class_agn.b(var1, var2);
   }

   public void b(EntityPlayer var1) {
   }

   public void c(EntityPlayer var1) {
   }

   public float a(class_aod var1) {
      float var2 = MathHelper.g(this.yaw);
      switch(Entity.SyntheticClass_1.a[var1.ordinal()]) {
      case 1:
         return var2 + 180.0F;
      case 2:
         return var2 + 270.0F;
      case 3:
         return var2 + 90.0F;
      default:
         return var2;
      }
   }

   public float a(class_amq var1) {
      float var2 = MathHelper.g(this.yaw);
      switch(Entity.SyntheticClass_1.b[var1.ordinal()]) {
      case 1:
         return -var2;
      case 2:
         return 180.0F - var2;
      default:
         return var2;
      }
   }

   public boolean bq() {
      return false;
   }

   public boolean br() {
      boolean var1 = this.aG;
      this.aG = false;
      return var1;
   }

   public Entity bs() {
      return null;
   }

   public List bt() {
      return (List)(this.h.isEmpty()?Collections.emptyList():Lists.newArrayList((Iterable)this.h));
   }

   public boolean w(Entity var1) {
      Iterator var2 = this.bt().iterator();

      Entity var3;
      do {
         if(!var2.hasNext()) {
            return false;
         }

         var3 = (Entity)var2.next();
      } while(!var3.equals(var1));

      return true;
   }

   public Collection bu() {
      HashSet var1 = Sets.newHashSet();
      this.a((Class)Entity.class, (Set)var1);
      return var1;
   }

   public Collection b(Class var1) {
      HashSet var2 = Sets.newHashSet();
      this.a((Class)var1, (Set)var2);
      return var2;
   }

   private void a(Class var1, Set var2) {
      Entity var4;
      for(Iterator var3 = this.bt().iterator(); var3.hasNext(); var4.a(var1, var2)) {
         var4 = (Entity)var3.next();
         if(var1.isAssignableFrom(var4.getClass())) {
            var2.add(var4);
         }
      }

   }

   public Entity bv() {
      Entity var1;
      for(var1 = this; var1.aH(); var1 = var1.bx()) {
         ;
      }

      return var1;
   }

   public boolean x(Entity var1) {
      return this.bv() == var1.bv();
   }

   public boolean y(Entity var1) {
      Iterator var2 = this.bt().iterator();

      Entity var3;
      do {
         if(!var2.hasNext()) {
            return false;
         }

         var3 = (Entity)var2.next();
         if(var3.equals(var1)) {
            return true;
         }
      } while(!var3.y(var1));

      return true;
   }

   public boolean bw() {
      Entity var1 = this.bs();
      return var1 instanceof EntityHuman?((EntityHuman)var1).cI():!this.world.E;
   }

   public Entity bx() {
      return this.as;
   }

   public class_axg z() {
      return class_axg.NORMAL;
   }

   public EnumSoundCategory by() {
      return EnumSoundCategory.NEUTRAL;
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b = new int[class_amq.values().length];

      static {
         try {
            b[class_amq.LEFT_RIGHT.ordinal()] = 1;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            b[class_amq.FRONT_BACK.ordinal()] = 2;
         } catch (NoSuchFieldError var4) {
            ;
         }

         a = new int[class_aod.values().length];

         try {
            a[class_aod.CLOCKWISE_180.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[class_aod.CLOCKWISE_90.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
