package net.minecraft.server;

import net.minecraft.server.CommandAbstract;
import net.minecraft.server.Entity;
import net.minecraft.server.ExceptionEntityNotFound;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.Scoreboard;
import net.minecraft.server.ScoreboardScore;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.class_bbk;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;

public class CommandObjectiveExecutor {
   private static final int a = CommandObjectiveExecutor.EnumCommandResult.values().length;
   private static final String[] b = new String[a];
   private String[] c;
   private String[] d;

   public CommandObjectiveExecutor() {
      this.c = b;
      this.d = b;
   }

   public void a(MinecraftServer var1, final ICommandListener var2, CommandObjectiveExecutor.EnumCommandResult var3, int var4) {
      String var5 = this.c[var3.a()];
      if(var5 != null) {
         ICommandListener var6 = new ICommandListener() {
            public String h_() {
               return var2.h_();
            }

            public IChatBaseComponent i_() {
               return var2.i_();
            }

            public void a(IChatBaseComponent var1) {
               var2.a(var1);
            }

            public boolean a(int var1, String var2x) {
               return true;
            }

            public BlockPosition c() {
               return var2.c();
            }

            public Vec3D d() {
               return var2.d();
            }

            public World e() {
               return var2.e();
            }

            public Entity f() {
               return var2.f();
            }

            public boolean z_() {
               return var2.z_();
            }

            public void a(CommandObjectiveExecutor.EnumCommandResult var1, int var2x) {
               var2.a(var1, var2x);
            }

            public MinecraftServer h() {
               return var2.h();
            }
         };

         String var7;
         try {
            var7 = CommandAbstract.e(var1, var6, var5);
         } catch (ExceptionEntityNotFound var12) {
            return;
         }

         String var8 = this.d[var3.a()];
         if(var8 != null) {
            Scoreboard var9 = var2.e().ad();
            class_bbk var10 = var9.b(var8);
            if(var10 != null) {
               if(var9.b(var7, var10)) {
                  ScoreboardScore var11 = var9.c(var7, var10);
                  var11.c(var4);
               }
            }
         }
      }
   }

   public void a(NBTTagCompound var1) {
      if(var1.b("CommandStats", 10)) {
         NBTTagCompound var2 = var1.o("CommandStats");
         CommandObjectiveExecutor.EnumCommandResult[] var3 = CommandObjectiveExecutor.EnumCommandResult.values();
         int var4 = var3.length;

         for(int var5 = 0; var5 < var4; ++var5) {
            CommandObjectiveExecutor.EnumCommandResult var6 = var3[var5];
            String var7 = var6.b() + "Name";
            String var8 = var6.b() + "Objective";
            if(var2.b(var7, 8) && var2.b(var8, 8)) {
               String var9 = var2.l(var7);
               String var10 = var2.l(var8);
               a(this, var6, var9, var10);
            }
         }

      }
   }

   public void b(NBTTagCompound var1) {
      NBTTagCompound var2 = new NBTTagCompound();
      CommandObjectiveExecutor.EnumCommandResult[] var3 = CommandObjectiveExecutor.EnumCommandResult.values();
      int var4 = var3.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         CommandObjectiveExecutor.EnumCommandResult var6 = var3[var5];
         String var7 = this.c[var6.a()];
         String var8 = this.d[var6.a()];
         if(var7 != null && var8 != null) {
            var2.a(var6.b() + "Name", var7);
            var2.a(var6.b() + "Objective", var8);
         }
      }

      if(!var2.c_()) {
         var1.a((String)"CommandStats", (NBTTag)var2);
      }

   }

   public static void a(CommandObjectiveExecutor var0, CommandObjectiveExecutor.EnumCommandResult var1, String var2, String var3) {
      if(var2 != null && !var2.isEmpty() && var3 != null && !var3.isEmpty()) {
         if(var0.c == b || var0.d == b) {
            var0.c = new String[a];
            var0.d = new String[a];
         }

         var0.c[var1.a()] = var2;
         var0.d[var1.a()] = var3;
      } else {
         a(var0, var1);
      }
   }

   private static void a(CommandObjectiveExecutor var0, CommandObjectiveExecutor.EnumCommandResult var1) {
      if(var0.c != b && var0.d != b) {
         var0.c[var1.a()] = null;
         var0.d[var1.a()] = null;
         boolean var2 = true;
         CommandObjectiveExecutor.EnumCommandResult[] var3 = CommandObjectiveExecutor.EnumCommandResult.values();
         int var4 = var3.length;

         for(int var5 = 0; var5 < var4; ++var5) {
            CommandObjectiveExecutor.EnumCommandResult var6 = var3[var5];
            if(var0.c[var6.a()] != null && var0.d[var6.a()] != null) {
               var2 = false;
               break;
            }
         }

         if(var2) {
            var0.c = b;
            var0.d = b;
         }

      }
   }

   public void a(CommandObjectiveExecutor var1) {
      CommandObjectiveExecutor.EnumCommandResult[] var2 = CommandObjectiveExecutor.EnumCommandResult.values();
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         CommandObjectiveExecutor.EnumCommandResult var5 = var2[var4];
         a(this, var5, var1.c[var5.a()], var1.d[var5.a()]);
      }

   }

   public static enum EnumCommandResult {
      SUCCESS_COUNT(0, "SuccessCount"),
      AFFECTED_BLOCKS(1, "AffectedBlocks"),
      AFFECTED_ENTITIES(2, "AffectedEntities"),
      AFFECTED_ITEMS(3, "AffectedItems"),
      QUERY_RESULT(4, "QueryResult");

      final int f;
      final String g;

      private EnumCommandResult(int var3, String var4) {
         this.f = var3;
         this.g = var4;
      }

      public int a() {
         return this.f;
      }

      public String b() {
         return this.g;
      }

      public static String[] c() {
         String[] var0 = new String[values().length];
         int var1 = 0;
         CommandObjectiveExecutor.EnumCommandResult[] var2 = values();
         int var3 = var2.length;

         for(int var4 = 0; var4 < var3; ++var4) {
            CommandObjectiveExecutor.EnumCommandResult var5 = var2[var4];
            var0[var1++] = var5.b();
         }

         return var0;
      }

      public static CommandObjectiveExecutor.EnumCommandResult a(String var0) {
         CommandObjectiveExecutor.EnumCommandResult[] var1 = values();
         int var2 = var1.length;

         for(int var3 = 0; var3 < var2; ++var3) {
            CommandObjectiveExecutor.EnumCommandResult var4 = var1[var3];
            if(var4.b().equals(var0)) {
               return var4;
            }
         }

         return null;
      }
   }
}
