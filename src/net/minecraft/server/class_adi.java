package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_rz;

public class class_adi extends Item {
   public class_adi() {
      this.j = 1;
      this.e(64);
      this.a(CreativeModeTab.i);
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      var4 = var4.a(var6);
      if(!var2.a(var4, var6, var1)) {
         return EnumResult.FAIL;
      } else {
         if(var3.getType(var4).getMaterial() == Material.a) {
            var3.a(var2, var4, class_ng.bu, EnumSoundCategory.BLOCKS, 1.0F, i.nextFloat() * 0.4F + 0.8F);
            var3.a((BlockPosition)var4, (IBlockData)Blocks.ab.u(), 11);
         }

         var1.a(1, (class_rz)var2);
         return EnumResult.SUCCESS;
      }
   }
}
