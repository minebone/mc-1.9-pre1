package net.minecraft.server;

import net.minecraft.server.class_bz;

public class ExceptionInvalidSyntax extends class_bz {
   public ExceptionInvalidSyntax() {
      this("commands.generic.snytax", new Object[0]);
   }

   public ExceptionInvalidSyntax(String var1, Object... var2) {
      super(var1, var2);
   }
}
