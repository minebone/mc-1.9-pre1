package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import java.util.Random;
import net.minecraft.server.Entity;
import net.minecraft.server.class_kk;

public interface class_bay {
   boolean a(Random var1, Entity var2);

   public abstract static class class_a_in_class_bay {
      private final class_kk a;
      private final Class b;

      protected class_a_in_class_bay(class_kk var1, Class var2) {
         this.a = var1;
         this.b = var2;
      }

      public class_kk a() {
         return this.a;
      }

      public Class b() {
         return this.b;
      }

      public abstract JsonElement a(class_bay var1, JsonSerializationContext var2);

      public abstract class_bay a(JsonElement var1, JsonDeserializationContext var2);
   }
}
