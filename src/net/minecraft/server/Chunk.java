package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CrashReport;
import net.minecraft.server.CrashReportSystemDetails;
import net.minecraft.server.Entity;
import net.minecraft.server.EntitySlice;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumSkyBlock;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.WorldType;
import net.minecraft.server.class_ahm;
import net.minecraft.server.class_aij;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_alf;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arx;
import net.minecraft.server.class_ary;
import net.minecraft.server.ChunkSection;
import net.minecraft.server.class_ate;
import net.minecraft.server.class_atg;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_e;
import net.minecraft.server.MathHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Chunk {
   private static final Logger d = LogManager.getLogger();
   public static final ChunkSection a = null;
   private final ChunkSection[] chunkSections;
   private final byte[] f;
   private final int[] g;
   private final boolean[] h;
   private boolean i;
   private final World world;
   private final int[] heightMap;
   public final int posX;
   public final int posZ;
   private boolean l;
   private final Map tileEntities;
   private final EntitySlice[] entitySlices;
   private boolean done;
   private boolean lit;
   private boolean q;
   private boolean r;
   private boolean s;
   private long t;
   private int u;
   private long lastSaved;
   private int w;
   private ConcurrentLinkedQueue x;

   public Chunk(World var1, int var2, int var3) {
      this.chunkSections = new ChunkSection[16];
      this.f = new byte[256];
      this.g = new int[256];
      this.h = new boolean[256];
      this.tileEntities = Maps.newHashMap();
      this.w = 4096;
      this.x = Queues.newConcurrentLinkedQueue();
      this.entitySlices = (EntitySlice[])(new EntitySlice[16]);
      this.world = var1;
      this.posX = var2;
      this.posZ = var3;
      this.heightMap = new int[256];

      for(int var4 = 0; var4 < this.entitySlices.length; ++var4) {
         this.entitySlices[var4] = new EntitySlice(Entity.class);
      }

      Arrays.fill(this.g, -999);
      Arrays.fill(this.f, (byte)-1);
   }

   public Chunk(World var1, class_ate var2, int var3, int var4) {
      this(var1, var3, var4);
      short var5 = 256;
      boolean var6 = !var1.s.isNotOverworld();

      for(int var7 = 0; var7 < 16; ++var7) {
         for(int var8 = 0; var8 < 16; ++var8) {
            for(int var9 = 0; var9 < var5; ++var9) {
               IBlockData var10 = var2.a(var7, var9, var8);
               if(var10.getMaterial() != Material.a) {
                  int var11 = var9 >> 4;
                  if(this.chunkSections[var11] == a) {
                     this.chunkSections[var11] = new ChunkSection(var11 << 4, var6);
                  }

                  this.chunkSections[var11].a(var7, var9 & 15, var8, var10);
               }
            }
         }
      }

   }

   public boolean a(int var1, int var2) {
      return var1 == this.posX && var2 == this.posZ;
   }

   public int e(BlockPosition var1) {
      return this.b(var1.p() & 15, var1.r() & 15);
   }

   public int b(int var1, int var2) {
      return this.heightMap[var2 << 4 | var1];
   }

   private ChunkSection y() {
      for(int var1 = this.chunkSections.length - 1; var1 >= 0; --var1) {
         if(this.chunkSections[var1] != a) {
            return this.chunkSections[var1];
         }
      }

      return null;
   }

   public int g() {
      ChunkSection var1 = this.y();
      return var1 == null?0:var1.getYPosition();
   }

   public ChunkSection[] getChunkSections() {
      return this.chunkSections;
   }

   public void b() {
      int var1 = this.g();
      this.u = Integer.MAX_VALUE;

      for(int var2 = 0; var2 < 16; ++var2) {
         for(int var3 = 0; var3 < 16; ++var3) {
            this.g[var2 + (var3 << 4)] = -999;

            int var4;
            for(var4 = var1 + 16; var4 > 0; --var4) {
               if(this.d(var2, var4 - 1, var3) != 0) {
                  this.heightMap[var3 << 4 | var2] = var4;
                  if(var4 < this.u) {
                     this.u = var4;
                  }
                  break;
               }
            }

            if(!this.world.s.isNotOverworld()) {
               var4 = 15;
               int var5 = var1 + 16 - 1;

               do {
                  int var6 = this.d(var2, var5, var3);
                  if(var6 == 0 && var4 != 15) {
                     var6 = 1;
                  }

                  var4 -= var6;
                  if(var4 > 0) {
                     ChunkSection var7 = this.chunkSections[var5 >> 4];
                     if(var7 != a) {
                        var7.setSkyLight(var2, var5 & 15, var3, var4);
                        this.world.m(new BlockPosition((this.posX << 4) + var2, var5, (this.posZ << 4) + var3));
                     }
                  }

                  --var5;
               } while(var5 > 0 && var4 > 0);
            }
         }
      }

      this.r = true;
   }

   private void d(int var1, int var2) {
      this.h[var1 + var2 * 16] = true;
      this.l = true;
   }

   private void h(boolean var1) {
      this.world.C.a("recheckGaps");
      if(this.world.a((BlockPosition)(new BlockPosition(this.posX * 16 + 8, 0, this.posZ * 16 + 8)), (int)16)) {
         for(int var2 = 0; var2 < 16; ++var2) {
            for(int var3 = 0; var3 < 16; ++var3) {
               if(this.h[var2 + var3 * 16]) {
                  this.h[var2 + var3 * 16] = false;
                  int var4 = this.b(var2, var3);
                  int var5 = this.posX * 16 + var2;
                  int var6 = this.posZ * 16 + var3;
                  int var7 = Integer.MAX_VALUE;

                  Iterator var8;
                  EnumDirection var9;
                  for(var8 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator(); var8.hasNext(); var7 = Math.min(var7, this.world.b(var5 + var9.g(), var6 + var9.i()))) {
                     var9 = (EnumDirection)var8.next();
                  }

                  this.b(var5, var6, var7);
                  var8 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

                  while(var8.hasNext()) {
                     var9 = (EnumDirection)var8.next();
                     this.b(var5 + var9.g(), var6 + var9.i(), var4);
                  }

                  if(var1) {
                     this.world.C.b();
                     return;
                  }
               }
            }
         }

         this.l = false;
      }

      this.world.C.b();
   }

   private void b(int var1, int var2, int var3) {
      int var4 = this.world.l(new BlockPosition(var1, 0, var2)).q();
      if(var4 > var3) {
         this.a(var1, var2, var3, var4 + 1);
      } else if(var4 < var3) {
         this.a(var1, var2, var4, var3 + 1);
      }

   }

   private void a(int var1, int var2, int var3, int var4) {
      if(var4 > var3 && this.world.a((BlockPosition)(new BlockPosition(var1, 0, var2)), (int)16)) {
         for(int var5 = var3; var5 < var4; ++var5) {
            this.world.c(EnumSkyBlock.SKY, new BlockPosition(var1, var5, var2));
         }

         this.r = true;
      }

   }

   private void c(int var1, int var2, int var3) {
      int var4 = this.heightMap[var3 << 4 | var1] & 255;
      int var5 = var4;
      if(var2 > var4) {
         var5 = var2;
      }

      while(var5 > 0 && this.d(var1, var5 - 1, var3) == 0) {
         --var5;
      }

      if(var5 != var4) {
         this.world.a(var1 + this.posX * 16, var3 + this.posZ * 16, var5, var4);
         this.heightMap[var3 << 4 | var1] = var5;
         int var6 = this.posX * 16 + var1;
         int var7 = this.posZ * 16 + var3;
         int var8;
         int var13;
         if(!this.world.s.isNotOverworld()) {
            ChunkSection var9;
            if(var5 < var4) {
               for(var8 = var5; var8 < var4; ++var8) {
                  var9 = this.chunkSections[var8 >> 4];
                  if(var9 != a) {
                     var9.setSkyLight(var1, var8 & 15, var3, 15);
                     this.world.m(new BlockPosition((this.posX << 4) + var1, var8, (this.posZ << 4) + var3));
                  }
               }
            } else {
               for(var8 = var4; var8 < var5; ++var8) {
                  var9 = this.chunkSections[var8 >> 4];
                  if(var9 != a) {
                     var9.setSkyLight(var1, var8 & 15, var3, 0);
                     this.world.m(new BlockPosition((this.posX << 4) + var1, var8, (this.posZ << 4) + var3));
                  }
               }
            }

            var8 = 15;

            while(var5 > 0 && var8 > 0) {
               --var5;
               var13 = this.d(var1, var5, var3);
               if(var13 == 0) {
                  var13 = 1;
               }

               var8 -= var13;
               if(var8 < 0) {
                  var8 = 0;
               }

               ChunkSection var10 = this.chunkSections[var5 >> 4];
               if(var10 != a) {
                  var10.setSkyLight(var1, var5 & 15, var3, var8);
               }
            }
         }

         var8 = this.heightMap[var3 << 4 | var1];
         var13 = var4;
         int var14 = var8;
         if(var8 < var4) {
            var13 = var8;
            var14 = var4;
         }

         if(var8 < this.u) {
            this.u = var8;
         }

         if(!this.world.s.isNotOverworld()) {
            Iterator var11 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

            while(var11.hasNext()) {
               EnumDirection var12 = (EnumDirection)var11.next();
               this.a(var6 + var12.g(), var7 + var12.i(), var13, var14);
            }

            this.a(var6, var7, var13, var14);
         }

         this.r = true;
      }
   }

   public int b(BlockPosition var1) {
      return this.a(var1).c();
   }

   private int d(int var1, int var2, int var3) {
      return this.a(var1, var2, var3).c();
   }

   public IBlockData a(BlockPosition var1) {
      return this.a(var1.p(), var1.q(), var1.r());
   }

   public IBlockData a(final int var1, final int var2, final int var3) {
      if(this.world.L() == WorldType.g) {
         IBlockData var8 = null;
         if(var2 == 60) {
            var8 = Blocks.cv.u();
         }

         if(var2 == 70) {
            var8 = class_atg.c(var1, var3);
         }

         return var8 == null?Blocks.AIR.u():var8;
      } else {
         try {
            if(var2 >= 0 && var2 >> 4 < this.chunkSections.length) {
               ChunkSection var4 = this.chunkSections[var2 >> 4];
               if(var4 != a) {
                  return var4.a(var1 & 15, var2 & 15, var3 & 15);
               }
            }

            return Blocks.AIR.u();
         } catch (Throwable var7) {
            CrashReport var5 = CrashReport.a(var7, "Getting block state");
            CrashReportSystemDetails var6 = var5.a("Block being got");
            var6.a("Location", new Callable() {
               public String a() throws Exception {
                  return CrashReportSystemDetails.a(var1, var2, var3);
               }

               // $FF: synthetic method
               public Object call() throws Exception {
                  return this.a();
               }
            });
            throw new class_e(var5);
         }
      }
   }

   public IBlockData a(BlockPosition var1, IBlockData var2) {
      int var3 = var1.p() & 15;
      int var4 = var1.q();
      int var5 = var1.r() & 15;
      int var6 = var5 << 4 | var3;
      if(var4 >= this.g[var6] - 1) {
         this.g[var6] = -999;
      }

      int var7 = this.heightMap[var6];
      IBlockData var8 = this.a(var1);
      if(var8 == var2) {
         return null;
      } else {
         Block var9 = var2.getBlock();
         Block var10 = var8.getBlock();
         ChunkSection var11 = this.chunkSections[var4 >> 4];
         boolean var12 = false;
         if(var11 == a) {
            if(var9 == Blocks.AIR) {
               return null;
            }

            var11 = this.chunkSections[var4 >> 4] = new ChunkSection(var4 >> 4 << 4, !this.world.s.isNotOverworld());
            var12 = var4 >= var7;
         }

         var11.a(var3, var4 & 15, var5, var2);
         if(var10 != var9) {
            if(!this.world.E) {
               var10.b(this.world, var1, var8);
            } else if(var10 instanceof class_alf) {
               this.world.s(var1);
            }
         }

         if(var11.a(var3, var4 & 15, var5).getBlock() != var9) {
            return null;
         } else {
            if(var12) {
               this.b();
            } else {
               int var13 = var2.c();
               int var14 = var8.c();
               if(var13 > 0) {
                  if(var4 >= var7) {
                     this.c(var3, var4 + 1, var5);
                  }
               } else if(var4 == var7 - 1) {
                  this.c(var3, var4, var5);
               }

               if(var13 != var14 && (var13 < var14 || this.a(EnumSkyBlock.SKY, var1) > 0 || this.a(EnumSkyBlock.BLOCK, var1) > 0)) {
                  this.d(var3, var5);
               }
            }

            TileEntity var15;
            if(var10 instanceof class_alf) {
               var15 = this.a(var1, Chunk.EnumTileEntityState.CHECK);
               if(var15 != null) {
                  var15.A();
               }
            }

            if(!this.world.E && var10 != var9) {
               var9.c(this.world, var1, var2);
            }

            if(var9 instanceof class_alf) {
               var15 = this.a(var1, Chunk.EnumTileEntityState.CHECK);
               if(var15 == null) {
                  var15 = ((class_alf)var9).a(this.world, var9.e(var2));
                  this.world.a(var1, var15);
               }

               if(var15 != null) {
                  var15.A();
               }
            }

            this.r = true;
            return var8;
         }
      }
   }

   public int a(EnumSkyBlock var1, BlockPosition var2) {
      int var3 = var2.p() & 15;
      int var4 = var2.q();
      int var5 = var2.r() & 15;
      ChunkSection var6 = this.chunkSections[var4 >> 4];
      return var6 == a?(this.c(var2)?var1.c:0):(var1 == EnumSkyBlock.SKY?(this.world.s.isNotOverworld()?0:var6.getSkyLight(var3, var4 & 15, var5)):(var1 == EnumSkyBlock.BLOCK?var6.getEmittedLight(var3, var4 & 15, var5):var1.c));
   }

   public void a(EnumSkyBlock var1, BlockPosition var2, int var3) {
      int var4 = var2.p() & 15;
      int var5 = var2.q();
      int var6 = var2.r() & 15;
      ChunkSection var7 = this.chunkSections[var5 >> 4];
      if(var7 == a) {
         var7 = this.chunkSections[var5 >> 4] = new ChunkSection(var5 >> 4 << 4, !this.world.s.isNotOverworld());
         this.b();
      }

      this.r = true;
      if(var1 == EnumSkyBlock.SKY) {
         if(!this.world.s.isNotOverworld()) {
            var7.setSkyLight(var4, var5 & 15, var6, var3);
         }
      } else if(var1 == EnumSkyBlock.BLOCK) {
         var7.setEmmitedLight(var4, var5 & 15, var6, var3);
      }

   }

   public int a(BlockPosition var1, int var2) {
      int var3 = var1.p() & 15;
      int var4 = var1.q();
      int var5 = var1.r() & 15;
      ChunkSection var6 = this.chunkSections[var4 >> 4];
      if(var6 == a) {
         return !this.world.s.isNotOverworld() && var2 < EnumSkyBlock.SKY.c?EnumSkyBlock.SKY.c - var2:0;
      } else {
         int var7 = this.world.s.isNotOverworld()?0:var6.getSkyLight(var3, var4 & 15, var5);
         var7 -= var2;
         int var8 = var6.getEmittedLight(var3, var4 & 15, var5);
         if(var8 > var7) {
            var7 = var8;
         }

         return var7;
      }
   }

   public void a(Entity var1) {
      this.s = true;
      int var2 = MathHelper.c(var1.locX / 16.0D);
      int var3 = MathHelper.c(var1.locZ / 16.0D);
      if(var2 != this.posX || var3 != this.posZ) {
         d.warn("Wrong location! (" + var2 + ", " + var3 + ") should be (" + this.posX + ", " + this.posZ + "), " + var1, new Object[]{var1});
         var1.S();
      }

      int var4 = MathHelper.c(var1.locY / 16.0D);
      if(var4 < 0) {
         var4 = 0;
      }

      if(var4 >= this.entitySlices.length) {
         var4 = this.entitySlices.length - 1;
      }

      var1.aa = true;
      var1.ab = this.posX;
      var1.ac = var4;
      var1.ad = this.posZ;
      this.entitySlices[var4].add(var1);
   }

   public void b(Entity var1) {
      this.a(var1, var1.ac);
   }

   public void a(Entity var1, int var2) {
      if(var2 < 0) {
         var2 = 0;
      }

      if(var2 >= this.entitySlices.length) {
         var2 = this.entitySlices.length - 1;
      }

      this.entitySlices[var2].remove(var1);
   }

   public boolean c(BlockPosition var1) {
      int var2 = var1.p() & 15;
      int var3 = var1.q();
      int var4 = var1.r() & 15;
      return var3 >= this.heightMap[var4 << 4 | var2];
   }

   private TileEntity g(BlockPosition var1) {
      IBlockData var2 = this.a(var1);
      Block var3 = var2.getBlock();
      return !var3.m()?null:((class_alf)var3).a(this.world, var2.getBlock().e(var2));
   }

   public TileEntity a(BlockPosition var1, Chunk.EnumTileEntityState var2) {
      TileEntity var3 = (TileEntity)this.tileEntities.get(var1);
      if(var3 == null) {
         if(var2 == Chunk.EnumTileEntityState.IMMEDIATE) {
            var3 = this.g(var1);
            this.world.a(var1, var3);
         } else if(var2 == Chunk.EnumTileEntityState.QUEUED) {
            this.x.add(var1);
         }
      } else if(var3.x()) {
         this.tileEntities.remove(var1);
         return null;
      }

      return var3;
   }

   public void a(TileEntity var1) {
      this.a(var1.v(), var1);
      if(this.i) {
         this.world.a(var1);
      }

   }

   public void a(BlockPosition var1, TileEntity var2) {
      var2.a(this.world);
      var2.a(var1);
      if(this.a(var1).getBlock() instanceof class_alf) {
         if(this.tileEntities.containsKey(var1)) {
            ((TileEntity)this.tileEntities.get(var1)).y();
         }

         var2.z();
         this.tileEntities.put(var1, var2);
      }
   }

   public void d(BlockPosition var1) {
      if(this.i) {
         TileEntity var2 = (TileEntity)this.tileEntities.remove(var1);
         if(var2 != null) {
            var2.y();
         }
      }

   }

   public void c() {
      this.i = true;
      this.world.b(this.tileEntities.values());

      for(int var1 = 0; var1 < this.entitySlices.length; ++var1) {
         Iterator var2 = this.entitySlices[var1].iterator();

         while(var2.hasNext()) {
            Entity var3 = (Entity)var2.next();
            var3.as();
         }

         this.world.a((Collection)this.entitySlices[var1]);
      }

   }

   public void d() {
      this.i = false;
      Iterator var1 = this.tileEntities.values().iterator();

      while(var1.hasNext()) {
         TileEntity var2 = (TileEntity)var1.next();
         this.world.b(var2);
      }

      for(int var3 = 0; var3 < this.entitySlices.length; ++var3) {
         this.world.c((Collection)this.entitySlices[var3]);
      }

   }

   public void e() {
      this.r = true;
   }

   public void a(Entity var1, AxisAlignedBB var2, List var3, Predicate var4) {
      int var5 = MathHelper.c((var2.b - 2.0D) / 16.0D);
      int var6 = MathHelper.c((var2.e + 2.0D) / 16.0D);
      var5 = MathHelper.a(var5, 0, this.entitySlices.length - 1);
      var6 = MathHelper.a(var6, 0, this.entitySlices.length - 1);

      label68:
      for(int var7 = var5; var7 <= var6; ++var7) {
         if(!this.entitySlices[var7].isEmpty()) {
            Iterator var8 = this.entitySlices[var7].iterator();

            while(true) {
               Entity var9;
               Entity[] var10;
               do {
                  do {
                     do {
                        if(!var8.hasNext()) {
                           continue label68;
                        }

                        var9 = (Entity)var8.next();
                     } while(!var9.bk().b(var2));
                  } while(var9 == var1);

                  if(var4 == null || var4.apply(var9)) {
                     var3.add(var9);
                  }

                  var10 = var9.aQ();
               } while(var10 == null);

               for(int var11 = 0; var11 < var10.length; ++var11) {
                  var9 = var10[var11];
                  if(var9 != var1 && var9.bk().b(var2) && (var4 == null || var4.apply(var9))) {
                     var3.add(var9);
                  }
               }
            }
         }
      }

   }

   public void a(Class var1, AxisAlignedBB var2, List var3, Predicate var4) {
      int var5 = MathHelper.c((var2.b - 2.0D) / 16.0D);
      int var6 = MathHelper.c((var2.e + 2.0D) / 16.0D);
      var5 = MathHelper.a(var5, 0, this.entitySlices.length - 1);
      var6 = MathHelper.a(var6, 0, this.entitySlices.length - 1);

      label33:
      for(int var7 = var5; var7 <= var6; ++var7) {
         Iterator var8 = this.entitySlices[var7].c(var1).iterator();

         while(true) {
            Entity var9;
            do {
               do {
                  if(!var8.hasNext()) {
                     continue label33;
                  }

                  var9 = (Entity)var8.next();
               } while(!var9.bk().b(var2));
            } while(var4 != null && !var4.apply(var9));

            var3.add(var9);
         }
      }

   }

   public boolean a(boolean var1) {
      if(var1) {
         if(this.s && this.world.P() != this.t || this.r) {
            return true;
         }
      } else if(this.s && this.world.P() >= this.t + 600L) {
         return true;
      }

      return this.r;
   }

   public Random a(long var1) {
      return new Random(this.world.O() + (long)(this.posX * this.posX * 4987142) + (long)(this.posX * 5947611) + (long)(this.posZ * this.posZ) * 4392871L + (long)(this.posZ * 389711) ^ var1);
   }

   public boolean f() {
      return false;
   }

   public void a(class_ary var1, class_arx var2) {
      Chunk var3 = var1.b(this.posX, this.posZ - 1);
      Chunk var4 = var1.b(this.posX + 1, this.posZ);
      Chunk var5 = var1.b(this.posX, this.posZ + 1);
      Chunk var6 = var1.b(this.posX - 1, this.posZ);
      if(var4 != null && var5 != null && var1.b(this.posX + 1, this.posZ + 1) != null) {
         this.a(var2);
      }

      if(var6 != null && var5 != null && var1.b(this.posX - 1, this.posZ + 1) != null) {
         var6.a(var2);
      }

      if(var3 != null && var4 != null && var1.b(this.posX + 1, this.posZ - 1) != null) {
         var3.a(var2);
      }

      if(var3 != null && var6 != null) {
         Chunk var7 = var1.b(this.posX - 1, this.posZ - 1);
         if(var7 != null) {
            var7.a(var2);
         }
      }

   }

   protected void a(class_arx var1) {
      if(this.u()) {
         if(var1.a(this, this.posX, this.posZ)) {
            this.e();
         }
      } else {
         this.o();
         var1.b(this.posX, this.posZ);
         this.e();
      }

   }

   public BlockPosition f(BlockPosition var1) {
      int var2 = var1.p() & 15;
      int var3 = var1.r() & 15;
      int var4 = var2 | var3 << 4;
      BlockPosition var5 = new BlockPosition(var1.p(), this.g[var4], var1.r());
      if(var5.q() == -999) {
         int var6 = this.g() + 15;
         var5 = new BlockPosition(var1.p(), var6, var1.r());
         int var7 = -1;

         while(true) {
            while(var5.q() > 0 && var7 == -1) {
               IBlockData var8 = this.a(var5);
               Material var9 = var8.getMaterial();
               if(!var9.c() && !var9.d()) {
                  var5 = var5.b();
               } else {
                  var7 = var5.q() + 1;
               }
            }

            this.g[var4] = var7;
            break;
         }
      }

      return new BlockPosition(var1.p(), this.g[var4], var1.r());
   }

   public void b(boolean var1) {
      if(this.l && !this.world.s.isNotOverworld() && !var1) {
         this.h(this.world.E);
      }

      this.q = true;
      if(!this.lit && this.done) {
         this.o();
      }

      while(!this.x.isEmpty()) {
         BlockPosition var2 = (BlockPosition)this.x.poll();
         if(this.a(var2, Chunk.EnumTileEntityState.CHECK) == null && this.a(var2).getBlock().m()) {
            TileEntity var3 = this.g(var2);
            this.world.a(var2, var3);
            this.world.b(var2, var2);
         }
      }

   }

   public boolean i() {
      return this.q && this.done && this.lit;
   }

   public boolean j() {
      return this.q;
   }

   public class_ahm k() {
      return new class_ahm(this.posX, this.posZ);
   }

   public boolean c(int var1, int var2) {
      if(var1 < 0) {
         var1 = 0;
      }

      if(var2 >= 256) {
         var2 = 255;
      }

      for(int var3 = var1; var3 <= var2; var3 += 16) {
         ChunkSection var4 = this.chunkSections[var3 >> 4];
         if(var4 != a && !var4.isEmpty()) {
            return false;
         }
      }

      return true;
   }

   public void a(ChunkSection[] var1) {
      if(this.chunkSections.length != var1.length) {
         d.warn("Could not set level chunk sections, array length is " + var1.length + " instead of " + this.chunkSections.length);
      } else {
         System.arraycopy(var1, 0, this.chunkSections, 0, this.chunkSections.length);
      }
   }

   public BiomeBase a(BlockPosition var1, class_aij var2) {
      int var3 = var1.p() & 15;
      int var4 = var1.r() & 15;
      int var5 = this.f[var4 << 4 | var3] & 255;
      BiomeBase var6;
      if(var5 == 255) {
         var6 = var2.a(var1, class_aik.c);
         var5 = BiomeBase.a(var6);
         this.f[var4 << 4 | var3] = (byte)(var5 & 255);
      }

      var6 = BiomeBase.b(var5);
      return var6 == null?class_aik.c:var6;
   }

   public byte[] l() {
      return this.f;
   }

   public void a(byte[] var1) {
      if(this.f.length != var1.length) {
         d.warn("Could not set level chunk biomes, array length is " + var1.length + " instead of " + this.f.length);
      } else {
         for(int var2 = 0; var2 < this.f.length; ++var2) {
            this.f[var2] = var1[var2];
         }

      }
   }

   public void m() {
      this.w = 0;
   }

   public void n() {
      if(this.w < 4096) {
         BlockPosition var1 = new BlockPosition(this.posX << 4, 0, this.posZ << 4);

         for(int var2 = 0; var2 < 8; ++var2) {
            if(this.w >= 4096) {
               return;
            }

            int var3 = this.w % 16;
            int var4 = this.w / 16 % 16;
            int var5 = this.w / 256;
            ++this.w;

            for(int var6 = 0; var6 < 16; ++var6) {
               BlockPosition var7 = var1.a(var4, (var3 << 4) + var6, var5);
               boolean var8 = var6 == 0 || var6 == 15 || var4 == 0 || var4 == 15 || var5 == 0 || var5 == 15;
               if(this.chunkSections[var3] == a && var8 || this.chunkSections[var3] != a && this.chunkSections[var3].a(var4, var6, var5).getMaterial() == Material.a) {
                  EnumDirection[] var9 = EnumDirection.values();
                  int var10 = var9.length;

                  for(int var11 = 0; var11 < var10; ++var11) {
                     EnumDirection var12 = var9[var11];
                     BlockPosition var13 = var7.a(var12);
                     if(this.world.getType(var13).d() > 0) {
                        this.world.w(var13);
                     }
                  }

                  this.world.w(var7);
               }
            }
         }

      }
   }

   public void o() {
      this.done = true;
      this.lit = true;
      BlockPosition var1 = new BlockPosition(this.posX << 4, 0, this.posZ << 4);
      if(!this.world.s.isNotOverworld()) {
         if(this.world.a(var1.a(-1, 0, -1), var1.a(16, this.world.K(), 16))) {
            label44:
            for(int var2 = 0; var2 < 16; ++var2) {
               for(int var3 = 0; var3 < 16; ++var3) {
                  if(!this.e(var2, var3)) {
                     this.lit = false;
                     break label44;
                  }
               }
            }

            if(this.lit) {
               Iterator var5 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

               while(var5.hasNext()) {
                  EnumDirection var6 = (EnumDirection)var5.next();
                  int var4 = var6.c() == EnumDirection.EnumAxisDirection.POSITIVE?16:1;
                  this.world.f(var1.a(var6, var4)).a(var6.d());
               }

               this.z();
            }
         } else {
            this.lit = false;
         }
      }

   }

   private void z() {
      for(int var1 = 0; var1 < this.h.length; ++var1) {
         this.h[var1] = true;
      }

      this.h(false);
   }

   private void a(EnumDirection var1) {
      if(this.done) {
         int var2;
         if(var1 == EnumDirection.EAST) {
            for(var2 = 0; var2 < 16; ++var2) {
               this.e(15, var2);
            }
         } else if(var1 == EnumDirection.WEST) {
            for(var2 = 0; var2 < 16; ++var2) {
               this.e(0, var2);
            }
         } else if(var1 == EnumDirection.SOUTH) {
            for(var2 = 0; var2 < 16; ++var2) {
               this.e(var2, 15);
            }
         } else if(var1 == EnumDirection.NORTH) {
            for(var2 = 0; var2 < 16; ++var2) {
               this.e(var2, 0);
            }
         }

      }
   }

   private boolean e(int var1, int var2) {
      int var3 = this.g();
      boolean var4 = false;
      boolean var5 = false;
      BlockPosition.class_a_in_class_cj var6 = new BlockPosition.class_a_in_class_cj((this.posX << 4) + var1, 0, (this.posZ << 4) + var2);

      int var7;
      for(var7 = var3 + 16 - 1; var7 > this.world.K() || var7 > 0 && !var5; --var7) {
         var6.c(var6.p(), var7, var6.r());
         int var8 = this.b((BlockPosition)var6);
         if(var8 == 255 && var6.q() < this.world.K()) {
            var5 = true;
         }

         if(!var4 && var8 > 0) {
            var4 = true;
         } else if(var4 && var8 == 0 && !this.world.w(var6)) {
            return false;
         }
      }

      for(var7 = var6.q(); var7 > 0; --var7) {
         var6.c(var6.p(), var7, var6.r());
         if(this.a((BlockPosition)var6).d() > 0) {
            this.world.w(var6);
         }
      }

      return true;
   }

   public boolean p() {
      return this.i;
   }

   public World q() {
      return this.world;
   }

   public int[] r() {
      return this.heightMap;
   }

   public void a(int[] var1) {
      if(this.heightMap.length != var1.length) {
         d.warn("Could not set level chunk heightmap, array length is " + var1.length + " instead of " + this.heightMap.length);
      } else {
         for(int var2 = 0; var2 < this.heightMap.length; ++var2) {
            this.heightMap[var2] = var1[var2];
         }

      }
   }

   public Map s() {
      return this.tileEntities;
   }

   public EntitySlice[] t() {
      return this.entitySlices;
   }

   public boolean u() {
      return this.done;
   }

   public void d(boolean var1) {
      this.done = var1;
   }

   public boolean v() {
      return this.lit;
   }

   public void e(boolean var1) {
      this.lit = var1;
   }

   public void f(boolean var1) {
      this.r = var1;
   }

   public void g(boolean var1) {
      this.s = var1;
   }

   public void b(long var1) {
      this.t = var1;
   }

   public int w() {
      return this.u;
   }

   public long x() {
      return this.lastSaved;
   }

   public void c(long var1) {
      this.lastSaved = var1;
   }

   public static enum EnumTileEntityState {
      IMMEDIATE,
      QUEUED,
      CHECK;
   }
}
