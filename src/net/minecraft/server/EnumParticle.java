package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Map;
import java.util.Set;

public enum EnumParticle {
   EXPLOSION_NORMAL("explode", 0, true),
   EXPLOSION_LARGE("largeexplode", 1, true),
   EXPLOSION_HUGE("hugeexplosion", 2, true),
   FIREWORKS_SPARK("fireworksSpark", 3, false),
   WATER_BUBBLE("bubble", 4, false),
   WATER_SPLASH("splash", 5, false),
   WATER_WAKE("wake", 6, false),
   SUSPENDED("suspended", 7, false),
   SUSPENDED_DEPTH("depthsuspend", 8, false),
   CRIT("crit", 9, false),
   CRIT_MAGIC("magicCrit", 10, false),
   SMOKE_NORMAL("smoke", 11, false),
   SMOKE_LARGE("largesmoke", 12, false),
   SPELL("spell", 13, false),
   SPELL_INSTANT("instantSpell", 14, false),
   SPELL_MOB("mobSpell", 15, false),
   SPELL_MOB_AMBIENT("mobSpellAmbient", 16, false),
   SPELL_WITCH("witchMagic", 17, false),
   DRIP_WATER("dripWater", 18, false),
   DRIP_LAVA("dripLava", 19, false),
   VILLAGER_ANGRY("angryVillager", 20, false),
   VILLAGER_HAPPY("happyVillager", 21, false),
   TOWN_AURA("townaura", 22, false),
   NOTE("note", 23, false),
   PORTAL("portal", 24, false),
   ENCHANTMENT_TABLE("enchantmenttable", 25, false),
   FLAME("flame", 26, false),
   LAVA("lava", 27, false),
   FOOTSTEP("footstep", 28, false),
   CLOUD("cloud", 29, false),
   REDSTONE("reddust", 30, false),
   SNOWBALL("snowballpoof", 31, false),
   SNOW_SHOVEL("snowshovel", 32, false),
   SLIME("slime", 33, false),
   HEART("heart", 34, false),
   BARRIER("barrier", 35, false),
   ITEM_CRACK("iconcrack", 36, false, 2),
   BLOCK_CRACK("blockcrack", 37, false, 1),
   BLOCK_DUST("blockdust", 38, false, 1),
   WATER_DROP("droplet", 39, false),
   ITEM_TAKE("take", 40, false),
   MOB_APPEARANCE("mobappearance", 41, true),
   DRAGON_BREATH("dragonbreath", 42, false),
   END_ROD("endRod", 43, false),
   DAMAGE_INDICATOR("damageIndicator", 44, true),
   SWEEP_ATTACK("sweepAttack", 45, true);

   private final String U;
   private final int V;
   private final boolean W;
   private final int X;
   private static final Map Y = Maps.newHashMap();
   private static final Map Z = Maps.newHashMap();

   private EnumParticle(String var3, int var4, boolean var5, int var6) {
      this.U = var3;
      this.V = var4;
      this.W = var5;
      this.X = var6;
   }

   private EnumParticle(String var3, int var4, boolean var5) {
      this(var3, var4, var5, 0);
   }

   public static Set a() {
      return Z.keySet();
   }

   public String b() {
      return this.U;
   }

   public int c() {
      return this.V;
   }

   public int d() {
      return this.X;
   }

   public boolean e() {
      return this.W;
   }

   public static EnumParticle a(int var0) {
      return (EnumParticle)Y.get(Integer.valueOf(var0));
   }

   public static EnumParticle a(String var0) {
      return (EnumParticle)Z.get(var0);
   }

   static {
      EnumParticle[] var0 = values();
      int var1 = var0.length;

      for(int var2 = 0; var2 < var1; ++var2) {
         EnumParticle var3 = var0[var2];
         Y.put(Integer.valueOf(var3.c()), var3);
         Z.put(var3.b(), var3);
      }

   }
}
