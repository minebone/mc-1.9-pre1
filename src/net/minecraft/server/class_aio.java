package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.BlockFlowers;
import net.minecraft.server.BlockTallPlant;
import net.minecraft.server.World;
import net.minecraft.server.class_ato;
import net.minecraft.server.class_atq;
import net.minecraft.server.class_aui;
import net.minecraft.server.class_auw;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_wc;
import net.minecraft.server.class_wi;

public class class_aio extends BiomeBase {
   protected static final class_atq y = new class_atq(false, true);
   protected static final class_atq z = new class_atq(false, false);
   protected static final class_auw A = new class_auw(false);
   private class_aio.class_a_in_class_aio B;

   public class_aio(class_aio.class_a_in_class_aio var1, BiomeBase.class_a_in_class_aif var2) {
      super(var2);
      this.B = var1;
      this.t.z = 10;
      this.t.B = 2;
      if(this.B == class_aio.class_a_in_class_aio.FLOWER) {
         this.t.z = 6;
         this.t.A = 100;
         this.t.B = 1;
         this.v.add(new BiomeBase.BiomeMeta(class_wc.class, 4, 2, 3));
      }

      if(this.B == class_aio.class_a_in_class_aio.NORMAL) {
         this.v.add(new BiomeBase.BiomeMeta(class_wi.class, 5, 4, 4));
      }

      if(this.B == class_aio.class_a_in_class_aio.ROOFED) {
         this.t.z = -999;
      }

   }

   public class_ato a(Random var1) {
      return (class_ato)(this.B == class_aio.class_a_in_class_aio.ROOFED && var1.nextInt(3) > 0?A:(this.B != class_aio.class_a_in_class_aio.BIRCH && var1.nextInt(5) != 0?(var1.nextInt(10) == 0?o:n):z));
   }

   public BlockFlowers.EnumFlowerVarient a(Random var1, BlockPosition var2) {
      if(this.B == class_aio.class_a_in_class_aio.FLOWER) {
         double var3 = MathHelper.a((1.0D + l.a((double)var2.p() / 48.0D, (double)var2.r() / 48.0D)) / 2.0D, 0.0D, 0.9999D);
         BlockFlowers.EnumFlowerVarient var5 = BlockFlowers.EnumFlowerVarient.values()[(int)(var3 * (double)BlockFlowers.EnumFlowerVarient.values().length)];
         return var5 == BlockFlowers.EnumFlowerVarient.BLUE_ORCHID?BlockFlowers.EnumFlowerVarient.POPPY:var5;
      } else {
         return super.a(var1, var2);
      }
   }

   public void a(World var1, Random var2, BlockPosition var3) {
      if(this.B == class_aio.class_a_in_class_aio.ROOFED) {
         this.b(var1, var2, var3);
      }

      int var4 = var2.nextInt(5) - 3;
      if(this.B == class_aio.class_a_in_class_aio.FLOWER) {
         var4 += 2;
      }

      this.a(var1, var2, var3, var4);
      super.a(var1, var2, var3);
   }

   protected void b(World var1, Random var2, BlockPosition var3) {
      for(int var4 = 0; var4 < 4; ++var4) {
         for(int var5 = 0; var5 < 4; ++var5) {
            int var6 = var4 * 4 + 1 + 8 + var2.nextInt(3);
            int var7 = var5 * 4 + 1 + 8 + var2.nextInt(3);
            BlockPosition var8 = var1.l(var3.a(var6, 0, var7));
            if(var2.nextInt(20) == 0) {
               class_aui var9 = new class_aui();
               var9.b(var1, var2, var8);
            } else {
               class_ato var10 = this.a(var2);
               var10.e();
               if(var10.b(var1, var2, var8)) {
                  var10.a(var1, var2, var8);
               }
            }
         }
      }

   }

   protected void a(World var1, Random var2, BlockPosition var3, int var4) {
      for(int var5 = 0; var5 < var4; ++var5) {
         int var6 = var2.nextInt(3);
         if(var6 == 0) {
            m.a(BlockTallPlant.EnumTallFlowerVariants.SYRINGA);
         } else if(var6 == 1) {
            m.a(BlockTallPlant.EnumTallFlowerVariants.ROSE);
         } else if(var6 == 2) {
            m.a(BlockTallPlant.EnumTallFlowerVariants.PAEONIA);
         }

         for(int var7 = 0; var7 < 5; ++var7) {
            int var8 = var2.nextInt(16) + 8;
            int var9 = var2.nextInt(16) + 8;
            int var10 = var2.nextInt(var1.l(var3.a(var8, 0, var9)).q() + 32);
            if(m.b(var1, var2, new BlockPosition(var3.p() + var8, var10, var3.r() + var9))) {
               break;
            }
         }
      }

   }

   public Class g() {
      return class_aio.class;
   }

   public static enum class_a_in_class_aio {
      NORMAL,
      FLOWER,
      BIRCH,
      ROOFED;
   }
}
