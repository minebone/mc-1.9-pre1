package net.minecraft.server;

public enum EnumInventorySlot {
   MAINHAND(EnumInventorySlot.EnumSlotType.HAND, 0, 0, "mainhand"),
   OFFHAND(EnumInventorySlot.EnumSlotType.HAND, 1, 5, "offhand"),
   FEET(EnumInventorySlot.EnumSlotType.ARMOR, 0, 1, "feet"),
   LEGS(EnumInventorySlot.EnumSlotType.ARMOR, 1, 2, "legs"),
   CHEST(EnumInventorySlot.EnumSlotType.ARMOR, 2, 3, "chest"),
   HEAD(EnumInventorySlot.EnumSlotType.ARMOR, 3, 4, "head");

   private final EnumInventorySlot.EnumSlotType g;
   private final int h;
   private final int i;
   private final String j;

   private EnumInventorySlot(EnumInventorySlot.EnumSlotType var3, int var4, int var5, String var6) {
      this.g = var3;
      this.h = var4;
      this.i = var5;
      this.j = var6;
   }

   public EnumInventorySlot.EnumSlotType a() {
      return this.g;
   }

   public int b() {
      return this.h;
   }

   public int c() {
      return this.i;
   }

   public String d() {
      return this.j;
   }

   public static EnumInventorySlot a(String var0) {
      EnumInventorySlot[] var1 = values();
      int var2 = var1.length;

      for(int var3 = 0; var3 < var2; ++var3) {
         EnumInventorySlot var4 = var1[var3];
         if(var4.d().equals(var0)) {
            return var4;
         }
      }

      throw new IllegalArgumentException("Invalid slot \'" + var0 + "\'");
   }

   public static enum EnumSlotType {
      HAND,
      ARMOR;
   }
}
