package net.minecraft.server;

import net.minecraft.server.AchievementList;
import net.minecraft.server.Container;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.Statistic;
import net.minecraft.server.class_aaz;
import net.minecraft.server.class_abs;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_afe;
import net.minecraft.server.class_aff;
import net.minecraft.server.class_afg;
import net.minecraft.server.IInventory;

public class class_aax extends Container {
   private IInventory a;
   private final class_abs f;
   private int g;
   private int h;

   public class_aax(PlayerInventory var1, IInventory var2) {
      this.a = var2;
      this.a((class_abs)(new class_aax.class_c_in_class_aax(var1.e, var2, 0, 56, 51)));
      this.a((class_abs)(new class_aax.class_c_in_class_aax(var1.e, var2, 1, 79, 58)));
      this.a((class_abs)(new class_aax.class_c_in_class_aax(var1.e, var2, 2, 102, 51)));
      this.f = this.a((class_abs)(new class_aax.class_b_in_class_aax(var2, 3, 79, 17)));
      this.a((class_abs)(new class_aax.class_a_in_class_aax(var2, 4, 17, 17)));

      int var3;
      for(var3 = 0; var3 < 3; ++var3) {
         for(int var4 = 0; var4 < 9; ++var4) {
            this.a((class_abs)(new class_abs(var1, var4 + var3 * 9 + 9, 8 + var4 * 18, 84 + var3 * 18)));
         }
      }

      for(var3 = 0; var3 < 9; ++var3) {
         this.a((class_abs)(new class_abs(var1, var3, 8 + var3 * 18, 142)));
      }

   }

   public void a(class_aaz var1) {
      super.a(var1);
      var1.a(this, (IInventory)this.a);
   }

   public void b() {
      super.b();

      for(int var1 = 0; var1 < this.e.size(); ++var1) {
         class_aaz var2 = (class_aaz)this.e.get(var1);
         if(this.g != this.a.c_(0)) {
            var2.a(this, 0, this.a.c_(0));
         }

         if(this.h != this.a.c_(1)) {
            var2.a(this, 1, this.a.c_(1));
         }
      }

      this.g = this.a.c_(0);
      this.h = this.a.c_(1);
   }

   public boolean a(EntityHuman var1) {
      return this.a.a(var1);
   }

   public ItemStack b(EntityHuman var1, int var2) {
      ItemStack var3 = null;
      class_abs var4 = (class_abs)this.c.get(var2);
      if(var4 != null && var4.e()) {
         ItemStack var5 = var4.d();
         var3 = var5.k();
         if((var2 < 0 || var2 > 2) && var2 != 3 && var2 != 4) {
            if(!this.f.e() && this.f.a(var5)) {
               if(!this.a(var5, 3, 4, false)) {
                  return null;
               }
            } else if(class_aax.class_c_in_class_aax.c_(var3)) {
               if(!this.a(var5, 0, 3, false)) {
                  return null;
               }
            } else if(class_aax.class_a_in_class_aax.b_(var3)) {
               if(!this.a(var5, 4, 5, false)) {
                  return null;
               }
            } else if(var2 >= 5 && var2 < 32) {
               if(!this.a(var5, 32, 41, false)) {
                  return null;
               }
            } else if(var2 >= 32 && var2 < 41) {
               if(!this.a(var5, 5, 32, false)) {
                  return null;
               }
            } else if(!this.a(var5, 5, 41, false)) {
               return null;
            }
         } else {
            if(!this.a(var5, 5, 41, true)) {
               return null;
            }

            var4.a(var5, var3);
         }

         if(var5.b == 0) {
            var4.d((ItemStack)null);
         } else {
            var4.f();
         }

         if(var5.b == var3.b) {
            return null;
         }

         var4.a(var1, var5);
      }

      return var3;
   }

   static class class_a_in_class_aax extends class_abs {
      public class_a_in_class_aax(IInventory var1, int var2, int var3, int var4) {
         super(var1, var2, var3, var4);
      }

      public boolean a(ItemStack var1) {
         return b_(var1);
      }

      public static boolean b_(ItemStack var0) {
         return var0 != null && var0.b() == Items.bN;
      }

      public int a() {
         return 64;
      }
   }

   static class class_b_in_class_aax extends class_abs {
      public class_b_in_class_aax(IInventory var1, int var2, int var3, int var4) {
         super(var1, var2, var3, var4);
      }

      public boolean a(ItemStack var1) {
         return var1 != null && class_afe.a(var1);
      }

      public int a() {
         return 64;
      }
   }

   static class class_c_in_class_aax extends class_abs {
      private EntityHuman a;

      public class_c_in_class_aax(EntityHuman var1, IInventory var2, int var3, int var4, int var5) {
         super(var2, var3, var4, var5);
         this.a = var1;
      }

      public boolean a(ItemStack var1) {
         return c_(var1);
      }

      public int a() {
         return 1;
      }

      public void a(EntityHuman var1, ItemStack var2) {
         if(class_aff.c(var2) != class_afg.b) {
            this.a.b((Statistic)AchievementList.B);
         }

         super.a(var1, var2);
      }

      public static boolean c_(ItemStack var0) {
         if(var0 == null) {
            return false;
         } else {
            Item var1 = var0.b();
            return var1 == Items.bG || var1 == Items.bJ || var1 == Items.bH || var1 == Items.bI;
         }
      }
   }
}
