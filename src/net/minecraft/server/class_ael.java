package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumAnimation;
import net.minecraft.server.EnumColor;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.ItemBanner;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_adq;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;
import net.minecraft.server.class_kk;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;

public class class_ael extends Item {
   public class_ael() {
      this.j = 1;
      this.a(CreativeModeTab.j);
      this.e(336);
      this.a(new class_kk("blocking"), new class_adq() {
      });
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      return super.a(var1, var2, var3, var4, var5, var6, var7, var8, var9);
   }

   public String a(ItemStack var1) {
      if(var1.a("BlockEntityTag", false) != null) {
         String var2 = "item.shield.";
         EnumColor var3 = ItemBanner.b(var1);
         var2 = var2 + var3.d() + ".name";
         return class_di.a(var2);
      } else {
         return class_di.a("item.shield.name");
      }
   }

   public EnumAnimation f(ItemStack var1) {
      return EnumAnimation.BLOCK;
   }

   public int e(ItemStack var1) {
      return 72000;
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      var3.c(var4);
      return new class_qo(EnumResult.SUCCESS, var1);
   }

   public boolean a(ItemStack var1, ItemStack var2) {
      return var2.b() == Item.a(Blocks.f)?true:super.a(var1, var2);
   }
}
