package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.Block;
import net.minecraft.server.BlockSandStone;
import net.minecraft.server.BlockStairs;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumColor;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.StructureBoundingBox;
import net.minecraft.server.StructurePiece;
import net.minecraft.server.World;
import net.minecraft.server.WorldGenFactory;
import net.minecraft.server.WorldGenVillage;
import net.minecraft.server.class_aij;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_akm;
import net.minecraft.server.class_amj;
import net.minecraft.server.class_ape;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.BaseBlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_zd;

public class WorldGenVillagePieces {
   public static void a() {
      WorldGenFactory.a(WorldGenVillagePieces.class_a_in_class_awj.class, "ViBH");
      WorldGenFactory.a(WorldGenVillagePieces.WorldGenVillageFarm2.class, "ViDF");
      WorldGenFactory.a(WorldGenVillagePieces.class_c_in_class_awj.class, "ViF");
      WorldGenFactory.a(WorldGenVillagePieces.class_d_in_class_awj.class, "ViL");
      WorldGenFactory.a(WorldGenVillagePieces.class_f_in_class_awj.class, "ViPH");
      WorldGenFactory.a(WorldGenVillagePieces.WorldGenVillageHouse.class, "ViSH");
      WorldGenFactory.a(WorldGenVillagePieces.WorldGenVillageHut.class, "ViSmH");
      WorldGenFactory.a(WorldGenVillagePieces.class_i_in_class_awj.class, "ViST");
      WorldGenFactory.a(WorldGenVillagePieces.class_j_in_class_awj.class, "ViS");
      WorldGenFactory.a(WorldGenVillagePieces.class_k_in_class_awj.class, "ViStart");
      WorldGenFactory.a(WorldGenVillagePieces.WorldGenVillageRoad.class, "ViSR");
      WorldGenFactory.a(WorldGenVillagePieces.class_m_in_class_awj.class, "ViTRH");
      WorldGenFactory.a(WorldGenVillagePieces.class_p_in_class_awj.class, "ViW");
   }

   public static List a(Random var0, int var1) {
      ArrayList var2 = Lists.newArrayList();
      var2.add(new WorldGenVillagePieces.class_e_in_class_awj(WorldGenVillagePieces.WorldGenVillageHouse.class, 4, MathHelper.a(var0, 2 + var1, 4 + var1 * 2)));
      var2.add(new WorldGenVillagePieces.class_e_in_class_awj(WorldGenVillagePieces.class_i_in_class_awj.class, 20, MathHelper.a(var0, 0 + var1, 1 + var1)));
      var2.add(new WorldGenVillagePieces.class_e_in_class_awj(WorldGenVillagePieces.class_a_in_class_awj.class, 20, MathHelper.a(var0, 0 + var1, 2 + var1)));
      var2.add(new WorldGenVillagePieces.class_e_in_class_awj(WorldGenVillagePieces.WorldGenVillageHut.class, 3, MathHelper.a(var0, 2 + var1, 5 + var1 * 3)));
      var2.add(new WorldGenVillagePieces.class_e_in_class_awj(WorldGenVillagePieces.class_f_in_class_awj.class, 15, MathHelper.a(var0, 0 + var1, 2 + var1)));
      var2.add(new WorldGenVillagePieces.class_e_in_class_awj(WorldGenVillagePieces.WorldGenVillageFarm2.class, 3, MathHelper.a(var0, 1 + var1, 4 + var1)));
      var2.add(new WorldGenVillagePieces.class_e_in_class_awj(WorldGenVillagePieces.class_c_in_class_awj.class, 3, MathHelper.a(var0, 2 + var1, 4 + var1 * 2)));
      var2.add(new WorldGenVillagePieces.class_e_in_class_awj(WorldGenVillagePieces.class_j_in_class_awj.class, 15, MathHelper.a((Random)var0, 0, 1 + var1)));
      var2.add(new WorldGenVillagePieces.class_e_in_class_awj(WorldGenVillagePieces.class_m_in_class_awj.class, 8, MathHelper.a(var0, 0 + var1, 3 + var1 * 2)));
      Iterator var3 = var2.iterator();

      while(var3.hasNext()) {
         if(((WorldGenVillagePieces.class_e_in_class_awj)var3.next()).d == 0) {
            var3.remove();
         }
      }

      return var2;
   }

   private static int a(List var0) {
      boolean var1 = false;
      int var2 = 0;

      WorldGenVillagePieces.class_e_in_class_awj var4;
      for(Iterator var3 = var0.iterator(); var3.hasNext(); var2 += var4.b) {
         var4 = (WorldGenVillagePieces.class_e_in_class_awj)var3.next();
         if(var4.d > 0 && var4.c < var4.d) {
            var1 = true;
         }
      }

      return var1?var2:-1;
   }

   private static WorldGenVillagePieces.WorldGenVillagePiece a(WorldGenVillagePieces.class_k_in_class_awj var0, WorldGenVillagePieces.class_e_in_class_awj var1, List var2, Random var3, int var4, int var5, int var6, EnumDirection var7, int var8) {
      Class var9 = var1.a;
      Object var10 = null;
      if(var9 == WorldGenVillagePieces.WorldGenVillageHouse.class) {
         var10 = WorldGenVillagePieces.WorldGenVillageHouse.a(var0, var2, var3, var4, var5, var6, var7, var8);
      } else if(var9 == WorldGenVillagePieces.class_i_in_class_awj.class) {
         var10 = WorldGenVillagePieces.class_i_in_class_awj.a(var0, var2, var3, var4, var5, var6, var7, var8);
      } else if(var9 == WorldGenVillagePieces.class_a_in_class_awj.class) {
         var10 = WorldGenVillagePieces.class_a_in_class_awj.a(var0, var2, var3, var4, var5, var6, var7, var8);
      } else if(var9 == WorldGenVillagePieces.WorldGenVillageHut.class) {
         var10 = WorldGenVillagePieces.WorldGenVillageHut.a(var0, var2, var3, var4, var5, var6, var7, var8);
      } else if(var9 == WorldGenVillagePieces.class_f_in_class_awj.class) {
         var10 = WorldGenVillagePieces.class_f_in_class_awj.a(var0, var2, var3, var4, var5, var6, var7, var8);
      } else if(var9 == WorldGenVillagePieces.WorldGenVillageFarm2.class) {
         var10 = WorldGenVillagePieces.WorldGenVillageFarm2.a(var0, var2, var3, var4, var5, var6, var7, var8);
      } else if(var9 == WorldGenVillagePieces.class_c_in_class_awj.class) {
         var10 = WorldGenVillagePieces.class_c_in_class_awj.a(var0, var2, var3, var4, var5, var6, var7, var8);
      } else if(var9 == WorldGenVillagePieces.class_j_in_class_awj.class) {
         var10 = WorldGenVillagePieces.class_j_in_class_awj.a(var0, var2, var3, var4, var5, var6, var7, var8);
      } else if(var9 == WorldGenVillagePieces.class_m_in_class_awj.class) {
         var10 = WorldGenVillagePieces.class_m_in_class_awj.a(var0, var2, var3, var4, var5, var6, var7, var8);
      }

      return (WorldGenVillagePieces.WorldGenVillagePiece)var10;
   }

   private static WorldGenVillagePieces.WorldGenVillagePiece c(WorldGenVillagePieces.class_k_in_class_awj var0, List var1, Random var2, int var3, int var4, int var5, EnumDirection var6, int var7) {
      int var8 = a(var0.e);
      if(var8 <= 0) {
         return null;
      } else {
         int var9 = 0;

         while(var9 < 5) {
            ++var9;
            int var10 = var2.nextInt(var8);
            Iterator var11 = var0.e.iterator();

            while(var11.hasNext()) {
               WorldGenVillagePieces.class_e_in_class_awj var12 = (WorldGenVillagePieces.class_e_in_class_awj)var11.next();
               var10 -= var12.b;
               if(var10 < 0) {
                  if(!var12.a(var7) || var12 == var0.d && var0.e.size() > 1) {
                     break;
                  }

                  WorldGenVillagePieces.WorldGenVillagePiece var13 = a(var0, var12, var1, var2, var3, var4, var5, var6, var7);
                  if(var13 != null) {
                     ++var12.c;
                     var0.d = var12;
                     if(!var12.a()) {
                        var0.e.remove(var12);
                     }

                     return var13;
                  }
               }
            }
         }

         StructureBoundingBox var14 = WorldGenVillagePieces.class_d_in_class_awj.a(var0, var1, var2, var3, var4, var5, var6);
         if(var14 != null) {
            return new WorldGenVillagePieces.class_d_in_class_awj(var0, var7, var2, var14, var6);
         } else {
            return null;
         }
      }
   }

   private static StructurePiece d(WorldGenVillagePieces.class_k_in_class_awj var0, List var1, Random var2, int var3, int var4, int var5, EnumDirection var6, int var7) {
      if(var7 > 50) {
         return null;
      } else if(Math.abs(var3 - var0.c().a) <= 112 && Math.abs(var5 - var0.c().c) <= 112) {
         WorldGenVillagePieces.WorldGenVillagePiece var8 = c(var0, var1, var2, var3, var4, var5, var6, var7 + 1);
         if(var8 != null) {
            int var9 = (var8.l.a + var8.l.d) / 2;
            int var10 = (var8.l.c + var8.l.f) / 2;
            int var11 = var8.l.d - var8.l.a;
            int var12 = var8.l.f - var8.l.c;
            int var13 = var11 > var12?var11:var12;
            if(var0.h().a(var9, var10, var13 / 2 + 4, WorldGenVillage.a)) {
               var1.add(var8);
               var0.f.add(var8);
               return var8;
            }
         }

         return null;
      } else {
         return null;
      }
   }

   private static StructurePiece e(WorldGenVillagePieces.class_k_in_class_awj var0, List var1, Random var2, int var3, int var4, int var5, EnumDirection var6, int var7) {
      if(var7 > 3 + var0.c) {
         return null;
      } else if(Math.abs(var3 - var0.c().a) <= 112 && Math.abs(var5 - var0.c().c) <= 112) {
         StructureBoundingBox var8 = WorldGenVillagePieces.WorldGenVillageRoad.a(var0, var1, var2, var3, var4, var5, var6);
         if(var8 != null && var8.b > 10) {
            WorldGenVillagePieces.WorldGenVillageRoad var9 = new WorldGenVillagePieces.WorldGenVillageRoad(var0, var7, var2, var8, var6);
            int var10 = (var9.l.a + var9.l.d) / 2;
            int var11 = (var9.l.c + var9.l.f) / 2;
            int var12 = var9.l.d - var9.l.a;
            int var13 = var9.l.f - var9.l.c;
            int var14 = var12 > var13?var12:var13;
            if(var0.h().a(var10, var11, var14 / 2 + 4, WorldGenVillage.a)) {
               var1.add(var9);
               var0.g.add(var9);
               return var9;
            }
         }

         return null;
      } else {
         return null;
      }
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumDirection.values().length];

      static {
         try {
            a[EnumDirection.NORTH.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.SOUTH.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.EAST.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static class class_d_in_class_awj extends WorldGenVillagePieces.WorldGenVillagePiece {
      public class_d_in_class_awj() {
      }

      public class_d_in_class_awj(WorldGenVillagePieces.class_k_in_class_awj var1, int var2, Random var3, StructureBoundingBox var4, EnumDirection var5) {
         super(var1, var2);
         this.a(var5);
         this.l = var4;
      }

      public static StructureBoundingBox a(WorldGenVillagePieces.class_k_in_class_awj var0, List var1, Random var2, int var3, int var4, int var5, EnumDirection var6) {
         StructureBoundingBox var7 = StructureBoundingBox.a(var3, var4, var5, 0, 0, 0, 3, 4, 2, var6);
         return StructurePiece.a(var1, var7) != null?null:var7;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.h < 0) {
            this.h = this.b(var1, var3);
            if(this.h < 0) {
               return true;
            }

            this.l.a(0, this.h - this.l.e + 4 - 1, 0);
         }

         this.a(var1, var3, 0, 0, 0, 2, 3, 1, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, Blocks.aO.u(), 1, 0, 0, var3);
         this.a(var1, Blocks.aO.u(), 1, 1, 0, var3);
         this.a(var1, Blocks.aO.u(), 1, 2, 0, var3);
         this.a(var1, Blocks.L.a(EnumColor.WHITE.b()), 1, 3, 0, var3);
         this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.EAST), 2, 3, 0, var3);
         this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.NORTH), 1, 3, 1, var3);
         this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.WEST), 0, 3, 0, var3);
         this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.SOUTH), 1, 3, -1, var3);
         return true;
      }
   }

   public static class WorldGenVillageFarm2 extends WorldGenVillagePieces.WorldGenVillagePiece {
      private Block a;
      private Block b;
      private Block c;
      private Block d;

      public WorldGenVillageFarm2() {
      }

      public WorldGenVillageFarm2(WorldGenVillagePieces.class_k_in_class_awj var1, int var2, Random var3, StructureBoundingBox var4, EnumDirection var5) {
         super(var1, var2);
         this.a((EnumDirection)var5);
         this.l = var4;
         this.a = this.a(var3);
         this.b = this.a(var3);
         this.c = this.a(var3);
         this.d = this.a(var3);
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("CA", Block.h.a((Object)this.a));
         var1.a("CB", Block.h.a((Object)this.b));
         var1.a("CC", Block.h.a((Object)this.c));
         var1.a("CD", Block.h.a((Object)this.d));
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.a = Block.b(var1.h("CA"));
         this.b = Block.b(var1.h("CB"));
         this.c = Block.b(var1.h("CC"));
         this.d = Block.b(var1.h("CD"));
         if(!(this.a instanceof class_akm)) {
            this.a = Blocks.aj;
         }

         if(!(this.b instanceof class_akm)) {
            this.b = Blocks.cb;
         }

         if(!(this.c instanceof class_akm)) {
            this.c = Blocks.cc;
         }

         if(!(this.d instanceof class_akm)) {
            this.d = Blocks.cZ;
         }

      }

      private Block a(Random var1) {
         switch(var1.nextInt(10)) {
         case 0:
         case 1:
            return Blocks.cb;
         case 2:
         case 3:
            return Blocks.cc;
         case 4:
            return Blocks.cZ;
         default:
            return Blocks.aj;
         }
      }

      public static WorldGenVillagePieces.WorldGenVillageFarm2 a(WorldGenVillagePieces.class_k_in_class_awj var0, List var1, Random var2, int var3, int var4, int var5, EnumDirection var6, int var7) {
         StructureBoundingBox var8 = StructureBoundingBox.a(var3, var4, var5, 0, 0, 0, 13, 4, 9, var6);
         return a((StructureBoundingBox)var8) && StructurePiece.a(var1, var8) == null?new WorldGenVillagePieces.WorldGenVillageFarm2(var0, var7, var2, var8, var6):null;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.h < 0) {
            this.h = this.b(var1, var3);
            if(this.h < 0) {
               return true;
            }

            this.l.a(0, this.h - this.l.e + 4 - 1, 0);
         }

         this.a(var1, var3, 0, 1, 0, 12, 4, 8, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 1, 0, 1, 2, 0, 7, Blocks.ak.u(), Blocks.ak.u(), false);
         this.a(var1, var3, 4, 0, 1, 5, 0, 7, Blocks.ak.u(), Blocks.ak.u(), false);
         this.a(var1, var3, 7, 0, 1, 8, 0, 7, Blocks.ak.u(), Blocks.ak.u(), false);
         this.a(var1, var3, 10, 0, 1, 11, 0, 7, Blocks.ak.u(), Blocks.ak.u(), false);
         this.a(var1, var3, 0, 0, 0, 0, 0, 8, Blocks.r.u(), Blocks.r.u(), false);
         this.a(var1, var3, 6, 0, 0, 6, 0, 8, Blocks.r.u(), Blocks.r.u(), false);
         this.a(var1, var3, 12, 0, 0, 12, 0, 8, Blocks.r.u(), Blocks.r.u(), false);
         this.a(var1, var3, 1, 0, 0, 11, 0, 0, Blocks.r.u(), Blocks.r.u(), false);
         this.a(var1, var3, 1, 0, 8, 11, 0, 8, Blocks.r.u(), Blocks.r.u(), false);
         this.a(var1, var3, 3, 0, 1, 3, 0, 7, Blocks.j.u(), Blocks.j.u(), false);
         this.a(var1, var3, 9, 0, 1, 9, 0, 7, Blocks.j.u(), Blocks.j.u(), false);

         int var4;
         int var5;
         for(var4 = 1; var4 <= 7; ++var4) {
            var5 = ((class_akm)this.a).g();
            int var6 = var5 / 3;
            this.a(var1, this.a.a(MathHelper.a(var2, var6, var5)), 1, 1, var4, var3);
            this.a(var1, this.a.a(MathHelper.a(var2, var6, var5)), 2, 1, var4, var3);
            int var7 = ((class_akm)this.b).g();
            int var8 = var7 / 3;
            this.a(var1, this.b.a(MathHelper.a(var2, var8, var7)), 4, 1, var4, var3);
            this.a(var1, this.b.a(MathHelper.a(var2, var8, var7)), 5, 1, var4, var3);
            int var9 = ((class_akm)this.c).g();
            int var10 = var9 / 3;
            this.a(var1, this.c.a(MathHelper.a(var2, var10, var9)), 7, 1, var4, var3);
            this.a(var1, this.c.a(MathHelper.a(var2, var10, var9)), 8, 1, var4, var3);
            int var11 = ((class_akm)this.d).g();
            int var12 = var11 / 3;
            this.a(var1, this.d.a(MathHelper.a(var2, var12, var11)), 10, 1, var4, var3);
            this.a(var1, this.d.a(MathHelper.a(var2, var12, var11)), 11, 1, var4, var3);
         }

         for(var4 = 0; var4 < 9; ++var4) {
            for(var5 = 0; var5 < 13; ++var5) {
               this.b(var1, var5, 4, var4, var3);
               this.b(var1, Blocks.d.u(), var5, -1, var4, var3);
            }
         }

         return true;
      }
   }

   public static class class_c_in_class_awj extends WorldGenVillagePieces.WorldGenVillagePiece {
      private Block a;
      private Block b;

      public class_c_in_class_awj() {
      }

      public class_c_in_class_awj(WorldGenVillagePieces.class_k_in_class_awj var1, int var2, Random var3, StructureBoundingBox var4, EnumDirection var5) {
         super(var1, var2);
         this.a((EnumDirection)var5);
         this.l = var4;
         this.a = this.a(var3);
         this.b = this.a(var3);
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("CA", Block.h.a((Object)this.a));
         var1.a("CB", Block.h.a((Object)this.b));
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.a = Block.b(var1.h("CA"));
         this.b = Block.b(var1.h("CB"));
      }

      private Block a(Random var1) {
         switch(var1.nextInt(10)) {
         case 0:
         case 1:
            return Blocks.cb;
         case 2:
         case 3:
            return Blocks.cc;
         case 4:
            return Blocks.cZ;
         default:
            return Blocks.aj;
         }
      }

      public static WorldGenVillagePieces.class_c_in_class_awj a(WorldGenVillagePieces.class_k_in_class_awj var0, List var1, Random var2, int var3, int var4, int var5, EnumDirection var6, int var7) {
         StructureBoundingBox var8 = StructureBoundingBox.a(var3, var4, var5, 0, 0, 0, 7, 4, 9, var6);
         return a((StructureBoundingBox)var8) && StructurePiece.a(var1, var8) == null?new WorldGenVillagePieces.class_c_in_class_awj(var0, var7, var2, var8, var6):null;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.h < 0) {
            this.h = this.b(var1, var3);
            if(this.h < 0) {
               return true;
            }

            this.l.a(0, this.h - this.l.e + 4 - 1, 0);
         }

         this.a(var1, var3, 0, 1, 0, 6, 4, 8, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 1, 0, 1, 2, 0, 7, Blocks.ak.u(), Blocks.ak.u(), false);
         this.a(var1, var3, 4, 0, 1, 5, 0, 7, Blocks.ak.u(), Blocks.ak.u(), false);
         this.a(var1, var3, 0, 0, 0, 0, 0, 8, Blocks.r.u(), Blocks.r.u(), false);
         this.a(var1, var3, 6, 0, 0, 6, 0, 8, Blocks.r.u(), Blocks.r.u(), false);
         this.a(var1, var3, 1, 0, 0, 5, 0, 0, Blocks.r.u(), Blocks.r.u(), false);
         this.a(var1, var3, 1, 0, 8, 5, 0, 8, Blocks.r.u(), Blocks.r.u(), false);
         this.a(var1, var3, 3, 0, 1, 3, 0, 7, Blocks.j.u(), Blocks.j.u(), false);

         int var4;
         int var5;
         for(var4 = 1; var4 <= 7; ++var4) {
            var5 = ((class_akm)this.a).g();
            int var6 = var5 / 3;
            this.a(var1, this.a.a(MathHelper.a(var2, var6, var5)), 1, 1, var4, var3);
            this.a(var1, this.a.a(MathHelper.a(var2, var6, var5)), 2, 1, var4, var3);
            int var7 = ((class_akm)this.b).g();
            int var8 = var7 / 3;
            this.a(var1, this.b.a(MathHelper.a(var2, var8, var7)), 4, 1, var4, var3);
            this.a(var1, this.b.a(MathHelper.a(var2, var8, var7)), 5, 1, var4, var3);
         }

         for(var4 = 0; var4 < 9; ++var4) {
            for(var5 = 0; var5 < 7; ++var5) {
               this.b(var1, var5, 4, var4, var3);
               this.b(var1, Blocks.d.u(), var5, -1, var4, var3);
            }
         }

         return true;
      }
   }

   public static class class_j_in_class_awj extends WorldGenVillagePieces.WorldGenVillagePiece {
      private boolean a;

      public class_j_in_class_awj() {
      }

      public class_j_in_class_awj(WorldGenVillagePieces.class_k_in_class_awj var1, int var2, Random var3, StructureBoundingBox var4, EnumDirection var5) {
         super(var1, var2);
         this.a(var5);
         this.l = var4;
      }

      public static WorldGenVillagePieces.class_j_in_class_awj a(WorldGenVillagePieces.class_k_in_class_awj var0, List var1, Random var2, int var3, int var4, int var5, EnumDirection var6, int var7) {
         StructureBoundingBox var8 = StructureBoundingBox.a(var3, var4, var5, 0, 0, 0, 10, 6, 7, var6);
         return a(var8) && StructurePiece.a(var1, var8) == null?new WorldGenVillagePieces.class_j_in_class_awj(var0, var7, var2, var8, var6):null;
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("Chest", this.a);
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.a = var1.p("Chest");
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.h < 0) {
            this.h = this.b(var1, var3);
            if(this.h < 0) {
               return true;
            }

            this.l.a(0, this.h - this.l.e + 6 - 1, 0);
         }

         this.a(var1, var3, 0, 1, 0, 9, 4, 6, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 0, 0, 0, 9, 0, 6, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 0, 4, 0, 9, 4, 6, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 0, 5, 0, 9, 5, 6, Blocks.U.u(), Blocks.U.u(), false);
         this.a(var1, var3, 1, 5, 1, 8, 5, 5, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 1, 1, 0, 2, 3, 0, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 0, 1, 0, 0, 4, 0, Blocks.r.u(), Blocks.r.u(), false);
         this.a(var1, var3, 3, 1, 0, 3, 4, 0, Blocks.r.u(), Blocks.r.u(), false);
         this.a(var1, var3, 0, 1, 6, 0, 4, 6, Blocks.r.u(), Blocks.r.u(), false);
         this.a(var1, Blocks.f.u(), 3, 3, 1, var3);
         this.a(var1, var3, 3, 1, 2, 3, 3, 2, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 4, 1, 3, 5, 3, 3, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 0, 1, 1, 0, 3, 5, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 1, 1, 6, 5, 3, 6, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 5, 1, 0, 5, 3, 0, Blocks.aO.u(), Blocks.aO.u(), false);
         this.a(var1, var3, 9, 1, 0, 9, 3, 0, Blocks.aO.u(), Blocks.aO.u(), false);
         this.a(var1, var3, 6, 1, 4, 9, 4, 6, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, Blocks.k.u(), 7, 1, 5, var3);
         this.a(var1, Blocks.k.u(), 8, 1, 5, var3);
         this.a(var1, Blocks.bi.u(), 9, 2, 5, var3);
         this.a(var1, Blocks.bi.u(), 9, 2, 4, var3);
         this.a(var1, var3, 7, 2, 4, 8, 2, 5, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, Blocks.e.u(), 6, 1, 3, var3);
         this.a(var1, Blocks.al.u(), 6, 2, 3, var3);
         this.a(var1, Blocks.al.u(), 6, 3, 3, var3);
         this.a(var1, Blocks.T.u(), 8, 1, 1, var3);
         this.a(var1, Blocks.bj.u(), 0, 2, 2, var3);
         this.a(var1, Blocks.bj.u(), 0, 2, 4, var3);
         this.a(var1, Blocks.bj.u(), 2, 2, 6, var3);
         this.a(var1, Blocks.bj.u(), 4, 2, 6, var3);
         this.a(var1, Blocks.aO.u(), 2, 1, 4, var3);
         this.a(var1, Blocks.aB.u(), 2, 2, 4, var3);
         this.a(var1, Blocks.f.u(), 1, 1, 5, var3);
         this.a(var1, Blocks.ad.u().set(BlockStairs.a, EnumDirection.NORTH), 2, 1, 5, var3);
         this.a(var1, Blocks.ad.u().set(BlockStairs.a, EnumDirection.WEST), 1, 1, 4, var3);
         if(!this.a && var3.b((BaseBlockPosition)(new BlockPosition(this.a(5, 5), this.d(1), this.b(5, 5))))) {
            this.a = true;
            this.a(var1, var3, var2, 5, 1, 5, class_azs.e);
         }

         int var4;
         for(var4 = 6; var4 <= 8; ++var4) {
            if(this.a(var1, var4, 0, -1, var3).getMaterial() == Material.a && this.a(var1, var4, -1, -1, var3).getMaterial() != Material.a) {
               this.a(var1, Blocks.aw.u().set(BlockStairs.a, EnumDirection.NORTH), var4, 0, -1, var3);
            }
         }

         for(var4 = 0; var4 < 7; ++var4) {
            for(int var5 = 0; var5 < 10; ++var5) {
               this.b(var1, var5, 6, var4, var3);
               this.b(var1, Blocks.e.u(), var5, -1, var4, var3);
            }
         }

         this.a(var1, var3, 7, 1, 1, 1);
         return true;
      }

      protected int c(int var1, int var2) {
         return 3;
      }
   }

   public static class class_m_in_class_awj extends WorldGenVillagePieces.WorldGenVillagePiece {
      public class_m_in_class_awj() {
      }

      public class_m_in_class_awj(WorldGenVillagePieces.class_k_in_class_awj var1, int var2, Random var3, StructureBoundingBox var4, EnumDirection var5) {
         super(var1, var2);
         this.a(var5);
         this.l = var4;
      }

      public static WorldGenVillagePieces.class_m_in_class_awj a(WorldGenVillagePieces.class_k_in_class_awj var0, List var1, Random var2, int var3, int var4, int var5, EnumDirection var6, int var7) {
         StructureBoundingBox var8 = StructureBoundingBox.a(var3, var4, var5, 0, 0, 0, 9, 7, 12, var6);
         return a(var8) && StructurePiece.a(var1, var8) == null?new WorldGenVillagePieces.class_m_in_class_awj(var0, var7, var2, var8, var6):null;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.h < 0) {
            this.h = this.b(var1, var3);
            if(this.h < 0) {
               return true;
            }

            this.l.a(0, this.h - this.l.e + 7 - 1, 0);
         }

         this.a(var1, var3, 1, 1, 1, 7, 4, 4, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 2, 1, 6, 8, 4, 10, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 2, 0, 5, 8, 0, 10, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 1, 0, 1, 7, 0, 4, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 0, 0, 0, 0, 3, 5, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 8, 0, 0, 8, 3, 10, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 1, 0, 0, 7, 2, 0, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 1, 0, 5, 2, 1, 5, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 2, 0, 6, 2, 3, 10, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 3, 0, 10, 7, 3, 10, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 1, 2, 0, 7, 3, 0, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 1, 2, 5, 2, 3, 5, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 0, 4, 1, 8, 4, 1, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 0, 4, 4, 3, 4, 4, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 0, 5, 2, 8, 5, 3, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, Blocks.f.u(), 0, 4, 2, var3);
         this.a(var1, Blocks.f.u(), 0, 4, 3, var3);
         this.a(var1, Blocks.f.u(), 8, 4, 2, var3);
         this.a(var1, Blocks.f.u(), 8, 4, 3, var3);
         this.a(var1, Blocks.f.u(), 8, 4, 4, var3);
         IBlockData var4 = Blocks.ad.u().set(BlockStairs.a, EnumDirection.NORTH);
         IBlockData var5 = Blocks.ad.u().set(BlockStairs.a, EnumDirection.SOUTH);
         IBlockData var6 = Blocks.ad.u().set(BlockStairs.a, EnumDirection.WEST);
         IBlockData var7 = Blocks.ad.u().set(BlockStairs.a, EnumDirection.EAST);

         int var8;
         int var9;
         for(var8 = -1; var8 <= 2; ++var8) {
            for(var9 = 0; var9 <= 8; ++var9) {
               this.a(var1, var4, var9, 4 + var8, var8, var3);
               if((var8 > -1 || var9 <= 1) && (var8 > 0 || var9 <= 3) && (var8 > 1 || var9 <= 4 || var9 >= 6)) {
                  this.a(var1, var5, var9, 4 + var8, 5 - var8, var3);
               }
            }
         }

         this.a(var1, var3, 3, 4, 5, 3, 4, 10, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 7, 4, 2, 7, 4, 10, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 4, 5, 4, 4, 5, 10, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 6, 5, 4, 6, 5, 10, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 5, 6, 3, 5, 6, 10, Blocks.f.u(), Blocks.f.u(), false);

         for(var8 = 4; var8 >= 1; --var8) {
            this.a(var1, Blocks.f.u(), var8, 2 + var8, 7 - var8, var3);

            for(var9 = 8 - var8; var9 <= 10; ++var9) {
               this.a(var1, var7, var8, 2 + var8, var9, var3);
            }
         }

         this.a(var1, Blocks.f.u(), 6, 6, 3, var3);
         this.a(var1, Blocks.f.u(), 7, 5, 4, var3);
         this.a(var1, var6, 6, 6, 4, var3);

         for(var8 = 6; var8 <= 8; ++var8) {
            for(var9 = 5; var9 <= 10; ++var9) {
               this.a(var1, var6, var8, 12 - var8, var9, var3);
            }
         }

         this.a(var1, Blocks.r.u(), 0, 2, 1, var3);
         this.a(var1, Blocks.r.u(), 0, 2, 4, var3);
         this.a(var1, Blocks.bj.u(), 0, 2, 2, var3);
         this.a(var1, Blocks.bj.u(), 0, 2, 3, var3);
         this.a(var1, Blocks.r.u(), 4, 2, 0, var3);
         this.a(var1, Blocks.bj.u(), 5, 2, 0, var3);
         this.a(var1, Blocks.r.u(), 6, 2, 0, var3);
         this.a(var1, Blocks.r.u(), 8, 2, 1, var3);
         this.a(var1, Blocks.bj.u(), 8, 2, 2, var3);
         this.a(var1, Blocks.bj.u(), 8, 2, 3, var3);
         this.a(var1, Blocks.r.u(), 8, 2, 4, var3);
         this.a(var1, Blocks.f.u(), 8, 2, 5, var3);
         this.a(var1, Blocks.r.u(), 8, 2, 6, var3);
         this.a(var1, Blocks.bj.u(), 8, 2, 7, var3);
         this.a(var1, Blocks.bj.u(), 8, 2, 8, var3);
         this.a(var1, Blocks.r.u(), 8, 2, 9, var3);
         this.a(var1, Blocks.r.u(), 2, 2, 6, var3);
         this.a(var1, Blocks.bj.u(), 2, 2, 7, var3);
         this.a(var1, Blocks.bj.u(), 2, 2, 8, var3);
         this.a(var1, Blocks.r.u(), 2, 2, 9, var3);
         this.a(var1, Blocks.r.u(), 4, 4, 10, var3);
         this.a(var1, Blocks.bj.u(), 5, 4, 10, var3);
         this.a(var1, Blocks.r.u(), 6, 4, 10, var3);
         this.a(var1, Blocks.f.u(), 5, 5, 10, var3);
         this.a(var1, Blocks.AIR.u(), 2, 1, 0, var3);
         this.a(var1, Blocks.AIR.u(), 2, 2, 0, var3);
         this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.NORTH), 2, 3, 1, var3);
         this.a(var1, var3, var2, 2, 1, 0, EnumDirection.NORTH);
         this.a(var1, var3, 1, 0, -1, 3, 2, -1, Blocks.AIR.u(), Blocks.AIR.u(), false);
         if(this.a(var1, 2, 0, -1, var3).getMaterial() == Material.a && this.a(var1, 2, -1, -1, var3).getMaterial() != Material.a) {
            this.a(var1, var4, 2, 0, -1, var3);
         }

         for(var8 = 0; var8 < 5; ++var8) {
            for(var9 = 0; var9 < 9; ++var9) {
               this.b(var1, var9, 7, var8, var3);
               this.b(var1, Blocks.e.u(), var9, -1, var8, var3);
            }
         }

         for(var8 = 5; var8 < 11; ++var8) {
            for(var9 = 2; var9 < 9; ++var9) {
               this.b(var1, var9, 7, var8, var3);
               this.b(var1, Blocks.e.u(), var9, -1, var8, var3);
            }
         }

         this.a(var1, var3, 4, 1, 2, 2);
         return true;
      }
   }

   public static class class_f_in_class_awj extends WorldGenVillagePieces.WorldGenVillagePiece {
      public class_f_in_class_awj() {
      }

      public class_f_in_class_awj(WorldGenVillagePieces.class_k_in_class_awj var1, int var2, Random var3, StructureBoundingBox var4, EnumDirection var5) {
         super(var1, var2);
         this.a(var5);
         this.l = var4;
      }

      public static WorldGenVillagePieces.class_f_in_class_awj a(WorldGenVillagePieces.class_k_in_class_awj var0, List var1, Random var2, int var3, int var4, int var5, EnumDirection var6, int var7) {
         StructureBoundingBox var8 = StructureBoundingBox.a(var3, var4, var5, 0, 0, 0, 9, 7, 11, var6);
         return a(var8) && StructurePiece.a(var1, var8) == null?new WorldGenVillagePieces.class_f_in_class_awj(var0, var7, var2, var8, var6):null;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.h < 0) {
            this.h = this.b(var1, var3);
            if(this.h < 0) {
               return true;
            }

            this.l.a(0, this.h - this.l.e + 7 - 1, 0);
         }

         this.a(var1, var3, 1, 1, 1, 7, 4, 4, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 2, 1, 6, 8, 4, 10, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 2, 0, 6, 8, 0, 10, Blocks.d.u(), Blocks.d.u(), false);
         this.a(var1, Blocks.e.u(), 6, 0, 6, var3);
         this.a(var1, var3, 2, 1, 6, 2, 1, 10, Blocks.aO.u(), Blocks.aO.u(), false);
         this.a(var1, var3, 8, 1, 6, 8, 1, 10, Blocks.aO.u(), Blocks.aO.u(), false);
         this.a(var1, var3, 3, 1, 10, 7, 1, 10, Blocks.aO.u(), Blocks.aO.u(), false);
         this.a(var1, var3, 1, 0, 1, 7, 0, 4, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 0, 0, 0, 0, 3, 5, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 8, 0, 0, 8, 3, 5, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 1, 0, 0, 7, 1, 0, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 1, 0, 5, 7, 1, 5, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 1, 2, 0, 7, 3, 0, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 1, 2, 5, 7, 3, 5, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 0, 4, 1, 8, 4, 1, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 0, 4, 4, 8, 4, 4, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 0, 5, 2, 8, 5, 3, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, Blocks.f.u(), 0, 4, 2, var3);
         this.a(var1, Blocks.f.u(), 0, 4, 3, var3);
         this.a(var1, Blocks.f.u(), 8, 4, 2, var3);
         this.a(var1, Blocks.f.u(), 8, 4, 3, var3);
         IBlockData var4 = Blocks.ad.u().set(BlockStairs.a, EnumDirection.NORTH);
         IBlockData var5 = Blocks.ad.u().set(BlockStairs.a, EnumDirection.SOUTH);
         IBlockData var6 = Blocks.ad.u().set(BlockStairs.a, EnumDirection.WEST);

         int var7;
         int var8;
         for(var7 = -1; var7 <= 2; ++var7) {
            for(var8 = 0; var8 <= 8; ++var8) {
               this.a(var1, var4, var8, 4 + var7, var7, var3);
               this.a(var1, var5, var8, 4 + var7, 5 - var7, var3);
            }
         }

         this.a(var1, Blocks.r.u(), 0, 2, 1, var3);
         this.a(var1, Blocks.r.u(), 0, 2, 4, var3);
         this.a(var1, Blocks.r.u(), 8, 2, 1, var3);
         this.a(var1, Blocks.r.u(), 8, 2, 4, var3);
         this.a(var1, Blocks.bj.u(), 0, 2, 2, var3);
         this.a(var1, Blocks.bj.u(), 0, 2, 3, var3);
         this.a(var1, Blocks.bj.u(), 8, 2, 2, var3);
         this.a(var1, Blocks.bj.u(), 8, 2, 3, var3);
         this.a(var1, Blocks.bj.u(), 2, 2, 5, var3);
         this.a(var1, Blocks.bj.u(), 3, 2, 5, var3);
         this.a(var1, Blocks.bj.u(), 5, 2, 0, var3);
         this.a(var1, Blocks.bj.u(), 6, 2, 5, var3);
         this.a(var1, Blocks.aO.u(), 2, 1, 3, var3);
         this.a(var1, Blocks.aB.u(), 2, 2, 3, var3);
         this.a(var1, Blocks.f.u(), 1, 1, 4, var3);
         this.a(var1, var4, 2, 1, 4, var3);
         this.a(var1, var6, 1, 1, 3, var3);
         this.a(var1, var3, 5, 0, 1, 7, 0, 3, Blocks.T.u(), Blocks.T.u(), false);
         this.a(var1, Blocks.T.u(), 6, 1, 1, var3);
         this.a(var1, Blocks.T.u(), 6, 1, 2, var3);
         this.a(var1, Blocks.AIR.u(), 2, 1, 0, var3);
         this.a(var1, Blocks.AIR.u(), 2, 2, 0, var3);
         this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.NORTH), 2, 3, 1, var3);
         this.a(var1, var3, var2, 2, 1, 0, EnumDirection.NORTH);
         if(this.a(var1, 2, 0, -1, var3).getMaterial() == Material.a && this.a(var1, 2, -1, -1, var3).getMaterial() != Material.a) {
            this.a(var1, var4, 2, 0, -1, var3);
         }

         this.a(var1, Blocks.AIR.u(), 6, 1, 5, var3);
         this.a(var1, Blocks.AIR.u(), 6, 2, 5, var3);
         this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.SOUTH), 6, 3, 4, var3);
         this.a(var1, var3, var2, 6, 1, 5, EnumDirection.SOUTH);

         for(var7 = 0; var7 < 5; ++var7) {
            for(var8 = 0; var8 < 9; ++var8) {
               this.b(var1, var8, 7, var7, var3);
               this.b(var1, Blocks.e.u(), var8, -1, var7, var3);
            }
         }

         this.a(var1, var3, 4, 1, 2, 2);
         return true;
      }

      protected int c(int var1, int var2) {
         return var1 == 0?4:super.c(var1, var2);
      }
   }

   public static class WorldGenVillageHut extends WorldGenVillagePieces.WorldGenVillagePiece {
      private boolean a;
      private int b;

      public WorldGenVillageHut() {
      }

      public WorldGenVillageHut(WorldGenVillagePieces.class_k_in_class_awj var1, int var2, Random var3, StructureBoundingBox var4, EnumDirection var5) {
         super(var1, var2);
         this.a(var5);
         this.l = var4;
         this.a = var3.nextBoolean();
         this.b = var3.nextInt(3);
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("T", this.b);
         var1.a("C", this.a);
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.b = var1.h("T");
         this.a = var1.p("C");
      }

      public static WorldGenVillagePieces.WorldGenVillageHut a(WorldGenVillagePieces.class_k_in_class_awj var0, List var1, Random var2, int var3, int var4, int var5, EnumDirection var6, int var7) {
         StructureBoundingBox var8 = StructureBoundingBox.a(var3, var4, var5, 0, 0, 0, 4, 6, 5, var6);
         return a(var8) && StructurePiece.a(var1, var8) == null?new WorldGenVillagePieces.WorldGenVillageHut(var0, var7, var2, var8, var6):null;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.h < 0) {
            this.h = this.b(var1, var3);
            if(this.h < 0) {
               return true;
            }

            this.l.a(0, this.h - this.l.e + 6 - 1, 0);
         }

         this.a(var1, var3, 1, 1, 1, 3, 5, 4, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 0, 0, 0, 3, 0, 4, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 1, 0, 1, 2, 0, 3, Blocks.d.u(), Blocks.d.u(), false);
         if(this.a) {
            this.a(var1, var3, 1, 4, 1, 2, 4, 3, Blocks.r.u(), Blocks.r.u(), false);
         } else {
            this.a(var1, var3, 1, 5, 1, 2, 5, 3, Blocks.r.u(), Blocks.r.u(), false);
         }

         this.a(var1, Blocks.r.u(), 1, 4, 0, var3);
         this.a(var1, Blocks.r.u(), 2, 4, 0, var3);
         this.a(var1, Blocks.r.u(), 1, 4, 4, var3);
         this.a(var1, Blocks.r.u(), 2, 4, 4, var3);
         this.a(var1, Blocks.r.u(), 0, 4, 1, var3);
         this.a(var1, Blocks.r.u(), 0, 4, 2, var3);
         this.a(var1, Blocks.r.u(), 0, 4, 3, var3);
         this.a(var1, Blocks.r.u(), 3, 4, 1, var3);
         this.a(var1, Blocks.r.u(), 3, 4, 2, var3);
         this.a(var1, Blocks.r.u(), 3, 4, 3, var3);
         this.a(var1, var3, 0, 1, 0, 0, 3, 0, Blocks.r.u(), Blocks.r.u(), false);
         this.a(var1, var3, 3, 1, 0, 3, 3, 0, Blocks.r.u(), Blocks.r.u(), false);
         this.a(var1, var3, 0, 1, 4, 0, 3, 4, Blocks.r.u(), Blocks.r.u(), false);
         this.a(var1, var3, 3, 1, 4, 3, 3, 4, Blocks.r.u(), Blocks.r.u(), false);
         this.a(var1, var3, 0, 1, 1, 0, 3, 3, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 3, 1, 1, 3, 3, 3, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 1, 1, 0, 2, 3, 0, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 1, 1, 4, 2, 3, 4, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, Blocks.bj.u(), 0, 2, 2, var3);
         this.a(var1, Blocks.bj.u(), 3, 2, 2, var3);
         if(this.b > 0) {
            this.a(var1, Blocks.aO.u(), this.b, 1, 3, var3);
            this.a(var1, Blocks.aB.u(), this.b, 2, 3, var3);
         }

         this.a(var1, Blocks.AIR.u(), 1, 1, 0, var3);
         this.a(var1, Blocks.AIR.u(), 1, 2, 0, var3);
         this.a(var1, var3, var2, 1, 1, 0, EnumDirection.NORTH);
         if(this.a(var1, 1, 0, -1, var3).getMaterial() == Material.a && this.a(var1, 1, -1, -1, var3).getMaterial() != Material.a) {
            this.a(var1, Blocks.aw.u().set(BlockStairs.a, EnumDirection.NORTH), 1, 0, -1, var3);
         }

         for(int var4 = 0; var4 < 5; ++var4) {
            for(int var5 = 0; var5 < 4; ++var5) {
               this.b(var1, var5, 6, var4, var3);
               this.b(var1, Blocks.e.u(), var5, -1, var4, var3);
            }
         }

         this.a(var1, var3, 1, 1, 2, 1);
         return true;
      }
   }

   public static class class_a_in_class_awj extends WorldGenVillagePieces.WorldGenVillagePiece {
      public class_a_in_class_awj() {
      }

      public class_a_in_class_awj(WorldGenVillagePieces.class_k_in_class_awj var1, int var2, Random var3, StructureBoundingBox var4, EnumDirection var5) {
         super(var1, var2);
         this.a(var5);
         this.l = var4;
      }

      public static WorldGenVillagePieces.class_a_in_class_awj a(WorldGenVillagePieces.class_k_in_class_awj var0, List var1, Random var2, int var3, int var4, int var5, EnumDirection var6, int var7) {
         StructureBoundingBox var8 = StructureBoundingBox.a(var3, var4, var5, 0, 0, 0, 9, 9, 6, var6);
         return a(var8) && StructurePiece.a(var1, var8) == null?new WorldGenVillagePieces.class_a_in_class_awj(var0, var7, var2, var8, var6):null;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.h < 0) {
            this.h = this.b(var1, var3);
            if(this.h < 0) {
               return true;
            }

            this.l.a(0, this.h - this.l.e + 9 - 1, 0);
         }

         this.a(var1, var3, 1, 1, 1, 7, 5, 4, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 0, 0, 0, 8, 0, 5, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 0, 5, 0, 8, 5, 5, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 0, 6, 1, 8, 6, 4, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 0, 7, 2, 8, 7, 3, Blocks.e.u(), Blocks.e.u(), false);

         int var5;
         for(int var4 = -1; var4 <= 2; ++var4) {
            for(var5 = 0; var5 <= 8; ++var5) {
               this.a(var1, Blocks.ad.u().set(BlockStairs.a, EnumDirection.NORTH), var5, 6 + var4, var4, var3);
               this.a(var1, Blocks.ad.u().set(BlockStairs.a, EnumDirection.SOUTH), var5, 6 + var4, 5 - var4, var3);
            }
         }

         this.a(var1, var3, 0, 1, 0, 0, 1, 5, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 1, 1, 5, 8, 1, 5, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 8, 1, 0, 8, 1, 4, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 2, 1, 0, 7, 1, 0, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 0, 2, 0, 0, 4, 0, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 0, 2, 5, 0, 4, 5, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 8, 2, 5, 8, 4, 5, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 8, 2, 0, 8, 4, 0, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 0, 2, 1, 0, 4, 4, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 1, 2, 5, 7, 4, 5, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 8, 2, 1, 8, 4, 4, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 1, 2, 0, 7, 4, 0, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, Blocks.bj.u(), 4, 2, 0, var3);
         this.a(var1, Blocks.bj.u(), 5, 2, 0, var3);
         this.a(var1, Blocks.bj.u(), 6, 2, 0, var3);
         this.a(var1, Blocks.bj.u(), 4, 3, 0, var3);
         this.a(var1, Blocks.bj.u(), 5, 3, 0, var3);
         this.a(var1, Blocks.bj.u(), 6, 3, 0, var3);
         this.a(var1, Blocks.bj.u(), 0, 2, 2, var3);
         this.a(var1, Blocks.bj.u(), 0, 2, 3, var3);
         this.a(var1, Blocks.bj.u(), 0, 3, 2, var3);
         this.a(var1, Blocks.bj.u(), 0, 3, 3, var3);
         this.a(var1, Blocks.bj.u(), 8, 2, 2, var3);
         this.a(var1, Blocks.bj.u(), 8, 2, 3, var3);
         this.a(var1, Blocks.bj.u(), 8, 3, 2, var3);
         this.a(var1, Blocks.bj.u(), 8, 3, 3, var3);
         this.a(var1, Blocks.bj.u(), 2, 2, 5, var3);
         this.a(var1, Blocks.bj.u(), 3, 2, 5, var3);
         this.a(var1, Blocks.bj.u(), 5, 2, 5, var3);
         this.a(var1, Blocks.bj.u(), 6, 2, 5, var3);
         this.a(var1, var3, 1, 4, 1, 7, 4, 1, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 1, 4, 4, 7, 4, 4, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 1, 3, 4, 7, 3, 4, Blocks.X.u(), Blocks.X.u(), false);
         this.a(var1, Blocks.f.u(), 7, 1, 4, var3);
         this.a(var1, Blocks.ad.u().set(BlockStairs.a, EnumDirection.EAST), 7, 1, 3, var3);
         IBlockData var7 = Blocks.ad.u().set(BlockStairs.a, EnumDirection.NORTH);
         this.a(var1, var7, 6, 1, 4, var3);
         this.a(var1, var7, 5, 1, 4, var3);
         this.a(var1, var7, 4, 1, 4, var3);
         this.a(var1, var7, 3, 1, 4, var3);
         this.a(var1, Blocks.aO.u(), 6, 1, 3, var3);
         this.a(var1, Blocks.aB.u(), 6, 2, 3, var3);
         this.a(var1, Blocks.aO.u(), 4, 1, 3, var3);
         this.a(var1, Blocks.aB.u(), 4, 2, 3, var3);
         this.a(var1, Blocks.ai.u(), 7, 1, 1, var3);
         this.a(var1, Blocks.AIR.u(), 1, 1, 0, var3);
         this.a(var1, Blocks.AIR.u(), 1, 2, 0, var3);
         this.a(var1, var3, var2, 1, 1, 0, EnumDirection.NORTH);
         if(this.a(var1, 1, 0, -1, var3).getMaterial() == Material.a && this.a(var1, 1, -1, -1, var3).getMaterial() != Material.a) {
            this.a(var1, Blocks.aw.u().set(BlockStairs.a, EnumDirection.NORTH), 1, 0, -1, var3);
         }

         for(var5 = 0; var5 < 6; ++var5) {
            for(int var6 = 0; var6 < 9; ++var6) {
               this.b(var1, var6, 9, var5, var3);
               this.b(var1, Blocks.e.u(), var6, -1, var5, var3);
            }
         }

         this.a(var1, var3, 2, 1, 2, 1);
         return true;
      }

      protected int c(int var1, int var2) {
         return 1;
      }
   }

   public static class class_i_in_class_awj extends WorldGenVillagePieces.WorldGenVillagePiece {
      public class_i_in_class_awj() {
      }

      public class_i_in_class_awj(WorldGenVillagePieces.class_k_in_class_awj var1, int var2, Random var3, StructureBoundingBox var4, EnumDirection var5) {
         super(var1, var2);
         this.a(var5);
         this.l = var4;
      }

      public static WorldGenVillagePieces.class_i_in_class_awj a(WorldGenVillagePieces.class_k_in_class_awj var0, List var1, Random var2, int var3, int var4, int var5, EnumDirection var6, int var7) {
         StructureBoundingBox var8 = StructureBoundingBox.a(var3, var4, var5, 0, 0, 0, 5, 12, 9, var6);
         return a(var8) && StructurePiece.a(var1, var8) == null?new WorldGenVillagePieces.class_i_in_class_awj(var0, var7, var2, var8, var6):null;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.h < 0) {
            this.h = this.b(var1, var3);
            if(this.h < 0) {
               return true;
            }

            this.l.a(0, this.h - this.l.e + 12 - 1, 0);
         }

         this.a(var1, var3, 1, 1, 1, 3, 3, 7, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 1, 5, 1, 3, 9, 3, Blocks.AIR.u(), Blocks.AIR.u(), false);
         this.a(var1, var3, 1, 0, 0, 3, 0, 8, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 1, 1, 0, 3, 10, 0, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 0, 1, 1, 0, 10, 3, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 4, 1, 1, 4, 10, 3, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 0, 0, 4, 0, 4, 7, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 4, 0, 4, 4, 4, 7, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 1, 1, 8, 3, 4, 8, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 1, 5, 4, 3, 10, 4, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 1, 5, 5, 3, 5, 7, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 0, 9, 0, 4, 9, 4, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 0, 4, 0, 4, 4, 4, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, Blocks.e.u(), 0, 11, 2, var3);
         this.a(var1, Blocks.e.u(), 4, 11, 2, var3);
         this.a(var1, Blocks.e.u(), 2, 11, 0, var3);
         this.a(var1, Blocks.e.u(), 2, 11, 4, var3);
         this.a(var1, Blocks.e.u(), 1, 1, 6, var3);
         this.a(var1, Blocks.e.u(), 1, 1, 7, var3);
         this.a(var1, Blocks.e.u(), 2, 1, 7, var3);
         this.a(var1, Blocks.e.u(), 3, 1, 6, var3);
         this.a(var1, Blocks.e.u(), 3, 1, 7, var3);
         IBlockData var4 = Blocks.aw.u().set(BlockStairs.a, EnumDirection.NORTH);
         IBlockData var5 = Blocks.aw.u().set(BlockStairs.a, EnumDirection.WEST);
         IBlockData var6 = Blocks.aw.u().set(BlockStairs.a, EnumDirection.EAST);
         this.a(var1, var4, 1, 1, 5, var3);
         this.a(var1, var4, 2, 1, 6, var3);
         this.a(var1, var4, 3, 1, 5, var3);
         this.a(var1, var5, 1, 2, 7, var3);
         this.a(var1, var6, 3, 2, 7, var3);
         this.a(var1, Blocks.bj.u(), 0, 2, 2, var3);
         this.a(var1, Blocks.bj.u(), 0, 3, 2, var3);
         this.a(var1, Blocks.bj.u(), 4, 2, 2, var3);
         this.a(var1, Blocks.bj.u(), 4, 3, 2, var3);
         this.a(var1, Blocks.bj.u(), 0, 6, 2, var3);
         this.a(var1, Blocks.bj.u(), 0, 7, 2, var3);
         this.a(var1, Blocks.bj.u(), 4, 6, 2, var3);
         this.a(var1, Blocks.bj.u(), 4, 7, 2, var3);
         this.a(var1, Blocks.bj.u(), 2, 6, 0, var3);
         this.a(var1, Blocks.bj.u(), 2, 7, 0, var3);
         this.a(var1, Blocks.bj.u(), 2, 6, 4, var3);
         this.a(var1, Blocks.bj.u(), 2, 7, 4, var3);
         this.a(var1, Blocks.bj.u(), 0, 3, 6, var3);
         this.a(var1, Blocks.bj.u(), 4, 3, 6, var3);
         this.a(var1, Blocks.bj.u(), 2, 3, 8, var3);
         this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.SOUTH), 2, 4, 7, var3);
         this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.EAST), 1, 4, 6, var3);
         this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.WEST), 3, 4, 6, var3);
         this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.NORTH), 2, 4, 5, var3);
         IBlockData var7 = Blocks.au.u().set(class_amj.a, EnumDirection.WEST);

         int var8;
         for(var8 = 1; var8 <= 9; ++var8) {
            this.a(var1, var7, 3, var8, 3, var3);
         }

         this.a(var1, Blocks.AIR.u(), 2, 1, 0, var3);
         this.a(var1, Blocks.AIR.u(), 2, 2, 0, var3);
         this.a(var1, var3, var2, 2, 1, 0, EnumDirection.NORTH);
         if(this.a(var1, 2, 0, -1, var3).getMaterial() == Material.a && this.a(var1, 2, -1, -1, var3).getMaterial() != Material.a) {
            this.a(var1, var4, 2, 0, -1, var3);
         }

         for(var8 = 0; var8 < 9; ++var8) {
            for(int var9 = 0; var9 < 5; ++var9) {
               this.b(var1, var9, 12, var8, var3);
               this.b(var1, Blocks.e.u(), var9, -1, var8, var3);
            }
         }

         this.a(var1, var3, 2, 1, 2, 1);
         return true;
      }

      protected int c(int var1, int var2) {
         return 2;
      }
   }

   public static class WorldGenVillageHouse extends WorldGenVillagePieces.WorldGenVillagePiece {
      private boolean a;

      public WorldGenVillageHouse() {
      }

      public WorldGenVillageHouse(WorldGenVillagePieces.class_k_in_class_awj var1, int var2, Random var3, StructureBoundingBox var4, EnumDirection var5) {
         super(var1, var2);
         this.a(var5);
         this.l = var4;
         this.a = var3.nextBoolean();
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("Terrace", this.a);
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.a = var1.p("Terrace");
      }

      public static WorldGenVillagePieces.WorldGenVillageHouse a(WorldGenVillagePieces.class_k_in_class_awj var0, List var1, Random var2, int var3, int var4, int var5, EnumDirection var6, int var7) {
         StructureBoundingBox var8 = StructureBoundingBox.a(var3, var4, var5, 0, 0, 0, 5, 6, 5, var6);
         return StructurePiece.a(var1, var8) != null?null:new WorldGenVillagePieces.WorldGenVillageHouse(var0, var7, var2, var8, var6);
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.h < 0) {
            this.h = this.b(var1, var3);
            if(this.h < 0) {
               return true;
            }

            this.l.a(0, this.h - this.l.e + 6 - 1, 0);
         }

         this.a(var1, var3, 0, 0, 0, 4, 0, 4, Blocks.e.u(), Blocks.e.u(), false);
         this.a(var1, var3, 0, 4, 0, 4, 4, 4, Blocks.r.u(), Blocks.r.u(), false);
         this.a(var1, var3, 1, 4, 1, 3, 4, 3, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, Blocks.e.u(), 0, 1, 0, var3);
         this.a(var1, Blocks.e.u(), 0, 2, 0, var3);
         this.a(var1, Blocks.e.u(), 0, 3, 0, var3);
         this.a(var1, Blocks.e.u(), 4, 1, 0, var3);
         this.a(var1, Blocks.e.u(), 4, 2, 0, var3);
         this.a(var1, Blocks.e.u(), 4, 3, 0, var3);
         this.a(var1, Blocks.e.u(), 0, 1, 4, var3);
         this.a(var1, Blocks.e.u(), 0, 2, 4, var3);
         this.a(var1, Blocks.e.u(), 0, 3, 4, var3);
         this.a(var1, Blocks.e.u(), 4, 1, 4, var3);
         this.a(var1, Blocks.e.u(), 4, 2, 4, var3);
         this.a(var1, Blocks.e.u(), 4, 3, 4, var3);
         this.a(var1, var3, 0, 1, 1, 0, 3, 3, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 4, 1, 1, 4, 3, 3, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, var3, 1, 1, 4, 3, 3, 4, Blocks.f.u(), Blocks.f.u(), false);
         this.a(var1, Blocks.bj.u(), 0, 2, 2, var3);
         this.a(var1, Blocks.bj.u(), 2, 2, 4, var3);
         this.a(var1, Blocks.bj.u(), 4, 2, 2, var3);
         this.a(var1, Blocks.f.u(), 1, 1, 0, var3);
         this.a(var1, Blocks.f.u(), 1, 2, 0, var3);
         this.a(var1, Blocks.f.u(), 1, 3, 0, var3);
         this.a(var1, Blocks.f.u(), 2, 3, 0, var3);
         this.a(var1, Blocks.f.u(), 3, 3, 0, var3);
         this.a(var1, Blocks.f.u(), 3, 2, 0, var3);
         this.a(var1, Blocks.f.u(), 3, 1, 0, var3);
         if(this.a(var1, 2, 0, -1, var3).getMaterial() == Material.a && this.a(var1, 2, -1, -1, var3).getMaterial() != Material.a) {
            this.a(var1, Blocks.aw.u().set(BlockStairs.a, EnumDirection.NORTH), 2, 0, -1, var3);
         }

         this.a(var1, var3, 1, 1, 1, 3, 3, 3, Blocks.AIR.u(), Blocks.AIR.u(), false);
         if(this.a) {
            this.a(var1, Blocks.aO.u(), 0, 5, 0, var3);
            this.a(var1, Blocks.aO.u(), 1, 5, 0, var3);
            this.a(var1, Blocks.aO.u(), 2, 5, 0, var3);
            this.a(var1, Blocks.aO.u(), 3, 5, 0, var3);
            this.a(var1, Blocks.aO.u(), 4, 5, 0, var3);
            this.a(var1, Blocks.aO.u(), 0, 5, 4, var3);
            this.a(var1, Blocks.aO.u(), 1, 5, 4, var3);
            this.a(var1, Blocks.aO.u(), 2, 5, 4, var3);
            this.a(var1, Blocks.aO.u(), 3, 5, 4, var3);
            this.a(var1, Blocks.aO.u(), 4, 5, 4, var3);
            this.a(var1, Blocks.aO.u(), 4, 5, 1, var3);
            this.a(var1, Blocks.aO.u(), 4, 5, 2, var3);
            this.a(var1, Blocks.aO.u(), 4, 5, 3, var3);
            this.a(var1, Blocks.aO.u(), 0, 5, 1, var3);
            this.a(var1, Blocks.aO.u(), 0, 5, 2, var3);
            this.a(var1, Blocks.aO.u(), 0, 5, 3, var3);
         }

         if(this.a) {
            IBlockData var4 = Blocks.au.u().set(class_amj.a, EnumDirection.SOUTH);
            this.a(var1, var4, 3, 1, 3, var3);
            this.a(var1, var4, 3, 2, 3, var3);
            this.a(var1, var4, 3, 3, 3, var3);
            this.a(var1, var4, 3, 4, 3, var3);
         }

         this.a(var1, Blocks.aa.u().set(class_ape.a, EnumDirection.NORTH), 2, 3, 1, var3);

         for(int var6 = 0; var6 < 5; ++var6) {
            for(int var5 = 0; var5 < 5; ++var5) {
               this.b(var1, var5, 6, var6, var3);
               this.b(var1, Blocks.e.u(), var5, -1, var6, var3);
            }
         }

         this.a(var1, var3, 1, 1, 2, 1);
         return true;
      }
   }

   public static class WorldGenVillageRoad extends WorldGenVillagePieces.class_o_in_class_awj {
      private int a;

      public WorldGenVillageRoad() {
      }

      public WorldGenVillageRoad(WorldGenVillagePieces.class_k_in_class_awj var1, int var2, Random var3, StructureBoundingBox var4, EnumDirection var5) {
         super(var1, var2);
         this.a(var5);
         this.l = var4;
         this.a = Math.max(var4.c(), var4.e());
      }

      protected void a(NBTTagCompound var1) {
         super.a(var1);
         var1.a("Length", this.a);
      }

      protected void b(NBTTagCompound var1) {
         super.b(var1);
         this.a = var1.h("Length");
      }

      public void a(StructurePiece var1, List var2, Random var3) {
         boolean var4 = false;

         int var5;
         StructurePiece var6;
         for(var5 = var3.nextInt(5); var5 < this.a - 8; var5 += 2 + var3.nextInt(5)) {
            var6 = this.a((WorldGenVillagePieces.class_k_in_class_awj)var1, var2, var3, 0, var5);
            if(var6 != null) {
               var5 += Math.max(var6.l.c(), var6.l.e());
               var4 = true;
            }
         }

         for(var5 = var3.nextInt(5); var5 < this.a - 8; var5 += 2 + var3.nextInt(5)) {
            var6 = this.b((WorldGenVillagePieces.class_k_in_class_awj)var1, var2, var3, 0, var5);
            if(var6 != null) {
               var5 += Math.max(var6.l.c(), var6.l.e());
               var4 = true;
            }
         }

         EnumDirection var7 = this.e();
         if(var4 && var3.nextInt(3) > 0 && var7 != null) {
            switch(WorldGenVillagePieces.SyntheticClass_1.a[var7.ordinal()]) {
            case 1:
               WorldGenVillagePieces.e((WorldGenVillagePieces.class_k_in_class_awj)var1, var2, var3, this.l.a - 1, this.l.b, this.l.c, EnumDirection.WEST, this.d());
               break;
            case 2:
               WorldGenVillagePieces.e((WorldGenVillagePieces.class_k_in_class_awj)var1, var2, var3, this.l.a - 1, this.l.b, this.l.f - 2, EnumDirection.WEST, this.d());
               break;
            case 3:
               WorldGenVillagePieces.e((WorldGenVillagePieces.class_k_in_class_awj)var1, var2, var3, this.l.a, this.l.b, this.l.c - 1, EnumDirection.NORTH, this.d());
               break;
            case 4:
               WorldGenVillagePieces.e((WorldGenVillagePieces.class_k_in_class_awj)var1, var2, var3, this.l.d - 2, this.l.b, this.l.c - 1, EnumDirection.NORTH, this.d());
            }
         }

         if(var4 && var3.nextInt(3) > 0 && var7 != null) {
            switch(WorldGenVillagePieces.SyntheticClass_1.a[var7.ordinal()]) {
            case 1:
               WorldGenVillagePieces.e((WorldGenVillagePieces.class_k_in_class_awj)var1, var2, var3, this.l.d + 1, this.l.b, this.l.c, EnumDirection.EAST, this.d());
               break;
            case 2:
               WorldGenVillagePieces.e((WorldGenVillagePieces.class_k_in_class_awj)var1, var2, var3, this.l.d + 1, this.l.b, this.l.f - 2, EnumDirection.EAST, this.d());
               break;
            case 3:
               WorldGenVillagePieces.e((WorldGenVillagePieces.class_k_in_class_awj)var1, var2, var3, this.l.a, this.l.b, this.l.f + 1, EnumDirection.SOUTH, this.d());
               break;
            case 4:
               WorldGenVillagePieces.e((WorldGenVillagePieces.class_k_in_class_awj)var1, var2, var3, this.l.d - 2, this.l.b, this.l.f + 1, EnumDirection.SOUTH, this.d());
            }
         }

      }

      public static StructureBoundingBox a(WorldGenVillagePieces.class_k_in_class_awj var0, List var1, Random var2, int var3, int var4, int var5, EnumDirection var6) {
         for(int var7 = 7 * MathHelper.a((Random)var2, 3, 5); var7 >= 7; var7 -= 7) {
            StructureBoundingBox var8 = StructureBoundingBox.a(var3, var4, var5, 0, 0, 0, 3, 3, var7, var6);
            if(StructurePiece.a(var1, var8) == null) {
               return var8;
            }
         }

         return null;
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         IBlockData var4 = this.a(Blocks.n.u());
         IBlockData var5 = this.a(Blocks.e.u());

         for(int var6 = this.l.a; var6 <= this.l.d; ++var6) {
            for(int var7 = this.l.c; var7 <= this.l.f; ++var7) {
               BlockPosition var8 = new BlockPosition(var6, 64, var7);
               if(var3.b((BaseBlockPosition)var8)) {
                  var8 = var1.q(var8).b();
                  var1.a((BlockPosition)var8, (IBlockData)var4, 2);
                  var1.a((BlockPosition)var8.b(), (IBlockData)var5, 2);
               }
            }
         }

         return true;
      }
   }

   public abstract static class class_o_in_class_awj extends WorldGenVillagePieces.WorldGenVillagePiece {
      public class_o_in_class_awj() {
      }

      protected class_o_in_class_awj(WorldGenVillagePieces.class_k_in_class_awj var1, int var2) {
         super(var1, var2);
      }
   }

   public static class class_k_in_class_awj extends WorldGenVillagePieces.class_p_in_class_awj {
      public class_aij a;
      public boolean b;
      public int c;
      public WorldGenVillagePieces.class_e_in_class_awj d;
      public List e;
      public List f = Lists.newArrayList();
      public List g = Lists.newArrayList();

      public class_k_in_class_awj() {
      }

      public class_k_in_class_awj(class_aij var1, int var2, Random var3, int var4, int var5, List var6, int var7) {
         super((WorldGenVillagePieces.class_k_in_class_awj)null, 0, var3, var4, var5);
         this.a = var1;
         this.e = var6;
         this.c = var7;
         BiomeBase var8 = var1.a(new BlockPosition(var4, 0, var5), class_aik.b);
         this.b = var8 == class_aik.d || var8 == class_aik.s;
         this.a(this.b);
      }

      public class_aij h() {
         return this.a;
      }
   }

   public static class class_p_in_class_awj extends WorldGenVillagePieces.WorldGenVillagePiece {
      public class_p_in_class_awj() {
      }

      public class_p_in_class_awj(WorldGenVillagePieces.class_k_in_class_awj var1, int var2, Random var3, int var4, int var5) {
         super(var1, var2);
         this.a(EnumDirection.EnumDirectionLimit.HORIZONTAL.a(var3));
         if(this.e().k() == EnumDirection.class_a_in_class_cq.Z) {
            this.l = new StructureBoundingBox(var4, 64, var5, var4 + 6 - 1, 78, var5 + 6 - 1);
         } else {
            this.l = new StructureBoundingBox(var4, 64, var5, var4 + 6 - 1, 78, var5 + 6 - 1);
         }

      }

      public void a(StructurePiece var1, List var2, Random var3) {
         WorldGenVillagePieces.e((WorldGenVillagePieces.class_k_in_class_awj)var1, var2, var3, this.l.a - 1, this.l.e - 4, this.l.c + 1, EnumDirection.WEST, this.d());
         WorldGenVillagePieces.e((WorldGenVillagePieces.class_k_in_class_awj)var1, var2, var3, this.l.d + 1, this.l.e - 4, this.l.c + 1, EnumDirection.EAST, this.d());
         WorldGenVillagePieces.e((WorldGenVillagePieces.class_k_in_class_awj)var1, var2, var3, this.l.a + 1, this.l.e - 4, this.l.c - 1, EnumDirection.NORTH, this.d());
         WorldGenVillagePieces.e((WorldGenVillagePieces.class_k_in_class_awj)var1, var2, var3, this.l.a + 1, this.l.e - 4, this.l.f + 1, EnumDirection.SOUTH, this.d());
      }

      public boolean a(World var1, Random var2, StructureBoundingBox var3) {
         if(this.h < 0) {
            this.h = this.b(var1, var3);
            if(this.h < 0) {
               return true;
            }

            this.l.a(0, this.h - this.l.e + 3, 0);
         }

         this.a(var1, var3, 1, 0, 1, 4, 12, 4, Blocks.e.u(), Blocks.i.u(), false);
         this.a(var1, Blocks.AIR.u(), 2, 12, 2, var3);
         this.a(var1, Blocks.AIR.u(), 3, 12, 2, var3);
         this.a(var1, Blocks.AIR.u(), 2, 12, 3, var3);
         this.a(var1, Blocks.AIR.u(), 3, 12, 3, var3);
         this.a(var1, Blocks.aO.u(), 1, 13, 1, var3);
         this.a(var1, Blocks.aO.u(), 1, 14, 1, var3);
         this.a(var1, Blocks.aO.u(), 4, 13, 1, var3);
         this.a(var1, Blocks.aO.u(), 4, 14, 1, var3);
         this.a(var1, Blocks.aO.u(), 1, 13, 4, var3);
         this.a(var1, Blocks.aO.u(), 1, 14, 4, var3);
         this.a(var1, Blocks.aO.u(), 4, 13, 4, var3);
         this.a(var1, Blocks.aO.u(), 4, 14, 4, var3);
         this.a(var1, var3, 1, 15, 1, 4, 15, 4, Blocks.e.u(), Blocks.e.u(), false);

         for(int var4 = 0; var4 <= 5; ++var4) {
            for(int var5 = 0; var5 <= 5; ++var5) {
               if(var5 == 0 || var5 == 5 || var4 == 0 || var4 == 5) {
                  this.a(var1, Blocks.n.u(), var5, 11, var4, var3);
                  this.b(var1, var5, 12, var4, var3);
               }
            }
         }

         return true;
      }
   }

   abstract static class WorldGenVillagePiece extends StructurePiece {
      protected int h = -1;
      private int a;
      private boolean b;

      public WorldGenVillagePiece() {
      }

      protected WorldGenVillagePiece(WorldGenVillagePieces.class_k_in_class_awj var1, int var2) {
         super(var2);
         if(var1 != null) {
            this.b = var1.b;
         }

      }

      protected void a(NBTTagCompound var1) {
         var1.a("HPos", this.h);
         var1.a("VCount", this.a);
         var1.a("Desert", this.b);
      }

      protected void b(NBTTagCompound var1) {
         this.h = var1.h("HPos");
         this.a = var1.h("VCount");
         this.b = var1.p("Desert");
      }

      protected StructurePiece a(WorldGenVillagePieces.class_k_in_class_awj var1, List var2, Random var3, int var4, int var5) {
         EnumDirection var6 = this.e();
         if(var6 != null) {
            switch(WorldGenVillagePieces.SyntheticClass_1.a[var6.ordinal()]) {
            case 1:
               return WorldGenVillagePieces.d(var1, var2, var3, this.l.a - 1, this.l.b + var4, this.l.c + var5, EnumDirection.WEST, this.d());
            case 2:
               return WorldGenVillagePieces.d(var1, var2, var3, this.l.a - 1, this.l.b + var4, this.l.c + var5, EnumDirection.WEST, this.d());
            case 3:
               return WorldGenVillagePieces.d(var1, var2, var3, this.l.a + var5, this.l.b + var4, this.l.c - 1, EnumDirection.NORTH, this.d());
            case 4:
               return WorldGenVillagePieces.d(var1, var2, var3, this.l.a + var5, this.l.b + var4, this.l.c - 1, EnumDirection.NORTH, this.d());
            }
         }

         return null;
      }

      protected StructurePiece b(WorldGenVillagePieces.class_k_in_class_awj var1, List var2, Random var3, int var4, int var5) {
         EnumDirection var6 = this.e();
         if(var6 != null) {
            switch(WorldGenVillagePieces.SyntheticClass_1.a[var6.ordinal()]) {
            case 1:
               return WorldGenVillagePieces.d(var1, var2, var3, this.l.d + 1, this.l.b + var4, this.l.c + var5, EnumDirection.EAST, this.d());
            case 2:
               return WorldGenVillagePieces.d(var1, var2, var3, this.l.d + 1, this.l.b + var4, this.l.c + var5, EnumDirection.EAST, this.d());
            case 3:
               return WorldGenVillagePieces.d(var1, var2, var3, this.l.a + var5, this.l.b + var4, this.l.f + 1, EnumDirection.SOUTH, this.d());
            case 4:
               return WorldGenVillagePieces.d(var1, var2, var3, this.l.a + var5, this.l.b + var4, this.l.f + 1, EnumDirection.SOUTH, this.d());
            }
         }

         return null;
      }

      protected int b(World var1, StructureBoundingBox var2) {
         int var3 = 0;
         int var4 = 0;
         BlockPosition.class_a_in_class_cj var5 = new BlockPosition.class_a_in_class_cj();

         for(int var6 = this.l.c; var6 <= this.l.f; ++var6) {
            for(int var7 = this.l.a; var7 <= this.l.d; ++var7) {
               var5.c(var7, 64, var6);
               if(var2.b((BaseBlockPosition)var5)) {
                  var3 += Math.max(var1.q(var5).q(), var1.s.i());
                  ++var4;
               }
            }
         }

         if(var4 == 0) {
            return -1;
         } else {
            return var3 / var4;
         }
      }

      protected static boolean a(StructureBoundingBox var0) {
         return var0 != null && var0.b > 10;
      }

      protected void a(World var1, StructureBoundingBox var2, int var3, int var4, int var5, int var6) {
         if(this.a < var6) {
            for(int var7 = this.a; var7 < var6; ++var7) {
               int var8 = this.a(var3 + var7, var5);
               int var9 = this.d(var4);
               int var10 = this.b(var3 + var7, var5);
               if(!var2.b((BaseBlockPosition)(new BlockPosition(var8, var9, var10)))) {
                  break;
               }

               ++this.a;
               class_zd var11 = new class_zd(var1);
               var11.b((double)var8 + 0.5D, (double)var9, (double)var10 + 0.5D, 0.0F, 0.0F);
               var11.a(var1.D(new BlockPosition(var11)), (class_sc)null);
               var11.l(this.c(var7, var11.cZ()));
               var1.a((Entity)var11);
            }

         }
      }

      protected int c(int var1, int var2) {
         return var2;
      }

      protected IBlockData a(IBlockData var1) {
         if(this.b) {
            if(var1.getBlock() == Blocks.r || var1.getBlock() == Blocks.s) {
               return Blocks.A.u();
            }

            if(var1.getBlock() == Blocks.e) {
               return Blocks.A.a(BlockSandStone.EnumSandstoneVariant.DEFAULT.a());
            }

            if(var1.getBlock() == Blocks.f) {
               return Blocks.A.a(BlockSandStone.EnumSandstoneVariant.SMOOTH.a());
            }

            if(var1.getBlock() == Blocks.ad) {
               return Blocks.bO.u().set(BlockStairs.a, var1.get(BlockStairs.a));
            }

            if(var1.getBlock() == Blocks.aw) {
               return Blocks.bO.u().set(BlockStairs.a, var1.get(BlockStairs.a));
            }

            if(var1.getBlock() == Blocks.n) {
               return Blocks.A.u();
            }
         }

         return var1;
      }

      protected void a(World var1, IBlockData var2, int var3, int var4, int var5, StructureBoundingBox var6) {
         IBlockData var7 = this.a(var2);
         super.a(var1, var7, var3, var4, var5, var6);
      }

      protected void a(World var1, StructureBoundingBox var2, int var3, int var4, int var5, int var6, int var7, int var8, IBlockData var9, IBlockData var10, boolean var11) {
         IBlockData var12 = this.a(var9);
         IBlockData var13 = this.a(var10);
         super.a(var1, var2, var3, var4, var5, var6, var7, var8, var12, var13, var11);
      }

      protected void b(World var1, IBlockData var2, int var3, int var4, int var5, StructureBoundingBox var6) {
         IBlockData var7 = this.a(var2);
         super.b(var1, var7, var3, var4, var5, var6);
      }

      protected void a(boolean var1) {
         this.b = var1;
      }
   }

   public static class class_e_in_class_awj {
      public Class a;
      public final int b;
      public int c;
      public int d;

      public class_e_in_class_awj(Class var1, int var2, int var3) {
         this.a = var1;
         this.b = var2;
         this.d = var3;
      }

      public boolean a(int var1) {
         return this.d == 0 || this.c < this.d;
      }

      public boolean a() {
         return this.d == 0 || this.c < this.d;
      }
   }
}
