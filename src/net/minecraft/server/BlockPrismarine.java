package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.class_di;
import net.minecraft.server.class_or;

public class BlockPrismarine extends Block {
   public static final BlockStateEnum a = BlockStateEnum.a("variant", BlockPrismarine.EnumPrismarineVariant.class);
   public static final int b = BlockPrismarine.EnumPrismarineVariant.ROUGH.a();
   public static final int c = BlockPrismarine.EnumPrismarineVariant.BRICKS.a();
   public static final int d = BlockPrismarine.EnumPrismarineVariant.DARK.a();

   public BlockPrismarine() {
      super(Material.e);
      this.w(this.A.b().set(a, BlockPrismarine.EnumPrismarineVariant.ROUGH));
      this.a(CreativeModeTab.b);
   }

   public String c() {
      return class_di.a(this.a() + "." + BlockPrismarine.EnumPrismarineVariant.ROUGH.c() + ".name");
   }

   public MaterialMapColor r(IBlockData var1) {
      return var1.get(a) == BlockPrismarine.EnumPrismarineVariant.ROUGH?MaterialMapColor.y:MaterialMapColor.G;
   }

   public int d(IBlockData var1) {
      return ((BlockPrismarine.EnumPrismarineVariant)var1.get(a)).a();
   }

   public int e(IBlockData var1) {
      return ((BlockPrismarine.EnumPrismarineVariant)var1.get(a)).a();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   public IBlockData a(int var1) {
      return this.u().set(a, BlockPrismarine.EnumPrismarineVariant.a(var1));
   }

   public static enum EnumPrismarineVariant implements class_or {
      ROUGH(0, "prismarine", "rough"),
      BRICKS(1, "prismarine_bricks", "bricks"),
      DARK(2, "dark_prismarine", "dark");

      private static final BlockPrismarine.EnumPrismarineVariant[] d = new BlockPrismarine.EnumPrismarineVariant[values().length];
      private final int e;
      private final String f;
      private final String g;

      private EnumPrismarineVariant(int var3, String var4, String var5) {
         this.e = var3;
         this.f = var4;
         this.g = var5;
      }

      public int a() {
         return this.e;
      }

      public String toString() {
         return this.f;
      }

      public static BlockPrismarine.EnumPrismarineVariant a(int var0) {
         if(var0 < 0 || var0 >= d.length) {
            var0 = 0;
         }

         return d[var0];
      }

      public String m() {
         return this.f;
      }

      public String c() {
         return this.g;
      }

      static {
         BlockPrismarine.EnumPrismarineVariant[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            BlockPrismarine.EnumPrismarineVariant var3 = var0[var2];
            d[var3.a()] = var3;
         }

      }
   }
}
