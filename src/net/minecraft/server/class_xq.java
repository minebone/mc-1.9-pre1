package net.minecraft.server;

import com.google.common.base.Predicate;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_akq;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_xz;
import net.minecraft.server.class_yc;
import org.apache.commons.lang3.Validate;

public abstract class class_xq extends Entity {
   private static final Predicate c = new Predicate() {
      public boolean a(Entity var1) {
         return var1 instanceof class_xq;
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((Entity)var1);
      }
   };
   private int d;
   protected BlockPosition a;
   public EnumDirection b;

   public class_xq(World var1) {
      super(var1);
      this.a(0.5F, 0.5F);
   }

   public class_xq(World var1, BlockPosition var2) {
      this(var1);
      this.a = var2;
   }

   protected void i() {
   }

   protected void a(EnumDirection var1) {
      Validate.notNull(var1);
      Validate.isTrue(var1.k().c());
      this.b = var1;
      this.lastYaw = this.yaw = (float)(this.b.b() * 90);
      this.j();
   }

   protected void j() {
      if(this.b != null) {
         double var1 = (double)this.a.p() + 0.5D;
         double var3 = (double)this.a.q() + 0.5D;
         double var5 = (double)this.a.r() + 0.5D;
         double var7 = 0.46875D;
         double var9 = this.a(this.l());
         double var11 = this.a(this.n());
         var1 -= (double)this.b.g() * 0.46875D;
         var5 -= (double)this.b.i() * 0.46875D;
         var3 += var11;
         EnumDirection var13 = this.b.f();
         var1 += var9 * (double)var13.g();
         var5 += var9 * (double)var13.i();
         this.locX = var1;
         this.locY = var3;
         this.locZ = var5;
         double var14 = (double)this.l();
         double var16 = (double)this.n();
         double var18 = (double)this.l();
         if(this.b.k() == EnumDirection.class_a_in_class_cq.Z) {
            var18 = 1.0D;
         } else {
            var14 = 1.0D;
         }

         var14 /= 32.0D;
         var16 /= 32.0D;
         var18 /= 32.0D;
         this.a((AxisAlignedBB)(new AxisAlignedBB(var1 - var14, var3 - var16, var5 - var18, var1 + var14, var3 + var16, var5 + var18)));
      }
   }

   private double a(int var1) {
      return var1 % 32 == 0?0.5D:0.0D;
   }

   public void m() {
      this.lastX = this.locX;
      this.lastY = this.locY;
      this.lastZ = this.locZ;
      if(this.d++ == 100 && !this.world.E) {
         this.d = 0;
         if(!this.dead && !this.k()) {
            this.S();
            this.a((Entity)null);
         }
      }

   }

   public boolean k() {
      if(!this.world.a((Entity)this, (AxisAlignedBB)this.bk()).isEmpty()) {
         return false;
      } else {
         int var1 = Math.max(1, this.l() / 16);
         int var2 = Math.max(1, this.n() / 16);
         BlockPosition var3 = this.a.a(this.b.d());
         EnumDirection var4 = this.b.f();

         for(int var5 = 0; var5 < var1; ++var5) {
            for(int var6 = 0; var6 < var2; ++var6) {
               int var7 = var1 > 2?-1:0;
               int var8 = var2 > 2?-1:0;
               BlockPosition var9 = var3.a(var4, var5 + var7).b(var6 + var8);
               IBlockData var10 = this.world.getType(var9);
               if(!var10.getMaterial().a() && !class_akq.B(var10)) {
                  return false;
               }
            }
         }

         return this.world.a((Entity)this, (AxisAlignedBB)this.bk(), (Predicate)c).isEmpty();
      }
   }

   public boolean ao() {
      return true;
   }

   public boolean t(Entity var1) {
      return var1 instanceof EntityHuman?this.a(DamageSource.a((EntityHuman)var1), 0.0F):false;
   }

   public EnumDirection bh() {
      return this.b;
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b(var1)) {
         return false;
      } else {
         if(!this.dead && !this.world.E) {
            this.S();
            this.an();
            this.a(var1.j());
         }

         return true;
      }
   }

   public void d(double var1, double var3, double var5) {
      if(!this.world.E && !this.dead && var1 * var1 + var3 * var3 + var5 * var5 > 0.0D) {
         this.S();
         this.a((Entity)null);
      }

   }

   public void g(double var1, double var3, double var5) {
      if(!this.world.E && !this.dead && var1 * var1 + var3 * var3 + var5 * var5 > 0.0D) {
         this.S();
         this.a((Entity)null);
      }

   }

   public void b(NBTTagCompound var1) {
      var1.a("Facing", (byte)this.b.b());
      BlockPosition var2 = this.q();
      var1.a("TileX", var2.p());
      var1.a("TileY", var2.q());
      var1.a("TileZ", var2.r());
   }

   public void a(NBTTagCompound var1) {
      this.a = new BlockPosition(var1.h("TileX"), var1.h("TileY"), var1.h("TileZ"));
      this.a(EnumDirection.b(var1.f("Facing")));
   }

   public abstract int l();

   public abstract int n();

   public abstract void a(Entity var1);

   public abstract void o();

   public class_yc a(ItemStack var1, float var2) {
      class_yc var3 = new class_yc(this.world, this.locX + (double)((float)this.b.g() * 0.15F), this.locY + (double)var2, this.locZ + (double)((float)this.b.i() * 0.15F), var1);
      var3.q();
      this.world.a((Entity)var3);
      return var3;
   }

   protected boolean aq() {
      return false;
   }

   public void b(double var1, double var3, double var5) {
      this.a = new BlockPosition(var1, var3, var5);
      this.j();
      this.ai = true;
   }

   public BlockPosition q() {
      return this.a;
   }

   public float a(class_aod var1) {
      if(this.b != null && this.b.k() != EnumDirection.class_a_in_class_cq.Y) {
         switch(class_xq.SyntheticClass_1.a[var1.ordinal()]) {
         case 1:
            this.b = this.b.d();
            break;
         case 2:
            this.b = this.b.f();
            break;
         case 3:
            this.b = this.b.e();
         }
      }

      return super.a(var1);
   }

   public float a(class_amq var1) {
      return this.a(var1.a(this.b));
   }

   public void a(class_xz var1) {
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[class_aod.values().length];

      static {
         try {
            a[class_aod.CLOCKWISE_180.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[class_aod.CLOCKWISE_90.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
