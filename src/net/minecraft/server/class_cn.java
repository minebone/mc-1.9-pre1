package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_akt;
import net.minecraft.server.class_ck;
import net.minecraft.server.class_cr;
import net.minecraft.server.class_cz;
import net.minecraft.server.class_yc;

public class class_cn implements class_cr {
   public final ItemStack a(class_ck var1, ItemStack var2) {
      ItemStack var3 = this.b(var1, var2);
      this.a(var1);
      this.a(var1, class_akt.e(var1.f()));
      return var3;
   }

   protected ItemStack b(class_ck var1, ItemStack var2) {
      EnumDirection var3 = class_akt.e(var1.f());
      class_cz var4 = class_akt.a(var1);
      ItemStack var5 = var2.a(1);
      a(var1.i(), var5, 6, var3, var4);
      return var2;
   }

   public static void a(World var0, ItemStack var1, int var2, EnumDirection var3, class_cz var4) {
      double var5 = var4.a();
      double var7 = var4.b();
      double var9 = var4.c();
      if(var3.k() == EnumDirection.class_a_in_class_cq.Y) {
         var7 -= 0.125D;
      } else {
         var7 -= 0.15625D;
      }

      class_yc var11 = new class_yc(var0, var5, var7, var9, var1);
      double var12 = var0.r.nextDouble() * 0.1D + 0.2D;
      var11.motX = (double)var3.g() * var12;
      var11.motY = 0.20000000298023224D;
      var11.motZ = (double)var3.i() * var12;
      var11.motX += var0.r.nextGaussian() * 0.007499999832361937D * (double)var2;
      var11.motY += var0.r.nextGaussian() * 0.007499999832361937D * (double)var2;
      var11.motZ += var0.r.nextGaussian() * 0.007499999832361937D * (double)var2;
      var0.a((Entity)var11);
   }

   protected void a(class_ck var1) {
      var1.i().b(1000, var1.d(), 0);
   }

   protected void a(class_ck var1, EnumDirection var2) {
      var1.i().b(2000, var1.d(), this.a(var2));
   }

   private int a(EnumDirection var1) {
      return var1.g() + 1 + (var1.i() + 1) * 3;
   }
}
