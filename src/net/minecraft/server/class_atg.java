package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Chunk;
import net.minecraft.server.EnumCreatureType;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arx;
import net.minecraft.server.class_ate;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;

public class class_atg implements class_arx {
   private static final List c = Lists.newArrayList();
   private static final int d;
   private static final int e;
   protected static final IBlockData a = Blocks.AIR.u();
   protected static final IBlockData b = Blocks.cv.u();
   private final World f;

   public class_atg(World var1) {
      this.f = var1;
   }

   public Chunk a(int var1, int var2) {
      class_ate var3 = new class_ate();

      int var7;
      for(int var4 = 0; var4 < 16; ++var4) {
         for(int var5 = 0; var5 < 16; ++var5) {
            int var6 = var1 * 16 + var4;
            var7 = var2 * 16 + var5;
            var3.a(var4, 60, var5, b);
            IBlockData var8 = c(var6, var7);
            if(var8 != null) {
               var3.a(var4, 70, var5, var8);
            }
         }
      }

      Chunk var9 = new Chunk(this.f, var3, var1, var2);
      var9.b();
      BiomeBase[] var10 = this.f.A().b((BiomeBase[])null, var1 * 16, var2 * 16, 16, 16);
      byte[] var11 = var9.l();

      for(var7 = 0; var7 < var11.length; ++var7) {
         var11[var7] = (byte)BiomeBase.a(var10[var7]);
      }

      var9.b();
      return var9;
   }

   public static IBlockData c(int var0, int var1) {
      IBlockData var2 = a;
      if(var0 > 0 && var1 > 0 && var0 % 2 != 0 && var1 % 2 != 0) {
         var0 /= 2;
         var1 /= 2;
         if(var0 <= d && var1 <= e) {
            int var3 = MathHelper.a(var0 * d + var1);
            if(var3 < c.size()) {
               var2 = (IBlockData)c.get(var3);
            }
         }
      }

      return var2;
   }

   public void b(int var1, int var2) {
   }

   public boolean a(Chunk var1, int var2, int var3) {
      return false;
   }

   public List a(EnumCreatureType var1, BlockPosition var2) {
      BiomeBase var3 = this.f.b(var2);
      return var3.a(var1);
   }

   public BlockPosition a(World var1, String var2, BlockPosition var3) {
      return null;
   }

   public void b(Chunk var1, int var2, int var3) {
   }

   static {
      Iterator var0 = Block.h.iterator();

      while(var0.hasNext()) {
         Block var1 = (Block)var0.next();
         c.addAll(var1.t().a());
      }

      d = MathHelper.f(MathHelper.c((float)c.size()));
      e = MathHelper.f((float)c.size() / (float)d);
   }
}
