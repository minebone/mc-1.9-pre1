package net.minecraft.server;

import net.minecraft.server.Enchantment;
import net.minecraft.server.class_agm;
import net.minecraft.server.EnumInventorySlot;

public class class_agv extends Enchantment {
   public class_agv(Enchantment.class_a_in_class_agl var1, EnumInventorySlot... var2) {
      super(var1, class_agm.BREAKABLE, var2);
      this.c("mending");
   }

   public int a(int var1) {
      return var1 * 25;
   }

   public int b(int var1) {
      return this.a(var1) + 50;
   }

   public boolean e() {
      return true;
   }

   public int b() {
      return 1;
   }
}
