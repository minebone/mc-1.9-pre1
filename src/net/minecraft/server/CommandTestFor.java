package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.Entity;
import net.minecraft.server.GameProfileSerializer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MojangsonParser;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ec;
import net.minecraft.server.ICommandListener;

public class CommandTestFor extends CommandAbstract {
   public String c() {
      return "testfor";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.testfor.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 1) {
         throw new class_cf("commands.testfor.usage", new Object[0]);
      } else {
         Entity var4 = b(var1, var2, var3[0]);
         NBTTagCompound var5 = null;
         if(var3.length >= 2) {
            try {
               var5 = MojangsonParser.a(a(var3, 1));
            } catch (class_ec var7) {
               throw new class_bz("commands.testfor.tagError", new Object[]{var7.getMessage()});
            }
         }

         if(var5 != null) {
            NBTTagCompound var6 = a(var4);
            if(!GameProfileSerializer.a(var5, var6, true)) {
               throw new class_bz("commands.testfor.failure", new Object[]{var4.h_()});
            }
         }

         a(var2, this, "commands.testfor.success", new Object[]{var4.h_()});
      }
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 0;
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, var1.J()):Collections.emptyList();
   }
}
