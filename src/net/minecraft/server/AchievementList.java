package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.List;
import net.minecraft.server.Achievement;
import net.minecraft.server.Blocks;
import net.minecraft.server.Items;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_nv;

public class AchievementList {
   public static int a;
   public static int b;
   public static int c;
   public static int d;
   public static final List e = Lists.newArrayList();
   public static final Achievement f = (new Achievement("achievement.openInventory", "openInventory", 0, 0, Items.aS, (Achievement)null)).a().c();
   public static final Achievement g = (new Achievement("achievement.mineWood", "mineWood", 2, 1, Blocks.r, f)).c();
   public static final Achievement h = (new Achievement("achievement.buildWorkBench", "buildWorkBench", 4, -1, Blocks.ai, g)).c();
   public static final Achievement i = (new Achievement("achievement.buildPickaxe", "buildPickaxe", 4, 2, Items.q, h)).c();
   public static final Achievement j = (new Achievement("achievement.buildFurnace", "buildFurnace", 3, 4, Blocks.al, i)).c();
   public static final Achievement k = (new Achievement("achievement.acquireIron", "acquireIron", 1, 4, Items.l, j)).c();
   public static final Achievement l = (new Achievement("achievement.buildHoe", "buildHoe", 2, -3, Items.K, h)).c();
   public static final Achievement m = (new Achievement("achievement.makeBread", "makeBread", -1, -3, Items.R, l)).c();
   public static final Achievement n = (new Achievement("achievement.bakeCake", "bakeCake", 0, -5, Items.bg, l)).c();
   public static final Achievement o = (new Achievement("achievement.buildBetterPickaxe", "buildBetterPickaxe", 6, 2, Items.u, i)).c();
   public static final Achievement p = (new Achievement("achievement.cookFish", "cookFish", 2, 6, Items.bc, j)).c();
   public static final Achievement q = (new Achievement("achievement.onARail", "onARail", 2, 3, Blocks.av, k)).b().c();
   public static final Achievement r = (new Achievement("achievement.buildSword", "buildSword", 6, -1, Items.o, h)).c();
   public static final Achievement s = (new Achievement("achievement.killEnemy", "killEnemy", 8, -1, Items.be, r)).c();
   public static final Achievement t = (new Achievement("achievement.killCow", "killCow", 7, -3, Items.aM, r)).c();
   public static final Achievement u = (new Achievement("achievement.flyPig", "flyPig", 9, -3, Items.aC, t)).b().c();
   public static final Achievement v = (new Achievement("achievement.snipeSkeleton", "snipeSkeleton", 7, 0, Items.f, s)).b().c();
   public static final Achievement w = (new Achievement("achievement.diamonds", "diamonds", -1, 5, Blocks.ag, k)).c();
   public static final Achievement x = (new Achievement("achievement.diamondsToYou", "diamondsToYou", -1, 2, Items.k, w)).c();
   public static final Achievement y = (new Achievement("achievement.portal", "portal", -1, 7, Blocks.Z, w)).c();
   public static final Achievement z = (new Achievement("achievement.ghast", "ghast", -4, 8, Items.bD, y)).b().c();
   public static final Achievement A = (new Achievement("achievement.blazeRod", "blazeRod", 0, 9, Items.bC, y)).c();
   public static final Achievement B = (new Achievement("achievement.potion", "potion", 2, 8, Items.bG, A)).c();
   public static final Achievement C = (new Achievement("achievement.theEnd", "theEnd", 3, 10, Items.bR, A)).b().c();
   public static final Achievement D = (new Achievement("achievement.theEnd2", "theEnd2", 4, 13, Blocks.bI, C)).b().c();
   public static final Achievement E = (new Achievement("achievement.enchantments", "enchantments", -4, 4, Blocks.bC, w)).c();
   public static final Achievement F = (new Achievement("achievement.overkill", "overkill", -4, 1, Items.w, E)).b().c();
   public static final Achievement G = (new Achievement("achievement.bookcase", "bookcase", -3, 6, Blocks.X, E)).c();
   public static final Achievement H = (new Achievement("achievement.breedCow", "breedCow", 7, -5, Items.Q, t)).c();
   public static final Achievement I = (new Achievement("achievement.spawnWither", "spawnWither", 7, 12, new ItemStack(Items.ch, 1, 1), D)).c();
   public static final Achievement J = (new Achievement("achievement.killWither", "killWither", 7, 10, Items.cj, I)).c();
   public static final Achievement K = (new Achievement("achievement.fullBeacon", "fullBeacon", 7, 8, Blocks.bY, J)).b().c();
   public static final Achievement L = (new Achievement("achievement.exploreAllBiomes", "exploreAllBiomes", 4, 8, Items.ah, C)).a(class_nv.class).b().c();
   public static final Achievement M = (new Achievement("achievement.overpowered", "overpowered", 6, 4, new ItemStack(Items.aq, 1, 1), o)).b().c();

   public static void a() {
   }
}
