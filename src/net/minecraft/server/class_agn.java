package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Enchantment;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumMonsterType;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.SystemUtils;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ago;
import net.minecraft.server.class_agp;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_ov;
import net.minecraft.server.class_rz;

public class class_agn {
   private static final Random a = new Random();
   private static final class_agn.class_e_in_class_agn b = new class_agn.class_e_in_class_agn();
   private static final class_agn.class_d_in_class_agn c = new class_agn.class_d_in_class_agn();
   private static final class_agn.class_b_in_class_agn d = new class_agn.class_b_in_class_agn();
   private static final class_agn.class_a_in_class_agn e = new class_agn.class_a_in_class_agn();

   public static int a(Enchantment var0, ItemStack var1) {
      if(var1 == null) {
         return 0;
      } else {
         NBTTagList var2 = var1.p();
         if(var2 == null) {
            return 0;
         } else {
            for(int var3 = 0; var3 < var2.c(); ++var3) {
               Enchantment var4 = Enchantment.c(var2.b(var3).g("id"));
               short var5 = var2.b(var3).g("lvl");
               if(var4 == var0) {
                  return var5;
               }
            }

            return 0;
         }
      }
   }

   public static Map a(ItemStack var0) {
      LinkedHashMap var1 = Maps.newLinkedHashMap();
      NBTTagList var2 = var0.b() == Items.cn?Items.cn.h(var0):var0.p();
      if(var2 != null) {
         for(int var3 = 0; var3 < var2.c(); ++var3) {
            Enchantment var4 = Enchantment.c(var2.b(var3).g("id"));
            short var5 = var2.b(var3).g("lvl");
            var1.put(var4, Integer.valueOf(var5));
         }
      }

      return var1;
   }

   public static void a(Map var0, ItemStack var1) {
      NBTTagList var2 = new NBTTagList();
      Iterator var3 = var0.entrySet().iterator();

      while(var3.hasNext()) {
         Entry var4 = (Entry)var3.next();
         Enchantment var5 = (Enchantment)var4.getKey();
         if(var5 != null) {
            int var6 = ((Integer)var4.getValue()).intValue();
            NBTTagCompound var7 = new NBTTagCompound();
            var7.a("id", (short)Enchantment.b(var5));
            var7.a("lvl", (short)var6);
            var2.a((NBTTag)var7);
            if(var1.b() == Items.cn) {
               Items.cn.a(var1, new class_ago(var5, var6));
            }
         }
      }

      if(var2.c_()) {
         if(var1.n()) {
            var1.o().q("ench");
         }
      } else if(var1.b() != Items.cn) {
         var1.a((String)"ench", (NBTTag)var2);
      }

   }

   private static void a(class_agn.class_c_in_class_agn var0, ItemStack var1) {
      if(var1 != null) {
         NBTTagList var2 = var1.p();
         if(var2 != null) {
            for(int var3 = 0; var3 < var2.c(); ++var3) {
               short var4 = var2.b(var3).g("id");
               short var5 = var2.b(var3).g("lvl");
               if(Enchantment.c(var4) != null) {
                  var0.a(Enchantment.c(var4), var5);
               }
            }

         }
      }
   }

   private static void a(class_agn.class_c_in_class_agn var0, Iterable var1) {
      Iterator var2 = var1.iterator();

      while(var2.hasNext()) {
         ItemStack var3 = (ItemStack)var2.next();
         a(var0, var3);
      }

   }

   public static int a(Iterable var0, DamageSource var1) {
      b.a = 0;
      b.b = var1;
      a((class_agn.class_c_in_class_agn)b, (Iterable)var0);
      return b.a;
   }

   public static float a(ItemStack var0, EnumMonsterType var1) {
      c.a = 0.0F;
      c.b = var1;
      a((class_agn.class_c_in_class_agn)c, (ItemStack)var0);
      return c.a;
   }

   public static void a(class_rz var0, Entity var1) {
      d.b = var1;
      d.a = var0;
      if(var0 != null) {
         a((class_agn.class_c_in_class_agn)d, (Iterable)var0.aF());
      }

      if(var1 instanceof EntityHuman) {
         a((class_agn.class_c_in_class_agn)d, (ItemStack)var0.ca());
      }

   }

   public static void b(class_rz var0, Entity var1) {
      e.a = var0;
      e.b = var1;
      if(var0 != null) {
         a((class_agn.class_c_in_class_agn)e, (Iterable)var0.aF());
      }

      if(var0 instanceof EntityHuman) {
         a((class_agn.class_c_in_class_agn)e, (ItemStack)var0.ca());
      }

   }

   public static int a(Enchantment var0, class_rz var1) {
      Iterable var2 = var0.a(var1);
      if(var2 == null) {
         return 0;
      } else {
         int var3 = 0;
         Iterator var4 = var2.iterator();

         while(var4.hasNext()) {
            ItemStack var5 = (ItemStack)var4.next();
            int var6 = a(var0, var5);
            if(var6 > var3) {
               var3 = var6;
            }
         }

         return var3;
      }
   }

   public static int a(class_rz var0) {
      return a(class_agp.n, var0);
   }

   public static int b(class_rz var0) {
      return a(class_agp.o, var0);
   }

   public static int c(class_rz var0) {
      return a(class_agp.f, var0);
   }

   public static int d(class_rz var0) {
      return a(class_agp.i, var0);
   }

   public static int e(class_rz var0) {
      return a(class_agp.q, var0);
   }

   public static int f(class_rz var0) {
      return a(class_agp.y, var0);
   }

   public static int g(class_rz var0) {
      return a(class_agp.z, var0);
   }

   public static int h(class_rz var0) {
      return a(class_agp.p, var0);
   }

   public static boolean i(class_rz var0) {
      return a(class_agp.g, var0) > 0;
   }

   public static ItemStack b(Enchantment var0, class_rz var1) {
      Iterable var2 = var0.a(var1);
      if(var2 == null) {
         return null;
      } else {
         ArrayList var3 = Lists.newArrayList();
         Iterator var4 = var2.iterator();

         while(var4.hasNext()) {
            ItemStack var5 = (ItemStack)var4.next();
            if(var5 != null && a(var0, var5) > 0) {
               var3.add(var5);
            }
         }

         return var3.isEmpty()?null:(ItemStack)var3.get(var1.bE().nextInt(var3.size()));
      }
   }

   public static int a(Random var0, int var1, int var2, ItemStack var3) {
      Item var4 = var3.b();
      int var5 = var4.c();
      if(var5 <= 0) {
         return 0;
      } else {
         if(var2 > 15) {
            var2 = 15;
         }

         int var6 = var0.nextInt(8) + 1 + (var2 >> 1) + var0.nextInt(var2 + 1);
         return var1 == 0?Math.max(var6 / 3, 1):(var1 == 1?var6 * 2 / 3 + 1:Math.max(var6, var2 * 2));
      }
   }

   public static ItemStack a(Random var0, ItemStack var1, int var2, boolean var3) {
      boolean var4 = var1.b() == Items.aS;
      List var5 = b(var0, var1, var2, var3);
      if(var4) {
         var1.a((Item)Items.cn);
      }

      Iterator var6 = var5.iterator();

      while(var6.hasNext()) {
         class_ago var7 = (class_ago)var6.next();
         if(var4) {
            Items.cn.a(var1, var7);
         } else {
            var1.a(var7.b, var7.c);
         }
      }

      return var1;
   }

   public static List b(Random var0, ItemStack var1, int var2, boolean var3) {
      ArrayList var4 = Lists.newArrayList();
      Item var5 = var1.b();
      int var6 = var5.c();
      if(var6 <= 0) {
         return var4;
      } else {
         var2 += 1 + var0.nextInt(var6 / 4 + 1) + var0.nextInt(var6 / 4 + 1);
         float var7 = (var0.nextFloat() + var0.nextFloat() - 1.0F) * 0.15F;
         var2 = MathHelper.a(Math.round((float)var2 + (float)var2 * var7), 1, Integer.MAX_VALUE);
         List var8 = a(var2, var1, var3);
         if(!var8.isEmpty()) {
            var4.add(class_ov.a(var0, var8));

            while(var0.nextInt(50) <= var2) {
               a(var8, (class_ago)SystemUtils.a(var4));
               if(var8.isEmpty()) {
                  break;
               }

               var4.add(class_ov.a(var0, var8));
               var2 /= 2;
            }
         }

         return var4;
      }
   }

   public static void a(List var0, class_ago var1) {
      Iterator var2 = var0.iterator();

      while(var2.hasNext()) {
         if(!var1.b.a(((class_ago)var2.next()).b)) {
            var2.remove();
         }
      }

   }

   public static List a(int var0, ItemStack var1, boolean var2) {
      ArrayList var3 = Lists.newArrayList();
      Item var4 = var1.b();
      boolean var5 = var1.b() == Items.aS;
      Iterator var6 = Enchantment.b.iterator();

      while(true) {
         while(true) {
            Enchantment var7;
            do {
               do {
                  if(!var6.hasNext()) {
                     return var3;
                  }

                  var7 = (Enchantment)var6.next();
               } while(var7.e() && !var2);
            } while(!var7.c.a(var4) && !var5);

            for(int var8 = var7.b(); var8 > var7.d() - 1; --var8) {
               if(var0 >= var7.a(var8) && var0 <= var7.b(var8)) {
                  var3.add(new class_ago(var7, var8));
                  break;
               }
            }
         }
      }
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
   }

   static final class class_a_in_class_agn implements class_agn.class_c_in_class_agn {
      public class_rz a;
      public Entity b;

      private class_a_in_class_agn() {
      }

      public void a(Enchantment var1, int var2) {
         var1.a(this.a, this.b, var2);
      }

      // $FF: synthetic method
      class_a_in_class_agn(class_agn.SyntheticClass_1 var1) {
         this();
      }
   }

   static final class class_b_in_class_agn implements class_agn.class_c_in_class_agn {
      public class_rz a;
      public Entity b;

      private class_b_in_class_agn() {
      }

      public void a(Enchantment var1, int var2) {
         var1.b(this.a, this.b, var2);
      }

      // $FF: synthetic method
      class_b_in_class_agn(class_agn.SyntheticClass_1 var1) {
         this();
      }
   }

   static final class class_d_in_class_agn implements class_agn.class_c_in_class_agn {
      public float a;
      public EnumMonsterType b;

      private class_d_in_class_agn() {
      }

      public void a(Enchantment var1, int var2) {
         this.a += var1.a(var2, this.b);
      }

      // $FF: synthetic method
      class_d_in_class_agn(class_agn.SyntheticClass_1 var1) {
         this();
      }
   }

   static final class class_e_in_class_agn implements class_agn.class_c_in_class_agn {
      public int a;
      public DamageSource b;

      private class_e_in_class_agn() {
      }

      public void a(Enchantment var1, int var2) {
         this.a += var1.a(var2, this.b);
      }

      // $FF: synthetic method
      class_e_in_class_agn(class_agn.SyntheticClass_1 var1) {
         this();
      }
   }

   interface class_c_in_class_agn {
      void a(Enchantment var1, int var2);
   }
}
