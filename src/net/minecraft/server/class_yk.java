package net.minecraft.server;

import com.google.common.base.Predicate;
import net.minecraft.server.Entity;
import net.minecraft.server.class_rp;

public interface class_yk extends class_rp {
   Predicate d = new Predicate() {
      public boolean a(Entity var1) {
         return var1 instanceof class_yk;
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((Entity)var1);
      }
   };
   Predicate e = new Predicate() {
      public boolean a(Entity var1) {
         return var1 instanceof class_yk && !var1.aM();
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((Entity)var1);
      }
   };
}
