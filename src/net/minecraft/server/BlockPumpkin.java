package net.minecraft.server;

import com.google.common.base.Predicate;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.BlockStatePredicate;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.ShapeDetector;
import net.minecraft.server.ShapeDetectorBuilder;
import net.minecraft.server.World;
import net.minecraft.server.class_amf;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_are;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_we;
import net.minecraft.server.class_wg;

public class BlockPumpkin extends class_amf {
   private ShapeDetector a;
   private ShapeDetector b;
   private ShapeDetector c;
   private ShapeDetector d;
   private static final Predicate e = new Predicate() {
      public boolean a(IBlockData var1) {
         return var1 != null && (var1.getBlock() == Blocks.aU || var1.getBlock() == Blocks.aZ);
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((IBlockData)var1);
      }
   };

   protected BlockPumpkin() {
      super(Material.C, MaterialMapColor.q);
      this.w(this.A.b().set(D, EnumDirection.NORTH));
      this.a(true);
      this.a(CreativeModeTab.b);
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      super.c(var1, var2, var3);
      this.c(var1, var2);
   }

   public boolean b(World var1, BlockPosition var2) {
      return this.e().a(var1, var2) != null || this.h().a(var1, var2) != null;
   }

   private void c(World var1, BlockPosition var2) {
      ShapeDetector.ShapeDetectorCollection var3;
      int var4;
      int var6;
      if((var3 = this.g().a(var1, var2)) != null) {
         for(var4 = 0; var4 < this.g().b(); ++var4) {
            class_are var5 = var3.a(0, var4, 0);
            var1.a((BlockPosition)var5.d(), (IBlockData)Blocks.AIR.u(), 2);
         }

         class_we var9 = new class_we(var1);
         BlockPosition var10 = var3.a(0, 2, 0).d();
         var9.b((double)var10.p() + 0.5D, (double)var10.q() + 0.05D, (double)var10.r() + 0.5D, 0.0F, 0.0F);
         var1.a((Entity)var9);

         for(var6 = 0; var6 < 120; ++var6) {
            var1.a(EnumParticle.SNOW_SHOVEL, (double)var10.p() + var1.r.nextDouble(), (double)var10.q() + var1.r.nextDouble() * 2.5D, (double)var10.r() + var1.r.nextDouble(), 0.0D, 0.0D, 0.0D, new int[0]);
         }

         for(var6 = 0; var6 < this.g().b(); ++var6) {
            class_are var7 = var3.a(0, var6, 0);
            var1.c(var7.d(), Blocks.AIR);
         }
      } else if((var3 = this.i().a(var1, var2)) != null) {
         for(var4 = 0; var4 < this.i().c(); ++var4) {
            for(int var12 = 0; var12 < this.i().b(); ++var12) {
               var1.a((BlockPosition)var3.a(var4, var12, 0).d(), (IBlockData)Blocks.AIR.u(), 2);
            }
         }

         BlockPosition var11 = var3.a(1, 2, 0).d();
         class_wg var13 = new class_wg(var1);
         var13.o(true);
         var13.b((double)var11.p() + 0.5D, (double)var11.q() + 0.05D, (double)var11.r() + 0.5D, 0.0F, 0.0F);
         var1.a((Entity)var13);

         for(var6 = 0; var6 < 120; ++var6) {
            var1.a(EnumParticle.SNOWBALL, (double)var11.p() + var1.r.nextDouble(), (double)var11.q() + var1.r.nextDouble() * 3.9D, (double)var11.r() + var1.r.nextDouble(), 0.0D, 0.0D, 0.0D, new int[0]);
         }

         for(var6 = 0; var6 < this.i().c(); ++var6) {
            for(int var14 = 0; var14 < this.i().b(); ++var14) {
               class_are var8 = var3.a(var6, var14, 0);
               var1.c(var8.d(), Blocks.AIR);
            }
         }
      }

   }

   public boolean a(World var1, BlockPosition var2) {
      return var1.getType(var2).getBlock().x.j() && var1.getType(var2.b()).q();
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(D, var2.a((EnumDirection)var1.get(D)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(D)));
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return this.u().set(D, var8.bh().d());
   }

   public IBlockData a(int var1) {
      return this.u().set(D, EnumDirection.b(var1));
   }

   public int e(IBlockData var1) {
      return ((EnumDirection)var1.get(D)).b();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{D});
   }

   protected ShapeDetector e() {
      if(this.a == null) {
         this.a = ShapeDetectorBuilder.a().a(new String[]{" ", "#", "#"}).a('#', class_are.a((Predicate)BlockStatePredicate.a(Blocks.aJ))).b();
      }

      return this.a;
   }

   protected ShapeDetector g() {
      if(this.b == null) {
         this.b = ShapeDetectorBuilder.a().a(new String[]{"^", "#", "#"}).a('^', class_are.a(e)).a('#', class_are.a((Predicate)BlockStatePredicate.a(Blocks.aJ))).b();
      }

      return this.b;
   }

   protected ShapeDetector h() {
      if(this.c == null) {
         this.c = ShapeDetectorBuilder.a().a(new String[]{"~ ~", "###", "~#~"}).a('#', class_are.a((Predicate)BlockStatePredicate.a(Blocks.S))).a('~', class_are.a((Predicate)BlockStatePredicate.a(Blocks.AIR))).b();
      }

      return this.c;
   }

   protected ShapeDetector i() {
      if(this.d == null) {
         this.d = ShapeDetectorBuilder.a().a(new String[]{"~^~", "###", "~#~"}).a('^', class_are.a(e)).a('#', class_are.a((Predicate)BlockStatePredicate.a(Blocks.S))).a('~', class_are.a((Predicate)BlockStatePredicate.a(Blocks.AIR))).b();
      }

      return this.d;
   }
}
