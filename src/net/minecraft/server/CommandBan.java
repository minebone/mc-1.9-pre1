package net.minecraft.server;

import com.mojang.authlib.GameProfile;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.class_mp;
import net.minecraft.server.class_ms;

public class CommandBan extends CommandAbstract {
   public String c() {
      return "ban";
   }

   public int a() {
      return 3;
   }

   public String b(ICommandListener var1) {
      return "commands.ban.usage";
   }

   public boolean a(MinecraftServer var1, ICommandListener var2) {
      return var1.getPlayerList().h().b() && super.a(var1, var2);
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length >= 1 && var3[0].length() > 0) {
         GameProfile var4 = var1.aA().a(var3[0]);
         if(var4 == null) {
            throw new class_bz("commands.ban.failed", new Object[]{var3[0]});
         } else {
            String var5 = null;
            if(var3.length >= 2) {
               var5 = a(var2, var3, 1).c();
            }

            class_ms var6 = new class_ms(var4, (Date)null, var2.h_(), (Date)null, var5);
            var1.getPlayerList().h().a((class_mp)var6);
            EntityPlayer var7 = var1.getPlayerList().a(var3[0]);
            if(var7 != null) {
               var7.a.c("You are banned from this server.");
            }

            a(var2, this, "commands.ban.success", new Object[]{var3[0]});
         }
      } else {
         throw new class_cf("commands.ban.usage", new Object[0]);
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length >= 1?a(var3, var1.J()):Collections.emptyList();
   }
}
