package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockSand;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.class_or;

public class BlockRedSandstone extends Block {
   public static final BlockStateEnum a = BlockStateEnum.a("type", BlockRedSandstone.EnumRedSandstoneVariant.class);

   public BlockRedSandstone() {
      super(Material.e, BlockSand.EnumSandVariant.RED_SAND.c());
      this.w(this.A.b().set(a, BlockRedSandstone.EnumRedSandstoneVariant.DEFAULT));
      this.a(CreativeModeTab.b);
   }

   public int d(IBlockData var1) {
      return ((BlockRedSandstone.EnumRedSandstoneVariant)var1.get(a)).a();
   }

   public IBlockData a(int var1) {
      return this.u().set(a, BlockRedSandstone.EnumRedSandstoneVariant.a(var1));
   }

   public int e(IBlockData var1) {
      return ((BlockRedSandstone.EnumRedSandstoneVariant)var1.get(a)).a();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   public static enum EnumRedSandstoneVariant implements class_or {
      DEFAULT(0, "red_sandstone", "default"),
      CHISELED(1, "chiseled_red_sandstone", "chiseled"),
      SMOOTH(2, "smooth_red_sandstone", "smooth");

      private static final BlockRedSandstone.EnumRedSandstoneVariant[] d = new BlockRedSandstone.EnumRedSandstoneVariant[values().length];
      private final int e;
      private final String f;
      private final String g;

      private EnumRedSandstoneVariant(int var3, String var4, String var5) {
         this.e = var3;
         this.f = var4;
         this.g = var5;
      }

      public int a() {
         return this.e;
      }

      public String toString() {
         return this.f;
      }

      public static BlockRedSandstone.EnumRedSandstoneVariant a(int var0) {
         if(var0 < 0 || var0 >= d.length) {
            var0 = 0;
         }

         return d[var0];
      }

      public String m() {
         return this.f;
      }

      public String c() {
         return this.g;
      }

      static {
         BlockRedSandstone.EnumRedSandstoneVariant[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            BlockRedSandstone.EnumRedSandstoneVariant var3 = var0[var2];
            d[var3.a()] = var3;
         }

      }
   }
}
