package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_auc;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_avb extends class_auc {
   private Block a;

   public class_avb(Block var1) {
      this.a = var1;
   }

   public boolean b(World var1, Random var2, BlockPosition var3) {
      if(var1.getType(var3.a()).getBlock() != Blocks.b) {
         return false;
      } else if(var1.getType(var3.b()).getBlock() != Blocks.b) {
         return false;
      } else if(var1.getType(var3).getMaterial() != Material.a && var1.getType(var3).getBlock() != Blocks.b) {
         return false;
      } else {
         int var4 = 0;
         if(var1.getType(var3.e()).getBlock() == Blocks.b) {
            ++var4;
         }

         if(var1.getType(var3.f()).getBlock() == Blocks.b) {
            ++var4;
         }

         if(var1.getType(var3.c()).getBlock() == Blocks.b) {
            ++var4;
         }

         if(var1.getType(var3.d()).getBlock() == Blocks.b) {
            ++var4;
         }

         int var5 = 0;
         if(var1.d(var3.e())) {
            ++var5;
         }

         if(var1.d(var3.f())) {
            ++var5;
         }

         if(var1.d(var3.c())) {
            ++var5;
         }

         if(var1.d(var3.d())) {
            ++var5;
         }

         if(var4 == 3 && var5 == 1) {
            var1.a((BlockPosition)var3, (IBlockData)this.a.u(), 2);
            var1.a(this.a, var3, var2);
         }

         return true;
      }
   }
}
