package net.minecraft.server;

import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityCreature;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityTypes;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aet;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.EnumHand;

public abstract class class_rn extends EntityCreature {
   private static final class_ke bv = DataWatcher.a(class_rn.class, class_kg.h);
   protected int a;
   protected int b;
   protected int c;
   private float bw = -1.0F;
   private float bx;

   public class_rn(World var1) {
      super(var1);
   }

   public abstract class_rn a(class_rn var1);

   public boolean a(EntityHuman var1, EnumHand var2, ItemStack var3) {
      if(var3 != null && var3.b() == Items.bT) {
         if(!this.world.E) {
            Class var4 = EntityTypes.a(EntityTypes.a(class_aet.h(var3)));
            if(var4 != null && this.getClass() == var4) {
               class_rn var5 = this.a(this);
               if(var5 != null) {
                  var5.b_(-24000);
                  var5.b(this.locX, this.locY, this.locZ, 0.0F, 0.0F);
                  this.world.a((Entity)var5);
                  if(var3.s()) {
                     var5.c(var3.q());
                  }

                  if(!var1.abilities.d) {
                     --var3.b;
                  }
               }
            }
         }

         return true;
      } else {
         return false;
      }
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)bv, (Object)Boolean.valueOf(false));
   }

   public int l() {
      return this.world.E?(((Boolean)this.datawatcher.a(bv)).booleanValue()?-1:1):this.a;
   }

   public void a(int var1, boolean var2) {
      int var3 = this.l();
      int var4 = var3;
      var3 += var1 * 20;
      if(var3 > 0) {
         var3 = 0;
         if(var4 < 0) {
            this.o();
         }
      }

      int var5 = var3 - var4;
      this.b_(var3);
      if(var2) {
         this.b += var5;
         if(this.c == 0) {
            this.c = 40;
         }
      }

      if(this.l() == 0) {
         this.b_(this.b);
      }

   }

   public void a(int var1) {
      this.a(var1, false);
   }

   public void b_(int var1) {
      this.datawatcher.b(bv, Boolean.valueOf(var1 < 0));
      this.a = var1;
      this.a(this.m_());
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("Age", this.l());
      var1.a("ForcedAge", this.b);
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.b_(var1.h("Age"));
      this.b = var1.h("ForcedAge");
   }

   public void a(class_ke var1) {
      if(bv.equals(var1)) {
         this.a(this.m_());
      }

      super.a(var1);
   }

   public void n() {
      super.n();
      if(this.world.E) {
         if(this.c > 0) {
            if(this.c % 4 == 0) {
               this.world.a(EnumParticle.VILLAGER_HAPPY, this.locX + (double)(this.random.nextFloat() * this.width * 2.0F) - (double)this.width, this.locY + 0.5D + (double)(this.random.nextFloat() * this.length), this.locZ + (double)(this.random.nextFloat() * this.width * 2.0F) - (double)this.width, 0.0D, 0.0D, 0.0D, new int[0]);
            }

            --this.c;
         }
      } else {
         int var1 = this.l();
         if(var1 < 0) {
            ++var1;
            this.b_(var1);
            if(var1 == 0) {
               this.o();
            }
         } else if(var1 > 0) {
            --var1;
            this.b_(var1);
         }
      }

   }

   protected void o() {
   }

   public boolean m_() {
      return this.l() < 0;
   }

   public void a(boolean var1) {
      this.a(var1?0.5F:1.0F);
   }

   protected final void a(float var1, float var2) {
      boolean var3 = this.bw > 0.0F;
      this.bw = var1;
      this.bx = var2;
      if(!var3) {
         this.a(1.0F);
      }

   }

   protected final void a(float var1) {
      super.a(this.bw * var1, this.bx * var1);
   }
}
