package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Map;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EntityWitch;
import net.minecraft.server.class_vt;
import net.minecraft.server.class_vw;
import net.minecraft.server.class_vx;
import net.minecraft.server.class_vz;
import net.minecraft.server.class_wa;
import net.minecraft.server.class_wb;
import net.minecraft.server.class_wc;
import net.minecraft.server.class_wd;
import net.minecraft.server.class_we;
import net.minecraft.server.class_wf;
import net.minecraft.server.class_wg;
import net.minecraft.server.class_wi;
import net.minecraft.server.class_wj;
import net.minecraft.server.class_wt;
import net.minecraft.server.class_xn;
import net.minecraft.server.class_yf;
import net.minecraft.server.class_yg;
import net.minecraft.server.class_yh;
import net.minecraft.server.class_yi;
import net.minecraft.server.class_yj;
import net.minecraft.server.class_yl;
import net.minecraft.server.class_ym;
import net.minecraft.server.class_yn;
import net.minecraft.server.class_yo;
import net.minecraft.server.class_yq;
import net.minecraft.server.class_yu;
import net.minecraft.server.class_yv;
import net.minecraft.server.class_yw;
import net.minecraft.server.class_yx;
import net.minecraft.server.class_yz;
import net.minecraft.server.class_zd;

public class class_sd {
   private static final Map a = Maps.newHashMap();

   public static EntityInsentient.EnumEntityPositionType a(Class var0) {
      return (EntityInsentient.EnumEntityPositionType)a.get(var0);
   }

   static {
      a.put(class_vt.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_vw.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_vx.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_wj.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_vz.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_wa.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_wb.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_wc.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_wd.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_we.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_wf.class, EntityInsentient.EnumEntityPositionType.IN_WATER);
      a.put(class_wg.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_wi.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_zd.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_wt.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_xn.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_yf.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_yg.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_yh.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_yi.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_yj.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_yl.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_ym.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_yn.class, EntityInsentient.EnumEntityPositionType.IN_WATER);
      a.put(class_yo.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_yq.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_yu.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_yv.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_yw.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_yx.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(EntityWitch.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
      a.put(class_yz.class, EntityInsentient.EnumEntityPositionType.ON_GROUND);
   }
}
