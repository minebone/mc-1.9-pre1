package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.minecraft.server.AchievementList;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityAnimal;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Statistic;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.class_rn;
import net.minecraft.server.class_rw;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_vx;

public class class_tc extends class_tj {
   private EntityAnimal d;
   World a;
   private EntityAnimal e;
   int b;
   double c;

   public class_tc(EntityAnimal var1, double var2) {
      this.d = var1;
      this.a = var1.world;
      this.c = var2;
      this.a(3);
   }

   public boolean a() {
      if(!this.d.df()) {
         return false;
      } else {
         this.e = this.f();
         return this.e != null;
      }
   }

   public boolean b() {
      return this.e.at() && this.e.df() && this.b < 60;
   }

   public void d() {
      this.e = null;
      this.b = 0;
   }

   public void e() {
      this.d.t().a(this.e, 10.0F, (float)this.d.cD());
      this.d.x().a((Entity)this.e, this.c);
      ++this.b;
      if(this.b >= 60 && this.d.h(this.e) < 9.0D) {
         this.i();
      }

   }

   private EntityAnimal f() {
      List var1 = this.a.a(this.d.getClass(), this.d.bk().g(8.0D));
      double var2 = Double.MAX_VALUE;
      EntityAnimal var4 = null;
      Iterator var5 = var1.iterator();

      while(var5.hasNext()) {
         EntityAnimal var6 = (EntityAnimal)var5.next();
         if(this.d.a(var6) && this.d.h(var6) < var2) {
            var4 = var6;
            var2 = this.d.h(var6);
         }
      }

      return var4;
   }

   private void i() {
      class_rn var1 = this.d.a((class_rn)this.e);
      if(var1 != null) {
         EntityHuman var2 = this.d.de();
         if(var2 == null && this.e.de() != null) {
            var2 = this.e.de();
         }

         if(var2 != null) {
            var2.b(StatisticList.C);
            if(this.d instanceof class_vx) {
               var2.b((Statistic)AchievementList.H);
            }
         }

         this.d.b_(6000);
         this.e.b_(6000);
         this.d.dg();
         this.e.dg();
         var1.b_(-24000);
         var1.b(this.d.locX, this.d.locY, this.d.locZ, 0.0F, 0.0F);
         this.a.a((Entity)var1);
         Random var3 = this.d.bE();

         for(int var4 = 0; var4 < 7; ++var4) {
            double var5 = var3.nextGaussian() * 0.02D;
            double var7 = var3.nextGaussian() * 0.02D;
            double var9 = var3.nextGaussian() * 0.02D;
            double var11 = var3.nextDouble() * (double)this.d.width * 2.0D - (double)this.d.width;
            double var13 = 0.5D + var3.nextDouble() * (double)this.d.length;
            double var15 = var3.nextDouble() * (double)this.d.width * 2.0D - (double)this.d.width;
            this.a.a(EnumParticle.HEART, this.d.locX + var11, this.d.locY + var13, this.d.locZ + var15, var5, var7, var9, new int[0]);
         }

         if(this.a.U().b("doMobLoot")) {
            this.a.a((Entity)(new class_rw(this.a, this.d.locX, this.d.locY, this.d.locZ, var3.nextInt(7) + 1)));
         }

      }
   }
}
