package net.minecraft.server;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import net.minecraft.server.NBTReadLimiter;
import net.minecraft.server.NBTTagByteArray;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagDouble;
import net.minecraft.server.NBTTagFloat;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.NBTTagString;
import net.minecraft.server.class_dm;
import net.minecraft.server.class_dq;
import net.minecraft.server.class_ds;
import net.minecraft.server.class_dt;
import net.minecraft.server.class_dv;
import net.minecraft.server.class_dz;

public abstract class NBTTag {
   public static final String[] a = new String[]{"END", "BYTE", "SHORT", "INT", "LONG", "FLOAT", "DOUBLE", "BYTE[]", "STRING", "LIST", "COMPOUND", "INT[]"};

   abstract void a(DataOutput var1) throws IOException;

   abstract void a(DataInput var1, int var2, NBTReadLimiter var3) throws IOException;

   public abstract String toString();

   public abstract byte a();

   protected static NBTTag a(byte var0) {
      switch(var0) {
      case 0:
         return new class_dq();
      case 1:
         return new class_dm();
      case 2:
         return new class_dz();
      case 3:
         return new class_dt();
      case 4:
         return new class_dv();
      case 5:
         return new NBTTagFloat();
      case 6:
         return new NBTTagDouble();
      case 7:
         return new NBTTagByteArray();
      case 8:
         return new NBTTagString();
      case 9:
         return new NBTTagList();
      case 10:
         return new NBTTagCompound();
      case 11:
         return new class_ds();
      default:
         return null;
      }
   }

   public abstract NBTTag b();

   public boolean c_() {
      return false;
   }

   public boolean equals(Object var1) {
      if(!(var1 instanceof NBTTag)) {
         return false;
      } else {
         NBTTag var2 = (NBTTag)var1;
         return this.a() == var2.a();
      }
   }

   public int hashCode() {
      return this.a();
   }

   protected String a_() {
      return this.toString();
   }

   public abstract static class class_a_in_class_eb extends NBTTag {
      public abstract long c();

      public abstract int d();

      public abstract short e();

      public abstract byte f();

      public abstract double g();

      public abstract float h();
   }
}
