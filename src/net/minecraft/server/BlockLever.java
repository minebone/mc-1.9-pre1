package net.minecraft.server;

import java.util.Iterator;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ajy;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.class_or;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;

public class BlockLever extends Block {
   public static final BlockStateEnum a = BlockStateEnum.a("facing", BlockLever.EnumLeverPosition.class);
   public static final class_arm b = class_arm.a("powered");
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.3125D, 0.20000000298023224D, 0.625D, 0.6875D, 0.800000011920929D, 1.0D);
   protected static final AxisAlignedBB d = new AxisAlignedBB(0.3125D, 0.20000000298023224D, 0.0D, 0.6875D, 0.800000011920929D, 0.375D);
   protected static final AxisAlignedBB e = new AxisAlignedBB(0.625D, 0.20000000298023224D, 0.3125D, 1.0D, 0.800000011920929D, 0.6875D);
   protected static final AxisAlignedBB f = new AxisAlignedBB(0.0D, 0.20000000298023224D, 0.3125D, 0.375D, 0.800000011920929D, 0.6875D);
   protected static final AxisAlignedBB g = new AxisAlignedBB(0.25D, 0.0D, 0.25D, 0.75D, 0.6000000238418579D, 0.75D);
   protected static final AxisAlignedBB B = new AxisAlignedBB(0.25D, 0.4000000059604645D, 0.25D, 0.75D, 1.0D, 0.75D);

   protected BlockLever() {
      super(Material.q);
      this.w(this.A.b().set(a, BlockLever.EnumLeverPosition.NORTH).set(b, Boolean.valueOf(false)));
      this.a(CreativeModeTab.d);
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(World var1, BlockPosition var2, EnumDirection var3) {
      return a(var1, var2, var3.d());
   }

   public boolean a(World var1, BlockPosition var2) {
      EnumDirection[] var3 = EnumDirection.values();
      int var4 = var3.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         EnumDirection var6 = var3[var5];
         if(a(var1, var2, var6)) {
            return true;
         }
      }

      return false;
   }

   protected static boolean a(World var0, BlockPosition var1, EnumDirection var2) {
      return class_ajy.a(var0, var1, var2);
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      IBlockData var9 = this.u().set(b, Boolean.valueOf(false));
      if(a(var1, var2, var3.d())) {
         return var9.set(a, BlockLever.EnumLeverPosition.a(var3, var8.bh()));
      } else {
         Iterator var10 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

         EnumDirection var11;
         do {
            if(!var10.hasNext()) {
               if(var1.getType(var2.b()).q()) {
                  return var9.set(a, BlockLever.EnumLeverPosition.a(EnumDirection.UP, var8.bh()));
               }

               return var9;
            }

            var11 = (EnumDirection)var10.next();
         } while(var11 == var3 || !a(var1, var2, var11.d()));

         return var9.set(a, BlockLever.EnumLeverPosition.a(var11, var8.bh()));
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(this.e(var1, var2, var3) && !a(var1, var2, ((BlockLever.EnumLeverPosition)var3.get(a)).c().d())) {
         this.b(var1, var2, var3, 0);
         var1.g(var2);
      }

   }

   private boolean e(World var1, BlockPosition var2, IBlockData var3) {
      if(this.a(var1, var2)) {
         return true;
      } else {
         this.b(var1, var2, var3, 0);
         var1.g(var2);
         return false;
      }
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      switch(BlockLever.SyntheticClass_1.b[((BlockLever.EnumLeverPosition)var1.get(a)).ordinal()]) {
      case 1:
      default:
         return f;
      case 2:
         return e;
      case 3:
         return d;
      case 4:
         return c;
      case 5:
      case 6:
         return g;
      case 7:
      case 8:
         return B;
      }
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(var1.E) {
         return true;
      } else {
         var3 = var3.a(b);
         var1.a((BlockPosition)var2, (IBlockData)var3, 3);
         float var11 = ((Boolean)var3.get(b)).booleanValue()?0.6F:0.5F;
         var1.a((EntityHuman)null, var2, class_ng.dc, EnumSoundCategory.BLOCKS, 0.3F, var11);
         var1.d(var2, this);
         EnumDirection var12 = ((BlockLever.EnumLeverPosition)var3.get(a)).c();
         var1.d(var2.a(var12.d()), this);
         return true;
      }
   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      if(((Boolean)var3.get(b)).booleanValue()) {
         var1.d(var2, this);
         EnumDirection var4 = ((BlockLever.EnumLeverPosition)var3.get(a)).c();
         var1.d(var2.a(var4.d()), this);
      }

      super.b(var1, var2, var3);
   }

   public int b(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return ((Boolean)var1.get(b)).booleanValue()?15:0;
   }

   public int c(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return !((Boolean)var1.get(b)).booleanValue()?0:(((BlockLever.EnumLeverPosition)var1.get(a)).c() == var4?15:0);
   }

   public boolean g(IBlockData var1) {
      return true;
   }

   public IBlockData a(int var1) {
      return this.u().set(a, BlockLever.EnumLeverPosition.a(var1 & 7)).set(b, Boolean.valueOf((var1 & 8) > 0));
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((BlockLever.EnumLeverPosition)var1.get(a)).a();
      if(((Boolean)var1.get(b)).booleanValue()) {
         var3 |= 8;
      }

      return var3;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      switch(BlockLever.SyntheticClass_1.c[var2.ordinal()]) {
      case 1:
         switch(BlockLever.SyntheticClass_1.b[((BlockLever.EnumLeverPosition)var1.get(a)).ordinal()]) {
         case 1:
            return var1.set(a, BlockLever.EnumLeverPosition.WEST);
         case 2:
            return var1.set(a, BlockLever.EnumLeverPosition.EAST);
         case 3:
            return var1.set(a, BlockLever.EnumLeverPosition.NORTH);
         case 4:
            return var1.set(a, BlockLever.EnumLeverPosition.SOUTH);
         default:
            return var1;
         }
      case 2:
         switch(BlockLever.SyntheticClass_1.b[((BlockLever.EnumLeverPosition)var1.get(a)).ordinal()]) {
         case 1:
            return var1.set(a, BlockLever.EnumLeverPosition.NORTH);
         case 2:
            return var1.set(a, BlockLever.EnumLeverPosition.SOUTH);
         case 3:
            return var1.set(a, BlockLever.EnumLeverPosition.EAST);
         case 4:
            return var1.set(a, BlockLever.EnumLeverPosition.WEST);
         case 5:
            return var1.set(a, BlockLever.EnumLeverPosition.UP_X);
         case 6:
            return var1.set(a, BlockLever.EnumLeverPosition.UP_Z);
         case 7:
            return var1.set(a, BlockLever.EnumLeverPosition.DOWN_Z);
         case 8:
            return var1.set(a, BlockLever.EnumLeverPosition.DOWN_X);
         }
      case 3:
         switch(BlockLever.SyntheticClass_1.b[((BlockLever.EnumLeverPosition)var1.get(a)).ordinal()]) {
         case 1:
            return var1.set(a, BlockLever.EnumLeverPosition.SOUTH);
         case 2:
            return var1.set(a, BlockLever.EnumLeverPosition.NORTH);
         case 3:
            return var1.set(a, BlockLever.EnumLeverPosition.WEST);
         case 4:
            return var1.set(a, BlockLever.EnumLeverPosition.EAST);
         case 5:
            return var1.set(a, BlockLever.EnumLeverPosition.UP_X);
         case 6:
            return var1.set(a, BlockLever.EnumLeverPosition.UP_Z);
         case 7:
            return var1.set(a, BlockLever.EnumLeverPosition.DOWN_Z);
         case 8:
            return var1.set(a, BlockLever.EnumLeverPosition.DOWN_X);
         }
      default:
         return var1;
      }
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a(((BlockLever.EnumLeverPosition)var1.get(a)).c()));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b;
      // $FF: synthetic field
      static final int[] c;
      // $FF: synthetic field
      static final int[] d = new int[EnumDirection.class_a_in_class_cq.values().length];

      static {
         try {
            d[EnumDirection.class_a_in_class_cq.X.ordinal()] = 1;
         } catch (NoSuchFieldError var19) {
            ;
         }

         try {
            d[EnumDirection.class_a_in_class_cq.Z.ordinal()] = 2;
         } catch (NoSuchFieldError var18) {
            ;
         }

         c = new int[class_aod.values().length];

         try {
            c[class_aod.CLOCKWISE_180.ordinal()] = 1;
         } catch (NoSuchFieldError var17) {
            ;
         }

         try {
            c[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var16) {
            ;
         }

         try {
            c[class_aod.CLOCKWISE_90.ordinal()] = 3;
         } catch (NoSuchFieldError var15) {
            ;
         }

         b = new int[BlockLever.EnumLeverPosition.values().length];

         try {
            b[BlockLever.EnumLeverPosition.EAST.ordinal()] = 1;
         } catch (NoSuchFieldError var14) {
            ;
         }

         try {
            b[BlockLever.EnumLeverPosition.WEST.ordinal()] = 2;
         } catch (NoSuchFieldError var13) {
            ;
         }

         try {
            b[BlockLever.EnumLeverPosition.SOUTH.ordinal()] = 3;
         } catch (NoSuchFieldError var12) {
            ;
         }

         try {
            b[BlockLever.EnumLeverPosition.NORTH.ordinal()] = 4;
         } catch (NoSuchFieldError var11) {
            ;
         }

         try {
            b[BlockLever.EnumLeverPosition.UP_Z.ordinal()] = 5;
         } catch (NoSuchFieldError var10) {
            ;
         }

         try {
            b[BlockLever.EnumLeverPosition.UP_X.ordinal()] = 6;
         } catch (NoSuchFieldError var9) {
            ;
         }

         try {
            b[BlockLever.EnumLeverPosition.DOWN_X.ordinal()] = 7;
         } catch (NoSuchFieldError var8) {
            ;
         }

         try {
            b[BlockLever.EnumLeverPosition.DOWN_Z.ordinal()] = 8;
         } catch (NoSuchFieldError var7) {
            ;
         }

         a = new int[EnumDirection.values().length];

         try {
            a[EnumDirection.DOWN.ordinal()] = 1;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[EnumDirection.UP.ordinal()] = 2;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[EnumDirection.NORTH.ordinal()] = 3;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.SOUTH.ordinal()] = 4;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 5;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.EAST.ordinal()] = 6;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum EnumLeverPosition implements class_or {
      DOWN_X(0, "down_x", EnumDirection.DOWN),
      EAST(1, "east", EnumDirection.EAST),
      WEST(2, "west", EnumDirection.WEST),
      SOUTH(3, "south", EnumDirection.SOUTH),
      NORTH(4, "north", EnumDirection.NORTH),
      UP_Z(5, "up_z", EnumDirection.UP),
      UP_X(6, "up_x", EnumDirection.UP),
      DOWN_Z(7, "down_z", EnumDirection.DOWN);

      private static final BlockLever.EnumLeverPosition[] i = new BlockLever.EnumLeverPosition[values().length];
      private final int j;
      private final String k;
      private final EnumDirection l;

      private EnumLeverPosition(int var3, String var4, EnumDirection var5) {
         this.j = var3;
         this.k = var4;
         this.l = var5;
      }

      public int a() {
         return this.j;
      }

      public EnumDirection c() {
         return this.l;
      }

      public String toString() {
         return this.k;
      }

      public static BlockLever.EnumLeverPosition a(int var0) {
         if(var0 < 0 || var0 >= i.length) {
            var0 = 0;
         }

         return i[var0];
      }

      public static BlockLever.EnumLeverPosition a(EnumDirection var0, EnumDirection var1) {
         switch(BlockLever.SyntheticClass_1.a[var0.ordinal()]) {
         case 1:
            switch(BlockLever.SyntheticClass_1.d[var1.k().ordinal()]) {
            case 1:
               return DOWN_X;
            case 2:
               return DOWN_Z;
            default:
               throw new IllegalArgumentException("Invalid entityFacing " + var1 + " for facing " + var0);
            }
         case 2:
            switch(BlockLever.SyntheticClass_1.d[var1.k().ordinal()]) {
            case 1:
               return UP_X;
            case 2:
               return UP_Z;
            default:
               throw new IllegalArgumentException("Invalid entityFacing " + var1 + " for facing " + var0);
            }
         case 3:
            return NORTH;
         case 4:
            return SOUTH;
         case 5:
            return WEST;
         case 6:
            return EAST;
         default:
            throw new IllegalArgumentException("Invalid facing: " + var0);
         }
      }

      public String m() {
         return this.k;
      }

      static {
         BlockLever.EnumLeverPosition[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            BlockLever.EnumLeverPosition var3 = var0[var2];
            i[var3.a()] = var3;
         }

      }
   }
}
