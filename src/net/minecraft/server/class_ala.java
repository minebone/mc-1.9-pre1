package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_aqp;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_ala extends class_ajm {
   protected class_ala(Material var1) {
      super(var1);
      this.a(1.0F);
   }

   public TileEntity a(World var1, int var2) {
      return new class_aqp();
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public int a(Random var1) {
      return 0;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return null;
   }

   public MaterialMapColor r(IBlockData var1) {
      return MaterialMapColor.E;
   }
}
