package net.minecraft.server;

import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.ChestLock;
import net.minecraft.server.Container;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.class_aba;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_qr;

public class class_qe implements class_qr {
   private String a;
   private class_qr b;
   private class_qr c;

   public class_qe(String var1, class_qr var2, class_qr var3) {
      this.a = var1;
      if(var2 == null) {
         var2 = var3;
      }

      if(var3 == null) {
         var3 = var2;
      }

      this.b = var2;
      this.c = var3;
      if(var2.x_()) {
         var3.a(var2.y_());
      } else if(var3.x_()) {
         var2.a(var3.y_());
      }

   }

   public int u_() {
      return this.b.u_() + this.c.u_();
   }

   public boolean a(IInventory var1) {
      return this.b == var1 || this.c == var1;
   }

   public String h_() {
      return this.b.o_()?this.b.h_():(this.c.o_()?this.c.h_():this.a);
   }

   public boolean o_() {
      return this.b.o_() || this.c.o_();
   }

   public IChatBaseComponent i_() {
      return (IChatBaseComponent)(this.o_()?new ChatComponentText(this.h_()):new ChatMessage(this.h_(), new Object[0]));
   }

   public ItemStack a(int var1) {
      return var1 >= this.b.u_()?this.c.a(var1 - this.b.u_()):this.b.a(var1);
   }

   public ItemStack a(int var1, int var2) {
      return var1 >= this.b.u_()?this.c.a(var1 - this.b.u_(), var2):this.b.a(var1, var2);
   }

   public ItemStack b(int var1) {
      return var1 >= this.b.u_()?this.c.b(var1 - this.b.u_()):this.b.b(var1);
   }

   public void a(int var1, ItemStack var2) {
      if(var1 >= this.b.u_()) {
         this.c.a(var1 - this.b.u_(), var2);
      } else {
         this.b.a(var1, var2);
      }

   }

   public int w_() {
      return this.b.w_();
   }

   public void v_() {
      this.b.v_();
      this.c.v_();
   }

   public boolean a(EntityHuman var1) {
      return this.b.a(var1) && this.c.a(var1);
   }

   public void b(EntityHuman var1) {
      this.b.b(var1);
      this.c.b(var1);
   }

   public void c(EntityHuman var1) {
      this.b.c(var1);
      this.c.c(var1);
   }

   public boolean b(int var1, ItemStack var2) {
      return true;
   }

   public int c_(int var1) {
      return 0;
   }

   public void b(int var1, int var2) {
   }

   public int g() {
      return 0;
   }

   public boolean x_() {
      return this.b.x_() || this.c.x_();
   }

   public void a(ChestLock var1) {
      this.b.a(var1);
      this.c.a(var1);
   }

   public ChestLock y_() {
      return this.b.y_();
   }

   public String k() {
      return this.b.k();
   }

   public Container a(PlayerInventory var1, EntityHuman var2) {
      return new class_aba(var1, this, var2);
   }

   public void l() {
      this.b.l();
      this.c.l();
   }
}
