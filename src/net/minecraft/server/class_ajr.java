package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_akm;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.BlockPosition;

public class class_ajr extends class_akm {
   public static final BlockStateInteger a = BlockStateInteger.a("age", 0, 3);
   private static final AxisAlignedBB[] d = new AxisAlignedBB[]{new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.125D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.25D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.375D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.5D, 1.0D)};

   protected BlockStateInteger e() {
      return a;
   }

   public int g() {
      return 3;
   }

   protected Item h() {
      return Items.cU;
   }

   protected Item i() {
      return Items.cV;
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(var4.nextInt(3) == 0) {
         this.e(var1, var2, var3);
      } else {
         super.b(var1, var2, var3, var4);
      }

   }

   protected int b(World var1) {
      return super.b(var1) / 3;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return d[((Integer)var1.get(this.e())).intValue()];
   }
}
