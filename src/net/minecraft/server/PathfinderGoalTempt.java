package net.minecraft.server;

import com.google.common.collect.Sets;
import java.util.Set;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityCreature;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_vd;

public class PathfinderGoalTempt extends class_tj {
   private EntityCreature a;
   private double b;
   private double c;
   private double d;
   private double e;
   private double f;
   private double g;
   private EntityHuman h;
   private int i;
   private boolean j;
   private Set k;
   private boolean l;

   public PathfinderGoalTempt(EntityCreature var1, double var2, Item var4, boolean var5) {
      this(var1, var2, var5, Sets.newHashSet((Object[])(new Item[]{var4})));
   }

   public PathfinderGoalTempt(EntityCreature var1, double var2, boolean var4, Set var5) {
      this.a = var1;
      this.b = var2;
      this.k = var5;
      this.l = var4;
      this.a(3);
      if(!(var1.x() instanceof class_vd)) {
         throw new IllegalArgumentException("Unsupported mob type for TemptGoal");
      }
   }

   public boolean a() {
      if(this.i > 0) {
         --this.i;
         return false;
      } else {
         this.h = this.a.world.a(this.a, 10.0D);
         return this.h == null?false:this.a(this.h.ca()) || this.a(this.h.cb());
      }
   }

   protected boolean a(ItemStack var1) {
      return var1 == null?false:this.k.contains(var1.b());
   }

   public boolean b() {
      if(this.l) {
         if(this.a.h(this.h) < 36.0D) {
            if(this.h.e(this.c, this.d, this.e) > 0.010000000000000002D) {
               return false;
            }

            if(Math.abs((double)this.h.pitch - this.f) > 5.0D || Math.abs((double)this.h.yaw - this.g) > 5.0D) {
               return false;
            }
         } else {
            this.c = this.h.locX;
            this.d = this.h.locY;
            this.e = this.h.locZ;
         }

         this.f = (double)this.h.pitch;
         this.g = (double)this.h.yaw;
      }

      return this.a();
   }

   public void c() {
      this.c = this.h.locX;
      this.d = this.h.locY;
      this.e = this.h.locZ;
      this.j = true;
   }

   public void d() {
      this.h = null;
      this.a.x().o();
      this.i = 100;
      this.j = false;
   }

   public void e() {
      this.a.t().a(this.h, (float)(this.a.cE() + 20), (float)this.a.cD());
      if(this.a.h(this.h) < 6.25D) {
         this.a.x().o();
      } else {
         this.a.x().a((Entity)this.h, this.b);
      }

   }

   public boolean f() {
      return this.j;
   }
}
