package net.minecraft.server;

import net.minecraft.server.BiomeBase;
import net.minecraft.server.CustomWorldSettingsFinal;
import net.minecraft.server.GenLayerRegionHills;
import net.minecraft.server.GenLayerSpecial;
import net.minecraft.server.WorldType;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_axi;
import net.minecraft.server.class_axk;
import net.minecraft.server.class_axl;
import net.minecraft.server.class_axm;
import net.minecraft.server.class_axn;
import net.minecraft.server.class_axo;
import net.minecraft.server.class_axq;
import net.minecraft.server.class_axt;
import net.minecraft.server.class_axv;
import net.minecraft.server.class_axx;
import net.minecraft.server.class_axy;
import net.minecraft.server.class_axz;
import net.minecraft.server.class_aya;
import net.minecraft.server.class_ayb;
import net.minecraft.server.class_ayc;
import net.minecraft.server.class_ayg;
import net.minecraft.server.class_ayh;

public abstract class class_axu {
   private long c;
   protected class_axu a;
   private long d;
   protected long b;

   public static class_axu[] a(long var0, WorldType var2, String var3) {
      class_axt var4 = new class_axt(1L);
      class_axq var13 = new class_axq(2000L, var4);
      class_axk var14 = new class_axk(1L, var13);
      class_ayh var15 = new class_ayh(2001L, var14);
      var14 = new class_axk(2L, var15);
      var14 = new class_axk(50L, var14);
      var14 = new class_axk(70L, var14);
      class_axx var16 = new class_axx(2L, var14);
      class_axm var17 = new class_axm(2L, var16);
      var14 = new class_axk(3L, var17);
      GenLayerSpecial var18 = new GenLayerSpecial(2L, var14, GenLayerSpecial.EnumGenLayerSpecial.COOL_WARM);
      var18 = new GenLayerSpecial(2L, var18, GenLayerSpecial.EnumGenLayerSpecial.HEAT_ICE);
      var18 = new GenLayerSpecial(3L, var18, GenLayerSpecial.EnumGenLayerSpecial.SPECIAL);
      var15 = new class_ayh(2002L, var18);
      var15 = new class_ayh(2003L, var15);
      var14 = new class_axk(4L, var15);
      class_axl var20 = new class_axl(5L, var14);
      class_axi var23 = new class_axi(4L, var20);
      class_axu var26 = class_ayh.b(1000L, var23, 0);
      CustomWorldSettingsFinal var5 = null;
      int var6 = 4;
      int var7 = var6;
      if(var2 == WorldType.f && !var3.isEmpty()) {
         var5 = CustomWorldSettingsFinal.class_a_in_class_atf.a(var3).b();
         var6 = var5.G;
         var7 = var5.H;
      }

      if(var2 == WorldType.d) {
         var6 = 6;
      }

      class_axu var8 = class_ayh.b(1000L, var26, 0);
      class_axy var19 = new class_axy(100L, var8);
      class_axo var9 = new class_axo(200L, var26, var2, var3);
      class_axu var21 = class_ayh.b(1000L, var9, 2);
      class_axn var24 = new class_axn(1000L, var21);
      class_axu var10 = class_ayh.b(1000L, var19, 2);
      GenLayerRegionHills var27 = new GenLayerRegionHills(1000L, var24, var10);
      var8 = class_ayh.b(1000L, var19, 2);
      var8 = class_ayh.b(1000L, var8, var7);
      class_axz var22 = new class_axz(1L, var8);
      class_ayc var25 = new class_ayc(1000L, var22);
      Object var28 = new class_axv(1001L, var27);

      for(int var11 = 0; var11 < var6; ++var11) {
         var28 = new class_ayh((long)(1000 + var11), (class_axu)var28);
         if(var11 == 0) {
            var28 = new class_axk(3L, (class_axu)var28);
         }

         if(var11 == 1 || var6 == 1) {
            var28 = new class_ayb(1000L, (class_axu)var28);
         }
      }

      class_ayc var29 = new class_ayc(1000L, (class_axu)var28);
      class_aya var30 = new class_aya(100L, var29, var25);
      class_ayg var12 = new class_ayg(10L, var30);
      var30.a(var0);
      var12.a(var0);
      return new class_axu[]{var30, var12, var30};
   }

   public class_axu(long var1) {
      this.b = var1;
      this.b *= this.b * 6364136223846793005L + 1442695040888963407L;
      this.b += var1;
      this.b *= this.b * 6364136223846793005L + 1442695040888963407L;
      this.b += var1;
      this.b *= this.b * 6364136223846793005L + 1442695040888963407L;
      this.b += var1;
   }

   public void a(long var1) {
      this.c = var1;
      if(this.a != null) {
         this.a.a(var1);
      }

      this.c *= this.c * 6364136223846793005L + 1442695040888963407L;
      this.c += this.b;
      this.c *= this.c * 6364136223846793005L + 1442695040888963407L;
      this.c += this.b;
      this.c *= this.c * 6364136223846793005L + 1442695040888963407L;
      this.c += this.b;
   }

   public void a(long var1, long var3) {
      this.d = this.c;
      this.d *= this.d * 6364136223846793005L + 1442695040888963407L;
      this.d += var1;
      this.d *= this.d * 6364136223846793005L + 1442695040888963407L;
      this.d += var3;
      this.d *= this.d * 6364136223846793005L + 1442695040888963407L;
      this.d += var1;
      this.d *= this.d * 6364136223846793005L + 1442695040888963407L;
      this.d += var3;
   }

   protected int a(int var1) {
      int var2 = (int)((this.d >> 24) % (long)var1);
      if(var2 < 0) {
         var2 += var1;
      }

      this.d *= this.d * 6364136223846793005L + 1442695040888963407L;
      this.d += this.c;
      return var2;
   }

   public abstract int[] a(int var1, int var2, int var3, int var4);

   protected static boolean a(int var0, int var1) {
      if(var0 == var1) {
         return true;
      } else {
         BiomeBase var2 = BiomeBase.b(var0);
         BiomeBase var3 = BiomeBase.b(var1);
         return var2 != null && var3 != null?(var2 != class_aik.N && var2 != class_aik.O?var2 == var3 || var2.g() == var3.g():var3 == class_aik.N || var3 == class_aik.O):false;
      }
   }

   protected static boolean b(int var0) {
      BiomeBase var1 = BiomeBase.b(var0);
      return var1 == class_aik.a || var1 == class_aik.z || var1 == class_aik.l;
   }

   protected int a(int... var1) {
      return var1[this.a(var1.length)];
   }

   protected int b(int var1, int var2, int var3, int var4) {
      return var2 == var3 && var3 == var4?var2:(var1 == var2 && var1 == var3?var1:(var1 == var2 && var1 == var4?var1:(var1 == var3 && var1 == var4?var1:(var1 == var2 && var3 != var4?var1:(var1 == var3 && var2 != var4?var1:(var1 == var4 && var2 != var3?var1:(var2 == var3 && var1 != var4?var2:(var2 == var4 && var1 != var3?var2:(var3 == var4 && var1 != var2?var3:this.a(new int[]{var1, var2, var3, var4}))))))))));
   }
}
