package net.minecraft.server;

import com.google.common.base.Predicate;
import java.util.Arrays;
import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EntityMinecartAbstract;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Item;
import net.minecraft.server.ItemArmor;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.ItemStack;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_dc;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_xz;
import net.minecraft.server.class_zl;

public class class_xp extends class_rz {
   private static final class_dc bp = new class_dc(0.0F, 0.0F, 0.0F);
   private static final class_dc bq = new class_dc(0.0F, 0.0F, 0.0F);
   private static final class_dc br = new class_dc(-10.0F, 0.0F, -10.0F);
   private static final class_dc bs = new class_dc(-15.0F, 0.0F, 10.0F);
   private static final class_dc bt = new class_dc(-1.0F, 0.0F, -1.0F);
   private static final class_dc bu = new class_dc(1.0F, 0.0F, 1.0F);
   public static final class_ke a = DataWatcher.a(class_xp.class, class_kg.a);
   public static final class_ke b = DataWatcher.a(class_xp.class, class_kg.i);
   public static final class_ke c = DataWatcher.a(class_xp.class, class_kg.i);
   public static final class_ke d = DataWatcher.a(class_xp.class, class_kg.i);
   public static final class_ke e = DataWatcher.a(class_xp.class, class_kg.i);
   public static final class_ke f = DataWatcher.a(class_xp.class, class_kg.i);
   public static final class_ke g = DataWatcher.a(class_xp.class, class_kg.i);
   private static final Predicate bv = new Predicate() {
      public boolean a(Entity var1) {
         return var1 instanceof EntityMinecartAbstract && ((EntityMinecartAbstract)var1).v() == EntityMinecartAbstract.EnumMinecartType.RIDEABLE;
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((Entity)var1);
      }
   };
   private final ItemStack[] bw;
   private final ItemStack[] bx;
   private boolean by;
   public long h;
   private int bz;
   private boolean bA;
   private class_dc bB;
   private class_dc bC;
   private class_dc bD;
   private class_dc bE;
   private class_dc bF;
   private class_dc bG;

   public class_xp(World var1) {
      super(var1);
      this.bw = new ItemStack[2];
      this.bx = new ItemStack[4];
      this.bB = bp;
      this.bC = bq;
      this.bD = br;
      this.bE = bs;
      this.bF = bt;
      this.bG = bu;
      this.noClip = this.r();
      this.a(0.5F, 1.975F);
   }

   public class_xp(World var1, double var2, double var4, double var6) {
      this(var1);
      this.b(var2, var4, var6);
   }

   public boolean cn() {
      return super.cn() && !this.r();
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)a, (Object)Byte.valueOf((byte)0));
      this.datawatcher.a((class_ke)b, (Object)bp);
      this.datawatcher.a((class_ke)c, (Object)bq);
      this.datawatcher.a((class_ke)d, (Object)br);
      this.datawatcher.a((class_ke)e, (Object)bs);
      this.datawatcher.a((class_ke)f, (Object)bt);
      this.datawatcher.a((class_ke)g, (Object)bu);
   }

   public Iterable aD() {
      return Arrays.asList(this.bw);
   }

   public Iterable aE() {
      return Arrays.asList(this.bx);
   }

   public ItemStack a(EnumInventorySlot var1) {
      ItemStack var2 = null;
      switch(class_xp.SyntheticClass_1.a[var1.a().ordinal()]) {
      case 1:
         var2 = this.bw[var1.b()];
         break;
      case 2:
         var2 = this.bx[var1.b()];
      }

      return var2;
   }

   public void a(EnumInventorySlot var1, ItemStack var2) {
      switch(class_xp.SyntheticClass_1.a[var1.a().ordinal()]) {
      case 1:
         this.a_(var2);
         this.bw[var1.b()] = var2;
         break;
      case 2:
         this.a_(var2);
         this.bx[var1.b()] = var2;
      }

   }

   public boolean c(int var1, ItemStack var2) {
      EnumInventorySlot var3;
      if(var1 == 98) {
         var3 = EnumInventorySlot.MAINHAND;
      } else if(var1 == 99) {
         var3 = EnumInventorySlot.OFFHAND;
      } else if(var1 == 100 + EnumInventorySlot.HEAD.b()) {
         var3 = EnumInventorySlot.HEAD;
      } else if(var1 == 100 + EnumInventorySlot.CHEST.b()) {
         var3 = EnumInventorySlot.CHEST;
      } else if(var1 == 100 + EnumInventorySlot.LEGS.b()) {
         var3 = EnumInventorySlot.LEGS;
      } else {
         if(var1 != 100 + EnumInventorySlot.FEET.b()) {
            return false;
         }

         var3 = EnumInventorySlot.FEET;
      }

      if(var2 != null && !EntityInsentient.b(var3, var2) && var3 != EnumInventorySlot.HEAD) {
         return false;
      } else {
         this.a(var3, var2);
         return true;
      }
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      NBTTagList var2 = new NBTTagList();

      for(int var3 = 0; var3 < this.bx.length; ++var3) {
         NBTTagCompound var4 = new NBTTagCompound();
         if(this.bx[var3] != null) {
            this.bx[var3].b(var4);
         }

         var2.a((NBTTag)var4);
      }

      var1.a((String)"ArmorItems", (NBTTag)var2);
      NBTTagList var6 = new NBTTagList();

      for(int var7 = 0; var7 < this.bw.length; ++var7) {
         NBTTagCompound var5 = new NBTTagCompound();
         if(this.bw[var7] != null) {
            this.bw[var7].b(var5);
         }

         var6.a((NBTTag)var5);
      }

      var1.a((String)"HandItems", (NBTTag)var6);
      if(this.bf() && (this.be() == null || this.be().isEmpty())) {
         var1.a("CustomNameVisible", this.bf());
      }

      var1.a("Invisible", this.aM());
      var1.a("Small", this.o());
      var1.a("ShowArms", this.s());
      var1.a("DisabledSlots", this.bz);
      var1.a("NoGravity", this.r());
      var1.a("NoBasePlate", this.t());
      if(this.u()) {
         var1.a("Marker", this.u());
      }

      var1.a((String)"Pose", (NBTTag)this.D());
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      NBTTagList var2;
      int var3;
      if(var1.b("ArmorItems", 9)) {
         var2 = var1.c("ArmorItems", 10);

         for(var3 = 0; var3 < this.bx.length; ++var3) {
            this.bx[var3] = ItemStack.a(var2.b(var3));
         }
      }

      if(var1.b("HandItems", 9)) {
         var2 = var1.c("HandItems", 10);

         for(var3 = 0; var3 < this.bw.length; ++var3) {
            this.bw[var3] = ItemStack.a(var2.b(var3));
         }
      }

      this.g(var1.p("Invisible"));
      this.l(var1.p("Small"));
      this.n(var1.p("ShowArms"));
      this.bz = var1.h("DisabledSlots");
      this.m(var1.p("NoGravity"));
      this.o(var1.p("NoBasePlate"));
      this.p(var1.p("Marker"));
      this.bA = !this.u();
      this.noClip = this.r();
      NBTTagCompound var4 = var1.o("Pose");
      this.g(var4);
   }

   private void g(NBTTagCompound var1) {
      NBTTagList var2 = var1.c("Head", 5);
      this.a(var2.c_()?bp:new class_dc(var2));
      NBTTagList var3 = var1.c("Body", 5);
      this.b(var3.c_()?bq:new class_dc(var3));
      NBTTagList var4 = var1.c("LeftArm", 5);
      this.c(var4.c_()?br:new class_dc(var4));
      NBTTagList var5 = var1.c("RightArm", 5);
      this.d(var5.c_()?bs:new class_dc(var5));
      NBTTagList var6 = var1.c("LeftLeg", 5);
      this.e(var6.c_()?bt:new class_dc(var6));
      NBTTagList var7 = var1.c("RightLeg", 5);
      this.f(var7.c_()?bu:new class_dc(var7));
   }

   private NBTTagCompound D() {
      NBTTagCompound var1 = new NBTTagCompound();
      if(!bp.equals(this.bB)) {
         var1.a((String)"Head", (NBTTag)this.bB.a());
      }

      if(!bq.equals(this.bC)) {
         var1.a((String)"Body", (NBTTag)this.bC.a());
      }

      if(!br.equals(this.bD)) {
         var1.a((String)"LeftArm", (NBTTag)this.bD.a());
      }

      if(!bs.equals(this.bE)) {
         var1.a((String)"RightArm", (NBTTag)this.bE.a());
      }

      if(!bt.equals(this.bF)) {
         var1.a((String)"LeftLeg", (NBTTag)this.bF.a());
      }

      if(!bu.equals(this.bG)) {
         var1.a((String)"RightLeg", (NBTTag)this.bG.a());
      }

      return var1;
   }

   public boolean ap() {
      return false;
   }

   protected void C(Entity var1) {
   }

   protected void cm() {
      List var1 = this.world.a((Entity)this, (AxisAlignedBB)this.bk(), (Predicate)bv);

      for(int var2 = 0; var2 < var1.size(); ++var2) {
         Entity var3 = (Entity)var1.get(var2);
         if(this.h(var3) <= 0.2D) {
            var3.i(this);
         }
      }

   }

   public EnumResult a(EntityHuman var1, Vec3D var2, ItemStack var3, EnumHand var4) {
      if(this.u()) {
         return EnumResult.PASS;
      } else if(!this.world.E && !var1.y()) {
         EnumInventorySlot var5 = EnumInventorySlot.MAINHAND;
         boolean var6 = var3 != null;
         Item var7 = var6?var3.b():null;
         if(var6 && var7 instanceof ItemArmor) {
            var5 = ((ItemArmor)var7).c;
         }

         if(var6 && (var7 == Items.ch || var7 == Item.a(Blocks.aU))) {
            var5 = EnumInventorySlot.HEAD;
         }

         double var8 = 0.1D;
         double var10 = 0.9D;
         double var12 = 0.4D;
         double var14 = 1.6D;
         EnumInventorySlot var16 = EnumInventorySlot.MAINHAND;
         boolean var17 = this.o();
         double var18 = var17?var2.c * 2.0D:var2.c;
         if(var18 >= 0.1D && var18 < 0.1D + (var17?0.8D:0.45D) && this.a(EnumInventorySlot.FEET) != null) {
            var16 = EnumInventorySlot.FEET;
         } else if(var18 >= 0.9D + (var17?0.3D:0.0D) && var18 < 0.9D + (var17?1.0D:0.7D) && this.a(EnumInventorySlot.CHEST) != null) {
            var16 = EnumInventorySlot.CHEST;
         } else if(var18 >= 0.4D && var18 < 0.4D + (var17?1.0D:0.8D) && this.a(EnumInventorySlot.LEGS) != null) {
            var16 = EnumInventorySlot.LEGS;
         } else if(var18 >= 1.6D && this.a(EnumInventorySlot.HEAD) != null) {
            var16 = EnumInventorySlot.HEAD;
         }

         boolean var20 = this.a(var16) != null;
         if(this.b(var16) || this.b(var5)) {
            var16 = var5;
            if(this.b(var5)) {
               return EnumResult.FAIL;
            }
         }

         if(var6 && var5 == EnumInventorySlot.MAINHAND && !this.s()) {
            return EnumResult.FAIL;
         } else {
            if(var6) {
               this.a(var1, var5, var3, var4);
            } else if(var20) {
               this.a(var1, var16, var3, var4);
            }

            return EnumResult.SUCCESS;
         }
      } else {
         return EnumResult.SUCCESS;
      }
   }

   private boolean b(EnumInventorySlot var1) {
      return (this.bz & 1 << var1.c()) != 0;
   }

   private void a(EntityHuman var1, EnumInventorySlot var2, ItemStack var3, EnumHand var4) {
      ItemStack var5 = this.a(var2);
      if(var5 == null || (this.bz & 1 << var2.c() + 8) == 0) {
         if(var5 != null || (this.bz & 1 << var2.c() + 16) == 0) {
            ItemStack var6;
            if(var1.abilities.d && (var5 == null || var5.b() == Item.a(Blocks.AIR)) && var3 != null) {
               var6 = var3.k();
               var6.b = 1;
               this.a(var2, var6);
            } else if(var3 != null && var3.b > 1) {
               if(var5 == null) {
                  var6 = var3.k();
                  var6.b = 1;
                  this.a(var2, var6);
                  --var3.b;
               }
            } else {
               this.a(var2, var3);
               var1.a((EnumHand)var4, (ItemStack)var5);
            }
         }
      }
   }

   public boolean a(DamageSource var1, float var2) {
      if(!this.world.E && !this.dead) {
         if(DamageSource.k.equals(var1)) {
            this.S();
            return false;
         } else if(!this.b((DamageSource)var1) && !this.by && !this.u()) {
            if(var1.c()) {
               this.I();
               this.S();
               return false;
            } else if(DamageSource.a.equals(var1)) {
               if(this.aG()) {
                  this.a(0.15F);
               } else {
                  this.g(5);
               }

               return false;
            } else if(DamageSource.c.equals(var1) && this.bP() > 0.5F) {
               this.a(4.0F);
               return false;
            } else {
               boolean var3 = "arrow".equals(var1.p());
               boolean var4 = "player".equals(var1.p());
               if(!var4 && !var3) {
                  return false;
               } else {
                  if(var1.i() instanceof class_zl) {
                     var1.i().S();
                  }

                  if(var1.j() instanceof EntityHuman && !((EntityHuman)var1.j()).abilities.e) {
                     return false;
                  } else if(var1.u()) {
                     this.E();
                     this.S();
                     return false;
                  } else {
                     long var5 = this.world.P();
                     if(var5 - this.h > 5L && !var3) {
                        this.world.a((Entity)this, (byte)32);
                        this.h = var5;
                     } else {
                        this.G();
                        this.E();
                        this.S();
                     }

                     return false;
                  }
               }
            }
         } else {
            return false;
         }
      } else {
         return false;
      }
   }

   private void E() {
      if(this.world instanceof WorldServer) {
         ((WorldServer)this.world).a(EnumParticle.BLOCK_DUST, this.locX, this.locY + (double)this.length / 1.5D, this.locZ, 10, (double)(this.width / 4.0F), (double)(this.length / 4.0F), (double)(this.width / 4.0F), 0.05D, new int[]{Block.j(Blocks.f.u())});
      }

   }

   private void a(float var1) {
      float var2 = this.bP();
      var2 -= var1;
      if(var2 <= 0.5F) {
         this.I();
         this.S();
      } else {
         this.c(var2);
      }

   }

   private void G() {
      Block.a(this.world, new BlockPosition(this), new ItemStack(Items.ct));
      this.I();
   }

   private void I() {
      this.world.a((EntityHuman)null, this.locX, this.locY, this.locZ, class_ng.j, this.by(), 1.0F, 1.0F);

      int var1;
      for(var1 = 0; var1 < this.bw.length; ++var1) {
         if(this.bw[var1] != null && this.bw[var1].b > 0) {
            if(this.bw[var1] != null) {
               Block.a(this.world, (new BlockPosition(this)).a(), this.bw[var1]);
            }

            this.bw[var1] = null;
         }
      }

      for(var1 = 0; var1 < this.bx.length; ++var1) {
         if(this.bx[var1] != null && this.bx[var1].b > 0) {
            if(this.bx[var1] != null) {
               Block.a(this.world, (new BlockPosition(this)).a(), this.bx[var1]);
            }

            this.bx[var1] = null;
         }
      }

   }

   protected float h(float var1, float var2) {
      this.aN = this.lastYaw;
      this.aM = this.yaw;
      return 0.0F;
   }

   public float bm() {
      return this.m_()?this.length * 0.5F:this.length * 0.9F;
   }

   public double aw() {
      return this.u()?0.0D:0.10000000149011612D;
   }

   public void g(float var1, float var2) {
      if(!this.r()) {
         super.g(var1, var2);
      }
   }

   public void m() {
      super.m();
      class_dc var1 = (class_dc)this.datawatcher.a(b);
      if(!this.bB.equals(var1)) {
         this.a(var1);
      }

      class_dc var2 = (class_dc)this.datawatcher.a(c);
      if(!this.bC.equals(var2)) {
         this.b(var2);
      }

      class_dc var3 = (class_dc)this.datawatcher.a(d);
      if(!this.bD.equals(var3)) {
         this.c(var3);
      }

      class_dc var4 = (class_dc)this.datawatcher.a(e);
      if(!this.bE.equals(var4)) {
         this.d(var4);
      }

      class_dc var5 = (class_dc)this.datawatcher.a(f);
      if(!this.bF.equals(var5)) {
         this.e(var5);
      }

      class_dc var6 = (class_dc)this.datawatcher.a(g);
      if(!this.bG.equals(var6)) {
         this.f(var6);
      }

      boolean var7 = this.u();
      if(!this.bA && var7) {
         this.a(false);
         this.i = false;
      } else {
         if(!this.bA || var7) {
            return;
         }

         this.a(true);
         this.i = true;
      }

      this.bA = var7;
   }

   private void a(boolean var1) {
      double var2 = this.locX;
      double var4 = this.locY;
      double var6 = this.locZ;
      if(var1) {
         this.a(0.5F, 1.975F);
      } else {
         this.a(0.0F, 0.0F);
      }

      this.b(var2, var4, var6);
   }

   protected void F() {
      this.g(this.by);
   }

   public void g(boolean var1) {
      this.by = var1;
      super.g(var1);
   }

   public boolean m_() {
      return this.o();
   }

   public void P() {
      this.S();
   }

   public boolean bp() {
      return this.aM();
   }

   private void l(boolean var1) {
      this.datawatcher.b(a, Byte.valueOf(this.a(((Byte)this.datawatcher.a(a)).byteValue(), 1, var1)));
   }

   public boolean o() {
      return (((Byte)this.datawatcher.a(a)).byteValue() & 1) != 0;
   }

   private void m(boolean var1) {
      this.datawatcher.b(a, Byte.valueOf(this.a(((Byte)this.datawatcher.a(a)).byteValue(), 2, var1)));
   }

   public boolean r() {
      return (((Byte)this.datawatcher.a(a)).byteValue() & 2) != 0;
   }

   private void n(boolean var1) {
      this.datawatcher.b(a, Byte.valueOf(this.a(((Byte)this.datawatcher.a(a)).byteValue(), 4, var1)));
   }

   public boolean s() {
      return (((Byte)this.datawatcher.a(a)).byteValue() & 4) != 0;
   }

   private void o(boolean var1) {
      this.datawatcher.b(a, Byte.valueOf(this.a(((Byte)this.datawatcher.a(a)).byteValue(), 8, var1)));
   }

   public boolean t() {
      return (((Byte)this.datawatcher.a(a)).byteValue() & 8) != 0;
   }

   private void p(boolean var1) {
      this.datawatcher.b(a, Byte.valueOf(this.a(((Byte)this.datawatcher.a(a)).byteValue(), 16, var1)));
   }

   public boolean u() {
      return (((Byte)this.datawatcher.a(a)).byteValue() & 16) != 0;
   }

   private byte a(byte var1, int var2, boolean var3) {
      if(var3) {
         var1 = (byte)(var1 | var2);
      } else {
         var1 = (byte)(var1 & ~var2);
      }

      return var1;
   }

   public void a(class_dc var1) {
      this.bB = var1;
      this.datawatcher.b(b, var1);
   }

   public void b(class_dc var1) {
      this.bC = var1;
      this.datawatcher.b(c, var1);
   }

   public void c(class_dc var1) {
      this.bD = var1;
      this.datawatcher.b(d, var1);
   }

   public void d(class_dc var1) {
      this.bE = var1;
      this.datawatcher.b(e, var1);
   }

   public void e(class_dc var1) {
      this.bF = var1;
      this.datawatcher.b(f, var1);
   }

   public void f(class_dc var1) {
      this.bG = var1;
      this.datawatcher.b(g, var1);
   }

   public class_dc w() {
      return this.bB;
   }

   public class_dc x() {
      return this.bC;
   }

   public boolean ao() {
      return super.ao() && !this.u();
   }

   protected class_nf e(int var1) {
      return class_ng.k;
   }

   protected class_nf bQ() {
      return class_ng.l;
   }

   protected class_nf bR() {
      return class_ng.j;
   }

   public void a(class_xz var1) {
   }

   public boolean cC() {
      return false;
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumInventorySlot.EnumSlotType.values().length];

      static {
         try {
            a[EnumInventorySlot.EnumSlotType.HAND.ordinal()] = 1;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumInventorySlot.EnumSlotType.ARMOR.ordinal()] = 2;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
