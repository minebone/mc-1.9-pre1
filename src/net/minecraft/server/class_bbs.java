package net.minecraft.server;

import net.minecraft.server.IScoreboardCriteria;

public class class_bbs implements IScoreboardCriteria {
   private final String o;

   public class_bbs(String var1) {
      this.o = var1;
      IScoreboardCriteria.a.put(var1, this);
   }

   public String a() {
      return this.o;
   }

   public boolean b() {
      return false;
   }

   public IScoreboardCriteria.EnumScoreboardHealthDisplay c() {
      return IScoreboardCriteria.EnumScoreboardHealthDisplay.INTEGER;
   }
}
