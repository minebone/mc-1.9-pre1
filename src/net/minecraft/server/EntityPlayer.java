package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mojang.authlib.GameProfile;
import io.netty.buffer.Unpooled;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.Achievement;
import net.minecraft.server.AchievementList;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.Block;
import net.minecraft.server.BlockCobbleWall;
import net.minecraft.server.BlockFenceGate;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.ChatModifier;
import net.minecraft.server.ChestLock;
import net.minecraft.server.Container;
import net.minecraft.server.CrashReport;
import net.minecraft.server.CrashReportSystemDetails;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityTypes;
import net.minecraft.server.EnumChatFormat;
import net.minecraft.server.EnumMainHandOption;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.IScoreboardCriteria;
import net.minecraft.server.InventoryMerchant;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MobEffect;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.OpListEntry;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketPlayOutCombatEvent;
import net.minecraft.server.PacketPlayOutCustomPayload;
import net.minecraft.server.PacketPlayOutGameStateChange;
import net.minecraft.server.PacketPlayOutResourcePackSend;
import net.minecraft.server.PlayerConnection;
import net.minecraft.server.ScoreboardScore;
import net.minecraft.server.ScoreboardTeamBase;
import net.minecraft.server.ServerStatisticManager;
import net.minecraft.server.Statistic;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.WorldServer;
import net.minecraft.server.WorldSettings;
import net.minecraft.server.class_aaz;
import net.minecraft.server.class_aba;
import net.minecraft.server.class_abj;
import net.minecraft.server.class_abn;
import net.minecraft.server.class_abr;
import net.minecraft.server.class_aco;
import net.minecraft.server.class_ado;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aej;
import net.minecraft.server.class_ahe;
import net.minecraft.server.class_ahg;
import net.minecraft.server.class_ali;
import net.minecraft.server.class_apx;
import net.minecraft.server.class_aqm;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.class_bbk;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_e;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutAnimation;
import net.minecraft.server.PacketPlayOutChat;
import net.minecraft.server.PacketPlayOutCloseWindow;
import net.minecraft.server.PacketPlayOutOpenWindow;
import net.minecraft.server.PacketPlayOutWindowItems;
import net.minecraft.server.PacketPlayOutWindowData;
import net.minecraft.server.PacketPlayOutSetSlot;
import net.minecraft.server.PacketPlayOutEntityStatus;
import net.minecraft.server.PacketPlayOutWorldEvent;
import net.minecraft.server.PacketPlayOutOpenSignEditor;
import net.minecraft.server.PacketPlayoutAbilities;
import net.minecraft.server.PacketPlayOutBed;
import net.minecraft.server.PacketPlayOutEntityDestroy;
import net.minecraft.server.PacketPlayOutRemoveEntityEffect;
import net.minecraft.server.PacketPlayOutCamera;
import net.minecraft.server.PacketPlayOutExperience;
import net.minecraft.server.PacketPlayOutUpdateHealth;
import net.minecraft.server.PacketPlayOutNamedSoundSomething;
import net.minecraft.server.PacketPlayOutEntityEffect;
import net.minecraft.server.class_il;
import net.minecraft.server.PlayerInteractManager;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.class_nv;
import net.minecraft.server.MathHelper;
import net.minecraft.server.IInventory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_qm;
import net.minecraft.server.class_qr;
import net.minecraft.server.class_qt;
import net.minecraft.server.class_rc;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_wj;
import net.minecraft.server.class_zl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EntityPlayer extends EntityHuman implements class_aaz {
   private static final Logger bQ = LogManager.getLogger();
   private String bR = "en_US";
   public PlayerConnection a;
   public final MinecraftServer server;
   public final PlayerInteractManager c;
   public double d;
   public double e;
   private final List bS = Lists.newLinkedList();
   private final ServerStatisticManager bT;
   private float bU = Float.MIN_VALUE;
   private int bV = Integer.MIN_VALUE;
   private int bW = Integer.MIN_VALUE;
   private int bX = Integer.MIN_VALUE;
   private int bY = Integer.MIN_VALUE;
   private int bZ = Integer.MIN_VALUE;
   private float ca = -1.0E8F;
   private int cb = -99999999;
   private boolean cc = true;
   private int cd = -99999999;
   private int ce = 60;
   private EntityHuman.EnumChatVisibility cf;
   private boolean cg = true;
   private long ch = System.currentTimeMillis();
   private Entity ci = null;
   private boolean pendingTeleport;
   private int ck;
   public boolean f;
   public int g;
   public boolean h;

   public EntityPlayer(MinecraftServer var1, WorldServer var2, GameProfile var3, PlayerInteractManager var4) {
      super(var2, var3);
      var4.player = this;
      this.c = var4;
      BlockPosition var5 = var2.R();
      if(!var2.s.isNotOverworld() && var2.T().q() != WorldSettings.EnumGamemode.ADVENTURE) {
         int var6 = Math.max(0, var1.a(var2));
         int var7 = MathHelper.c(var2.aj().b((double)var5.p(), (double)var5.r()));
         if(var7 < var6) {
            var6 = var7;
         }

         if(var7 <= 1) {
            var6 = 1;
         }

         var5 = var2.q(var5.a(this.random.nextInt(var6 * 2 + 1) - var6, 0, this.random.nextInt(var6 * 2 + 1) - var6));
      }

      this.server = var1;
      this.bT = var1.getPlayerList().a((EntityHuman)this);
      this.P = 0.0F;
      this.a(var5, 0.0F, 0.0F);

      while(!var2.a((Entity)this, (AxisAlignedBB)this.bk()).isEmpty() && this.locY < 255.0D) {
         this.b(this.locX, this.locY + 1.0D, this.locZ);
      }

   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      if(var1.b("playerGameType", 99)) {
         if(this.h().at()) {
            this.c.a(this.h().n());
         } else {
            this.c.a(WorldSettings.EnumGamemode.a(var1.h("playerGameType")));
         }
      }

   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("playerGameType", this.c.b().a());
      Entity var2 = this.bv();
      if(this.bx() != null && var2 != this & var2.b(EntityPlayer.class).size() == 1) {
         NBTTagCompound var3 = new NBTTagCompound();
         NBTTagCompound var4 = new NBTTagCompound();
         var2.d(var4);
         var3.a("Attach", this.bx().getUniqueId());
         var3.a((String)"Entity", (NBTTag)var4);
         var1.a((String)"RootVehicle", (NBTTag)var3);
      }

   }

   public void a(int var1) {
      super.a(var1);
      this.cd = -1;
   }

   public void b(int var1) {
      super.b(var1);
      this.cd = -1;
   }

   public void j_() {
      this.bt.a((class_aaz)this);
   }

   public void j() {
      super.j();
      this.a.a((Packet)(new PacketPlayOutCombatEvent(this.bT(), PacketPlayOutCombatEvent.EnumCombatEventType.ENTER_COMBAT)));
   }

   public void k() {
      super.k();
      this.a.a((Packet)(new PacketPlayOutCombatEvent(this.bT(), PacketPlayOutCombatEvent.EnumCombatEventType.END_COMBAT)));
   }

   protected class_ado l() {
      return new class_aej(this);
   }

   public void m() {
      this.c.a();
      --this.ce;
      if(this.noDamageTicks > 0) {
         --this.noDamageTicks;
      }

      this.bt.b();
      if(!this.world.E && !this.bt.a((EntityHuman)this)) {
         this.q();
         this.bt = this.bs;
      }

      while(!this.bS.isEmpty()) {
         int var1 = Math.min(this.bS.size(), Integer.MAX_VALUE);
         int[] var2 = new int[var1];
         Iterator var3 = this.bS.iterator();
         int var4 = 0;

         while(var3.hasNext() && var4 < var1) {
            var2[var4++] = ((Integer)var3.next()).intValue();
            var3.remove();
         }

         this.a.a((Packet)(new PacketPlayOutEntityDestroy(var2)));
      }

      Entity var5 = this.G();
      if(var5 != this) {
         if(!var5.at()) {
            this.e(this);
         } else {
            this.move(var5.locX, var5.locY, var5.locZ, var5.yaw, var5.pitch);
            this.server.getPlayerList().d(this);
            if(this.aJ()) {
               this.e(this);
            }
         }
      }

   }

   public void k_() {
      try {
         super.m();

         for(int var1 = 0; var1 < this.br.u_(); ++var1) {
            ItemStack var5 = this.br.a(var1);
            if(var5 != null && var5.b().f()) {
               Packet var6 = ((class_aco)var5.b()).a(var5, this.world, this);
               if(var6 != null) {
                  this.a.a(var6);
               }
            }
         }

         if(this.bP() != this.ca || this.cb != this.bu.a() || this.bu.e() == 0.0F != this.cc) {
            this.a.a((Packet)(new PacketPlayOutUpdateHealth(this.bP(), this.bu.a(), this.bu.e())));
            this.ca = this.bP();
            this.cb = this.bu.a();
            this.cc = this.bu.e() == 0.0F;
         }

         if(this.bP() + this.co() != this.bU) {
            this.bU = this.bP() + this.co();
            this.a(IScoreboardCriteria.g, MathHelper.f(this.bU));
         }

         if(this.bu.a() != this.bV) {
            this.bV = this.bu.a();
            this.a(IScoreboardCriteria.h, MathHelper.f((float)this.bV));
         }

         if(this.aO() != this.bW) {
            this.bW = this.aO();
            this.a(IScoreboardCriteria.i, MathHelper.f((float)this.bW));
         }

         if(this.bS() != this.bX) {
            this.bX = this.bS();
            this.a(IScoreboardCriteria.j, MathHelper.f((float)this.bX));
         }

         if(this.bL != this.bZ) {
            this.bZ = this.bL;
            this.a(IScoreboardCriteria.k, MathHelper.f((float)this.bZ));
         }

         if(this.bK != this.bY) {
            this.bY = this.bK;
            this.a(IScoreboardCriteria.l, MathHelper.f((float)this.bY));
         }

         if(this.bL != this.cd) {
            this.cd = this.bL;
            this.a.a((Packet)(new PacketPlayOutExperience(this.bM, this.bL, this.bK)));
         }

         if(this.ticksLived % 20 * 5 == 0 && !this.E().a((Achievement)AchievementList.L)) {
            this.o();
         }

      } catch (Throwable var4) {
         CrashReport var2 = CrashReport.a(var4, "Ticking player");
         CrashReportSystemDetails var3 = var2.a("Player being ticked");
         this.a((CrashReportSystemDetails)var3);
         throw new class_e(var2);
      }
   }

   private void a(IScoreboardCriteria var1, int var2) {
      Collection var3 = this.cV().a(var1);
      Iterator var4 = var3.iterator();

      while(var4.hasNext()) {
         class_bbk var5 = (class_bbk)var4.next();
         ScoreboardScore var6 = this.cV().c(this.h_(), var5);
         var6.c(var2);
      }

   }

   protected void o() {
      BiomeBase var1 = this.world.b(new BlockPosition(MathHelper.c(this.locX), 0, MathHelper.c(this.locZ)));
      String var2 = var1.l();
      class_nv var3 = (class_nv)this.E().b(AchievementList.L);
      if(var3 == null) {
         var3 = (class_nv)this.E().a(AchievementList.L, new class_nv());
      }

      var3.add(var2);
      if(this.E().b(AchievementList.L) && var3.size() >= BiomeBase.i.size()) {
         HashSet var4 = Sets.newHashSet((Iterable)BiomeBase.i);
         Iterator var5 = var3.iterator();

         while(var5.hasNext()) {
            String var6 = (String)var5.next();
            Iterator var7 = var4.iterator();

            while(var7.hasNext()) {
               BiomeBase var8 = (BiomeBase)var7.next();
               if(var8.l().equals(var6)) {
                  var7.remove();
               }
            }

            if(var4.isEmpty()) {
               break;
            }
         }

         if(var4.isEmpty()) {
            this.b((Statistic)AchievementList.L);
         }
      }

   }

   public void a(DamageSource var1) {
      boolean var2 = this.world.U().b("showDeathMessages");
      this.a.a((Packet)(new PacketPlayOutCombatEvent(this.bT(), PacketPlayOutCombatEvent.EnumCombatEventType.ENTITY_DIED, var2)));
      if(var2) {
         ScoreboardTeamBase var3 = this.aN();
         if(var3 != null && var3.j() != ScoreboardTeamBase.EnumNameTagVisibility.ALWAYS) {
            if(var3.j() == ScoreboardTeamBase.EnumNameTagVisibility.HIDE_FOR_OTHER_TEAMS) {
               this.server.getPlayerList().a((EntityHuman)this, (IChatBaseComponent)this.bT().b());
            } else if(var3.j() == ScoreboardTeamBase.EnumNameTagVisibility.HIDE_FOR_OWN_TEAM) {
               this.server.getPlayerList().b((EntityHuman)this, (IChatBaseComponent)this.bT().b());
            }
         } else {
            this.server.getPlayerList().a(this.bT().b());
         }
      }

      if(!this.world.U().b("keepInventory") && !this.y()) {
         this.br.n();
      }

      Collection var7 = this.world.ad().a(IScoreboardCriteria.d);
      Iterator var4 = var7.iterator();

      while(var4.hasNext()) {
         class_bbk var5 = (class_bbk)var4.next();
         ScoreboardScore var6 = this.cV().c(this.h_(), var5);
         var6.a();
      }

      class_rz var8 = this.bU();
      if(var8 != null) {
         EntityTypes.class_a_in_class_rs var9 = (EntityTypes.class_a_in_class_rs)EntityTypes.a.get(EntityTypes.b((Entity)var8));
         if(var9 != null) {
            this.b((Statistic)var9.e);
         }

         var8.b(this, this.ba);
      }

      this.b((Statistic)StatisticList.A);
      this.a(StatisticList.h);
      this.bT().g();
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b((DamageSource)var1)) {
         return false;
      } else {
         boolean var3 = this.server.aa() && this.db() && "fall".equals(var1.r);
         if(!var3 && this.ce > 0 && var1 != DamageSource.k) {
            return false;
         } else {
            if(var1 instanceof class_rc) {
               Entity var4 = var1.j();
               if(var4 instanceof EntityHuman && !this.a((EntityHuman)var4)) {
                  return false;
               }

               if(var4 instanceof class_zl) {
                  class_zl var5 = (class_zl)var4;
                  if(var5.e instanceof EntityHuman && !this.a((EntityHuman)var5.e)) {
                     return false;
                  }
               }
            }

            return super.a(var1, var2);
         }
      }
   }

   public boolean a(EntityHuman var1) {
      return !this.db()?false:super.a(var1);
   }

   private boolean db() {
      return this.server.af();
   }

   public Entity c(int var1) {
      this.pendingTeleport = true;
      if(this.dimension == 1 && var1 == 1) {
         this.b((Statistic)AchievementList.D);
         this.world.e((Entity)this);
         this.h = true;
         this.a.a((Packet)(new PacketPlayOutGameStateChange(4, 0.0F)));
         return this;
      } else {
         if(this.dimension == 0 && var1 == 1) {
            this.b((Statistic)AchievementList.C);
            var1 = 1;
         } else {
            this.b((Statistic)AchievementList.y);
         }

         this.server.getPlayerList().a(this, var1);
         this.a.a((Packet)(new PacketPlayOutWorldEvent(1032, BlockPosition.a, 0, false)));
         this.cd = -1;
         this.ca = -1.0F;
         this.cb = -1;
         return this;
      }
   }

   public boolean a(EntityPlayer var1) {
      return var1.y()?this.G() == this:(this.y()?false:super.a((EntityPlayer)var1));
   }

   private void a(TileEntity var1) {
      if(var1 != null) {
         Packet var2 = var1.D_();
         if(var2 != null) {
            this.a.a(var2);
         }
      }

   }

   public void a(Entity var1, int var2) {
      super.a(var1, var2);
      this.bt.b();
   }

   public EntityHuman.EnumBedResult a(BlockPosition var1) {
      EntityHuman.EnumBedResult var2 = super.a(var1);
      if(var2 == EntityHuman.EnumBedResult.OK) {
         this.b((Statistic)StatisticList.ad);
         PacketPlayOutBed var3 = new PacketPlayOutBed(this, var1);
         this.x().v().a((Entity)this, (Packet)var3);
         this.a.a(this.locX, this.locY, this.locZ, this.yaw, this.pitch);
         this.a.a((Packet)var3);
      }

      return var2;
   }

   public void a(boolean var1, boolean var2, boolean var3) {
      if(this.ck()) {
         this.x().v().b(this, new PacketPlayOutAnimation(this, 2));
      }

      super.a(var1, var2, var3);
      if(this.a != null) {
         this.a.a(this.locX, this.locY, this.locZ, this.yaw, this.pitch);
      }

   }

   public boolean a(Entity var1, boolean var2) {
      Entity var3 = this.bx();
      if(!super.a((Entity)var1, var2)) {
         return false;
      } else {
         Entity var4 = this.bx();
         if(var4 != var3 && this.a != null) {
            this.a.a(this.locX, this.locY, this.locZ, this.yaw, this.pitch);
         }

         return true;
      }
   }

   public void p() {
      Entity var1 = this.bx();
      super.p();
      Entity var2 = this.bx();
      if(var2 != var1 && this.a != null) {
         this.a.a(this.locX, this.locY, this.locZ, this.yaw, this.pitch);
      }

   }

   protected void a(double var1, boolean var3, IBlockData var4, BlockPosition var5) {
   }

   protected void b(BlockPosition var1) {
      if(!this.y()) {
         super.b((BlockPosition)var1);
      }

   }

   public void a(double var1, boolean var3) {
      int var4 = MathHelper.c(this.locX);
      int var5 = MathHelper.c(this.locY - 0.20000000298023224D);
      int var6 = MathHelper.c(this.locZ);
      BlockPosition var7 = new BlockPosition(var4, var5, var6);
      IBlockData var8 = this.world.getType(var7);
      if(var8.getMaterial() == Material.a) {
         BlockPosition var9 = var7.b();
         IBlockData var10 = this.world.getType(var9);
         Block var11 = var10.getBlock();
         if(var11 instanceof class_ali || var11 instanceof BlockCobbleWall || var11 instanceof BlockFenceGate) {
            var7 = var9;
            var8 = var10;
         }
      }

      super.a(var1, var3, var8, var7);
   }

   public void a(class_aqm var1) {
      var1.a((EntityHuman)this);
      this.a.a((Packet)(new PacketPlayOutOpenSignEditor(var1.v())));
   }

   private void dc() {
      this.ck = this.ck % 100 + 1;
   }

   public void a(class_qm var1) {
      if(var1 instanceof class_qt && ((class_qt)var1).b() != null && this.y()) {
         this.a((new ChatMessage("container.spectatorCantOpen", new Object[0])).a((new ChatModifier()).a(EnumChatFormat.RED)));
      } else {
         this.dc();
         this.a.a((Packet)(new PacketPlayOutOpenWindow(this.ck, var1.k(), var1.i_())));
         this.bt = var1.a(this.br, this);
         this.bt.d = this.ck;
         this.bt.a((class_aaz)this);
      }
   }

   public void openContainer(IInventory var1) {
      if(var1 instanceof class_qt && ((class_qt)var1).b() != null && this.y()) {
         this.a((new ChatMessage("container.spectatorCantOpen", new Object[0])).a((new ChatModifier()).a(EnumChatFormat.RED)));
      } else {
         if(this.bt != this.bs) {
            this.q();
         }

         if(var1 instanceof class_qr) {
            class_qr var2 = (class_qr)var1;
            if(var2.x_() && !this.a((ChestLock)var2.y_()) && !this.y()) {
               this.a.a((Packet)(new PacketPlayOutChat(new ChatMessage("container.isLocked", new Object[]{var1.i_()}), (byte)2)));
               this.a.a((Packet)(new PacketPlayOutNamedSoundSomething(class_ng.U, EnumSoundCategory.BLOCKS, this.locX, this.locY, this.locZ, 1.0F, 1.0F)));
               return;
            }
         }

         this.dc();
         if(var1 instanceof class_qm) {
            this.a.a((Packet)(new PacketPlayOutOpenWindow(this.ck, ((class_qm)var1).k(), var1.i_(), var1.u_())));
            this.bt = ((class_qm)var1).a(this.br, this);
         } else {
            this.a.a((Packet)(new PacketPlayOutOpenWindow(this.ck, "minecraft:container", var1.i_(), var1.u_())));
            this.bt = new class_aba(this.br, var1, this);
         }

         this.bt.d = this.ck;
         this.bt.a((class_aaz)this);
      }
   }

   public void a(class_ahe var1) {
      this.dc();
      this.bt = new class_abn(this.br, var1, this.world);
      this.bt.d = this.ck;
      this.bt.a((class_aaz)this);
      InventoryMerchant var2 = ((class_abn)this.bt).e();
      IChatBaseComponent var3 = var1.i_();
      this.a.a((Packet)(new PacketPlayOutOpenWindow(this.ck, "minecraft:villager", var3, var2.u_())));
      class_ahg var4 = var1.b_(this);
      if(var4 != null) {
         PacketDataSerializer var5 = new PacketDataSerializer(Unpooled.buffer());
         var5.writeInt(this.ck);
         var4.a(var5);
         this.a.a((Packet)(new PacketPlayOutCustomPayload("MC|TrList", var5)));
      }

   }

   public void a(class_wj var1, IInventory var2) {
      if(this.bt != this.bs) {
         this.q();
      }

      this.dc();
      this.a.a((Packet)(new PacketPlayOutOpenWindow(this.ck, "EntityHorse", var2.i_(), var2.u_(), var1.getId())));
      this.bt = new class_abj(this.br, var2, var1, this);
      this.bt.d = this.ck;
      this.bt.a((class_aaz)this);
   }

   public void a(ItemStack var1, EnumHand var2) {
      Item var3 = var1.b();
      if(var3 == Items.bX) {
         PacketDataSerializer var4 = new PacketDataSerializer(Unpooled.buffer());
         var4.a((Enum)var2);
         this.a.a((Packet)(new PacketPlayOutCustomPayload("MC|BOpen", var4)));
      }

   }

   public void a(class_apx var1) {
      if(this.a(2, "")) {
         var1.d(true);
         this.a((TileEntity)var1);
      }

   }

   public void a(Container var1, int var2, ItemStack var3) {
      if(!(var1.a(var2) instanceof class_abr)) {
         if(!this.f) {
            this.a.a((Packet)(new PacketPlayOutSetSlot(var1.d, var2, var3)));
         }
      }
   }

   public void a(Container var1) {
      this.a(var1, var1.a());
   }

   public void a(Container var1, List var2) {
      this.a.a((Packet)(new PacketPlayOutWindowItems(var1.d, var2)));
      this.a.a((Packet)(new PacketPlayOutSetSlot(-1, -1, this.br.o())));
   }

   public void a(Container var1, int var2, int var3) {
      this.a.a((Packet)(new PacketPlayOutWindowData(var1.d, var2, var3)));
   }

   public void a(Container var1, IInventory var2) {
      for(int var3 = 0; var3 < var2.g(); ++var3) {
         this.a.a((Packet)(new PacketPlayOutWindowData(var1.d, var3, var2.c_(var3))));
      }

   }

   public void q() {
      this.a.a((Packet)(new PacketPlayOutCloseWindow(this.bt.d)));
      this.s();
   }

   public void r() {
      if(!this.f) {
         this.a.a((Packet)(new PacketPlayOutSetSlot(-1, -1, this.br.o())));
      }
   }

   public void s() {
      this.bt.b((EntityHuman)this);
      this.bt = this.bs;
   }

   public void a(float var1, float var2, boolean var3, boolean var4) {
      if(this.aH()) {
         if(var1 >= -1.0F && var1 <= 1.0F) {
            this.bd = var1;
         }

         if(var2 >= -1.0F && var2 <= 1.0F) {
            this.be = var2;
         }

         this.bc = var3;
         this.d(var4);
      }

   }

   public void a(Statistic var1, int var2) {
      if(var1 != null) {
         this.bT.b(this, var1, var2);
         Iterator var3 = this.cV().a(var1.k()).iterator();

         while(var3.hasNext()) {
            class_bbk var4 = (class_bbk)var3.next();
            this.cV().c(this.h_(), var4).a(var2);
         }

         if(this.bT.e()) {
            this.bT.a(this);
         }

      }
   }

   public void a(Statistic var1) {
      if(var1 != null) {
         this.bT.a(this, var1, 0);
         Iterator var2 = this.cV().a(var1.k()).iterator();

         while(var2.hasNext()) {
            class_bbk var3 = (class_bbk)var2.next();
            this.cV().c(this.h_(), var3).c(0);
         }

         if(this.bT.e()) {
            this.bT.a(this);
         }

      }
   }

   public void t() {
      this.ay();
      if(this.bF) {
         this.a(true, false, false);
      }

   }

   public void u() {
      this.ca = -1.0E8F;
   }

   public void b(IChatBaseComponent var1) {
      this.a.a((Packet)(new PacketPlayOutChat(var1)));
   }

   protected void v() {
      if(this.bm != null && this.cr()) {
         this.a.a((Packet)(new PacketPlayOutEntityStatus(this, (byte)9)));
         super.v();
      }

   }

   public void a(EntityHuman var1, boolean var2) {
      super.a(var1, var2);
      this.cd = -1;
      this.ca = -1.0F;
      this.cb = -1;
      this.bS.addAll(((EntityPlayer)var1).bS);
   }

   protected void a(MobEffect var1) {
      super.a((MobEffect)var1);
      this.a.a((Packet)(new PacketPlayOutEntityEffect(this.getId(), var1)));
   }

   protected void a(MobEffect var1, boolean var2) {
      super.a((MobEffect)var1, var2);
      this.a.a((Packet)(new PacketPlayOutEntityEffect(this.getId(), var1)));
   }

   protected void b(MobEffect var1) {
      super.b((MobEffect)var1);
      this.a.a((Packet)(new PacketPlayOutRemoveEntityEffect(this.getId(), var1.a())));
   }

   public void a(double var1, double var3, double var5) {
      this.a.a(var1, var3, var5, this.yaw, this.pitch);
   }

   public void a(Entity var1) {
      this.x().v().b(this, new PacketPlayOutAnimation(var1, 4));
   }

   public void b(Entity var1) {
      this.x().v().b(this, new PacketPlayOutAnimation(var1, 5));
   }

   public void updateAbilities() {
      if(this.a != null) {
         this.a.a((Packet)(new PacketPlayoutAbilities(this.abilities)));
         this.F();
      }
   }

   public WorldServer x() {
      return (WorldServer)this.world;
   }

   public void a(WorldSettings.EnumGamemode var1) {
      this.c.a(var1);
      this.a.a((Packet)(new PacketPlayOutGameStateChange(3, (float)var1.a())));
      if(var1 == WorldSettings.EnumGamemode.SPECTATOR) {
         this.p();
      } else {
         this.e(this);
      }

      this.updateAbilities();
      this.cp();
   }

   public boolean y() {
      return this.c.b() == WorldSettings.EnumGamemode.SPECTATOR;
   }

   public boolean l_() {
      return this.c.b() == WorldSettings.EnumGamemode.CREATIVE;
   }

   public void a(IChatBaseComponent var1) {
      this.a.a((Packet)(new PacketPlayOutChat(var1)));
   }

   public boolean a(int var1, String var2) {
      if("seed".equals(var2) && !this.server.aa()) {
         return true;
      } else if(!"tell".equals(var2) && !"help".equals(var2) && !"me".equals(var2) && !"trigger".equals(var2)) {
         if(this.server.getPlayerList().h(this.getProfile())) {
            OpListEntry var3 = (OpListEntry)this.server.getPlayerList().m().getOpListEntry(this.getProfile());
            return var3 != null?var3.a() >= var1:this.server.q() >= var1;
         } else {
            return false;
         }
      } else {
         return true;
      }
   }

   public String A() {
      String var1 = this.a.a.b().toString();
      var1 = var1.substring(var1.indexOf("/") + 1);
      var1 = var1.substring(0, var1.indexOf(":"));
      return var1;
   }

   public void a(class_il var1) {
      this.bR = var1.a();
      this.cf = var1.c();
      this.cg = var1.d();
      this.Q().b(bp, Byte.valueOf((byte)var1.e()));
      this.Q().b(bq, Byte.valueOf((byte)(var1.f() == EnumMainHandOption.LEFT?0:1)));
   }

   public EntityHuman.EnumChatVisibility C() {
      return this.cf;
   }

   public void a(String var1, String var2) {
      this.a.a((Packet)(new PacketPlayOutResourcePackSend(var1, var2)));
   }

   public BlockPosition c() {
      return new BlockPosition(this.locX, this.locY + 0.5D, this.locZ);
   }

   public void D() {
      this.ch = MinecraftServer.av();
   }

   public ServerStatisticManager E() {
      return this.bT;
   }

   public void c(Entity var1) {
      if(var1 instanceof EntityHuman) {
         this.a.a((Packet)(new PacketPlayOutEntityDestroy(new int[]{var1.getId()})));
      } else {
         this.bS.add(Integer.valueOf(var1.getId()));
      }

   }

   public void d(Entity var1) {
      this.bS.remove(Integer.valueOf(var1.getId()));
   }

   protected void F() {
      if(this.y()) {
         this.bL();
         this.g(true);
      } else {
         super.F();
      }

      this.x().v().a(this);
   }

   public Entity G() {
      return (Entity)(this.ci == null?this:this.ci);
   }

   public void e(Entity var1) {
      Entity var2 = this.G();
      this.ci = (Entity)(var1 == null?this:var1);
      if(var2 != this.ci) {
         this.a.a((Packet)(new PacketPlayOutCamera(this.ci)));
         this.a(this.ci.locX, this.ci.locY, this.ci.locZ);
      }

   }

   protected void H() {
      if(this.portalCooldown > 0 && !this.pendingTeleport) {
         --this.portalCooldown;
      }

   }

   public void f(Entity var1) {
      if(this.c.b() == WorldSettings.EnumGamemode.SPECTATOR) {
         this.e(var1);
      } else {
         super.f(var1);
      }

   }

   public long I() {
      return this.ch;
   }

   public IChatBaseComponent J() {
      return null;
   }

   public void a(EnumHand var1) {
      super.a((EnumHand)var1);
      this.cY();
   }

   public boolean isPendingTeleport() {
      return this.pendingTeleport;
   }

   public void setPendingTeleport() {
      this.pendingTeleport = false;
   }

   public void M() {
      this.b(7, true);
   }
}
