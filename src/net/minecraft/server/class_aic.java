package net.minecraft.server;

import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.class_ov;

public class class_aic extends class_ov.class_a_in_class_ov {
   private final NBTTagCompound b;

   public class_aic() {
      super(1);
      this.b = new NBTTagCompound();
      this.b.a("id", "Pig");
   }

   public class_aic(NBTTagCompound var1) {
      this(var1.b("Weight", 99)?var1.h("Weight"):1, var1.o("Entity"));
   }

   public class_aic(int var1, NBTTagCompound var2) {
      super(var1);
      this.b = var2;
   }

   public NBTTagCompound a() {
      NBTTagCompound var1 = new NBTTagCompound();
      var1.a((String)"Entity", (NBTTag)this.b);
      var1.a("Weight", this.a);
      return var1;
   }

   public NBTTagCompound b() {
      return this.b;
   }
}
