package net.minecraft.server;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSyntaxException;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_bab;
import net.minecraft.server.class_bae;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_kk;
import net.minecraft.server.EnumInventorySlot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_bah extends class_bae {
   private static final Logger a = LogManager.getLogger();
   private final class_bah.class_a_in_class_bah[] b;

   public class_bah(class_baq[] var1, class_bah.class_a_in_class_bah[] var2) {
      super(var1);
      this.b = var2;
   }

   public ItemStack a(ItemStack var1, Random var2, class_azy var3) {
      class_bah.class_a_in_class_bah[] var4 = this.b;
      int var5 = var4.length;

      for(int var6 = 0; var6 < var5; ++var6) {
         class_bah.class_a_in_class_bah var7 = var4[var6];
         UUID var8 = var7.e;
         if(var8 == null) {
            var8 = UUID.randomUUID();
         }

         EnumInventorySlot var9 = var7.f[var2.nextInt(var7.f.length)];
         var1.a(var7.b, new AttributeModifier(var8, var7.a, (double)var7.d.b(var2), var7.c), var9);
      }

      return var1;
   }

   static class class_a_in_class_bah {
      private final String a;
      private final String b;
      private final int c;
      private final class_bab d;
      private final UUID e;
      private final EnumInventorySlot[] f;

      private class_a_in_class_bah(String var1, String var2, int var3, class_bab var4, EnumInventorySlot[] var5, UUID var6) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
         this.d = var4;
         this.e = var6;
         this.f = var5;
      }

      public JsonObject a(JsonSerializationContext var1) {
         JsonObject var2 = new JsonObject();
         var2.addProperty("name", this.a);
         var2.addProperty("attribute", this.b);
         var2.addProperty("operation", a(this.c));
         var2.add("amount", var1.serialize(this.d));
         if(this.e != null) {
            var2.addProperty("id", this.e.toString());
         }

         if(this.f.length == 1) {
            var2.addProperty("slot", this.f[0].d());
         } else {
            JsonArray var3 = new JsonArray();
            EnumInventorySlot[] var4 = this.f;
            int var5 = var4.length;

            for(int var6 = 0; var6 < var5; ++var6) {
               EnumInventorySlot var7 = var4[var6];
               var3.add(new JsonPrimitive(var7.d()));
            }

            var2.add("slot", var3);
         }

         return var2;
      }

      public static class_bah.class_a_in_class_bah a(JsonObject var0, JsonDeserializationContext var1) {
         String var2 = ChatDeserializer.h(var0, "name");
         String var3 = ChatDeserializer.h(var0, "attribute");
         int var4 = a(ChatDeserializer.h(var0, "operation"));
         class_bab var5 = (class_bab)ChatDeserializer.a(var0, "amount", var1, class_bab.class);
         UUID var7 = null;
         EnumInventorySlot[] var6;
         if(ChatDeserializer.a(var0, "slot")) {
            var6 = new EnumInventorySlot[]{EnumInventorySlot.a(ChatDeserializer.h(var0, "slot"))};
         } else {
            if(!ChatDeserializer.d(var0, "slot")) {
               throw new JsonSyntaxException("Invalid or missing attribute modifier slot; must be either string or array of strings.");
            }

            JsonArray var8 = ChatDeserializer.u(var0, "slot");
            var6 = new EnumInventorySlot[var8.size()];
            int var9 = 0;

            JsonElement var11;
            for(Iterator var10 = var8.iterator(); var10.hasNext(); var6[var9++] = EnumInventorySlot.a(ChatDeserializer.a(var11, "slot"))) {
               var11 = (JsonElement)var10.next();
            }

            if(var6.length == 0) {
               throw new JsonSyntaxException("Invalid attribute modifier slot; must contain at least one entry.");
            }
         }

         if(var0.has("id")) {
            String var13 = ChatDeserializer.h(var0, "id");

            try {
               var7 = UUID.fromString(var13);
            } catch (IllegalArgumentException var12) {
               throw new JsonSyntaxException("Invalid attribute modifier id \'" + var13 + "\' (must be UUID format, with dashes)");
            }
         }

         return new class_bah.class_a_in_class_bah(var2, var3, var4, var5, var6, var7);
      }

      private static String a(int var0) {
         switch(var0) {
         case 0:
            return "addition";
         case 1:
            return "multiply_base";
         case 2:
            return "multiply_total";
         default:
            throw new IllegalArgumentException("Unknown operation " + var0);
         }
      }

      private static int a(String var0) {
         if(var0.equals("addition")) {
            return 0;
         } else if(var0.equals("multiply_base")) {
            return 1;
         } else if(var0.equals("multiply_total")) {
            return 2;
         } else {
            throw new JsonSyntaxException("Unknown attribute modifier operation " + var0);
         }
      }
   }

   public static class class_b_in_class_bah extends class_bae.class_a_in_class_bae {
      public class_b_in_class_bah() {
         super(new class_kk("set_attributes"), class_bah.class);
      }

      public void a(JsonObject var1, class_bah var2, JsonSerializationContext var3) {
         JsonArray var4 = new JsonArray();
         class_bah.class_a_in_class_bah[] var5 = var2.b;
         int var6 = var5.length;

         for(int var7 = 0; var7 < var6; ++var7) {
            class_bah.class_a_in_class_bah var8 = var5[var7];
            var4.add(var8.a(var3));
         }

         var1.add("modifiers", var4);
      }

      public class_bah a(JsonObject var1, JsonDeserializationContext var2, class_baq[] var3) {
         JsonArray var4 = ChatDeserializer.u(var1, "modifiers");
         class_bah.class_a_in_class_bah[] var5 = new class_bah.class_a_in_class_bah[var4.size()];
         int var6 = 0;

         JsonElement var8;
         for(Iterator var7 = var4.iterator(); var7.hasNext(); var5[var6++] = class_bah.class_a_in_class_bah.a(ChatDeserializer.m(var8, "modifier"), var2)) {
            var8 = (JsonElement)var7.next();
         }

         if(var5.length == 0) {
            throw new JsonSyntaxException("Invalid attribute modifiers array; cannot be empty");
         } else {
            return new class_bah(var3, var5);
         }
      }

      // $FF: synthetic method
      public class_bae b(JsonObject var1, JsonDeserializationContext var2, class_baq[] var3) {
         return this.a(var1, var2, var3);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(JsonObject var1, class_bae var2, JsonSerializationContext var3) {
         this.a(var1, (class_bah)var2, var3);
      }
   }
}
