package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;

public class class_add extends Item {
   public class_add() {
      this.a(CreativeModeTab.f);
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      if(var3.E) {
         return EnumResult.SUCCESS;
      } else {
         var4 = var4.a(var6);
         if(!var2.a(var4, var6, var1)) {
            return EnumResult.FAIL;
         } else {
            if(var3.getType(var4).getMaterial() == Material.a) {
               var3.a((EntityHuman)null, var4, class_ng.bj, EnumSoundCategory.BLOCKS, 1.0F, (i.nextFloat() - i.nextFloat()) * 0.2F + 1.0F);
               var3.a(var4, Blocks.ab.u());
            }

            if(!var2.abilities.d) {
               --var1.b;
            }

            return EnumResult.SUCCESS;
         }
      }
   }
}
