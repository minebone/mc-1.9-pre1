package net.minecraft.server;

import java.util.Collection;
import net.minecraft.server.InventoryCrafting;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aff;
import net.minecraft.server.class_aft;

class class_aga implements class_aft {
   private static final ItemStack[] a = new ItemStack[9];

   public boolean a(InventoryCrafting var1, World var2) {
      if(var1.i() == 3 && var1.h() == 3) {
         for(int var3 = 0; var3 < var1.i(); ++var3) {
            for(int var4 = 0; var4 < var1.h(); ++var4) {
               ItemStack var5 = var1.c(var3, var4);
               if(var5 == null) {
                  return false;
               }

               Item var6 = var5.b();
               if(var3 == 1 && var4 == 1) {
                  if(var6 != Items.bI) {
                     return false;
                  }
               } else if(var6 != Items.g) {
                  return false;
               }
            }
         }

         return true;
      } else {
         return false;
      }
   }

   public ItemStack a(InventoryCrafting var1) {
      ItemStack var2 = var1.c(1, 1);
      if(var2 != null && var2.b() == Items.bI) {
         ItemStack var3 = new ItemStack(Items.i, 8);
         class_aff.a(var3, class_aff.c(var2));
         class_aff.a((ItemStack)var3, (Collection)class_aff.b(var2));
         return var3;
      } else {
         return null;
      }
   }

   public int a() {
      return 9;
   }

   public ItemStack b() {
      return null;
   }

   public ItemStack[] b(InventoryCrafting var1) {
      return a;
   }
}
