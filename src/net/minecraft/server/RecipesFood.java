package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.CraftingManager;
import net.minecraft.server.EnumColor;
import net.minecraft.server.Items;
import net.minecraft.server.ItemStack;

public class RecipesFood {
   public void a(CraftingManager var1) {
      var1.b(new ItemStack(Items.C), new Object[]{Blocks.P, Blocks.Q, Items.B});
      var1.a(new ItemStack(Items.bj, 8), new Object[]{"#X#", Character.valueOf('X'), new ItemStack(Items.bd, 1, EnumColor.BROWN.b()), Character.valueOf('#'), Items.Q});
      var1.a(new ItemStack(Items.bx), new Object[]{" R ", "CPM", " B ", Character.valueOf('R'), new ItemStack(Items.bw), Character.valueOf('C'), Items.cb, Character.valueOf('P'), Items.cd, Character.valueOf('M'), Blocks.P, Character.valueOf('B'), Items.B});
      var1.a(new ItemStack(Items.bx), new Object[]{" R ", "CPD", " B ", Character.valueOf('R'), new ItemStack(Items.bw), Character.valueOf('C'), Items.cb, Character.valueOf('P'), Items.cd, Character.valueOf('D'), Blocks.Q, Character.valueOf('B'), Items.B});
      var1.a(new ItemStack(Blocks.bk), new Object[]{"MMM", "MMM", "MMM", Character.valueOf('M'), Items.bm});
      var1.a(new ItemStack(Items.cW), new Object[]{"OOO", "OOO", " B ", Character.valueOf('O'), Items.cV, Character.valueOf('B'), Items.B});
      var1.a(new ItemStack(Items.bo), new Object[]{"M", Character.valueOf('M'), Items.bm});
      var1.a(new ItemStack(Items.bn, 4), new Object[]{"M", Character.valueOf('M'), Blocks.aU});
      var1.b(new ItemStack(Items.ck), new Object[]{Blocks.aU, Items.bf, Items.aW});
      var1.b(new ItemStack(Items.bM), new Object[]{Items.bL, Blocks.P, Items.bf});
      var1.b(new ItemStack(Items.bN, 2), new Object[]{Items.bC});
      var1.b(new ItemStack(Items.bO), new Object[]{Items.bN, Items.aT});
   }
}
