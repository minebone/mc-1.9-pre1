package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockBed;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aoo;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;

public class class_aca extends Item {
   public class_aca() {
      this.a(CreativeModeTab.c);
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      if(var3.E) {
         return EnumResult.SUCCESS;
      } else if(var6 != EnumDirection.UP) {
         return EnumResult.FAIL;
      } else {
         IBlockData var10 = var3.getType(var4);
         Block var11 = var10.getBlock();
         boolean var12 = var11.a((class_ahw)var3, (BlockPosition)var4);
         if(!var12) {
            var4 = var4.a();
         }

         int var13 = MathHelper.c((double)(var2.yaw * 4.0F / 360.0F) + 0.5D) & 3;
         EnumDirection var14 = EnumDirection.b(var13);
         BlockPosition var15 = var4.a(var14);
         if(var2.a(var4, var6, var1) && var2.a(var15, var6, var1)) {
            boolean var16 = var3.getType(var15).getBlock().a((class_ahw)var3, (BlockPosition)var15);
            boolean var17 = var12 || var3.d(var4);
            boolean var18 = var16 || var3.d(var15);
            if(var17 && var18 && var3.getType(var4.b()).q() && var3.getType(var15.b()).q()) {
               IBlockData var19 = Blocks.C.u().set(BlockBed.b, Boolean.valueOf(false)).set(BlockBed.D, var14).set(BlockBed.a, BlockBed.EnumBedPart.FOOT);
               if(var3.a((BlockPosition)var4, (IBlockData)var19, 11)) {
                  IBlockData var20 = var19.set(BlockBed.a, BlockBed.EnumBedPart.HEAD);
                  var3.a((BlockPosition)var15, (IBlockData)var20, 11);
               }

               class_aoo var21 = var19.getBlock().w();
               var3.a((EntityHuman)null, var4, var21.e(), EnumSoundCategory.BLOCKS, (var21.a() + 1.0F) / 2.0F, var21.b() * 0.8F);
               --var1.b;
               return EnumResult.SUCCESS;
            } else {
               return EnumResult.FAIL;
            }
         } else {
            return EnumResult.FAIL;
         }
      }
   }
}
