package net.minecraft.server;

import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.Entity;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.BlockPosition;

public interface ICommandListener {
   String h_();

   IChatBaseComponent i_();

   void a(IChatBaseComponent var1);

   boolean a(int var1, String var2);

   BlockPosition c();

   Vec3D d();

   World e();

   Entity f();

   boolean z_();

   void a(CommandObjectiveExecutor.EnumCommandResult var1, int var2);

   MinecraftServer h();
}
