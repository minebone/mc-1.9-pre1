package net.minecraft.server;

import net.minecraft.server.Container;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_zd;

public class class_up extends class_tj {
   private class_zd a;

   public class_up(class_zd var1) {
      this.a = var1;
      this.a(5);
   }

   public boolean a() {
      if(!this.a.at()) {
         return false;
      } else if(this.a.ah()) {
         return false;
      } else if(!this.a.onGround) {
         return false;
      } else if(this.a.velocityChanged) {
         return false;
      } else {
         EntityHuman var1 = this.a.t_();
         return var1 == null?false:(this.a.h(var1) > 16.0D?false:var1.bt instanceof Container);
      }
   }

   public void c() {
      this.a.x().o();
   }

   public void d() {
      this.a.a_((EntityHuman)null);
   }
}
