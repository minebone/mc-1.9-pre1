package net.minecraft.server;

import com.google.common.collect.Queues;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.local.LocalChannel;
import io.netty.channel.local.LocalEventLoopGroup;
import io.netty.channel.local.LocalServerChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.handler.timeout.TimeoutException;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.GenericFutureListener;
import java.net.SocketAddress;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import javax.crypto.SecretKey;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.EnumProtocol;
import net.minecraft.server.EnumProtocolDirection;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftEncryption;
import net.minecraft.server.PacketDecompressor;
import net.minecraft.server.class_eg;
import net.minecraft.server.class_eh;
import net.minecraft.server.class_ej;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.class_ku;
import net.minecraft.server.class_ky;
import net.minecraft.server.class_oj;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

public class NetworkManager extends SimpleChannelInboundHandler {
   private static final Logger g = LogManager.getLogger();
   public static final Marker a = MarkerManager.getMarker("NETWORK");
   public static final Marker b = MarkerManager.getMarker("NETWORK_PACKETS", a);
   public static final AttributeKey c = AttributeKey.valueOf("protocol");
   public static final class_oj d = new class_oj() {
      protected NioEventLoopGroup a() {
         return new NioEventLoopGroup(0, (new ThreadFactoryBuilder()).setNameFormat("Netty Client IO #%d").setDaemon(true).build());
      }

      // $FF: synthetic method
      protected Object b() {
         return this.a();
      }
   };
   public static final class_oj e = new class_oj() {
      protected EpollEventLoopGroup a() {
         return new EpollEventLoopGroup(0, (new ThreadFactoryBuilder()).setNameFormat("Netty Epoll Client IO #%d").setDaemon(true).build());
      }

      // $FF: synthetic method
      protected Object b() {
         return this.a();
      }
   };
   public static final class_oj f = new class_oj() {
      protected LocalEventLoopGroup a() {
         return new LocalEventLoopGroup(0, (new ThreadFactoryBuilder()).setNameFormat("Netty Local Client IO #%d").setDaemon(true).build());
      }

      // $FF: synthetic method
      protected Object b() {
         return this.a();
      }
   };
   private final EnumProtocolDirection h;
   private final Queue i = Queues.newConcurrentLinkedQueue();
   private final ReentrantReadWriteLock j = new ReentrantReadWriteLock();
   private Channel k;
   private SocketAddress l;
   private PacketListener m;
   private IChatBaseComponent n;
   private boolean o;
   private boolean p;

   public NetworkManager(EnumProtocolDirection var1) {
      this.h = var1;
   }

   public void channelActive(ChannelHandlerContext var1) throws Exception {
      super.channelActive(var1);
      this.k = var1.channel();
      this.l = this.k.remoteAddress();

      try {
         this.a(EnumProtocol.HANDSHAKING);
      } catch (Throwable var3) {
         g.fatal((Object)var3);
      }

   }

   public void a(EnumProtocol var1) {
      this.k.attr(c).set(var1);
      this.k.config().setAutoRead(true);
      g.debug("Enabled auto read");
   }

   public void channelInactive(ChannelHandlerContext var1) throws Exception {
      this.a((IChatBaseComponent)(new ChatMessage("disconnect.endOfStream", new Object[0])));
   }

   public void exceptionCaught(ChannelHandlerContext var1, Throwable var2) throws Exception {
      ChatMessage var3;
      if(var2 instanceof TimeoutException) {
         var3 = new ChatMessage("disconnect.timeout", new Object[0]);
      } else {
         var3 = new ChatMessage("disconnect.genericReason", new Object[]{"Internal Exception: " + var2});
      }

      g.debug((Object)var2);
      this.a((IChatBaseComponent)var3);
   }

   protected void a(ChannelHandlerContext var1, Packet var2) throws Exception {
      if(this.k.isOpen()) {
         try {
            var2.handle(this.m);
         } catch (class_ku var4) {
            ;
         }
      }

   }

   public void a(PacketListener var1) {
      Validate.notNull(var1, "packetListener", new Object[0]);
      g.debug("Set listener of {} to {}", new Object[]{this, var1});
      this.m = var1;
   }

   public void a(Packet var1) {
      if(this.g()) {
         this.m();
         this.a((Packet)var1, (GenericFutureListener[])null);
      } else {
         this.j.writeLock().lock();

         try {
            this.i.add(new NetworkManager.class_a_in_class_ek(var1, (GenericFutureListener[])null));
         } finally {
            this.j.writeLock().unlock();
         }
      }

   }

   public void a(Packet var1, GenericFutureListener var2, GenericFutureListener... var3) {
      if(this.g()) {
         this.m();
         this.a(var1, (GenericFutureListener[])ArrayUtils.add(var3, 0, var2));
      } else {
         this.j.writeLock().lock();

         try {
            this.i.add(new NetworkManager.class_a_in_class_ek(var1, (GenericFutureListener[])ArrayUtils.add(var3, 0, var2)));
         } finally {
            this.j.writeLock().unlock();
         }
      }

   }

   private void a(final Packet var1, final GenericFutureListener[] var2) {
      final EnumProtocol var3 = EnumProtocol.a(var1);
      final EnumProtocol var4 = (EnumProtocol)this.k.attr(c).get();
      if(var4 != var3) {
         g.debug("Disabled auto read");
         this.k.config().setAutoRead(false);
      }

      if(this.k.eventLoop().inEventLoop()) {
         if(var3 != var4) {
            this.a(var3);
         }

         ChannelFuture var5 = this.k.writeAndFlush(var1);
         if(var2 != null) {
            var5.addListeners(var2);
         }

         var5.addListener(ChannelFutureListener.FIRE_EXCEPTION_ON_FAILURE);
      } else {
         this.k.eventLoop().execute(new Runnable() {
            public void run() {
               if(var3 != var4) {
                  NetworkManager.this.a(var3);
               }

               ChannelFuture var1x = NetworkManager.this.k.writeAndFlush(var1);
               if(var2 != null) {
                  var1x.addListeners(var2);
               }

               var1x.addListener(ChannelFutureListener.FIRE_EXCEPTION_ON_FAILURE);
            }
         });
      }

   }

   private void m() {
      if(this.k != null && this.k.isOpen()) {
         this.j.readLock().lock();

         try {
            while(!this.i.isEmpty()) {
               NetworkManager.class_a_in_class_ek var1 = (NetworkManager.class_a_in_class_ek)this.i.poll();
               this.a(var1.a, var1.b);
            }
         } finally {
            this.j.readLock().unlock();
         }

      }
   }

   public void a() {
      this.m();
      if(this.m instanceof class_ky) {
         ((class_ky)this.m).c();
      }

      this.k.flush();
   }

   public SocketAddress b() {
      return this.l;
   }

   public void a(IChatBaseComponent var1) {
      if(this.k.isOpen()) {
         this.k.close().awaitUninterruptibly();
         this.n = var1;
      }

   }

   public boolean c() {
      return this.k instanceof LocalChannel || this.k instanceof LocalServerChannel;
   }

   public void a(SecretKey var1) {
      this.o = true;
      this.k.pipeline().addBefore("splitter", "decrypt", new class_eg(MinecraftEncryption.a(2, var1)));
      this.k.pipeline().addBefore("prepender", "encrypt", new class_eh(MinecraftEncryption.a(1, var1)));
   }

   public boolean g() {
      return this.k != null && this.k.isOpen();
   }

   public boolean h() {
      return this.k == null;
   }

   public PacketListener i() {
      return this.m;
   }

   public IChatBaseComponent j() {
      return this.n;
   }

   public void k() {
      this.k.config().setAutoRead(false);
   }

   public void a(int var1) {
      if(var1 >= 0) {
         if(this.k.pipeline().get("decompress") instanceof PacketDecompressor) {
            ((PacketDecompressor)this.k.pipeline().get("decompress")).a(var1);
         } else {
            this.k.pipeline().addBefore("decoder", "decompress", new PacketDecompressor(var1));
         }

         if(this.k.pipeline().get("compress") instanceof class_ej) {
            ((class_ej)this.k.pipeline().get("compress")).a(var1);
         } else {
            this.k.pipeline().addBefore("encoder", "compress", new class_ej(var1));
         }
      } else {
         if(this.k.pipeline().get("decompress") instanceof PacketDecompressor) {
            this.k.pipeline().remove("decompress");
         }

         if(this.k.pipeline().get("compress") instanceof class_ej) {
            this.k.pipeline().remove("compress");
         }
      }

   }

   public void l() {
      if(this.k != null && !this.k.isOpen()) {
         if(this.p) {
            g.warn("handleDisconnection() called twice");
         } else {
            this.p = true;
            if(this.j() != null) {
               this.i().disconnect(this.j());
            } else if(this.i() != null) {
               this.i().disconnect(new ChatComponentText("Disconnected"));
            }
         }

      }
   }

   // $FF: synthetic method
   protected void channelRead0(ChannelHandlerContext var1, Object var2) throws Exception {
      this.a(var1, (Packet)var2);
   }

   static class class_a_in_class_ek {
      private final Packet a;
      private final GenericFutureListener[] b;

      public class_a_in_class_ek(Packet var1, GenericFutureListener... var2) {
         this.a = var1;
         this.b = var2;
      }
   }
}
