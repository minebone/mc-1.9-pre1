package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Item;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.class_di;
import net.minecraft.server.class_or;

public class BlockStone extends Block {
   public static final BlockStateEnum a = BlockStateEnum.a("variant", BlockStone.EnumStoneVariant.class);

   public BlockStone() {
      super(Material.e);
      this.w(this.A.b().set(a, BlockStone.EnumStoneVariant.STONE));
      this.a(CreativeModeTab.b);
   }

   public String c() {
      return class_di.a(this.a() + "." + BlockStone.EnumStoneVariant.STONE.d() + ".name");
   }

   public MaterialMapColor r(IBlockData var1) {
      return ((BlockStone.EnumStoneVariant)var1.get(a)).c();
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return var1.get(a) == BlockStone.EnumStoneVariant.STONE?Item.a(Blocks.e):Item.a(Blocks.b);
   }

   public int d(IBlockData var1) {
      return ((BlockStone.EnumStoneVariant)var1.get(a)).a();
   }

   public IBlockData a(int var1) {
      return this.u().set(a, BlockStone.EnumStoneVariant.a(var1));
   }

   public int e(IBlockData var1) {
      return ((BlockStone.EnumStoneVariant)var1.get(a)).a();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   public static enum EnumStoneVariant implements class_or {
      STONE(0, MaterialMapColor.m, "stone"),
      GRANITE(1, MaterialMapColor.l, "granite"),
      GRANITE_SMOOTH(2, MaterialMapColor.l, "smooth_granite", "graniteSmooth"),
      DIORITE(3, MaterialMapColor.p, "diorite"),
      DIORITE_SMOOTH(4, MaterialMapColor.p, "smooth_diorite", "dioriteSmooth"),
      ANDESITE(5, MaterialMapColor.m, "andesite"),
      ANDESITE_SMOOTH(6, MaterialMapColor.m, "smooth_andesite", "andesiteSmooth");

      private static final BlockStone.EnumStoneVariant[] h = new BlockStone.EnumStoneVariant[values().length];
      private final int i;
      private final String j;
      private final String k;
      private final MaterialMapColor l;

      private EnumStoneVariant(int var3, MaterialMapColor var4, String var5) {
         this(var3, var4, var5, var5);
      }

      private EnumStoneVariant(int var3, MaterialMapColor var4, String var5, String var6) {
         this.i = var3;
         this.j = var5;
         this.k = var6;
         this.l = var4;
      }

      public int a() {
         return this.i;
      }

      public MaterialMapColor c() {
         return this.l;
      }

      public String toString() {
         return this.j;
      }

      public static BlockStone.EnumStoneVariant a(int var0) {
         if(var0 < 0 || var0 >= h.length) {
            var0 = 0;
         }

         return h[var0];
      }

      public String m() {
         return this.j;
      }

      public String d() {
         return this.k;
      }

      static {
         BlockStone.EnumStoneVariant[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            BlockStone.EnumStoneVariant var3 = var0[var2];
            h[var3.a()] = var3;
         }

      }
   }
}
