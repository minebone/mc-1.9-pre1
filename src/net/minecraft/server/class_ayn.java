package net.minecraft.server;

import net.minecraft.server.EntityInsentient;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ayl;
import net.minecraft.server.class_aym;
import net.minecraft.server.class_oh;
import net.minecraft.server.MathHelper;

public abstract class class_ayn {
   protected class_ahw a;
   protected EntityInsentient b;
   protected final class_oh c = new class_oh();
   protected int d;
   protected int e;
   protected int f;
   protected boolean g;
   protected boolean h;
   protected boolean i;

   public void a(class_ahw var1, EntityInsentient var2) {
      this.a = var1;
      this.b = var2;
      this.c.c();
      this.d = MathHelper.d(var2.width + 1.0F);
      this.e = MathHelper.d(var2.length + 1.0F);
      this.f = MathHelper.d(var2.width + 1.0F);
   }

   public void a() {
   }

   protected class_aym a(int var1, int var2, int var3) {
      int var4 = class_aym.b(var1, var2, var3);
      class_aym var5 = (class_aym)this.c.a(var4);
      if(var5 == null) {
         var5 = new class_aym(var1, var2, var3);
         this.c.a(var4, var5);
      }

      return var5;
   }

   public abstract class_aym b();

   public abstract class_aym a(double var1, double var3, double var5);

   public abstract int a(class_aym[] var1, class_aym var2, class_aym var3, float var4);

   public abstract class_ayl a(class_ahw var1, int var2, int var3, int var4, EntityInsentient var5, int var6, int var7, int var8, boolean var9, boolean var10);

   public void a(boolean var1) {
      this.g = var1;
   }

   public void b(boolean var1) {
      this.h = var1;
   }

   public void c(boolean var1) {
      this.i = var1;
   }

   public boolean c() {
      return this.g;
   }

   public boolean d() {
      return this.h;
   }

   public boolean e() {
      return this.i;
   }
}
