package net.minecraft.server;

import com.google.common.base.Predicate;
import net.minecraft.server.Block;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.PathfinderGoalMoveThroughVillage;
import net.minecraft.server.Village;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_to;
import net.minecraft.server.class_tr;
import net.minecraft.server.class_tv;
import net.minecraft.server.class_tw;
import net.minecraft.server.class_tz;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_ut;
import net.minecraft.server.class_uu;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_vy;
import net.minecraft.server.class_yh;
import net.minecraft.server.class_yk;
import net.minecraft.server.class_ys;

public class class_wg extends class_vy {
   protected static final class_ke a = DataWatcher.a(class_wg.class, class_kg.a);
   private int c;
   Village b;
   private int bv;
   private int bw;

   public class_wg(World var1) {
      super(var1);
      this.a(1.4F, 2.7F);
   }

   protected void r() {
      this.bp.a(1, new class_tr(this, 1.0D, true));
      this.bp.a(2, new class_tw(this, 0.9D, 32.0F));
      this.bp.a(3, new PathfinderGoalMoveThroughVillage(this, 0.6D, true));
      this.bp.a(4, new class_tv(this, 1.0D));
      this.bp.a(5, new class_tz(this));
      this.bp.a(6, new class_uf(this, 0.6D));
      this.bp.a(7, new class_to(this, EntityHuman.class, 6.0F));
      this.bp.a(8, new class_ue(this));
      this.bq.a(1, new class_ut(this));
      this.bq.a(2, new class_uu(this, false, new Class[0]));
      this.bq.a(3, new class_ux(this, EntityInsentient.class, 10, false, true, new Predicate() {
         public boolean a(EntityInsentient var1) {
            return var1 != null && class_yk.e.apply(var1) && !(var1 instanceof class_yh);
         }

         // $FF: synthetic method
         public boolean apply(Object var1) {
            return this.a((EntityInsentient)var1);
         }
      }));
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)a, (Object)Byte.valueOf((byte)0));
   }

   protected void M() {
      if(--this.c <= 0) {
         this.c = 70 + this.random.nextInt(50);
         this.b = this.world.ai().a(new BlockPosition(this), 32);
         if(this.b == null) {
            this.cX();
         } else {
            BlockPosition var1 = this.b.a();
            this.a(var1, (int)((float)this.b.b() * 0.6F));
         }
      }

      super.M();
   }

   protected void bz() {
      super.bz();
      this.a((class_sk)class_ys.a).a(100.0D);
      this.a((class_sk)class_ys.d).a(0.25D);
      this.a((class_sk)class_ys.c).a(1.0D);
   }

   protected int d(int var1) {
      return var1;
   }

   protected void C(Entity var1) {
      if(var1 instanceof class_yk && !(var1 instanceof class_yh) && this.bE().nextInt(20) == 0) {
         this.c((class_rz)var1);
      }

      super.C(var1);
   }

   public void n() {
      super.n();
      if(this.bv > 0) {
         --this.bv;
      }

      if(this.bw > 0) {
         --this.bw;
      }

      if(this.motX * this.motX + this.motZ * this.motZ > 2.500000277905201E-7D && this.random.nextInt(5) == 0) {
         int var1 = MathHelper.c(this.locX);
         int var2 = MathHelper.c(this.locY - 0.20000000298023224D);
         int var3 = MathHelper.c(this.locZ);
         IBlockData var4 = this.world.getType(new BlockPosition(var1, var2, var3));
         if(var4.getMaterial() != Material.a) {
            this.world.a(EnumParticle.BLOCK_CRACK, this.locX + ((double)this.random.nextFloat() - 0.5D) * (double)this.width, this.bk().b + 0.1D, this.locZ + ((double)this.random.nextFloat() - 0.5D) * (double)this.width, 4.0D * ((double)this.random.nextFloat() - 0.5D), 0.5D, ((double)this.random.nextFloat() - 0.5D) * 4.0D, new int[]{Block.j(var4)});
         }
      }

   }

   public boolean d(Class var1) {
      return this.db() && EntityHuman.class.isAssignableFrom(var1)?false:(var1 == class_yh.class?false:super.d(var1));
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("PlayerCreated", this.db());
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.o(var1.p("PlayerCreated"));
   }

   public boolean B(Entity var1) {
      this.bv = 10;
      this.world.a((Entity)this, (byte)4);
      boolean var2 = var1.a(DamageSource.a((class_rz)this), (float)(7 + this.random.nextInt(15)));
      if(var2) {
         var1.motY += 0.4000000059604645D;
         this.a(this, var1);
      }

      this.a(class_ng.cD, 1.0F, 1.0F);
      return var2;
   }

   public Village o() {
      return this.b;
   }

   public void a(boolean var1) {
      this.bw = var1?400:0;
      this.world.a((Entity)this, (byte)11);
   }

   protected class_nf bQ() {
      return class_ng.cF;
   }

   protected class_nf bR() {
      return class_ng.cE;
   }

   protected void a(BlockPosition var1, Block var2) {
      this.a(class_ng.cG, 1.0F, 1.0F);
   }

   protected class_kk J() {
      return class_azs.y;
   }

   public int da() {
      return this.bw;
   }

   public boolean db() {
      return (((Byte)this.datawatcher.a(a)).byteValue() & 1) != 0;
   }

   public void o(boolean var1) {
      byte var2 = ((Byte)this.datawatcher.a(a)).byteValue();
      if(var1) {
         this.datawatcher.b(a, Byte.valueOf((byte)(var2 | 1)));
      } else {
         this.datawatcher.b(a, Byte.valueOf((byte)(var2 & -2)));
      }

   }

   public void a(DamageSource var1) {
      if(!this.db() && this.aR != null && this.b != null) {
         this.b.a(this.aR.h_(), -5);
      }

      super.a(var1);
   }
}
