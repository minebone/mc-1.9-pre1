package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_auc;
import net.minecraft.server.BlockPosition;

public class class_auj extends class_auc {
   private Block a = Blocks.cB;
   private int b;

   public class_auj(int var1) {
      this.b = var1;
   }

   public boolean b(World var1, Random var2, BlockPosition var3) {
      while(var1.d(var3) && var3.q() > 2) {
         var3 = var3.b();
      }

      if(var1.getType(var3).getBlock() != Blocks.aJ) {
         return false;
      } else {
         int var4 = var2.nextInt(this.b - 2) + 2;
         byte var5 = 1;

         for(int var6 = var3.p() - var4; var6 <= var3.p() + var4; ++var6) {
            for(int var7 = var3.r() - var4; var7 <= var3.r() + var4; ++var7) {
               int var8 = var6 - var3.p();
               int var9 = var7 - var3.r();
               if(var8 * var8 + var9 * var9 <= var4 * var4) {
                  for(int var10 = var3.q() - var5; var10 <= var3.q() + var5; ++var10) {
                     BlockPosition var11 = new BlockPosition(var6, var10, var7);
                     Block var12 = var1.getType(var11).getBlock();
                     if(var12 == Blocks.d || var12 == Blocks.aJ || var12 == Blocks.aI) {
                        var1.a((BlockPosition)var11, (IBlockData)this.a.u(), 2);
                     }
                  }
               }
            }
         }

         return true;
      }
   }
}
