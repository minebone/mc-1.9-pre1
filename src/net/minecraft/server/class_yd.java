package net.minecraft.server;

import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_rz;

public class class_yd extends Entity {
   private static final class_ke a = DataWatcher.a(class_yd.class, class_kg.b);
   private class_rz b;
   private int c;

   public class_yd(World var1) {
      super(var1);
      this.c = 80;
      this.i = true;
      this.a(0.98F, 0.98F);
   }

   public class_yd(World var1, double var2, double var4, double var6, class_rz var8) {
      this(var1);
      this.b(var2, var4, var6);
      float var9 = (float)(Math.random() * 6.2831854820251465D);
      this.motX = (double)(-((float)Math.sin((double)var9)) * 0.02F);
      this.motY = 0.20000000298023224D;
      this.motZ = (double)(-((float)Math.cos((double)var9)) * 0.02F);
      this.a(80);
      this.lastX = var2;
      this.lastY = var4;
      this.lastZ = var6;
      this.b = var8;
   }

   protected void i() {
      this.datawatcher.a((class_ke)a, (Object)Integer.valueOf(80));
   }

   protected boolean ad() {
      return false;
   }

   public boolean ao() {
      return !this.dead;
   }

   public void m() {
      this.lastX = this.locX;
      this.lastY = this.locY;
      this.lastZ = this.locZ;
      this.motY -= 0.03999999910593033D;
      this.d(this.motX, this.motY, this.motZ);
      this.motX *= 0.9800000190734863D;
      this.motY *= 0.9800000190734863D;
      this.motZ *= 0.9800000190734863D;
      if(this.onGround) {
         this.motX *= 0.699999988079071D;
         this.motZ *= 0.699999988079071D;
         this.motY *= -0.5D;
      }

      --this.c;
      if(this.c <= 0) {
         this.S();
         if(!this.world.E) {
            this.n();
         }
      } else {
         this.ai();
         this.world.a(EnumParticle.SMOKE_NORMAL, this.locX, this.locY + 0.5D, this.locZ, 0.0D, 0.0D, 0.0D, new int[0]);
      }

   }

   private void n() {
      float var1 = 4.0F;
      this.world.a(this, this.locX, this.locY + (double)(this.length / 16.0F), this.locZ, var1, true);
   }

   protected void b(NBTTagCompound var1) {
      var1.a("Fuse", (short)this.l());
   }

   protected void a(NBTTagCompound var1) {
      this.a(var1.g("Fuse"));
   }

   public class_rz j() {
      return this.b;
   }

   public float bm() {
      return 0.0F;
   }

   public void a(int var1) {
      this.datawatcher.b(a, Integer.valueOf(var1));
      this.c = var1;
   }

   public void a(class_ke var1) {
      if(a.equals(var1)) {
         this.c = this.k();
      }

   }

   public int k() {
      return ((Integer)this.datawatcher.a(a)).intValue();
   }

   public int l() {
      return this.c;
   }
}
