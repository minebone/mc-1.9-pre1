package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_amf;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_aoi;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arn;
import net.minecraft.server.IBlockState;
import net.minecraft.server.BlockPosition;

public class class_apk extends class_aoi {
   public static final class_arn b = class_amf.D;
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.0D, 0.28125D, 0.0D, 0.125D, 0.78125D, 1.0D);
   protected static final AxisAlignedBB d = new AxisAlignedBB(0.875D, 0.28125D, 0.0D, 1.0D, 0.78125D, 1.0D);
   protected static final AxisAlignedBB e = new AxisAlignedBB(0.0D, 0.28125D, 0.0D, 1.0D, 0.78125D, 0.125D);
   protected static final AxisAlignedBB f = new AxisAlignedBB(0.0D, 0.28125D, 0.875D, 1.0D, 0.78125D, 1.0D);

   public class_apk() {
      this.w(this.A.b().set(b, EnumDirection.NORTH));
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      switch(class_apk.SyntheticClass_1.a[((EnumDirection)var1.get(b)).ordinal()]) {
      case 1:
      default:
         return f;
      case 2:
         return e;
      case 3:
         return d;
      case 4:
         return c;
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      EnumDirection var5 = (EnumDirection)var3.get(b);
      if(!var1.getType(var2.a(var5.d())).getMaterial().a()) {
         this.b(var1, var2, var3, 0);
         var1.g(var2);
      }

      super.a(var1, var2, var3, var4);
   }

   public IBlockData a(int var1) {
      EnumDirection var2 = EnumDirection.a(var1);
      if(var2.k() == EnumDirection.class_a_in_class_cq.Y) {
         var2 = EnumDirection.NORTH;
      }

      return this.u().set(b, var2);
   }

   public int e(IBlockData var1) {
      return ((EnumDirection)var1.get(b)).a();
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(b, var2.a((EnumDirection)var1.get(b)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(b)));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{b});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumDirection.values().length];

      static {
         try {
            a[EnumDirection.NORTH.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.SOUTH.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.EAST.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
