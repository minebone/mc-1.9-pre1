package net.minecraft.server;

import java.io.IOException;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayInListener;

public class class_jd implements Packet {
   private BlockPosition a;
   private String[] b;

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.e();
      this.b = new String[4];

      for(int var2 = 0; var2 < 4; ++var2) {
         this.b[var2] = var1.c(384);
      }

   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.a(this.a);

      for(int var2 = 0; var2 < 4; ++var2) {
         var1.a(this.b[var2]);
      }

   }

   public void a(PacketPlayInListener var1) {
      var1.a(this);
   }

   public BlockPosition a() {
      return this.a;
   }

   public String[] b() {
      return this.b;
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketPlayInListener)var1);
   }
}
