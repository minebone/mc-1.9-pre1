package net.minecraft.server;

import com.google.common.collect.Sets;
import java.util.Set;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Item;
import net.minecraft.server.ItemTool;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;

public class class_aby extends ItemTool {
   private static final Set e = Sets.newHashSet((Object[])(new Block[]{Blocks.f, Blocks.X, Blocks.r, Blocks.s, Blocks.ae, Blocks.aU, Blocks.aZ, Blocks.bk, Blocks.au, Blocks.cd, Blocks.aB}));
   private static final float[] m = new float[]{6.0F, 8.0F, 8.0F, 8.0F, 6.0F};
   private static final float[] n = new float[]{-3.2F, -3.2F, -3.1F, -3.0F, -3.0F};

   protected class_aby(Item.class_a_in_class_adn var1) {
      super(var1, e);
      this.b = m[var1.ordinal()];
      this.c = n[var1.ordinal()];
   }

   public float a(ItemStack var1, IBlockData var2) {
      Material var3 = var2.getMaterial();
      return var3 != Material.d && var3 != Material.k && var3 != Material.l?super.a(var1, var2):this.a;
   }
}
