package net.minecraft.server;

import com.google.common.collect.Sets;
import java.util.Set;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.ItemTool;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_rz;

public class class_aem extends ItemTool {
   private static final Set e = Sets.newHashSet((Object[])(new Block[]{Blocks.aL, Blocks.d, Blocks.ak, Blocks.c, Blocks.n, Blocks.bw, Blocks.m, Blocks.aJ, Blocks.aH, Blocks.aW, Blocks.da}));

   public class_aem(Item.class_a_in_class_adn var1) {
      super(1.5F, -3.0F, var1, e);
   }

   public boolean a(IBlockData var1) {
      Block var2 = var1.getBlock();
      return var2 == Blocks.aH?true:var2 == Blocks.aJ;
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      if(!var2.a(var4.a(var6), var6, var1)) {
         return EnumResult.FAIL;
      } else {
         IBlockData var10 = var3.getType(var4);
         Block var11 = var10.getBlock();
         if(var6 != EnumDirection.DOWN && var3.getType(var4.a()).getMaterial() == Material.a && var11 == Blocks.c) {
            IBlockData var12 = Blocks.da.u();
            var3.a(var2, var4, class_ng.eN, EnumSoundCategory.BLOCKS, 1.0F, 1.0F);
            if(!var3.E) {
               var3.a((BlockPosition)var4, (IBlockData)var12, 11);
               var1.a(1, (class_rz)var2);
            }

            return EnumResult.SUCCESS;
         } else {
            return EnumResult.PASS;
         }
      }
   }
}
