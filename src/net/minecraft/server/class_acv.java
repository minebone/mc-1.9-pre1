package net.minecraft.server;

import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;
import net.minecraft.server.class_zy;

public class class_acv extends Item {
   public class_acv() {
      this.j = 16;
      this.a(CreativeModeTab.l);
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      if(!var3.abilities.d) {
         --var1.b;
      }

      var2.a((EntityHuman)null, var3.locX, var3.locY, var3.locZ, class_ng.aA, EnumSoundCategory.NEUTRAL, 0.5F, 0.4F / (i.nextFloat() * 0.4F + 0.8F));
      if(!var2.E) {
         class_zy var5 = new class_zy(var2, var3);
         var5.a(var3, var3.pitch, var3.yaw, 0.0F, 1.5F, 1.0F);
         var2.a((Entity)var5);
      }

      var3.b(StatisticList.b((Item)this));
      return new class_qo(EnumResult.SUCCESS, var1);
   }
}
