package net.minecraft.server;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListenableFutureTask;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.GameProfileRepository;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.base64.Base64;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.Proxy;
import java.security.KeyPair;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import javax.imageio.ImageIO;
import net.minecraft.server.Bootstrap;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandDispatcher;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.CrashReport;
import net.minecraft.server.DedicatedServer;
import net.minecraft.server.DemoWorldServer;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MojangStatisticsGenerator;
import net.minecraft.server.PlayerList;
import net.minecraft.server.ServerConnection;
import net.minecraft.server.ServerPing;
import net.minecraft.server.SystemUtils;
import net.minecraft.server.UserCache;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.WorldData;
import net.minecraft.server.WorldLoaderServer;
import net.minecraft.server.WorldServer;
import net.minecraft.server.WorldSettings;
import net.minecraft.server.WorldType;
import net.minecraft.server.class_aht;
import net.minecraft.server.class_ahu;
import net.minecraft.server.class_azh;
import net.minecraft.server.class_azj;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_e;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutUpdateTime;
import net.minecraft.server.class_ky;
import net.minecraft.server.class_l;
import net.minecraft.server.class_ll;
import net.minecraft.server.class_lq;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_oo;
import net.minecraft.server.class_op;
import net.minecraft.server.class_pb;
import net.minecraft.server.class_pc;
import net.minecraft.server.class_qb;
import net.minecraft.server.class_qw;
import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class MinecraftServer implements Runnable, ICommandListener, class_qb, class_qw {
   private static final Logger k = LogManager.getLogger();
   public static final File a = new File("usercache.json");
   private final class_azj l;
   private final MojangStatisticsGenerator m = new MojangStatisticsGenerator("server", this, av());
   private final File n;
   private final List o = Lists.newArrayList();
   protected final class_l b;
   public final class_oo c = new class_oo();
   private final ServerConnection p;
   private final ServerPing q = new ServerPing();
   private final Random r = new Random();
   private final class_pb s;
   private String t;
   private int u = -1;
   public WorldServer[] d;
   private PlayerList v;
   private boolean w = true;
   private boolean x;
   private int y;
   protected final Proxy e;
   public String f;
   public int g;
   private boolean z;
   private boolean A;
   private boolean B;
   private boolean C;
   private boolean D;
   private String E;
   private int F;
   private int G = 0;
   public final long[] h = new long[100];
   public long[][] i;
   private KeyPair H;
   private String I;
   private String J;
   private boolean L;
   private boolean M;
   private String N = "";
   private String O = "";
   private boolean P;
   private long Q;
   private String R;
   private boolean S;
   private boolean T;
   private final YggdrasilAuthenticationService U;
   private final MinecraftSessionService V;
   private final GameProfileRepository W;
   private final UserCache X;
   private long Y = 0L;
   protected final Queue j = Queues.newArrayDeque();
   private Thread Z;
   private long aa = av();

   public MinecraftServer(File var1, Proxy var2, class_pb var3, YggdrasilAuthenticationService var4, MinecraftSessionService var5, GameProfileRepository var6, UserCache var7) {
      this.e = var2;
      this.U = var4;
      this.V = var5;
      this.W = var6;
      this.X = var7;
      this.n = var1;
      this.p = new ServerConnection(this);
      this.b = this.i();
      this.l = new WorldLoaderServer(var1, var3);
      this.s = var3;
   }

   protected CommandDispatcher i() {
      return new CommandDispatcher(this);
   }

   protected abstract boolean j() throws IOException;

   protected void a(String var1) {
      if(this.W().b(var1)) {
         k.info("Converting map!");
         this.b("menu.convertingLevel");
         this.W().a(var1, new class_op() {
            private long b = System.currentTimeMillis();

            public void a(String var1) {
            }

            public void a(int var1) {
               if(System.currentTimeMillis() - this.b >= 1000L) {
                  this.b = System.currentTimeMillis();
                  MinecraftServer.k.info("Converting... " + var1 + "%");
               }

            }

            public void c(String var1) {
            }
         });
      }

   }

   protected synchronized void b(String var1) {
      this.R = var1;
   }

   protected void a(String var1, String var2, long var3, WorldType var5, String var6) {
      this.a(var1);
      this.b("menu.loadingLevel");
      this.d = new WorldServer[3];
      this.i = new long[this.d.length][100];
      class_azh var7 = this.l.a(var1, true);
      this.a(this.S(), var7);
      WorldData var9 = var7.d();
      WorldSettings var8;
      if(var9 == null) {
         if(this.V()) {
            var8 = DemoWorldServer.a;
         } else {
            var8 = new WorldSettings(var3, this.n(), this.m(), this.p(), var5);
            var8.a(var6);
            if(this.M) {
               var8.a();
            }
         }

         var9 = new WorldData(var8, var2);
      } else {
         var9.a(var2);
         var8 = new WorldSettings(var9);
      }

      for(int var10 = 0; var10 < this.d.length; ++var10) {
         byte var11 = 0;
         if(var10 == 1) {
            var11 = -1;
         }

         if(var10 == 2) {
            var11 = 1;
         }

         if(var10 == 0) {
            if(this.V()) {
               this.d[var10] = (WorldServer)(new DemoWorldServer(this, var7, var9, var11, this.c)).b();
            } else {
               this.d[var10] = (WorldServer)(new WorldServer(this, var7, var9, var11, this.c)).b();
            }

            this.d[var10].a(var8);
         } else {
            this.d[var10] = (WorldServer)(new class_ll(this, var7, var11, this.d[0], this.c)).b();
         }

         this.d[var10].a((class_ahu)(new class_lq(this, this.d[var10])));
         if(!this.R()) {
            this.d[var10].T().a(this.n());
         }
      }

      this.v.a(this.d);
      this.a(this.o());
      this.l();
   }

   protected void l() {
      boolean var1 = true;
      boolean var2 = true;
      boolean var3 = true;
      boolean var4 = true;
      int var5 = 0;
      this.b("menu.generatingTerrain");
      byte var6 = 0;
      k.info("Preparing start region for level " + var6);
      WorldServer var7 = this.d[var6];
      BlockPosition var8 = var7.R();
      long var9 = av();

      for(int var11 = -192; var11 <= 192 && this.w(); var11 += 16) {
         for(int var12 = -192; var12 <= 192 && this.w(); var12 += 16) {
            long var13 = av();
            if(var13 - var9 > 1000L) {
               this.a_("Preparing spawn area", var5 * 100 / 625);
               var9 = var13;
            }

            ++var5;
            var7.r().d(var8.p() + var11 >> 4, var8.r() + var12 >> 4);
         }
      }

      this.t();
   }

   protected void a(String var1, class_azh var2) {
      File var3 = new File(var2.b(), "resources.zip");
      if(var3.isFile()) {
         this.a_("level://" + var1 + "/" + "resources.zip", "");
      }

   }

   public abstract boolean m();

   public abstract WorldSettings.EnumGamemode n();

   public abstract EnumDifficulty o();

   public abstract boolean p();

   public abstract int q();

   public abstract boolean r();

   public abstract boolean s();

   protected void a_(String var1, int var2) {
      this.f = var1;
      this.g = var2;
      k.info(var1 + ": " + var2 + "%");
   }

   protected void t() {
      this.f = null;
      this.g = 0;
   }

   protected void a(boolean var1) {
      WorldServer[] var2 = this.d;
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         WorldServer var5 = var2[var4];
         if(var5 != null) {
            if(!var1) {
               k.info("Saving chunks for level \'" + var5.T().j() + "\'/" + var5.s.p().b());
            }

            try {
               var5.a(true, (class_op)null);
            } catch (class_aht var7) {
               k.warn(var7.getMessage());
            }
         }
      }

   }

   protected void u() {
      k.info("Stopping server");
      if(this.am() != null) {
         this.am().b();
      }

      if(this.v != null) {
         k.info("Saving players");
         this.v.j();
         this.v.u();
      }

      if(this.d != null) {
         k.info("Saving worlds");

         int var1;
         for(var1 = 0; var1 < this.d.length; ++var1) {
            if(this.d[var1] != null) {
               this.d[var1].b = false;
            }
         }

         this.a(false);

         for(var1 = 0; var1 < this.d.length; ++var1) {
            if(this.d[var1] != null) {
               this.d[var1].s();
            }
         }
      }

      if(this.m.d()) {
         this.m.e();
      }

   }

   public String v() {
      return this.t;
   }

   public void c(String var1) {
      this.t = var1;
   }

   public boolean w() {
      return this.w;
   }

   public void x() {
      this.w = false;
   }

   public void run() {
      try {
         if(this.j()) {
            this.aa = av();
            long var1 = 0L;
            this.q.a((IChatBaseComponent)(new ChatComponentText(this.E)));
            this.q.a(new ServerPing.ServerData("1.9-pre2", 104));
            this.a(this.q);

            while(this.w) {
               long var48 = av();
               long var5 = var48 - this.aa;
               if(var5 > 2000L && this.aa - this.Q >= 15000L) {
                  k.warn("Can\'t keep up! Did the system time change, or is the server overloaded? Running {}ms behind, skipping {} tick(s)", new Object[]{Long.valueOf(var5), Long.valueOf(var5 / 50L)});
                  var5 = 2000L;
                  this.Q = this.aa;
               }

               if(var5 < 0L) {
                  k.warn("Time ran backwards! Did the system time change?");
                  var5 = 0L;
               }

               var1 += var5;
               this.aa = var48;
               if(this.d[0].g()) {
                  this.C();
                  var1 = 0L;
               } else {
                  while(var1 > 50L) {
                     var1 -= 50L;
                     this.C();
                  }
               }

               Thread.sleep(Math.max(1L, 50L - var1));
               this.P = true;
            }
         } else {
            this.a((CrashReport)null);
         }
      } catch (Throwable var46) {
         k.error("Encountered an unexpected exception", var46);
         CrashReport var2 = null;
         if(var46 instanceof class_e) {
            var2 = this.b(((class_e)var46).a());
         } else {
            var2 = this.b(new CrashReport("Exception in server tick loop", var46));
         }

         File var3 = new File(new File(this.A(), "crash-reports"), "crash-" + (new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss")).format(new Date()) + "-server.txt");
         if(var2.a(var3)) {
            k.error("This crash report has been saved to: " + var3.getAbsolutePath());
         } else {
            k.error("We were unable to save this crash report to disk.");
         }

         this.a(var2);
      } finally {
         try {
            this.x = true;
            this.u();
         } catch (Throwable var44) {
            k.error("Exception stopping the server", var44);
         } finally {
            this.B();
         }

      }

   }

   public void a(ServerPing var1) {
      File var2 = this.d("server-icon.png");
      if(!var2.exists()) {
         var2 = this.W().b(this.S(), "icon.png");
      }

      if(var2.isFile()) {
         ByteBuf var3 = Unpooled.buffer();

         try {
            BufferedImage var4 = ImageIO.read(var2);
            Validate.validState(var4.getWidth() == 64, "Must be 64 pixels wide", new Object[0]);
            Validate.validState(var4.getHeight() == 64, "Must be 64 pixels high", new Object[0]);
            ImageIO.write(var4, "PNG", new ByteBufOutputStream(var3));
            ByteBuf var5 = Base64.encode(var3);
            var1.a("data:image/png;base64," + var5.toString(Charsets.UTF_8));
         } catch (Exception var9) {
            k.error((String)"Couldn\'t load server icon", (Throwable)var9);
         } finally {
            var3.release();
         }
      }

   }

   public File A() {
      return new File(".");
   }

   protected void a(CrashReport var1) {
   }

   protected void B() {
   }

   protected void C() {
      long var1 = System.nanoTime();
      ++this.y;
      if(this.S) {
         this.S = false;
         this.c.a = true;
         this.c.a();
      }

      this.c.a("root");
      this.D();
      if(var1 - this.Y >= 5000000000L) {
         this.Y = var1;
         this.q.a(new ServerPing.ServerPingPlayerSample(this.I(), this.H()));
         GameProfile[] var3 = new GameProfile[Math.min(this.H(), 12)];
         int var4 = MathHelper.a((Random)this.r, 0, this.H() - var3.length);

         for(int var5 = 0; var5 < var3.length; ++var5) {
            var3[var5] = ((EntityPlayer)this.v.v().get(var4 + var5)).getProfile();
         }

         Collections.shuffle(Arrays.asList(var3));
         this.q.b().a(var3);
      }

      if(this.y % 900 == 0) {
         this.c.a("save");
         this.v.j();
         this.a(true);
         this.c.b();
      }

      this.c.a("tallying");
      this.h[this.y % 100] = System.nanoTime() - var1;
      this.c.b();
      this.c.a("snooper");
      if(!this.m.d() && this.y > 100) {
         this.m.a();
      }

      if(this.y % 6000 == 0) {
         this.m.b();
      }

      this.c.b();
      this.c.b();
   }

   public void D() {
      this.c.a("jobs");
      Queue var1 = this.j;
      synchronized(this.j) {
         while(!this.j.isEmpty()) {
            SystemUtils.a((FutureTask)this.j.poll(), k);
         }
      }

      this.c.c("levels");

      int var10;
      for(var10 = 0; var10 < this.d.length; ++var10) {
         long var2 = System.nanoTime();
         if(var10 == 0 || this.E()) {
            WorldServer var4 = this.d[var10];
            this.c.a(var4.T().j());
            if(this.y % 20 == 0) {
               this.c.a("timeSync");
               this.v.a((Packet)(new PacketPlayOutUpdateTime(var4.P(), var4.Q(), var4.U().b("doDaylightCycle"))), var4.s.p().a());
               this.c.b();
            }

            this.c.a("tick");

            CrashReport var6;
            try {
               var4.d();
            } catch (Throwable var8) {
               var6 = CrashReport.a(var8, "Exception ticking world");
               var4.a((CrashReport)var6);
               throw new class_e(var6);
            }

            try {
               var4.k();
            } catch (Throwable var7) {
               var6 = CrashReport.a(var7, "Exception ticking world entities");
               var4.a((CrashReport)var6);
               throw new class_e(var6);
            }

            this.c.b();
            this.c.a("tracker");
            var4.v().a();
            this.c.b();
            this.c.b();
         }

         this.i[var10][this.y % 100] = System.nanoTime() - var2;
      }

      this.c.c("connection");
      this.am().c();
      this.c.c("players");
      this.v.e();
      this.c.c("tickables");

      for(var10 = 0; var10 < this.o.size(); ++var10) {
         ((class_ky)this.o.get(var10)).c();
      }

      this.c.b();
   }

   public boolean E() {
      return true;
   }

   public void a(class_ky var1) {
      this.o.add(var1);
   }

   public static void main(String[] var0) {
      Bootstrap.c();

      try {
         boolean var1 = true;
         String var2 = null;
         String var3 = ".";
         String var4 = null;
         boolean var5 = false;
         boolean var6 = false;
         int var7 = -1;

         for(int var8 = 0; var8 < var0.length; ++var8) {
            String var9 = var0[var8];
            String var10 = var8 == var0.length - 1?null:var0[var8 + 1];
            boolean var11 = false;
            if(!var9.equals("nogui") && !var9.equals("--nogui")) {
               if(var9.equals("--port") && var10 != null) {
                  var11 = true;

                  try {
                     var7 = Integer.parseInt(var10);
                  } catch (NumberFormatException var13) {
                     ;
                  }
               } else if(var9.equals("--singleplayer") && var10 != null) {
                  var11 = true;
                  var2 = var10;
               } else if(var9.equals("--universe") && var10 != null) {
                  var11 = true;
                  var3 = var10;
               } else if(var9.equals("--world") && var10 != null) {
                  var11 = true;
                  var4 = var10;
               } else if(var9.equals("--demo")) {
                  var5 = true;
               } else if(var9.equals("--bonusChest")) {
                  var6 = true;
               }
            } else {
               var1 = false;
            }

            if(var11) {
               ++var8;
            }
         }

         YggdrasilAuthenticationService var15 = new YggdrasilAuthenticationService(Proxy.NO_PROXY, UUID.randomUUID().toString());
         MinecraftSessionService var16 = var15.createMinecraftSessionService();
         GameProfileRepository var17 = var15.createProfileRepository();
         UserCache var18 = new UserCache(var17, new File(var3, a.getName()));
         final DedicatedServer var12 = new DedicatedServer(new File(var3), class_pc.a(), var15, var16, var17, var18);
         if(var2 != null) {
            var12.i(var2);
         }

         if(var4 != null) {
            var12.j(var4);
         }

         if(var7 >= 0) {
            var12.b(var7);
         }

         if(var5) {
            var12.b(true);
         }

         if(var6) {
            var12.c(true);
         }

         if(var1 && !GraphicsEnvironment.isHeadless()) {
            var12.aN();
         }

         var12.F();
         Runtime.getRuntime().addShutdownHook(new Thread("Server Shutdown Thread") {
            public void run() {
               var12.u();
            }
         });
      } catch (Exception var14) {
         k.fatal((String)"Failed to start the minecraft server", (Throwable)var14);
      }

   }

   public void F() {
      this.Z = new Thread(this, "Server thread");
      this.Z.start();
   }

   public File d(String var1) {
      return new File(this.A(), var1);
   }

   public void e(String var1) {
      k.info(var1);
   }

   public void f(String var1) {
      k.warn(var1);
   }

   public WorldServer a(int var1) {
      return var1 == -1?this.d[1]:(var1 == 1?this.d[2]:this.d[0]);
   }

   public String G() {
      return "1.9-pre2";
   }

   public int H() {
      return this.v.o();
   }

   public int I() {
      return this.v.p();
   }

   public String[] J() {
      return this.v.f();
   }

   public GameProfile[] K() {
      return this.v.g();
   }

   public boolean L() {
      return false;
   }

   public void g(String var1) {
      k.error(var1);
   }

   public void h(String var1) {
      if(this.L()) {
         k.info(var1);
      }

   }

   public String getServerModName() {
      return "vanilla";
   }

   public CrashReport b(CrashReport var1) {
      var1.g().a("Profiler Position", new Callable() {
         public String a() throws Exception {
            return MinecraftServer.this.c.a?MinecraftServer.this.c.c():"N/A (disabled)";
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
      if(this.v != null) {
         var1.g().a("Player Count", new Callable() {
            public String a() {
               return MinecraftServer.this.v.o() + " / " + MinecraftServer.this.v.p() + "; " + MinecraftServer.this.v.v();
            }

            // $FF: synthetic method
            public Object call() throws Exception {
               return this.a();
            }
         });
      }

      return var1;
   }

   public List a(ICommandListener var1, String var2, BlockPosition var3, boolean var4) {
      ArrayList var5 = Lists.newArrayList();
      boolean var6 = var2.startsWith("/");
      if(var6) {
         var2 = var2.substring(1);
      }

      if(!var6 && !var4) {
         String[] var13 = var2.split(" ", -1);
         String var14 = var13[var13.length - 1];
         String[] var15 = this.v.f();
         int var16 = var15.length;

         for(int var11 = 0; var11 < var16; ++var11) {
            String var12 = var15[var11];
            if(CommandAbstract.a(var14, var12)) {
               var5.add(var12);
            }
         }

         return var5;
      } else {
         boolean var7 = !var2.contains(" ");
         List var8 = this.b.a(var1, var2, var3);
         if(!var8.isEmpty()) {
            Iterator var9 = var8.iterator();

            while(var9.hasNext()) {
               String var10 = (String)var9.next();
               if(var7) {
                  var5.add("/" + var10);
               } else {
                  var5.add(var10);
               }
            }
         }

         return var5;
      }
   }

   public boolean M() {
      return this.n != null;
   }

   public String h_() {
      return "Server";
   }

   public void a(IChatBaseComponent var1) {
      k.info(var1.c());
   }

   public boolean a(int var1, String var2) {
      return true;
   }

   public class_l N() {
      return this.b;
   }

   public KeyPair O() {
      return this.H;
   }

   public int P() {
      return this.u;
   }

   public void b(int var1) {
      this.u = var1;
   }

   public String Q() {
      return this.I;
   }

   public void i(String var1) {
      this.I = var1;
   }

   public boolean R() {
      return this.I != null;
   }

   public String S() {
      return this.J;
   }

   public void j(String var1) {
      this.J = var1;
   }

   public void a(KeyPair var1) {
      this.H = var1;
   }

   public void a(EnumDifficulty var1) {
      for(int var2 = 0; var2 < this.d.length; ++var2) {
         WorldServer var3 = this.d[var2];
         if(var3 != null) {
            if(var3.T().s()) {
               var3.T().a(EnumDifficulty.HARD);
               var3.a(true, true);
            } else if(this.R()) {
               var3.T().a(var1);
               var3.a(var3.ae() != EnumDifficulty.PEACEFUL, true);
            } else {
               var3.T().a(var1);
               var3.a(this.U(), this.A);
            }
         }
      }

   }

   protected boolean U() {
      return true;
   }

   public boolean V() {
      return this.L;
   }

   public void b(boolean var1) {
      this.L = var1;
   }

   public void c(boolean var1) {
      this.M = var1;
   }

   public class_azj W() {
      return this.l;
   }

   public String X() {
      return this.N;
   }

   public String Y() {
      return this.O;
   }

   public void a_(String var1, String var2) {
      this.N = var1;
      this.O = var2;
   }

   public void a(MojangStatisticsGenerator var1) {
      var1.a("whitelist_enabled", Boolean.valueOf(false));
      var1.a("whitelist_count", Integer.valueOf(0));
      if(this.v != null) {
         var1.a("players_current", Integer.valueOf(this.H()));
         var1.a("players_max", Integer.valueOf(this.I()));
         var1.a("players_seen", Integer.valueOf(this.v.q().length));
      }

      var1.a("uses_auth", Boolean.valueOf(this.z));
      var1.a("gui_state", this.ao()?"enabled":"disabled");
      var1.a("run_time", Long.valueOf((av() - var1.g()) / 60L * 1000L));
      var1.a("avg_tick_ms", Integer.valueOf((int)(MathHelper.a(this.h) * 1.0E-6D)));
      int var2 = 0;
      if(this.d != null) {
         for(int var3 = 0; var3 < this.d.length; ++var3) {
            if(this.d[var3] != null) {
               WorldServer var4 = this.d[var3];
               WorldData var5 = var4.T();
               var1.a("world[" + var2 + "][dimension]", Integer.valueOf(var4.s.p().a()));
               var1.a("world[" + var2 + "][mode]", var5.q());
               var1.a("world[" + var2 + "][difficulty]", var4.ae());
               var1.a("world[" + var2 + "][hardcore]", Boolean.valueOf(var5.s()));
               var1.a("world[" + var2 + "][generator_name]", var5.t().a());
               var1.a("world[" + var2 + "][generator_version]", Integer.valueOf(var5.t().d()));
               var1.a("world[" + var2 + "][height]", Integer.valueOf(this.F));
               var1.a("world[" + var2 + "][chunks_loaded]", Integer.valueOf(var4.r().g()));
               ++var2;
            }
         }
      }

      var1.a("worlds", Integer.valueOf(var2));
   }

   public void b(MojangStatisticsGenerator var1) {
      var1.b("singleplayer", Boolean.valueOf(this.R()));
      var1.b("server_brand", this.getServerModName());
      var1.b("gui_supported", GraphicsEnvironment.isHeadless()?"headless":"supported");
      var1.b("dedicated", Boolean.valueOf(this.aa()));
   }

   public boolean Z() {
      return true;
   }

   public abstract boolean aa();

   public boolean ab() {
      return this.z;
   }

   public void d(boolean var1) {
      this.z = var1;
   }

   public boolean ac() {
      return this.A;
   }

   public void e(boolean var1) {
      this.A = var1;
   }

   public boolean ad() {
      return this.B;
   }

   public abstract boolean ae();

   public void f(boolean var1) {
      this.B = var1;
   }

   public boolean af() {
      return this.C;
   }

   public void g(boolean var1) {
      this.C = var1;
   }

   public boolean ag() {
      return this.D;
   }

   public void h(boolean var1) {
      this.D = var1;
   }

   public abstract boolean ah();

   public String ai() {
      return this.E;
   }

   public void l(String var1) {
      this.E = var1;
   }

   public int aj() {
      return this.F;
   }

   public void c(int var1) {
      this.F = var1;
   }

   public boolean ak() {
      return this.x;
   }

   public PlayerList getPlayerList() {
      return this.v;
   }

   public void a(PlayerList var1) {
      this.v = var1;
   }

   public void a(WorldSettings.EnumGamemode var1) {
      for(int var2 = 0; var2 < this.d.length; ++var2) {
         this.d[var2].T().a(var1);
      }

   }

   public ServerConnection am() {
      return this.p;
   }

   public boolean ao() {
      return false;
   }

   public abstract String a(WorldSettings.EnumGamemode var1, boolean var2);

   public int ap() {
      return this.y;
   }

   public void aq() {
      this.S = true;
   }

   public BlockPosition c() {
      return BlockPosition.a;
   }

   public Vec3D d() {
      return Vec3D.a;
   }

   public World e() {
      return this.d[0];
   }

   public Entity f() {
      return null;
   }

   public int as() {
      return 16;
   }

   public boolean a(World var1, BlockPosition var2, EntityHuman var3) {
      return false;
   }

   public void i(boolean var1) {
      this.T = var1;
   }

   public boolean at() {
      return this.T;
   }

   public Proxy au() {
      return this.e;
   }

   public static long av() {
      return System.currentTimeMillis();
   }

   public int aw() {
      return this.G;
   }

   public void d(int var1) {
      this.G = var1;
   }

   public IChatBaseComponent i_() {
      return new ChatComponentText(this.h_());
   }

   public boolean ax() {
      return true;
   }

   public MinecraftSessionService ay() {
      return this.V;
   }

   public GameProfileRepository az() {
      return this.W;
   }

   public UserCache aA() {
      return this.X;
   }

   public ServerPing aB() {
      return this.q;
   }

   public void aC() {
      this.Y = 0L;
   }

   public Entity a(UUID var1) {
      WorldServer[] var2 = this.d;
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         WorldServer var5 = var2[var4];
         if(var5 != null) {
            Entity var6 = var5.a(var1);
            if(var6 != null) {
               return var6;
            }
         }
      }

      return null;
   }

   public boolean z_() {
      return this.d[0].U().b("sendCommandFeedback");
   }

   public void a(CommandObjectiveExecutor.EnumCommandResult var1, int var2) {
   }

   public MinecraftServer h() {
      return this;
   }

   public int aD() {
      return 29999984;
   }

   public ListenableFuture a(Callable var1) {
      Validate.notNull(var1);
      if(!this.aE() && !this.ak()) {
         ListenableFutureTask var2 = ListenableFutureTask.create(var1);
         Queue var3 = this.j;
         synchronized(this.j) {
            this.j.add(var2);
            return var2;
         }
      } else {
         try {
            return Futures.immediateFuture(var1.call());
         } catch (Exception var6) {
            return Futures.immediateFailedCheckedFuture(var6);
         }
      }
   }

   public ListenableFuture a(Runnable var1) {
      Validate.notNull(var1);
      return this.a(Executors.callable(var1));
   }

   public boolean aE() {
      return Thread.currentThread() == this.Z;
   }

   public int aF() {
      return 256;
   }

   public long aG() {
      return this.aa;
   }

   public Thread aH() {
      return this.Z;
   }

   public class_pb aI() {
      return this.s;
   }

   public int a(WorldServer var1) {
      return var1 != null?var1.U().c("spawnRadius"):10;
   }
}
