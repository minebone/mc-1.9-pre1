package net.minecraft.server;

import net.minecraft.server.BiomeBase;
import net.minecraft.server.CustomWorldSettingsFinal;
import net.minecraft.server.IntCache;
import net.minecraft.server.WorldType;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_axu;

public class class_axo extends class_axu {
   private BiomeBase[] c = new BiomeBase[]{class_aik.d, class_aik.d, class_aik.d, class_aik.K, class_aik.K, class_aik.c};
   private BiomeBase[] d = new BiomeBase[]{class_aik.f, class_aik.E, class_aik.e, class_aik.c, class_aik.C, class_aik.h};
   private BiomeBase[] e = new BiomeBase[]{class_aik.f, class_aik.e, class_aik.g, class_aik.c};
   private BiomeBase[] f = new BiomeBase[]{class_aik.n, class_aik.n, class_aik.n, class_aik.F};
   private final CustomWorldSettingsFinal g;

   public class_axo(long var1, class_axu var3, WorldType var4, String var5) {
      super(var1);
      this.a = var3;
      if(var4 == WorldType.h) {
         this.c = new BiomeBase[]{class_aik.d, class_aik.f, class_aik.e, class_aik.h, class_aik.c, class_aik.g};
         this.g = null;
      } else if(var4 == WorldType.f) {
         this.g = CustomWorldSettingsFinal.class_a_in_class_atf.a(var5).b();
      } else {
         this.g = null;
      }

   }

   public int[] a(int var1, int var2, int var3, int var4) {
      int[] var5 = this.a.a(var1, var2, var3, var4);
      int[] var6 = IntCache.a(var3 * var4);

      for(int var7 = 0; var7 < var4; ++var7) {
         for(int var8 = 0; var8 < var3; ++var8) {
            this.a((long)(var8 + var1), (long)(var7 + var2));
            int var9 = var5[var8 + var7 * var3];
            int var10 = (var9 & 3840) >> 8;
            var9 &= -3841;
            if(this.g != null && this.g.F >= 0) {
               var6[var8 + var7 * var3] = this.g.F;
            } else if(b(var9)) {
               var6[var8 + var7 * var3] = var9;
            } else if(var9 == BiomeBase.a(class_aik.p)) {
               var6[var8 + var7 * var3] = var9;
            } else if(var9 == 1) {
               if(var10 > 0) {
                  if(this.a(3) == 0) {
                     var6[var8 + var7 * var3] = BiomeBase.a(class_aik.O);
                  } else {
                     var6[var8 + var7 * var3] = BiomeBase.a(class_aik.N);
                  }
               } else {
                  var6[var8 + var7 * var3] = BiomeBase.a(this.c[this.a(this.c.length)]);
               }
            } else if(var9 == 2) {
               if(var10 > 0) {
                  var6[var8 + var7 * var3] = BiomeBase.a(class_aik.w);
               } else {
                  var6[var8 + var7 * var3] = BiomeBase.a(this.d[this.a(this.d.length)]);
               }
            } else if(var9 == 3) {
               if(var10 > 0) {
                  var6[var8 + var7 * var3] = BiomeBase.a(class_aik.H);
               } else {
                  var6[var8 + var7 * var3] = BiomeBase.a(this.e[this.a(this.e.length)]);
               }
            } else if(var9 == 4) {
               var6[var8 + var7 * var3] = BiomeBase.a(this.f[this.a(this.f.length)]);
            } else {
               var6[var8 + var7 * var3] = BiomeBase.a(class_aik.p);
            }
         }
      }

      return var6;
   }
}
