package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockSnow;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.class_acb;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aoo;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;

public class class_aer extends class_acb {
   public class_aer(Block var1) {
      super(var1);
      this.e(0);
      this.a(true);
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      if(var1.b != 0 && var2.a(var4, var6, var1)) {
         IBlockData var10 = var3.getType(var4);
         Block var11 = var10.getBlock();
         BlockPosition var12 = var4;
         if((var6 != EnumDirection.UP || var11 != this.a) && !var11.a((class_ahw)var3, (BlockPosition)var4)) {
            var12 = var4.a(var6);
            var10 = var3.getType(var12);
            var11 = var10.getBlock();
         }

         if(var11 == this.a) {
            int var13 = ((Integer)var10.get(BlockSnow.a)).intValue();
            if(var13 <= 7) {
               IBlockData var14 = var10.set(BlockSnow.a, Integer.valueOf(var13 + 1));
               AxisAlignedBB var15 = var14.d(var3, var12);
               if(var15 != Block.k && var3.c(var15.a(var12)) && var3.a((BlockPosition)var12, (IBlockData)var14, 10)) {
                  class_aoo var16 = this.a.w();
                  var3.a(var2, var12, var16.e(), EnumSoundCategory.BLOCKS, (var16.a() + 1.0F) / 2.0F, var16.b() * 0.8F);
                  --var1.b;
                  return EnumResult.SUCCESS;
               }
            }
         }

         return super.a(var1, var2, var3, var12, var5, var6, var7, var8, var9);
      } else {
         return EnumResult.FAIL;
      }
   }

   public int a(int var1) {
      return var1;
   }
}
