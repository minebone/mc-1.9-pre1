package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockSnow;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.class_acb;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aoo;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_rz;

public class class_acc extends Item {
   private Block a;

   public class_acc(Block var1) {
      this.a = var1;
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      IBlockData var10 = var3.getType(var4);
      Block var11 = var10.getBlock();
      if(var11 == Blocks.aH && ((Integer)var10.get(BlockSnow.a)).intValue() < 1) {
         var6 = EnumDirection.UP;
      } else if(!var11.a((class_ahw)var3, (BlockPosition)var4)) {
         var4 = var4.a(var6);
      }

      if(var2.a(var4, var6, var1) && var1.b != 0 && var3.a(this.a, var4, false, var6, (Entity)null, var1)) {
         IBlockData var12 = this.a.a(var3, var4, var6, var7, var8, var9, 0, var2);
         if(!var3.a((BlockPosition)var4, (IBlockData)var12, 11)) {
            return EnumResult.FAIL;
         } else {
            var12 = var3.getType(var4);
            if(var12.getBlock() == this.a) {
               class_acb.a(var3, var2, var4, var1);
               var12.getBlock().a((World)var3, (BlockPosition)var4, (IBlockData)var12, (class_rz)var2, (ItemStack)var1);
            }

            class_aoo var13 = this.a.w();
            var3.a(var2, var4, var13.e(), EnumSoundCategory.BLOCKS, (var13.a() + 1.0F) / 2.0F, var13.b() * 0.8F);
            --var1.b;
            return EnumResult.SUCCESS;
         }
      } else {
         return EnumResult.FAIL;
      }
   }
}
