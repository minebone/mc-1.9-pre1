package net.minecraft.server;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.CrashReport;
import net.minecraft.server.CrashReportSystemDetails;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.class_e;
import net.minecraft.server.class_l;
import net.minecraft.server.ICommandListener;

public abstract class class_ahi implements ICommandListener {
   private static final SimpleDateFormat a = new SimpleDateFormat("HH:mm:ss");
   private int b;
   private boolean c = true;
   private IChatBaseComponent d = null;
   private String e = "";
   private String f = "@";
   private final CommandObjectiveExecutor g = new CommandObjectiveExecutor();

   public int k() {
      return this.b;
   }

   public void a(int var1) {
      this.b = var1;
   }

   public IChatBaseComponent l() {
      return (IChatBaseComponent)(this.d == null?new ChatComponentText(""):this.d);
   }

   public void a(NBTTagCompound var1) {
      var1.a("Command", this.e);
      var1.a("SuccessCount", this.b);
      var1.a("CustomName", this.f);
      var1.a("TrackOutput", this.c);
      if(this.d != null && this.c) {
         var1.a("LastOutput", IChatBaseComponent.ChatSerializer.a(this.d));
      }

      this.g.b(var1);
   }

   public void b(NBTTagCompound var1) {
      this.e = var1.l("Command");
      this.b = var1.h("SuccessCount");
      if(var1.b("CustomName", 8)) {
         this.f = var1.l("CustomName");
      }

      if(var1.b("TrackOutput", 1)) {
         this.c = var1.p("TrackOutput");
      }

      if(var1.b("LastOutput", 8) && this.c) {
         try {
            this.d = IChatBaseComponent.ChatSerializer.a(var1.l("LastOutput"));
         } catch (Throwable var3) {
            this.d = new ChatComponentText(var3.getMessage());
         }
      } else {
         this.d = null;
      }

      this.g.a(var1);
   }

   public boolean a(int var1, String var2) {
      return var1 <= 2;
   }

   public void a(String var1) {
      this.e = var1;
      this.b = 0;
   }

   public String m() {
      return this.e;
   }

   public void a(World var1) {
      if(var1.E) {
         this.b = 0;
      } else if("Searge".equalsIgnoreCase(this.e)) {
         this.d = new ChatComponentText("#itzlipofutzli");
         this.b = 1;
      } else {
         MinecraftServer var2 = this.h();
         if(var2 != null && var2.M() && var2.ah()) {
            class_l var3 = var2.N();

            try {
               this.d = null;
               this.b = var3.a(this, this.e);
            } catch (Throwable var7) {
               CrashReport var5 = CrashReport.a(var7, "Executing command block");
               CrashReportSystemDetails var6 = var5.a("Command to be executed");
               var6.a("Command", new Callable() {
                  public String a() throws Exception {
                     return class_ahi.this.m();
                  }

                  // $FF: synthetic method
                  public Object call() throws Exception {
                     return this.a();
                  }
               });
               var6.a("Name", new Callable() {
                  public String a() throws Exception {
                     return class_ahi.this.h_();
                  }

                  // $FF: synthetic method
                  public Object call() throws Exception {
                     return this.a();
                  }
               });
               throw new class_e(var5);
            }
         } else {
            this.b = 0;
         }

      }
   }

   public String h_() {
      return this.f;
   }

   public IChatBaseComponent i_() {
      return new ChatComponentText(this.h_());
   }

   public void b(String var1) {
      this.f = var1;
   }

   public void a(IChatBaseComponent var1) {
      if(this.c && this.e() != null && !this.e().E) {
         this.d = (new ChatComponentText("[" + a.format(new Date()) + "] ")).a(var1);
         this.i();
      }

   }

   public boolean z_() {
      MinecraftServer var1 = this.h();
      return var1 == null || !var1.M() || var1.d[0].U().b("commandBlockOutput");
   }

   public void a(CommandObjectiveExecutor.EnumCommandResult var1, int var2) {
      this.g.a(this.h(), this, var1, var2);
   }

   public abstract void i();

   public void b(IChatBaseComponent var1) {
      this.d = var1;
   }

   public void a(boolean var1) {
      this.c = var1;
   }

   public boolean n() {
      return this.c;
   }

   public boolean a(EntityHuman var1) {
      if(!var1.abilities.d) {
         return false;
      } else {
         if(var1.e().E) {
            var1.a(this);
         }

         return true;
      }
   }

   public CommandObjectiveExecutor o() {
      return this.g;
   }
}
