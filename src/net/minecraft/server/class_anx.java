package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_anx extends Block {
   private final boolean a;

   public class_anx(boolean var1) {
      super(Material.t);
      this.a = var1;
      if(var1) {
         this.a(1.0F);
      }

   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      if(!var1.E) {
         if(this.a && !var1.y(var2)) {
            var1.a((BlockPosition)var2, (IBlockData)Blocks.bJ.u(), 2);
         } else if(!this.a && var1.y(var2)) {
            var1.a((BlockPosition)var2, (IBlockData)Blocks.bK.u(), 2);
         }

      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!var1.E) {
         if(this.a && !var1.y(var2)) {
            var1.a((BlockPosition)var2, (Block)this, 4);
         } else if(!this.a && var1.y(var2)) {
            var1.a((BlockPosition)var2, (IBlockData)Blocks.bK.u(), 2);
         }

      }
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(!var1.E) {
         if(this.a && !var1.y(var2)) {
            var1.a((BlockPosition)var2, (IBlockData)Blocks.bJ.u(), 2);
         }

      }
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Item.a(Blocks.bJ);
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Blocks.bJ);
   }

   protected ItemStack u(IBlockData var1) {
      return new ItemStack(Blocks.bJ);
   }
}
