package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.TileEntityBanner;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_amf;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arn;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;

public class class_ajk extends class_ajm {
   public static final class_arn a = class_amf.D;
   public static final BlockStateInteger b = BlockStateInteger.a("rotation", 0, 15);
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.25D, 0.0D, 0.25D, 0.75D, 1.0D, 0.75D);

   protected class_ajk() {
      super(Material.d);
   }

   public String c() {
      return class_di.a("item.banner.white.name");
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(class_ahw var1, BlockPosition var2) {
      return true;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean d() {
      return true;
   }

   public TileEntity a(World var1, int var2) {
      return new TileEntityBanner();
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Items.cO;
   }

   private ItemStack e(World var1, BlockPosition var2, IBlockData var3) {
      TileEntity var4 = var1.r(var2);
      if(var4 instanceof TileEntityBanner) {
         ItemStack var5 = new ItemStack(Items.cO, 1, ((TileEntityBanner)var4).b());
         NBTTagCompound var6 = new NBTTagCompound();
         var4.a(var6);
         var6.q("x");
         var6.q("y");
         var6.q("z");
         var6.q("id");
         var5.a((String)"BlockEntityTag", (NBTTag)var6);
         return var5;
      } else {
         return null;
      }
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      ItemStack var4 = this.e(var1, var2, var3);
      return var4 != null?var4:new ItemStack(Items.cO);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, float var4, int var5) {
      ItemStack var6 = this.e(var1, var2, var3);
      if(var6 != null) {
         a((World)var1, (BlockPosition)var2, (ItemStack)var6);
      } else {
         super.a(var1, var2, var3, var4, var5);
      }

   }

   public boolean a(World var1, BlockPosition var2) {
      return !this.b(var1, var2) && super.a(var1, var2);
   }

   public void a(World var1, EntityHuman var2, BlockPosition var3, IBlockData var4, TileEntity var5, ItemStack var6) {
      if(var5 instanceof TileEntityBanner) {
         TileEntityBanner var7 = (TileEntityBanner)var5;
         ItemStack var8 = new ItemStack(Items.cO, 1, ((TileEntityBanner)var5).b());
         NBTTagCompound var9 = new NBTTagCompound();
         TileEntityBanner.a(var9, var7.b(), var7.d());
         var8.a((String)"BlockEntityTag", (NBTTag)var9);
         a((World)var1, (BlockPosition)var3, (ItemStack)var8);
      } else {
         super.a(var1, var2, var3, var4, (TileEntity)null, var6);
      }

   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumDirection.values().length];

      static {
         try {
            a[EnumDirection.NORTH.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.SOUTH.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.EAST.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static class class_a_in_class_ajk extends class_ajk {
      public class_a_in_class_ajk() {
         this.w(this.A.b().set(b, Integer.valueOf(0)));
      }

      public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
         return c;
      }

      public IBlockData a(IBlockData var1, class_aod var2) {
         return var1.set(b, Integer.valueOf(var2.a(((Integer)var1.get(b)).intValue(), 16)));
      }

      public IBlockData a(IBlockData var1, class_amq var2) {
         return var1.set(b, Integer.valueOf(var2.a(((Integer)var1.get(b)).intValue(), 16)));
      }

      public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
         if(!var1.getType(var2.b()).getMaterial().a()) {
            this.b(var1, var2, var3, 0);
            var1.g(var2);
         }

         super.a(var1, var2, var3, var4);
      }

      public IBlockData a(int var1) {
         return this.u().set(b, Integer.valueOf(var1));
      }

      public int e(IBlockData var1) {
         return ((Integer)var1.get(b)).intValue();
      }

      protected BlockStateList b() {
         return new BlockStateList(this, new IBlockState[]{b});
      }
   }

   public static class class_b_in_class_ajk extends class_ajk {
      protected static final AxisAlignedBB d = new AxisAlignedBB(0.0D, 0.0D, 0.875D, 1.0D, 0.78125D, 1.0D);
      protected static final AxisAlignedBB e = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.78125D, 0.125D);
      protected static final AxisAlignedBB f = new AxisAlignedBB(0.875D, 0.0D, 0.0D, 1.0D, 0.78125D, 1.0D);
      protected static final AxisAlignedBB g = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.125D, 0.78125D, 1.0D);

      public class_b_in_class_ajk() {
         this.w(this.A.b().set(a, EnumDirection.NORTH));
      }

      public IBlockData a(IBlockData var1, class_aod var2) {
         return var1.set(a, var2.a((EnumDirection)var1.get(a)));
      }

      public IBlockData a(IBlockData var1, class_amq var2) {
         return var1.a(var2.a((EnumDirection)var1.get(a)));
      }

      public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
         switch(class_ajk.SyntheticClass_1.a[((EnumDirection)var1.get(a)).ordinal()]) {
         case 1:
         default:
            return d;
         case 2:
            return e;
         case 3:
            return f;
         case 4:
            return g;
         }
      }

      public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
         EnumDirection var5 = (EnumDirection)var3.get(a);
         if(!var1.getType(var2.a(var5.d())).getMaterial().a()) {
            this.b(var1, var2, var3, 0);
            var1.g(var2);
         }

         super.a(var1, var2, var3, var4);
      }

      public IBlockData a(int var1) {
         EnumDirection var2 = EnumDirection.a(var1);
         if(var2.k() == EnumDirection.class_a_in_class_cq.Y) {
            var2 = EnumDirection.NORTH;
         }

         return this.u().set(a, var2);
      }

      public int e(IBlockData var1) {
         return ((EnumDirection)var1.get(a)).a();
      }

      protected BlockStateList b() {
         return new BlockStateList(this, new IBlockState[]{a});
      }
   }
}
