package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.class_ady;

public class ItemAnvil extends class_ady {
   public ItemAnvil(Block var1) {
      super(var1, var1, new String[]{"intact", "slightlyDamaged", "veryDamaged"});
   }

   public int a(int var1) {
      return var1 << 2;
   }
}
