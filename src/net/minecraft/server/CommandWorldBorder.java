package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_aru;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.MathHelper;

public class CommandWorldBorder extends CommandAbstract {
   public String c() {
      return "worldborder";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.worldborder.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 1) {
         throw new class_cf("commands.worldborder.usage", new Object[0]);
      } else {
         class_aru var4 = this.a(var1);
         double var5;
         double var7;
         long var9;
         if(var3[0].equals("set")) {
            if(var3.length != 2 && var3.length != 3) {
               throw new class_cf("commands.worldborder.set.usage", new Object[0]);
            }

            var5 = var4.j();
            var7 = a(var3[1], 1.0D, 6.0E7D);
            var9 = var3.length > 2?a(var3[2], 0L, 9223372036854775L) * 1000L:0L;
            if(var9 > 0L) {
               var4.a(var5, var7, var9);
               if(var5 > var7) {
                  a(var2, this, "commands.worldborder.setSlowly.shrink.success", new Object[]{String.format("%.1f", new Object[]{Double.valueOf(var7)}), String.format("%.1f", new Object[]{Double.valueOf(var5)}), Long.toString(var9 / 1000L)});
               } else {
                  a(var2, this, "commands.worldborder.setSlowly.grow.success", new Object[]{String.format("%.1f", new Object[]{Double.valueOf(var7)}), String.format("%.1f", new Object[]{Double.valueOf(var5)}), Long.toString(var9 / 1000L)});
               }
            } else {
               var4.a(var7);
               a(var2, this, "commands.worldborder.set.success", new Object[]{String.format("%.1f", new Object[]{Double.valueOf(var7)}), String.format("%.1f", new Object[]{Double.valueOf(var5)})});
            }
         } else if(var3[0].equals("add")) {
            if(var3.length != 2 && var3.length != 3) {
               throw new class_cf("commands.worldborder.add.usage", new Object[0]);
            }

            var5 = var4.h();
            var7 = var5 + a(var3[1], -var5, 6.0E7D - var5);
            var9 = var4.i() + (var3.length > 2?a(var3[2], 0L, 9223372036854775L) * 1000L:0L);
            if(var9 > 0L) {
               var4.a(var5, var7, var9);
               if(var5 > var7) {
                  a(var2, this, "commands.worldborder.setSlowly.shrink.success", new Object[]{String.format("%.1f", new Object[]{Double.valueOf(var7)}), String.format("%.1f", new Object[]{Double.valueOf(var5)}), Long.toString(var9 / 1000L)});
               } else {
                  a(var2, this, "commands.worldborder.setSlowly.grow.success", new Object[]{String.format("%.1f", new Object[]{Double.valueOf(var7)}), String.format("%.1f", new Object[]{Double.valueOf(var5)}), Long.toString(var9 / 1000L)});
               }
            } else {
               var4.a(var7);
               a(var2, this, "commands.worldborder.set.success", new Object[]{String.format("%.1f", new Object[]{Double.valueOf(var7)}), String.format("%.1f", new Object[]{Double.valueOf(var5)})});
            }
         } else if(var3[0].equals("center")) {
            if(var3.length != 3) {
               throw new class_cf("commands.worldborder.center.usage", new Object[0]);
            }

            BlockPosition var11 = var2.c();
            double var6 = b((double)var11.p() + 0.5D, var3[1], true);
            double var8 = b((double)var11.r() + 0.5D, var3[2], true);
            var4.c(var6, var8);
            a(var2, this, "commands.worldborder.center.success", new Object[]{Double.valueOf(var6), Double.valueOf(var8)});
         } else if(var3[0].equals("damage")) {
            if(var3.length < 2) {
               throw new class_cf("commands.worldborder.damage.usage", new Object[0]);
            }

            if(var3[1].equals("buffer")) {
               if(var3.length != 3) {
                  throw new class_cf("commands.worldborder.damage.buffer.usage", new Object[0]);
               }

               var5 = a(var3[2], 0.0D);
               var7 = var4.m();
               var4.b(var5);
               a(var2, this, "commands.worldborder.damage.buffer.success", new Object[]{String.format("%.1f", new Object[]{Double.valueOf(var5)}), String.format("%.1f", new Object[]{Double.valueOf(var7)})});
            } else if(var3[1].equals("amount")) {
               if(var3.length != 3) {
                  throw new class_cf("commands.worldborder.damage.amount.usage", new Object[0]);
               }

               var5 = a(var3[2], 0.0D);
               var7 = var4.n();
               var4.c(var5);
               a(var2, this, "commands.worldborder.damage.amount.success", new Object[]{String.format("%.2f", new Object[]{Double.valueOf(var5)}), String.format("%.2f", new Object[]{Double.valueOf(var7)})});
            }
         } else if(var3[0].equals("warning")) {
            if(var3.length < 2) {
               throw new class_cf("commands.worldborder.warning.usage", new Object[0]);
            }

            int var12;
            int var13;
            if(var3[1].equals("time")) {
               if(var3.length != 3) {
                  throw new class_cf("commands.worldborder.warning.time.usage", new Object[0]);
               }

               var13 = a(var3[2], 0);
               var12 = var4.p();
               var4.b(var13);
               a(var2, this, "commands.worldborder.warning.time.success", new Object[]{Integer.valueOf(var13), Integer.valueOf(var12)});
            } else if(var3[1].equals("distance")) {
               if(var3.length != 3) {
                  throw new class_cf("commands.worldborder.warning.distance.usage", new Object[0]);
               }

               var13 = a(var3[2], 0);
               var12 = var4.q();
               var4.c(var13);
               a(var2, this, "commands.worldborder.warning.distance.success", new Object[]{Integer.valueOf(var13), Integer.valueOf(var12)});
            }
         } else {
            if(!var3[0].equals("get")) {
               throw new class_cf("commands.worldborder.usage", new Object[0]);
            }

            var5 = var4.h();
            var2.a(CommandObjectiveExecutor.EnumCommandResult.QUERY_RESULT, MathHelper.c(var5 + 0.5D));
            var2.a(new ChatMessage("commands.worldborder.get.success", new Object[]{String.format("%.0f", new Object[]{Double.valueOf(var5)})}));
         }

      }
   }

   protected class_aru a(MinecraftServer var1) {
      return var1.d[0].aj();
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, new String[]{"set", "center", "damage", "warning", "add", "get"}):(var3.length == 2 && var3[0].equals("damage")?a(var3, new String[]{"buffer", "amount"}):(var3.length >= 2 && var3.length <= 3 && var3[0].equals("center")?b(var3, 1, var4):(var3.length == 2 && var3[0].equals("warning")?a(var3, new String[]{"time", "distance"}):Collections.emptyList())));
   }
}
