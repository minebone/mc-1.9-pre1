package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Entity;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_yc;

public class class_qi {
   private static final Random a = new Random();

   public static void a(World var0, BlockPosition var1, IInventory var2) {
      a(var0, (double)var1.p(), (double)var1.q(), (double)var1.r(), var2);
   }

   public static void a(World var0, Entity var1, IInventory var2) {
      a(var0, var1.locX, var1.locY, var1.locZ, var2);
   }

   private static void a(World var0, double var1, double var3, double var5, IInventory var7) {
      for(int var8 = 0; var8 < var7.u_(); ++var8) {
         ItemStack var9 = var7.a(var8);
         if(var9 != null) {
            a(var0, var1, var3, var5, var9);
         }
      }

   }

   public static void a(World var0, double var1, double var3, double var5, ItemStack var7) {
      float var8 = a.nextFloat() * 0.8F + 0.1F;
      float var9 = a.nextFloat() * 0.8F + 0.1F;
      float var10 = a.nextFloat() * 0.8F + 0.1F;

      while(var7.b > 0) {
         int var11 = a.nextInt(21) + 10;
         if(var11 > var7.b) {
            var11 = var7.b;
         }

         var7.b -= var11;
         class_yc var12 = new class_yc(var0, var1 + (double)var8, var3 + (double)var9, var5 + (double)var10, new ItemStack(var7.b(), var11, var7.i()));
         if(var7.n()) {
            var12.k().d((NBTTagCompound)var7.o().b());
         }

         float var13 = 0.05F;
         var12.motX = a.nextGaussian() * (double)var13;
         var12.motY = a.nextGaussian() * (double)var13 + 0.20000000298023224D;
         var12.motZ = a.nextGaussian() * (double)var13;
         var0.a((Entity)var12);
      }

   }
}
