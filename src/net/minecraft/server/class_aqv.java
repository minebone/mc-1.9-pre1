package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockPistonExtension;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.TileEntity;
import net.minecraft.server.TileEntityPiston;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_aqt;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arn;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;

public class class_aqv extends class_ajm {
   public static final class_arn a = BlockPistonExtension.H;
   public static final BlockStateEnum b = BlockPistonExtension.a;

   public class_aqv() {
      super(Material.H);
      this.w(this.A.b().set(a, EnumDirection.NORTH).set(b, BlockPistonExtension.EnumPistonType.DEFAULT));
      this.c(-1.0F);
   }

   public TileEntity a(World var1, int var2) {
      return null;
   }

   public static TileEntity a(IBlockData var0, EnumDirection var1, boolean var2, boolean var3) {
      return new TileEntityPiston(var0, var1, var2, var3);
   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      TileEntity var4 = var1.r(var2);
      if(var4 instanceof TileEntityPiston) {
         ((TileEntityPiston)var4).h();
      } else {
         super.b(var1, var2, var3);
      }

   }

   public boolean a(World var1, BlockPosition var2) {
      return false;
   }

   public boolean b(World var1, BlockPosition var2, EnumDirection var3) {
      return false;
   }

   public void d(World var1, BlockPosition var2, IBlockData var3) {
      BlockPosition var4 = var2.a(((EnumDirection)var3.get(a)).d());
      IBlockData var5 = var1.getType(var4);
      if(var5.getBlock() instanceof class_aqt && ((Boolean)var5.get(class_aqt.a)).booleanValue()) {
         var1.g(var4);
      }

   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(!var1.E && var1.r(var2) == null) {
         var1.g(var2);
         return true;
      } else {
         return false;
      }
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return null;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, float var4, int var5) {
      if(!var1.E) {
         TileEntityPiston var6 = this.c(var1, var2);
         if(var6 != null) {
            IBlockData var7 = var6.b();
            var7.getBlock().b(var1, var2, var7, 0);
         }
      }
   }

   public MovingObjectPosition a(IBlockData var1, World var2, BlockPosition var3, Vec3D var4, Vec3D var5) {
      return null;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!var1.E) {
         var1.r(var2);
      }

   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      TileEntityPiston var4 = this.c(var2, var3);
      return var4 == null?null:var4.a((class_ahw)var2, (BlockPosition)var3);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      TileEntityPiston var4 = this.c(var2, var3);
      return var4 != null?var4.a(var2, var3):j;
   }

   private TileEntityPiston c(class_ahw var1, BlockPosition var2) {
      TileEntity var3 = var1.r(var2);
      return var3 instanceof TileEntityPiston?(TileEntityPiston)var3:null;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return null;
   }

   public IBlockData a(int var1) {
      return this.u().set(a, BlockPistonExtension.e(var1)).set(b, (var1 & 8) > 0?BlockPistonExtension.EnumPistonType.STICKY:BlockPistonExtension.EnumPistonType.DEFAULT);
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(a, var2.a((EnumDirection)var1.get(a)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(a)));
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((EnumDirection)var1.get(a)).a();
      if(var1.get(b) == BlockPistonExtension.EnumPistonType.STICKY) {
         var3 |= 8;
      }

      return var3;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b});
   }
}
