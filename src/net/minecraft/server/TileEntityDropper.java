package net.minecraft.server;

import net.minecraft.server.TileEntityDispenser;

public class TileEntityDropper extends TileEntityDispenser {
   public String h_() {
      return this.o_()?this.a:"container.dropper";
   }

   public String k() {
      return "minecraft:dropper";
   }
}
