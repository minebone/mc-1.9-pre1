package net.minecraft.server;

import java.util.List;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Entity;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_aqq;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_alb extends class_ajm {
   protected static final AxisAlignedBB a = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.75D, 1.0D);

   protected class_alb(Material var1) {
      super(var1);
      this.a(1.0F);
   }

   public TileEntity a(World var1, int var2) {
      return new class_aqq();
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return a;
   }

   public void a(IBlockData var1, World var2, BlockPosition var3, AxisAlignedBB var4, List var5, Entity var6) {
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public int a(Random var1) {
      return 0;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Entity var4) {
      if(!var4.aH() && !var4.aI() && var4.aU() && !var1.E && var4.bk().b(var3.c(var1, var2).a(var2))) {
         var4.c(1);
      }

   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return null;
   }

   public MaterialMapColor r(IBlockData var1) {
      return MaterialMapColor.E;
   }
}
