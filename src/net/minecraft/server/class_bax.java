package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Map;
import net.minecraft.server.class_bay;
import net.minecraft.server.class_baz;
import net.minecraft.server.class_kk;

public class class_bax {
   private static final Map a = Maps.newHashMap();
   private static final Map b = Maps.newHashMap();

   public static void a(class_bay.class_a_in_class_bay var0) {
      class_kk var1 = var0.a();
      Class var2 = var0.b();
      if(a.containsKey(var1)) {
         throw new IllegalArgumentException("Can\'t re-register entity property name " + var1);
      } else if(b.containsKey(var2)) {
         throw new IllegalArgumentException("Can\'t re-register entity property class " + var2.getName());
      } else {
         a.put(var1, var0);
         b.put(var2, var0);
      }
   }

   public static class_bay.class_a_in_class_bay a(class_kk var0) {
      class_bay.class_a_in_class_bay var1 = (class_bay.class_a_in_class_bay)a.get(var0);
      if(var1 == null) {
         throw new IllegalArgumentException("Unknown loot entity property \'" + var0 + "\'");
      } else {
         return var1;
      }
   }

   public static class_bay.class_a_in_class_bay a(class_bay var0) {
      class_bay.class_a_in_class_bay var1 = (class_bay.class_a_in_class_bay)b.get(var0.getClass());
      if(var1 == null) {
         throw new IllegalArgumentException("Unknown loot entity property " + var0);
      } else {
         return var1;
      }
   }

   static {
      a((class_bay.class_a_in_class_bay)(new class_baz.class_a_in_class_baz()));
   }
}
