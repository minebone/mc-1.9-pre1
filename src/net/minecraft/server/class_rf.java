package net.minecraft.server;

import net.minecraft.server.AttributeMapBase;
import net.minecraft.server.MobEffectType;
import net.minecraft.server.class_rz;

public class class_rf extends MobEffectType {
   protected class_rf(boolean var1, int var2) {
      super(var1, var2);
   }

   public void a(class_rz var1, AttributeMapBase var2, int var3) {
      var1.n(var1.co() - (float)(4 * (var3 + 1)));
      super.a(var1, var2, var3);
   }

   public void b(class_rz var1, AttributeMapBase var2, int var3) {
      var1.n(var1.co() + (float)(4 * (var3 + 1)));
      super.b(var1, var2, var3);
   }
}
