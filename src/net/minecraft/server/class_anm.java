package net.minecraft.server;

import com.google.common.base.Predicate;
import net.minecraft.server.Block;
import net.minecraft.server.BlockMinecartTrackAbstract;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.World;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.BlockPosition;

public class class_anm extends BlockMinecartTrackAbstract {
   public static final BlockStateEnum d = BlockStateEnum.a("shape", BlockMinecartTrackAbstract.EnumTrackPosition.class, new Predicate() {
      public boolean a(BlockMinecartTrackAbstract.EnumTrackPosition var1) {
         return var1 != BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST && var1 != BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST && var1 != BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST && var1 != BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST;
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((BlockMinecartTrackAbstract.EnumTrackPosition)var1);
      }
   });
   public static final class_arm e = class_arm.a("powered");

   protected class_anm() {
      super(true);
      this.w(this.A.b().set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH).set(e, Boolean.valueOf(false)));
   }

   protected boolean a(World var1, BlockPosition var2, IBlockData var3, boolean var4, int var5) {
      if(var5 >= 8) {
         return false;
      } else {
         int var6 = var2.p();
         int var7 = var2.q();
         int var8 = var2.r();
         boolean var9 = true;
         BlockMinecartTrackAbstract.EnumTrackPosition var10 = (BlockMinecartTrackAbstract.EnumTrackPosition)var3.get(d);
         switch(class_anm.SyntheticClass_1.a[var10.ordinal()]) {
         case 1:
            if(var4) {
               ++var8;
            } else {
               --var8;
            }
            break;
         case 2:
            if(var4) {
               --var6;
            } else {
               ++var6;
            }
            break;
         case 3:
            if(var4) {
               --var6;
            } else {
               ++var6;
               ++var7;
               var9 = false;
            }

            var10 = BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST;
            break;
         case 4:
            if(var4) {
               --var6;
               ++var7;
               var9 = false;
            } else {
               ++var6;
            }

            var10 = BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST;
            break;
         case 5:
            if(var4) {
               ++var8;
            } else {
               --var8;
               ++var7;
               var9 = false;
            }

            var10 = BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH;
            break;
         case 6:
            if(var4) {
               ++var8;
               ++var7;
               var9 = false;
            } else {
               --var8;
            }

            var10 = BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH;
         }

         return this.a(var1, new BlockPosition(var6, var7, var8), var4, var5, var10)?true:var9 && this.a(var1, new BlockPosition(var6, var7 - 1, var8), var4, var5, var10);
      }
   }

   protected boolean a(World var1, BlockPosition var2, boolean var3, int var4, BlockMinecartTrackAbstract.EnumTrackPosition var5) {
      IBlockData var6 = var1.getType(var2);
      if(var6.getBlock() != this) {
         return false;
      } else {
         BlockMinecartTrackAbstract.EnumTrackPosition var7 = (BlockMinecartTrackAbstract.EnumTrackPosition)var6.get(d);
         return var5 == BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST && (var7 == BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH || var7 == BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH || var7 == BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH)?false:(var5 == BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH && (var7 == BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST || var7 == BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST || var7 == BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST)?false:(((Boolean)var6.get(e)).booleanValue()?(var1.y(var2)?true:this.a(var1, var2, var6, var3, var4 + 1)):false));
      }
   }

   protected void b(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      boolean var5 = ((Boolean)var3.get(e)).booleanValue();
      boolean var6 = var1.y(var2) || this.a(var1, var2, var3, true, 0) || this.a(var1, var2, var3, false, 0);
      if(var6 != var5) {
         var1.a((BlockPosition)var2, (IBlockData)var3.set(e, Boolean.valueOf(var6)), 3);
         var1.d(var2.b(), this);
         if(((BlockMinecartTrackAbstract.EnumTrackPosition)var3.get(d)).c()) {
            var1.d(var2.a(), this);
         }
      }

   }

   public IBlockState g() {
      return d;
   }

   public IBlockData a(int var1) {
      return this.u().set(d, BlockMinecartTrackAbstract.EnumTrackPosition.a(var1 & 7)).set(e, Boolean.valueOf((var1 & 8) > 0));
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((BlockMinecartTrackAbstract.EnumTrackPosition)var1.get(d)).a();
      if(((Boolean)var1.get(e)).booleanValue()) {
         var3 |= 8;
      }

      return var3;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      switch(class_anm.SyntheticClass_1.b[var2.ordinal()]) {
      case 1:
         switch(class_anm.SyntheticClass_1.a[((BlockMinecartTrackAbstract.EnumTrackPosition)var1.get(d)).ordinal()]) {
         case 3:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST);
         case 4:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST);
         case 5:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH);
         case 6:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH);
         case 7:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST);
         case 8:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST);
         case 9:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST);
         case 10:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST);
         }
      case 2:
         switch(class_anm.SyntheticClass_1.a[((BlockMinecartTrackAbstract.EnumTrackPosition)var1.get(d)).ordinal()]) {
         case 1:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST);
         case 2:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH);
         case 3:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH);
         case 4:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH);
         case 5:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST);
         case 6:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST);
         case 7:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST);
         case 8:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST);
         case 9:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST);
         case 10:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST);
         }
      case 3:
         switch(class_anm.SyntheticClass_1.a[((BlockMinecartTrackAbstract.EnumTrackPosition)var1.get(d)).ordinal()]) {
         case 1:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST);
         case 2:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH);
         case 3:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH);
         case 4:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH);
         case 5:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST);
         case 6:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST);
         case 7:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST);
         case 8:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST);
         case 9:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST);
         case 10:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST);
         }
      default:
         return var1;
      }
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      BlockMinecartTrackAbstract.EnumTrackPosition var3 = (BlockMinecartTrackAbstract.EnumTrackPosition)var1.get(d);
      switch(class_anm.SyntheticClass_1.c[var2.ordinal()]) {
      case 1:
         switch(class_anm.SyntheticClass_1.a[var3.ordinal()]) {
         case 5:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH);
         case 6:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH);
         case 7:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST);
         case 8:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST);
         case 9:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST);
         case 10:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST);
         default:
            return super.a(var1, var2);
         }
      case 2:
         switch(class_anm.SyntheticClass_1.a[var3.ordinal()]) {
         case 3:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST);
         case 4:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST);
         case 5:
         case 6:
         default:
            break;
         case 7:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST);
         case 8:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST);
         case 9:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST);
         case 10:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST);
         }
      }

      return super.a(var1, var2);
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{d, e});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b;
      // $FF: synthetic field
      static final int[] c = new int[class_amq.values().length];

      static {
         try {
            c[class_amq.LEFT_RIGHT.ordinal()] = 1;
         } catch (NoSuchFieldError var15) {
            ;
         }

         try {
            c[class_amq.FRONT_BACK.ordinal()] = 2;
         } catch (NoSuchFieldError var14) {
            ;
         }

         b = new int[class_aod.values().length];

         try {
            b[class_aod.CLOCKWISE_180.ordinal()] = 1;
         } catch (NoSuchFieldError var13) {
            ;
         }

         try {
            b[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var12) {
            ;
         }

         try {
            b[class_aod.CLOCKWISE_90.ordinal()] = 3;
         } catch (NoSuchFieldError var11) {
            ;
         }

         a = new int[BlockMinecartTrackAbstract.EnumTrackPosition.values().length];

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH.ordinal()] = 1;
         } catch (NoSuchFieldError var10) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST.ordinal()] = 2;
         } catch (NoSuchFieldError var9) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST.ordinal()] = 3;
         } catch (NoSuchFieldError var8) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST.ordinal()] = 4;
         } catch (NoSuchFieldError var7) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH.ordinal()] = 5;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH.ordinal()] = 6;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST.ordinal()] = 7;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST.ordinal()] = 8;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST.ordinal()] = 9;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST.ordinal()] = 10;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
