package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Map;
import java.util.concurrent.Callable;
import net.minecraft.server.Block;
import net.minecraft.server.BlockJukeBox;
import net.minecraft.server.Blocks;
import net.minecraft.server.CrashReportSystemDetails;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntityBanner;
import net.minecraft.server.TileEntityBeacon;
import net.minecraft.server.TileEntityBrewingStand;
import net.minecraft.server.TileEntityComparator;
import net.minecraft.server.TileEntityDispenser;
import net.minecraft.server.TileEntityDropper;
import net.minecraft.server.TileEntityEnchantTable;
import net.minecraft.server.TileEntityFlowerPot;
import net.minecraft.server.TileEntityFurnace;
import net.minecraft.server.TileEntityHopper;
import net.minecraft.server.TileEntityPiston;
import net.minecraft.server.World;
import net.minecraft.server.class_apw;
import net.minecraft.server.class_apx;
import net.minecraft.server.class_apz;
import net.minecraft.server.class_aqd;
import net.minecraft.server.class_aqj;
import net.minecraft.server.class_aqk;
import net.minecraft.server.class_aqm;
import net.minecraft.server.class_aqn;
import net.minecraft.server.class_aqo;
import net.minecraft.server.class_aqp;
import net.minecraft.server.class_aqq;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class TileEntity {
   private static final Logger a = LogManager.getLogger();
   private static Map f = Maps.newHashMap();
   private static Map g = Maps.newHashMap();
   protected World b;
   protected BlockPosition c = BlockPosition.a;
   protected boolean d;
   private int h = -1;
   protected Block e;

   private static void a(Class var0, String var1) {
      if(f.containsKey(var1)) {
         throw new IllegalArgumentException("Duplicate id: " + var1);
      } else {
         f.put(var1, var0);
         g.put(var0, var1);
      }
   }

   public World D() {
      return this.b;
   }

   public void a(World var1) {
      this.b = var1;
   }

   public boolean t() {
      return this.b != null;
   }

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      this.c = new BlockPosition(var2.h("x"), var2.h("y"), var2.h("z"));
   }

   public void a(NBTTagCompound var1) {
      String var2 = (String)g.get(this.getClass());
      if(var2 == null) {
         throw new RuntimeException(this.getClass() + " is missing a mapping! This is a bug!");
      } else {
         var1.a("id", var2);
         var1.a("x", this.c.p());
         var1.a("y", this.c.q());
         var1.a("z", this.c.r());
      }
   }

   public static TileEntity b(MinecraftServer var0, NBTTagCompound var1) {
      TileEntity var2 = null;
      String var3 = var1.l("id");

      try {
         Class var4 = (Class)f.get(var3);
         if(var4 != null) {
            var2 = (TileEntity)var4.newInstance();
         }
      } catch (Throwable var6) {
         a.error("Failed to create block entity " + var3, var6);
      }

      if(var2 != null) {
         try {
            var2.a(var0, var1);
         } catch (Throwable var5) {
            a.error("Failed to load data for block entity " + var3, var5);
            var2 = null;
         }
      } else {
         a.warn("Skipping BlockEntity with id " + var3);
      }

      return var2;
   }

   public int u() {
      if(this.h == -1) {
         IBlockData var1 = this.b.getType(this.c);
         this.h = var1.getBlock().e(var1);
      }

      return this.h;
   }

   public void v_() {
      if(this.b != null) {
         IBlockData var1 = this.b.getType(this.c);
         this.h = var1.getBlock().e(var1);
         this.b.b(this.c, this);
         if(this.w() != Blocks.AIR) {
            this.b.f(this.c, this.w());
         }
      }

   }

   public BlockPosition v() {
      return this.c;
   }

   public Block w() {
      if(this.e == null && this.b != null) {
         this.e = this.b.getType(this.c).getBlock();
      }

      return this.e;
   }

   public Packet D_() {
      return null;
   }

   public boolean x() {
      return this.d;
   }

   public void y() {
      this.d = true;
   }

   public void z() {
      this.d = false;
   }

   public boolean c(int var1, int var2) {
      return false;
   }

   public void A() {
      this.e = null;
      this.h = -1;
   }

   public void a(CrashReportSystemDetails var1) {
      var1.a("Name", new Callable() {
         public String a() throws Exception {
            return (String)TileEntity.g.get(TileEntity.this.getClass()) + " // " + TileEntity.this.getClass().getCanonicalName();
         }

         // $FF: synthetic method
         public Object call() throws Exception {
            return this.a();
         }
      });
      if(this.b != null) {
         CrashReportSystemDetails.a(var1, this.c, this.w(), this.u());
         var1.a("Actual block type", new Callable() {
            public String a() throws Exception {
               int var1 = Block.a(TileEntity.this.b.getType(TileEntity.this.c).getBlock());

               try {
                  return String.format("ID #%d (%s // %s)", new Object[]{Integer.valueOf(var1), Block.b(var1).a(), Block.b(var1).getClass().getCanonicalName()});
               } catch (Throwable var3) {
                  return "ID #" + var1;
               }
            }

            // $FF: synthetic method
            public Object call() throws Exception {
               return this.a();
            }
         });
         var1.a("Actual block data value", new Callable() {
            public String a() throws Exception {
               IBlockData var1 = TileEntity.this.b.getType(TileEntity.this.c);
               int var2 = var1.getBlock().e(var1);
               if(var2 < 0) {
                  return "Unknown? (Got " + var2 + ")";
               } else {
                  String var3 = String.format("%4s", new Object[]{Integer.toBinaryString(var2)}).replace(" ", "0");
                  return String.format("%1$d / 0x%1$X / 0b%2$s", new Object[]{Integer.valueOf(var2), var3});
               }
            }

            // $FF: synthetic method
            public Object call() throws Exception {
               return this.a();
            }
         });
      }
   }

   public void a(BlockPosition var1) {
      if(var1 instanceof BlockPosition.class_a_in_class_cj || var1 instanceof BlockPosition.class_b_in_class_cj) {
         a.warn((String)"Tried to assign a mutable BlockPos to a block entity...", (Throwable)(new Error(var1.getClass().toString())));
         var1 = new BlockPosition(var1);
      }

      this.c = var1;
   }

   public boolean B() {
      return false;
   }

   static {
      a(TileEntityFurnace.class, "Furnace");
      a(class_apw.class, "Chest");
      a(class_aqd.class, "EnderChest");
      a(BlockJukeBox.TileEntityRecordPlayer.class, "RecordPlayer");
      a(TileEntityDispenser.class, "Trap");
      a(TileEntityDropper.class, "Dropper");
      a(class_aqm.class, "Sign");
      a(class_aqj.class, "MobSpawner");
      a(class_aqk.class, "Music");
      a(TileEntityPiston.class, "Piston");
      a(TileEntityBrewingStand.class, "Cauldron");
      a(TileEntityEnchantTable.class, "EnchantTable");
      a(class_aqq.class, "Airportal");
      a(TileEntityBeacon.class, "Beacon");
      a(class_aqn.class, "Skull");
      a(class_apz.class, "DLDetector");
      a(TileEntityHopper.class, "Hopper");
      a(TileEntityComparator.class, "Comparator");
      a(TileEntityFlowerPot.class, "FlowerPot");
      a(TileEntityBanner.class, "Banner");
      a(class_aqo.class, "Structure");
      a(class_aqp.class, "EndGateway");
      a(class_apx.class, "Control");
   }
}
