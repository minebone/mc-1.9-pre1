package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;

public class class_akg extends Block {
   public class_akg() {
      super(Material.B);
      this.a(CreativeModeTab.b);
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Items.aP;
   }

   public int a(Random var1) {
      return 4;
   }
}
