package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.List;
import net.minecraft.server.Block;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_aoa;
import net.minecraft.server.class_aqk;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;

public class class_anc extends class_ajm {
   private static final List a = Lists.newArrayList((Object[])(new class_nf[]{class_ng.dG, class_ng.dE, class_ng.dI, class_ng.dH, class_ng.dF}));

   public class_anc() {
      super(Material.d);
      this.a(CreativeModeTab.d);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      boolean var5 = var1.y(var2);
      TileEntity var6 = var1.r(var2);
      if(var6 instanceof class_aqk) {
         class_aqk var7 = (class_aqk)var6;
         if(var7.f != var5) {
            if(var5) {
               var7.a(var1, var2);
            }

            var7.f = var5;
         }
      }

   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(var1.E) {
         return true;
      } else {
         TileEntity var11 = var1.r(var2);
         if(var11 instanceof class_aqk) {
            class_aqk var12 = (class_aqk)var11;
            var12.b();
            var12.a(var1, var2);
            var4.b(StatisticList.U);
         }

         return true;
      }
   }

   public void a(World var1, BlockPosition var2, EntityHuman var3) {
      if(!var1.E) {
         TileEntity var4 = var1.r(var2);
         if(var4 instanceof class_aqk) {
            ((class_aqk)var4).a(var1, var2);
            var3.b(StatisticList.T);
         }

      }
   }

   public TileEntity a(World var1, int var2) {
      return new class_aqk();
   }

   private class_nf e(int var1) {
      if(var1 < 0 || var1 >= a.size()) {
         var1 = 0;
      }

      return (class_nf)a.get(var1);
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, int var4, int var5) {
      float var6 = (float)Math.pow(2.0D, (double)(var5 - 12) / 12.0D);
      var1.a((EntityHuman)null, var2, this.e(var4), EnumSoundCategory.BLOCKS, 3.0F, var6);
      var1.a(EnumParticle.NOTE, (double)var2.p() + 0.5D, (double)var2.q() + 1.2D, (double)var2.r() + 0.5D, (double)var5 / 24.0D, 0.0D, 0.0D, new int[0]);
      return true;
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.MODEL;
   }
}
