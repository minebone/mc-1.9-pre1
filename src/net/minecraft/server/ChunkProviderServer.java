package net.minecraft.server;

import com.google.common.collect.Lists;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import net.minecraft.server.Chunk;
import net.minecraft.server.CrashReport;
import net.minecraft.server.CrashReportSystemDetails;
import net.minecraft.server.EnumCreatureType;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_ahm;
import net.minecraft.server.class_aht;
import net.minecraft.server.class_arx;
import net.minecraft.server.class_ary;
import net.minecraft.server.class_asl;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_e;
import net.minecraft.server.class_ol;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ChunkProviderServer implements class_ary {
   private static final Logger a = LogManager.getLogger();
   private final Set b = Collections.newSetFromMap(new ConcurrentHashMap());
   private final class_arx c;
   private final class_asl d;
   private final class_ol e = new class_ol();
   private final List f = Lists.newArrayList();
   private final WorldServer g;

   public ChunkProviderServer(WorldServer var1, class_asl var2, class_arx var3) {
      this.g = var1;
      this.d = var2;
      this.c = var3;
   }

   public List a() {
      return this.f;
   }

   public void a(int var1, int var2) {
      if(this.g.s.c(var1, var2)) {
         this.b.add(Long.valueOf(class_ahm.a(var1, var2)));
      }

   }

   public void b() {
      Iterator var1 = this.f.iterator();

      while(var1.hasNext()) {
         Chunk var2 = (Chunk)var1.next();
         this.a(var2.posX, var2.posZ);
      }

   }

   public Chunk b(int var1, int var2) {
      long var3 = class_ahm.a(var1, var2);
      Chunk var5 = (Chunk)this.e.a(var3);
      this.b.remove(Long.valueOf(var3));
      return var5;
   }

   public Chunk c(int var1, int var2) {
      Chunk var3 = this.b(var1, var2);
      if(var3 == null) {
         var3 = this.f(var1, var2);
         if(var3 != null) {
            this.e.a(class_ahm.a(var1, var2), var3);
            this.f.add(var3);
            var3.c();
            var3.a((class_ary)this, (class_arx)this.c);
         }
      }

      return var3;
   }

   public Chunk d(int var1, int var2) {
      Chunk var3 = this.c(var1, var2);
      if(var3 == null) {
         long var4 = class_ahm.a(var1, var2);
         var3 = this.f(var1, var2);
         if(var3 == null) {
            try {
               var3 = this.c.a(var1, var2);
            } catch (Throwable var9) {
               CrashReport var7 = CrashReport.a(var9, "Exception generating new chunk");
               CrashReportSystemDetails var8 = var7.a("Chunk to be generated");
               var8.a((String)"Location", (Object)String.format("%d,%d", new Object[]{Integer.valueOf(var1), Integer.valueOf(var2)}));
               var8.a((String)"Position hash", (Object)Long.valueOf(var4));
               var8.a((String)"Generator", (Object)this.c);
               throw new class_e(var7);
            }
         }

         this.e.a(var4, var3);
         this.f.add(var3);
         var3.c();
         var3.a((class_ary)this, (class_arx)this.c);
      }

      return var3;
   }

   private Chunk f(int var1, int var2) {
      try {
         Chunk var3 = this.d.a(this.g, var1, var2);
         if(var3 != null) {
            var3.b(this.g.P());
            this.c.b(var3, var1, var2);
         }

         return var3;
      } catch (Exception var4) {
         a.error((String)"Couldn\'t load chunk", (Throwable)var4);
         return null;
      }
   }

   private void a(Chunk var1) {
      try {
         this.d.b(this.g, var1);
      } catch (Exception var3) {
         a.error((String)"Couldn\'t save entities", (Throwable)var3);
      }

   }

   private void b(Chunk var1) {
      try {
         var1.b(this.g.P());
         this.d.a(this.g, var1);
      } catch (IOException var3) {
         a.error((String)"Couldn\'t save chunk", (Throwable)var3);
      } catch (class_aht var4) {
         a.error((String)"Couldn\'t save chunk; already in use by another instance of Minecraft?", (Throwable)var4);
      }

   }

   public boolean a(boolean var1) {
      int var2 = 0;
      ArrayList var3 = Lists.newArrayList((Iterable)this.f);

      for(int var4 = 0; var4 < var3.size(); ++var4) {
         Chunk var5 = (Chunk)var3.get(var4);
         if(var1) {
            this.a(var5);
         }

         if(var5.a(var1)) {
            this.b(var5);
            var5.f(false);
            ++var2;
            if(var2 == 24 && !var1) {
               return false;
            }
         }
      }

      return true;
   }

   public void c() {
      this.d.b();
   }

   public boolean d() {
      if(!this.g.b) {
         for(int var1 = 0; var1 < 100; ++var1) {
            if(!this.b.isEmpty()) {
               Long var2 = (Long)this.b.iterator().next();
               Chunk var3 = (Chunk)this.e.a(var2.longValue());
               if(var3 != null) {
                  var3.d();
                  this.b(var3);
                  this.a(var3);
                  this.e.d(var2.longValue());
                  this.f.remove(var3);
               }

               this.b.remove(var2);
            }
         }

         this.d.a();
      }

      return false;
   }

   public boolean e() {
      return !this.g.b;
   }

   public String f() {
      return "ServerChunkCache: " + this.e.a() + " Drop: " + this.b.size();
   }

   public List a(EnumCreatureType var1, BlockPosition var2) {
      return this.c.a(var1, var2);
   }

   public BlockPosition a(World var1, String var2, BlockPosition var3) {
      return this.c.a(var1, var2, var3);
   }

   public int g() {
      return this.e.a();
   }

   public boolean e(int var1, int var2) {
      return this.e.b(class_ahm.a(var1, var2));
   }
}
