package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ash;
import net.minecraft.server.class_asi;
import net.minecraft.server.class_oa;

public class class_asc implements class_ash {
   private final class_oa a;
   private final class_asi b;
   private final int c;

   public class_asc(int var1, class_asi var2) {
      this.c = var1;
      this.b = var2;
      this.a = new class_oa(1 << var1);
   }

   public int a(IBlockData var1) {
      int var2 = this.a.a(var1);
      if(var2 == -1) {
         var2 = this.a.c(var1);
         if(var2 >= 1 << this.c) {
            var2 = this.b.a(this.c + 1, var1);
         }
      }

      return var2;
   }

   public IBlockData a(int var1) {
      return (IBlockData)this.a.a(var1);
   }

   public void b(PacketDataSerializer var1) {
      int var2 = this.a.b();
      var1.writeVarInt(var2);

      for(int var3 = 0; var3 < var2; ++var3) {
         var1.writeVarInt(Block.i.a(this.a.a(var3)));
      }

   }

   public int a() {
      int var1 = PacketDataSerializer.a(this.a.b());

      for(int var2 = 0; var2 < this.a.b(); ++var2) {
         var1 += PacketDataSerializer.a(Block.i.a(this.a.a(var2)));
      }

      return var1;
   }
}
