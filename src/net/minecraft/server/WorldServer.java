package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.util.concurrent.ListenableFuture;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.Block;
import net.minecraft.server.BlockActionData;
import net.minecraft.server.Blocks;
import net.minecraft.server.Chunk;
import net.minecraft.server.ChunkProviderServer;
import net.minecraft.server.CrashReport;
import net.minecraft.server.CrashReportSystemDetails;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityAnimal;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityTracker;
import net.minecraft.server.EntityTypes;
import net.minecraft.server.EnumCreatureType;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.PacketPlayOutGameStateChange;
import net.minecraft.server.PersistentCollection;
import net.minecraft.server.PersistentScoreboard;
import net.minecraft.server.PersistentVillage;
import net.minecraft.server.StructureBoundingBox;
import net.minecraft.server.TileEntity;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.WorldData;
import net.minecraft.server.WorldProviderNormal;
import net.minecraft.server.WorldSettings;
import net.minecraft.server.WorldType;
import net.minecraft.server.class_ahm;
import net.minecraft.server.class_aho;
import net.minecraft.server.class_aht;
import net.minecraft.server.class_ahz;
import net.minecraft.server.class_aia;
import net.minecraft.server.class_aid;
import net.minecraft.server.class_aij;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ary;
import net.minecraft.server.ChunkSection;
import net.minecraft.server.class_asl;
import net.minecraft.server.class_ats;
import net.minecraft.server.class_awl;
import net.minecraft.server.Material;
import net.minecraft.server.class_ayv;
import net.minecraft.server.class_ayw;
import net.minecraft.server.class_azh;
import net.minecraft.server.class_baa;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_e;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutEntityWeather;
import net.minecraft.server.PacketPlayOutBlockAction;
import net.minecraft.server.PacketPlayOutEntityStatus;
import net.minecraft.server.PacketPlayOutExplosion;
import net.minecraft.server.PacketPlayOutWorldParticles;
import net.minecraft.server.class_kw;
import net.minecraft.server.class_ll;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.class_lv;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_oo;
import net.minecraft.server.class_op;
import net.minecraft.server.class_ov;
import net.minecraft.server.class_qb;
import net.minecraft.server.class_qk;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_vp;
import net.minecraft.server.class_wh;
import net.minecraft.server.class_wj;
import net.minecraft.server.class_wl;
import net.minecraft.server.class_xz;
import net.minecraft.server.class_zc;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WorldServer extends World implements class_qb {
   private static final Logger a = LogManager.getLogger();
   private final MinecraftServer I;
   private final EntityTracker J;
   private final class_lv K;
   private final Set L = Sets.newHashSet();
   private final TreeSet M = new TreeSet();
   private final Map N = Maps.newHashMap();
   public boolean b;
   private boolean O;
   private int P;
   private final class_aia Q;
   private final class_ahz R = new class_ahz();
   protected final class_vp c = new class_vp(this);
   private WorldServer.class_a_in_class_lp[] S = new WorldServer.class_a_in_class_lp[]{new WorldServer.class_a_in_class_lp(null), new WorldServer.class_a_in_class_lp(null)};
   private int T;
   private List U = Lists.newArrayList();

   public WorldServer(MinecraftServer var1, class_azh var2, WorldData var3, int var4, class_oo var5) {
      super(var2, var3, WorldProviderNormal.a(var4).d(), var5, false);
      this.I = var1;
      this.J = new EntityTracker(this);
      this.K = new class_lv(this);
      this.s.a((World)this);
      this.v = this.n();
      this.Q = new class_aia(this);
      this.H();
      this.I();
      this.aj().a(var1.aD());
   }

   public World b() {
      this.z = new PersistentCollection(this.w);
      String var1 = PersistentVillage.a(this.s);
      PersistentVillage var2 = (PersistentVillage)this.z.a(PersistentVillage.class, var1);
      if(var2 == null) {
         this.A = new PersistentVillage(this);
         this.z.a((String)var1, (class_ayw)this.A);
      } else {
         this.A = var2;
         this.A.a((World)this);
      }

      this.D = new class_kw(this.I);
      PersistentScoreboard var3 = (PersistentScoreboard)this.z.a(PersistentScoreboard.class, "scoreboard");
      if(var3 == null) {
         var3 = new PersistentScoreboard();
         this.z.a((String)"scoreboard", (class_ayw)var3);
      }

      var3.a(this.D);
      ((class_kw)this.D).a((Runnable)(new class_ayv(var3)));
      this.B = new class_baa(new File(new File(this.w.b(), "data"), "loot_tables"));
      this.aj().c(this.x.B(), this.x.C());
      this.aj().c(this.x.H());
      this.aj().b(this.x.G());
      this.aj().c(this.x.I());
      this.aj().b(this.x.J());
      if(this.x.E() > 0L) {
         this.aj().a(this.x.D(), this.x.F(), this.x.E());
      } else {
         this.aj().a(this.x.D());
      }

      return this;
   }

   public void d() {
      super.d();
      if(this.T().s() && this.ae() != EnumDifficulty.HARD) {
         this.T().a(EnumDifficulty.HARD);
      }

      this.s.k().b();
      if(this.g()) {
         if(this.U().b("doDaylightCycle")) {
            long var1 = this.x.f() + 24000L;
            this.x.c(var1 - var1 % 24000L);
         }

         this.f();
      }

      this.C.a("mobSpawner");
      if(this.U().b("doMobSpawning") && this.x.t() != WorldType.g) {
         this.R.a(this, this.F, this.G, this.x.e() % 400L == 0L);
      }

      this.C.c("chunkSource");
      this.v.d();
      int var3 = this.a(1.0F);
      if(var3 != this.af()) {
         this.c(var3);
      }

      this.x.b(this.x.e() + 1L);
      if(this.U().b("doDaylightCycle")) {
         this.x.c(this.x.f() + 1L);
      }

      this.C.c("tickPending");
      this.a(false);
      this.C.c("tickBlocks");
      this.j();
      this.C.c("chunkMap");
      this.K.c();
      this.C.c("village");
      this.A.a();
      this.c.a();
      this.C.c("portalForcer");
      this.Q.a(this.P());
      this.C.b();
      this.ao();
   }

   public BiomeBase.BiomeMeta a(EnumCreatureType var1, BlockPosition var2) {
      List var3 = this.r().a(var1, var2);
      return var3 != null && !var3.isEmpty()?(BiomeBase.BiomeMeta)class_ov.a(this.r, var3):null;
   }

   public boolean a(EnumCreatureType var1, BiomeBase.BiomeMeta var2, BlockPosition var3) {
      List var4 = this.r().a(var1, var3);
      return var4 != null && !var4.isEmpty()?var4.contains(var2):false;
   }

   public void e() {
      this.O = false;
      if(!this.i.isEmpty()) {
         int var1 = 0;
         int var2 = 0;
         Iterator var3 = this.i.iterator();

         while(var3.hasNext()) {
            EntityHuman var4 = (EntityHuman)var3.next();
            if(var4.y()) {
               ++var1;
            } else if(var4.ck()) {
               ++var2;
            }
         }

         this.O = var2 > 0 && var2 >= this.i.size() - var1;
      }

   }

   protected void f() {
      this.O = false;
      Iterator var1 = this.i.iterator();

      while(var1.hasNext()) {
         EntityHuman var2 = (EntityHuman)var1.next();
         if(var2.ck()) {
            var2.a(false, false, true);
         }
      }

      this.c();
   }

   private void c() {
      this.x.g(0);
      this.x.b(false);
      this.x.f(0);
      this.x.a(false);
   }

   public boolean g() {
      if(this.O && !this.E) {
         Iterator var1 = this.i.iterator();

         EntityHuman var2;
         do {
            if(!var1.hasNext()) {
               return true;
            }

            var2 = (EntityHuman)var1.next();
         } while(var2.y() || var2.cL());

         return false;
      } else {
         return false;
      }
   }

   protected boolean a(int var1, int var2, boolean var3) {
      return this.r().e(var1, var2);
   }

   protected void i() {
      this.C.a("playerCheckLight");
      if(!this.i.isEmpty()) {
         int var1 = this.r.nextInt(this.i.size());
         EntityHuman var2 = (EntityHuman)this.i.get(var1);
         int var3 = MathHelper.c(var2.locX) + this.r.nextInt(11) - 5;
         int var4 = MathHelper.c(var2.locY) + this.r.nextInt(11) - 5;
         int var5 = MathHelper.c(var2.locZ) + this.r.nextInt(11) - 5;
         this.w(new BlockPosition(var3, var4, var5));
      }

      this.C.b();
   }

   protected void j() {
      this.i();
      if(this.x.t() == WorldType.g) {
         Iterator var19 = this.K.b();

         while(var19.hasNext()) {
            ((Chunk)var19.next()).b(false);
         }

      } else {
         int var1 = this.U().c("randomTickSpeed");
         boolean var2 = this.W();
         boolean var3 = this.V();
         this.C.a("pollingChunks");

         for(Iterator var4 = this.K.b(); var4.hasNext(); this.C.b()) {
            this.C.a("getChunk");
            Chunk var5 = (Chunk)var4.next();
            int var6 = var5.posX * 16;
            int var7 = var5.posZ * 16;
            this.C.c("checkNextLight");
            var5.n();
            this.C.c("tickChunk");
            var5.b(false);
            this.C.c("thunder");
            int var8;
            BlockPosition var9;
            if(var2 && var3 && this.r.nextInt(100000) == 0) {
               this.l = this.l * 3 + 1013904223;
               var8 = this.l >> 2;
               var9 = this.a(new BlockPosition(var6 + (var8 & 15), 0, var7 + (var8 >> 8 & 15)));
               if(this.B(var9)) {
                  class_qk var10 = this.D(var9);
                  if(this.r.nextDouble() < (double)var10.b() * 0.05D) {
                     class_wj var11 = new class_wj(this);
                     var11.a(class_wl.SKELETON);
                     var11.x(true);
                     var11.b_(0);
                     var11.b((double)var9.p(), (double)var9.q(), (double)var9.r());
                     this.a((Entity)var11);
                     this.d(new class_xz(this, (double)var9.p(), (double)var9.q(), (double)var9.r(), true));
                  } else {
                     this.d(new class_xz(this, (double)var9.p(), (double)var9.q(), (double)var9.r(), false));
                  }
               }
            }

            this.C.c("iceandsnow");
            if(this.r.nextInt(16) == 0) {
               this.l = this.l * 3 + 1013904223;
               var8 = this.l >> 2;
               var9 = this.p(new BlockPosition(var6 + (var8 & 15), 0, var7 + (var8 >> 8 & 15)));
               BlockPosition var22 = var9.b();
               if(this.v(var22)) {
                  this.a((BlockPosition)var22, (IBlockData)Blocks.aI.u());
               }

               if(var2 && this.f(var9, true)) {
                  this.a((BlockPosition)var9, (IBlockData)Blocks.aH.u());
               }

               if(var2 && this.b((BlockPosition)var22).d()) {
                  this.getType(var22).getBlock().h(this, var22);
               }
            }

            this.C.c("tickBlocks");
            if(var1 > 0) {
               ChunkSection[] var20 = var5.getChunkSections();
               int var21 = var20.length;

               for(int var23 = 0; var23 < var21; ++var23) {
                  ChunkSection var24 = var20[var23];
                  if(var24 != Chunk.a && var24.shouldTick()) {
                     for(int var12 = 0; var12 < var1; ++var12) {
                        this.l = this.l * 3 + 1013904223;
                        int var13 = this.l >> 2;
                        int var14 = var13 & 15;
                        int var15 = var13 >> 8 & 15;
                        int var16 = var13 >> 16 & 15;
                        IBlockData var17 = var24.a(var14, var16, var15);
                        Block var18 = var17.getBlock();
                        this.C.a("randomTick");
                        if(var18.l()) {
                           var18.a((World)this, (BlockPosition)(new BlockPosition(var14 + var6, var16 + var24.getYPosition(), var15 + var7)), (IBlockData)var17, (Random)this.r);
                        }

                        this.C.b();
                     }
                  }
               }
            }
         }

         this.C.b();
      }
   }

   protected BlockPosition a(BlockPosition var1) {
      BlockPosition var2 = this.p(var1);
      AxisAlignedBB var3 = (new AxisAlignedBB(var2, new BlockPosition(var2.p(), this.Y(), var2.r()))).g(3.0D);
      List var4 = this.a(class_rz.class, var3, new Predicate() {
         public boolean a(class_rz var1) {
            return var1 != null && var1.at() && WorldServer.this.h(var1.c());
         }

         // $FF: synthetic method
         public boolean apply(Object var1) {
            return this.a((class_rz)var1);
         }
      });
      if(!var4.isEmpty()) {
         return ((class_rz)var4.get(this.r.nextInt(var4.size()))).c();
      } else {
         if(var2.q() == -1) {
            var2 = var2.b(2);
         }

         return var2;
      }
   }

   public boolean a(BlockPosition var1, Block var2) {
      class_aid var3 = new class_aid(var1, var2);
      return this.U.contains(var3);
   }

   public boolean b(BlockPosition var1, Block var2) {
      class_aid var3 = new class_aid(var1, var2);
      return this.L.contains(var3);
   }

   public void a(BlockPosition var1, Block var2, int var3) {
      this.a(var1, var2, var3, 0);
   }

   public void a(BlockPosition var1, Block var2, int var3, int var4) {
      if(var1 instanceof BlockPosition.class_a_in_class_cj || var1 instanceof BlockPosition.class_b_in_class_cj) {
         var1 = new BlockPosition(var1);
         LogManager.getLogger().warn((String)"Tried to assign a mutable BlockPos to tick data...", (Throwable)(new Error(var1.getClass().toString())));
      }

      byte var5 = 0;
      Material var6 = var2.u().getMaterial();
      if(this.d && var6 != Material.a) {
         if(var2.s()) {
            var5 = 8;
            if(this.a((BlockPosition)var1.a(-var5, -var5, -var5), (BlockPosition)var1.a(var5, var5, var5))) {
               IBlockData var8 = this.getType(var1);
               if(var8.getMaterial() != Material.a && var8.getBlock() == var2) {
                  var8.getBlock().b((World)this, (BlockPosition)var1, (IBlockData)var8, (Random)this.r);
               }
            }

            return;
         }

         var3 = 1;
      }

      class_aid var7 = new class_aid(var1, var2);
      if(this.a((BlockPosition)var1.a(-var5, -var5, -var5), (BlockPosition)var1.a(var5, var5, var5))) {
         if(var6 != Material.a) {
            var7.a((long)var3 + this.x.e());
            var7.a(var4);
         }

         if(!this.L.contains(var7)) {
            this.L.add(var7);
            this.M.add(var7);
         }
      }

   }

   public void b(BlockPosition var1, Block var2, int var3, int var4) {
      if(var1 instanceof BlockPosition.class_a_in_class_cj || var1 instanceof BlockPosition.class_b_in_class_cj) {
         var1 = new BlockPosition(var1);
         LogManager.getLogger().warn((String)"Tried to assign a mutable BlockPos to tick data...", (Throwable)(new Error(var1.getClass().toString())));
      }

      class_aid var5 = new class_aid(var1, var2);
      var5.a(var4);
      Material var6 = var2.u().getMaterial();
      if(var6 != Material.a) {
         var5.a((long)var3 + this.x.e());
      }

      if(!this.L.contains(var5)) {
         this.L.add(var5);
         this.M.add(var5);
      }

   }

   public void k() {
      if(this.i.isEmpty()) {
         if(this.P++ >= 300) {
            return;
         }
      } else {
         this.m();
      }

      this.s.r();
      super.k();
   }

   protected void l() {
      super.l();
      this.C.c("players");

      for(int var1 = 0; var1 < this.i.size(); ++var1) {
         Entity var2 = (Entity)this.i.get(var1);
         Entity var3 = var2.bx();
         if(var3 != null) {
            if(!var3.dead && var3.w(var2)) {
               continue;
            }

            var2.p();
         }

         this.C.a("tick");
         if(!var2.dead) {
            try {
               this.g(var2);
            } catch (Throwable var7) {
               CrashReport var5 = CrashReport.a(var7, "Ticking player");
               CrashReportSystemDetails var6 = var5.a("Player being ticked");
               var2.a(var6);
               throw new class_e(var5);
            }
         }

         this.C.b();
         this.C.a("remove");
         if(var2.dead) {
            int var4 = var2.ab;
            int var8 = var2.ad;
            if(var2.aa && this.a(var4, var8, true)) {
               this.a(var4, var8).b(var2);
            }

            this.e.remove(var2);
            this.c(var2);
         }

         this.C.b();
      }

   }

   public void m() {
      this.P = 0;
   }

   public boolean a(boolean var1) {
      if(this.x.t() == WorldType.g) {
         return false;
      } else {
         int var2 = this.M.size();
         if(var2 != this.L.size()) {
            throw new IllegalStateException("TickNextTick list out of synch");
         } else {
            if(var2 > 1000) {
               var2 = 1000;
            }

            this.C.a("cleaning");

            class_aid var4;
            for(int var3 = 0; var3 < var2; ++var3) {
               var4 = (class_aid)this.M.first();
               if(!var1 && var4.b > this.x.e()) {
                  break;
               }

               this.M.remove(var4);
               this.L.remove(var4);
               this.U.add(var4);
            }

            this.C.b();
            this.C.a("ticking");
            Iterator var11 = this.U.iterator();

            while(var11.hasNext()) {
               var4 = (class_aid)var11.next();
               var11.remove();
               byte var5 = 0;
               if(this.a((BlockPosition)var4.a.a(-var5, -var5, -var5), (BlockPosition)var4.a.a(var5, var5, var5))) {
                  IBlockData var6 = this.getType(var4.a);
                  if(var6.getMaterial() != Material.a && Block.a(var6.getBlock(), var4.a())) {
                     try {
                        var6.getBlock().b((World)this, (BlockPosition)var4.a, (IBlockData)var6, (Random)this.r);
                     } catch (Throwable var10) {
                        CrashReport var8 = CrashReport.a(var10, "Exception while ticking a block");
                        CrashReportSystemDetails var9 = var8.a("Block being ticked");
                        CrashReportSystemDetails.a(var9, var4.a, var6);
                        throw new class_e(var8);
                     }
                  }
               } else {
                  this.a(var4.a, var4.a(), 0);
               }
            }

            this.C.b();
            this.U.clear();
            return !this.M.isEmpty();
         }
      }
   }

   public List a(Chunk var1, boolean var2) {
      class_ahm var3 = var1.k();
      int var4 = (var3.a << 4) - 2;
      int var5 = var4 + 16 + 2;
      int var6 = (var3.b << 4) - 2;
      int var7 = var6 + 16 + 2;
      return this.a(new StructureBoundingBox(var4, 0, var6, var5, 256, var7), var2);
   }

   public List a(StructureBoundingBox var1, boolean var2) {
      ArrayList var3 = null;

      for(int var4 = 0; var4 < 2; ++var4) {
         Iterator var5;
         if(var4 == 0) {
            var5 = this.M.iterator();
         } else {
            var5 = this.U.iterator();
         }

         while(var5.hasNext()) {
            class_aid var6 = (class_aid)var5.next();
            BlockPosition var7 = var6.a;
            if(var7.p() >= var1.a && var7.p() < var1.d && var7.r() >= var1.c && var7.r() < var1.f) {
               if(var2) {
                  if(var4 == 0) {
                     this.L.remove(var6);
                  }

                  var5.remove();
               }

               if(var3 == null) {
                  var3 = Lists.newArrayList();
               }

               var3.add(var6);
            }
         }
      }

      return var3;
   }

   public void a(Entity var1, boolean var2) {
      if(!this.am() && (var1 instanceof EntityAnimal || var1 instanceof class_wh)) {
         var1.S();
      }

      if(!this.al() && var1 instanceof class_zc) {
         var1.S();
      }

      super.a(var1, var2);
   }

   private boolean al() {
      return this.I.ad();
   }

   private boolean am() {
      return this.I.ac();
   }

   protected class_ary n() {
      class_asl var1 = this.w.a(this.s);
      return new ChunkProviderServer(this, var1, this.s.c());
   }

   public List a(int var1, int var2, int var3, int var4, int var5, int var6) {
      ArrayList var7 = Lists.newArrayList();

      for(int var8 = 0; var8 < this.g.size(); ++var8) {
         TileEntity var9 = (TileEntity)this.g.get(var8);
         BlockPosition var10 = var9.v();
         if(var10.p() >= var1 && var10.q() >= var2 && var10.r() >= var3 && var10.p() < var4 && var10.q() < var5 && var10.r() < var6) {
            var7.add(var9);
         }
      }

      return var7;
   }

   public boolean a(EntityHuman var1, BlockPosition var2) {
      return !this.I.a(this, var2, var1) && this.aj().a(var2);
   }

   public void a(WorldSettings var1) {
      if(!this.x.v()) {
         try {
            this.b(var1);
            if(this.x.t() == WorldType.g) {
               this.an();
            }

            super.a(var1);
         } catch (Throwable var6) {
            CrashReport var3 = CrashReport.a(var6, "Exception initializing level");

            try {
               this.a((CrashReport)var3);
            } catch (Throwable var5) {
               ;
            }

            throw new class_e(var3);
         }

         this.x.d(true);
      }

   }

   private void an() {
      this.x.f(false);
      this.x.c(true);
      this.x.b(false);
      this.x.a(false);
      this.x.i(1000000000);
      this.x.c(6000L);
      this.x.a(WorldSettings.EnumGamemode.SPECTATOR);
      this.x.g(false);
      this.x.a(EnumDifficulty.PEACEFUL);
      this.x.e(true);
      this.U().a("doDaylightCycle", "false");
   }

   private void b(WorldSettings var1) {
      if(!this.s.e()) {
         this.x.a(BlockPosition.a.b(this.s.i()));
      } else if(this.x.t() == WorldType.g) {
         this.x.a(BlockPosition.a.a());
      } else {
         this.y = true;
         class_aij var2 = this.s.k();
         List var3 = var2.a();
         Random var4 = new Random(this.O());
         BlockPosition var5 = var2.a(0, 0, 256, var3, var4);
         int var6 = 8;
         int var7 = this.s.i();
         int var8 = 8;
         if(var5 != null) {
            var6 = var5.p();
            var8 = var5.r();
         } else {
            a.warn("Unable to find spawn biome");
         }

         int var9 = 0;

         while(!this.s.a(var6, var8)) {
            var6 += var4.nextInt(64) - var4.nextInt(64);
            var8 += var4.nextInt(64) - var4.nextInt(64);
            ++var9;
            if(var9 == 1000) {
               break;
            }
         }

         this.x.a(new BlockPosition(var6, var7, var8));
         this.y = false;
         if(var1.c()) {
            this.o();
         }

      }
   }

   protected void o() {
      class_ats var1 = new class_ats();

      for(int var2 = 0; var2 < 10; ++var2) {
         int var3 = this.x.b() + this.r.nextInt(6) - this.r.nextInt(6);
         int var4 = this.x.d() + this.r.nextInt(6) - this.r.nextInt(6);
         BlockPosition var5 = this.q(new BlockPosition(var3, 0, var4)).a();
         if(var1.b(this, this.r, var5)) {
            break;
         }
      }

   }

   public BlockPosition p() {
      return this.s.h();
   }

   public void a(boolean var1, class_op var2) throws class_aht {
      ChunkProviderServer var3 = this.r();
      if(var3.e()) {
         if(var2 != null) {
            var2.a("Saving level");
         }

         this.a();
         if(var2 != null) {
            var2.c("Saving chunks");
         }

         var3.a(var1);
         ArrayList var4 = Lists.newArrayList((Iterable)var3.a());
         Iterator var5 = var4.iterator();

         while(var5.hasNext()) {
            Chunk var6 = (Chunk)var5.next();
            if(var6 != null && !this.K.a(var6.posX, var6.posZ)) {
               var3.a(var6.posX, var6.posZ);
            }
         }

      }
   }

   public void q() {
      ChunkProviderServer var1 = this.r();
      if(var1.e()) {
         var1.c();
      }
   }

   protected void a() throws class_aht {
      this.N();
      WorldServer[] var1 = this.I.d;
      int var2 = var1.length;

      for(int var3 = 0; var3 < var2; ++var3) {
         WorldServer var4 = var1[var3];
         if(var4 instanceof class_ll) {
            ((class_ll)var4).c();
         }
      }

      this.x.a(this.aj().h());
      this.x.d(this.aj().f());
      this.x.c(this.aj().g());
      this.x.e(this.aj().m());
      this.x.f(this.aj().n());
      this.x.j(this.aj().q());
      this.x.k(this.aj().p());
      this.x.b(this.aj().j());
      this.x.e(this.aj().i());
      this.w.a(this.x, this.I.getPlayerList().t());
      this.z.a();
   }

   public boolean a(Entity var1) {
      return this.i(var1)?super.a(var1):false;
   }

   public void a(Collection var1) {
      ArrayList var2 = Lists.newArrayList((Iterable)var1);
      Iterator var3 = var2.iterator();

      while(var3.hasNext()) {
         Entity var4 = (Entity)var3.next();
         if(this.i(var4)) {
            this.e.add(var4);
            this.b(var4);
         }
      }

   }

   private boolean i(Entity var1) {
      if(var1.dead) {
         a.warn("Tried to add entity " + EntityTypes.b(var1) + " but it was marked as removed already");
         return false;
      } else {
         UUID var2 = var1.getUniqueId();
         if(this.N.containsKey(var2)) {
            Entity var3 = (Entity)this.N.get(var2);
            if(this.f.contains(var3)) {
               this.f.remove(var3);
            } else {
               if(!(var1 instanceof EntityHuman)) {
                  a.warn("Keeping entity " + EntityTypes.b(var3) + " that already exists with UUID " + var2.toString());
                  return false;
               }

               a.warn("Force-added player with duplicate UUID " + var2.toString());
            }

            this.f(var3);
         }

         return true;
      }
   }

   protected void b(Entity var1) {
      super.b(var1);
      this.k.a(var1.getId(), var1);
      this.N.put(var1.getUniqueId(), var1);
      Entity[] var2 = var1.aQ();
      if(var2 != null) {
         for(int var3 = 0; var3 < var2.length; ++var3) {
            this.k.a(var2[var3].getId(), var2[var3]);
         }
      }

   }

   protected void c(Entity var1) {
      super.c(var1);
      this.k.d(var1.getId());
      this.N.remove(var1.getUniqueId());
      Entity[] var2 = var1.aQ();
      if(var2 != null) {
         for(int var3 = 0; var3 < var2.length; ++var3) {
            this.k.d(var2[var3].getId());
         }
      }

   }

   public boolean d(Entity var1) {
      if(super.d(var1)) {
         this.I.getPlayerList().a((EntityHuman)null, var1.locX, var1.locY, var1.locZ, 512.0D, this.s.p().a(), new PacketPlayOutEntityWeather(var1));
         return true;
      } else {
         return false;
      }
   }

   public void a(Entity var1, byte var2) {
      this.v().b(var1, new PacketPlayOutEntityStatus(var1, var2));
   }

   public ChunkProviderServer r() {
      return (ChunkProviderServer)super.z();
   }

   public class_aho a(Entity var1, double var2, double var4, double var6, float var8, boolean var9, boolean var10) {
      class_aho var11 = new class_aho(this, var1, var2, var4, var6, var8, var9, var10);
      var11.a();
      var11.a(false);
      if(!var10) {
         var11.d();
      }

      Iterator var12 = this.i.iterator();

      while(var12.hasNext()) {
         EntityHuman var13 = (EntityHuman)var12.next();
         if(var13.e(var2, var4, var6) < 4096.0D) {
            ((EntityPlayer)var13).a.a((Packet)(new PacketPlayOutExplosion(var2, var4, var6, var8, var11.e(), (Vec3D)var11.b().get(var13))));
         }
      }

      return var11;
   }

   public void c(BlockPosition var1, Block var2, int var3, int var4) {
      BlockActionData var5 = new BlockActionData(var1, var2, var3, var4);
      Iterator var6 = this.S[this.T].iterator();

      BlockActionData var7;
      do {
         if(!var6.hasNext()) {
            this.S[this.T].add(var5);
            return;
         }

         var7 = (BlockActionData)var6.next();
      } while(!var7.equals(var5));

   }

   private void ao() {
      while(!this.S[this.T].isEmpty()) {
         int var1 = this.T;
         this.T ^= 1;
         Iterator var2 = this.S[var1].iterator();

         while(var2.hasNext()) {
            BlockActionData var3 = (BlockActionData)var2.next();
            if(this.a(var3)) {
               this.I.getPlayerList().a((EntityHuman)null, (double)var3.a().p(), (double)var3.a().q(), (double)var3.a().r(), 64.0D, this.s.p().a(), new PacketPlayOutBlockAction(var3.a(), var3.d(), var3.b(), var3.c()));
            }
         }

         this.S[var1].clear();
      }

   }

   private boolean a(BlockActionData var1) {
      IBlockData var2 = this.getType(var1.a());
      return var2.getBlock() == var1.d()?var2.getBlock().a(this, var1.a(), var2, var1.b(), var1.c()):false;
   }

   public void s() {
      this.w.a();
   }

   protected void t() {
      boolean var1 = this.W();
      super.t();
      if(this.n != this.o) {
         this.I.getPlayerList().a((Packet)(new PacketPlayOutGameStateChange(7, this.o)), this.s.p().a());
      }

      if(this.p != this.q) {
         this.I.getPlayerList().a((Packet)(new PacketPlayOutGameStateChange(8, this.q)), this.s.p().a());
      }

      if(var1 != this.W()) {
         if(var1) {
            this.I.getPlayerList().sendAll((Packet)(new PacketPlayOutGameStateChange(2, 0.0F)));
         } else {
            this.I.getPlayerList().sendAll((Packet)(new PacketPlayOutGameStateChange(1, 0.0F)));
         }

         this.I.getPlayerList().sendAll((Packet)(new PacketPlayOutGameStateChange(7, this.o)));
         this.I.getPlayerList().sendAll((Packet)(new PacketPlayOutGameStateChange(8, this.q)));
      }

   }

   public MinecraftServer u() {
      return this.I;
   }

   public EntityTracker v() {
      return this.J;
   }

   public class_lv w() {
      return this.K;
   }

   public class_aia x() {
      return this.Q;
   }

   public class_awl y() {
      return this.w.h();
   }

   public void a(EnumParticle var1, double var2, double var4, double var6, int var8, double var9, double var11, double var13, double var15, int... var17) {
      this.a(var1, false, var2, var4, var6, var8, var9, var11, var13, var15, var17);
   }

   public void a(EnumParticle var1, boolean var2, double var3, double var5, double var7, int var9, double var10, double var12, double var14, double var16, int... var18) {
      PacketPlayOutWorldParticles var19 = new PacketPlayOutWorldParticles(var1, var2, (float)var3, (float)var5, (float)var7, (float)var10, (float)var12, (float)var14, (float)var16, var9, var18);

      for(int var20 = 0; var20 < this.i.size(); ++var20) {
         EntityPlayer var21 = (EntityPlayer)this.i.get(var20);
         this.a(var21, var2, var3, var5, var7, var19);
      }

   }

   public void a(EntityPlayer var1, EnumParticle var2, boolean var3, double var4, double var6, double var8, int var10, double var11, double var13, double var15, double var17, int... var19) {
      PacketPlayOutWorldParticles var20 = new PacketPlayOutWorldParticles(var2, var3, (float)var4, (float)var6, (float)var8, (float)var11, (float)var13, (float)var15, (float)var17, var10, var19);
      this.a(var1, var3, var4, var6, var8, var20);
   }

   private void a(EntityPlayer var1, boolean var2, double var3, double var5, double var7, Packet var9) {
      BlockPosition var10 = var1.c();
      double var11 = var10.e(var3, var5, var7);
      if(var11 <= 1024.0D || var2 && var11 <= 262144.0D) {
         var1.a.a(var9);
      }

   }

   public Entity a(UUID var1) {
      return (Entity)this.N.get(var1);
   }

   public ListenableFuture a(Runnable var1) {
      return this.I.a(var1);
   }

   public boolean aE() {
      return this.I.aE();
   }

   // $FF: synthetic method
   public class_ary z() {
      return this.r();
   }

   static class class_a_in_class_lp extends ArrayList {
      private class_a_in_class_lp() {
      }

      // $FF: synthetic method
      class_a_in_class_lp(Object var1) {
         this();
      }
   }
}
