package net.minecraft.server;

import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EnumColor;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.class_ajp;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_apc;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_aor extends class_apc {
   public static final BlockStateEnum a = BlockStateEnum.a("color", EnumColor.class);

   public class_aor() {
      super(Material.s, false);
      this.w(this.A.b().set(b, Boolean.valueOf(false)).set(c, Boolean.valueOf(false)).set(d, Boolean.valueOf(false)).set(e, Boolean.valueOf(false)).set(a, EnumColor.WHITE));
      this.a(CreativeModeTab.c);
   }

   public int d(IBlockData var1) {
      return ((EnumColor)var1.get(a)).a();
   }

   public MaterialMapColor r(IBlockData var1) {
      return ((EnumColor)var1.get(a)).e();
   }

   public IBlockData a(int var1) {
      return this.u().set(a, EnumColor.b(var1));
   }

   public int e(IBlockData var1) {
      return ((EnumColor)var1.get(a)).a();
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      switch(class_aor.SyntheticClass_1.a[var2.ordinal()]) {
      case 1:
         return var1.set(b, var1.get(d)).set(c, var1.get(e)).set(d, var1.get(b)).set(e, var1.get(c));
      case 2:
         return var1.set(b, var1.get(c)).set(c, var1.get(d)).set(d, var1.get(e)).set(e, var1.get(b));
      case 3:
         return var1.set(b, var1.get(e)).set(c, var1.get(b)).set(d, var1.get(c)).set(e, var1.get(d));
      default:
         return var1;
      }
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      switch(class_aor.SyntheticClass_1.b[var2.ordinal()]) {
      case 1:
         return var1.set(b, var1.get(d)).set(d, var1.get(b));
      case 2:
         return var1.set(c, var1.get(e)).set(e, var1.get(c));
      default:
         return super.a(var1, var2);
      }
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{b, c, e, d, a});
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      if(!var1.E) {
         class_ajp.c(var1, var2);
      }

   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      if(!var1.E) {
         class_ajp.c(var1, var2);
      }

   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b = new int[class_amq.values().length];

      static {
         try {
            b[class_amq.LEFT_RIGHT.ordinal()] = 1;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            b[class_amq.FRONT_BACK.ordinal()] = 2;
         } catch (NoSuchFieldError var4) {
            ;
         }

         a = new int[class_aod.values().length];

         try {
            a[class_aod.CLOCKWISE_180.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[class_aod.CLOCKWISE_90.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
