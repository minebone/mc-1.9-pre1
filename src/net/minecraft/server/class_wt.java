package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.MobEffect;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.Path;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_asz;
import net.minecraft.server.class_ata;
import net.minecraft.server.class_aub;
import net.minecraft.server.Material;
import net.minecraft.server.class_aym;
import net.minecraft.server.class_ayo;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rc;
import net.minecraft.server.class_rw;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_wq;
import net.minecraft.server.class_wr;
import net.minecraft.server.class_ws;
import net.minecraft.server.class_xd;
import net.minecraft.server.class_xj;
import net.minecraft.server.class_xk;
import net.minecraft.server.class_yk;
import net.minecraft.server.class_ys;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_wt extends EntityInsentient implements class_wq, class_yk {
   private static final Logger bH = LogManager.getLogger();
   public static final class_ke a = DataWatcher.a(class_wt.class, class_kg.b);
   public double[][] b = new double[64][3];
   public int c = -1;
   public class_wr[] bt;
   public class_wr bu;
   public class_wr bv;
   public class_wr bw;
   public class_wr bx;
   public class_wr by;
   public class_wr bz;
   public class_wr bA;
   public class_wr bB;
   public float bC;
   public float bD;
   public boolean bE;
   public int bF;
   public class_ws bG;
   private final class_asz bI;
   private final class_xk bJ;
   private int bK = 200;
   private int bL;
   private final class_aym[] bM = new class_aym[24];
   private final int[] bN = new int[24];
   private final Path bO = new Path();

   public class_wt(World var1) {
      super(var1);
      this.bt = new class_wr[]{this.bu = new class_wr(this, "head", 6.0F, 6.0F), this.bv = new class_wr(this, "neck", 6.0F, 6.0F), this.bw = new class_wr(this, "body", 8.0F, 8.0F), this.bx = new class_wr(this, "tail", 4.0F, 4.0F), this.by = new class_wr(this, "tail", 4.0F, 4.0F), this.bz = new class_wr(this, "tail", 4.0F, 4.0F), this.bA = new class_wr(this, "wing", 4.0F, 4.0F), this.bB = new class_wr(this, "wing", 4.0F, 4.0F)};
      this.c(this.bV());
      this.a(16.0F, 8.0F);
      this.noClip = true;
      this.fireProof = true;
      this.bK = 100;
      this.ah = true;
      if(!var1.E && var1.s instanceof class_ata) {
         this.bI = ((class_ata)var1.s).s();
      } else {
         this.bI = null;
      }

      this.bJ = new class_xk(this);
   }

   protected void bz() {
      super.bz();
      this.a((class_sk)class_ys.a).a(200.0D);
   }

   protected void i() {
      super.i();
      this.Q().a((class_ke)a, (Object)Integer.valueOf(class_xj.k.b()));
   }

   public double[] a(int var1, float var2) {
      if(this.bP() <= 0.0F) {
         var2 = 0.0F;
      }

      var2 = 1.0F - var2;
      int var3 = this.c - var1 & 63;
      int var4 = this.c - var1 - 1 & 63;
      double[] var5 = new double[3];
      double var6 = this.b[var3][0];
      double var8 = MathHelper.g(this.b[var4][0] - var6);
      var5[0] = var6 + var8 * (double)var2;
      var6 = this.b[var3][1];
      var8 = this.b[var4][1] - var6;
      var5[1] = var6 + var8 * (double)var2;
      var5[2] = this.b[var3][2] + (this.b[var4][2] - this.b[var3][2]) * (double)var2;
      return var5;
   }

   public void n() {
      float var1;
      float var2;
      if(this.world.E) {
         this.c(this.bP());
         if(!this.ac()) {
            var1 = MathHelper.b(this.bD * 6.2831855F);
            var2 = MathHelper.b(this.bC * 6.2831855F);
            if(var2 <= -0.3F && var1 >= -0.3F) {
               this.world.a(this.locX, this.locY, this.locZ, class_ng.aN, this.by(), 5.0F, 0.8F + this.random.nextFloat() * 0.3F, false);
            }

            if(!this.bJ.a().a() && --this.bK < 0) {
               this.world.a(this.locX, this.locY, this.locZ, class_ng.aO, this.by(), 2.5F, 0.8F + this.random.nextFloat() * 0.3F, false);
               this.bK = 200 + this.random.nextInt(200);
            }
         }
      }

      this.bC = this.bD;
      float var25;
      if(this.bP() <= 0.0F) {
         var1 = (this.random.nextFloat() - 0.5F) * 8.0F;
         var2 = (this.random.nextFloat() - 0.5F) * 4.0F;
         var25 = (this.random.nextFloat() - 0.5F) * 8.0F;
         this.world.a(EnumParticle.EXPLOSION_LARGE, this.locX + (double)var1, this.locY + 2.0D + (double)var2, this.locZ + (double)var25, 0.0D, 0.0D, 0.0D, new int[0]);
      } else {
         this.cV();
         var1 = 0.2F / (MathHelper.a(this.motX * this.motX + this.motZ * this.motZ) * 10.0F + 1.0F);
         var1 *= (float)Math.pow(2.0D, this.motY);
         if(this.bJ.a().a()) {
            this.bD += 0.1F;
         } else if(this.bE) {
            this.bD += var1 * 0.5F;
         } else {
            this.bD += var1;
         }

         this.yaw = MathHelper.g(this.yaw);
         if(this.cR()) {
            this.bD = 0.5F;
         } else {
            if(this.c < 0) {
               for(int var22 = 0; var22 < this.b.length; ++var22) {
                  this.b[var22][0] = (double)this.yaw;
                  this.b[var22][1] = this.locY;
               }
            }

            if(++this.c == this.b.length) {
               this.c = 0;
            }

            this.b[this.c][0] = (double)this.yaw;
            this.b[this.c][1] = this.locY;
            double var4;
            double var6;
            double var8;
            float var12;
            if(this.world.E) {
               if(this.bg > 0) {
                  double var23 = this.locX + (this.bh - this.locX) / (double)this.bg;
                  var4 = this.locY + (this.bi - this.locY) / (double)this.bg;
                  var6 = this.locZ + (this.bj - this.locZ) / (double)this.bg;
                  var8 = MathHelper.g(this.bk - (double)this.yaw);
                  this.yaw = (float)((double)this.yaw + var8 / (double)this.bg);
                  this.pitch = (float)((double)this.pitch + (this.bl - (double)this.pitch) / (double)this.bg);
                  --this.bg;
                  this.b(var23, var4, var6);
                  this.b(this.yaw, this.pitch);
               }

               this.bJ.a().b();
            } else {
               class_xd var24 = this.bJ.a();
               var24.c();
               if(this.bJ.a() != var24) {
                  var24 = this.bJ.a();
                  var24.c();
               }

               Vec3D var3 = var24.g();
               if(var3 != null) {
                  var4 = var3.b - this.locX;
                  var6 = var3.c - this.locY;
                  var8 = var3.d - this.locZ;
                  double var10 = var4 * var4 + var6 * var6 + var8 * var8;
                  var12 = var24.f();
                  var6 = MathHelper.a(var6 / (double)MathHelper.a(var4 * var4 + var8 * var8), (double)(-var12), (double)var12);
                  this.motY += var6 * 0.10000000149011612D;
                  this.yaw = MathHelper.g(this.yaw);
                  double var13 = MathHelper.a(MathHelper.g(180.0D - MathHelper.b(var4, var8) * 57.2957763671875D - (double)this.yaw), -50.0D, 50.0D);
                  Vec3D var15 = (new Vec3D(var3.b - this.locX, var3.c - this.locY, var3.d - this.locZ)).a();
                  Vec3D var16 = (new Vec3D((double)MathHelper.a(this.yaw * 0.017453292F), this.motY, (double)(-MathHelper.b(this.yaw * 0.017453292F)))).a();
                  float var17 = Math.max(((float)var16.b(var15) + 0.5F) / 1.5F, 0.0F);
                  this.bf *= 0.8F;
                  this.bf = (float)((double)this.bf + var13 * (double)var24.h());
                  this.yaw += this.bf * 0.1F;
                  float var18 = (float)(2.0D / (var10 + 1.0D));
                  float var19 = 0.06F;
                  this.a(0.0F, -1.0F, var19 * (var17 * var18 + (1.0F - var18)));
                  if(this.bE) {
                     this.d(this.motX * 0.800000011920929D, this.motY * 0.800000011920929D, this.motZ * 0.800000011920929D);
                  } else {
                     this.d(this.motX, this.motY, this.motZ);
                  }

                  Vec3D var20 = (new Vec3D(this.motX, this.motY, this.motZ)).a();
                  float var21 = ((float)var20.b(var16) + 1.0F) / 2.0F;
                  var21 = 0.8F + 0.15F * var21;
                  this.motX *= (double)var21;
                  this.motZ *= (double)var21;
                  this.motY *= 0.9100000262260437D;
               }
            }

            this.aM = this.yaw;
            this.bu.width = this.bu.length = 1.0F;
            this.bv.width = this.bv.length = 3.0F;
            this.bx.width = this.bx.length = 2.0F;
            this.by.width = this.by.length = 2.0F;
            this.bz.width = this.bz.length = 2.0F;
            this.bw.length = 3.0F;
            this.bw.width = 5.0F;
            this.bA.length = 2.0F;
            this.bA.width = 4.0F;
            this.bB.length = 3.0F;
            this.bB.width = 4.0F;
            var2 = (float)(this.a(5, 1.0F)[1] - this.a(10, 1.0F)[1]) * 10.0F * 0.017453292F;
            var25 = MathHelper.b(var2);
            float var26 = MathHelper.a(var2);
            float var5 = this.yaw * 0.017453292F;
            float var27 = MathHelper.a(var5);
            float var7 = MathHelper.b(var5);
            this.bw.m();
            this.bw.b(this.locX + (double)(var27 * 0.5F), this.locY, this.locZ - (double)(var7 * 0.5F), 0.0F, 0.0F);
            this.bA.m();
            this.bA.b(this.locX + (double)(var7 * 4.5F), this.locY + 2.0D, this.locZ + (double)(var27 * 4.5F), 0.0F, 0.0F);
            this.bB.m();
            this.bB.b(this.locX - (double)(var7 * 4.5F), this.locY + 2.0D, this.locZ - (double)(var27 * 4.5F), 0.0F, 0.0F);
            if(!this.world.E && this.ax == 0) {
               this.a(this.world.b((Entity)this, (AxisAlignedBB)this.bA.bk().b(4.0D, 2.0D, 4.0D).c(0.0D, -2.0D, 0.0D)));
               this.a(this.world.b((Entity)this, (AxisAlignedBB)this.bB.bk().b(4.0D, 2.0D, 4.0D).c(0.0D, -2.0D, 0.0D)));
               this.b(this.world.b((Entity)this, (AxisAlignedBB)this.bu.bk().g(1.0D)));
               this.b(this.world.b((Entity)this, (AxisAlignedBB)this.bv.bk().g(1.0D)));
            }

            double[] var28 = this.a(5, 1.0F);
            float var9 = MathHelper.a(this.yaw * 0.017453292F - this.bf * 0.01F);
            float var30 = MathHelper.b(this.yaw * 0.017453292F - this.bf * 0.01F);
            this.bu.m();
            this.bv.m();
            float var11 = this.q(1.0F);
            this.bu.b(this.locX + (double)(var9 * 6.5F * var25), this.locY + (double)var11 + (double)(var26 * 6.5F), this.locZ - (double)(var30 * 6.5F * var25), 0.0F, 0.0F);
            this.bv.b(this.locX + (double)(var9 * 5.5F * var25), this.locY + (double)var11 + (double)(var26 * 5.5F), this.locZ - (double)(var30 * 5.5F * var25), 0.0F, 0.0F);

            for(int var29 = 0; var29 < 3; ++var29) {
               class_wr var31 = null;
               if(var29 == 0) {
                  var31 = this.bx;
               }

               if(var29 == 1) {
                  var31 = this.by;
               }

               if(var29 == 2) {
                  var31 = this.bz;
               }

               double[] var32 = this.a(12 + var29 * 2, 1.0F);
               var12 = this.yaw * 0.017453292F + this.c(var32[0] - var28[0]) * 0.017453292F;
               float var33 = MathHelper.a(var12);
               float var14 = MathHelper.b(var12);
               float var34 = 1.5F;
               float var35 = (float)(var29 + 1) * 2.0F;
               var31.m();
               var31.b(this.locX - (double)((var27 * var34 + var33 * var35) * var25), this.locY + (var32[1] - var28[1]) - (double)((var35 + var34) * var26) + 1.5D, this.locZ + (double)((var7 * var34 + var14 * var35) * var25), 0.0F, 0.0F);
            }

            if(!this.world.E) {
               this.bE = this.b(this.bu.bk()) | this.b(this.bv.bk()) | this.b(this.bw.bk());
               if(this.bI != null) {
                  this.bI.b(this);
               }
            }

         }
      }
   }

   private float q(float var1) {
      double var2 = 0.0D;
      if(this.bJ.a().a()) {
         var2 = -1.0D;
      } else {
         double[] var4 = this.a(5, 1.0F);
         double[] var5 = this.a(0, 1.0F);
         var2 = var4[1] - var5[0];
      }

      return (float)var2;
   }

   private void cV() {
      if(this.bG != null) {
         if(this.bG.dead) {
            this.bG = null;
         } else if(this.ticksLived % 10 == 0 && this.bP() < this.bV()) {
            this.c(this.bP() + 1.0F);
         }
      }

      if(this.random.nextInt(10) == 0) {
         List var1 = this.world.a(class_ws.class, this.bk().g(32.0D));
         class_ws var2 = null;
         double var3 = Double.MAX_VALUE;
         Iterator var5 = var1.iterator();

         while(var5.hasNext()) {
            class_ws var6 = (class_ws)var5.next();
            double var7 = var6.h(this);
            if(var7 < var3) {
               var3 = var7;
               var2 = var6;
            }
         }

         this.bG = var2;
      }

   }

   private void a(List var1) {
      double var2 = (this.bw.bk().a + this.bw.bk().d) / 2.0D;
      double var4 = (this.bw.bk().c + this.bw.bk().f) / 2.0D;
      Iterator var6 = var1.iterator();

      while(var6.hasNext()) {
         Entity var7 = (Entity)var6.next();
         if(var7 instanceof class_rz) {
            double var8 = var7.locX - var2;
            double var10 = var7.locZ - var4;
            double var12 = var8 * var8 + var10 * var10;
            var7.g(var8 / var12 * 4.0D, 0.20000000298023224D, var10 / var12 * 4.0D);
            if(!this.bJ.a().a() && ((class_rz)var7).bG() < var7.ticksLived - 2) {
               var7.a(DamageSource.a((class_rz)this), 5.0F);
               this.a(this, var7);
            }
         }
      }

   }

   private void b(List var1) {
      for(int var2 = 0; var2 < var1.size(); ++var2) {
         Entity var3 = (Entity)var1.get(var2);
         if(var3 instanceof class_rz) {
            var3.a(DamageSource.a((class_rz)this), 10.0F);
            this.a(this, var3);
         }
      }

   }

   private float c(double var1) {
      return (float)MathHelper.g(var1);
   }

   private boolean b(AxisAlignedBB var1) {
      int var2 = MathHelper.c(var1.a);
      int var3 = MathHelper.c(var1.b);
      int var4 = MathHelper.c(var1.c);
      int var5 = MathHelper.c(var1.d);
      int var6 = MathHelper.c(var1.e);
      int var7 = MathHelper.c(var1.f);
      boolean var8 = false;
      boolean var9 = false;

      for(int var10 = var2; var10 <= var5; ++var10) {
         for(int var11 = var3; var11 <= var6; ++var11) {
            for(int var12 = var4; var12 <= var7; ++var12) {
               BlockPosition var13 = new BlockPosition(var10, var11, var12);
               IBlockData var14 = this.world.getType(var13);
               Block var15 = var14.getBlock();
               if(var14.getMaterial() != Material.a && var14.getMaterial() != Material.o) {
                  if(!this.world.U().b("mobGriefing")) {
                     var8 = true;
                  } else if(var15 != Blocks.cv && var15 != Blocks.Z && var15 != Blocks.bH && var15 != Blocks.h && var15 != Blocks.bF && var15 != Blocks.bG) {
                     if(var15 != Blocks.bX && var15 != Blocks.dc && var15 != Blocks.dd && var15 != Blocks.bi && var15 != Blocks.db) {
                        var9 = this.world.g(var13) || var9;
                     } else {
                        var8 = true;
                     }
                  } else {
                     var8 = true;
                  }
               }
            }
         }
      }

      if(var9) {
         double var16 = var1.a + (var1.d - var1.a) * (double)this.random.nextFloat();
         double var17 = var1.b + (var1.e - var1.b) * (double)this.random.nextFloat();
         double var18 = var1.c + (var1.f - var1.c) * (double)this.random.nextFloat();
         this.world.a(EnumParticle.EXPLOSION_LARGE, var16, var17, var18, 0.0D, 0.0D, 0.0D, new int[0]);
      }

      return var8;
   }

   public boolean a(class_wr var1, DamageSource var2, float var3) {
      var3 = this.bJ.a().a(var1, var2, var3);
      if(var1 != this.bu) {
         var3 = var3 / 4.0F + Math.min(var3, 1.0F);
      }

      if(var3 < 0.01F) {
         return false;
      } else {
         if(var2.j() instanceof EntityHuman || var2.c()) {
            float var4 = this.bP();
            this.e(var2, var3);
            if(this.bP() <= 0.0F && !this.bJ.a().a()) {
               this.c(1.0F);
               this.bJ.a(class_xj.j);
            }

            if(this.bJ.a().a()) {
               this.bL = (int)((float)this.bL + (var4 - this.bP()));
               if((float)this.bL > 0.25F * this.bV()) {
                  this.bL = 0;
                  this.bJ.a(class_xj.e);
               }
            }
         }

         return true;
      }
   }

   public boolean a(DamageSource var1, float var2) {
      if(var1 instanceof class_rc && ((class_rc)var1).x()) {
         this.a(this.bw, var1, var2);
      }

      return false;
   }

   protected boolean e(DamageSource var1, float var2) {
      return super.a((DamageSource)var1, var2);
   }

   public void P() {
      this.S();
      if(this.bI != null) {
         this.bI.b(this);
         this.bI.a(this);
      }

   }

   protected void bB() {
      if(this.bI != null) {
         this.bI.b(this);
      }

      ++this.bF;
      if(this.bF >= 180 && this.bF <= 200) {
         float var1 = (this.random.nextFloat() - 0.5F) * 8.0F;
         float var2 = (this.random.nextFloat() - 0.5F) * 4.0F;
         float var3 = (this.random.nextFloat() - 0.5F) * 8.0F;
         this.world.a(EnumParticle.EXPLOSION_HUGE, this.locX + (double)var1, this.locY + 2.0D + (double)var2, this.locZ + (double)var3, 0.0D, 0.0D, 0.0D, new int[0]);
      }

      boolean var4 = this.world.U().b("doMobLoot");
      short var5 = 500;
      if(this.bI != null && !this.bI.d()) {
         var5 = 12000;
      }

      if(!this.world.E) {
         if(this.bF > 150 && this.bF % 5 == 0 && var4) {
            this.a(MathHelper.d((float)var5 * 0.08F));
         }

         if(this.bF == 1) {
            this.world.a(1028, new BlockPosition(this), 0);
         }
      }

      this.d(0.0D, 0.10000000149011612D, 0.0D);
      this.aM = this.yaw += 20.0F;
      if(this.bF == 200 && !this.world.E) {
         if(var4) {
            this.a(MathHelper.d((float)var5 * 0.2F));
         }

         if(this.bI != null) {
            this.bI.a(this);
         }

         this.S();
      }

   }

   private void a(int var1) {
      while(var1 > 0) {
         int var2 = class_rw.a(var1);
         var1 -= var2;
         this.world.a((Entity)(new class_rw(this.world, this.locX, this.locY, this.locZ, var2)));
      }

   }

   public int o() {
      if(this.bM[0] == null) {
         boolean var1 = false;
         boolean var2 = false;
         boolean var3 = false;
         boolean var4 = false;

         for(int var5 = 0; var5 < 24; ++var5) {
            int var6 = 5;
            int var7;
            int var9;
            if(var5 < 12) {
               var7 = (int)(60.0F * MathHelper.b(2.0F * (-3.1415927F + 0.2617994F * (float)var5)));
               var9 = (int)(60.0F * MathHelper.a(2.0F * (-3.1415927F + 0.2617994F * (float)var5)));
            } else {
               int var10;
               if(var5 < 20) {
                  var10 = var5 - 12;
                  var7 = (int)(40.0F * MathHelper.b(2.0F * (-3.1415927F + 0.3926991F * (float)var10)));
                  var9 = (int)(40.0F * MathHelper.a(2.0F * (-3.1415927F + 0.3926991F * (float)var10)));
                  var6 += 10;
               } else {
                  var10 = var5 - 20;
                  var7 = (int)(20.0F * MathHelper.b(2.0F * (-3.1415927F + 0.7853982F * (float)var10)));
                  var9 = (int)(20.0F * MathHelper.a(2.0F * (-3.1415927F + 0.7853982F * (float)var10)));
               }
            }

            int var8 = Math.max(this.world.K() + 10, this.world.q(new BlockPosition(var7, 0, var9)).q() + var6);
            this.bM[var5] = new class_aym(var7, var8, var9);
         }

         this.bN[0] = 6146;
         this.bN[1] = 8197;
         this.bN[2] = 8202;
         this.bN[3] = 16404;
         this.bN[4] = '耨';
         this.bN[5] = '聐';
         this.bN[6] = 65696;
         this.bN[7] = 131392;
         this.bN[8] = 131712;
         this.bN[9] = 263424;
         this.bN[10] = 526848;
         this.bN[11] = 525313;
         this.bN[12] = 1581057;
         this.bN[13] = 3166214;
         this.bN[14] = 2138120;
         this.bN[15] = 6373424;
         this.bN[16] = 4358208;
         this.bN[17] = 12910976;
         this.bN[18] = 9044480;
         this.bN[19] = 9706496;
         this.bN[20] = 15216640;
         this.bN[21] = 13688832;
         this.bN[22] = 11763712;
         this.bN[23] = 8257536;
      }

      return this.l(this.locX, this.locY, this.locZ);
   }

   public int l(double var1, double var3, double var5) {
      float var7 = 10000.0F;
      int var8 = 0;
      class_aym var9 = new class_aym(MathHelper.c(var1), MathHelper.c(var3), MathHelper.c(var5));
      byte var10 = 0;
      if(this.bI == null || this.bI.c() == 0) {
         var10 = 12;
      }

      for(int var11 = var10; var11 < 24; ++var11) {
         if(this.bM[var11] != null) {
            float var12 = this.bM[var11].b(var9);
            if(var12 < var7) {
               var7 = var12;
               var8 = var11;
            }
         }
      }

      return var8;
   }

   public class_ayo a(int var1, int var2, class_aym var3) {
      class_aym var5;
      for(int var4 = 0; var4 < 24; ++var4) {
         var5 = this.bM[var4];
         var5.i = false;
         var5.g = 0.0F;
         var5.e = 0.0F;
         var5.f = 0.0F;
         var5.h = null;
         var5.d = -1;
      }

      class_aym var13 = this.bM[var1];
      var5 = this.bM[var2];
      var13.e = 0.0F;
      var13.f = var13.a(var5);
      var13.g = var13.f;
      this.bO.a();
      this.bO.a(var13);
      class_aym var6 = var13;
      byte var7 = 0;
      if(this.bI == null || this.bI.c() == 0) {
         var7 = 12;
      }

      while(!this.bO.e()) {
         class_aym var8 = this.bO.c();
         if(var8.equals(var5)) {
            if(var3 != null) {
               var3.h = var5;
               var5 = var3;
            }

            return this.a(var13, var5);
         }

         if(var8.a(var5) < var6.a(var5)) {
            var6 = var8;
         }

         var8.i = true;
         int var9 = 0;

         int var10;
         for(var10 = 0; var10 < 24; ++var10) {
            if(this.bM[var10] == var8) {
               var9 = var10;
               break;
            }
         }

         for(var10 = var7; var10 < 24; ++var10) {
            if((this.bN[var9] & 1 << var10) > 0) {
               class_aym var11 = this.bM[var10];
               if(!var11.i) {
                  float var12 = var8.e + var8.a(var11);
                  if(!var11.a() || var12 < var11.e) {
                     var11.h = var8;
                     var11.e = var12;
                     var11.f = var11.a(var5);
                     if(var11.a()) {
                        this.bO.a(var11, var11.e + var11.f);
                     } else {
                        var11.g = var11.e + var11.f;
                        this.bO.a(var11);
                     }
                  }
               }
            }
         }
      }

      if(var6 == var13) {
         return null;
      } else {
         bH.debug("Failed to find path from {} to {}", new Object[]{Integer.valueOf(var1), Integer.valueOf(var2)});
         if(var3 != null) {
            var3.h = var6;
            var6 = var3;
         }

         return this.a(var13, var6);
      }
   }

   private class_ayo a(class_aym var1, class_aym var2) {
      int var3 = 1;

      class_aym var4;
      for(var4 = var2; var4.h != null; var4 = var4.h) {
         ++var3;
      }

      class_aym[] var5 = new class_aym[var3];
      var4 = var2;
      --var3;

      for(var5[var3] = var2; var4.h != null; var5[var3] = var4) {
         var4 = var4.h;
         --var3;
      }

      return new class_ayo(var5);
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("DragonPhase", this.bJ.a().i().b());
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      if(var1.e("DragonPhase")) {
         this.bJ.a(class_xj.a(var1.h("DragonPhase")));
      }

   }

   protected void L() {
   }

   public Entity[] aQ() {
      return this.bt;
   }

   public boolean ao() {
      return false;
   }

   public World a() {
      return this.world;
   }

   public EnumSoundCategory by() {
      return EnumSoundCategory.HOSTILE;
   }

   protected class_nf G() {
      return class_ng.aK;
   }

   protected class_nf bQ() {
      return class_ng.aP;
   }

   protected float cc() {
      return 5.0F;
   }

   public Vec3D a(float var1) {
      class_xd var2 = this.bJ.a();
      class_xj var3 = var2.i();
      Vec3D var4;
      float var6;
      if(var3 != class_xj.d && var3 != class_xj.e) {
         if(var2.a()) {
            float var10 = this.pitch;
            var6 = 1.5F;
            this.pitch = -6.0F * var6 * 5.0F;
            var4 = this.f(var1);
            this.pitch = var10;
         } else {
            var4 = this.f(var1);
         }
      } else {
         BlockPosition var5 = this.world.q(class_aub.a);
         var6 = Math.max(MathHelper.a(this.d(var5)) / 4.0F, 1.0F);
         float var7 = 6.0F / var6;
         float var8 = this.pitch;
         float var9 = 1.5F;
         this.pitch = -var7 * var9 * 5.0F;
         var4 = this.f(var1);
         this.pitch = var8;
      }

      return var4;
   }

   public void a(class_ws var1, BlockPosition var2, DamageSource var3) {
      EntityHuman var4;
      if(var3.j() instanceof EntityHuman) {
         var4 = (EntityHuman)var3.j();
      } else {
         var4 = this.world.a(var2, 64.0D, 64.0D);
      }

      if(var1 == this.bG) {
         this.a(this.bu, DamageSource.b(var4), 10.0F);
      }

      this.bJ.a().a(var1, var2, var3, var4);
   }

   public void a(class_ke var1) {
      if(a.equals(var1) && this.world.E) {
         this.bJ.a(class_xj.a(((Integer)this.Q().a(a)).intValue()));
      }

      super.a((class_ke)var1);
   }

   public class_xk cT() {
      return this.bJ;
   }

   public class_asz cU() {
      return this.bI;
   }

   public void c(MobEffect var1) {
   }

   protected boolean n(Entity var1) {
      return false;
   }

   public boolean aU() {
      return false;
   }
}
