package net.minecraft.server;

import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_adq;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_xv;

public class class_adh extends Item {
   public class_adh() {
      this.e(64);
      this.d(1);
      this.a(CreativeModeTab.i);
      this.a(new class_kk("cast"), new class_adq() {
      });
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      if(var3.bP != null) {
         int var5 = var3.bP.j();
         var1.a(var5, (class_rz)var3);
         var3.a((EnumHand)var4);
      } else {
         var2.a((EntityHuman)null, var3.locX, var3.locY, var3.locZ, class_ng.G, EnumSoundCategory.NEUTRAL, 0.5F, 0.4F / (i.nextFloat() * 0.4F + 0.8F));
         if(!var2.E) {
            var2.a((Entity)(new class_xv(var2, var3)));
         }

         var3.a((EnumHand)var4);
         var3.b(StatisticList.b((Item)this));
      }

      return new class_qo(EnumResult.SUCCESS, var1);
   }

   public boolean g_(ItemStack var1) {
      return super.g_(var1);
   }

   public int c() {
      return 1;
   }
}
