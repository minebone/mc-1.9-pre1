package net.minecraft.server;

import net.minecraft.server.WorldProviderNormal;
import net.minecraft.server.class_asu;

public class class_asx extends class_asu {
   public WorldProviderNormal p() {
      return WorldProviderNormal.OVERWORLD;
   }

   public boolean c(int var1, int var2) {
      return !this.b.c(var1, var2);
   }
}
