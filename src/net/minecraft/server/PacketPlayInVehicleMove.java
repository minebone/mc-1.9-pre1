package net.minecraft.server;

import java.io.IOException;
import net.minecraft.server.Entity;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayInListener;

public class PacketPlayInVehicleMove implements Packet {
   private double a;
   private double b;
   private double c;
   private float d;
   private float e;

   public PacketPlayInVehicleMove() {
   }

   public PacketPlayInVehicleMove(Entity var1) {
      this.a = var1.locX;
      this.b = var1.locY;
      this.c = var1.locZ;
      this.d = var1.yaw;
      this.e = var1.pitch;
   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.readDouble();
      this.b = var1.readDouble();
      this.c = var1.readDouble();
      this.d = var1.readFloat();
      this.e = var1.readFloat();
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.writeDouble(this.a);
      var1.writeDouble(this.b);
      var1.writeDouble(this.c);
      var1.writeFloat(this.d);
      var1.writeFloat(this.e);
   }

   public void a(PacketPlayInListener var1) {
      var1.a(this);
   }

   public double a() {
      return this.a;
   }

   public double b() {
      return this.b;
   }

   public double c() {
      return this.c;
   }

   public float d() {
      return this.d;
   }

   public float e() {
      return this.e;
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketPlayInListener)var1);
   }
}
