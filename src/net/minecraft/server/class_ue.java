package net.minecraft.server;

import net.minecraft.server.EntityInsentient;
import net.minecraft.server.class_tj;

public class class_ue extends class_tj {
   private EntityInsentient a;
   private double b;
   private double c;
   private int d;

   public class_ue(EntityInsentient var1) {
      this.a = var1;
      this.a(3);
   }

   public boolean a() {
      return this.a.bE().nextFloat() < 0.02F;
   }

   public boolean b() {
      return this.d >= 0;
   }

   public void c() {
      double var1 = 6.283185307179586D * this.a.bE().nextDouble();
      this.b = Math.cos(var1);
      this.c = Math.sin(var1);
      this.d = 20 + this.a.bE().nextInt(20);
   }

   public void e() {
      --this.d;
      this.a.t().a(this.a.locX + this.b, this.a.locY + (double)this.a.bm(), this.a.locZ + this.c, (float)this.a.cE(), (float)this.a.cD());
   }
}
