package net.minecraft.server;

import java.io.File;
import net.minecraft.server.FileIOThread;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.RegionFileCache;
import net.minecraft.server.WorldData;
import net.minecraft.server.WorldNBTStorage;
import net.minecraft.server.class_asl;
import net.minecraft.server.class_asr;
import net.minecraft.server.class_asu;
import net.minecraft.server.class_asw;
import net.minecraft.server.class_ata;
import net.minecraft.server.class_pb;

public class class_azb extends WorldNBTStorage {
   public class_azb(File var1, String var2, boolean var3, class_pb var4) {
      super(var1, var2, var3, var4);
   }

   public class_asl a(class_asu var1) {
      File var2 = this.b();
      File var3;
      if(var1 instanceof class_asw) {
         var3 = new File(var2, "DIM-1");
         var3.mkdirs();
         return new class_asr(var3, this.a);
      } else if(var1 instanceof class_ata) {
         var3 = new File(var2, "DIM1");
         var3.mkdirs();
         return new class_asr(var3, this.a);
      } else {
         return new class_asr(var2, this.a);
      }
   }

   public void a(WorldData var1, NBTTagCompound var2) {
      var1.e(19133);
      super.a(var1, var2);
   }

   public void a() {
      try {
         FileIOThread.a().b();
      } catch (InterruptedException var2) {
         var2.printStackTrace();
      }

      RegionFileCache.a();
   }
}
