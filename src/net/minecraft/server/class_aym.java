package net.minecraft.server;

import net.minecraft.server.class_ayl;
import net.minecraft.server.MathHelper;

public class class_aym {
   public final int a;
   public final int b;
   public final int c;
   private final int n;
   public int d = -1;
   public float e;
   public float f;
   public float g;
   public class_aym h;
   public boolean i;
   public float j = 0.0F;
   public float k = 0.0F;
   public float l = 0.0F;
   public class_ayl m = class_ayl.BLOCKED;

   public class_aym(int var1, int var2, int var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.n = b(var1, var2, var3);
   }

   public class_aym a(int var1, int var2, int var3) {
      class_aym var4 = new class_aym(var1, var2, var3);
      var4.d = this.d;
      var4.e = this.e;
      var4.f = this.f;
      var4.g = this.g;
      var4.h = this.h;
      var4.i = this.i;
      var4.j = this.j;
      var4.k = this.k;
      var4.l = this.l;
      var4.m = this.m;
      return var4;
   }

   public static int b(int var0, int var1, int var2) {
      return var1 & 255 | (var0 & 32767) << 8 | (var2 & 32767) << 24 | (var0 < 0?Integer.MIN_VALUE:0) | (var2 < 0?'耀':0);
   }

   public float a(class_aym var1) {
      float var2 = (float)(var1.a - this.a);
      float var3 = (float)(var1.b - this.b);
      float var4 = (float)(var1.c - this.c);
      return MathHelper.c(var2 * var2 + var3 * var3 + var4 * var4);
   }

   public float b(class_aym var1) {
      float var2 = (float)(var1.a - this.a);
      float var3 = (float)(var1.b - this.b);
      float var4 = (float)(var1.c - this.c);
      return var2 * var2 + var3 * var3 + var4 * var4;
   }

   public float c(class_aym var1) {
      float var2 = (float)Math.abs(var1.a - this.a);
      float var3 = (float)Math.abs(var1.b - this.b);
      float var4 = (float)Math.abs(var1.c - this.c);
      return var2 + var3 + var4;
   }

   public boolean equals(Object var1) {
      if(!(var1 instanceof class_aym)) {
         return false;
      } else {
         class_aym var2 = (class_aym)var1;
         return this.n == var2.n && this.a == var2.a && this.b == var2.b && this.c == var2.c;
      }
   }

   public int hashCode() {
      return this.n;
   }

   public boolean a() {
      return this.d >= 0;
   }

   public String toString() {
      return this.a + ", " + this.b + ", " + this.c;
   }
}
