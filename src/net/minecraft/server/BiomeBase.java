package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;
import net.minecraft.server.BiomeDecorator;
import net.minecraft.server.BlockFlowers;
import net.minecraft.server.BlockLongGrass;
import net.minecraft.server.BlockSand;
import net.minecraft.server.Blocks;
import net.minecraft.server.EntityWitch;
import net.minecraft.server.EnumCreatureType;
import net.minecraft.server.World;
import net.minecraft.server.class_aie;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_ail;
import net.minecraft.server.class_aim;
import net.minecraft.server.class_aio;
import net.minecraft.server.class_aip;
import net.minecraft.server.class_aiq;
import net.minecraft.server.class_air;
import net.minecraft.server.class_ais;
import net.minecraft.server.class_ait;
import net.minecraft.server.class_aiu;
import net.minecraft.server.class_aiv;
import net.minecraft.server.class_aiw;
import net.minecraft.server.class_aix;
import net.minecraft.server.class_aiy;
import net.minecraft.server.class_aiz;
import net.minecraft.server.class_aja;
import net.minecraft.server.class_ajb;
import net.minecraft.server.class_ajc;
import net.minecraft.server.class_ajd;
import net.minecraft.server.class_ajf;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ate;
import net.minecraft.server.class_ato;
import net.minecraft.server.class_atp;
import net.minecraft.server.class_aty;
import net.minecraft.server.class_auc;
import net.minecraft.server.class_avd;
import net.minecraft.server.class_ave;
import net.minecraft.server.class_avf;
import net.minecraft.server.class_awu;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ct;
import net.minecraft.server.class_cx;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_ov;
import net.minecraft.server.class_vt;
import net.minecraft.server.class_vw;
import net.minecraft.server.class_vx;
import net.minecraft.server.class_wb;
import net.minecraft.server.class_wd;
import net.minecraft.server.class_wf;
import net.minecraft.server.class_yh;
import net.minecraft.server.class_yi;
import net.minecraft.server.class_yv;
import net.minecraft.server.class_yw;
import net.minecraft.server.class_yx;
import net.minecraft.server.class_yz;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class BiomeBase {
   private static final Logger y = LogManager.getLogger();
   protected static final IBlockData a = Blocks.b.u();
   protected static final IBlockData b = Blocks.AIR.u();
   protected static final IBlockData c = Blocks.h.u();
   protected static final IBlockData d = Blocks.n.u();
   protected static final IBlockData e = Blocks.cM.u();
   protected static final IBlockData f = Blocks.A.u();
   protected static final IBlockData g = Blocks.aI.u();
   protected static final IBlockData h = Blocks.j.u();
   public static final Set i = Sets.newHashSet();
   public static final class_ct j = new class_ct();
   protected static final class_awu k = new class_awu(new Random(1234L), 1);
   protected static final class_awu l = new class_awu(new Random(2345L), 1);
   protected static final class_aty m = new class_aty();
   protected static final class_avf n = new class_avf(false);
   protected static final class_atp o = new class_atp(false);
   protected static final class_avd p = new class_avd();
   public static final class_cx q = new class_cx();
   private final String z;
   private final float A;
   private final float B;
   private final float C;
   private final float D;
   private final int E;
   private final boolean F;
   private final boolean G;
   private final String H;
   public IBlockData r = Blocks.c.u();
   public IBlockData s = Blocks.d.u();
   public BiomeDecorator t;
   protected List u = Lists.newArrayList();
   protected List v = Lists.newArrayList();
   protected List w = Lists.newArrayList();
   protected List x = Lists.newArrayList();

   public static int a(BiomeBase var0) {
      return q.a(var0);
   }

   public static BiomeBase a(int var0) {
      return (BiomeBase)q.a(var0);
   }

   public static BiomeBase b(BiomeBase var0) {
      return (BiomeBase)j.a(a(var0));
   }

   protected BiomeBase(BiomeBase.class_a_in_class_aif var1) {
      this.z = var1.a;
      this.A = var1.b;
      this.B = var1.c;
      this.C = var1.d;
      this.D = var1.e;
      this.E = var1.f;
      this.F = var1.g;
      this.G = var1.h;
      this.H = var1.i;
      this.t = this.a();
      this.v.add(new BiomeBase.BiomeMeta(class_wd.class, 12, 4, 4));
      this.v.add(new BiomeBase.BiomeMeta(class_wb.class, 10, 4, 4));
      this.v.add(new BiomeBase.BiomeMeta(class_vw.class, 10, 4, 4));
      this.v.add(new BiomeBase.BiomeMeta(class_vx.class, 8, 4, 4));
      this.u.add(new BiomeBase.BiomeMeta(class_yx.class, 100, 4, 4));
      this.u.add(new BiomeBase.BiomeMeta(class_yz.class, 100, 4, 4));
      this.u.add(new BiomeBase.BiomeMeta(class_yv.class, 100, 4, 4));
      this.u.add(new BiomeBase.BiomeMeta(class_yh.class, 100, 4, 4));
      this.u.add(new BiomeBase.BiomeMeta(class_yw.class, 100, 4, 4));
      this.u.add(new BiomeBase.BiomeMeta(class_yi.class, 10, 1, 4));
      this.u.add(new BiomeBase.BiomeMeta(EntityWitch.class, 5, 1, 1));
      this.w.add(new BiomeBase.BiomeMeta(class_wf.class, 10, 4, 4));
      this.x.add(new BiomeBase.BiomeMeta(class_vt.class, 10, 8, 8));
   }

   protected BiomeDecorator a() {
      return new BiomeDecorator();
   }

   public boolean b() {
      return this.H != null;
   }

   public class_ato a(Random var1) {
      return (class_ato)(var1.nextInt(10) == 0?o:n);
   }

   public class_auc b(Random var1) {
      return new class_ave(BlockLongGrass.EnumTallGrassType.GRASS);
   }

   public BlockFlowers.EnumFlowerVarient a(Random var1, BlockPosition var2) {
      return var1.nextInt(3) > 0?BlockFlowers.EnumFlowerVarient.DANDELION:BlockFlowers.EnumFlowerVarient.POPPY;
   }

   public List a(EnumCreatureType var1) {
      switch(BiomeBase.SyntheticClass_1.a[var1.ordinal()]) {
      case 1:
         return this.u;
      case 2:
         return this.v;
      case 3:
         return this.w;
      case 4:
         return this.x;
      default:
         return Collections.emptyList();
      }
   }

   public boolean c() {
      return this.p();
   }

   public boolean d() {
      return this.p()?false:this.G;
   }

   public boolean e() {
      return this.k() > 0.85F;
   }

   public float f() {
      return 0.1F;
   }

   public final float a(BlockPosition var1) {
      if(var1.q() > 64) {
         float var2 = (float)(k.a((double)((float)var1.p() / 8.0F), (double)((float)var1.r() / 8.0F)) * 4.0D);
         return this.n() - (var2 + (float)var1.q() - 64.0F) * 0.05F / 30.0F;
      } else {
         return this.n();
      }
   }

   public void a(World var1, Random var2, BlockPosition var3) {
      this.t.a(var1, var2, this, var3);
   }

   public void a(World var1, Random var2, class_ate var3, int var4, int var5, double var6) {
      this.b(var1, var2, var3, var4, var5, var6);
   }

   public final void b(World var1, Random var2, class_ate var3, int var4, int var5, double var6) {
      int var8 = var1.K();
      IBlockData var9 = this.r;
      IBlockData var10 = this.s;
      int var11 = -1;
      int var12 = (int)(var6 / 3.0D + 3.0D + var2.nextDouble() * 0.25D);
      int var13 = var4 & 15;
      int var14 = var5 & 15;
      BlockPosition.class_a_in_class_cj var15 = new BlockPosition.class_a_in_class_cj();

      for(int var16 = 255; var16 >= 0; --var16) {
         if(var16 <= var2.nextInt(5)) {
            var3.a(var14, var16, var13, c);
         } else {
            IBlockData var17 = var3.a(var14, var16, var13);
            if(var17.getMaterial() == Material.a) {
               var11 = -1;
            } else if(var17.getBlock() == Blocks.b) {
               if(var11 == -1) {
                  if(var12 <= 0) {
                     var9 = b;
                     var10 = a;
                  } else if(var16 >= var8 - 4 && var16 <= var8 + 1) {
                     var9 = this.r;
                     var10 = this.s;
                  }

                  if(var16 < var8 && (var9 == null || var9.getMaterial() == Material.a)) {
                     if(this.a((BlockPosition)var15.c(var4, var16, var5)) < 0.15F) {
                        var9 = g;
                     } else {
                        var9 = h;
                     }
                  }

                  var11 = var12;
                  if(var16 >= var8 - 1) {
                     var3.a(var14, var16, var13, var9);
                  } else if(var16 < var8 - 7 - var12) {
                     var9 = b;
                     var10 = a;
                     var3.a(var14, var16, var13, d);
                  } else {
                     var3.a(var14, var16, var13, var10);
                  }
               } else if(var11 > 0) {
                  --var11;
                  var3.a(var14, var16, var13, var10);
                  if(var11 == 0 && var10.getBlock() == Blocks.m) {
                     var11 = var2.nextInt(4) + Math.max(0, var16 - 63);
                     var10 = var10.get(BlockSand.a) == BlockSand.EnumSandVariant.RED_SAND?e:f;
                  }
               }
            }
         }
      }

   }

   public Class g() {
      return this.getClass();
   }

   public BiomeBase.EnumTemperature h() {
      return (double)this.n() < 0.2D?BiomeBase.EnumTemperature.COLD:((double)this.n() < 1.0D?BiomeBase.EnumTemperature.MEDIUM:BiomeBase.EnumTemperature.WARM);
   }

   public static BiomeBase b(int var0) {
      return a(var0, (BiomeBase)null);
   }

   public static BiomeBase a(int var0, BiomeBase var1) {
      BiomeBase var2 = a(var0);
      return var2 == null?var1:var2;
   }

   public boolean i() {
      return false;
   }

   public final float j() {
      return this.A;
   }

   public final float k() {
      return this.D;
   }

   public final String l() {
      return this.z;
   }

   public final float m() {
      return this.B;
   }

   public final float n() {
      return this.C;
   }

   public final boolean p() {
      return this.F;
   }

   public static void q() {
      a(0, "ocean", new class_aiw((new BiomeBase.class_a_in_class_aif("Ocean")).c(-1.0F).d(0.1F)));
      a(1, "plains", new class_aix(false, (new BiomeBase.class_a_in_class_aif("Plains")).c(0.125F).d(0.05F).a(0.8F).b(0.4F)));
      a(2, "desert", new class_ail((new BiomeBase.class_a_in_class_aif("Desert")).c(0.125F).d(0.05F).a(2.0F).b(0.0F).a()));
      a(3, "extreme_hills", new class_aim(class_aim.class_a_in_class_aim.NORMAL, (new BiomeBase.class_a_in_class_aif("Extreme Hills")).c(1.0F).d(0.5F).a(0.2F).b(0.3F)));
      a(4, "forest", new class_aio(class_aio.class_a_in_class_aio.NORMAL, (new BiomeBase.class_a_in_class_aif("Forest")).a(0.7F).b(0.8F)));
      a(5, "taiga", new class_ajc(class_ajc.class_a_in_class_ajc.NORMAL, (new BiomeBase.class_a_in_class_aif("Taiga")).c(0.2F).d(0.2F).a(0.25F).b(0.8F)));
      a(6, "swampland", new class_ajb((new BiomeBase.class_a_in_class_aif("Swampland")).c(-0.2F).d(0.1F).a(0.8F).b(0.9F).a(14745518)));
      a(7, "river", new class_aiy((new BiomeBase.class_a_in_class_aif("River")).c(-0.5F).d(0.0F)));
      a(8, "hell", new class_aip((new BiomeBase.class_a_in_class_aif("Hell")).a(2.0F).b(0.0F).a()));
      a(9, "sky", new class_ajd((new BiomeBase.class_a_in_class_aif("The End")).a()));
      a(10, "frozen_ocean", new class_aiw((new BiomeBase.class_a_in_class_aif("FrozenOcean")).c(-1.0F).d(0.1F).a(0.0F).b(0.5F).b()));
      a(11, "frozen_river", new class_aiy((new BiomeBase.class_a_in_class_aif("FrozenRiver")).c(-0.5F).d(0.0F).a(0.0F).b(0.5F).b()));
      a(12, "ice_flats", new class_aiq(false, (new BiomeBase.class_a_in_class_aif("Ice Plains")).c(0.125F).d(0.05F).a(0.0F).b(0.5F).b()));
      a(13, "ice_mountains", new class_aiq(false, (new BiomeBase.class_a_in_class_aif("Ice Mountains")).c(0.45F).d(0.3F).a(0.0F).b(0.5F).b()));
      a(14, "mushroom_island", new class_ait((new BiomeBase.class_a_in_class_aif("MushroomIsland")).c(0.2F).d(0.3F).a(0.9F).b(1.0F)));
      a(15, "mushroom_island_shore", new class_ait((new BiomeBase.class_a_in_class_aif("MushroomIslandShore")).c(0.0F).d(0.025F).a(0.9F).b(1.0F)));
      a(16, "beaches", new class_aie((new BiomeBase.class_a_in_class_aif("Beach")).c(0.0F).d(0.025F).a(0.8F).b(0.4F)));
      a(17, "desert_hills", new class_ail((new BiomeBase.class_a_in_class_aif("DesertHills")).c(0.45F).d(0.3F).a(2.0F).b(0.0F).a()));
      a(18, "forest_hills", new class_aio(class_aio.class_a_in_class_aio.NORMAL, (new BiomeBase.class_a_in_class_aif("ForestHills")).c(0.45F).d(0.3F).a(0.7F).b(0.8F)));
      a(19, "taiga_hills", new class_ajc(class_ajc.class_a_in_class_ajc.NORMAL, (new BiomeBase.class_a_in_class_aif("TaigaHills")).a(0.25F).b(0.8F).c(0.45F).d(0.3F)));
      a(20, "smaller_extreme_hills", new class_aim(class_aim.class_a_in_class_aim.EXTRA_TREES, (new BiomeBase.class_a_in_class_aif("Extreme Hills Edge")).c(0.8F).d(0.3F).a(0.2F).b(0.3F)));
      a(21, "jungle", new class_air(false, (new BiomeBase.class_a_in_class_aif("Jungle")).a(0.95F).b(0.9F)));
      a(22, "jungle_hills", new class_air(false, (new BiomeBase.class_a_in_class_aif("JungleHills")).c(0.45F).d(0.3F).a(0.95F).b(0.9F)));
      a(23, "jungle_edge", new class_air(true, (new BiomeBase.class_a_in_class_aif("JungleEdge")).a(0.95F).b(0.8F)));
      a(24, "deep_ocean", new class_aiw((new BiomeBase.class_a_in_class_aif("Deep Ocean")).c(-1.8F).d(0.1F)));
      a(25, "stone_beach", new class_aja((new BiomeBase.class_a_in_class_aif("Stone Beach")).c(0.1F).d(0.8F).a(0.2F).b(0.3F)));
      a(26, "cold_beach", new class_aie((new BiomeBase.class_a_in_class_aif("Cold Beach")).c(0.0F).d(0.025F).a(0.05F).b(0.3F).b()));
      a(27, "birch_forest", new class_aio(class_aio.class_a_in_class_aio.BIRCH, (new BiomeBase.class_a_in_class_aif("Birch Forest")).a(0.6F).b(0.6F)));
      a(28, "birch_forest_hills", new class_aio(class_aio.class_a_in_class_aio.BIRCH, (new BiomeBase.class_a_in_class_aif("Birch Forest Hills")).c(0.45F).d(0.3F).a(0.6F).b(0.6F)));
      a(29, "roofed_forest", new class_aio(class_aio.class_a_in_class_aio.ROOFED, (new BiomeBase.class_a_in_class_aif("Roofed Forest")).a(0.7F).b(0.8F)));
      a(30, "taiga_cold", new class_ajc(class_ajc.class_a_in_class_ajc.NORMAL, (new BiomeBase.class_a_in_class_aif("Cold Taiga")).c(0.2F).d(0.2F).a(-0.5F).b(0.4F).b()));
      a(31, "taiga_cold_hills", new class_ajc(class_ajc.class_a_in_class_ajc.NORMAL, (new BiomeBase.class_a_in_class_aif("Cold Taiga Hills")).c(0.45F).d(0.3F).a(-0.5F).b(0.4F).b()));
      a(32, "redwood_taiga", new class_ajc(class_ajc.class_a_in_class_ajc.MEGA, (new BiomeBase.class_a_in_class_aif("Mega Taiga")).a(0.3F).b(0.8F).c(0.2F).d(0.2F)));
      a(33, "redwood_taiga_hills", new class_ajc(class_ajc.class_a_in_class_ajc.MEGA, (new BiomeBase.class_a_in_class_aif("Mega Taiga Hills")).c(0.45F).d(0.3F).a(0.3F).b(0.8F)));
      a(34, "extreme_hills_with_trees", new class_aim(class_aim.class_a_in_class_aim.EXTRA_TREES, (new BiomeBase.class_a_in_class_aif("Extreme Hills+")).c(1.0F).d(0.5F).a(0.2F).b(0.3F)));
      a(35, "savanna", new class_aiz((new BiomeBase.class_a_in_class_aif("Savanna")).c(0.125F).d(0.05F).a(1.2F).b(0.0F).a()));
      a(36, "savanna_rock", new class_aiz((new BiomeBase.class_a_in_class_aif("Savanna Plateau")).c(1.5F).d(0.025F).a(1.0F).b(0.0F).a()));
      a(37, "mesa", new class_ais(false, false, (new BiomeBase.class_a_in_class_aif("Mesa")).a(2.0F).b(0.0F).a()));
      a(38, "mesa_rock", new class_ais(false, true, (new BiomeBase.class_a_in_class_aif("Mesa Plateau F")).c(1.5F).d(0.025F).a(2.0F).b(0.0F).a()));
      a(39, "mesa_clear_rock", new class_ais(false, false, (new BiomeBase.class_a_in_class_aif("Mesa Plateau")).c(1.5F).d(0.025F).a(2.0F).b(0.0F).a()));
      a(127, "void", new class_ajf((new BiomeBase.class_a_in_class_aif("The Void")).a()));
      a(129, "mutated_plains", new class_aix(true, (new BiomeBase.class_a_in_class_aif("Sunflower Plains")).a("plains").c(0.125F).d(0.05F).a(0.8F).b(0.4F)));
      a(130, "mutated_desert", new class_ail((new BiomeBase.class_a_in_class_aif("Desert M")).a("desert").c(0.225F).d(0.25F).a(2.0F).b(0.0F).a()));
      a(131, "mutated_extreme_hills", new class_aim(class_aim.class_a_in_class_aim.MUTATED, (new BiomeBase.class_a_in_class_aif("Extreme Hills M")).a("extreme_hills").c(1.0F).d(0.5F).a(0.2F).b(0.3F)));
      a(132, "mutated_forest", new class_aio(class_aio.class_a_in_class_aio.FLOWER, (new BiomeBase.class_a_in_class_aif("Flower Forest")).a("forest").d(0.4F).a(0.7F).b(0.8F)));
      a(133, "mutated_taiga", new class_ajc(class_ajc.class_a_in_class_ajc.NORMAL, (new BiomeBase.class_a_in_class_aif("Taiga M")).a("taiga").c(0.3F).d(0.4F).a(0.25F).b(0.8F)));
      a(134, "mutated_swampland", new class_ajb((new BiomeBase.class_a_in_class_aif("Swampland M")).a("swampland").c(-0.1F).d(0.3F).a(0.8F).b(0.9F).a(14745518)));
      a(140, "mutated_ice_flats", new class_aiq(true, (new BiomeBase.class_a_in_class_aif("Ice Plains Spikes")).a("ice_flats").c(0.425F).d(0.45000002F).a(0.0F).b(0.5F).b()));
      a(149, "mutated_jungle", new class_air(false, (new BiomeBase.class_a_in_class_aif("Jungle M")).a("jungle").c(0.2F).d(0.4F).a(0.95F).b(0.9F)));
      a(151, "mutated_jungle_edge", new class_air(true, (new BiomeBase.class_a_in_class_aif("JungleEdge M")).a("jungle_edge").c(0.2F).d(0.4F).a(0.95F).b(0.8F)));
      a(155, "mutated_birch_forest", new class_aiu((new BiomeBase.class_a_in_class_aif("Birch Forest M")).a("birch_forest").c(0.2F).d(0.4F).a(0.6F).b(0.6F)));
      a(156, "mutated_birch_forest_hills", new class_aiu((new BiomeBase.class_a_in_class_aif("Birch Forest Hills M")).a("birch_forest").c(0.55F).d(0.5F).a(0.6F).b(0.6F)));
      a(157, "mutated_roofed_forest", new class_aio(class_aio.class_a_in_class_aio.ROOFED, (new BiomeBase.class_a_in_class_aif("Roofed Forest M")).a("roofed_forest").c(0.2F).d(0.4F).a(0.7F).b(0.8F)));
      a(158, "mutated_taiga_cold", new class_ajc(class_ajc.class_a_in_class_ajc.NORMAL, (new BiomeBase.class_a_in_class_aif("Cold Taiga M")).a("taiga_cold").c(0.3F).d(0.4F).a(-0.5F).b(0.4F).b()));
      a(160, "mutated_redwood_taiga", new class_ajc(class_ajc.class_a_in_class_ajc.MEGA_SPRUCE, (new BiomeBase.class_a_in_class_aif("Mega Spruce Taiga")).a("redwood_taiga").c(0.2F).d(0.2F).a(0.25F).b(0.8F)));
      a(161, "mutated_redwood_taiga_hills", new class_ajc(class_ajc.class_a_in_class_ajc.MEGA_SPRUCE, (new BiomeBase.class_a_in_class_aif("Redwood Taiga Hills M")).a("redwood_taiga_hills").c(0.2F).d(0.2F).a(0.25F).b(0.8F)));
      a(162, "mutated_extreme_hills_with_trees", new class_aim(class_aim.class_a_in_class_aim.MUTATED, (new BiomeBase.class_a_in_class_aif("Extreme Hills+ M")).a("extreme_hills_with_trees").c(1.0F).d(0.5F).a(0.2F).b(0.3F)));
      a(163, "mutated_savanna", new class_aiv((new BiomeBase.class_a_in_class_aif("Savanna M")).a("savanna").c(0.3625F).d(1.225F).a(1.1F).b(0.0F).a()));
      a(164, "mutated_savanna_rock", new class_aiv((new BiomeBase.class_a_in_class_aif("Savanna Plateau M")).a("savanna_rock").c(1.05F).d(1.2125001F).a(1.0F).b(0.0F).a()));
      a(165, "mutated_mesa", new class_ais(true, false, (new BiomeBase.class_a_in_class_aif("Mesa (Bryce)")).a("mesa").a(2.0F).b(0.0F).a()));
      a(166, "mutated_mesa_rock", new class_ais(false, true, (new BiomeBase.class_a_in_class_aif("Mesa Plateau F M")).a("mesa_rock").c(0.45F).d(0.3F).a(2.0F).b(0.0F).a()));
      a(167, "mutated_mesa_clear_rock", new class_ais(false, false, (new BiomeBase.class_a_in_class_aif("Mesa Plateau M")).a("mesa_clear_rock").c(0.45F).d(0.3F).a(2.0F).b(0.0F).a()));
      Collections.addAll(i, new BiomeBase[]{class_aik.a, class_aik.c, class_aik.d, class_aik.e, class_aik.f, class_aik.g, class_aik.h, class_aik.i, class_aik.m, class_aik.n, class_aik.o, class_aik.p, class_aik.q, class_aik.r, class_aik.s, class_aik.t, class_aik.u, class_aik.w, class_aik.x, class_aik.y, class_aik.z, class_aik.A, class_aik.B, class_aik.C, class_aik.D, class_aik.E, class_aik.F, class_aik.G, class_aik.H, class_aik.I, class_aik.J, class_aik.K, class_aik.L, class_aik.M, class_aik.N, class_aik.O});
   }

   private static void a(int var0, String var1, BiomeBase var2) {
      q.a(var0, new class_kk(var1), var2);
      if(var2.b()) {
         j.a(var2, a((BiomeBase)q.c(new class_kk(var2.H))));
      }

   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumCreatureType.values().length];

      static {
         try {
            a[EnumCreatureType.MONSTER.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumCreatureType.CREATURE.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumCreatureType.WATER_CREATURE.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumCreatureType.AMBIENT.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static class class_a_in_class_aif {
      private final String a;
      private float b = 0.1F;
      private float c = 0.2F;
      private float d = 0.5F;
      private float e = 0.5F;
      private int f = 16777215;
      private boolean g;
      private boolean h = true;
      private String i;

      public class_a_in_class_aif(String var1) {
         this.a = var1;
      }

      protected BiomeBase.class_a_in_class_aif a(float var1) {
         if(var1 > 0.1F && var1 < 0.2F) {
            throw new IllegalArgumentException("Please avoid temperatures in the range 0.1 - 0.2 because of snow");
         } else {
            this.d = var1;
            return this;
         }
      }

      protected BiomeBase.class_a_in_class_aif b(float var1) {
         this.e = var1;
         return this;
      }

      protected BiomeBase.class_a_in_class_aif c(float var1) {
         this.b = var1;
         return this;
      }

      protected BiomeBase.class_a_in_class_aif d(float var1) {
         this.c = var1;
         return this;
      }

      protected BiomeBase.class_a_in_class_aif a() {
         this.h = false;
         return this;
      }

      protected BiomeBase.class_a_in_class_aif b() {
         this.g = true;
         return this;
      }

      protected BiomeBase.class_a_in_class_aif a(int var1) {
         this.f = var1;
         return this;
      }

      protected BiomeBase.class_a_in_class_aif a(String var1) {
         this.i = var1;
         return this;
      }
   }

   public static class BiomeMeta extends class_ov.class_a_in_class_ov {
      public Class b;
      public int c;
      public int d;

      public BiomeMeta(Class var1, int var2, int var3, int var4) {
         super(var2);
         this.b = var1;
         this.c = var3;
         this.d = var4;
      }

      public String toString() {
         return this.b.getSimpleName() + "*(" + this.c + "-" + this.d + "):" + this.a;
      }
   }

   public static enum EnumTemperature {
      OCEAN,
      COLD,
      MEDIUM,
      WARM;
   }
}
