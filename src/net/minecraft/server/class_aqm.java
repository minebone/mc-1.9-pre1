package net.minecraft.server;

import net.minecraft.server.ChatClickable;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatModifier;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.class_bz;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ev;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutUpdateSign;
import net.minecraft.server.ICommandListener;

public class class_aqm extends TileEntity {
   public final IChatBaseComponent[] a = new IChatBaseComponent[]{new ChatComponentText(""), new ChatComponentText(""), new ChatComponentText(""), new ChatComponentText("")};
   public int f = -1;
   private boolean g = true;
   private EntityHuman h;
   private final CommandObjectiveExecutor i = new CommandObjectiveExecutor();

   public void a(NBTTagCompound var1) {
      super.a(var1);

      for(int var2 = 0; var2 < 4; ++var2) {
         String var3 = IChatBaseComponent.ChatSerializer.a(this.a[var2]);
         var1.a("Text" + (var2 + 1), var3);
      }

      this.i.b(var1);
   }

   public void a(final MinecraftServer var1, NBTTagCompound var2) {
      this.g = false;
      super.a(var1, var2);
      ICommandListener var3 = new ICommandListener() {
         public String h_() {
            return "Sign";
         }

         public IChatBaseComponent i_() {
            return new ChatComponentText(this.h_());
         }

         public void a(IChatBaseComponent var1x) {
         }

         public boolean a(int var1x, String var2) {
            return true;
         }

         public BlockPosition c() {
            return class_aqm.this.c;
         }

         public Vec3D d() {
            return new Vec3D((double)class_aqm.this.c.p() + 0.5D, (double)class_aqm.this.c.q() + 0.5D, (double)class_aqm.this.c.r() + 0.5D);
         }

         public World e() {
            return class_aqm.this.b;
         }

         public Entity f() {
            return null;
         }

         public boolean z_() {
            return false;
         }

         public void a(CommandObjectiveExecutor.EnumCommandResult var1x, int var2) {
         }

         public MinecraftServer h() {
            return var1;
         }
      };

      for(int var4 = 0; var4 < 4; ++var4) {
         String var5 = var2.l("Text" + (var4 + 1));
         IChatBaseComponent var6 = IChatBaseComponent.ChatSerializer.a(var5);

         try {
            this.a[var4] = class_ev.a(var3, var6, (Entity)null);
         } catch (class_bz var8) {
            this.a[var4] = var6;
         }
      }

      this.i.a(var2);
   }

   public Packet D_() {
      IChatBaseComponent[] var1 = new IChatBaseComponent[4];
      System.arraycopy(this.a, 0, var1, 0, 4);
      return new PacketPlayOutUpdateSign(this.b, this.c, var1);
   }

   public boolean B() {
      return true;
   }

   public boolean b() {
      return this.g;
   }

   public void a(EntityHuman var1) {
      this.h = var1;
   }

   public EntityHuman c() {
      return this.h;
   }

   public boolean b(final EntityHuman var1) {
      ICommandListener var2 = new ICommandListener() {
         public String h_() {
            return var1.h_();
         }

         public IChatBaseComponent i_() {
            return var1.i_();
         }

         public void a(IChatBaseComponent var1x) {
         }

         public boolean a(int var1x, String var2) {
            return var1x <= 2;
         }

         public BlockPosition c() {
            return class_aqm.this.c;
         }

         public Vec3D d() {
            return new Vec3D((double)class_aqm.this.c.p() + 0.5D, (double)class_aqm.this.c.q() + 0.5D, (double)class_aqm.this.c.r() + 0.5D);
         }

         public World e() {
            return var1.e();
         }

         public Entity f() {
            return var1;
         }

         public boolean z_() {
            return false;
         }

         public void a(CommandObjectiveExecutor.EnumCommandResult var1x, int var2) {
            if(class_aqm.this.b != null && !class_aqm.this.b.E) {
               class_aqm.this.i.a(class_aqm.this.b.u(), this, var1x, var2);
            }

         }

         public MinecraftServer h() {
            return var1.h();
         }
      };

      for(int var3 = 0; var3 < this.a.length; ++var3) {
         ChatModifier var4 = this.a[var3] == null?null:this.a[var3].b();
         if(var4 != null && var4.h() != null) {
            ChatClickable var5 = var4.h();
            if(var5.a() == ChatClickable.class_a_in_class_et.RUN_COMMAND) {
               var1.h().N().a(var2, var5.b());
            }
         }
      }

      return true;
   }

   public CommandObjectiveExecutor d() {
      return this.i;
   }
}
