package net.minecraft.server;

import net.minecraft.server.BiomeBase;
import net.minecraft.server.class_ajg;

public class class_ajf extends BiomeBase {
   public class_ajf(BiomeBase.class_a_in_class_aif var1) {
      super(var1);
      this.u.clear();
      this.v.clear();
      this.w.clear();
      this.x.clear();
      this.t = new class_ajg();
   }

   public boolean i() {
      return true;
   }
}
