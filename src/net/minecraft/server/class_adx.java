package net.minecraft.server;

import net.minecraft.server.BlockMinecartTrackAbstract;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityMinecartAbstract;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_akt;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ck;
import net.minecraft.server.class_cn;
import net.minecraft.server.class_cr;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;

public class class_adx extends Item {
   private static final class_cr a = new class_cn() {
      private final class_cn b = new class_cn();

      public ItemStack b(class_ck var1, ItemStack var2) {
         EnumDirection var3 = class_akt.e(var1.f());
         World var4 = var1.i();
         double var5 = var1.a() + (double)var3.g() * 1.125D;
         double var7 = Math.floor(var1.b()) + (double)var3.h();
         double var9 = var1.c() + (double)var3.i() * 1.125D;
         BlockPosition var11 = var1.d().a(var3);
         IBlockData var12 = var4.getType(var11);
         BlockMinecartTrackAbstract.EnumTrackPosition var13 = var12.getBlock() instanceof BlockMinecartTrackAbstract?(BlockMinecartTrackAbstract.EnumTrackPosition)var12.get(((BlockMinecartTrackAbstract)var12.getBlock()).g()):BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH;
         double var14;
         if(BlockMinecartTrackAbstract.i(var12)) {
            if(var13.c()) {
               var14 = 0.6D;
            } else {
               var14 = 0.1D;
            }
         } else {
            if(var12.getMaterial() != Material.a || !BlockMinecartTrackAbstract.i(var4.getType(var11.b()))) {
               return this.b.a(var1, var2);
            }

            IBlockData var16 = var4.getType(var11.b());
            BlockMinecartTrackAbstract.EnumTrackPosition var17 = var16.getBlock() instanceof BlockMinecartTrackAbstract?(BlockMinecartTrackAbstract.EnumTrackPosition)var16.get(((BlockMinecartTrackAbstract)var16.getBlock()).g()):BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH;
            if(var3 != EnumDirection.DOWN && var17.c()) {
               var14 = -0.4D;
            } else {
               var14 = -0.9D;
            }
         }

         EntityMinecartAbstract var18 = EntityMinecartAbstract.a(var4, var5, var7 + var14, var9, ((class_adx)var2.b()).b);
         if(var2.s()) {
            var18.c(var2.q());
         }

         var4.a((Entity)var18);
         var2.a(1);
         return var2;
      }

      protected void a(class_ck var1) {
         var1.i().b(1000, var1.d(), 0);
      }
   };
   private final EntityMinecartAbstract.EnumMinecartType b;

   public class_adx(EntityMinecartAbstract.EnumMinecartType var1) {
      this.j = 1;
      this.b = var1;
      this.a(CreativeModeTab.e);
      class_akt.c.a(this, a);
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      IBlockData var10 = var3.getType(var4);
      if(!BlockMinecartTrackAbstract.i(var10)) {
         return EnumResult.FAIL;
      } else {
         if(!var3.E) {
            BlockMinecartTrackAbstract.EnumTrackPosition var11 = var10.getBlock() instanceof BlockMinecartTrackAbstract?(BlockMinecartTrackAbstract.EnumTrackPosition)var10.get(((BlockMinecartTrackAbstract)var10.getBlock()).g()):BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH;
            double var12 = 0.0D;
            if(var11.c()) {
               var12 = 0.5D;
            }

            EntityMinecartAbstract var14 = EntityMinecartAbstract.a(var3, (double)var4.p() + 0.5D, (double)var4.q() + 0.0625D + var12, (double)var4.r() + 0.5D, this.b);
            if(var1.s()) {
               var14.c(var1.q());
            }

            var3.a((Entity)var14);
         }

         --var1.b;
         return EnumResult.SUCCESS;
      }
   }
}
