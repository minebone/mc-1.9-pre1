package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumColor;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aju;
import net.minecraft.server.class_anf;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_wd;

public class class_act extends Item {
   public static final int[] a = new int[]{1973019, 11743532, 3887386, 5320730, 2437522, 8073150, 2651799, 11250603, 4408131, 14188952, 4312372, 14602026, 6719955, 12801229, 15435844, 15790320};

   public class_act() {
      this.a(true);
      this.e(0);
      this.a(CreativeModeTab.l);
   }

   public String f_(ItemStack var1) {
      int var2 = var1.i();
      return super.a() + "." + EnumColor.a(var2).d();
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      if(!var2.a(var4.a(var6), var6, var1)) {
         return EnumResult.FAIL;
      } else {
         EnumColor var10 = EnumColor.a(var1.i());
         if(var10 == EnumColor.WHITE) {
            if(a(var1, var3, var4)) {
               if(!var3.E) {
                  var3.b(2005, var4, 0);
               }

               return EnumResult.SUCCESS;
            }
         } else if(var10 == EnumColor.BROWN) {
            IBlockData var11 = var3.getType(var4);
            Block var12 = var11.getBlock();
            if(var12 == Blocks.r && var11.get(class_anf.b) == BlockWood.EnumLogVariant.JUNGLE) {
               if(var6 != EnumDirection.DOWN && var6 != EnumDirection.UP) {
                  var4 = var4.a(var6);
                  if(var3.d(var4)) {
                     IBlockData var13 = Blocks.bN.a(var3, var4, var6, var7, var8, var9, 0, var2);
                     var3.a((BlockPosition)var4, (IBlockData)var13, 10);
                     if(!var2.abilities.d) {
                        --var1.b;
                     }
                  }

                  return EnumResult.SUCCESS;
               }

               return EnumResult.FAIL;
            }

            return EnumResult.FAIL;
         }

         return EnumResult.PASS;
      }
   }

   public static boolean a(ItemStack var0, World var1, BlockPosition var2) {
      IBlockData var3 = var1.getType(var2);
      if(var3.getBlock() instanceof class_aju) {
         class_aju var4 = (class_aju)var3.getBlock();
         if(var4.a(var1, var2, var3, var1.E)) {
            if(!var1.E) {
               if(var4.a(var1, var1.r, var2, var3)) {
                  var4.b(var1, var1.r, var2, var3);
               }

               --var0.b;
            }

            return true;
         }
      }

      return false;
   }

   public boolean a(ItemStack var1, EntityHuman var2, class_rz var3, EnumHand var4) {
      if(var3 instanceof class_wd) {
         class_wd var5 = (class_wd)var3;
         EnumColor var6 = EnumColor.a(var1.i());
         if(!var5.da() && var5.cZ() != var6) {
            var5.b(var6);
            --var1.b;
         }

         return true;
      } else {
         return false;
      }
   }
}
