package net.minecraft.server;

import net.minecraft.server.DamageSource;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Vec3D;
import net.minecraft.server.class_aub;
import net.minecraft.server.class_aym;
import net.minecraft.server.class_ayo;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_ws;
import net.minecraft.server.class_wt;
import net.minecraft.server.class_wv;
import net.minecraft.server.class_xh;
import net.minecraft.server.class_xj;

public class class_wz extends class_wv {
   private class_ayo b;
   private Vec3D c;
   private boolean d;

   public class_wz(class_wt var1) {
      super(var1);
   }

   public class_xj i() {
      return class_xj.a;
   }

   public void c() {
      double var1 = this.c == null?0.0D:this.c.c(this.a.locX, this.a.locY, this.a.locZ);
      if(var1 < 100.0D || var1 > 22500.0D || this.a.positionChanged || this.a.B) {
         this.j();
      }

   }

   public void d() {
      this.b = null;
      this.c = null;
   }

   public Vec3D g() {
      return this.c;
   }

   private void j() {
      int var2;
      if(this.b != null && this.b.b()) {
         BlockPosition var1 = this.a.world.q(new BlockPosition(class_aub.a));
         var2 = this.a.cU() == null?0:this.a.cU().c();
         if(this.a.bE().nextInt(var2 + 3) == 0) {
            this.a.cT().a(class_xj.c);
            return;
         }

         double var3 = 64.0D;
         EntityHuman var5 = this.a.world.a(var1, var3, var3);
         if(var5 != null) {
            var3 = var5.d((BlockPosition)var1) / 512.0D;
         }

         if(var5 != null && (this.a.bE().nextInt(MathHelper.a((int)var3) + 2) == 0 || this.a.bE().nextInt(var2 + 2) == 0)) {
            this.a(var5);
            return;
         }
      }

      if(this.b == null || this.b.b()) {
         int var6 = this.a.o();
         var2 = var6;
         if(this.a.bE().nextInt(8) == 0) {
            this.d = !this.d;
            var2 = var6 + 6;
         }

         if(this.d) {
            ++var2;
         } else {
            --var2;
         }

         if(this.a.cU() != null && this.a.cU().c() >= 0) {
            var2 %= 12;
            if(var2 < 0) {
               var2 += 12;
            }
         } else {
            var2 -= 12;
            var2 &= 7;
            var2 += 12;
         }

         this.b = this.a.a(var6, var2, (class_aym)null);
         if(this.b != null) {
            this.b.a();
         }
      }

      this.k();
   }

   private void a(EntityHuman var1) {
      this.a.cT().a(class_xj.b);
      ((class_xh)this.a.cT().b(class_xj.b)).a(var1);
   }

   private void k() {
      if(this.b != null && !this.b.b()) {
         Vec3D var1 = this.b.f();
         this.b.a();
         double var2 = var1.b;
         double var4 = var1.d;

         double var6;
         do {
            var6 = var1.c + (double)(this.a.bE().nextFloat() * 20.0F);
         } while(var6 < var1.c);

         this.c = new Vec3D(var2, var6, var4);
      }

   }

   public void a(class_ws var1, BlockPosition var2, DamageSource var3, EntityHuman var4) {
      if(var4 != null) {
         this.a(var4);
      }

   }
}
