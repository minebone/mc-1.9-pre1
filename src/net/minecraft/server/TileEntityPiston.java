package net.minecraft.server;

import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.class_ahw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_axg;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ky;

public class TileEntityPiston extends TileEntity implements class_ky {
   private IBlockData a;
   private EnumDirection f;
   private boolean g;
   private boolean h;
   private float i;
   private float j;

   public TileEntityPiston() {
   }

   public TileEntityPiston(IBlockData var1, EnumDirection var2, boolean var3, boolean var4) {
      this.a = var1;
      this.f = var2;
      this.g = var3;
      this.h = var4;
   }

   public IBlockData b() {
      return this.a;
   }

   public int u() {
      return 0;
   }

   public boolean d() {
      return this.g;
   }

   public EnumDirection e() {
      return this.f;
   }

   private float e(float var1) {
      return this.g?var1 - 1.0F:1.0F - var1;
   }

   public AxisAlignedBB a(class_ahw var1, BlockPosition var2) {
      return this.a(var1, var2, this.i).a(this.a(var1, var2, this.j));
   }

   public AxisAlignedBB a(class_ahw var1, BlockPosition var2, float var3) {
      var3 = this.e(var3);
      return this.a.c(var1, var2).c((double)(var3 * (float)this.f.g()), (double)(var3 * (float)this.f.h()), (double)(var3 * (float)this.f.i()));
   }

   private void i() {
      AxisAlignedBB var1 = this.a((class_ahw)this.b, (BlockPosition)this.c).a(this.c);
      List var2 = this.b.b((Entity)null, (AxisAlignedBB)var1);
      if(!var2.isEmpty()) {
         EnumDirection var3 = this.g?this.f:this.f.d();

         for(int var4 = 0; var4 < var2.size(); ++var4) {
            Entity var5 = (Entity)var2.get(var4);
            if(var5.z() != class_axg.IGNORE) {
               if(this.a.getBlock() == Blocks.cE) {
                  switch(TileEntityPiston.SyntheticClass_1.a[var3.k().ordinal()]) {
                  case 1:
                     var5.motX = (double)var3.g();
                     break;
                  case 2:
                     var5.motY = (double)var3.h();
                     break;
                  case 3:
                     var5.motZ = (double)var3.i();
                  }
               }

               double var6 = 0.0D;
               double var8 = 0.0D;
               double var10 = 0.0D;
               AxisAlignedBB var12 = var5.bk();
               switch(TileEntityPiston.SyntheticClass_1.a[var3.k().ordinal()]) {
               case 1:
                  if(var3.c() == EnumDirection.EnumAxisDirection.POSITIVE) {
                     var6 = var1.d - var12.a;
                  } else {
                     var6 = var12.d - var1.a;
                  }

                  var6 += 0.01D;
                  break;
               case 2:
                  if(var3.c() == EnumDirection.EnumAxisDirection.POSITIVE) {
                     var8 = var1.e - var12.b;
                  } else {
                     var8 = var12.e - var1.b;
                  }

                  var8 += 0.01D;
                  break;
               case 3:
                  if(var3.c() == EnumDirection.EnumAxisDirection.POSITIVE) {
                     var10 = var1.f - var12.c;
                  } else {
                     var10 = var12.f - var1.c;
                  }

                  var10 += 0.01D;
               }

               var5.d(var6 * (double)var3.g(), var8 * (double)var3.h(), var10 * (double)var3.i());
            }
         }

      }
   }

   public void h() {
      if(this.j < 1.0F && this.b != null) {
         this.j = this.i = 1.0F;
         this.b.s(this.c);
         this.y();
         if(this.b.getType(this.c).getBlock() == Blocks.M) {
            this.b.a((BlockPosition)this.c, (IBlockData)this.a, 3);
            this.b.e(this.c, this.a.getBlock());
         }
      }

   }

   public void c() {
      this.j = this.i;
      if(this.j >= 1.0F) {
         this.i();
         this.b.s(this.c);
         this.y();
         if(this.b.getType(this.c).getBlock() == Blocks.M) {
            this.b.a((BlockPosition)this.c, (IBlockData)this.a, 3);
            this.b.e(this.c, this.a.getBlock());
         }

      } else {
         this.i += 0.5F;
         if(this.i >= 1.0F) {
            this.i = 1.0F;
         }

         this.i();
      }
   }

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      this.a = Block.b(var2.h("blockId")).a(var2.h("blockData"));
      this.f = EnumDirection.a(var2.h("facing"));
      this.j = this.i = var2.j("progress");
      this.g = var2.p("extending");
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      var1.a("blockId", Block.a(this.a.getBlock()));
      var1.a("blockData", this.a.getBlock().e(this.a));
      var1.a("facing", this.f.a());
      var1.a("progress", this.j);
      var1.a("extending", this.g);
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumDirection.class_a_in_class_cq.values().length];

      static {
         try {
            a[EnumDirection.class_a_in_class_cq.X.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.class_a_in_class_cq.Y.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.class_a_in_class_cq.Z.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
