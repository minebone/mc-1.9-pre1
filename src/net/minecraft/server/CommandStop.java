package net.minecraft.server;

import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.ICommandListener;

public class CommandStop extends CommandAbstract {
   public String c() {
      return "stop";
   }

   public String b(ICommandListener var1) {
      return "commands.stop.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var1.d != null) {
         a(var2, this, "commands.stop.start", new Object[0]);
      }

      var1.x();
   }
}
