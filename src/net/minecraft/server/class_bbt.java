package net.minecraft.server;

import net.minecraft.server.IScoreboardCriteria;
import net.minecraft.server.class_bbs;

public class class_bbt extends class_bbs {
   public class_bbt(String var1) {
      super(var1);
   }

   public boolean b() {
      return true;
   }

   public IScoreboardCriteria.EnumScoreboardHealthDisplay c() {
      return IScoreboardCriteria.EnumScoreboardHealthDisplay.HEARTS;
   }
}
