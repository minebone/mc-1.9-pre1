package net.minecraft.server;

import java.util.List;
import java.util.Random;
import net.minecraft.server.Blocks;
import net.minecraft.server.Container;
import net.minecraft.server.Enchantment;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumColor;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.class_aaz;
import net.minecraft.server.class_abs;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agn;
import net.minecraft.server.class_ago;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_qu;

public class ContainerEnchantTable extends Container {
   public IInventory a = new class_qu("Enchant", true, 2) {
      public int w_() {
         return 64;
      }

      public void v_() {
         super.v_();
         ContainerEnchantTable.this.a((IInventory)this);
      }
   };
   private World j;
   private BlockPosition k;
   private Random l = new Random();
   public int f;
   public int[] g = new int[3];
   public int[] h = new int[]{-1, -1, -1};
   public int[] i = new int[]{-1, -1, -1};

   public ContainerEnchantTable(PlayerInventory var1, World var2, BlockPosition var3) {
      this.j = var2;
      this.k = var3;
      this.f = var1.e.cP();
      this.a((class_abs)(new class_abs(this.a, 0, 15, 47) {
         public boolean a(ItemStack var1) {
            return true;
         }

         public int a() {
            return 1;
         }
      }));
      this.a((class_abs)(new class_abs(this.a, 1, 35, 47) {
         public boolean a(ItemStack var1) {
            return var1.b() == Items.bd && EnumColor.a(var1.i()) == EnumColor.BLUE;
         }
      }));

      int var4;
      for(var4 = 0; var4 < 3; ++var4) {
         for(int var5 = 0; var5 < 9; ++var5) {
            this.a((class_abs)(new class_abs(var1, var5 + var4 * 9 + 9, 8 + var5 * 18, 84 + var4 * 18)));
         }
      }

      for(var4 = 0; var4 < 9; ++var4) {
         this.a((class_abs)(new class_abs(var1, var4, 8 + var4 * 18, 142)));
      }

   }

   protected void c(class_aaz var1) {
      var1.a(this, 0, this.g[0]);
      var1.a(this, 1, this.g[1]);
      var1.a(this, 2, this.g[2]);
      var1.a(this, 3, this.f & -16);
      var1.a(this, 4, this.h[0]);
      var1.a(this, 5, this.h[1]);
      var1.a(this, 6, this.h[2]);
      var1.a(this, 7, this.i[0]);
      var1.a(this, 8, this.i[1]);
      var1.a(this, 9, this.i[2]);
   }

   public void a(class_aaz var1) {
      super.a(var1);
      this.c(var1);
   }

   public void b() {
      super.b();

      for(int var1 = 0; var1 < this.e.size(); ++var1) {
         class_aaz var2 = (class_aaz)this.e.get(var1);
         this.c(var2);
      }

   }

   public void a(IInventory var1) {
      if(var1 == this.a) {
         ItemStack var2 = var1.a(0);
         int var3;
         if(var2 != null && var2.v()) {
            if(!this.j.E) {
               var3 = 0;

               int var4;
               for(var4 = -1; var4 <= 1; ++var4) {
                  for(int var5 = -1; var5 <= 1; ++var5) {
                     if((var4 != 0 || var5 != 0) && this.j.d(this.k.a(var5, 0, var4)) && this.j.d(this.k.a(var5, 1, var4))) {
                        if(this.j.getType(this.k.a(var5 * 2, 0, var4 * 2)).getBlock() == Blocks.X) {
                           ++var3;
                        }

                        if(this.j.getType(this.k.a(var5 * 2, 1, var4 * 2)).getBlock() == Blocks.X) {
                           ++var3;
                        }

                        if(var5 != 0 && var4 != 0) {
                           if(this.j.getType(this.k.a(var5 * 2, 0, var4)).getBlock() == Blocks.X) {
                              ++var3;
                           }

                           if(this.j.getType(this.k.a(var5 * 2, 1, var4)).getBlock() == Blocks.X) {
                              ++var3;
                           }

                           if(this.j.getType(this.k.a(var5, 0, var4 * 2)).getBlock() == Blocks.X) {
                              ++var3;
                           }

                           if(this.j.getType(this.k.a(var5, 1, var4 * 2)).getBlock() == Blocks.X) {
                              ++var3;
                           }
                        }
                     }
                  }
               }

               this.l.setSeed((long)this.f);

               for(var4 = 0; var4 < 3; ++var4) {
                  this.g[var4] = class_agn.a(this.l, var4, var3, var2);
                  this.h[var4] = -1;
                  this.i[var4] = -1;
                  if(this.g[var4] < var4 + 1) {
                     this.g[var4] = 0;
                  }
               }

               for(var4 = 0; var4 < 3; ++var4) {
                  if(this.g[var4] > 0) {
                     List var7 = this.a(var2, var4, this.g[var4]);
                     if(var7 != null && !var7.isEmpty()) {
                        class_ago var6 = (class_ago)var7.get(this.l.nextInt(var7.size()));
                        this.h[var4] = Enchantment.b(var6.b);
                        this.i[var4] = var6.c;
                     }
                  }
               }

               this.b();
            }
         } else {
            for(var3 = 0; var3 < 3; ++var3) {
               this.g[var3] = 0;
               this.h[var3] = -1;
               this.i[var3] = -1;
            }
         }
      }

   }

   public boolean a(EntityHuman var1, int var2) {
      ItemStack var3 = this.a.a(0);
      ItemStack var4 = this.a.a(1);
      int var5 = var2 + 1;
      if((var4 == null || var4.b < var5) && !var1.abilities.d) {
         return false;
      } else if(this.g[var2] <= 0 || var3 == null || (var1.bK < var5 || var1.bK < this.g[var2]) && !var1.abilities.d) {
         return false;
      } else {
         if(!this.j.E) {
            List var6 = this.a(var3, var2, this.g[var2]);
            boolean var7 = var3.b() == Items.aS;
            if(var6 != null) {
               var1.b(var5);
               if(var7) {
                  var3.a((Item)Items.cn);
               }

               for(int var8 = 0; var8 < var6.size(); ++var8) {
                  class_ago var9 = (class_ago)var6.get(var8);
                  if(var7) {
                     Items.cn.a(var3, var9);
                  } else {
                     var3.a(var9.b, var9.c);
                  }
               }

               if(!var1.abilities.d) {
                  var4.b -= var5;
                  if(var4.b <= 0) {
                     this.a.a(1, (ItemStack)null);
                  }
               }

               var1.b(StatisticList.Y);
               this.a.v_();
               this.f = var1.cP();
               this.a(this.a);
            }
         }

         return true;
      }
   }

   private List a(ItemStack var1, int var2, int var3) {
      this.l.setSeed((long)(this.f + var2));
      List var4 = class_agn.b(this.l, var1, var3, false);
      if(var1.b() == Items.aS && var4.size() > 1) {
         var4.remove(this.l.nextInt(var4.size()));
      }

      return var4;
   }

   public void b(EntityHuman var1) {
      super.b(var1);
      if(!this.j.E) {
         for(int var2 = 0; var2 < this.a.u_(); ++var2) {
            ItemStack var3 = this.a.b(var2);
            if(var3 != null) {
               var1.a(var3, false);
            }
         }

      }
   }

   public boolean a(EntityHuman var1) {
      return this.j.getType(this.k).getBlock() != Blocks.bC?false:var1.e((double)this.k.p() + 0.5D, (double)this.k.q() + 0.5D, (double)this.k.r() + 0.5D) <= 64.0D;
   }

   public ItemStack b(EntityHuman var1, int var2) {
      ItemStack var3 = null;
      class_abs var4 = (class_abs)this.c.get(var2);
      if(var4 != null && var4.e()) {
         ItemStack var5 = var4.d();
         var3 = var5.k();
         if(var2 == 0) {
            if(!this.a(var5, 2, 38, true)) {
               return null;
            }
         } else if(var2 == 1) {
            if(!this.a(var5, 2, 38, true)) {
               return null;
            }
         } else if(var5.b() == Items.bd && EnumColor.a(var5.i()) == EnumColor.BLUE) {
            if(!this.a(var5, 1, 2, true)) {
               return null;
            }
         } else {
            if(((class_abs)this.c.get(0)).e() || !((class_abs)this.c.get(0)).a(var5)) {
               return null;
            }

            if(var5.n() && var5.b == 1) {
               ((class_abs)this.c.get(0)).d(var5.k());
               var5.b = 0;
            } else if(var5.b >= 1) {
               ((class_abs)this.c.get(0)).d(new ItemStack(var5.b(), 1, var5.i()));
               --var5.b;
            }
         }

         if(var5.b == 0) {
            var4.d((ItemStack)null);
         } else {
            var4.f();
         }

         if(var5.b == var3.b) {
            return null;
         }

         var4.a(var1, var5);
      }

      return var3;
   }
}
