package net.minecraft.server;

import net.minecraft.server.DamageSource;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Vec3D;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_wr;
import net.minecraft.server.class_ws;
import net.minecraft.server.class_xj;

public interface class_xd {
   boolean a();

   void b();

   void c();

   void a(class_ws var1, BlockPosition var2, DamageSource var3, EntityHuman var4);

   void d();

   void e();

   float f();

   float h();

   class_xj i();

   Vec3D g();

   float a(class_wr var1, DamageSource var2, float var3);
}
