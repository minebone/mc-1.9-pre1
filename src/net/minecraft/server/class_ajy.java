package net.minecraft.server;

import java.util.List;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_akr;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_zl;

public abstract class class_ajy extends class_akr {
   public static final class_arm a = class_arm.a("powered");
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.3125D, 0.875D, 0.375D, 0.6875D, 1.0D, 0.625D);
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.3125D, 0.0D, 0.375D, 0.6875D, 0.125D, 0.625D);
   protected static final AxisAlignedBB d = new AxisAlignedBB(0.3125D, 0.375D, 0.875D, 0.6875D, 0.625D, 1.0D);
   protected static final AxisAlignedBB e = new AxisAlignedBB(0.3125D, 0.375D, 0.0D, 0.6875D, 0.625D, 0.125D);
   protected static final AxisAlignedBB f = new AxisAlignedBB(0.875D, 0.375D, 0.3125D, 1.0D, 0.625D, 0.6875D);
   protected static final AxisAlignedBB g = new AxisAlignedBB(0.0D, 0.375D, 0.3125D, 0.125D, 0.625D, 0.6875D);
   protected static final AxisAlignedBB B = new AxisAlignedBB(0.3125D, 0.9375D, 0.375D, 0.6875D, 1.0D, 0.625D);
   protected static final AxisAlignedBB C = new AxisAlignedBB(0.3125D, 0.0D, 0.375D, 0.6875D, 0.0625D, 0.625D);
   protected static final AxisAlignedBB D = new AxisAlignedBB(0.3125D, 0.375D, 0.9375D, 0.6875D, 0.625D, 1.0D);
   protected static final AxisAlignedBB E = new AxisAlignedBB(0.3125D, 0.375D, 0.0D, 0.6875D, 0.625D, 0.0625D);
   protected static final AxisAlignedBB F = new AxisAlignedBB(0.9375D, 0.375D, 0.3125D, 1.0D, 0.625D, 0.6875D);
   protected static final AxisAlignedBB G = new AxisAlignedBB(0.0D, 0.375D, 0.3125D, 0.0625D, 0.625D, 0.6875D);
   private final boolean I;

   protected class_ajy(boolean var1) {
      super(Material.q);
      this.w(this.A.b().set(H, EnumDirection.NORTH).set(a, Boolean.valueOf(false)));
      this.a(true);
      this.a(CreativeModeTab.d);
      this.I = var1;
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public int a(World var1) {
      return this.I?30:20;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(World var1, BlockPosition var2, EnumDirection var3) {
      return a(var1, var2, var3.d());
   }

   public boolean a(World var1, BlockPosition var2) {
      EnumDirection[] var3 = EnumDirection.values();
      int var4 = var3.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         EnumDirection var6 = var3[var5];
         if(a(var1, var2, var6)) {
            return true;
         }
      }

      return false;
   }

   protected static boolean a(World var0, BlockPosition var1, EnumDirection var2) {
      BlockPosition var3 = var1.a(var2);
      return var2 == EnumDirection.DOWN?var0.getType(var3).q():var0.getType(var3).l();
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return a(var1, var2, var3.d())?this.u().set(H, var3).set(a, Boolean.valueOf(false)):this.u().set(H, EnumDirection.DOWN).set(a, Boolean.valueOf(false));
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(this.e(var1, var2, var3) && !a(var1, var2, ((EnumDirection)var3.get(H)).d())) {
         this.b(var1, var2, var3, 0);
         var1.g(var2);
      }

   }

   private boolean e(World var1, BlockPosition var2, IBlockData var3) {
      if(this.a(var1, var2)) {
         return true;
      } else {
         this.b(var1, var2, var3, 0);
         var1.g(var2);
         return false;
      }
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      EnumDirection var4 = (EnumDirection)var1.get(H);
      boolean var5 = ((Boolean)var1.get(a)).booleanValue();
      switch(class_ajy.SyntheticClass_1.a[var4.ordinal()]) {
      case 1:
         return var5?G:g;
      case 2:
         return var5?F:f;
      case 3:
         return var5?E:e;
      case 4:
      default:
         return var5?D:d;
      case 5:
         return var5?C:c;
      case 6:
         return var5?B:b;
      }
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(((Boolean)var3.get(a)).booleanValue()) {
         return true;
      } else {
         var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Boolean.valueOf(true)), 3);
         var1.b(var2, var2);
         this.a(var4, var1, var2);
         this.c(var1, var2, (EnumDirection)var3.get(H));
         var1.a((BlockPosition)var2, (Block)this, this.a(var1));
         return true;
      }
   }

   protected abstract void a(EntityHuman var1, World var2, BlockPosition var3);

   protected abstract void b(World var1, BlockPosition var2);

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      if(((Boolean)var3.get(a)).booleanValue()) {
         this.c(var1, var2, (EnumDirection)var3.get(H));
      }

      super.b(var1, var2, var3);
   }

   public int b(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return ((Boolean)var1.get(a)).booleanValue()?15:0;
   }

   public int c(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return !((Boolean)var1.get(a)).booleanValue()?0:(var1.get(H) == var4?15:0);
   }

   public boolean g(IBlockData var1) {
      return true;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Random var4) {
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(!var1.E) {
         if(((Boolean)var3.get(a)).booleanValue()) {
            if(this.I) {
               this.e(var3, var1, var2);
            } else {
               var1.a(var2, var3.set(a, Boolean.valueOf(false)));
               this.c(var1, var2, (EnumDirection)var3.get(H));
               this.b(var1, var2);
               var1.b(var2, var2);
            }

         }
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Entity var4) {
      if(!var1.E) {
         if(this.I) {
            if(!((Boolean)var3.get(a)).booleanValue()) {
               this.e(var3, var1, var2);
            }
         }
      }
   }

   private void e(IBlockData var1, World var2, BlockPosition var3) {
      List var4 = var2.a(class_zl.class, var1.c(var2, var3).a(var3));
      boolean var5 = !var4.isEmpty();
      boolean var6 = ((Boolean)var1.get(a)).booleanValue();
      if(var5 && !var6) {
         var2.a(var3, var1.set(a, Boolean.valueOf(true)));
         this.c(var2, var3, (EnumDirection)var1.get(H));
         var2.b(var3, var3);
         this.a((EntityHuman)null, (World)var2, (BlockPosition)var3);
      }

      if(!var5 && var6) {
         var2.a(var3, var1.set(a, Boolean.valueOf(false)));
         this.c(var2, var3, (EnumDirection)var1.get(H));
         var2.b(var3, var3);
         this.b(var2, var3);
      }

      if(var5) {
         var2.a((BlockPosition)(new BlockPosition(var3)), (Block)this, this.a(var2));
      }

   }

   private void c(World var1, BlockPosition var2, EnumDirection var3) {
      var1.d(var2, this);
      var1.d(var2.a(var3.d()), this);
   }

   public IBlockData a(int var1) {
      EnumDirection var2;
      switch(var1 & 7) {
      case 0:
         var2 = EnumDirection.DOWN;
         break;
      case 1:
         var2 = EnumDirection.EAST;
         break;
      case 2:
         var2 = EnumDirection.WEST;
         break;
      case 3:
         var2 = EnumDirection.SOUTH;
         break;
      case 4:
         var2 = EnumDirection.NORTH;
         break;
      case 5:
      default:
         var2 = EnumDirection.UP;
      }

      return this.u().set(H, var2).set(a, Boolean.valueOf((var1 & 8) > 0));
   }

   public int e(IBlockData var1) {
      int var2;
      switch(class_ajy.SyntheticClass_1.a[((EnumDirection)var1.get(H)).ordinal()]) {
      case 1:
         var2 = 1;
         break;
      case 2:
         var2 = 2;
         break;
      case 3:
         var2 = 3;
         break;
      case 4:
         var2 = 4;
         break;
      case 5:
      default:
         var2 = 5;
         break;
      case 6:
         var2 = 0;
      }

      if(((Boolean)var1.get(a)).booleanValue()) {
         var2 |= 8;
      }

      return var2;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(H, var2.a((EnumDirection)var1.get(H)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(H)));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{H, a});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[EnumDirection.values().length];

      static {
         try {
            a[EnumDirection.EAST.ordinal()] = 1;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[EnumDirection.WEST.ordinal()] = 2;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[EnumDirection.SOUTH.ordinal()] = 3;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[EnumDirection.NORTH.ordinal()] = 4;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.UP.ordinal()] = 5;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.DOWN.ordinal()] = 6;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
