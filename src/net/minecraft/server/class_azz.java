package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.class_azw;
import net.minecraft.server.class_azx;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_kk;

public class class_azz extends class_azw {
   protected final class_kk a;

   public class_azz(class_kk var1, int var2, int var3, class_baq[] var4) {
      super(var2, var3, var4);
      this.a = var1;
   }

   public void a(Collection var1, Random var2, class_azy var3) {
      class_azx var4 = var3.e().a(this.a);
      List var5 = var4.a(var2, var3);
      var1.addAll(var5);
   }

   protected void a(JsonObject var1, JsonSerializationContext var2) {
      var1.addProperty("name", this.a.toString());
   }

   public static class_azz a(JsonObject var0, JsonDeserializationContext var1, int var2, int var3, class_baq[] var4) {
      class_kk var5 = new class_kk(ChatDeserializer.h(var0, "name"));
      return new class_azz(var5, var2, var3, var4);
   }
}
