package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_auc;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_aug extends class_auc {
   public boolean b(World var1, Random var2, BlockPosition var3) {
      if(!var1.d(var3)) {
         return false;
      } else if(var1.getType(var3.a()).getBlock() != Blocks.aV) {
         return false;
      } else {
         var1.a((BlockPosition)var3, (IBlockData)Blocks.aX.u(), 2);

         for(int var4 = 0; var4 < 1500; ++var4) {
            BlockPosition var5 = var3.a(var2.nextInt(8) - var2.nextInt(8), -var2.nextInt(12), var2.nextInt(8) - var2.nextInt(8));
            if(var1.getType(var5).getMaterial() == Material.a) {
               int var6 = 0;
               EnumDirection[] var7 = EnumDirection.values();
               int var8 = var7.length;

               for(int var9 = 0; var9 < var8; ++var9) {
                  EnumDirection var10 = var7[var9];
                  if(var1.getType(var5.a(var10)).getBlock() == Blocks.aX) {
                     ++var6;
                  }

                  if(var6 > 1) {
                     break;
                  }
               }

               if(var6 == 1) {
                  var1.a((BlockPosition)var5, (IBlockData)Blocks.aX.u(), 2);
               }
            }
         }

         return true;
      }
   }
}
