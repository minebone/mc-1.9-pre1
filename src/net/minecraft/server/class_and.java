package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Item;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;

public class class_and extends Block {
   public class_and() {
      super(Material.e);
      this.a(CreativeModeTab.b);
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Item.a(Blocks.Z);
   }

   public MaterialMapColor r(IBlockData var1) {
      return MaterialMapColor.E;
   }
}
