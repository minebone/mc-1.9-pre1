package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.minecraft.server.Entity;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_aje;
import net.minecraft.server.class_asz;
import net.minecraft.server.class_ava;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ws;

public enum class_asy {
   a {
      public void a(WorldServer var1, class_asz var2, List var3, int var4, BlockPosition var5) {
         BlockPosition var6 = new BlockPosition(0, 128, 0);
         Iterator var7 = var3.iterator();

         while(var7.hasNext()) {
            class_ws var8 = (class_ws)var7.next();
            var8.a(var6);
         }

         var2.a(b);
      }
   },
   b {
      public void a(WorldServer var1, class_asz var2, List var3, int var4, BlockPosition var5) {
         if(var4 < 100) {
            if(var4 == 0 || var4 == 50 || var4 == 51 || var4 == 52 || var4 >= 95) {
               var1.b(3001, new BlockPosition(0, 128, 0), 0);
            }
         } else {
            var2.a(c);
         }

      }
   },
   c {
      public void a(WorldServer var1, class_asz var2, List var3, int var4, BlockPosition var5) {
         byte var6 = 40;
         boolean var7 = var4 % var6 == 0;
         boolean var8 = var4 % var6 == var6 - 1;
         if(var7 || var8) {
            class_ava.class_a_in_class_ava[] var9 = class_aje.a(var1);
            int var10 = var4 / var6;
            if(var10 < var9.length) {
               class_ava.class_a_in_class_ava var11 = var9[var10];
               if(var7) {
                  Iterator var12 = var3.iterator();

                  while(var12.hasNext()) {
                     class_ws var13 = (class_ws)var12.next();
                     var13.a(new BlockPosition(var11.a(), var11.d() + 1, var11.b()));
                  }
               } else {
                  byte var15 = 10;
                  Iterator var16 = BlockPosition.b(new BlockPosition(var11.a() - var15, var11.d() - var15, var11.b() - var15), new BlockPosition(var11.a() + var15, var11.d() + var15, var11.b() + var15)).iterator();

                  while(var16.hasNext()) {
                     BlockPosition.class_a_in_class_cj var14 = (BlockPosition.class_a_in_class_cj)var16.next();
                     var1.g(var14);
                  }

                  var1.a((Entity)null, (double)((float)var11.a() + 0.5F), (double)var11.d(), (double)((float)var11.b() + 0.5F), 5.0F, true);
                  class_ava var17 = new class_ava();
                  var17.a(var11);
                  var17.a(true);
                  var17.a(new BlockPosition(0, 128, 0));
                  var17.b(var1, new Random(), new BlockPosition(var11.a(), 45, var11.b()));
               }
            } else if(var7) {
               var2.a(d);
            }
         }

      }
   },
   d {
      public void a(WorldServer var1, class_asz var2, List var3, int var4, BlockPosition var5) {
         Iterator var6;
         class_ws var7;
         if(var4 >= 100) {
            var2.a(e);
            var2.f();
            var6 = var3.iterator();

            while(var6.hasNext()) {
               var7 = (class_ws)var6.next();
               var7.a((BlockPosition)null);
               var1.a(var7, var7.locX, var7.locY, var7.locZ, 6.0F, false);
               var7.S();
            }
         } else if(var4 >= 80) {
            var1.b(3001, new BlockPosition(0, 128, 0), 0);
         } else if(var4 == 0) {
            var6 = var3.iterator();

            while(var6.hasNext()) {
               var7 = (class_ws)var6.next();
               var7.a(new BlockPosition(0, 128, 0));
            }
         } else if(var4 < 5) {
            var1.b(3001, new BlockPosition(0, 128, 0), 0);
         }

      }
   },
   e {
      public void a(WorldServer var1, class_asz var2, List var3, int var4, BlockPosition var5) {
      }
   };

   private class_asy() {
   }

   public abstract void a(WorldServer var1, class_asz var2, List var3, int var4, BlockPosition var5);

   // $FF: synthetic method
   class_asy(Object var3) {
      this();
   }
}
