package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.Entity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;

public class CommandKill extends CommandAbstract {
   public String c() {
      return "kill";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.kill.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length == 0) {
         EntityPlayer var5 = a(var2);
         var5.P();
         a(var2, this, "commands.kill.successful", new Object[]{var5.i_()});
      } else {
         Entity var4 = b(var1, var2, var3[0]);
         var4.P();
         a(var2, this, "commands.kill.successful", new Object[]{var4.i_()});
      }
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 0;
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, var1.J()):Collections.emptyList();
   }
}
