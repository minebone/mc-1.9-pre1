package net.minecraft.server;

import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.class_ox;

public class class_ph implements class_ox {
   public int a() {
      return 110;
   }

   public NBTTagCompound a(NBTTagCompound var1) {
      if("EntityHorse".equals(var1.l("id")) && !var1.b("SaddleItem", 10) && var1.p("Saddle")) {
         NBTTagCompound var2 = new NBTTagCompound();
         var2.a("id", "minecraft:saddle");
         var2.a("Count", (byte)1);
         var2.a("Damage", (short)0);
         var1.a((String)"SaddleItem", (NBTTag)var2);
         var1.q("Saddle");
      }

      return var1;
   }
}
