package net.minecraft.server;

import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;

public class PlayerAbilities {
   public boolean a;
   public boolean b;
   public boolean c;
   public boolean d;
   public boolean e = true;
   private float f = 0.05F;
   private float g = 0.1F;

   public void a(NBTTagCompound var1) {
      NBTTagCompound var2 = new NBTTagCompound();
      var2.a("invulnerable", this.a);
      var2.a("flying", this.b);
      var2.a("mayfly", this.c);
      var2.a("instabuild", this.d);
      var2.a("mayBuild", this.e);
      var2.a("flySpeed", this.f);
      var2.a("walkSpeed", this.g);
      var1.a((String)"abilities", (NBTTag)var2);
   }

   public void b(NBTTagCompound var1) {
      if(var1.b("abilities", 10)) {
         NBTTagCompound var2 = var1.o("abilities");
         this.a = var2.p("invulnerable");
         this.b = var2.p("flying");
         this.c = var2.p("mayfly");
         this.d = var2.p("instabuild");
         if(var2.b("flySpeed", 99)) {
            this.f = var2.j("flySpeed");
            this.g = var2.j("walkSpeed");
         }

         if(var2.b("mayBuild", 1)) {
            this.e = var2.p("mayBuild");
         }
      }

   }

   public float a() {
      return this.f;
   }

   public float b() {
      return this.g;
   }
}
