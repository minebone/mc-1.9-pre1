package net.minecraft.server;

import java.io.IOException;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayInListener;
import org.apache.commons.lang3.StringUtils;

public class class_ii implements Packet {
   private String a;
   private boolean b;
   private BlockPosition c;

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.c(32767);
      this.b = var1.readBoolean();
      boolean var2 = var1.readBoolean();
      if(var2) {
         this.c = var1.e();
      }

   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.a(StringUtils.substring(this.a, 0, 32767));
      var1.writeBoolean(this.b);
      boolean var2 = this.c != null;
      var1.writeBoolean(var2);
      if(var2) {
         var1.a(this.c);
      }

   }

   public void a(PacketPlayInListener var1) {
      var1.a(this);
   }

   public String a() {
      return this.a;
   }

   public BlockPosition b() {
      return this.c;
   }

   public boolean c() {
      return this.b;
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketPlayInListener)var1);
   }
}
