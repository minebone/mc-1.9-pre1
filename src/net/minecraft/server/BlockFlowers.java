package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import java.util.Collection;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.class_ajx;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.class_or;

public abstract class BlockFlowers extends class_ajx {
   protected BlockStateEnum a;

   protected BlockFlowers() {
      this.w(this.A.b().set(this.g(), this.e() == BlockFlowers.class_b_in_class_all.RED?BlockFlowers.EnumFlowerVarient.POPPY:BlockFlowers.EnumFlowerVarient.DANDELION));
   }

   public int d(IBlockData var1) {
      return ((BlockFlowers.EnumFlowerVarient)var1.get(this.g())).b();
   }

   public IBlockData a(int var1) {
      return this.u().set(this.g(), BlockFlowers.EnumFlowerVarient.a(this.e(), var1));
   }

   public abstract BlockFlowers.class_b_in_class_all e();

   public IBlockState g() {
      if(this.a == null) {
         this.a = BlockStateEnum.a("type", BlockFlowers.EnumFlowerVarient.class, new Predicate() {
            public boolean a(BlockFlowers.EnumFlowerVarient var1) {
               return var1.a() == BlockFlowers.this.e();
            }

            // $FF: synthetic method
            public boolean apply(Object var1) {
               return this.a((BlockFlowers.EnumFlowerVarient)var1);
            }
         });
      }

      return this.a;
   }

   public int e(IBlockData var1) {
      return ((BlockFlowers.EnumFlowerVarient)var1.get(this.g())).b();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{this.g()});
   }

   public static enum EnumFlowerVarient implements class_or {
      DANDELION(BlockFlowers.class_b_in_class_all.YELLOW, 0, "dandelion"),
      POPPY(BlockFlowers.class_b_in_class_all.RED, 0, "poppy"),
      BLUE_ORCHID(BlockFlowers.class_b_in_class_all.RED, 1, "blue_orchid", "blueOrchid"),
      ALLIUM(BlockFlowers.class_b_in_class_all.RED, 2, "allium"),
      HOUSTONIA(BlockFlowers.class_b_in_class_all.RED, 3, "houstonia"),
      RED_TULIP(BlockFlowers.class_b_in_class_all.RED, 4, "red_tulip", "tulipRed"),
      ORANGE_TULIP(BlockFlowers.class_b_in_class_all.RED, 5, "orange_tulip", "tulipOrange"),
      WHITE_TULIP(BlockFlowers.class_b_in_class_all.RED, 6, "white_tulip", "tulipWhite"),
      PINK_TULIP(BlockFlowers.class_b_in_class_all.RED, 7, "pink_tulip", "tulipPink"),
      OXEYE_DAISY(BlockFlowers.class_b_in_class_all.RED, 8, "oxeye_daisy", "oxeyeDaisy");

      private static final BlockFlowers.EnumFlowerVarient[][] k = new BlockFlowers.EnumFlowerVarient[BlockFlowers.class_b_in_class_all.values().length][];
      private final BlockFlowers.class_b_in_class_all l;
      private final int m;
      private final String n;
      private final String o;

      private EnumFlowerVarient(BlockFlowers.class_b_in_class_all var3, int var4, String var5) {
         this(var3, var4, var5, var5);
      }

      private EnumFlowerVarient(BlockFlowers.class_b_in_class_all var3, int var4, String var5, String var6) {
         this.l = var3;
         this.m = var4;
         this.n = var5;
         this.o = var6;
      }

      public BlockFlowers.class_b_in_class_all a() {
         return this.l;
      }

      public int b() {
         return this.m;
      }

      public static BlockFlowers.EnumFlowerVarient a(BlockFlowers.class_b_in_class_all var0, int var1) {
         BlockFlowers.EnumFlowerVarient[] var2 = k[var0.ordinal()];
         if(var1 < 0 || var1 >= var2.length) {
            var1 = 0;
         }

         return var2[var1];
      }

      public String toString() {
         return this.n;
      }

      public String m() {
         return this.n;
      }

      public String d() {
         return this.o;
      }

      static {
         BlockFlowers.class_b_in_class_all[] var0 = BlockFlowers.class_b_in_class_all.values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            final BlockFlowers.class_b_in_class_all var3 = var0[var2];
            Collection var4 = Collections2.filter(Lists.newArrayList((Object[])values()), new Predicate() {
               public boolean a(BlockFlowers.EnumFlowerVarient var1) {
                  return var1.a() == var3;
               }

               // $FF: synthetic method
               public boolean apply(Object var1) {
                  return this.a((BlockFlowers.EnumFlowerVarient)var1);
               }
            });
            k[var3.ordinal()] = (BlockFlowers.EnumFlowerVarient[])var4.toArray(new BlockFlowers.EnumFlowerVarient[var4.size()]);
         }

      }
   }

   public static enum class_b_in_class_all {
      YELLOW,
      RED;

      public BlockFlowers a() {
         return this == YELLOW?Blocks.N:Blocks.O;
      }
   }
}
