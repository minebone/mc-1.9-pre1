package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.class_or;

public class BlockSmoothBrick extends Block {
   public static final BlockStateEnum a = BlockStateEnum.a("variant", BlockSmoothBrick.EnumStonebrickType.class);
   public static final int b = BlockSmoothBrick.EnumStonebrickType.DEFAULT.a();
   public static final int c = BlockSmoothBrick.EnumStonebrickType.MOSSY.a();
   public static final int d = BlockSmoothBrick.EnumStonebrickType.CRACKED.a();
   public static final int e = BlockSmoothBrick.EnumStonebrickType.CHISELED.a();

   public BlockSmoothBrick() {
      super(Material.e);
      this.w(this.A.b().set(a, BlockSmoothBrick.EnumStonebrickType.DEFAULT));
      this.a(CreativeModeTab.b);
   }

   public int d(IBlockData var1) {
      return ((BlockSmoothBrick.EnumStonebrickType)var1.get(a)).a();
   }

   public IBlockData a(int var1) {
      return this.u().set(a, BlockSmoothBrick.EnumStonebrickType.a(var1));
   }

   public int e(IBlockData var1) {
      return ((BlockSmoothBrick.EnumStonebrickType)var1.get(a)).a();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   public static enum EnumStonebrickType implements class_or {
      DEFAULT(0, "stonebrick", "default"),
      MOSSY(1, "mossy_stonebrick", "mossy"),
      CRACKED(2, "cracked_stonebrick", "cracked"),
      CHISELED(3, "chiseled_stonebrick", "chiseled");

      private static final BlockSmoothBrick.EnumStonebrickType[] e = new BlockSmoothBrick.EnumStonebrickType[values().length];
      private final int f;
      private final String g;
      private final String h;

      private EnumStonebrickType(int var3, String var4, String var5) {
         this.f = var3;
         this.g = var4;
         this.h = var5;
      }

      public int a() {
         return this.f;
      }

      public String toString() {
         return this.g;
      }

      public static BlockSmoothBrick.EnumStonebrickType a(int var0) {
         if(var0 < 0 || var0 >= e.length) {
            var0 = 0;
         }

         return e[var0];
      }

      public String m() {
         return this.g;
      }

      public String c() {
         return this.h;
      }

      static {
         BlockSmoothBrick.EnumStonebrickType[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            BlockSmoothBrick.EnumStonebrickType var3 = var0[var2];
            e[var3.a()] = var3;
         }

      }
   }
}
