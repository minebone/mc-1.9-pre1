package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Callable;
import net.minecraft.server.Chunk;
import net.minecraft.server.CrashReport;
import net.minecraft.server.CrashReportSystemDetails;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityFallingBlock;
import net.minecraft.server.EntityFireball;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.EntityMinecartAbstract;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_aaa;
import net.minecraft.server.class_aab;
import net.minecraft.server.EntityBoat;
import net.minecraft.server.class_e;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutAttachEntity;
import net.minecraft.server.PacketPlayOutPlayerRefresh;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.class_lt;
import net.minecraft.server.class_oh;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_ro;
import net.minecraft.server.class_rp;
import net.minecraft.server.class_rw;
import net.minecraft.server.class_vt;
import net.minecraft.server.class_wf;
import net.minecraft.server.class_ws;
import net.minecraft.server.class_wt;
import net.minecraft.server.class_xn;
import net.minecraft.server.class_xp;
import net.minecraft.server.class_xq;
import net.minecraft.server.class_xv;
import net.minecraft.server.class_yc;
import net.minecraft.server.class_yd;
import net.minecraft.server.class_zl;
import net.minecraft.server.class_zn;
import net.minecraft.server.class_zp;
import net.minecraft.server.class_zt;
import net.minecraft.server.class_zu;
import net.minecraft.server.class_zv;
import net.minecraft.server.class_zy;
import net.minecraft.server.class_zz;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EntityTracker {
   private static final Logger a = LogManager.getLogger();
   private final WorldServer b;
   private Set c = Sets.newHashSet();
   private class_oh d = new class_oh();
   private int e;

   public EntityTracker(WorldServer var1) {
      this.b = var1;
      this.e = var1.u().getPlayerList().d();
   }

   public static long a(double var0) {
      return MathHelper.d(var0 * 4096.0D);
   }

   public void a(Entity var1) {
      if(var1 instanceof EntityPlayer) {
         this.a(var1, 512, 2);
         EntityPlayer var2 = (EntityPlayer)var1;
         Iterator var3 = this.c.iterator();

         while(var3.hasNext()) {
            class_lt var4 = (class_lt)var3.next();
            if(var4.b() != var2) {
               var4.b(var2);
            }
         }
      } else if(var1 instanceof class_xv) {
         this.a(var1, 64, 5, true);
      } else if(var1 instanceof class_zl) {
         this.a(var1, 64, 20, false);
      } else if(var1 instanceof class_zu) {
         this.a(var1, 64, 10, false);
      } else if(var1 instanceof EntityFireball) {
         this.a(var1, 64, 10, false);
      } else if(var1 instanceof class_zv) {
         this.a(var1, 64, 10, true);
      } else if(var1 instanceof class_zz) {
         this.a(var1, 64, 10, true);
      } else if(var1 instanceof class_zn) {
         this.a(var1, 64, 4, true);
      } else if(var1 instanceof class_zy) {
         this.a(var1, 64, 10, true);
      } else if(var1 instanceof class_aab) {
         this.a(var1, 64, 10, true);
      } else if(var1 instanceof class_aaa) {
         this.a(var1, 64, 10, true);
      } else if(var1 instanceof class_zp) {
         this.a(var1, 64, 10, true);
      } else if(var1 instanceof class_yc) {
         this.a(var1, 64, 20, true);
      } else if(var1 instanceof EntityMinecartAbstract) {
         this.a(var1, 80, 3, true);
      } else if(var1 instanceof EntityBoat) {
         this.a(var1, 80, 3, true);
      } else if(var1 instanceof class_wf) {
         this.a(var1, 64, 3, true);
      } else if(var1 instanceof class_xn) {
         this.a(var1, 80, 3, false);
      } else if(var1 instanceof class_zt) {
         this.a(var1, 80, 3, true);
      } else if(var1 instanceof class_vt) {
         this.a(var1, 80, 3, false);
      } else if(var1 instanceof class_wt) {
         this.a(var1, 160, 3, true);
      } else if(var1 instanceof class_rp) {
         this.a(var1, 80, 3, true);
      } else if(var1 instanceof class_yd) {
         this.a(var1, 160, 10, true);
      } else if(var1 instanceof EntityFallingBlock) {
         this.a(var1, 160, 20, true);
      } else if(var1 instanceof class_xq) {
         this.a(var1, 160, Integer.MAX_VALUE, false);
      } else if(var1 instanceof class_xp) {
         this.a(var1, 160, 3, true);
      } else if(var1 instanceof class_rw) {
         this.a(var1, 160, 20, true);
      } else if(var1 instanceof class_ro) {
         this.a(var1, 160, Integer.MAX_VALUE, true);
      } else if(var1 instanceof class_ws) {
         this.a(var1, 256, Integer.MAX_VALUE, false);
      }

   }

   public void a(Entity var1, int var2, int var3) {
      this.a(var1, var2, var3, false);
   }

   public void a(Entity var1, int var2, final int var3, boolean var4) {
      try {
         if(this.d.b(var1.getId())) {
            throw new IllegalStateException("Entity is already tracked!");
         }

         class_lt var5 = new class_lt(var1, var2, this.e, var3, var4);
         this.c.add(var5);
         this.d.a(var1.getId(), var5);
         var5.b(this.b.i);
      } catch (Throwable var10) {
         CrashReport var6 = CrashReport.a(var10, "Adding entity to track");
         CrashReportSystemDetails var7 = var6.a("Entity To Track");
         var7.a((String)"Tracking range", (Object)(var2 + " blocks"));
         var7.a("Update interval", new Callable() {
            public String a() throws Exception {
               String var1 = "Once per " + var3 + " ticks";
               if(var3 == Integer.MAX_VALUE) {
                  var1 = "Maximum (" + var1 + ")";
               }

               return var1;
            }

            // $FF: synthetic method
            public Object call() throws Exception {
               return this.a();
            }
         });
         var1.a(var7);
         ((class_lt)this.d.a(var1.getId())).b().a(var6.a("Entity That Is Already Tracked"));

         try {
            throw new class_e(var6);
         } catch (class_e var9) {
            a.error((String)"\"Silently\" catching entity tracking error.", (Throwable)var9);
         }
      }

   }

   public void b(Entity var1) {
      if(var1 instanceof EntityPlayer) {
         EntityPlayer var2 = (EntityPlayer)var1;
         Iterator var3 = this.c.iterator();

         while(var3.hasNext()) {
            class_lt var4 = (class_lt)var3.next();
            var4.a(var2);
         }
      }

      class_lt var5 = (class_lt)this.d.d(var1.getId());
      if(var5 != null) {
         this.c.remove(var5);
         var5.a();
      }

   }

   public void a() {
      ArrayList var1 = Lists.newArrayList();
      Iterator var2 = this.c.iterator();

      while(var2.hasNext()) {
         class_lt var3 = (class_lt)var2.next();
         var3.a(this.b.i);
         if(var3.b) {
            Entity var4 = var3.b();
            if(var4 instanceof EntityPlayer) {
               var1.add((EntityPlayer)var4);
            }
         }
      }

      for(int var6 = 0; var6 < var1.size(); ++var6) {
         EntityPlayer var7 = (EntityPlayer)var1.get(var6);
         Iterator var8 = this.c.iterator();

         while(var8.hasNext()) {
            class_lt var5 = (class_lt)var8.next();
            if(var5.b() != var7) {
               var5.b(var7);
            }
         }
      }

   }

   public void a(EntityPlayer var1) {
      Iterator var2 = this.c.iterator();

      while(var2.hasNext()) {
         class_lt var3 = (class_lt)var2.next();
         if(var3.b() == var1) {
            var3.b(this.b.i);
         } else {
            var3.b(var1);
         }
      }

   }

   public void a(Entity var1, Packet var2) {
      class_lt var3 = (class_lt)this.d.a(var1.getId());
      if(var3 != null) {
         var3.a(var2);
      }

   }

   public void b(Entity var1, Packet var2) {
      class_lt var3 = (class_lt)this.d.a(var1.getId());
      if(var3 != null) {
         var3.b(var2);
      }

   }

   public void b(EntityPlayer var1) {
      Iterator var2 = this.c.iterator();

      while(var2.hasNext()) {
         class_lt var3 = (class_lt)var2.next();
         var3.d(var1);
      }

   }

   public void a(EntityPlayer var1, Chunk var2) {
      ArrayList var3 = Lists.newArrayList();
      ArrayList var4 = Lists.newArrayList();
      Iterator var5 = this.c.iterator();

      while(var5.hasNext()) {
         class_lt var6 = (class_lt)var5.next();
         Entity var7 = var6.b();
         if(var7 != var1 && var7.ab == var2.posX && var7.ad == var2.posZ) {
            var6.b(var1);
            if(var7 instanceof EntityInsentient && ((EntityInsentient)var7).cQ() != null) {
               var3.add(var7);
            }

            if(!var7.bt().isEmpty()) {
               var4.add(var7);
            }
         }
      }

      Entity var8;
      if(!var3.isEmpty()) {
         var5 = var3.iterator();

         while(var5.hasNext()) {
            var8 = (Entity)var5.next();
            var1.a.a((Packet)(new PacketPlayOutAttachEntity(var8, ((EntityInsentient)var8).cQ())));
         }
      }

      if(!var4.isEmpty()) {
         var5 = var4.iterator();

         while(var5.hasNext()) {
            var8 = (Entity)var5.next();
            var1.a.a((Packet)(new PacketPlayOutPlayerRefresh(var8)));
         }
      }

   }

   public void a(int var1) {
      this.e = (var1 - 1) * 16;
      Iterator var2 = this.c.iterator();

      while(var2.hasNext()) {
         class_lt var3 = (class_lt)var2.next();
         var3.a(this.e);
      }

   }
}
