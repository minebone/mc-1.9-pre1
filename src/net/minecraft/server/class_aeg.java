package net.minecraft.server;

import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_wb;

public class class_aeg extends Item {
   public class_aeg() {
      this.j = 1;
      this.a(CreativeModeTab.e);
   }

   public boolean a(ItemStack var1, EntityHuman var2, class_rz var3, EnumHand var4) {
      if(var3 instanceof class_wb) {
         class_wb var5 = (class_wb)var3;
         if(!var5.cZ() && !var5.m_()) {
            var5.o(true);
            var5.world.a(var2, var5.locX, var5.locY, var5.locZ, class_ng.dO, EnumSoundCategory.NEUTRAL, 0.5F, 1.0F);
            --var1.b;
         }

         return true;
      } else {
         return false;
      }
   }

   public boolean a(ItemStack var1, class_rz var2, class_rz var3) {
      this.a(var1, (EntityHuman)null, var2, EnumHand.MAIN_HAND);
      return true;
   }
}
