package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BlockFlowers;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_auc;
import net.minecraft.server.BlockPosition;

public class class_aud extends class_auc {
   private BlockFlowers a;
   private IBlockData b;

   public class_aud(BlockFlowers var1, BlockFlowers.EnumFlowerVarient var2) {
      this.a(var1, var2);
   }

   public void a(BlockFlowers var1, BlockFlowers.EnumFlowerVarient var2) {
      this.a = var1;
      this.b = var1.u().set(var1.g(), var2);
   }

   public boolean b(World var1, Random var2, BlockPosition var3) {
      for(int var4 = 0; var4 < 64; ++var4) {
         BlockPosition var5 = var3.a(var2.nextInt(8) - var2.nextInt(8), var2.nextInt(4) - var2.nextInt(4), var2.nextInt(8) - var2.nextInt(8));
         if(var1.d(var5) && (!var1.s.isNotOverworld() || var5.q() < 255) && this.a.f(var1, var5, this.b)) {
            var1.a((BlockPosition)var5, (IBlockData)this.b, 2);
         }
      }

      return true;
   }
}
