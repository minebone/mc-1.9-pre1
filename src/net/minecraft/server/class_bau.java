package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import java.util.Random;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_kk;

public class class_bau implements class_baq {
   private final float a;

   public class_bau(float var1) {
      this.a = var1;
   }

   public boolean a(Random var1, class_azy var2) {
      return var1.nextFloat() < this.a;
   }

   public static class class_a_in_class_bau extends class_baq.class_a_in_class_baq {
      protected class_a_in_class_bau() {
         super(new class_kk("random_chance"), class_bau.class);
      }

      public void a(JsonObject var1, class_bau var2, JsonSerializationContext var3) {
         var1.addProperty("chance", (Number)Float.valueOf(var2.a));
      }

      public class_bau a(JsonObject var1, JsonDeserializationContext var2) {
         return new class_bau(ChatDeserializer.l(var1, "chance"));
      }

      // $FF: synthetic method
      public class_baq b(JsonObject var1, JsonDeserializationContext var2) {
         return this.a(var1, var2);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(JsonObject var1, class_baq var2, JsonSerializationContext var3) {
         this.a(var1, (class_bau)var2, var3);
      }
   }
}
