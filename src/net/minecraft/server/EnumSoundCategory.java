package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Map;
import java.util.Set;

public enum EnumSoundCategory {
   MASTER("master"),
   MUSIC("music"),
   RECORDS("record"),
   WEATHER("weather"),
   BLOCKS("block"),
   HOSTILE("hostile"),
   NEUTRAL("neutral"),
   PLAYERS("player"),
   AMBIENT("ambient"),
   VOICE("voice");

   private static final Map k = Maps.newHashMap();
   private final String l;

   private EnumSoundCategory(String var3) {
      this.l = var3;
   }

   public String a() {
      return this.l;
   }

   public static EnumSoundCategory a(String var0) {
      return (EnumSoundCategory)k.get(var0);
   }

   public static Set b() {
      return k.keySet();
   }

   static {
      EnumSoundCategory[] var0 = values();
      int var1 = var0.length;

      for(int var2 = 0; var2 < var1; ++var2) {
         EnumSoundCategory var3 = var0[var2];
         if(k.containsKey(var3.a())) {
            throw new Error("Clash in Sound Category name pools! Cannot insert " + var3);
         }

         k.put(var3.a(), var3);
      }

   }
}
