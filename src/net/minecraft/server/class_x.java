package net.minecraft.server;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.class_oo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_x extends CommandAbstract {
   private static final Logger a = LogManager.getLogger();
   private long b;
   private int c;

   public String c() {
      return "debug";
   }

   public int a() {
      return 3;
   }

   public String b(ICommandListener var1) {
      return "commands.debug.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 1) {
         throw new class_cf("commands.debug.usage", new Object[0]);
      } else {
         if(var3[0].equals("start")) {
            if(var3.length != 1) {
               throw new class_cf("commands.debug.usage", new Object[0]);
            }

            a(var2, this, "commands.debug.start", new Object[0]);
            var1.aq();
            this.b = MinecraftServer.av();
            this.c = var1.ap();
         } else {
            if(!var3[0].equals("stop")) {
               throw new class_cf("commands.debug.usage", new Object[0]);
            }

            if(var3.length != 1) {
               throw new class_cf("commands.debug.usage", new Object[0]);
            }

            if(!var1.c.a) {
               throw new class_bz("commands.debug.notStarted", new Object[0]);
            }

            long var4 = MinecraftServer.av();
            int var6 = var1.ap();
            long var7 = var4 - this.b;
            int var9 = var6 - this.c;
            this.a(var7, var9, var1);
            var1.c.a = false;
            a(var2, this, "commands.debug.stop", new Object[]{Float.valueOf((float)var7 / 1000.0F), Integer.valueOf(var9)});
         }

      }
   }

   private void a(long var1, int var3, MinecraftServer var4) {
      File var5 = new File(var4.d("debug"), "profile-results-" + (new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss")).format(new Date()) + ".txt");
      var5.getParentFile().mkdirs();

      try {
         FileWriter var6 = new FileWriter(var5);
         var6.write(this.b(var1, var3, var4));
         var6.close();
      } catch (Throwable var7) {
         a.error("Could not save profiler results to " + var5, var7);
      }

   }

   private String b(long var1, int var3, MinecraftServer var4) {
      StringBuilder var5 = new StringBuilder();
      var5.append("---- Minecraft Profiler Results ----\n");
      var5.append("// ");
      var5.append(d());
      var5.append("\n\n");
      var5.append("Time span: ").append(var1).append(" ms\n");
      var5.append("Tick span: ").append(var3).append(" ticks\n");
      var5.append("// This is approximately ").append(String.format("%.2f", new Object[]{Float.valueOf((float)var3 / ((float)var1 / 1000.0F))})).append(" ticks per second. It should be ").append(20).append(" ticks per second\n\n");
      var5.append("--- BEGIN PROFILE DUMP ---\n\n");
      this.a(0, "root", var5, var4);
      var5.append("--- END PROFILE DUMP ---\n\n");
      return var5.toString();
   }

   private void a(int var1, String var2, StringBuilder var3, MinecraftServer var4) {
      List var5 = var4.c.b(var2);
      if(var5 != null && var5.size() >= 3) {
         for(int var6 = 1; var6 < var5.size(); ++var6) {
            class_oo.class_a_in_class_oo var7 = (class_oo.class_a_in_class_oo)var5.get(var6);
            var3.append(String.format("[%02d] ", new Object[]{Integer.valueOf(var1)}));

            for(int var8 = 0; var8 < var1; ++var8) {
               var3.append("|   ");
            }

            var3.append(var7.c).append(" - ").append(String.format("%.2f", new Object[]{Double.valueOf(var7.a)})).append("%/").append(String.format("%.2f", new Object[]{Double.valueOf(var7.b)})).append("%\n");
            if(!var7.c.equals("unspecified")) {
               try {
                  this.a(var1 + 1, var2 + "." + var7.c, var3, var4);
               } catch (Exception var9) {
                  var3.append("[[ EXCEPTION ").append(var9).append(" ]]");
               }
            }
         }

      }
   }

   private static String d() {
      String[] var0 = new String[]{"Shiny numbers!", "Am I not running fast enough? :(", "I\'m working as hard as I can!", "Will I ever be good enough for you? :(", "Speedy. Zoooooom!", "Hello world", "40% better than a crash report.", "Now with extra numbers", "Now with less numbers", "Now with the same numbers", "You should add flames to things, it makes them go faster!", "Do you feel the need for... optimization?", "*cracks redstone whip*", "Maybe if you treated it better then it\'ll have more motivation to work faster! Poor server."};

      try {
         return var0[(int)(System.nanoTime() % (long)var0.length)];
      } catch (Throwable var2) {
         return "Witty comment unavailable :(";
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, new String[]{"start", "stop"}):Collections.emptyList();
   }
}
