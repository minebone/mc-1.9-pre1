package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Iterator;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockAnvil;
import net.minecraft.server.Blocks;
import net.minecraft.server.CrashReportSystemDetails;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_alf;
import net.minecraft.server.class_alg;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.MathHelper;

public class EntityFallingBlock extends Entity {
   private IBlockData e;
   public int a;
   public boolean b = true;
   private boolean f;
   private boolean g;
   private int h = 40;
   private float as = 2.0F;
   public NBTTagCompound c;
   protected static final class_ke d = DataWatcher.a(EntityFallingBlock.class, class_kg.j);

   public EntityFallingBlock(World var1) {
      super(var1);
   }

   public EntityFallingBlock(World var1, double var2, double var4, double var6, IBlockData var8) {
      super(var1);
      this.e = var8;
      this.i = true;
      this.a(0.98F, 0.98F);
      this.b(var2, var4 + (double)((1.0F - this.length) / 2.0F), var6);
      this.motX = 0.0D;
      this.motY = 0.0D;
      this.motZ = 0.0D;
      this.lastX = var2;
      this.lastY = var4;
      this.lastZ = var6;
      this.a(new BlockPosition(this));
   }

   public void a(BlockPosition var1) {
      this.datawatcher.b(d, var1);
   }

   protected boolean ad() {
      return false;
   }

   protected void i() {
      this.datawatcher.a((class_ke)d, (Object)BlockPosition.a);
   }

   public boolean ao() {
      return !this.dead;
   }

   public void m() {
      Block var1 = this.e.getBlock();
      if(this.e.getMaterial() == Material.a) {
         this.S();
      } else {
         this.lastX = this.locX;
         this.lastY = this.locY;
         this.lastZ = this.locZ;
         BlockPosition var2;
         if(this.a++ == 0) {
            var2 = new BlockPosition(this);
            if(this.world.getType(var2).getBlock() == var1) {
               this.world.g(var2);
            } else if(!this.world.E) {
               this.S();
               return;
            }
         }

         this.motY -= 0.03999999910593033D;
         this.d(this.motX, this.motY, this.motZ);
         this.motX *= 0.9800000190734863D;
         this.motY *= 0.9800000190734863D;
         this.motZ *= 0.9800000190734863D;
         if(!this.world.E) {
            var2 = new BlockPosition(this);
            if(this.onGround) {
               IBlockData var3 = this.world.getType(var2);
               if(class_alg.i(this.world.getType(new BlockPosition(this.locX, this.locY - 0.009999999776482582D, this.locZ)))) {
                  this.onGround = false;
                  return;
               }

               this.motX *= 0.699999988079071D;
               this.motZ *= 0.699999988079071D;
               this.motY *= -0.5D;
               if(var3.getBlock() != Blocks.M) {
                  this.S();
                  if(!this.f) {
                     if(this.world.a(var1, var2, true, EnumDirection.UP, (Entity)null, (ItemStack)null) && !class_alg.i(this.world.getType(var2.b())) && this.world.a((BlockPosition)var2, (IBlockData)this.e, 3)) {
                        if(var1 instanceof class_alg) {
                           ((class_alg)var1).a_(this.world, var2);
                        }

                        if(this.c != null && var1 instanceof class_alf) {
                           TileEntity var4 = this.world.r(var2);
                           if(var4 != null) {
                              NBTTagCompound var5 = new NBTTagCompound();
                              var4.a(var5);
                              Iterator var6 = this.c.c().iterator();

                              while(var6.hasNext()) {
                                 String var7 = (String)var6.next();
                                 NBTTag var8 = this.c.c(var7);
                                 if(!var7.equals("x") && !var7.equals("y") && !var7.equals("z")) {
                                    var5.a(var7, var8.b());
                                 }
                              }

                              var4.a(this.world.u(), var5);
                              var4.v_();
                           }
                        }
                     } else if(this.b && this.world.U().b("doEntityDrops")) {
                        this.a(new ItemStack(var1, 1, var1.d(this.e)), 0.0F);
                     }
                  }
               }
            } else if(this.a > 100 && !this.world.E && (var2.q() < 1 || var2.q() > 256) || this.a > 600) {
               if(this.b && this.world.U().b("doEntityDrops")) {
                  this.a(new ItemStack(var1, 1, var1.d(this.e)), 0.0F);
               }

               this.S();
            }
         }

      }
   }

   public void e(float var1, float var2) {
      Block var3 = this.e.getBlock();
      if(this.g) {
         int var4 = MathHelper.f(var1 - 1.0F);
         if(var4 > 0) {
            ArrayList var5 = Lists.newArrayList((Iterable)this.world.b((Entity)this, (AxisAlignedBB)this.bk()));
            boolean var6 = var3 == Blocks.cf;
            DamageSource var7 = var6?DamageSource.o:DamageSource.p;
            Iterator var8 = var5.iterator();

            while(var8.hasNext()) {
               Entity var9 = (Entity)var8.next();
               var9.a(var7, (float)Math.min(MathHelper.d((float)var4 * this.as), this.h));
            }

            if(var6 && (double)this.random.nextFloat() < 0.05000000074505806D + (double)var4 * 0.05D) {
               int var10 = ((Integer)this.e.get(BlockAnvil.b)).intValue();
               ++var10;
               if(var10 > 2) {
                  this.f = true;
               } else {
                  this.e = this.e.set(BlockAnvil.b, Integer.valueOf(var10));
               }
            }
         }
      }

   }

   protected void b(NBTTagCompound var1) {
      Block var2 = this.e != null?this.e.getBlock():Blocks.AIR;
      class_kk var3 = (class_kk)Block.h.b(var2);
      var1.a("Block", var3 == null?"":var3.toString());
      var1.a("Data", (byte)var2.e(this.e));
      var1.a("Time", this.a);
      var1.a("DropItem", this.b);
      var1.a("HurtEntities", this.g);
      var1.a("FallHurtAmount", this.as);
      var1.a("FallHurtMax", this.h);
      if(this.c != null) {
         var1.a((String)"TileEntityData", (NBTTag)this.c);
      }

   }

   protected void a(NBTTagCompound var1) {
      int var2 = var1.f("Data") & 255;
      if(var1.b("Block", 8)) {
         this.e = Block.b(var1.l("Block")).a(var2);
      } else if(var1.b("TileID", 99)) {
         this.e = Block.b(var1.h("TileID")).a(var2);
      } else {
         this.e = Block.b(var1.f("Tile") & 255).a(var2);
      }

      this.a = var1.h("Time");
      Block var3 = this.e.getBlock();
      if(var1.b("HurtEntities", 99)) {
         this.g = var1.p("HurtEntities");
         this.as = var1.j("FallHurtAmount");
         this.h = var1.h("FallHurtMax");
      } else if(var3 == Blocks.cf) {
         this.g = true;
      }

      if(var1.b("DropItem", 99)) {
         this.b = var1.p("DropItem");
      }

      if(var1.b("TileEntityData", 10)) {
         this.c = var1.o("TileEntityData");
      }

      if(var3 == null || var3.u().getMaterial() == Material.a) {
         this.e = Blocks.m.u();
      }

   }

   public void a(boolean var1) {
      this.g = var1;
   }

   public void a(CrashReportSystemDetails var1) {
      super.a(var1);
      if(this.e != null) {
         Block var2 = this.e.getBlock();
         var1.a((String)"Immitating block ID", (Object)Integer.valueOf(Block.a(var2)));
         var1.a((String)"Immitating block data", (Object)Integer.valueOf(var2.e(this.e)));
      }

   }

   public IBlockData l() {
      return this.e;
   }

   public boolean bq() {
      return true;
   }
}
