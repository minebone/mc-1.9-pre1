package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.InventoryCrafting;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aft;

public class class_afx implements class_aft {
   private final ItemStack a;
   private final List b;

   public class_afx(ItemStack var1, List var2) {
      this.a = var1;
      this.b = var2;
   }

   public ItemStack b() {
      return this.a;
   }

   public ItemStack[] b(InventoryCrafting var1) {
      ItemStack[] var2 = new ItemStack[var1.u_()];

      for(int var3 = 0; var3 < var2.length; ++var3) {
         ItemStack var4 = var1.a(var3);
         if(var4 != null && var4.b().r()) {
            var2[var3] = new ItemStack(var4.b().q());
         }
      }

      return var2;
   }

   public boolean a(InventoryCrafting var1, World var2) {
      ArrayList var3 = Lists.newArrayList((Iterable)this.b);

      for(int var4 = 0; var4 < var1.h(); ++var4) {
         for(int var5 = 0; var5 < var1.i(); ++var5) {
            ItemStack var6 = var1.c(var5, var4);
            if(var6 != null) {
               boolean var7 = false;
               Iterator var8 = var3.iterator();

               while(var8.hasNext()) {
                  ItemStack var9 = (ItemStack)var8.next();
                  if(var6.b() == var9.b() && (var9.i() == 32767 || var6.i() == var9.i())) {
                     var7 = true;
                     var3.remove(var9);
                     break;
                  }
               }

               if(!var7) {
                  return false;
               }
            }
         }
      }

      return var3.isEmpty();
   }

   public ItemStack a(InventoryCrafting var1) {
      return this.a.k();
   }

   public int a() {
      return this.b.size();
   }
}
