package net.minecraft.server;

import net.minecraft.server.EntityTameableAnimal;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_vb;

public class class_uz extends class_vb {
   EntityTameableAnimal a;
   class_rz b;
   private int c;

   public class_uz(EntityTameableAnimal var1) {
      super(var1, false);
      this.a = var1;
      this.a(1);
   }

   public boolean a() {
      if(!this.a.cZ()) {
         return false;
      } else {
         class_rz var1 = this.a.dc();
         if(var1 == null) {
            return false;
         } else {
            this.b = var1.bF();
            int var2 = var1.bG();
            return var2 != this.c && this.a(this.b, false) && this.a.a(this.b, var1);
         }
      }
   }

   public void c() {
      this.e.c(this.b);
      class_rz var1 = this.a.dc();
      if(var1 != null) {
         this.c = var1.bG();
      }

      super.c();
   }
}
