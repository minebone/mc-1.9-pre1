package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import java.util.Random;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_bab;
import net.minecraft.server.class_bae;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_kk;

public class class_bai extends class_bae {
   private final class_bab a;

   public class_bai(class_baq[] var1, class_bab var2) {
      super(var1);
      this.a = var2;
   }

   public ItemStack a(ItemStack var1, Random var2, class_azy var3) {
      var1.b = this.a.a(var2);
      return var1;
   }

   public static class class_a_in_class_bai extends class_bae.class_a_in_class_bae {
      protected class_a_in_class_bai() {
         super(new class_kk("set_count"), class_bai.class);
      }

      public void a(JsonObject var1, class_bai var2, JsonSerializationContext var3) {
         var1.add("count", var3.serialize(var2.a));
      }

      public class_bai a(JsonObject var1, JsonDeserializationContext var2, class_baq[] var3) {
         return new class_bai(var3, (class_bab)ChatDeserializer.a(var1, "count", var2, class_bab.class));
      }

      // $FF: synthetic method
      public class_bae b(JsonObject var1, JsonDeserializationContext var2, class_baq[] var3) {
         return this.a(var1, var2, var3);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(JsonObject var1, class_bae var2, JsonSerializationContext var3) {
         this.a(var1, (class_bai)var2, var3);
      }
   }
}
