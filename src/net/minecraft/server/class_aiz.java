package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.BlockTallPlant;
import net.minecraft.server.World;
import net.minecraft.server.class_ato;
import net.minecraft.server.class_auz;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_wj;

public class class_aiz extends BiomeBase {
   private static final class_auz y = new class_auz(false);

   protected class_aiz(BiomeBase.class_a_in_class_aif var1) {
      super(var1);
      this.v.add(new BiomeBase.BiomeMeta(class_wj.class, 1, 2, 6));
      this.t.z = 1;
      this.t.A = 4;
      this.t.B = 20;
   }

   public class_ato a(Random var1) {
      return (class_ato)(var1.nextInt(5) > 0?y:n);
   }

   public void a(World var1, Random var2, BlockPosition var3) {
      m.a(BlockTallPlant.EnumTallFlowerVariants.GRASS);

      for(int var4 = 0; var4 < 7; ++var4) {
         int var5 = var2.nextInt(16) + 8;
         int var6 = var2.nextInt(16) + 8;
         int var7 = var2.nextInt(var1.l(var3.a(var5, 0, var6)).q() + 32);
         m.b(var1, var2, var3.a(var5, var7, var6));
      }

      super.a(var1, var2, var3);
   }

   public Class g() {
      return class_aiz.class;
   }
}
