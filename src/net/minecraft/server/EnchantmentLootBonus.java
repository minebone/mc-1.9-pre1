package net.minecraft.server;

import net.minecraft.server.Enchantment;
import net.minecraft.server.class_agm;
import net.minecraft.server.class_agp;
import net.minecraft.server.EnumInventorySlot;

public class EnchantmentLootBonus extends Enchantment {
   protected EnchantmentLootBonus(Enchantment.class_a_in_class_agl var1, class_agm var2, EnumInventorySlot... var3) {
      super(var1, var2, var3);
      if(var2 == class_agm.DIGGER) {
         this.c("lootBonusDigger");
      } else if(var2 == class_agm.FISHING_ROD) {
         this.c("lootBonusFishing");
      } else {
         this.c("lootBonus");
      }

   }

   public int a(int var1) {
      return 15 + (var1 - 1) * 9;
   }

   public int b(int var1) {
      return super.a(var1) + 50;
   }

   public int b() {
      return 3;
   }

   public boolean a(Enchantment var1) {
      return super.a(var1) && var1 != class_agp.r;
   }
}
