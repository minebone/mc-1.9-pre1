package net.minecraft.server;

import java.io.IOException;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayInListener;
import net.minecraft.server.EnumHand;

public class class_je implements Packet {
   private EnumHand a;

   public class_je() {
   }

   public class_je(EnumHand var1) {
      this.a = var1;
   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = (EnumHand)var1.a(EnumHand.class);
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.a((Enum)this.a);
   }

   public void a(PacketPlayInListener var1) {
      var1.a(this);
   }

   public EnumHand a() {
      return this.a;
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketPlayInListener)var1);
   }
}
