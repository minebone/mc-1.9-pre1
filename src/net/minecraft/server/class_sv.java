package net.minecraft.server;

import net.minecraft.server.EntityInsentient;

public class class_sv {
   private EntityInsentient b;
   protected boolean a;

   public class_sv(EntityInsentient var1) {
      this.b = var1;
   }

   public void a() {
      this.a = true;
   }

   public void b() {
      this.b.k(this.a);
      this.a = false;
   }
}
