package net.minecraft.server;

import java.util.Calendar;
import net.minecraft.server.AchievementList;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Enchantment;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityCreature;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumMonsterType;
import net.minecraft.server.Items;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.Statistic;
import net.minecraft.server.World;
import net.minecraft.server.class_aac;
import net.minecraft.server.EntityBoat;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agn;
import net.minecraft.server.class_agp;
import net.minecraft.server.class_asw;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_qk;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sz;
import net.minecraft.server.class_tf;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_to;
import net.minecraft.server.class_tr;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_uh;
import net.minecraft.server.class_uj;
import net.minecraft.server.class_uu;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_wg;
import net.minecraft.server.class_wi;
import net.minecraft.server.class_yh;
import net.minecraft.server.class_yp;
import net.minecraft.server.class_yr;
import net.minecraft.server.class_ys;
import net.minecraft.server.class_zl;

public class class_yv extends class_yp implements class_yr {
   private static final class_ke a = DataWatcher.a(class_yv.class, class_kg.b);
   private static final class_ke b = DataWatcher.a(class_yv.class, class_kg.h);
   private final class_uh c = new class_uh(this, 1.0D, 20, 15.0F);
   private final class_tr bv = new class_tr(this, 1.2D, false) {
      public void d() {
         super.d();
         class_yv.this.a(false);
      }

      public void c() {
         super.c();
         class_yv.this.a(true);
      }
   };

   public class_yv(World var1) {
      super(var1);
      this.o();
   }

   protected void r() {
      this.bp.a(1, new class_tg(this));
      this.bp.a(2, new class_uj(this));
      this.bp.a(3, new class_tf(this, 1.0D));
      this.bp.a(3, new class_sz(this, class_wi.class, 6.0F, 1.0D, 1.2D));
      this.bp.a(5, new class_uf(this, 1.0D));
      this.bp.a(6, new class_to(this, EntityHuman.class, 8.0F));
      this.bp.a(6, new class_ue(this));
      this.bq.a(1, new class_uu(this, false, new Class[0]));
      this.bq.a(2, new class_ux(this, EntityHuman.class, true));
      this.bq.a(3, new class_ux(this, class_wg.class, true));
   }

   protected void bz() {
      super.bz();
      this.a((class_sk)class_ys.d).a(0.25D);
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)a, (Object)Integer.valueOf(0));
      this.datawatcher.a((class_ke)b, (Object)Boolean.valueOf(false));
   }

   protected class_nf G() {
      return class_ng.fc;
   }

   protected class_nf bQ() {
      return class_ng.fh;
   }

   protected class_nf bR() {
      return class_ng.fd;
   }

   protected void a(BlockPosition var1, Block var2) {
      this.a(class_ng.fj, 0.15F, 1.0F);
   }

   public boolean B(Entity var1) {
      if(super.B(var1)) {
         if(this.da() == 1 && var1 instanceof class_rz) {
            ((class_rz)var1).c(new MobEffect(MobEffectList.t, 200));
         }

         return true;
      } else {
         return false;
      }
   }

   public EnumMonsterType bZ() {
      return EnumMonsterType.UNDEAD;
   }

   public void n() {
      if(this.world.B() && !this.world.E) {
         float var1 = this.e(1.0F);
         BlockPosition var2 = this.bx() instanceof EntityBoat?(new BlockPosition(this.locX, (double)Math.round(this.locY), this.locZ)).a():new BlockPosition(this.locX, (double)Math.round(this.locY), this.locZ);
         if(var1 > 0.5F && this.random.nextFloat() * 30.0F < (var1 - 0.4F) * 2.0F && this.world.h(var2)) {
            boolean var3 = true;
            ItemStack var4 = this.a((EnumInventorySlot)EnumInventorySlot.HEAD);
            if(var4 != null) {
               if(var4.e()) {
                  var4.b(var4.h() + this.random.nextInt(2));
                  if(var4.h() >= var4.j()) {
                     this.b(var4);
                     this.a((EnumInventorySlot)EnumInventorySlot.HEAD, (ItemStack)null);
                  }
               }

               var3 = false;
            }

            if(var3) {
               this.g(8);
            }
         }
      }

      if(this.world.E) {
         this.b(this.da());
      }

      super.n();
   }

   public void av() {
      super.av();
      if(this.bx() instanceof EntityCreature) {
         EntityCreature var1 = (EntityCreature)this.bx();
         this.aM = var1.aM;
      }

   }

   public void a(DamageSource var1) {
      super.a(var1);
      if(var1.i() instanceof class_zl && var1.j() instanceof EntityHuman) {
         EntityHuman var2 = (EntityHuman)var1.j();
         double var3 = var2.locX - this.locX;
         double var5 = var2.locZ - this.locZ;
         if(var3 * var3 + var5 * var5 >= 2500.0D) {
            var2.b((Statistic)AchievementList.v);
         }
      } else if(var1.j() instanceof class_yh && ((class_yh)var1.j()).o() && ((class_yh)var1.j()).dd()) {
         ((class_yh)var1.j()).de();
         this.a(new ItemStack(Items.ch, 1, this.da() == 1?1:0), 0.0F);
      }

   }

   protected class_kk J() {
      return this.da() == 1?class_azs.ak:class_azs.aj;
   }

   protected void a(class_qk var1) {
      super.a(var1);
      this.a(EnumInventorySlot.MAINHAND, new ItemStack(Items.f));
   }

   public class_sc a(class_qk var1, class_sc var2) {
      var2 = super.a(var1, var2);
      if(this.world.s instanceof class_asw && this.bE().nextInt(5) > 0) {
         this.bp.a(4, this.bv);
         this.a(1);
         this.a(EnumInventorySlot.MAINHAND, new ItemStack(Items.s));
         this.a((class_sk)class_ys.e).a(4.0D);
      } else {
         this.bp.a(4, this.c);
         this.a(var1);
         this.b(var1);
      }

      this.l(this.random.nextFloat() < 0.55F * var1.c());
      if(this.a((EnumInventorySlot)EnumInventorySlot.HEAD) == null) {
         Calendar var3 = this.world.ac();
         if(var3.get(2) + 1 == 10 && var3.get(5) == 31 && this.random.nextFloat() < 0.25F) {
            this.a(EnumInventorySlot.HEAD, new ItemStack(this.random.nextFloat() < 0.1F?Blocks.aZ:Blocks.aU));
            this.bs[EnumInventorySlot.HEAD.b()] = 0.0F;
         }
      }

      return var2;
   }

   public void o() {
      if(this.world != null && !this.world.E) {
         this.bp.a((class_tj)this.bv);
         this.bp.a((class_tj)this.c);
         ItemStack var1 = this.ca();
         if(var1 != null && var1.b() == Items.f) {
            this.bp.a(4, this.c);
         } else {
            this.bp.a(4, this.bv);
         }
      }

   }

   public void a(class_rz var1, float var2) {
      class_aac var3 = new class_aac(this.world, this);
      double var4 = var1.locX - this.locX;
      double var6 = var1.bk().b + (double)(var1.length / 3.0F) - var3.locY;
      double var8 = var1.locZ - this.locZ;
      double var10 = (double)MathHelper.a(var4 * var4 + var8 * var8);
      var3.c(var4, var6 + var10 * 0.20000000298023224D, var8, 1.6F, (float)(14 - this.world.ae().a() * 4));
      int var12 = class_agn.a((Enchantment)class_agp.u, (class_rz)this);
      int var13 = class_agn.a((Enchantment)class_agp.v, (class_rz)this);
      var3.c((double)(var2 * 2.0F) + this.random.nextGaussian() * 0.25D + (double)((float)this.world.ae().a() * 0.11F));
      if(var12 > 0) {
         var3.c(var3.k() + (double)var12 * 0.5D + 0.5D);
      }

      if(var13 > 0) {
         var3.a(var13);
      }

      if(class_agn.a((Enchantment)class_agp.w, (class_rz)this) > 0 || this.da() == 1) {
         var3.g(100);
      }

      this.a(class_ng.fi, 1.0F, 1.0F / (this.bE().nextFloat() * 0.4F + 0.8F));
      this.world.a((Entity)var3);
   }

   public int da() {
      return ((Integer)this.datawatcher.a(a)).intValue();
   }

   public void a(int var1) {
      this.datawatcher.b(a, Integer.valueOf(var1));
      this.fireProof = var1 == 1;
      this.b(var1);
   }

   private void b(int var1) {
      if(var1 == 1) {
         this.a(0.7F, 2.4F);
      } else {
         this.a(0.6F, 1.99F);
      }

   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      if(var1.b("SkeletonType", 99)) {
         byte var2 = var1.f("SkeletonType");
         this.a(var2);
      }

      this.o();
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("SkeletonType", (byte)this.da());
   }

   public void a(EnumInventorySlot var1, ItemStack var2) {
      super.a(var1, var2);
      if(!this.world.E && var1 == EnumInventorySlot.MAINHAND) {
         this.o();
      }

   }

   public float bm() {
      return this.da() == 1?2.1F:1.74F;
   }

   public double aw() {
      return -0.35D;
   }

   public void a(boolean var1) {
      this.datawatcher.b(b, Boolean.valueOf(var1));
   }
}
