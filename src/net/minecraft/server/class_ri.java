package net.minecraft.server;

import net.minecraft.server.MobEffectType;

public class class_ri extends MobEffectType {
   public class_ri(boolean var1, int var2) {
      super(var1, var2);
   }

   public boolean b() {
      return true;
   }

   public boolean a(int var1, int var2) {
      return var1 >= 1;
   }
}
