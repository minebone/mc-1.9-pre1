package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_ajx extends Block {
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.30000001192092896D, 0.0D, 0.30000001192092896D, 0.699999988079071D, 0.6000000238418579D, 0.699999988079071D);

   protected class_ajx() {
      this(Material.k);
   }

   protected class_ajx(Material var1) {
      this(var1, var1.r());
   }

   protected class_ajx(Material var1, MaterialMapColor var2) {
      super(var1, var2);
      this.a(true);
      this.a(CreativeModeTab.c);
   }

   public boolean a(World var1, BlockPosition var2) {
      return super.a(var1, var2) && this.i(var1.getType(var2.b()));
   }

   protected boolean i(IBlockData var1) {
      return var1.getBlock() == Blocks.c || var1.getBlock() == Blocks.d || var1.getBlock() == Blocks.ak;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      super.a(var1, var2, var3, var4);
      this.e(var1, var2, var3);
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      this.e(var1, var2, var3);
   }

   protected void e(World var1, BlockPosition var2, IBlockData var3) {
      if(!this.f(var1, var2, var3)) {
         this.b(var1, var2, var3, 0);
         var1.a((BlockPosition)var2, (IBlockData)Blocks.AIR.u(), 3);
      }

   }

   public boolean f(World var1, BlockPosition var2, IBlockData var3) {
      return this.i(var1.getType(var2.b()));
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return b;
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }
}
