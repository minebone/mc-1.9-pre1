package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Map;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_apd;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.class_ata;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_alk extends Block {
   public static final BlockStateInteger a = BlockStateInteger.a("age", 0, 15);
   public static final class_arm b = class_arm.a("north");
   public static final class_arm c = class_arm.a("east");
   public static final class_arm d = class_arm.a("south");
   public static final class_arm e = class_arm.a("west");
   public static final class_arm f = class_arm.a("up");
   private final Map g = Maps.newIdentityHashMap();
   private final Map B = Maps.newIdentityHashMap();

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return !var2.getType(var3.b()).q() && !Blocks.ab.c(var2, var3.b())?var1.set(b, Boolean.valueOf(this.c(var2, var3.c()))).set(c, Boolean.valueOf(this.c(var2, var3.f()))).set(d, Boolean.valueOf(this.c(var2, var3.d()))).set(e, Boolean.valueOf(this.c(var2, var3.e()))).set(f, Boolean.valueOf(this.c(var2, var3.a()))):this.u();
   }

   protected class_alk() {
      super(Material.o);
      this.w(this.A.b().set(a, Integer.valueOf(0)).set(b, Boolean.valueOf(false)).set(c, Boolean.valueOf(false)).set(d, Boolean.valueOf(false)).set(e, Boolean.valueOf(false)).set(f, Boolean.valueOf(false)));
      this.a(true);
   }

   public static void e() {
      Blocks.ab.a(Blocks.f, 5, 20);
      Blocks.ab.a(Blocks.bL, 5, 20);
      Blocks.ab.a(Blocks.bM, 5, 20);
      Blocks.ab.a(Blocks.bo, 5, 20);
      Blocks.ab.a(Blocks.bp, 5, 20);
      Blocks.ab.a(Blocks.bq, 5, 20);
      Blocks.ab.a(Blocks.br, 5, 20);
      Blocks.ab.a(Blocks.bs, 5, 20);
      Blocks.ab.a(Blocks.bt, 5, 20);
      Blocks.ab.a(Blocks.aO, 5, 20);
      Blocks.ab.a(Blocks.aP, 5, 20);
      Blocks.ab.a(Blocks.aQ, 5, 20);
      Blocks.ab.a(Blocks.aR, 5, 20);
      Blocks.ab.a(Blocks.aS, 5, 20);
      Blocks.ab.a(Blocks.aT, 5, 20);
      Blocks.ab.a(Blocks.ad, 5, 20);
      Blocks.ab.a(Blocks.bV, 5, 20);
      Blocks.ab.a(Blocks.bU, 5, 20);
      Blocks.ab.a(Blocks.bW, 5, 20);
      Blocks.ab.a(Blocks.cC, 5, 20);
      Blocks.ab.a(Blocks.cD, 5, 20);
      Blocks.ab.a(Blocks.r, 5, 5);
      Blocks.ab.a(Blocks.s, 5, 5);
      Blocks.ab.a(Blocks.t, 30, 60);
      Blocks.ab.a(Blocks.u, 30, 60);
      Blocks.ab.a(Blocks.X, 30, 20);
      Blocks.ab.a(Blocks.W, 15, 100);
      Blocks.ab.a(Blocks.H, 60, 100);
      Blocks.ab.a(Blocks.cF, 60, 100);
      Blocks.ab.a(Blocks.N, 60, 100);
      Blocks.ab.a(Blocks.O, 60, 100);
      Blocks.ab.a(Blocks.I, 60, 100);
      Blocks.ab.a(Blocks.L, 30, 60);
      Blocks.ab.a(Blocks.bn, 15, 100);
      Blocks.ab.a(Blocks.cA, 5, 5);
      Blocks.ab.a(Blocks.cx, 60, 20);
      Blocks.ab.a(Blocks.cy, 60, 20);
   }

   public void a(Block var1, int var2, int var3) {
      this.g.put(var1, Integer.valueOf(var2));
      this.B.put(var1, Integer.valueOf(var3));
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public int a(Random var1) {
      return 0;
   }

   public int a(World var1) {
      return 30;
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(var1.U().b("doFireTick")) {
         if(!this.a(var1, var2)) {
            var1.g(var2);
         }

         Block var5 = var1.getType(var2.b()).getBlock();
         boolean var6 = var5 == Blocks.aV;
         if(var1.s instanceof class_ata && var5 == Blocks.h) {
            var6 = true;
         }

         int var7 = ((Integer)var3.get(a)).intValue();
         if(!var6 && var1.W() && this.b(var1, var2) && var4.nextFloat() < 0.2F + (float)var7 * 0.03F) {
            var1.g(var2);
         } else {
            if(var7 < 15) {
               var3 = var3.set(a, Integer.valueOf(var7 + var4.nextInt(3) / 2));
               var1.a((BlockPosition)var2, (IBlockData)var3, 4);
            }

            var1.a((BlockPosition)var2, (Block)this, this.a(var1) + var4.nextInt(10));
            if(!var6) {
               if(!this.c(var1, var2)) {
                  if(!var1.getType(var2.b()).q() || var7 > 3) {
                     var1.g(var2);
                  }

                  return;
               }

               if(!this.c((class_ahw)var1, var2.b()) && var7 == 15 && var4.nextInt(4) == 0) {
                  var1.g(var2);
                  return;
               }
            }

            boolean var8 = var1.C(var2);
            byte var9 = 0;
            if(var8) {
               var9 = -50;
            }

            this.a(var1, var2.f(), 300 + var9, var4, var7);
            this.a(var1, var2.e(), 300 + var9, var4, var7);
            this.a(var1, var2.b(), 250 + var9, var4, var7);
            this.a(var1, var2.a(), 250 + var9, var4, var7);
            this.a(var1, var2.c(), 300 + var9, var4, var7);
            this.a(var1, var2.d(), 300 + var9, var4, var7);

            for(int var10 = -1; var10 <= 1; ++var10) {
               for(int var11 = -1; var11 <= 1; ++var11) {
                  for(int var12 = -1; var12 <= 4; ++var12) {
                     if(var10 != 0 || var12 != 0 || var11 != 0) {
                        int var13 = 100;
                        if(var12 > 1) {
                           var13 += (var12 - 1) * 100;
                        }

                        BlockPosition var14 = var2.a(var10, var12, var11);
                        int var15 = this.d(var1, var14);
                        if(var15 > 0) {
                           int var16 = (var15 + 40 + var1.ae().a() * 7) / (var7 + 30);
                           if(var8) {
                              var16 /= 2;
                           }

                           if(var16 > 0 && var4.nextInt(var13) <= var16 && (!var1.W() || !this.b(var1, var14))) {
                              int var17 = var7 + var4.nextInt(5) / 4;
                              if(var17 > 15) {
                                 var17 = 15;
                              }

                              var1.a((BlockPosition)var14, (IBlockData)var3.set(a, Integer.valueOf(var17)), 3);
                           }
                        }
                     }
                  }
               }
            }

         }
      }
   }

   protected boolean b(World var1, BlockPosition var2) {
      return var1.B(var2) || var1.B(var2.e()) || var1.B(var2.f()) || var1.B(var2.c()) || var1.B(var2.d());
   }

   public boolean s() {
      return false;
   }

   private int c(Block var1) {
      Integer var2 = (Integer)this.B.get(var1);
      return var2 == null?0:var2.intValue();
   }

   private int d(Block var1) {
      Integer var2 = (Integer)this.g.get(var1);
      return var2 == null?0:var2.intValue();
   }

   private void a(World var1, BlockPosition var2, int var3, Random var4, int var5) {
      int var6 = this.c(var1.getType(var2).getBlock());
      if(var4.nextInt(var3) < var6) {
         IBlockData var7 = var1.getType(var2);
         if(var4.nextInt(var5 + 10) < 5 && !var1.B(var2)) {
            int var8 = var5 + var4.nextInt(5) / 4;
            if(var8 > 15) {
               var8 = 15;
            }

            var1.a((BlockPosition)var2, (IBlockData)this.u().set(a, Integer.valueOf(var8)), 3);
         } else {
            var1.g(var2);
         }

         if(var7.getBlock() == Blocks.W) {
            Blocks.W.d(var1, var2, var7.set(class_apd.a, Boolean.valueOf(true)));
         }
      }

   }

   private boolean c(World var1, BlockPosition var2) {
      EnumDirection[] var3 = EnumDirection.values();
      int var4 = var3.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         EnumDirection var6 = var3[var5];
         if(this.c((class_ahw)var1, var2.a(var6))) {
            return true;
         }
      }

      return false;
   }

   private int d(World var1, BlockPosition var2) {
      if(!var1.d(var2)) {
         return 0;
      } else {
         int var3 = 0;
         EnumDirection[] var4 = EnumDirection.values();
         int var5 = var4.length;

         for(int var6 = 0; var6 < var5; ++var6) {
            EnumDirection var7 = var4[var6];
            var3 = Math.max(this.d(var1.getType(var2.a(var7)).getBlock()), var3);
         }

         return var3;
      }
   }

   public boolean n() {
      return false;
   }

   public boolean c(class_ahw var1, BlockPosition var2) {
      return this.d(var1.getType(var2).getBlock()) > 0;
   }

   public boolean a(World var1, BlockPosition var2) {
      return var1.getType(var2.b()).q() || this.c(var1, var2);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!var1.getType(var2.b()).q() && !this.c(var1, var2)) {
         var1.g(var2);
      }

   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      if(var1.s.p().a() > 0 || !Blocks.aY.b(var1, var2)) {
         if(!var1.getType(var2.b()).q() && !this.c(var1, var2)) {
            var1.g(var2);
         } else {
            var1.a((BlockPosition)var2, (Block)this, this.a(var1) + var1.r.nextInt(10));
         }
      }
   }

   public MaterialMapColor r(IBlockData var1) {
      return MaterialMapColor.f;
   }

   public IBlockData a(int var1) {
      return this.u().set(a, Integer.valueOf(var1));
   }

   public int e(IBlockData var1) {
      return ((Integer)var1.get(a)).intValue();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b, c, d, e, f});
   }
}
