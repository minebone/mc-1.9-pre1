package net.minecraft.server;

import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;

public class class_adz extends Item {
   public class_adz() {
      this.a(CreativeModeTab.i);
   }

   public boolean a(ItemStack var1, EntityHuman var2, class_rz var3, EnumHand var4) {
      if(!var1.s()) {
         return false;
      } else if(var3 instanceof EntityInsentient) {
         EntityInsentient var5 = (EntityInsentient)var3;
         var5.c((String)var1.q());
         var5.cL();
         --var1.b;
         return true;
      } else {
         return super.a(var1, var2, var3, var4);
      }
   }
}
