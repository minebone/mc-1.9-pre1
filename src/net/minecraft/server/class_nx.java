package net.minecraft.server;

import net.minecraft.server.MathHelper;
import org.apache.commons.lang3.Validate;

public class class_nx {
   private final long[] a;
   private final int b;
   private final long c;
   private final int d;

   public class_nx(int var1, int var2) {
      Validate.inclusiveBetween(1L, 32L, (long)var1);
      this.d = var2;
      this.b = var1;
      this.c = (1L << var1) - 1L;
      this.a = new long[MathHelper.c(var2 * var1, 64) / 64];
   }

   public void a(int var1, int var2) {
      Validate.inclusiveBetween(0L, (long)(this.d - 1), (long)var1);
      Validate.inclusiveBetween(0L, this.c, (long)var2);
      int var3 = var1 * this.b;
      int var4 = var3 / 64;
      int var5 = ((var1 + 1) * this.b - 1) / 64;
      int var6 = var3 % 64;
      this.a[var4] = this.a[var4] & ~(this.c << var6) | ((long)var2 & this.c) << var6;
      if(var4 != var5) {
         int var7 = 64 - var6;
         int var8 = this.b - var7;
         this.a[var5] = this.a[var5] >>> var8 << var8 | ((long)var2 & this.c) >> var7;
      }

   }

   public int a(int var1) {
      Validate.inclusiveBetween(0L, (long)(this.d - 1), (long)var1);
      int var2 = var1 * this.b;
      int var3 = var2 / 64;
      int var4 = ((var1 + 1) * this.b - 1) / 64;
      int var5 = var2 % 64;
      if(var3 == var4) {
         return (int)(this.a[var3] >>> var5 & this.c);
      } else {
         int var6 = 64 - var5;
         return (int)((this.a[var3] >>> var5 | this.a[var4] << var6) & this.c);
      }
   }

   public long[] a() {
      return this.a;
   }

   public int b() {
      return this.d;
   }
}
