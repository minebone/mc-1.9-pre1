package net.minecraft.server;

import net.minecraft.server.PlayerAbilities;
import net.minecraft.server.WorldData;
import net.minecraft.server.WorldType;

public final class WorldSettings {
   private final long a;
   private final WorldSettings.EnumGamemode b;
   private final boolean c;
   private final boolean d;
   private final WorldType e;
   private boolean f;
   private boolean g;
   private String h;

   public WorldSettings(long var1, WorldSettings.EnumGamemode var3, boolean var4, boolean var5, WorldType var6) {
      this.h = "";
      this.a = var1;
      this.b = var3;
      this.c = var4;
      this.d = var5;
      this.e = var6;
   }

   public WorldSettings(WorldData var1) {
      this(var1.a(), var1.q(), var1.r(), var1.s(), var1.t());
   }

   public WorldSettings a() {
      this.g = true;
      return this;
   }

   public WorldSettings a(String var1) {
      this.h = var1;
      return this;
   }

   public boolean c() {
      return this.g;
   }

   public long d() {
      return this.a;
   }

   public WorldSettings.EnumGamemode e() {
      return this.b;
   }

   public boolean f() {
      return this.d;
   }

   public boolean g() {
      return this.c;
   }

   public WorldType h() {
      return this.e;
   }

   public boolean i() {
      return this.f;
   }

   public static WorldSettings.EnumGamemode a(int var0) {
      return WorldSettings.EnumGamemode.a(var0);
   }

   public String j() {
      return this.h;
   }

   public static enum EnumGamemode {
      NOT_SET(-1, "", ""),
      SURVIVAL(0, "survival", "s"),
      CREATIVE(1, "creative", "c"),
      ADVENTURE(2, "adventure", "a"),
      SPECTATOR(3, "spectator", "sp");

      int f;
      String g;
      String h;

      private EnumGamemode(int var3, String var4, String var5) {
         this.f = var3;
         this.g = var4;
         this.h = var5;
      }

      public int a() {
         return this.f;
      }

      public String b() {
         return this.g;
      }

      public void a(PlayerAbilities var1) {
         if(this == CREATIVE) {
            var1.c = true;
            var1.d = true;
            var1.a = true;
         } else if(this == SPECTATOR) {
            var1.c = true;
            var1.d = false;
            var1.a = true;
            var1.b = true;
         } else {
            var1.c = false;
            var1.d = false;
            var1.a = false;
            var1.b = false;
         }

         var1.e = !this.c();
      }

      public boolean c() {
         return this == ADVENTURE || this == SPECTATOR;
      }

      public boolean d() {
         return this == CREATIVE;
      }

      public boolean e() {
         return this == SURVIVAL || this == ADVENTURE;
      }

      public static WorldSettings.EnumGamemode a(int var0) {
         return a(var0, SURVIVAL);
      }

      public static WorldSettings.EnumGamemode a(int var0, WorldSettings.EnumGamemode var1) {
         WorldSettings.EnumGamemode[] var2 = values();
         int var3 = var2.length;

         for(int var4 = 0; var4 < var3; ++var4) {
            WorldSettings.EnumGamemode var5 = var2[var4];
            if(var5.f == var0) {
               return var5;
            }
         }

         return var1;
      }

      public static WorldSettings.EnumGamemode a(String var0, WorldSettings.EnumGamemode var1) {
         WorldSettings.EnumGamemode[] var2 = values();
         int var3 = var2.length;

         for(int var4 = 0; var4 < var3; ++var4) {
            WorldSettings.EnumGamemode var5 = var2[var4];
            if(var5.g.equals(var0) || var5.h.equals(var0)) {
               return var5;
            }
         }

         return var1;
      }
   }
}
