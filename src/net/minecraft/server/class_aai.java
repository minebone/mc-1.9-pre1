package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityMinecartAbstract;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahi;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.EnumHand;

public class class_aai extends EntityMinecartAbstract {
   private static final class_ke a = DataWatcher.a(class_aai.class, class_kg.d);
   private static final class_ke b = DataWatcher.a(class_aai.class, class_kg.e);
   private final class_ahi c = new class_ahi() {
      public void i() {
         class_aai.this.Q().b(class_aai.a, this.m());
         class_aai.this.Q().b(class_aai.b, this.l());
      }

      public BlockPosition c() {
         return new BlockPosition(class_aai.this.locX, class_aai.this.locY + 0.5D, class_aai.this.locZ);
      }

      public Vec3D d() {
         return new Vec3D(class_aai.this.locX, class_aai.this.locY, class_aai.this.locZ);
      }

      public World e() {
         return class_aai.this.world;
      }

      public Entity f() {
         return class_aai.this;
      }

      public MinecraftServer h() {
         return class_aai.this.world.u();
      }
   };
   private int d = 0;

   public class_aai(World var1) {
      super(var1);
   }

   public class_aai(World var1, double var2, double var4, double var6) {
      super(var1, var2, var4, var6);
   }

   protected void i() {
      super.i();
      this.Q().a((class_ke)a, (Object)"");
      this.Q().a((class_ke)b, (Object)(new ChatComponentText("")));
   }

   protected void a(NBTTagCompound var1) {
      super.a(var1);
      this.c.b(var1);
      this.Q().b(a, this.j().m());
      this.Q().b(b, this.j().l());
   }

   protected void b(NBTTagCompound var1) {
      super.b(var1);
      this.c.a(var1);
   }

   public EntityMinecartAbstract.EnumMinecartType v() {
      return EntityMinecartAbstract.EnumMinecartType.COMMAND_BLOCK;
   }

   public IBlockData x() {
      return Blocks.bX.u();
   }

   public class_ahi j() {
      return this.c;
   }

   public void a(int var1, int var2, int var3, boolean var4) {
      if(var4 && this.ticksLived - this.d >= 4) {
         this.j().a(this.world);
         this.d = this.ticksLived;
      }

   }

   public boolean a(EntityHuman var1, ItemStack var2, EnumHand var3) {
      this.c.a(var1);
      return false;
   }

   public void a(class_ke var1) {
      super.a((class_ke)var1);
      if(b.equals(var1)) {
         try {
            this.c.b((IChatBaseComponent)this.Q().a(b));
         } catch (Throwable var3) {
            ;
         }
      } else if(a.equals(var1)) {
         this.c.a((String)this.Q().a(a));
      }

   }

   public boolean bq() {
      return true;
   }
}
