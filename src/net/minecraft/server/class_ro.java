package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.MobEffect;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.class_afd;
import net.minecraft.server.class_aff;
import net.minecraft.server.class_afg;
import net.minecraft.server.class_axg;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rz;

public class class_ro extends Entity {
   private static final class_ke a = DataWatcher.a(class_ro.class, class_kg.c);
   private static final class_ke b = DataWatcher.a(class_ro.class, class_kg.b);
   private static final class_ke c = DataWatcher.a(class_ro.class, class_kg.h);
   private static final class_ke d = DataWatcher.a(class_ro.class, class_kg.b);
   private class_afd e;
   private final List f;
   private final Map g;
   private int h;
   private int as;
   private int at;
   private boolean au;
   private int av;
   private float aw;
   private float ax;
   private class_rz ay;
   private UUID az;

   public class_ro(World var1) {
      super(var1);
      this.e = class_afg.a;
      this.f = Lists.newArrayList();
      this.g = Maps.newHashMap();
      this.h = 600;
      this.as = 20;
      this.at = 20;
      this.noClip = true;
      this.fireProof = true;
      this.a(3.0F);
   }

   public class_ro(World var1, double var2, double var4, double var6) {
      this(var1);
      this.b(var2, var4, var6);
   }

   protected void i() {
      this.Q().a((class_ke)b, (Object)Integer.valueOf(0));
      this.Q().a((class_ke)a, (Object)Float.valueOf(0.5F));
      this.Q().a((class_ke)c, (Object)Boolean.valueOf(false));
      this.Q().a((class_ke)d, (Object)Integer.valueOf(EnumParticle.SPELL_MOB.c()));
   }

   public void a(float var1) {
      double var2 = this.locX;
      double var4 = this.locY;
      double var6 = this.locZ;
      this.a(var1 * 2.0F, 0.5F);
      this.b(var2, var4, var6);
      if(!this.world.E) {
         this.Q().b(a, Float.valueOf(var1));
      }

   }

   public float j() {
      return ((Float)this.Q().a(a)).floatValue();
   }

   public void a(class_afd var1) {
      this.e = var1;
      if(!this.au) {
         if(var1 == class_afg.a && this.f.isEmpty()) {
            this.Q().b(b, Integer.valueOf(0));
         } else {
            this.Q().b(b, Integer.valueOf(class_aff.a((Collection)class_aff.a((class_afd)var1, (Collection)this.f))));
         }
      }

   }

   public void a(MobEffect var1) {
      this.f.add(var1);
      if(!this.au) {
         this.Q().b(b, Integer.valueOf(class_aff.a((Collection)class_aff.a((class_afd)this.e, (Collection)this.f))));
      }

   }

   public int k() {
      return ((Integer)this.Q().a(b)).intValue();
   }

   public void a(int var1) {
      this.au = true;
      this.Q().b(b, Integer.valueOf(var1));
   }

   public EnumParticle l() {
      return EnumParticle.a(((Integer)this.Q().a(d)).intValue());
   }

   public void a(EnumParticle var1) {
      this.Q().b(d, Integer.valueOf(var1.c()));
   }

   protected void a(boolean var1) {
      this.Q().b(c, Boolean.valueOf(var1));
   }

   public boolean n() {
      return ((Boolean)this.Q().a(c)).booleanValue();
   }

   public int o() {
      return this.h;
   }

   public void b(int var1) {
      this.h = var1;
   }

   public void m() {
      super.m();
      boolean var1 = this.n();
      float var2 = this.j();
      if(this.world.E) {
         EnumParticle var3 = this.l();
         float var7;
         float var8;
         float var9;
         int var11;
         int var12;
         int var13;
         if(var1) {
            if(this.random.nextBoolean()) {
               int[] var4 = new int[var3.d()];

               for(int var5 = 0; var5 < 2; ++var5) {
                  float var6 = this.random.nextFloat() * 6.2831855F;
                  var7 = MathHelper.c(this.random.nextFloat()) * 0.2F;
                  var8 = MathHelper.b(var6) * var7;
                  var9 = MathHelper.a(var6) * var7;
                  if(var3 == EnumParticle.SPELL_MOB) {
                     int var10 = this.random.nextBoolean()?16777215:this.k();
                     var11 = var10 >> 16 & 255;
                     var12 = var10 >> 8 & 255;
                     var13 = var10 & 255;
                     this.world.a(EnumParticle.SPELL_MOB, this.locX + (double)var8, this.locY, this.locZ + (double)var9, (double)((float)var11 / 255.0F), (double)((float)var12 / 255.0F), (double)((float)var13 / 255.0F), new int[0]);
                  } else {
                     this.world.a(var3, this.locX + (double)var8, this.locY, this.locZ + (double)var9, 0.0D, 0.0D, 0.0D, var4);
                  }
               }
            }
         } else {
            float var17 = 3.1415927F * var2 * var2;
            int[] var20 = new int[var3.d()];

            for(int var22 = 0; (float)var22 < var17; ++var22) {
               var7 = this.random.nextFloat() * 6.2831855F;
               var8 = MathHelper.c(this.random.nextFloat()) * var2;
               var9 = MathHelper.b(var7) * var8;
               float var29 = MathHelper.a(var7) * var8;
               if(var3 == EnumParticle.SPELL_MOB) {
                  var11 = this.k();
                  var12 = var11 >> 16 & 255;
                  var13 = var11 >> 8 & 255;
                  int var14 = var11 & 255;
                  this.world.a(EnumParticle.SPELL_MOB, this.locX + (double)var9, this.locY, this.locZ + (double)var29, (double)((float)var12 / 255.0F), (double)((float)var13 / 255.0F), (double)((float)var14 / 255.0F), new int[0]);
               } else {
                  this.world.a(var3, this.locX + (double)var9, this.locY, this.locZ + (double)var29, (0.5D - this.random.nextDouble()) * 0.15D, 0.009999999776482582D, (0.5D - this.random.nextDouble()) * 0.15D, var20);
               }
            }
         }
      } else {
         if(this.ticksLived >= this.as + this.h) {
            this.S();
            return;
         }

         boolean var16 = this.ticksLived < this.as;
         if(var1 != var16) {
            this.a(var16);
         }

         if(var16) {
            return;
         }

         if(this.ax != 0.0F) {
            var2 += this.ax;
            if(var2 < 0.5F) {
               this.S();
               return;
            }

            this.a(var2);
         }

         if(this.ticksLived % 5 == 0) {
            Iterator var18 = this.g.entrySet().iterator();

            while(var18.hasNext()) {
               Entry var21 = (Entry)var18.next();
               if(this.ticksLived >= ((Integer)var21.getValue()).intValue()) {
                  var18.remove();
               }
            }

            ArrayList var19 = Lists.newArrayList();
            Iterator var23 = this.e.a().iterator();

            while(var23.hasNext()) {
               MobEffect var25 = (MobEffect)var23.next();
               var19.add(new MobEffect(var25.a(), var25.b() / 4, var25.c(), var25.d(), var25.e()));
            }

            var19.addAll(this.f);
            if(var19.isEmpty()) {
               this.g.clear();
            } else {
               List var24 = this.world.a(class_rz.class, this.bk());
               if(!var24.isEmpty()) {
                  Iterator var26 = var24.iterator();

                  while(true) {
                     class_rz var27;
                     double var31;
                     do {
                        do {
                           do {
                              if(!var26.hasNext()) {
                                 return;
                              }

                              var27 = (class_rz)var26.next();
                           } while(this.g.containsKey(var27));
                        } while(!var27.cC());

                        double var28 = var27.locX - this.locX;
                        double var30 = var27.locZ - this.locZ;
                        var31 = var28 * var28 + var30 * var30;
                     } while(var31 > (double)(var2 * var2));

                     this.g.put(var27, Integer.valueOf(this.ticksLived + this.at));
                     Iterator var32 = var19.iterator();

                     while(var32.hasNext()) {
                        MobEffect var15 = (MobEffect)var32.next();
                        if(var15.a().b()) {
                           var15.a().a(this, this.w(), var27, var15.c(), 0.5D);
                        } else {
                           var27.c(new MobEffect(var15));
                        }
                     }

                     if(this.aw != 0.0F) {
                        var2 += this.aw;
                        if(var2 < 0.5F) {
                           this.S();
                           return;
                        }

                        this.a(var2);
                     }

                     if(this.av != 0) {
                        this.h += this.av;
                        if(this.h <= 0) {
                           this.S();
                           return;
                        }
                     }
                  }
               }
            }
         }
      }

   }

   public void b(float var1) {
      this.aw = var1;
   }

   public void c(float var1) {
      this.ax = var1;
   }

   public void e(int var1) {
      this.as = var1;
   }

   public void a(class_rz var1) {
      this.ay = var1;
      this.az = var1 == null?null:var1.getUniqueId();
   }

   public class_rz w() {
      if(this.ay == null && this.az != null && this.world instanceof WorldServer) {
         Entity var1 = ((WorldServer)this.world).a(this.az);
         if(var1 instanceof class_rz) {
            this.ay = (class_rz)var1;
         }
      }

      return this.ay;
   }

   protected void a(NBTTagCompound var1) {
      this.ticksLived = var1.h("Age");
      this.h = var1.h("Duration");
      this.as = var1.h("WaitTime");
      this.at = var1.h("ReapplicationDelay");
      this.av = var1.h("DurationOnUse");
      this.aw = var1.j("RadiusOnUse");
      this.ax = var1.j("RadiusPerTick");
      this.a(var1.j("Radius"));
      this.az = var1.a("OwnerUUID");
      if(var1.b("Particle", 8)) {
         EnumParticle var2 = EnumParticle.a(var1.l("Particle"));
         if(var2 != null) {
            this.a(var2);
         }
      }

      if(var1.b("Color", 99)) {
         this.a(var1.h("Color"));
      }

      if(var1.b("Potion", 8)) {
         this.a(class_aff.c(var1));
      }

      if(var1.b("Effects", 9)) {
         NBTTagList var5 = var1.c("Effects", 10);
         this.f.clear();

         for(int var3 = 0; var3 < var5.c(); ++var3) {
            MobEffect var4 = MobEffect.b(var5.b(var3));
            if(var4 != null) {
               this.a(var4);
            }
         }
      }

   }

   protected void b(NBTTagCompound var1) {
      var1.a("Age", this.ticksLived);
      var1.a("Duration", this.h);
      var1.a("WaitTime", this.as);
      var1.a("ReapplicationDelay", this.at);
      var1.a("DurationOnUse", this.av);
      var1.a("RadiusOnUse", this.aw);
      var1.a("RadiusPerTick", this.ax);
      var1.a("Radius", this.j());
      var1.a("Particle", this.l().b());
      if(this.az != null) {
         var1.a("OwnerUUID", this.az);
      }

      if(this.au) {
         var1.a("Color", this.k());
      }

      if(this.e != class_afg.a && this.e != null) {
         var1.a("Potion", ((class_kk)class_afd.a.b(this.e)).toString());
      }

      if(!this.f.isEmpty()) {
         NBTTagList var2 = new NBTTagList();
         Iterator var3 = this.f.iterator();

         while(var3.hasNext()) {
            MobEffect var4 = (MobEffect)var3.next();
            var2.a((NBTTag)var4.a(new NBTTagCompound()));
         }

         var1.a((String)"Effects", (NBTTag)var2);
      }

   }

   public void a(class_ke var1) {
      if(a.equals(var1)) {
         this.a(this.j());
      }

      super.a(var1);
   }

   public class_axg z() {
      return class_axg.IGNORE;
   }
}
