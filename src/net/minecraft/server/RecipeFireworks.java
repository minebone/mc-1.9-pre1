package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import net.minecraft.server.InventoryCrafting;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.World;
import net.minecraft.server.class_act;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aft;

public class RecipeFireworks implements class_aft {
   private ItemStack a;

   public boolean a(InventoryCrafting var1, World var2) {
      this.a = null;
      int var3 = 0;
      int var4 = 0;
      int var5 = 0;
      int var6 = 0;
      int var7 = 0;
      int var8 = 0;

      for(int var9 = 0; var9 < var1.u_(); ++var9) {
         ItemStack var10 = var1.a(var9);
         if(var10 != null) {
            if(var10.b() == Items.J) {
               ++var4;
            } else if(var10.b() == Items.cm) {
               ++var6;
            } else if(var10.b() == Items.bd) {
               ++var5;
            } else if(var10.b() == Items.aR) {
               ++var3;
            } else if(var10.b() == Items.ba) {
               ++var7;
            } else if(var10.b() == Items.k) {
               ++var7;
            } else if(var10.b() == Items.bV) {
               ++var8;
            } else if(var10.b() == Items.I) {
               ++var8;
            } else if(var10.b() == Items.bE) {
               ++var8;
            } else {
               if(var10.b() != Items.ch) {
                  return false;
               }

               ++var8;
            }
         }
      }

      var7 += var5 + var8;
      if(var4 <= 3 && var3 <= 1) {
         NBTTagCompound var16;
         NBTTagCompound var19;
         if(var4 >= 1 && var3 == 1 && var7 == 0) {
            this.a = new ItemStack(Items.cl);
            if(var6 > 0) {
               var16 = new NBTTagCompound();
               var19 = new NBTTagCompound();
               NBTTagList var25 = new NBTTagList();

               for(int var22 = 0; var22 < var1.u_(); ++var22) {
                  ItemStack var26 = var1.a(var22);
                  if(var26 != null && var26.b() == Items.cm && var26.n() && var26.o().b("Explosion", 10)) {
                     var25.a((NBTTag)var26.o().o("Explosion"));
                  }
               }

               var19.a((String)"Explosions", (NBTTag)var25);
               var19.a("Flight", (byte)var4);
               var16.a((String)"Fireworks", (NBTTag)var19);
               this.a.d(var16);
            }

            return true;
         } else if(var4 == 1 && var3 == 0 && var6 == 0 && var5 > 0 && var8 <= 1) {
            this.a = new ItemStack(Items.cm);
            var16 = new NBTTagCompound();
            var19 = new NBTTagCompound();
            byte var23 = 0;
            ArrayList var12 = Lists.newArrayList();

            for(int var13 = 0; var13 < var1.u_(); ++var13) {
               ItemStack var14 = var1.a(var13);
               if(var14 != null) {
                  if(var14.b() == Items.bd) {
                     var12.add(Integer.valueOf(class_act.a[var14.i() & 15]));
                  } else if(var14.b() == Items.ba) {
                     var19.a("Flicker", true);
                  } else if(var14.b() == Items.k) {
                     var19.a("Trail", true);
                  } else if(var14.b() == Items.bV) {
                     var23 = 1;
                  } else if(var14.b() == Items.I) {
                     var23 = 4;
                  } else if(var14.b() == Items.bE) {
                     var23 = 2;
                  } else if(var14.b() == Items.ch) {
                     var23 = 3;
                  }
               }
            }

            int[] var24 = new int[var12.size()];

            for(int var27 = 0; var27 < var24.length; ++var27) {
               var24[var27] = ((Integer)var12.get(var27)).intValue();
            }

            var19.a("Colors", var24);
            var19.a("Type", var23);
            var16.a((String)"Explosion", (NBTTag)var19);
            this.a.d(var16);
            return true;
         } else if(var4 == 0 && var3 == 0 && var6 == 1 && var5 > 0 && var5 == var7) {
            ArrayList var15 = Lists.newArrayList();

            for(int var17 = 0; var17 < var1.u_(); ++var17) {
               ItemStack var11 = var1.a(var17);
               if(var11 != null) {
                  if(var11.b() == Items.bd) {
                     var15.add(Integer.valueOf(class_act.a[var11.i() & 15]));
                  } else if(var11.b() == Items.cm) {
                     this.a = var11.k();
                     this.a.b = 1;
                  }
               }
            }

            int[] var18 = new int[var15.size()];

            for(int var20 = 0; var20 < var18.length; ++var20) {
               var18[var20] = ((Integer)var15.get(var20)).intValue();
            }

            if(this.a != null && this.a.n()) {
               NBTTagCompound var21 = this.a.o().o("Explosion");
               if(var21 == null) {
                  return false;
               } else {
                  var21.a("FadeColors", var18);
                  return true;
               }
            } else {
               return false;
            }
         } else {
            return false;
         }
      } else {
         return false;
      }
   }

   public ItemStack a(InventoryCrafting var1) {
      return this.a.k();
   }

   public int a() {
      return 10;
   }

   public ItemStack b() {
      return this.a;
   }

   public ItemStack[] b(InventoryCrafting var1) {
      ItemStack[] var2 = new ItemStack[var1.u_()];

      for(int var3 = 0; var3 < var2.length; ++var3) {
         ItemStack var4 = var1.a(var3);
         if(var4 != null && var4.b().r()) {
            var2[var3] = new ItemStack(var4.b().q());
         }
      }

      return var2;
   }
}
