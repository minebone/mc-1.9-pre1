package net.minecraft.server;

import java.io.IOException;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayInListener;

public class PacketPlayInSteer implements Packet {
   private boolean a;
   private boolean b;

   public PacketPlayInSteer() {
   }

   public PacketPlayInSteer(boolean var1, boolean var2) {
      this.a = var1;
      this.b = var2;
   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.readBoolean();
      this.b = var1.readBoolean();
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.writeBoolean(this.a);
      var1.writeBoolean(this.b);
   }

   public void a(PacketPlayInListener var1) {
      var1.a(this);
   }

   public boolean a() {
      return this.a;
   }

   public boolean b() {
      return this.b;
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketPlayInListener)var1);
   }
}
