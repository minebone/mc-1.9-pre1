package net.minecraft.server;

import java.util.Arrays;
import java.util.concurrent.Callable;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.CrashReport;
import net.minecraft.server.CrashReportSystemDetails;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.GameProfileSerializer;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.Item;
import net.minecraft.server.ItemArmor;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_e;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_qg;
import net.minecraft.server.class_rz;

public class PlayerInventory implements IInventory {
   public final ItemStack[] a = new ItemStack[36];
   public final ItemStack[] b = new ItemStack[4];
   public final ItemStack[] c = new ItemStack[1];
   private final ItemStack[][] g;
   public int d;
   public EntityHuman e;
   private ItemStack h;
   public boolean f;

   public PlayerInventory(EntityHuman var1) {
      this.g = new ItemStack[][]{this.a, this.b, this.c};
      this.e = var1;
   }

   public ItemStack h() {
      return e(this.d)?this.a[this.d]:null;
   }

   public static int i() {
      return 9;
   }

   private boolean a(ItemStack var1, ItemStack var2) {
      return var1 != null && this.b(var1, var2) && var1.d() && var1.b < var1.c() && var1.b < this.w_();
   }

   private boolean b(ItemStack var1, ItemStack var2) {
      return var1.b() == var2.b() && (!var1.f() || var1.i() == var2.i()) && ItemStack.a(var1, var2);
   }

   public int j() {
      for(int var1 = 0; var1 < this.a.length; ++var1) {
         if(this.a[var1] == null) {
            return var1;
         }
      }

      return -1;
   }

   public void d(int var1) {
      this.d = this.k();
      ItemStack var2 = this.a[this.d];
      this.a[this.d] = this.a[var1];
      this.a[var1] = var2;
   }

   public static boolean e(int var0) {
      return var0 >= 0 && var0 < 9;
   }

   public int k() {
      int var1;
      int var2;
      for(var1 = 0; var1 < 9; ++var1) {
         var2 = (this.d + var1) % 9;
         if(this.a[var2] == null) {
            return var2;
         }
      }

      for(var1 = 0; var1 < 9; ++var1) {
         var2 = (this.d + var1) % 9;
         if(!this.a[var2].w()) {
            return var2;
         }
      }

      return this.d;
   }

   public int a(Item var1, int var2, int var3, NBTTagCompound var4) {
      int var5 = 0;

      int var6;
      for(var6 = 0; var6 < this.u_(); ++var6) {
         ItemStack var7 = this.a(var6);
         if(var7 != null && (var1 == null || var7.b() == var1) && (var2 <= -1 || var7.i() == var2) && (var4 == null || GameProfileSerializer.a(var4, var7.o(), true))) {
            int var8 = var3 <= 0?var7.b:Math.min(var3 - var5, var7.b);
            var5 += var8;
            if(var3 != 0) {
               var7.b -= var8;
               if(var7.b == 0) {
                  this.a(var6, (ItemStack)null);
               }

               if(var3 > 0 && var5 >= var3) {
                  return var5;
               }
            }
         }
      }

      if(this.h != null) {
         if(var1 != null && this.h.b() != var1) {
            return var5;
         }

         if(var2 > -1 && this.h.i() != var2) {
            return var5;
         }

         if(var4 != null && !GameProfileSerializer.a(var4, this.h.o(), true)) {
            return var5;
         }

         var6 = var3 <= 0?this.h.b:Math.min(var3 - var5, this.h.b);
         var5 += var6;
         if(var3 != 0) {
            this.h.b -= var6;
            if(this.h.b == 0) {
               this.h = null;
            }

            if(var3 > 0 && var5 >= var3) {
               return var5;
            }
         }
      }

      return var5;
   }

   private int g(ItemStack var1) {
      Item var2 = var1.b();
      int var3 = var1.b;
      int var4 = this.h(var1);
      if(var4 == -1) {
         var4 = this.j();
      }

      if(var4 == -1) {
         return var3;
      } else {
         ItemStack var5 = this.a(var4);
         if(var5 == null) {
            var5 = new ItemStack(var2, 0, var1.i());
            if(var1.n()) {
               var5.d((NBTTagCompound)var1.o().b());
            }

            this.a(var4, var5);
         }

         int var6 = var3;
         if(var3 > var5.c() - var5.b) {
            var6 = var5.c() - var5.b;
         }

         if(var6 > this.w_() - var5.b) {
            var6 = this.w_() - var5.b;
         }

         if(var6 == 0) {
            return var3;
         } else {
            var3 -= var6;
            var5.b += var6;
            var5.c = 5;
            return var3;
         }
      }
   }

   private int h(ItemStack var1) {
      if(this.a(this.a(this.d), var1)) {
         return this.d;
      } else if(this.a(this.a(40), var1)) {
         return 40;
      } else {
         for(int var2 = 0; var2 < this.a.length; ++var2) {
            if(this.a(this.a[var2], var1)) {
               return var2;
            }
         }

         return -1;
      }
   }

   public void m() {
      for(int var1 = 0; var1 < this.g.length; ++var1) {
         ItemStack[] var2 = this.g[var1];

         for(int var3 = 0; var3 < var2.length; ++var3) {
            if(var2[var3] != null) {
               var2[var3].a(this.e.world, this.e, var3, this.d == var3);
            }
         }
      }

   }

   public boolean c(final ItemStack var1) {
      if(var1 != null && var1.b != 0 && var1.b() != null) {
         try {
            int var2;
            if(var1.g()) {
               var2 = this.j();
               if(var2 >= 0) {
                  this.a[var2] = ItemStack.c(var1);
                  this.a[var2].c = 5;
                  var1.b = 0;
                  return true;
               } else if(this.e.abilities.d) {
                  var1.b = 0;
                  return true;
               } else {
                  return false;
               }
            } else {
               do {
                  var2 = var1.b;
                  var1.b = this.g(var1);
               } while(var1.b > 0 && var1.b < var2);

               if(var1.b == var2 && this.e.abilities.d) {
                  var1.b = 0;
                  return true;
               } else {
                  return var1.b < var2;
               }
            }
         } catch (Throwable var5) {
            CrashReport var3 = CrashReport.a(var5, "Adding item to inventory");
            CrashReportSystemDetails var4 = var3.a("Item being added");
            var4.a((String)"Item ID", (Object)Integer.valueOf(Item.a(var1.b())));
            var4.a((String)"Item data", (Object)Integer.valueOf(var1.i()));
            var4.a("Item name", new Callable() {
               public String a() throws Exception {
                  return var1.q();
               }

               // $FF: synthetic method
               public Object call() throws Exception {
                  return this.a();
               }
            });
            throw new class_e(var3);
         }
      } else {
         return false;
      }
   }

   public ItemStack a(int var1, int var2) {
      ItemStack[] var3 = null;
      ItemStack[][] var4 = this.g;
      int var5 = var4.length;

      for(int var6 = 0; var6 < var5; ++var6) {
         ItemStack[] var7 = var4[var6];
         if(var1 < var7.length) {
            var3 = var7;
            break;
         }

         var1 -= var7.length;
      }

      return var3 != null && var3[var1] != null?class_qg.a(var3, var1, var2):null;
   }

   public void d(ItemStack var1) {
      ItemStack[][] var2 = this.g;
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         ItemStack[] var5 = var2[var4];

         for(int var6 = 0; var6 < var5.length; ++var6) {
            if(var5[var6] == var1) {
               var5[var6] = null;
               break;
            }
         }
      }

   }

   public ItemStack b(int var1) {
      ItemStack[] var2 = null;
      ItemStack[][] var3 = this.g;
      int var4 = var3.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         ItemStack[] var6 = var3[var5];
         if(var1 < var6.length) {
            var2 = var6;
            break;
         }

         var1 -= var6.length;
      }

      if(var2 != null && var2[var1] != null) {
         Object var7 = var2[var1];
         var2[var1] = null;
         return (ItemStack)var7;
      } else {
         return null;
      }
   }

   public void a(int var1, ItemStack var2) {
      ItemStack[] var3 = null;
      ItemStack[][] var4 = this.g;
      int var5 = var4.length;

      for(int var6 = 0; var6 < var5; ++var6) {
         ItemStack[] var7 = var4[var6];
         if(var1 < var7.length) {
            var3 = var7;
            break;
         }

         var1 -= var7.length;
      }

      if(var3 != null) {
         var3[var1] = var2;
      }

   }

   public float a(IBlockData var1) {
      float var2 = 1.0F;
      if(this.a[this.d] != null) {
         var2 *= this.a[this.d].a(var1);
      }

      return var2;
   }

   public NBTTagList a(NBTTagList var1) {
      int var2;
      NBTTagCompound var3;
      for(var2 = 0; var2 < this.a.length; ++var2) {
         if(this.a[var2] != null) {
            var3 = new NBTTagCompound();
            var3.a("Slot", (byte)var2);
            this.a[var2].b(var3);
            var1.a((NBTTag)var3);
         }
      }

      for(var2 = 0; var2 < this.b.length; ++var2) {
         if(this.b[var2] != null) {
            var3 = new NBTTagCompound();
            var3.a("Slot", (byte)(var2 + 100));
            this.b[var2].b(var3);
            var1.a((NBTTag)var3);
         }
      }

      for(var2 = 0; var2 < this.c.length; ++var2) {
         if(this.c[var2] != null) {
            var3 = new NBTTagCompound();
            var3.a("Slot", (byte)(var2 + 150));
            this.c[var2].b(var3);
            var1.a((NBTTag)var3);
         }
      }

      return var1;
   }

   public void b(NBTTagList var1) {
      Arrays.fill(this.a, (Object)null);
      Arrays.fill(this.b, (Object)null);
      Arrays.fill(this.c, (Object)null);

      for(int var2 = 0; var2 < var1.c(); ++var2) {
         NBTTagCompound var3 = var1.b(var2);
         int var4 = var3.f("Slot") & 255;
         ItemStack var5 = ItemStack.a(var3);
         if(var5 != null) {
            if(var4 >= 0 && var4 < this.a.length) {
               this.a[var4] = var5;
            } else if(var4 >= 100 && var4 < this.b.length + 100) {
               this.b[var4 - 100] = var5;
            } else if(var4 >= 150 && var4 < this.c.length + 150) {
               this.c[var4 - 150] = var5;
            }
         }
      }

   }

   public int u_() {
      return this.a.length + this.b.length + this.c.length;
   }

   public ItemStack a(int var1) {
      ItemStack[] var2 = null;
      ItemStack[][] var3 = this.g;
      int var4 = var3.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         ItemStack[] var6 = var3[var5];
         if(var1 < var6.length) {
            var2 = var6;
            break;
         }

         var1 -= var6.length;
      }

      return var2 == null?null:var2[var1];
   }

   public String h_() {
      return "container.inventory";
   }

   public boolean o_() {
      return false;
   }

   public IChatBaseComponent i_() {
      return (IChatBaseComponent)(this.o_()?new ChatComponentText(this.h_()):new ChatMessage(this.h_(), new Object[0]));
   }

   public int w_() {
      return 64;
   }

   public boolean b(IBlockData var1) {
      if(var1.getMaterial().l()) {
         return true;
      } else {
         ItemStack var2 = this.a(this.d);
         return var2 != null?var2.b(var1):false;
      }
   }

   public void a(float var1) {
      var1 /= 4.0F;
      if(var1 < 1.0F) {
         var1 = 1.0F;
      }

      for(int var2 = 0; var2 < this.b.length; ++var2) {
         if(this.b[var2] != null && this.b[var2].b() instanceof ItemArmor) {
            this.b[var2].a((int)var1, (class_rz)this.e);
            if(this.b[var2].b == 0) {
               this.b[var2] = null;
            }
         }
      }

   }

   public void n() {
      ItemStack[][] var1 = this.g;
      int var2 = var1.length;

      for(int var3 = 0; var3 < var2; ++var3) {
         ItemStack[] var4 = var1[var3];

         for(int var5 = 0; var5 < var4.length; ++var5) {
            if(var4[var5] != null) {
               this.e.a(var4[var5], true, false);
               var4[var5] = null;
            }
         }
      }

   }

   public void v_() {
      this.f = true;
   }

   public void e(ItemStack var1) {
      this.h = var1;
   }

   public ItemStack o() {
      return this.h;
   }

   public boolean a(EntityHuman var1) {
      return this.e.dead?false:var1.h(this.e) <= 64.0D;
   }

   public boolean f(ItemStack var1) {
      ItemStack[][] var2 = this.g;
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         ItemStack[] var5 = var2[var4];

         for(int var6 = 0; var6 < var5.length; ++var6) {
            if(var5[var6] != null && var5[var6].a(var1)) {
               return true;
            }
         }
      }

      return false;
   }

   public void b(EntityHuman var1) {
   }

   public void c(EntityHuman var1) {
   }

   public boolean b(int var1, ItemStack var2) {
      return true;
   }

   public void a(PlayerInventory var1) {
      for(int var2 = 0; var2 < this.u_(); ++var2) {
         this.a(var2, var1.a(var2));
      }

      this.d = var1.d;
   }

   public int c_(int var1) {
      return 0;
   }

   public void b(int var1, int var2) {
   }

   public int g() {
      return 0;
   }

   public void l() {
      ItemStack[][] var1 = this.g;
      int var2 = var1.length;

      for(int var3 = 0; var3 < var2; ++var3) {
         ItemStack[] var4 = var1[var3];

         for(int var5 = 0; var5 < var4.length; ++var5) {
            var4[var5] = null;
         }
      }

   }
}
