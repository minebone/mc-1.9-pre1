package net.minecraft.server;

import net.minecraft.server.class_sk;

public abstract class AttributeBase implements class_sk {
   private final class_sk a;
   private final String b;
   private final double c;
   private boolean d;

   protected AttributeBase(class_sk var1, String var2, double var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      if(var2 == null) {
         throw new IllegalArgumentException("Name cannot be null!");
      }
   }

   public String a() {
      return this.b;
   }

   public double b() {
      return this.c;
   }

   public boolean c() {
      return this.d;
   }

   public AttributeBase a(boolean var1) {
      this.d = var1;
      return this;
   }

   public class_sk d() {
      return this.a;
   }

   public int hashCode() {
      return this.b.hashCode();
   }

   public boolean equals(Object var1) {
      return var1 instanceof class_sk && this.b.equals(((class_sk)var1).a());
   }
}
