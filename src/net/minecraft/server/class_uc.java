package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import net.minecraft.server.Entity;
import net.minecraft.server.Vec3D;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_vl;
import net.minecraft.server.class_zd;

public class class_uc extends class_tj {
   private class_zd a;
   private class_rz b;
   private double c;
   private int d;

   public class_uc(class_zd var1, double var2) {
      this.a = var1;
      this.c = var2;
      this.a(1);
   }

   public boolean a() {
      if(this.a.l() >= 0) {
         return false;
      } else if(this.a.bE().nextInt(400) != 0) {
         return false;
      } else {
         List var1 = this.a.world.a(class_zd.class, this.a.bk().b(6.0D, 3.0D, 6.0D));
         double var2 = Double.MAX_VALUE;
         Iterator var4 = var1.iterator();

         while(var4.hasNext()) {
            class_zd var5 = (class_zd)var4.next();
            if(var5 != this.a && !var5.db() && var5.l() < 0) {
               double var6 = var5.h(this.a);
               if(var6 <= var2) {
                  var2 = var6;
                  this.b = var5;
               }
            }
         }

         if(this.b == null) {
            Vec3D var8 = class_vl.a(this.a, 16, 3);
            if(var8 == null) {
               return false;
            }
         }

         return true;
      }
   }

   public boolean b() {
      return this.d > 0;
   }

   public void c() {
      if(this.b != null) {
         this.a.p(true);
      }

      this.d = 1000;
   }

   public void d() {
      this.a.p(false);
      this.b = null;
   }

   public void e() {
      --this.d;
      if(this.b != null) {
         if(this.a.h(this.b) > 4.0D) {
            this.a.x().a((Entity)this.b, this.c);
         }
      } else if(this.a.x().n()) {
         Vec3D var1 = class_vl.a(this.a, 16, 3);
         if(var1 == null) {
            return;
         }

         this.a.x().a(var1.b, var1.c, var1.d, this.c);
      }

   }
}
