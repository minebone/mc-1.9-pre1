package net.minecraft.server;

import java.util.Iterator;
import net.minecraft.server.ChatBaseComponent;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.Scoreboard;
import net.minecraft.server.ScoreboardScore;
import net.minecraft.server.UtilColor;
import net.minecraft.server.class_bbk;
import net.minecraft.server.ICommandListener;

public class ChatComponentScore extends ChatBaseComponent {
   private final String b;
   private final String c;
   private String d = "";

   public ChatComponentScore(String var1, String var2) {
      this.b = var1;
      this.c = var2;
   }

   public String g() {
      return this.b;
   }

   public String h() {
      return this.c;
   }

   public void b(String var1) {
      this.d = var1;
   }

   public String e() {
      return this.d;
   }

   public void a(ICommandListener var1) {
      MinecraftServer var2 = var1.h();
      if(var2 != null && var2.M() && UtilColor.b(this.d)) {
         Scoreboard var3 = var2.a(0).ad();
         class_bbk var4 = var3.b(this.c);
         if(var3.b(this.b, var4)) {
            ScoreboardScore var5 = var3.c(this.b, var4);
            this.b(String.format("%d", new Object[]{Integer.valueOf(var5.c())}));
            return;
         }
      }

      this.d = "";
   }

   public ChatComponentScore i() {
      ChatComponentScore var1 = new ChatComponentScore(this.b, this.c);
      var1.b(this.d);
      var1.a(this.b().m());
      Iterator var2 = this.a().iterator();

      while(var2.hasNext()) {
         IChatBaseComponent var3 = (IChatBaseComponent)var2.next();
         var1.a(var3.f());
      }

      return var1;
   }

   public boolean equals(Object var1) {
      if(this == var1) {
         return true;
      } else if(!(var1 instanceof ChatComponentScore)) {
         return false;
      } else {
         ChatComponentScore var2 = (ChatComponentScore)var1;
         return this.b.equals(var2.b) && this.c.equals(var2.c) && super.equals(var1);
      }
   }

   public String toString() {
      return "ScoreComponent{name=\'" + this.b + '\'' + "objective=\'" + this.c + '\'' + ", siblings=" + this.a + ", style=" + this.b() + '}';
   }

   // $FF: synthetic method
   public IChatBaseComponent f() {
      return this.i();
   }
}
