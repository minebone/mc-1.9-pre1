package net.minecraft.server;

import net.minecraft.server.BiomeBase;
import net.minecraft.server.IntCache;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_axu;

public class class_aya extends class_axu {
   private class_axu c;
   private class_axu d;

   public class_aya(long var1, class_axu var3, class_axu var4) {
      super(var1);
      this.c = var3;
      this.d = var4;
   }

   public void a(long var1) {
      this.c.a(var1);
      this.d.a(var1);
      super.a(var1);
   }

   public int[] a(int var1, int var2, int var3, int var4) {
      int[] var5 = this.c.a(var1, var2, var3, var4);
      int[] var6 = this.d.a(var1, var2, var3, var4);
      int[] var7 = IntCache.a(var3 * var4);

      for(int var8 = 0; var8 < var3 * var4; ++var8) {
         if(var5[var8] != BiomeBase.a(class_aik.a) && var5[var8] != BiomeBase.a(class_aik.z)) {
            if(var6[var8] == BiomeBase.a(class_aik.i)) {
               if(var5[var8] == BiomeBase.a(class_aik.n)) {
                  var7[var8] = BiomeBase.a(class_aik.m);
               } else if(var5[var8] != BiomeBase.a(class_aik.p) && var5[var8] != BiomeBase.a(class_aik.q)) {
                  var7[var8] = var6[var8] & 255;
               } else {
                  var7[var8] = BiomeBase.a(class_aik.q);
               }
            } else {
               var7[var8] = var5[var8];
            }
         } else {
            var7[var8] = var5[var8];
         }
      }

      return var7;
   }
}
