package net.minecraft.server;

import java.util.Arrays;
import java.util.List;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumChatFormat;
import net.minecraft.server.ExceptionPlayerNotFound;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;

public class CommandTell extends CommandAbstract {
   public List b() {
      return Arrays.asList(new String[]{"w", "msg"});
   }

   public String c() {
      return "tell";
   }

   public int a() {
      return 0;
   }

   public String b(ICommandListener var1) {
      return "commands.message.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 2) {
         throw new class_cf("commands.message.usage", new Object[0]);
      } else {
         EntityPlayer var4 = a(var1, var2, var3[0]);
         if(var4 == var2) {
            throw new ExceptionPlayerNotFound("commands.message.sameTarget", new Object[0]);
         } else {
            IChatBaseComponent var5 = b(var2, var3, 1, !(var2 instanceof EntityHuman));
            ChatMessage var6 = new ChatMessage("commands.message.display.incoming", new Object[]{var2.i_(), var5.f()});
            ChatMessage var7 = new ChatMessage("commands.message.display.outgoing", new Object[]{var4.i_(), var5.f()});
            var6.b().a(EnumChatFormat.GRAY).b(Boolean.valueOf(true));
            var7.b().a(EnumChatFormat.GRAY).b(Boolean.valueOf(true));
            var4.a((IChatBaseComponent)var6);
            var2.a(var7);
         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return a(var3, var1.J());
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 0;
   }
}
