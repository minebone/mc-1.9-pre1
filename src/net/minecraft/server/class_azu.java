package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import java.util.Collection;
import java.util.Random;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.Item;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_azw;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_bae;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_bar;
import net.minecraft.server.class_kk;

public class class_azu extends class_azw {
   protected final Item a;
   protected final class_bae[] b;

   public class_azu(Item var1, int var2, int var3, class_bae[] var4, class_baq[] var5) {
      super(var2, var3, var5);
      this.a = var1;
      this.b = var4;
   }

   public void a(Collection var1, Random var2, class_azy var3) {
      ItemStack var4 = new ItemStack(this.a);
      int var5 = 0;

      for(int var6 = this.b.length; var5 < var6; ++var5) {
         class_bae var7 = this.b[var5];
         if(class_bar.a(var7.a(), var2, var3)) {
            var4 = var7.a(var4, var2, var3);
         }
      }

      if(var4.b > 0) {
         if(var4.b < this.a.j()) {
            var1.add(var4);
         } else {
            var5 = var4.b;

            while(var5 > 0) {
               ItemStack var8 = var4.k();
               var8.b = Math.min(var4.c(), var5);
               var5 -= var8.b;
               var1.add(var8);
            }
         }
      }

   }

   protected void a(JsonObject var1, JsonSerializationContext var2) {
      if(this.b != null && this.b.length > 0) {
         var1.add("functions", var2.serialize(this.b));
      }

      class_kk var3 = (class_kk)Item.f.b(this.a);
      if(var3 == null) {
         throw new IllegalArgumentException("Can\'t serialize unknown item " + this.a);
      } else {
         var1.addProperty("name", var3.toString());
      }
   }

   public static class_azu a(JsonObject var0, JsonDeserializationContext var1, int var2, int var3, class_baq[] var4) {
      Item var5 = ChatDeserializer.i(var0, "name");
      class_bae[] var6;
      if(var0.has("functions")) {
         var6 = (class_bae[])ChatDeserializer.a(var0, "functions", var1, class_bae[].class);
      } else {
         var6 = new class_bae[0];
      }

      return new class_azu(var5, var2, var3, var6, var4);
   }
}
