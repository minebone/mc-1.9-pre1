package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_akq;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;
import net.minecraft.server.EnumHand;

public class BlockRepeater extends class_akq {
   public static final class_arm a = class_arm.a("locked");
   public static final BlockStateInteger b = BlockStateInteger.a("delay", 1, 4);

   protected BlockRepeater(boolean var1) {
      super(var1);
      this.w(this.A.b().set(D, EnumDirection.NORTH).set(b, Integer.valueOf(1)).set(a, Boolean.valueOf(false)));
   }

   public String c() {
      return class_di.a("item.diode.name");
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return var1.set(a, Boolean.valueOf(this.b(var2, var3, var1)));
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(D, var2.a((EnumDirection)var1.get(D)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(D)));
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(!var4.abilities.e) {
         return false;
      } else {
         var1.a((BlockPosition)var2, (IBlockData)var3.a(b), 3);
         return true;
      }
   }

   protected int i(IBlockData var1) {
      return ((Integer)var1.get(b)).intValue() * 2;
   }

   protected IBlockData x(IBlockData var1) {
      Integer var2 = (Integer)var1.get(b);
      Boolean var3 = (Boolean)var1.get(a);
      EnumDirection var4 = (EnumDirection)var1.get(D);
      return Blocks.bc.u().set(D, var4).set(b, var2).set(a, var3);
   }

   protected IBlockData y(IBlockData var1) {
      Integer var2 = (Integer)var1.get(b);
      Boolean var3 = (Boolean)var1.get(a);
      EnumDirection var4 = (EnumDirection)var1.get(D);
      return Blocks.bb.u().set(D, var4).set(b, var2).set(a, var3);
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Items.bi;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Items.bi);
   }

   public boolean b(class_ahw var1, BlockPosition var2, IBlockData var3) {
      return this.c(var1, var2, var3) > 0;
   }

   protected boolean A(IBlockData var1) {
      return B(var1);
   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      super.b((World)var1, var2, (IBlockData)var3);
      this.h(var1, var2, var3);
   }

   public IBlockData a(int var1) {
      return this.u().set(D, EnumDirection.b(var1)).set(a, Boolean.valueOf(false)).set(b, Integer.valueOf(1 + (var1 >> 2)));
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((EnumDirection)var1.get(D)).b();
      var3 |= ((Integer)var1.get(b)).intValue() - 1 << 2;
      return var3;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{D, b, a});
   }
}
