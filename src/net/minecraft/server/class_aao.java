package net.minecraft.server;

import net.minecraft.server.BlockMinecartTrackAbstract;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityMinecartAbstract;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aho;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.class_zl;

public class class_aao extends EntityMinecartAbstract {
   private int a = -1;

   public class_aao(World var1) {
      super(var1);
   }

   public class_aao(World var1, double var2, double var4, double var6) {
      super(var1, var2, var4, var6);
   }

   public EntityMinecartAbstract.EnumMinecartType v() {
      return EntityMinecartAbstract.EnumMinecartType.TNT;
   }

   public IBlockData x() {
      return Blocks.W.u();
   }

   public void m() {
      super.m();
      if(this.a > 0) {
         --this.a;
         this.world.a(EnumParticle.SMOKE_NORMAL, this.locX, this.locY + 0.5D, this.locZ, 0.0D, 0.0D, 0.0D, new int[0]);
      } else if(this.a == 0) {
         this.c(this.motX * this.motX + this.motZ * this.motZ);
      }

      if(this.positionChanged) {
         double var1 = this.motX * this.motX + this.motZ * this.motZ;
         if(var1 >= 0.009999999776482582D) {
            this.c(var1);
         }
      }

   }

   public boolean a(DamageSource var1, float var2) {
      Entity var3 = var1.i();
      if(var3 instanceof class_zl) {
         class_zl var4 = (class_zl)var3;
         if(var4.aG()) {
            this.c(var4.motX * var4.motX + var4.motY * var4.motY + var4.motZ * var4.motZ);
         }
      }

      return super.a(var1, var2);
   }

   public void a(DamageSource var1) {
      super.a(var1);
      double var2 = this.motX * this.motX + this.motZ * this.motZ;
      if(!var1.c() && this.world.U().b("doEntityDrops")) {
         this.a(new ItemStack(Blocks.W, 1), 0.0F);
      }

      if(var1.o() || var1.c() || var2 >= 0.009999999776482582D) {
         this.c(var2);
      }

   }

   protected void c(double var1) {
      if(!this.world.E) {
         double var3 = Math.sqrt(var1);
         if(var3 > 5.0D) {
            var3 = 5.0D;
         }

         this.world.a(this, this.locX, this.locY, this.locZ, (float)(4.0D + this.random.nextDouble() * 1.5D * var3), true);
         this.S();
      }

   }

   public void e(float var1, float var2) {
      if(var1 >= 3.0F) {
         float var3 = var1 / 10.0F;
         this.c((double)(var3 * var3));
      }

      super.e(var1, var2);
   }

   public void a(int var1, int var2, int var3, boolean var4) {
      if(var4 && this.a < 0) {
         this.j();
      }

   }

   public void j() {
      this.a = 80;
      if(!this.world.E) {
         this.world.a((Entity)this, (byte)10);
         if(!this.ac()) {
            this.world.a((EntityHuman)null, this.locX, this.locY, this.locZ, class_ng.gd, EnumSoundCategory.BLOCKS, 1.0F, 1.0F);
         }
      }

   }

   public boolean l() {
      return this.a > -1;
   }

   public float a(class_aho var1, World var2, BlockPosition var3, IBlockData var4) {
      return !this.l() || !BlockMinecartTrackAbstract.i(var4) && !BlockMinecartTrackAbstract.b(var2, var3.a())?super.a(var1, var2, var3, var4):0.0F;
   }

   public boolean a(class_aho var1, World var2, BlockPosition var3, IBlockData var4, float var5) {
      return !this.l() || !BlockMinecartTrackAbstract.i(var4) && !BlockMinecartTrackAbstract.b(var2, var3.a())?super.a(var1, var2, var3, var4, var5):false;
   }

   protected void a(NBTTagCompound var1) {
      super.a(var1);
      if(var1.b("TNTFuse", 99)) {
         this.a = var1.h("TNTFuse");
      }

   }

   protected void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("TNTFuse", this.a);
   }
}
