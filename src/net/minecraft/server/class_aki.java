package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EnumColor;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;

public class class_aki extends Block {
   public static final BlockStateEnum a = BlockStateEnum.a("color", EnumColor.class);

   public class_aki(Material var1) {
      super(var1);
      this.w(this.A.b().set(a, EnumColor.WHITE));
      this.a(CreativeModeTab.b);
   }

   public int d(IBlockData var1) {
      return ((EnumColor)var1.get(a)).a();
   }

   public MaterialMapColor r(IBlockData var1) {
      return ((EnumColor)var1.get(a)).e();
   }

   public IBlockData a(int var1) {
      return this.u().set(a, EnumColor.b(var1));
   }

   public int e(IBlockData var1) {
      return ((EnumColor)var1.get(a)).a();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }
}
