package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.class_adj;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;

public class class_aeh extends class_adj {
   private Block b;
   private Block c;

   public class_aeh(int var1, float var2, Block var3, Block var4) {
      super(var1, var2, false);
      this.b = var3;
      this.c = var4;
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      if(var6 == EnumDirection.UP && var2.a(var4.a(var6), var6, var1) && var3.getType(var4).getBlock() == this.c && var3.d(var4.a())) {
         var3.a((BlockPosition)var4.a(), (IBlockData)this.b.u(), 11);
         --var1.b;
         return EnumResult.SUCCESS;
      } else {
         return EnumResult.FAIL;
      }
   }
}
