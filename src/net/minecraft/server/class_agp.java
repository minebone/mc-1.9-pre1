package net.minecraft.server;

import net.minecraft.server.Bootstrap;
import net.minecraft.server.Enchantment;
import net.minecraft.server.class_kk;

public class class_agp {
   public static final Enchantment a = a("protection");
   public static final Enchantment b = a("fire_protection");
   public static final Enchantment c = a("feather_falling");
   public static final Enchantment d = a("blast_protection");
   public static final Enchantment e = a("projectile_protection");
   public static final Enchantment f = a("respiration");
   public static final Enchantment g = a("aqua_affinity");
   public static final Enchantment h = a("thorns");
   public static final Enchantment i = a("depth_strider");
   public static final Enchantment j = a("frost_walker");
   public static final Enchantment k = a("sharpness");
   public static final Enchantment l = a("smite");
   public static final Enchantment m = a("bane_of_arthropods");
   public static final Enchantment n = a("knockback");
   public static final Enchantment o = a("fire_aspect");
   public static final Enchantment p = a("looting");
   public static final Enchantment q = a("efficiency");
   public static final Enchantment r = a("silk_touch");
   public static final Enchantment s = a("unbreaking");
   public static final Enchantment t = a("fortune");
   public static final Enchantment u = a("power");
   public static final Enchantment v = a("punch");
   public static final Enchantment w = a("flame");
   public static final Enchantment x = a("infinity");
   public static final Enchantment y = a("luck_of_the_sea");
   public static final Enchantment z = a("lure");
   public static final Enchantment A = a("mending");

   private static Enchantment a(String var0) {
      Enchantment var1 = (Enchantment)Enchantment.b.c(new class_kk(var0));
      if(var1 == null) {
         throw new IllegalStateException("Invalid Enchantment requested: " + var0);
      } else {
         return var1;
      }
   }

   static {
      if(!Bootstrap.a()) {
         throw new RuntimeException("Accessed MobEffects before Bootstrap!");
      }
   }
}
