package net.minecraft.server;

import com.google.common.base.Predicate;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockMinecartTrackAbstract;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Container;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityMinecartAbstract;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.class_aai;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_ru;

public class class_akp extends BlockMinecartTrackAbstract {
   public static final BlockStateEnum d = BlockStateEnum.a("shape", BlockMinecartTrackAbstract.EnumTrackPosition.class, new Predicate() {
      public boolean a(BlockMinecartTrackAbstract.EnumTrackPosition var1) {
         return var1 != BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST && var1 != BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST && var1 != BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST && var1 != BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST;
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((BlockMinecartTrackAbstract.EnumTrackPosition)var1);
      }
   });
   public static final class_arm e = class_arm.a("powered");

   public class_akp() {
      super(true);
      this.w(this.A.b().set(e, Boolean.valueOf(false)).set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH));
      this.a(true);
   }

   public int a(World var1) {
      return 20;
   }

   public boolean g(IBlockData var1) {
      return true;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Entity var4) {
      if(!var1.E) {
         if(!((Boolean)var3.get(e)).booleanValue()) {
            this.e(var1, var2, var3);
         }
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Random var4) {
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(!var1.E && ((Boolean)var3.get(e)).booleanValue()) {
         this.e(var1, var2, var3);
      }
   }

   public int b(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return ((Boolean)var1.get(e)).booleanValue()?15:0;
   }

   public int c(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return !((Boolean)var1.get(e)).booleanValue()?0:(var4 == EnumDirection.UP?15:0);
   }

   private void e(World var1, BlockPosition var2, IBlockData var3) {
      boolean var4 = ((Boolean)var3.get(e)).booleanValue();
      boolean var5 = false;
      List var6 = this.a(var1, var2, EntityMinecartAbstract.class, new Predicate[0]);
      if(!var6.isEmpty()) {
         var5 = true;
      }

      if(var5 && !var4) {
         var1.a((BlockPosition)var2, (IBlockData)var3.set(e, Boolean.valueOf(true)), 3);
         this.b(var1, var2, var3, true);
         var1.d(var2, this);
         var1.d(var2.b(), this);
         var1.b(var2, var2);
      }

      if(!var5 && var4) {
         var1.a((BlockPosition)var2, (IBlockData)var3.set(e, Boolean.valueOf(false)), 3);
         this.b(var1, var2, var3, false);
         var1.d(var2, this);
         var1.d(var2.b(), this);
         var1.b(var2, var2);
      }

      if(var5) {
         var1.a((BlockPosition)(new BlockPosition(var2)), (Block)this, this.a(var1));
      }

      var1.f(var2, this);
   }

   protected void b(World var1, BlockPosition var2, IBlockData var3, boolean var4) {
      BlockMinecartTrackAbstract.class_a_in_class_ajo var5 = new BlockMinecartTrackAbstract.class_a_in_class_ajo(var1, var2, var3);
      List var6 = var5.a();
      Iterator var7 = var6.iterator();

      while(var7.hasNext()) {
         BlockPosition var8 = (BlockPosition)var7.next();
         IBlockData var9 = var1.getType(var8);
         if(var9 != null) {
            var9.getBlock().a(var1, var8, var9, var9.getBlock());
         }
      }

   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      super.c(var1, var2, var3);
      this.e(var1, var2, var3);
   }

   public IBlockState g() {
      return d;
   }

   public boolean v(IBlockData var1) {
      return true;
   }

   public int d(IBlockData var1, World var2, BlockPosition var3) {
      if(((Boolean)var1.get(e)).booleanValue()) {
         List var4 = this.a(var2, var3, class_aai.class, new Predicate[0]);
         if(!var4.isEmpty()) {
            return ((class_aai)var4.get(0)).j().k();
         }

         List var5 = this.a(var2, var3, EntityMinecartAbstract.class, new Predicate[]{class_ru.c});
         if(!var5.isEmpty()) {
            return Container.b((IInventory)var5.get(0));
         }
      }

      return 0;
   }

   protected List a(World var1, BlockPosition var2, Class var3, Predicate... var4) {
      AxisAlignedBB var5 = this.a(var2);
      return var4.length != 1?var1.a(var3, var5):var1.a(var3, var5, var4[0]);
   }

   private AxisAlignedBB a(BlockPosition var1) {
      float var2 = 0.2F;
      return new AxisAlignedBB((double)((float)var1.p() + 0.2F), (double)var1.q(), (double)((float)var1.r() + 0.2F), (double)((float)(var1.p() + 1) - 0.2F), (double)((float)(var1.q() + 1) - 0.2F), (double)((float)(var1.r() + 1) - 0.2F));
   }

   public IBlockData a(int var1) {
      return this.u().set(d, BlockMinecartTrackAbstract.EnumTrackPosition.a(var1 & 7)).set(e, Boolean.valueOf((var1 & 8) > 0));
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((BlockMinecartTrackAbstract.EnumTrackPosition)var1.get(d)).a();
      if(((Boolean)var1.get(e)).booleanValue()) {
         var3 |= 8;
      }

      return var3;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      switch(class_akp.SyntheticClass_1.b[var2.ordinal()]) {
      case 1:
         switch(class_akp.SyntheticClass_1.a[((BlockMinecartTrackAbstract.EnumTrackPosition)var1.get(d)).ordinal()]) {
         case 1:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST);
         case 2:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST);
         case 3:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH);
         case 4:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH);
         case 5:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST);
         case 6:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST);
         case 7:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST);
         case 8:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST);
         }
      case 2:
         switch(class_akp.SyntheticClass_1.a[((BlockMinecartTrackAbstract.EnumTrackPosition)var1.get(d)).ordinal()]) {
         case 1:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH);
         case 2:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH);
         case 3:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST);
         case 4:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST);
         case 5:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST);
         case 6:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST);
         case 7:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST);
         case 8:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST);
         case 9:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST);
         case 10:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH);
         }
      case 3:
         switch(class_akp.SyntheticClass_1.a[((BlockMinecartTrackAbstract.EnumTrackPosition)var1.get(d)).ordinal()]) {
         case 1:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH);
         case 2:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH);
         case 3:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST);
         case 4:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST);
         case 5:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST);
         case 6:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST);
         case 7:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST);
         case 8:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST);
         case 9:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST);
         case 10:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH);
         }
      default:
         return var1;
      }
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      BlockMinecartTrackAbstract.EnumTrackPosition var3 = (BlockMinecartTrackAbstract.EnumTrackPosition)var1.get(d);
      switch(class_akp.SyntheticClass_1.c[var2.ordinal()]) {
      case 1:
         switch(class_akp.SyntheticClass_1.a[var3.ordinal()]) {
         case 3:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH);
         case 4:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH);
         case 5:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST);
         case 6:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST);
         case 7:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST);
         case 8:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST);
         default:
            return super.a(var1, var2);
         }
      case 2:
         switch(class_akp.SyntheticClass_1.a[var3.ordinal()]) {
         case 1:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST);
         case 2:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST);
         case 3:
         case 4:
         default:
            break;
         case 5:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST);
         case 6:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST);
         case 7:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST);
         case 8:
            return var1.set(d, BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST);
         }
      }

      return super.a(var1, var2);
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{d, e});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b;
      // $FF: synthetic field
      static final int[] c = new int[class_amq.values().length];

      static {
         try {
            c[class_amq.LEFT_RIGHT.ordinal()] = 1;
         } catch (NoSuchFieldError var15) {
            ;
         }

         try {
            c[class_amq.FRONT_BACK.ordinal()] = 2;
         } catch (NoSuchFieldError var14) {
            ;
         }

         b = new int[class_aod.values().length];

         try {
            b[class_aod.CLOCKWISE_180.ordinal()] = 1;
         } catch (NoSuchFieldError var13) {
            ;
         }

         try {
            b[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var12) {
            ;
         }

         try {
            b[class_aod.CLOCKWISE_90.ordinal()] = 3;
         } catch (NoSuchFieldError var11) {
            ;
         }

         a = new int[BlockMinecartTrackAbstract.EnumTrackPosition.values().length];

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST.ordinal()] = 1;
         } catch (NoSuchFieldError var10) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST.ordinal()] = 2;
         } catch (NoSuchFieldError var9) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH.ordinal()] = 3;
         } catch (NoSuchFieldError var8) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH.ordinal()] = 4;
         } catch (NoSuchFieldError var7) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST.ordinal()] = 5;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST.ordinal()] = 6;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST.ordinal()] = 7;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST.ordinal()] = 8;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH.ordinal()] = 9;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST.ordinal()] = 10;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
