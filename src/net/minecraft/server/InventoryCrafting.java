package net.minecraft.server;

import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.Container;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_qg;

public class InventoryCrafting implements IInventory {
   private final ItemStack[] a;
   private final int b;
   private final int c;
   private final Container d;

   public InventoryCrafting(Container var1, int var2, int var3) {
      int var4 = var2 * var3;
      this.a = new ItemStack[var4];
      this.d = var1;
      this.b = var2;
      this.c = var3;
   }

   public int u_() {
      return this.a.length;
   }

   public ItemStack a(int var1) {
      return var1 >= this.u_()?null:this.a[var1];
   }

   public ItemStack c(int var1, int var2) {
      return var1 >= 0 && var1 < this.b && var2 >= 0 && var2 <= this.c?this.a(var1 + var2 * this.b):null;
   }

   public String h_() {
      return "container.crafting";
   }

   public boolean o_() {
      return false;
   }

   public IChatBaseComponent i_() {
      return (IChatBaseComponent)(this.o_()?new ChatComponentText(this.h_()):new ChatMessage(this.h_(), new Object[0]));
   }

   public ItemStack b(int var1) {
      return class_qg.a(this.a, var1);
   }

   public ItemStack a(int var1, int var2) {
      ItemStack var3 = class_qg.a(this.a, var1, var2);
      if(var3 != null) {
         this.d.a((IInventory)this);
      }

      return var3;
   }

   public void a(int var1, ItemStack var2) {
      this.a[var1] = var2;
      this.d.a((IInventory)this);
   }

   public int w_() {
      return 64;
   }

   public void v_() {
   }

   public boolean a(EntityHuman var1) {
      return true;
   }

   public void b(EntityHuman var1) {
   }

   public void c(EntityHuman var1) {
   }

   public boolean b(int var1, ItemStack var2) {
      return true;
   }

   public int c_(int var1) {
      return 0;
   }

   public void b(int var1, int var2) {
   }

   public int g() {
      return 0;
   }

   public void l() {
      for(int var1 = 0; var1 < this.a.length; ++var1) {
         this.a[var1] = null;
      }

   }

   public int h() {
      return this.c;
   }

   public int i() {
      return this.b;
   }
}
