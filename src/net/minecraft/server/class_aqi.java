package net.minecraft.server;

import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.ChestLock;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.class_qm;
import net.minecraft.server.class_qr;

public abstract class class_aqi extends TileEntity implements class_qm, class_qr {
   private ChestLock a = ChestLock.a;

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      this.a = ChestLock.b(var2);
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      if(this.a != null) {
         this.a.a(var1);
      }

   }

   public boolean x_() {
      return this.a != null && !this.a.a();
   }

   public ChestLock y_() {
      return this.a;
   }

   public void a(ChestLock var1) {
      this.a = var1;
   }

   public IChatBaseComponent i_() {
      return (IChatBaseComponent)(this.o_()?new ChatComponentText(this.h_()):new ChatMessage(this.h_(), new Object[0]));
   }
}
