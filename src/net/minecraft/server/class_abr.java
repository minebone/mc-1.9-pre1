package net.minecraft.server;

import net.minecraft.server.AchievementList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CraftingManager;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.InventoryCrafting;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.Statistic;
import net.minecraft.server.class_abs;
import net.minecraft.server.class_adm;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aea;
import net.minecraft.server.class_aew;
import net.minecraft.server.IInventory;

public class class_abr extends class_abs {
   private final InventoryCrafting a;
   private final EntityHuman b;
   private int c;

   public class_abr(EntityHuman var1, InventoryCrafting var2, IInventory var3, int var4, int var5, int var6) {
      super(var3, var4, var5, var6);
      this.b = var1;
      this.a = var2;
   }

   public boolean a(ItemStack var1) {
      return false;
   }

   public ItemStack a(int var1) {
      if(this.e()) {
         this.c += Math.min(var1, this.d().b);
      }

      return super.a(var1);
   }

   protected void a(ItemStack var1, int var2) {
      this.c += var2;
      this.c(var1);
   }

   protected void c(ItemStack var1) {
      if(this.c > 0) {
         var1.a(this.b.world, this.b, this.c);
      }

      this.c = 0;
      if(var1.b() == Item.a(Blocks.ai)) {
         this.b.b((Statistic)AchievementList.h);
      }

      if(var1.b() instanceof class_aea) {
         this.b.b((Statistic)AchievementList.i);
      }

      if(var1.b() == Item.a(Blocks.al)) {
         this.b.b((Statistic)AchievementList.j);
      }

      if(var1.b() instanceof class_adm) {
         this.b.b((Statistic)AchievementList.l);
      }

      if(var1.b() == Items.R) {
         this.b.b((Statistic)AchievementList.m);
      }

      if(var1.b() == Items.bg) {
         this.b.b((Statistic)AchievementList.n);
      }

      if(var1.b() instanceof class_aea && ((class_aea)var1.b()).g() != Item.class_a_in_class_adn.WOOD) {
         this.b.b((Statistic)AchievementList.o);
      }

      if(var1.b() instanceof class_aew) {
         this.b.b((Statistic)AchievementList.r);
      }

      if(var1.b() == Item.a(Blocks.bC)) {
         this.b.b((Statistic)AchievementList.E);
      }

      if(var1.b() == Item.a(Blocks.X)) {
         this.b.b((Statistic)AchievementList.G);
      }

   }

   public void a(EntityHuman var1, ItemStack var2) {
      this.c(var2);
      ItemStack[] var3 = CraftingManager.a().b(this.a, var1.world);

      for(int var4 = 0; var4 < var3.length; ++var4) {
         ItemStack var5 = this.a.a(var4);
         ItemStack var6 = var3[var4];
         if(var5 != null) {
            this.a.a(var4, 1);
            var5 = this.a.a(var4);
         }

         if(var6 != null) {
            if(var5 == null) {
               this.a.a(var4, var6);
            } else if(ItemStack.c(var5, var6) && ItemStack.a(var5, var6)) {
               var6.b += var5.b;
               this.a.a(var4, var6);
            } else if(!this.b.br.c(var6)) {
               this.b.a(var6, false);
            }
         }
      }

   }
}
