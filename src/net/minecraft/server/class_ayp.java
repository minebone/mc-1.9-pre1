package net.minecraft.server;

import java.util.HashSet;
import java.util.Set;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.Path;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aym;
import net.minecraft.server.class_ayn;
import net.minecraft.server.class_ayo;
import net.minecraft.server.BlockPosition;

public class class_ayp {
   private final Path a = new Path();
   private final Set b = new HashSet();
   private final class_aym[] c = new class_aym[32];
   private final class_ayn d;

   public class_ayp(class_ayn var1) {
      this.d = var1;
   }

   public class_ayo a(class_ahw var1, EntityInsentient var2, Entity var3, float var4) {
      return this.a(var1, var2, var3.locX, var3.bk().b, var3.locZ, var4);
   }

   public class_ayo a(class_ahw var1, EntityInsentient var2, BlockPosition var3, float var4) {
      return this.a(var1, var2, (double)((float)var3.p() + 0.5F), (double)((float)var3.q() + 0.5F), (double)((float)var3.r() + 0.5F), var4);
   }

   private class_ayo a(class_ahw var1, EntityInsentient var2, double var3, double var5, double var7, float var9) {
      this.a.a();
      this.d.a(var1, var2);
      class_aym var10 = this.d.b();
      class_aym var11 = this.d.a(var3, var5, var7);
      class_ayo var12 = this.a(var10, var11, var9);
      this.d.a();
      return var12;
   }

   private class_ayo a(class_aym var1, class_aym var2, float var3) {
      var1.e = 0.0F;
      var1.f = var1.c(var2);
      var1.g = var1.f;
      this.a.a();
      this.b.clear();
      this.a.a(var1);
      class_aym var4 = var1;
      int var5 = 0;

      while(!this.a.e()) {
         ++var5;
         if(var5 >= 2000) {
            break;
         }

         class_aym var6 = this.a.c();
         if(var6.equals(var2)) {
            var4 = var2;
            break;
         }

         if(var6.c(var2) < var4.c(var2)) {
            var4 = var6;
         }

         var6.i = true;
         int var7 = this.d.a(this.c, var6, var2, var3);

         for(int var8 = 0; var8 < var7; ++var8) {
            class_aym var9 = this.c[var8];
            float var10 = var6.c(var9);
            var9.j = var6.j + var10;
            var9.k = var10 + var9.l;
            float var11 = var6.e + var9.k;
            if(var9.j < var3 && (!var9.a() || var11 < var9.e)) {
               var9.h = var6;
               var9.e = var11;
               var9.f = var9.c(var2) + var9.l;
               if(var9.a()) {
                  this.a.a(var9, var9.e + var9.f);
               } else {
                  var9.g = var9.e + var9.f;
                  this.a.a(var9);
               }
            }
         }
      }

      if(var4 == var1) {
         return null;
      } else {
         class_ayo var12 = this.a(var1, var4);
         return var12;
      }
   }

   private class_ayo a(class_aym var1, class_aym var2) {
      int var3 = 1;

      class_aym var4;
      for(var4 = var2; var4.h != null; var4 = var4.h) {
         ++var3;
      }

      class_aym[] var5 = new class_aym[var3];
      var4 = var2;
      --var3;

      for(var5[var3] = var2; var4.h != null; var5[var3] = var4) {
         var4 = var4.h;
         --var3;
      }

      return new class_ayo(var5);
   }
}
