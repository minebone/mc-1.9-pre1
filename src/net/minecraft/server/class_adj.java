package net.minecraft.server;

import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumAnimation;
import net.minecraft.server.Item;
import net.minecraft.server.MobEffect;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;
import net.minecraft.server.class_rz;

public class class_adj extends Item {
   public final int a;
   private final int b;
   private final float c;
   private final boolean d;
   private boolean e;
   private MobEffect m;
   private float n;

   public class_adj(int var1, float var2, boolean var3) {
      this.a = 32;
      this.b = var1;
      this.d = var3;
      this.c = var2;
      this.a(CreativeModeTab.h);
   }

   public class_adj(int var1, boolean var2) {
      this(var1, 0.6F, var2);
   }

   public ItemStack a(ItemStack var1, World var2, class_rz var3) {
      --var1.b;
      if(var3 instanceof EntityHuman) {
         EntityHuman var4 = (EntityHuman)var3;
         var4.cR().a(this, var1);
         var2.a((EntityHuman)null, var4.locX, var4.locY, var4.locZ, class_ng.dZ, EnumSoundCategory.PLAYERS, 0.5F, var2.r.nextFloat() * 0.1F + 0.9F);
         this.a(var1, var2, var4);
         var4.b(StatisticList.b((Item)this));
      }

      return var1;
   }

   protected void a(ItemStack var1, World var2, EntityHuman var3) {
      if(!var2.E && this.m != null && var2.r.nextFloat() < this.n) {
         var3.c(new MobEffect(this.m));
      }

   }

   public int e(ItemStack var1) {
      return 32;
   }

   public EnumAnimation f(ItemStack var1) {
      return EnumAnimation.EAT;
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      if(var3.l(this.e)) {
         var3.c(var4);
         return new class_qo(EnumResult.SUCCESS, var1);
      } else {
         return new class_qo(EnumResult.FAIL, var1);
      }
   }

   public int h(ItemStack var1) {
      return this.b;
   }

   public float i(ItemStack var1) {
      return this.c;
   }

   public boolean g() {
      return this.d;
   }

   public class_adj a(MobEffect var1, float var2) {
      this.m = var1;
      this.n = var2;
      return this;
   }

   public class_adj h() {
      this.e = true;
      return this;
   }
}
