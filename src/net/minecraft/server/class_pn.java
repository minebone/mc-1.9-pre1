package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.List;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.class_ox;

public class class_pn implements class_ox {
   private static final List a = Lists.newArrayList((Object[])(new String[]{"MinecartRideable", "MinecartChest", "MinecartFurnace", "MinecartTNT", "MinecartSpawner", "MinecartHopper", "MinecartCommandBlock"}));

   public int a() {
      return 106;
   }

   public NBTTagCompound a(NBTTagCompound var1) {
      if("Minecart".equals(var1.l("id"))) {
         String var2 = "MinecartRideable";
         int var3 = var1.h("Type");
         if(var3 > 0 && var3 < a.size()) {
            var2 = (String)a.get(var3);
         }

         var1.a("id", var2);
         var1.q("Type");
      }

      return var1;
   }
}
