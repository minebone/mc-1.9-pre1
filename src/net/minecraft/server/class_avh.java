package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_auc;
import net.minecraft.server.BlockPosition;

public class class_avh extends class_auc {
   public boolean b(World var1, Random var2, BlockPosition var3) {
      for(int var4 = 0; var4 < 10; ++var4) {
         int var5 = var3.p() + var2.nextInt(8) - var2.nextInt(8);
         int var6 = var3.q() + var2.nextInt(4) - var2.nextInt(4);
         int var7 = var3.r() + var2.nextInt(8) - var2.nextInt(8);
         if(var1.d(new BlockPosition(var5, var6, var7)) && Blocks.bx.a(var1, new BlockPosition(var5, var6, var7))) {
            var1.a((BlockPosition)(new BlockPosition(var5, var6, var7)), (IBlockData)Blocks.bx.u(), 2);
         }
      }

      return true;
   }
}
