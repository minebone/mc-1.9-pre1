package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Container;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.TileEntityDispenser;
import net.minecraft.server.TileEntityDropper;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_akr;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aoa;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_aqt;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.class_arn;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ck;
import net.minecraft.server.class_cl;
import net.minecraft.server.class_cn;
import net.minecraft.server.class_cp;
import net.minecraft.server.class_cr;
import net.minecraft.server.class_cz;
import net.minecraft.server.class_da;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_qi;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;

public class class_akt extends class_ajm {
   public static final class_arn a = class_akr.H;
   public static final class_arm b = class_arm.a("triggered");
   public static final class_cp c = new class_cp(new class_cn());
   protected Random d = new Random();

   protected class_akt() {
      super(Material.e);
      this.w(this.A.b().set(a, EnumDirection.NORTH).set(b, Boolean.valueOf(false)));
      this.a((CreativeModeTab)CreativeModeTab.d);
   }

   public int a(World var1) {
      return 4;
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      super.c(var1, var2, var3);
      this.e(var1, var2, var3);
   }

   private void e(World var1, BlockPosition var2, IBlockData var3) {
      if(!var1.E) {
         EnumDirection var4 = (EnumDirection)var3.get(a);
         boolean var5 = var1.getType(var2.c()).b();
         boolean var6 = var1.getType(var2.d()).b();
         if(var4 == EnumDirection.NORTH && var5 && !var6) {
            var4 = EnumDirection.SOUTH;
         } else if(var4 == EnumDirection.SOUTH && var6 && !var5) {
            var4 = EnumDirection.NORTH;
         } else {
            boolean var7 = var1.getType(var2.e()).b();
            boolean var8 = var1.getType(var2.f()).b();
            if(var4 == EnumDirection.WEST && var7 && !var8) {
               var4 = EnumDirection.EAST;
            } else if(var4 == EnumDirection.EAST && var8 && !var7) {
               var4 = EnumDirection.WEST;
            }
         }

         var1.a((BlockPosition)var2, (IBlockData)var3.set(a, var4).set(b, Boolean.valueOf(false)), 2);
      }
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(var1.E) {
         return true;
      } else {
         TileEntity var11 = var1.r(var2);
         if(var11 instanceof TileEntityDispenser) {
            var4.openContainer((IInventory)((TileEntityDispenser)var11));
            if(var11 instanceof TileEntityDropper) {
               var4.b(StatisticList.Q);
            } else {
               var4.b(StatisticList.S);
            }
         }

         return true;
      }
   }

   protected void c(World var1, BlockPosition var2) {
      class_cl var3 = new class_cl(var1, var2);
      TileEntityDispenser var4 = (TileEntityDispenser)var3.h();
      if(var4 != null) {
         int var5 = var4.m();
         if(var5 < 0) {
            var1.b(1001, var2, 0);
         } else {
            ItemStack var6 = var4.a(var5);
            class_cr var7 = this.a(var6);
            if(var7 != class_cr.a) {
               ItemStack var8 = var7.a(var3, var6);
               var4.a(var5, var8.b <= 0?null:var8);
            }

         }
      }
   }

   protected class_cr a(ItemStack var1) {
      return (class_cr)c.c(var1 == null?null:var1.b());
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      boolean var5 = var1.y(var2) || var1.y(var2.a());
      boolean var6 = ((Boolean)var3.get(b)).booleanValue();
      if(var5 && !var6) {
         var1.a((BlockPosition)var2, (Block)this, this.a(var1));
         var1.a((BlockPosition)var2, (IBlockData)var3.set(b, Boolean.valueOf(true)), 4);
      } else if(!var5 && var6) {
         var1.a((BlockPosition)var2, (IBlockData)var3.set(b, Boolean.valueOf(false)), 4);
      }

   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(!var1.E) {
         this.c(var1, var2);
      }

   }

   public TileEntity a(World var1, int var2) {
      return new TileEntityDispenser();
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return this.u().set(a, class_aqt.a(var2, var8)).set(b, Boolean.valueOf(false));
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, class_rz var4, ItemStack var5) {
      var1.a((BlockPosition)var2, (IBlockData)var3.set(a, class_aqt.a(var2, var4)), 2);
      if(var5.s()) {
         TileEntity var6 = var1.r(var2);
         if(var6 instanceof TileEntityDispenser) {
            ((TileEntityDispenser)var6).a(var5.q());
         }
      }

   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      TileEntity var4 = var1.r(var2);
      if(var4 instanceof TileEntityDispenser) {
         class_qi.a(var1, (BlockPosition)var2, (TileEntityDispenser)var4);
         var1.f(var2, this);
      }

      super.b(var1, var2, var3);
   }

   public static class_cz a(class_ck var0) {
      EnumDirection var1 = e(var0.f());
      double var2 = var0.a() + 0.7D * (double)var1.g();
      double var4 = var0.b() + 0.7D * (double)var1.h();
      double var6 = var0.c() + 0.7D * (double)var1.i();
      return new class_da(var2, var4, var6);
   }

   public static EnumDirection e(int var0) {
      return EnumDirection.a(var0 & 7);
   }

   public boolean v(IBlockData var1) {
      return true;
   }

   public int d(IBlockData var1, World var2, BlockPosition var3) {
      return Container.a(var2.r(var3));
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.MODEL;
   }

   public IBlockData a(int var1) {
      return this.u().set(a, e(var1)).set(b, Boolean.valueOf((var1 & 8) > 0));
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((EnumDirection)var1.get(a)).a();
      if(((Boolean)var1.get(b)).booleanValue()) {
         var3 |= 8;
      }

      return var3;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(a, var2.a((EnumDirection)var1.get(a)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(a)));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b});
   }
}
