package net.minecraft.server;

import net.minecraft.server.BlockFlowers;
import net.minecraft.server.BlockTallPlant;
import net.minecraft.server.Blocks;
import net.minecraft.server.CraftingManager;
import net.minecraft.server.EnumColor;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.ItemStack;

public class class_afm {
   public void a(CraftingManager var1) {
      int var2;
      for(var2 = 0; var2 < 16; ++var2) {
         var1.b(new ItemStack(Blocks.L, 1, var2), new Object[]{new ItemStack(Items.bd, 1, 15 - var2), new ItemStack(Item.a(Blocks.L))});
         var1.a(new ItemStack(Blocks.cu, 8, 15 - var2), new Object[]{"###", "#X#", "###", Character.valueOf('#'), new ItemStack(Blocks.cz), Character.valueOf('X'), new ItemStack(Items.bd, 1, var2)});
         var1.a(new ItemStack(Blocks.cG, 8, 15 - var2), new Object[]{"###", "#X#", "###", Character.valueOf('#'), new ItemStack(Blocks.w), Character.valueOf('X'), new ItemStack(Items.bd, 1, var2)});
         var1.a(new ItemStack(Blocks.cH, 16, var2), new Object[]{"###", "###", Character.valueOf('#'), new ItemStack(Blocks.cG, 1, var2)});
      }

      var1.b(new ItemStack(Items.bd, 1, EnumColor.YELLOW.b()), new Object[]{new ItemStack(Blocks.N, 1, BlockFlowers.EnumFlowerVarient.DANDELION.b())});
      var1.b(new ItemStack(Items.bd, 1, EnumColor.RED.b()), new Object[]{new ItemStack(Blocks.O, 1, BlockFlowers.EnumFlowerVarient.POPPY.b())});
      var1.b(new ItemStack(Items.bd, 3, EnumColor.WHITE.b()), new Object[]{Items.be});
      var1.b(new ItemStack(Items.bd, 2, EnumColor.PINK.b()), new Object[]{new ItemStack(Items.bd, 1, EnumColor.RED.b()), new ItemStack(Items.bd, 1, EnumColor.WHITE.b())});
      var1.b(new ItemStack(Items.bd, 2, EnumColor.ORANGE.b()), new Object[]{new ItemStack(Items.bd, 1, EnumColor.RED.b()), new ItemStack(Items.bd, 1, EnumColor.YELLOW.b())});
      var1.b(new ItemStack(Items.bd, 2, EnumColor.LIME.b()), new Object[]{new ItemStack(Items.bd, 1, EnumColor.GREEN.b()), new ItemStack(Items.bd, 1, EnumColor.WHITE.b())});
      var1.b(new ItemStack(Items.bd, 2, EnumColor.GRAY.b()), new Object[]{new ItemStack(Items.bd, 1, EnumColor.BLACK.b()), new ItemStack(Items.bd, 1, EnumColor.WHITE.b())});
      var1.b(new ItemStack(Items.bd, 2, EnumColor.SILVER.b()), new Object[]{new ItemStack(Items.bd, 1, EnumColor.GRAY.b()), new ItemStack(Items.bd, 1, EnumColor.WHITE.b())});
      var1.b(new ItemStack(Items.bd, 3, EnumColor.SILVER.b()), new Object[]{new ItemStack(Items.bd, 1, EnumColor.BLACK.b()), new ItemStack(Items.bd, 1, EnumColor.WHITE.b()), new ItemStack(Items.bd, 1, EnumColor.WHITE.b())});
      var1.b(new ItemStack(Items.bd, 2, EnumColor.LIGHT_BLUE.b()), new Object[]{new ItemStack(Items.bd, 1, EnumColor.BLUE.b()), new ItemStack(Items.bd, 1, EnumColor.WHITE.b())});
      var1.b(new ItemStack(Items.bd, 2, EnumColor.CYAN.b()), new Object[]{new ItemStack(Items.bd, 1, EnumColor.BLUE.b()), new ItemStack(Items.bd, 1, EnumColor.GREEN.b())});
      var1.b(new ItemStack(Items.bd, 2, EnumColor.PURPLE.b()), new Object[]{new ItemStack(Items.bd, 1, EnumColor.BLUE.b()), new ItemStack(Items.bd, 1, EnumColor.RED.b())});
      var1.b(new ItemStack(Items.bd, 2, EnumColor.MAGENTA.b()), new Object[]{new ItemStack(Items.bd, 1, EnumColor.PURPLE.b()), new ItemStack(Items.bd, 1, EnumColor.PINK.b())});
      var1.b(new ItemStack(Items.bd, 3, EnumColor.MAGENTA.b()), new Object[]{new ItemStack(Items.bd, 1, EnumColor.BLUE.b()), new ItemStack(Items.bd, 1, EnumColor.RED.b()), new ItemStack(Items.bd, 1, EnumColor.PINK.b())});
      var1.b(new ItemStack(Items.bd, 4, EnumColor.MAGENTA.b()), new Object[]{new ItemStack(Items.bd, 1, EnumColor.BLUE.b()), new ItemStack(Items.bd, 1, EnumColor.RED.b()), new ItemStack(Items.bd, 1, EnumColor.RED.b()), new ItemStack(Items.bd, 1, EnumColor.WHITE.b())});
      var1.b(new ItemStack(Items.bd, 1, EnumColor.LIGHT_BLUE.b()), new Object[]{new ItemStack(Blocks.O, 1, BlockFlowers.EnumFlowerVarient.BLUE_ORCHID.b())});
      var1.b(new ItemStack(Items.bd, 1, EnumColor.MAGENTA.b()), new Object[]{new ItemStack(Blocks.O, 1, BlockFlowers.EnumFlowerVarient.ALLIUM.b())});
      var1.b(new ItemStack(Items.bd, 1, EnumColor.SILVER.b()), new Object[]{new ItemStack(Blocks.O, 1, BlockFlowers.EnumFlowerVarient.HOUSTONIA.b())});
      var1.b(new ItemStack(Items.bd, 1, EnumColor.RED.b()), new Object[]{new ItemStack(Blocks.O, 1, BlockFlowers.EnumFlowerVarient.RED_TULIP.b())});
      var1.b(new ItemStack(Items.bd, 1, EnumColor.ORANGE.b()), new Object[]{new ItemStack(Blocks.O, 1, BlockFlowers.EnumFlowerVarient.ORANGE_TULIP.b())});
      var1.b(new ItemStack(Items.bd, 1, EnumColor.SILVER.b()), new Object[]{new ItemStack(Blocks.O, 1, BlockFlowers.EnumFlowerVarient.WHITE_TULIP.b())});
      var1.b(new ItemStack(Items.bd, 1, EnumColor.PINK.b()), new Object[]{new ItemStack(Blocks.O, 1, BlockFlowers.EnumFlowerVarient.PINK_TULIP.b())});
      var1.b(new ItemStack(Items.bd, 1, EnumColor.SILVER.b()), new Object[]{new ItemStack(Blocks.O, 1, BlockFlowers.EnumFlowerVarient.OXEYE_DAISY.b())});
      var1.b(new ItemStack(Items.bd, 2, EnumColor.YELLOW.b()), new Object[]{new ItemStack(Blocks.cF, 1, BlockTallPlant.EnumTallFlowerVariants.SUNFLOWER.a())});
      var1.b(new ItemStack(Items.bd, 2, EnumColor.MAGENTA.b()), new Object[]{new ItemStack(Blocks.cF, 1, BlockTallPlant.EnumTallFlowerVariants.SYRINGA.a())});
      var1.b(new ItemStack(Items.bd, 2, EnumColor.RED.b()), new Object[]{new ItemStack(Blocks.cF, 1, BlockTallPlant.EnumTallFlowerVariants.ROSE.a())});
      var1.b(new ItemStack(Items.bd, 2, EnumColor.PINK.b()), new Object[]{new ItemStack(Blocks.cF, 1, BlockTallPlant.EnumTallFlowerVariants.PAEONIA.a())});
      var1.b(new ItemStack(Items.bd, 1, EnumColor.RED.b()), new Object[]{new ItemStack(Items.cV, 1)});

      for(var2 = 0; var2 < 16; ++var2) {
         var1.a(new ItemStack(Blocks.cy, 3, var2), new Object[]{"##", Character.valueOf('#'), new ItemStack(Blocks.L, 1, var2)});
      }

   }
}
