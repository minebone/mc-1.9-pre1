package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_azv;
import net.minecraft.server.class_azy;
import net.minecraft.server.MathHelper;
import net.minecraft.server.IInventory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_azx {
   private static final Logger b = LogManager.getLogger();
   public static final class_azx a = new class_azx(new class_azv[0]);
   private final class_azv[] c;

   public class_azx(class_azv[] var1) {
      this.c = var1;
   }

   public List a(Random var1, class_azy var2) {
      ArrayList var3 = Lists.newArrayList();
      if(var2.a(this)) {
         class_azv[] var4 = this.c;
         int var5 = var4.length;

         for(int var6 = 0; var6 < var5; ++var6) {
            class_azv var7 = var4[var6];
            var7.b(var3, var1, var2);
         }

         var2.b(this);
      } else {
         b.warn("Detected infinite loop in loot tables");
      }

      return var3;
   }

   public void a(IInventory var1, Random var2, class_azy var3) {
      List var4 = this.a(var2, var3);
      List var5 = this.a(var1, var2);
      this.a(var4, var5.size(), var2);
      Iterator var6 = var4.iterator();

      while(var6.hasNext()) {
         ItemStack var7 = (ItemStack)var6.next();
         if(var5.isEmpty()) {
            b.warn("Tried to over-fill a container");
            return;
         }

         if(var7 == null) {
            var1.a(((Integer)var5.remove(var5.size() - 1)).intValue(), (ItemStack)null);
         } else {
            var1.a(((Integer)var5.remove(var5.size() - 1)).intValue(), var7);
         }
      }

   }

   private void a(List var1, int var2, Random var3) {
      ArrayList var4 = Lists.newArrayList();
      Iterator var5 = var1.iterator();

      while(var5.hasNext()) {
         ItemStack var6 = (ItemStack)var5.next();
         if(var6.b <= 0) {
            var5.remove();
         } else if(var6.b > 1) {
            var4.add(var6);
            var5.remove();
         }
      }

      var2 -= var1.size();

      while(var2 > 0 && var4.size() > 0) {
         ItemStack var8 = (ItemStack)var4.remove(MathHelper.a((Random)var3, 0, var4.size() - 1));
         int var9 = MathHelper.a((Random)var3, 1, var8.b / 2);
         var8.b -= var9;
         ItemStack var7 = var8.k();
         var7.b = var9;
         if(var8.b > 1 && var3.nextBoolean()) {
            var4.add(var8);
         } else {
            var1.add(var8);
         }

         if(var7.b > 1 && var3.nextBoolean()) {
            var4.add(var7);
         } else {
            var1.add(var7);
         }
      }

      var1.addAll(var4);
      Collections.shuffle(var1, var3);
   }

   private List a(IInventory var1, Random var2) {
      ArrayList var3 = Lists.newArrayList();

      for(int var4 = 0; var4 < var1.u_(); ++var4) {
         if(var1.a(var4) == null) {
            var3.add(Integer.valueOf(var4));
         }
      }

      Collections.shuffle(var3, var2);
      return var3;
   }

   public static class class_a_in_class_azx implements JsonDeserializer, JsonSerializer {
      public class_azx a(JsonElement var1, Type var2, JsonDeserializationContext var3) throws JsonParseException {
         JsonObject var4 = ChatDeserializer.m(var1, "loot table");
         class_azv[] var5 = (class_azv[])ChatDeserializer.a(var4, "pools", new class_azv[0], var3, class_azv[].class);
         return new class_azx(var5);
      }

      public JsonElement a(class_azx var1, Type var2, JsonSerializationContext var3) {
         JsonObject var4 = new JsonObject();
         var4.add("pools", var3.serialize(var1.c));
         return var4;
      }

      // $FF: synthetic method
      public JsonElement serialize(Object var1, Type var2, JsonSerializationContext var3) {
         return this.a((class_azx)var1, var2, var3);
      }

      // $FF: synthetic method
      public Object deserialize(JsonElement var1, Type var2, JsonDeserializationContext var3) throws JsonParseException {
         return this.a(var1, var2, var3);
      }
   }
}
