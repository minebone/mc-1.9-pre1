package net.minecraft.server;

import java.io.IOException;
import java.util.UUID;
import net.minecraft.server.EntityPainting;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketListenerPlayOut;

/*TODO: Packet changed*/
public class PacketPlayOutSpawnEntityPainting implements Packet {
   private int a;
   private UUID b;
   private BlockPosition c;
   private EnumDirection d;
   private String e;

   public PacketPlayOutSpawnEntityPainting() {
   }

   public PacketPlayOutSpawnEntityPainting(EntityPainting var1) {
      this.a = var1.getId();
      this.b = var1.getUniqueId();
      this.c = var1.q();
      this.d = var1.b;
      this.e = var1.c.B;
   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.readVarInt();
      this.b = var1.i();
      this.e = var1.c(EntityPainting.EnumArt.A);
      this.c = var1.e();
      this.d = EnumDirection.b(var1.readUnsignedByte());
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.writeVarInt(this.a);
      var1.a(this.b);
      var1.a(this.e);
      var1.a(this.c);
      var1.writeByte(this.d.b());
   }

   public void a(PacketListenerPlayOut var1) {
      var1.a(this);
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketListenerPlayOut)var1);
   }
}
