package net.minecraft.server;

import net.minecraft.server.ChatComponentText;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.Entity;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;

public class RemoteControlCommandListener implements ICommandListener {
   private final StringBuffer a = new StringBuffer();
   private final MinecraftServer b;

   public RemoteControlCommandListener(MinecraftServer var1) {
      this.b = var1;
   }

   public void i() {
      this.a.setLength(0);
   }

   public String j() {
      return this.a.toString();
   }

   public String h_() {
      return "Rcon";
   }

   public IChatBaseComponent i_() {
      return new ChatComponentText(this.h_());
   }

   public void a(IChatBaseComponent var1) {
      this.a.append(var1.c());
   }

   public boolean a(int var1, String var2) {
      return true;
   }

   public BlockPosition c() {
      return BlockPosition.a;
   }

   public Vec3D d() {
      return Vec3D.a;
   }

   public World e() {
      return this.b.e();
   }

   public Entity f() {
      return null;
   }

   public boolean z_() {
      return true;
   }

   public void a(CommandObjectiveExecutor.EnumCommandResult var1, int var2) {
   }

   public MinecraftServer h() {
      return this.b;
   }
}
