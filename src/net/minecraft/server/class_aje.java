package net.minecraft.server;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.Lists;
import com.google.common.collect.Range;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.BiomeDecorator;
import net.minecraft.server.World;
import net.minecraft.server.class_ava;
import net.minecraft.server.BlockPosition;

public class class_aje extends BiomeDecorator {
   private static final LoadingCache L = CacheBuilder.newBuilder().expireAfterWrite(5L, TimeUnit.MINUTES).build(new class_aje.class_a_in_class_aje());
   private final class_ava M = new class_ava();

   protected void a(BiomeBase var1, World var2, Random var3) {
      this.a(var2, var3);
      class_ava.class_a_in_class_ava[] var4 = a(var2);
      class_ava.class_a_in_class_ava[] var5 = var4;
      int var6 = var4.length;

      for(int var7 = 0; var7 < var6; ++var7) {
         class_ava.class_a_in_class_ava var8 = var5[var7];
         if(var8.a(this.b)) {
            this.M.a(var8);
            this.M.b(var2, var3, new BlockPosition(var8.a(), 45, var8.b()));
         }
      }

   }

   public static class_ava.class_a_in_class_ava[] a(World var0) {
      Random var1 = new Random(var0.O());
      long var2 = var1.nextLong() & 65535L;
      return (class_ava.class_a_in_class_ava[])L.getUnchecked(Long.valueOf(var2));
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
   }

   static class class_a_in_class_aje extends CacheLoader {
      private class_a_in_class_aje() {
      }

      public class_ava.class_a_in_class_ava[] a(Long var1) throws Exception {
         ArrayList var2 = Lists.newArrayList((Iterable)ContiguousSet.create(Range.closedOpen(Integer.valueOf(0), Integer.valueOf(10)), DiscreteDomain.integers()));
         Collections.shuffle(var2, new Random(var1.longValue()));
         class_ava.class_a_in_class_ava[] var3 = new class_ava.class_a_in_class_ava[10];

         for(int var4 = 0; var4 < 10; ++var4) {
            int var5 = (int)(42.0D * Math.cos(2.0D * (-3.141592653589793D + 0.3141592653589793D * (double)var4)));
            int var6 = (int)(42.0D * Math.sin(2.0D * (-3.141592653589793D + 0.3141592653589793D * (double)var4)));
            int var7 = ((Integer)var2.get(var4)).intValue();
            int var8 = 2 + var7 / 3;
            int var9 = 76 + var7 * 3;
            boolean var10 = var7 == 1 || var7 == 2;
            var3[var4] = new class_ava.class_a_in_class_ava(var5, var6, var8, var9, var10);
         }

         return var3;
      }

      // $FF: synthetic method
      public Object load(Object var1) throws Exception {
         return this.a((Long)var1);
      }

      // $FF: synthetic method
      class_a_in_class_aje(class_aje.SyntheticClass_1 var1) {
         this();
      }
   }
}
