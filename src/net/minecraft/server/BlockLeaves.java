package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aoo;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public abstract class BlockLeaves extends Block {
   public static final class_arm a = class_arm.a("decayable");
   public static final class_arm b = class_arm.a("check_decay");
   protected boolean c;
   int[] d;

   public BlockLeaves() {
      super(Material.j);
      this.a(true);
      this.a(CreativeModeTab.c);
      this.c(0.2F);
      this.d(1);
      this.a(class_aoo.c);
   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      byte var4 = 1;
      int var5 = var4 + 1;
      int var6 = var2.p();
      int var7 = var2.q();
      int var8 = var2.r();
      if(var1.a(new BlockPosition(var6 - var5, var7 - var5, var8 - var5), new BlockPosition(var6 + var5, var7 + var5, var8 + var5))) {
         for(int var9 = -var4; var9 <= var4; ++var9) {
            for(int var10 = -var4; var10 <= var4; ++var10) {
               for(int var11 = -var4; var11 <= var4; ++var11) {
                  BlockPosition var12 = var2.a(var9, var10, var11);
                  IBlockData var13 = var1.getType(var12);
                  if(var13.getMaterial() == Material.j && !((Boolean)var13.get(b)).booleanValue()) {
                     var1.a((BlockPosition)var12, (IBlockData)var13.set(b, Boolean.valueOf(true)), 4);
                  }
               }
            }
         }
      }

   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(!var1.E) {
         if(((Boolean)var3.get(b)).booleanValue() && ((Boolean)var3.get(a)).booleanValue()) {
            byte var5 = 4;
            int var6 = var5 + 1;
            int var7 = var2.p();
            int var8 = var2.q();
            int var9 = var2.r();
            byte var10 = 32;
            int var11 = var10 * var10;
            int var12 = var10 / 2;
            if(this.d == null) {
               this.d = new int[var10 * var10 * var10];
            }

            if(var1.a(new BlockPosition(var7 - var6, var8 - var6, var9 - var6), new BlockPosition(var7 + var6, var8 + var6, var9 + var6))) {
               BlockPosition.class_a_in_class_cj var13 = new BlockPosition.class_a_in_class_cj();
               int var14 = -var5;

               label116:
               while(true) {
                  int var15;
                  int var16;
                  if(var14 > var5) {
                     var14 = 1;

                     while(true) {
                        if(var14 > 4) {
                           break label116;
                        }

                        for(var15 = -var5; var15 <= var5; ++var15) {
                           for(var16 = -var5; var16 <= var5; ++var16) {
                              for(int var20 = -var5; var20 <= var5; ++var20) {
                                 if(this.d[(var15 + var12) * var11 + (var16 + var12) * var10 + var20 + var12] == var14 - 1) {
                                    if(this.d[(var15 + var12 - 1) * var11 + (var16 + var12) * var10 + var20 + var12] == -2) {
                                       this.d[(var15 + var12 - 1) * var11 + (var16 + var12) * var10 + var20 + var12] = var14;
                                    }

                                    if(this.d[(var15 + var12 + 1) * var11 + (var16 + var12) * var10 + var20 + var12] == -2) {
                                       this.d[(var15 + var12 + 1) * var11 + (var16 + var12) * var10 + var20 + var12] = var14;
                                    }

                                    if(this.d[(var15 + var12) * var11 + (var16 + var12 - 1) * var10 + var20 + var12] == -2) {
                                       this.d[(var15 + var12) * var11 + (var16 + var12 - 1) * var10 + var20 + var12] = var14;
                                    }

                                    if(this.d[(var15 + var12) * var11 + (var16 + var12 + 1) * var10 + var20 + var12] == -2) {
                                       this.d[(var15 + var12) * var11 + (var16 + var12 + 1) * var10 + var20 + var12] = var14;
                                    }

                                    if(this.d[(var15 + var12) * var11 + (var16 + var12) * var10 + (var20 + var12 - 1)] == -2) {
                                       this.d[(var15 + var12) * var11 + (var16 + var12) * var10 + (var20 + var12 - 1)] = var14;
                                    }

                                    if(this.d[(var15 + var12) * var11 + (var16 + var12) * var10 + var20 + var12 + 1] == -2) {
                                       this.d[(var15 + var12) * var11 + (var16 + var12) * var10 + var20 + var12 + 1] = var14;
                                    }
                                 }
                              }
                           }
                        }

                        ++var14;
                     }
                  }

                  for(var15 = -var5; var15 <= var5; ++var15) {
                     for(var16 = -var5; var16 <= var5; ++var16) {
                        IBlockData var17 = var1.getType(var13.c(var7 + var14, var8 + var15, var9 + var16));
                        Block var18 = var17.getBlock();
                        if(var18 != Blocks.r && var18 != Blocks.s) {
                           if(var17.getMaterial() == Material.j) {
                              this.d[(var14 + var12) * var11 + (var15 + var12) * var10 + var16 + var12] = -2;
                           } else {
                              this.d[(var14 + var12) * var11 + (var15 + var12) * var10 + var16 + var12] = -1;
                           }
                        } else {
                           this.d[(var14 + var12) * var11 + (var15 + var12) * var10 + var16 + var12] = 0;
                        }
                     }
                  }

                  ++var14;
               }
            }

            int var19 = this.d[var12 * var11 + var12 * var10 + var12];
            if(var19 >= 0) {
               var1.a((BlockPosition)var2, (IBlockData)var3.set(b, Boolean.valueOf(false)), 4);
            } else {
               this.b(var1, var2);
            }
         }

      }
   }

   private void b(World var1, BlockPosition var2) {
      this.b(var1, var2, var1.getType(var2), 0);
      var1.g(var2);
   }

   public int a(Random var1) {
      return var1.nextInt(20) == 0?1:0;
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Item.a(Blocks.g);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, float var4, int var5) {
      if(!var1.E) {
         int var6 = this.i(var3);
         if(var5 > 0) {
            var6 -= 2 << var5;
            if(var6 < 10) {
               var6 = 10;
            }
         }

         if(var1.r.nextInt(var6) == 0) {
            Item var7 = this.a(var3, var1.r, var5);
            a(var1, var2, new ItemStack(var7, 1, this.d(var3)));
         }

         var6 = 200;
         if(var5 > 0) {
            var6 -= 10 << var5;
            if(var6 < 40) {
               var6 = 40;
            }
         }

         this.a(var1, var2, var3, var6);
      }

   }

   protected void a(World var1, BlockPosition var2, IBlockData var3, int var4) {
   }

   protected int i(IBlockData var1) {
      return 20;
   }

   public boolean b(IBlockData var1) {
      return !this.c;
   }

   public boolean j() {
      return false;
   }

   public abstract BlockWood.EnumLogVariant e(int var1);
}
