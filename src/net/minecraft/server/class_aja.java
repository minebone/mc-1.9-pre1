package net.minecraft.server;

import net.minecraft.server.BiomeBase;
import net.minecraft.server.Blocks;

public class class_aja extends BiomeBase {
   public class_aja(BiomeBase.class_a_in_class_aif var1) {
      super(var1);
      this.v.clear();
      this.r = Blocks.b.u();
      this.s = Blocks.b.u();
      this.t.z = -999;
      this.t.C = 0;
      this.t.E = 0;
      this.t.F = 0;
   }
}
