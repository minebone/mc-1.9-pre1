package net.minecraft.server;

import java.util.UUID;
import net.minecraft.server.IChatBaseComponent;

public abstract class BossBar {
   private final UUID uniqueId;
   protected IChatBaseComponent text;
   protected float health;
   protected BossBar.EnumBossBarColor color;
   protected BossBar.EnumBossBarType type;
   protected boolean e;
   protected boolean f;
   protected boolean g;

   public BossBar(UUID var1, IChatBaseComponent var2, BossBar.EnumBossBarColor var3, BossBar.EnumBossBarType var4) {
      this.uniqueId = var1;
      this.text = var2;
      this.color = var3;
      this.type = var4;
      this.health = 1.0F;
   }

   public UUID d() {
      return this.uniqueId;
   }

   public IChatBaseComponent e() {
      return this.text;
   }

   public float f() {
      return this.health;
   }

   public void a(float var1) {
      this.health = var1;
   }

   public BossBar.EnumBossBarColor g() {
      return this.color;
   }

   public BossBar.EnumBossBarType h() {
      return this.type;
   }

   public boolean i() {
      return this.e;
   }

   public BossBar a(boolean var1) {
      this.e = var1;
      return this;
   }

   public boolean j() {
      return this.f;
   }

   public BossBar b(boolean var1) {
      this.f = var1;
      return this;
   }

   public BossBar c(boolean var1) {
      this.g = var1;
      return this;
   }

   public boolean k() {
      return this.g;
   }

   public static enum EnumBossBarType {
      PROGRESS,
      NOTCHED_6,
      NOTCHED_10,
      NOTCHED_12,
      NOTCHED_20;
   }

   public static enum EnumBossBarColor {
      PINK,
      BLUE,
      RED,
      GREEN,
      YELLOW,
      PURPLE,
      WHITE;
   }
}
