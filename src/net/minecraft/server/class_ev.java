package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import net.minecraft.server.ChatComponentScore;
import net.minecraft.server.ChatComponentSelector;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.ChatModifier;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.ExceptionEntityNotFound;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.PlayerSelector;
import net.minecraft.server.class_bz;
import net.minecraft.server.ICommandListener;

public class class_ev {
   public static IChatBaseComponent a(ICommandListener var0, IChatBaseComponent var1, Entity var2) throws class_bz {
      Object var3 = null;
      if(var1 instanceof ChatComponentScore) {
         ChatComponentScore var4 = (ChatComponentScore)var1;
         String var5 = var4.g();
         if(PlayerSelector.b(var5)) {
            List var6 = PlayerSelector.b(var0, var5, Entity.class);
            if(var6.size() != 1) {
               throw new ExceptionEntityNotFound();
            }

            Entity var7 = (Entity)var6.get(0);
            if(var7 instanceof EntityHuman) {
               var5 = var7.h_();
            } else {
               var5 = var7.getUniqueId().toString();
            }
         }

         var3 = var2 != null && var5.equals("*")?new ChatComponentScore(var2.h_(), var4.h()):new ChatComponentScore(var5, var4.h());
         ((ChatComponentScore)var3).a(var0);
      } else if(var1 instanceof ChatComponentSelector) {
         String var8 = ((ChatComponentSelector)var1).g();
         var3 = PlayerSelector.b(var0, var8);
         if(var3 == null) {
            var3 = new ChatComponentText("");
         }
      } else if(var1 instanceof ChatComponentText) {
         var3 = new ChatComponentText(((ChatComponentText)var1).g());
      } else {
         if(!(var1 instanceof ChatMessage)) {
            return var1;
         }

         Object[] var9 = ((ChatMessage)var1).j();

         for(int var12 = 0; var12 < var9.length; ++var12) {
            Object var11 = var9[var12];
            if(var11 instanceof IChatBaseComponent) {
               var9[var12] = a(var0, (IChatBaseComponent)var11, var2);
            }
         }

         var3 = new ChatMessage(((ChatMessage)var1).i(), var9);
      }

      ChatModifier var10 = var1.b();
      if(var10 != null) {
         ((IChatBaseComponent)var3).a(var10.m());
      }

      Iterator var14 = var1.a().iterator();

      while(var14.hasNext()) {
         IChatBaseComponent var13 = (IChatBaseComponent)var14.next();
         ((IChatBaseComponent)var3).a(a(var0, var13, var2));
      }

      return (IChatBaseComponent)var3;
   }
}
