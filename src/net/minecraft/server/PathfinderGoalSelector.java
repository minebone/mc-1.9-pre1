package net.minecraft.server;

import com.google.common.collect.Sets;
import java.util.Iterator;
import java.util.Set;
import net.minecraft.server.class_oo;
import net.minecraft.server.class_tj;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PathfinderGoalSelector {
   private static final Logger a = LogManager.getLogger();
   private final Set b = Sets.newLinkedHashSet();
   private final Set c = Sets.newLinkedHashSet();
   private final class_oo d;
   private int e;
   private int f = 3;
   private int g = 0;

   public PathfinderGoalSelector(class_oo var1) {
      this.d = var1;
   }

   public void a(int var1, class_tj var2) {
      this.b.add(new PathfinderGoalSelector.class_a_in_class_tk(var1, var2));
   }

   public void a(class_tj var1) {
      Iterator var2 = this.b.iterator();

      PathfinderGoalSelector.class_a_in_class_tk var3;
      class_tj var4;
      do {
         if(!var2.hasNext()) {
            return;
         }

         var3 = (PathfinderGoalSelector.class_a_in_class_tk)var2.next();
         var4 = var3.a;
      } while(var4 != var1);

      if(var3.c) {
         var3.c = false;
         var3.a.d();
         this.c.remove(var3);
      }

      var2.remove();
   }

   public void a() {
      this.d.a("goalSetup");
      Iterator var1;
      PathfinderGoalSelector.class_a_in_class_tk var2;
      if(this.e++ % this.f == 0) {
         var1 = this.b.iterator();

         label57:
         while(true) {
            do {
               while(true) {
                  if(!var1.hasNext()) {
                     break label57;
                  }

                  var2 = (PathfinderGoalSelector.class_a_in_class_tk)var1.next();
                  if(var2.c) {
                     break;
                  }

                  if(this.b(var2) && var2.a.a()) {
                     var2.c = true;
                     var2.a.c();
                     this.c.add(var2);
                  }
               }
            } while(this.b(var2) && this.a(var2));

            var2.c = false;
            var2.a.d();
            this.c.remove(var2);
         }
      } else {
         var1 = this.c.iterator();

         while(var1.hasNext()) {
            var2 = (PathfinderGoalSelector.class_a_in_class_tk)var1.next();
            if(!this.a(var2)) {
               var2.c = false;
               var2.a.d();
               var1.remove();
            }
         }
      }

      this.d.b();
      if(!this.c.isEmpty()) {
         this.d.a("goalTick");
         var1 = this.c.iterator();

         while(var1.hasNext()) {
            var2 = (PathfinderGoalSelector.class_a_in_class_tk)var1.next();
            var2.a.e();
         }

         this.d.b();
      }

   }

   private boolean a(PathfinderGoalSelector.class_a_in_class_tk var1) {
      return var1.a.b();
   }

   private boolean b(PathfinderGoalSelector.class_a_in_class_tk var1) {
      if(this.c.isEmpty()) {
         return true;
      } else if(this.b(var1.a.h())) {
         return false;
      } else {
         Iterator var2 = this.c.iterator();

         while(var2.hasNext()) {
            PathfinderGoalSelector.class_a_in_class_tk var3 = (PathfinderGoalSelector.class_a_in_class_tk)var2.next();
            if(var3 != var1) {
               if(var1.b >= var3.b) {
                  if(!this.a(var1, var3)) {
                     return false;
                  }
               } else if(!var3.a.g()) {
                  return false;
               }
            }
         }

         return true;
      }
   }

   private boolean a(PathfinderGoalSelector.class_a_in_class_tk var1, PathfinderGoalSelector.class_a_in_class_tk var2) {
      return (var1.a.h() & var2.a.h()) == 0;
   }

   public boolean b(int var1) {
      return (this.g & var1) > 0;
   }

   public void c(int var1) {
      this.g |= var1;
   }

   public void d(int var1) {
      this.g &= ~var1;
   }

   public void a(int var1, boolean var2) {
      if(var2) {
         this.d(var1);
      } else {
         this.c(var1);
      }

   }

   class class_a_in_class_tk {
      public final class_tj a;
      public final int b;
      public boolean c;

      public class_a_in_class_tk(int var2, class_tj var3) {
         this.b = var2;
         this.a = var3;
      }

      public boolean equals(Object var1) {
         return this == var1?true:(var1 != null && this.getClass() == var1.getClass()?this.a.equals(((PathfinderGoalSelector.class_a_in_class_tk)var1).a):false);
      }

      public int hashCode() {
         return this.a.hashCode();
      }
   }
}
