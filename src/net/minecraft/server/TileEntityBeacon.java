package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import net.minecraft.server.AchievementList;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Container;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumColor;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Items;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.MobEffectType;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.Statistic;
import net.minecraft.server.class_aaw;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aoq;
import net.minecraft.server.class_aor;
import net.minecraft.server.class_aqi;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutTileEntityData;
import net.minecraft.server.class_ky;
import net.minecraft.server.class_qx;
import net.minecraft.server.class_wd;

public class TileEntityBeacon extends class_aqi implements class_ky, class_qx {
   public static final MobEffectType[][] a = new MobEffectType[][]{{MobEffectList.a, MobEffectList.c}, {MobEffectList.k, MobEffectList.h}, {MobEffectList.e}, {MobEffectList.j}};
   private static final Set f = Sets.newHashSet();
   private final List g = Lists.newArrayList();
   private boolean j;
   private int k = -1;
   private MobEffectType l;
   private MobEffectType m;
   private ItemStack n;
   private String o;

   public void c() {
      if(this.b.P() % 80L == 0L) {
         this.m();
      }

   }

   public void m() {
      if(this.b != null) {
         this.F();
         this.E();
      }

   }

   private void E() {
      if(this.j && this.k > 0 && !this.b.E && this.l != null) {
         double var1 = (double)(this.k * 10 + 10);
         byte var3 = 0;
         if(this.k >= 4 && this.l == this.m) {
            var3 = 1;
         }

         int var4 = (9 + this.k * 2) * 20;
         int var5 = this.c.p();
         int var6 = this.c.q();
         int var7 = this.c.r();
         AxisAlignedBB var8 = (new AxisAlignedBB((double)var5, (double)var6, (double)var7, (double)(var5 + 1), (double)(var6 + 1), (double)(var7 + 1))).g(var1).a(0.0D, (double)this.b.Y(), 0.0D);
         List var9 = this.b.a(EntityHuman.class, var8);
         Iterator var10 = var9.iterator();

         EntityHuman var11;
         while(var10.hasNext()) {
            var11 = (EntityHuman)var10.next();
            var11.c(new MobEffect(this.l, var4, var3, true, true));
         }

         if(this.k >= 4 && this.l != this.m && this.m != null) {
            var10 = var9.iterator();

            while(var10.hasNext()) {
               var11 = (EntityHuman)var10.next();
               var11.c(new MobEffect(this.m, var4, 0, true, true));
            }
         }
      }

   }

   private void F() {
      int var1 = this.k;
      int var2 = this.c.p();
      int var3 = this.c.q();
      int var4 = this.c.r();
      this.k = 0;
      this.g.clear();
      this.j = true;
      TileEntityBeacon.class_a_in_class_apt var5 = new TileEntityBeacon.class_a_in_class_apt(class_wd.a(EnumColor.WHITE));
      this.g.add(var5);
      boolean var6 = true;
      BlockPosition.class_a_in_class_cj var7 = new BlockPosition.class_a_in_class_cj();

      int var8;
      for(var8 = var3 + 1; var8 < 256; ++var8) {
         IBlockData var9 = this.b.getType(var7.c(var2, var8, var4));
         float[] var10;
         if(var9.getBlock() == Blocks.cG) {
            var10 = class_wd.a((EnumColor)var9.get(class_aoq.a));
         } else {
            if(var9.getBlock() != Blocks.cH) {
               if(var9.c() >= 15 && var9.getBlock() != Blocks.h) {
                  this.j = false;
                  this.g.clear();
                  break;
               }

               var5.a();
               continue;
            }

            var10 = class_wd.a((EnumColor)var9.get(class_aor.a));
         }

         if(!var6) {
            var10 = new float[]{(var5.b()[0] + var10[0]) / 2.0F, (var5.b()[1] + var10[1]) / 2.0F, (var5.b()[2] + var10[2]) / 2.0F};
         }

         if(Arrays.equals(var10, var5.b())) {
            var5.a();
         } else {
            var5 = new TileEntityBeacon.class_a_in_class_apt(var10);
            this.g.add(var5);
         }

         var6 = false;
      }

      if(this.j) {
         for(var8 = 1; var8 <= 4; this.k = var8++) {
            int var15 = var3 - var8;
            if(var15 < 0) {
               break;
            }

            boolean var17 = true;

            for(int var11 = var2 - var8; var11 <= var2 + var8 && var17; ++var11) {
               for(int var12 = var4 - var8; var12 <= var4 + var8; ++var12) {
                  Block var13 = this.b.getType(new BlockPosition(var11, var15, var12)).getBlock();
                  if(var13 != Blocks.bT && var13 != Blocks.R && var13 != Blocks.ah && var13 != Blocks.S) {
                     var17 = false;
                     break;
                  }
               }
            }

            if(!var17) {
               break;
            }
         }

         if(this.k == 0) {
            this.j = false;
         }
      }

      if(!this.b.E && this.k == 4 && var1 < this.k) {
         Iterator var14 = this.b.a(EntityHuman.class, (new AxisAlignedBB((double)var2, (double)var3, (double)var4, (double)var2, (double)(var3 - 4), (double)var4)).b(10.0D, 5.0D, 10.0D)).iterator();

         while(var14.hasNext()) {
            EntityHuman var16 = (EntityHuman)var14.next();
            var16.b((Statistic)AchievementList.K);
         }
      }

   }

   public Packet D_() {
      NBTTagCompound var1 = new NBTTagCompound();
      this.a(var1);
      return new PacketPlayOutTileEntityData(this.c, 3, var1);
   }

   private static MobEffectType f(int var0) {
      MobEffectType var1 = MobEffectType.a(var0);
      return f.contains(var1)?var1:null;
   }

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      this.l = f(var2.h("Primary"));
      this.m = f(var2.h("Secondary"));
      this.k = var2.h("Levels");
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      var1.a("Primary", MobEffectType.a(this.l));
      var1.a("Secondary", MobEffectType.a(this.m));
      var1.a("Levels", this.k);
   }

   public int u_() {
      return 1;
   }

   public ItemStack a(int var1) {
      return var1 == 0?this.n:null;
   }

   public ItemStack a(int var1, int var2) {
      if(var1 == 0 && this.n != null) {
         if(var2 >= this.n.b) {
            ItemStack var3 = this.n;
            this.n = null;
            return var3;
         } else {
            this.n.b -= var2;
            return new ItemStack(this.n.b(), var2, this.n.i());
         }
      } else {
         return null;
      }
   }

   public ItemStack b(int var1) {
      if(var1 == 0) {
         ItemStack var2 = this.n;
         this.n = null;
         return var2;
      } else {
         return null;
      }
   }

   public void a(int var1, ItemStack var2) {
      if(var1 == 0) {
         this.n = var2;
      }

   }

   public String h_() {
      return this.o_()?this.o:"container.beacon";
   }

   public boolean o_() {
      return this.o != null && !this.o.isEmpty();
   }

   public void a(String var1) {
      this.o = var1;
   }

   public int w_() {
      return 1;
   }

   public boolean a(EntityHuman var1) {
      return this.b.r(this.c) != this?false:var1.e((double)this.c.p() + 0.5D, (double)this.c.q() + 0.5D, (double)this.c.r() + 0.5D) <= 64.0D;
   }

   public void b(EntityHuman var1) {
   }

   public void c(EntityHuman var1) {
   }

   public boolean b(int var1, ItemStack var2) {
      return var2.b() == Items.bY || var2.b() == Items.k || var2.b() == Items.m || var2.b() == Items.l;
   }

   public String k() {
      return "minecraft:beacon";
   }

   public Container a(PlayerInventory var1, EntityHuman var2) {
      return new class_aaw(var1, this);
   }

   public int c_(int var1) {
      switch(var1) {
      case 0:
         return this.k;
      case 1:
         return MobEffectType.a(this.l);
      case 2:
         return MobEffectType.a(this.m);
      default:
         return 0;
      }
   }

   public void b(int var1, int var2) {
      switch(var1) {
      case 0:
         this.k = var2;
         break;
      case 1:
         this.l = f(var2);
         break;
      case 2:
         this.m = f(var2);
      }

   }

   public int g() {
      return 3;
   }

   public void l() {
      this.n = null;
   }

   public boolean c(int var1, int var2) {
      if(var1 == 1) {
         this.m();
         return true;
      } else {
         return super.c(var1, var2);
      }
   }

   public int[] a(EnumDirection var1) {
      return new int[0];
   }

   public boolean a(int var1, ItemStack var2, EnumDirection var3) {
      return false;
   }

   public boolean b(int var1, ItemStack var2, EnumDirection var3) {
      return false;
   }

   static {
      MobEffectType[][] var0 = a;
      int var1 = var0.length;

      for(int var2 = 0; var2 < var1; ++var2) {
         MobEffectType[] var3 = var0[var2];
         Collections.addAll(f, var3);
      }

   }

   public static class class_a_in_class_apt {
      private final float[] a;
      private int b;

      public class_a_in_class_apt(float[] var1) {
         this.a = var1;
         this.b = 1;
      }

      protected void a() {
         ++this.b;
      }

      public float[] b() {
         return this.a;
      }
   }
}
