package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;

public class CommandXp extends CommandAbstract {
   public String c() {
      return "xp";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.xp.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length <= 0) {
         throw new class_cf("commands.xp.usage", new Object[0]);
      } else {
         String var4 = var3[0];
         boolean var5 = var4.endsWith("l") || var4.endsWith("L");
         if(var5 && var4.length() > 1) {
            var4 = var4.substring(0, var4.length() - 1);
         }

         int var6 = a(var4);
         boolean var7 = var6 < 0;
         if(var7) {
            var6 *= -1;
         }

         EntityPlayer var8 = var3.length > 1?a(var1, var2, var3[1]):a(var2);
         if(var5) {
            var2.a(CommandObjectiveExecutor.EnumCommandResult.QUERY_RESULT, var8.bK);
            if(var7) {
               var8.a(-var6);
               a(var2, this, "commands.xp.success.negative.levels", new Object[]{Integer.valueOf(var6), var8.h_()});
            } else {
               var8.a(var6);
               a(var2, this, "commands.xp.success.levels", new Object[]{Integer.valueOf(var6), var8.h_()});
            }
         } else {
            var2.a(CommandObjectiveExecutor.EnumCommandResult.QUERY_RESULT, var8.bL);
            if(var7) {
               throw new class_bz("commands.xp.failure.widthdrawXp", new Object[0]);
            }

            var8.n(var6);
            a(var2, this, "commands.xp.success", new Object[]{Integer.valueOf(var6), var8.h_()});
         }

      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 2?a(var3, var1.J()):Collections.emptyList();
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 1;
   }
}
