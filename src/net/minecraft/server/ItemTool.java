package net.minecraft.server;

import com.google.common.collect.Multimap;
import java.util.Set;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_ys;

public class ItemTool extends Item {
   private Set e;
   protected float a;
   protected float b;
   protected float c;
   protected Item.class_a_in_class_adn d;

   protected ItemTool(float var1, float var2, Item.class_a_in_class_adn var3, Set var4) {
      this.a = 4.0F;
      this.d = var3;
      this.e = var4;
      this.j = 1;
      this.e(var3.a());
      this.a = var3.b();
      this.b = var1 + var3.c();
      this.c = var2;
      this.a(CreativeModeTab.i);
   }

   protected ItemTool(Item.class_a_in_class_adn var1, Set var2) {
      this(0.0F, 0.0F, var1, var2);
   }

   public float a(ItemStack var1, IBlockData var2) {
      return this.e.contains(var2.getBlock())?this.a:1.0F;
   }

   public boolean a(ItemStack var1, class_rz var2, class_rz var3) {
      var1.a(2, (class_rz)var3);
      return true;
   }

   public boolean a(ItemStack var1, World var2, IBlockData var3, BlockPosition var4, class_rz var5) {
      if((double)var3.b(var2, var4) != 0.0D) {
         var1.a(1, (class_rz)var5);
      }

      return true;
   }

   public Item.class_a_in_class_adn g() {
      return this.d;
   }

   public int c() {
      return this.d.e();
   }

   public String h() {
      return this.d.toString();
   }

   public boolean a(ItemStack var1, ItemStack var2) {
      return this.d.f() == var2.b()?true:super.a(var1, var2);
   }

   public Multimap a(EnumInventorySlot var1) {
      Multimap var2 = super.a(var1);
      if(var1 == EnumInventorySlot.MAINHAND) {
         var2.put(class_ys.e.a(), new AttributeModifier(g, "Tool modifier", (double)this.b, 0));
         var2.put(class_ys.f.a(), new AttributeModifier(h, "Tool modifier", (double)this.c, 0));
      }

      return var2;
   }
}
