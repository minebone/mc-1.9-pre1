package net.minecraft.server;

import java.util.UUID;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityCreature;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_azs;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.class_qk;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sl;
import net.minecraft.server.class_uu;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_ys;
import net.minecraft.server.class_yz;

public class class_yq extends class_yz {
   private static final UUID b = UUID.fromString("49455A49-7EC5-45BA-B886-3B90B23A1718");
   private static final AttributeModifier c = (new AttributeModifier(b, "Attacking speed boost", 0.05D, 0)).a(false);
   private int bv;
   private int bw;
   private UUID bx;

   public class_yq(World var1) {
      super(var1);
      this.fireProof = true;
   }

   public void a(class_rz var1) {
      super.a((class_rz)var1);
      if(var1 != null) {
         this.bx = var1.getUniqueId();
      }

   }

   protected void o() {
      this.bq.a(1, new class_yq.class_b_in_class_yq(this));
      this.bq.a(2, new class_yq.class_a_in_class_yq(this));
   }

   protected void bz() {
      super.bz();
      this.a((class_sk)a).a(0.0D);
      this.a((class_sk)class_ys.d).a(0.23000000417232513D);
      this.a((class_sk)class_ys.e).a(5.0D);
   }

   public void m() {
      super.m();
   }

   protected void M() {
      class_sl var1 = this.a((class_sk)class_ys.d);
      if(this.da()) {
         if(!this.m_() && !var1.a(c)) {
            var1.b(c);
         }

         --this.bv;
      } else if(var1.a(c)) {
         var1.c(c);
      }

      if(this.bw > 0 && --this.bw == 0) {
         this.a(class_ng.hj, this.cc() * 2.0F, ((this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F) * 1.8F);
      }

      if(this.bv > 0 && this.bx != null && this.bF() == null) {
         EntityHuman var2 = this.world.b(this.bx);
         this.a((class_rz)var2);
         this.aR = var2;
         this.aS = this.bG();
      }

      super.M();
   }

   public boolean cF() {
      return this.world.ae() != EnumDifficulty.PEACEFUL;
   }

   public boolean cG() {
      return this.world.a((AxisAlignedBB)this.bk(), (Entity)this) && this.world.a((Entity)this, (AxisAlignedBB)this.bk()).isEmpty() && !this.world.e(this.bk());
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("Anger", (short)this.bv);
      if(this.bx != null) {
         var1.a("HurtBy", this.bx.toString());
      } else {
         var1.a("HurtBy", "");
      }

   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.bv = var1.g("Anger");
      String var2 = var1.l("HurtBy");
      if(!var2.isEmpty()) {
         this.bx = UUID.fromString(var2);
         EntityHuman var3 = this.world.b(this.bx);
         this.a((class_rz)var3);
         if(var3 != null) {
            this.aR = var3;
            this.aS = this.bG();
         }
      }

   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b(var1)) {
         return false;
      } else {
         Entity var3 = var1.j();
         if(var3 instanceof EntityHuman) {
            this.a(var3);
         }

         return super.a(var1, var2);
      }
   }

   private void a(Entity var1) {
      this.bv = 400 + this.random.nextInt(400);
      this.bw = this.random.nextInt(40);
      if(var1 instanceof class_rz) {
         this.a((class_rz)var1);
      }

   }

   public boolean da() {
      return this.bv > 0;
   }

   protected class_nf G() {
      return class_ng.hi;
   }

   protected class_nf bQ() {
      return class_ng.hl;
   }

   protected class_nf bR() {
      return class_ng.hk;
   }

   protected class_kk J() {
      return class_azs.ai;
   }

   public boolean a(EntityHuman var1, EnumHand var2, ItemStack var3) {
      return false;
   }

   protected void a(class_qk var1) {
      this.a((EnumInventorySlot)EnumInventorySlot.MAINHAND, (ItemStack)(new ItemStack(Items.D)));
   }

   public class_sc a(class_qk var1, class_sc var2) {
      super.a(var1, var2);
      this.df();
      return var2;
   }

   static class class_a_in_class_yq extends class_ux {
      public class_a_in_class_yq(class_yq var1) {
         super(var1, EntityHuman.class, true);
      }

      public boolean a() {
         return ((class_yq)this.e).da() && super.a();
      }
   }

   static class class_b_in_class_yq extends class_uu {
      public class_b_in_class_yq(class_yq var1) {
         super(var1, true, new Class[0]);
      }

      protected void a(EntityCreature var1, class_rz var2) {
         super.a(var1, var2);
         if(var1 instanceof class_yq) {
            ((class_yq)var1).a((Entity)var2);
         }

      }
   }
}
