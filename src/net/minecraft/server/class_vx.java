package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.EntityAnimal;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Items;
import net.minecraft.server.PathfinderGoalTempt;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rn;
import net.minecraft.server.class_tc;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_ti;
import net.minecraft.server.class_to;
import net.minecraft.server.class_ub;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_ys;

public class class_vx extends EntityAnimal {
   public class_vx(World var1) {
      super(var1);
      this.a(0.9F, 1.4F);
   }

   protected void r() {
      this.bp.a(0, new class_tg(this));
      this.bp.a(1, new class_ub(this, 2.0D));
      this.bp.a(2, new class_tc(this, 1.0D));
      this.bp.a(3, new PathfinderGoalTempt(this, 1.25D, Items.Q, false));
      this.bp.a(4, new class_ti(this, 1.25D));
      this.bp.a(5, new class_uf(this, 1.0D));
      this.bp.a(6, new class_to(this, EntityHuman.class, 6.0F));
      this.bp.a(7, new class_ue(this));
   }

   protected void bz() {
      super.bz();
      this.a(class_ys.a).a(10.0D);
      this.a(class_ys.d).a(0.20000000298023224D);
   }

   protected class_nf G() {
      return class_ng.ak;
   }

   protected class_nf bQ() {
      return class_ng.am;
   }

   protected class_nf bR() {
      return class_ng.al;
   }

   protected void a(BlockPosition var1, Block var2) {
      this.a(class_ng.ao, 0.15F, 1.0F);
   }

   protected float cc() {
      return 0.4F;
   }

   protected class_kk J() {
      return class_azs.G;
   }

   public boolean a(EntityHuman var1, EnumHand var2, ItemStack var3) {
      if(var3 != null && var3.b() == Items.ay && !var1.abilities.d && !this.m_()) {
         var1.a(class_ng.an, 1.0F, 1.0F);
         if(--var3.b == 0) {
            var1.a((EnumHand)var2, (ItemStack)(new ItemStack(Items.aN)));
         } else if(!var1.br.c(new ItemStack(Items.aN))) {
            var1.a(new ItemStack(Items.aN), false);
         }

         return true;
      } else {
         return super.a(var1, var2, var3);
      }
   }

   public class_vx b(class_rn var1) {
      return new class_vx(this.world);
   }

   public float bm() {
      return this.m_()?this.length:1.3F;
   }

   // $FF: synthetic method
   public class_rn a(class_rn var1) {
      return this.b(var1);
   }
}
