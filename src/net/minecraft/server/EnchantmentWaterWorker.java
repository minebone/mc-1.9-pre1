package net.minecraft.server;

import net.minecraft.server.Enchantment;
import net.minecraft.server.class_agm;
import net.minecraft.server.EnumInventorySlot;

public class EnchantmentWaterWorker extends Enchantment {
   public EnchantmentWaterWorker(Enchantment.class_a_in_class_agl var1, EnumInventorySlot... var2) {
      super(var1, class_agm.ARMOR_HEAD, var2);
      this.c("waterWorker");
   }

   public int a(int var1) {
      return 1;
   }

   public int b(int var1) {
      return this.a(var1) + 40;
   }

   public int b() {
      return 1;
   }
}
