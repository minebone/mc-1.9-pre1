package net.minecraft.server;

import com.google.common.collect.ImmutableList;
import java.util.List;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.class_co;
import net.minecraft.server.class_kk;

public class class_afd {
   private static final class_kk b = new class_kk("water");
   public static final class_co a = new class_co(b);
   private static int c;
   private final String d;
   private final ImmutableList e;

   public static int a(class_afd var0) {
      return a.a((Object)var0);
   }

   public static class_afd a(String var0) {
      return (class_afd)a.c(new class_kk(var0));
   }

   public class_afd(MobEffect... var1) {
      this((String)null, var1);
   }

   public class_afd(String var1, MobEffect... var2) {
      this.d = var1;
      this.e = ImmutableList.copyOf((Object[])var2);
   }

   public String b(String var1) {
      return this.d == null?var1 + ((class_kk)a.b(this)).a():var1 + this.d;
   }

   public List a() {
      return this.e;
   }

   public static void b() {
      a("empty", new class_afd(new MobEffect[0]));
      a("water", new class_afd(new MobEffect[0]));
      a("mundane", new class_afd(new MobEffect[0]));
      a("thick", new class_afd(new MobEffect[0]));
      a("awkward", new class_afd(new MobEffect[0]));
      a("night_vision", new class_afd(new MobEffect[]{new MobEffect(MobEffectList.p, 3600)}));
      a("long_night_vision", new class_afd("night_vision", new MobEffect[]{new MobEffect(MobEffectList.p, 9600)}));
      a("invisibility", new class_afd(new MobEffect[]{new MobEffect(MobEffectList.n, 3600)}));
      a("long_invisibility", new class_afd("invisibility", new MobEffect[]{new MobEffect(MobEffectList.n, 9600)}));
      a("leaping", new class_afd(new MobEffect[]{new MobEffect(MobEffectList.h, 3600)}));
      a("long_leaping", new class_afd("leaping", new MobEffect[]{new MobEffect(MobEffectList.h, 9600)}));
      a("strong_leaping", new class_afd("leaping", new MobEffect[]{new MobEffect(MobEffectList.h, 1800, 1)}));
      a("fire_resistance", new class_afd(new MobEffect[]{new MobEffect(MobEffectList.l, 3600)}));
      a("long_fire_resistance", new class_afd("fire_resistance", new MobEffect[]{new MobEffect(MobEffectList.l, 9600)}));
      a("swiftness", new class_afd(new MobEffect[]{new MobEffect(MobEffectList.a, 3600)}));
      a("long_swiftness", new class_afd("swiftness", new MobEffect[]{new MobEffect(MobEffectList.a, 9600)}));
      a("strong_swiftness", new class_afd("swiftness", new MobEffect[]{new MobEffect(MobEffectList.a, 1800, 1)}));
      a("slowness", new class_afd(new MobEffect[]{new MobEffect(MobEffectList.b, 1800)}));
      a("long_slowness", new class_afd("slowness", new MobEffect[]{new MobEffect(MobEffectList.b, 4800)}));
      a("water_breathing", new class_afd(new MobEffect[]{new MobEffect(MobEffectList.m, 3600)}));
      a("long_water_breathing", new class_afd("water_breathing", new MobEffect[]{new MobEffect(MobEffectList.m, 9600)}));
      a("healing", new class_afd(new MobEffect[]{new MobEffect(MobEffectList.f, 1)}));
      a("strong_healing", new class_afd("healing", new MobEffect[]{new MobEffect(MobEffectList.f, 1, 1)}));
      a("harming", new class_afd(new MobEffect[]{new MobEffect(MobEffectList.g, 1)}));
      a("strong_harming", new class_afd("harming", new MobEffect[]{new MobEffect(MobEffectList.g, 1, 1)}));
      a("poison", new class_afd(new MobEffect[]{new MobEffect(MobEffectList.s, 900)}));
      a("long_poison", new class_afd("poison", new MobEffect[]{new MobEffect(MobEffectList.s, 1800)}));
      a("strong_poison", new class_afd("poison", new MobEffect[]{new MobEffect(MobEffectList.s, 432, 1)}));
      a("regeneration", new class_afd(new MobEffect[]{new MobEffect(MobEffectList.j, 900)}));
      a("long_regeneration", new class_afd("regeneration", new MobEffect[]{new MobEffect(MobEffectList.j, 1800)}));
      a("strong_regeneration", new class_afd("regeneration", new MobEffect[]{new MobEffect(MobEffectList.j, 450, 1)}));
      a("strength", new class_afd(new MobEffect[]{new MobEffect(MobEffectList.e, 3600)}));
      a("long_strength", new class_afd("strength", new MobEffect[]{new MobEffect(MobEffectList.e, 9600)}));
      a("strong_strength", new class_afd("strength", new MobEffect[]{new MobEffect(MobEffectList.e, 1800, 1)}));
      a("weakness", new class_afd(new MobEffect[]{new MobEffect(MobEffectList.r, 1800)}));
      a("long_weakness", new class_afd("weakness", new MobEffect[]{new MobEffect(MobEffectList.r, 4800)}));
      a("luck", new class_afd("luck", new MobEffect[]{new MobEffect(MobEffectList.z, 6000)}));
      a.a();
   }

   protected static void a(String var0, class_afd var1) {
      a.a(c++, new class_kk(var0), var1);
   }
}
