package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MojangsonParser;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ec;
import net.minecraft.server.ICommandListener;

public class CommandBlockData extends CommandAbstract {
   public String c() {
      return "blockdata";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.blockdata.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 4) {
         throw new class_cf("commands.blockdata.usage", new Object[0]);
      } else {
         var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_BLOCKS, 0);
         BlockPosition var4 = a(var2, var3, 0, false);
         World var5 = var2.e();
         if(!var5.e(var4)) {
            throw new class_bz("commands.blockdata.outOfWorld", new Object[0]);
         } else {
            IBlockData var6 = var5.getType(var4);
            TileEntity var7 = var5.r(var4);
            if(var7 == null) {
               throw new class_bz("commands.blockdata.notValid", new Object[0]);
            } else {
               NBTTagCompound var8 = new NBTTagCompound();
               var7.a(var8);
               NBTTagCompound var9 = (NBTTagCompound)var8.b();

               NBTTagCompound var10;
               try {
                  var10 = MojangsonParser.a(a(var2, var3, 3).c());
               } catch (class_ec var12) {
                  throw new class_bz("commands.blockdata.tagError", new Object[]{var12.getMessage()});
               }

               var8.a(var10);
               var8.a("x", var4.p());
               var8.a("y", var4.q());
               var8.a("z", var4.r());
               if(var8.equals(var9)) {
                  throw new class_bz("commands.blockdata.failed", new Object[]{var8.toString()});
               } else {
                  var7.a(var1, var8);
                  var7.v_();
                  var5.a(var4, var6, var6, 3);
                  var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_BLOCKS, 1);
                  a(var2, this, "commands.blockdata.success", new Object[]{var8.toString()});
               }
            }
         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length > 0 && var3.length <= 3?a(var3, 0, var4):Collections.emptyList();
   }
}
