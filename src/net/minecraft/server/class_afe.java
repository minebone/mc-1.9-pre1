package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.Item;
import net.minecraft.server.ItemFish;
import net.minecraft.server.Items;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aec;
import net.minecraft.server.class_afd;
import net.minecraft.server.class_aff;
import net.minecraft.server.class_afg;

public class class_afe {
   private static final List a = Lists.newArrayList();
   private static final List b = Lists.newArrayList();
   private static final List c = Lists.newArrayList();
   private static final Predicate d = new Predicate() {
      public boolean a(ItemStack var1) {
         Iterator var2 = class_afe.c.iterator();

         class_afe.class_a_in_class_afe var3;
         do {
            if(!var2.hasNext()) {
               return false;
            }

            var3 = (class_afe.class_a_in_class_afe)var2.next();
         } while(!var3.a(var1));

         return true;
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((ItemStack)var1);
      }
   };

   public static boolean a(ItemStack var0) {
      return b(var0) || c(var0);
   }

   protected static boolean b(ItemStack var0) {
      int var1 = 0;

      for(int var2 = b.size(); var1 < var2; ++var1) {
         if(((class_afe.class_b_in_class_afe)b.get(var1)).b.apply(var0)) {
            return true;
         }
      }

      return false;
   }

   protected static boolean c(ItemStack var0) {
      int var1 = 0;

      for(int var2 = a.size(); var1 < var2; ++var1) {
         if(((class_afe.class_b_in_class_afe)a.get(var1)).b.apply(var0)) {
            return true;
         }
      }

      return false;
   }

   public static boolean a(ItemStack var0, ItemStack var1) {
      return !d.apply(var0)?false:b(var0, var1) || c(var0, var1);
   }

   protected static boolean b(ItemStack var0, ItemStack var1) {
      Item var2 = var0.b();
      int var3 = 0;

      for(int var4 = b.size(); var3 < var4; ++var3) {
         class_afe.class_b_in_class_afe var5 = (class_afe.class_b_in_class_afe)b.get(var3);
         if(var5.a == var2 && var5.b.apply(var1)) {
            return true;
         }
      }

      return false;
   }

   protected static boolean c(ItemStack var0, ItemStack var1) {
      class_afd var2 = class_aff.c(var0);
      int var3 = 0;

      for(int var4 = a.size(); var3 < var4; ++var3) {
         class_afe.class_b_in_class_afe var5 = (class_afe.class_b_in_class_afe)a.get(var3);
         if(var5.a == var2 && var5.b.apply(var1)) {
            return true;
         }
      }

      return false;
   }

   public static ItemStack d(ItemStack var0, ItemStack var1) {
      if(var1 != null) {
         class_afd var2 = class_aff.c(var1);
         Item var3 = var1.b();
         int var4 = 0;

         int var5;
         class_afe.class_b_in_class_afe var6;
         for(var5 = b.size(); var4 < var5; ++var4) {
            var6 = (class_afe.class_b_in_class_afe)b.get(var4);
            if(var6.a == var3 && var6.b.apply(var0)) {
               return class_aff.a(new ItemStack((Item)var6.c), var2);
            }
         }

         var4 = 0;

         for(var5 = a.size(); var4 < var5; ++var4) {
            var6 = (class_afe.class_b_in_class_afe)a.get(var4);
            if(var6.a == var2 && var6.b.apply(var0)) {
               return class_aff.a(new ItemStack(var3), (class_afd)var6.c);
            }
         }
      }

      return var1;
   }

   public static void a() {
      class_afe.class_a_in_class_afe var0 = new class_afe.class_a_in_class_afe(Items.bF);
      class_afe.class_a_in_class_afe var1 = new class_afe.class_a_in_class_afe(Items.cg);
      class_afe.class_a_in_class_afe var2 = new class_afe.class_a_in_class_afe(Items.aE);
      class_afe.class_a_in_class_afe var3 = new class_afe.class_a_in_class_afe(Items.bM);
      class_afe.class_a_in_class_afe var4 = new class_afe.class_a_in_class_afe(Items.by);
      class_afe.class_a_in_class_afe var5 = new class_afe.class_a_in_class_afe(Items.ba);
      class_afe.class_a_in_class_afe var6 = new class_afe.class_a_in_class_afe(Items.bO);
      class_afe.class_a_in_class_afe var7 = new class_afe.class_a_in_class_afe(Items.bf);
      class_afe.class_a_in_class_afe var8 = new class_afe.class_a_in_class_afe(Items.bb, ItemFish.EnumFish.PUFFERFISH.a());
      class_afe.class_a_in_class_afe var9 = new class_afe.class_a_in_class_afe(Items.bS);
      class_afe.class_a_in_class_afe var10 = new class_afe.class_a_in_class_afe(Items.bL);
      class_afe.class_a_in_class_afe var11 = new class_afe.class_a_in_class_afe(Items.bD);
      class_afe.class_a_in_class_afe var12 = new class_afe.class_a_in_class_afe(Items.bN);
      a(new class_afe.class_a_in_class_afe(Items.bG));
      a(new class_afe.class_a_in_class_afe(Items.bH));
      a(new class_afe.class_a_in_class_afe(Items.bI));
      a(Items.bG, new class_afe.class_a_in_class_afe(Items.J), Items.bH);
      a(Items.bH, new class_afe.class_a_in_class_afe(Items.bK), Items.bI);
      a((class_afd)class_afg.b, (Predicate)var9, (class_afd)class_afg.c);
      a((class_afd)class_afg.b, (Predicate)var11, (class_afd)class_afg.c);
      a((class_afd)class_afg.b, (Predicate)var4, (class_afd)class_afg.c);
      a((class_afd)class_afg.b, (Predicate)var12, (class_afd)class_afg.c);
      a((class_afd)class_afg.b, (Predicate)var10, (class_afd)class_afg.c);
      a((class_afd)class_afg.b, (Predicate)var7, (class_afd)class_afg.c);
      a((class_afd)class_afg.b, (Predicate)var6, (class_afd)class_afg.c);
      a((class_afd)class_afg.b, (Predicate)var5, (class_afd)class_afg.d);
      a((class_afd)class_afg.b, (Predicate)var2, (class_afd)class_afg.c);
      a((class_afd)class_afg.b, (Predicate)var0, (class_afd)class_afg.e);
      a((class_afd)class_afg.e, (Predicate)var1, (class_afd)class_afg.f);
      a((class_afd)class_afg.f, (Predicate)var2, (class_afd)class_afg.g);
      a((class_afd)class_afg.f, (Predicate)var3, (class_afd)class_afg.h);
      a((class_afd)class_afg.g, (Predicate)var3, (class_afd)class_afg.i);
      a((class_afd)class_afg.h, (Predicate)var2, (class_afd)class_afg.i);
      a((class_afd)class_afg.e, (Predicate)var6, (class_afd)class_afg.m);
      a((class_afd)class_afg.m, (Predicate)var2, (class_afd)class_afg.n);
      a((class_afd)class_afg.e, (Predicate)var4, (class_afd)class_afg.j);
      a((class_afd)class_afg.j, (Predicate)var2, (class_afd)class_afg.k);
      a((class_afd)class_afg.j, (Predicate)var5, (class_afd)class_afg.l);
      a((class_afd)class_afg.j, (Predicate)var3, (class_afd)class_afg.r);
      a((class_afd)class_afg.k, (Predicate)var3, (class_afd)class_afg.s);
      a((class_afd)class_afg.r, (Predicate)var2, (class_afd)class_afg.s);
      a((class_afd)class_afg.o, (Predicate)var3, (class_afd)class_afg.r);
      a((class_afd)class_afg.p, (Predicate)var3, (class_afd)class_afg.s);
      a((class_afd)class_afg.e, (Predicate)var7, (class_afd)class_afg.o);
      a((class_afd)class_afg.o, (Predicate)var2, (class_afd)class_afg.p);
      a((class_afd)class_afg.o, (Predicate)var5, (class_afd)class_afg.q);
      a((class_afd)class_afg.e, (Predicate)var8, (class_afd)class_afg.t);
      a((class_afd)class_afg.t, (Predicate)var2, (class_afd)class_afg.u);
      a((class_afd)class_afg.e, (Predicate)var9, (class_afd)class_afg.v);
      a((class_afd)class_afg.v, (Predicate)var5, (class_afd)class_afg.w);
      a((class_afd)class_afg.v, (Predicate)var3, (class_afd)class_afg.x);
      a((class_afd)class_afg.w, (Predicate)var3, (class_afd)class_afg.y);
      a((class_afd)class_afg.x, (Predicate)var5, (class_afd)class_afg.y);
      a((class_afd)class_afg.z, (Predicate)var3, (class_afd)class_afg.x);
      a((class_afd)class_afg.A, (Predicate)var3, (class_afd)class_afg.x);
      a((class_afd)class_afg.B, (Predicate)var3, (class_afd)class_afg.y);
      a((class_afd)class_afg.e, (Predicate)var10, (class_afd)class_afg.z);
      a((class_afd)class_afg.z, (Predicate)var2, (class_afd)class_afg.A);
      a((class_afd)class_afg.z, (Predicate)var5, (class_afd)class_afg.B);
      a((class_afd)class_afg.e, (Predicate)var11, (class_afd)class_afg.C);
      a((class_afd)class_afg.C, (Predicate)var2, (class_afd)class_afg.D);
      a((class_afd)class_afg.C, (Predicate)var5, (class_afd)class_afg.E);
      a((class_afd)class_afg.e, (Predicate)var12, (class_afd)class_afg.F);
      a((class_afd)class_afg.F, (Predicate)var2, (class_afd)class_afg.G);
      a((class_afd)class_afg.F, (Predicate)var5, (class_afd)class_afg.H);
      a((class_afd)class_afg.b, (Predicate)var3, (class_afd)class_afg.I);
      a((class_afd)class_afg.I, (Predicate)var2, (class_afd)class_afg.J);
   }

   private static void a(class_aec var0, class_afe.class_a_in_class_afe var1, class_aec var2) {
      b.add(new class_afe.class_b_in_class_afe(var0, var1, var2));
   }

   private static void a(class_afe.class_a_in_class_afe var0) {
      c.add(var0);
   }

   private static void a(class_afd var0, Predicate var1, class_afd var2) {
      a.add(new class_afe.class_b_in_class_afe(var0, var1, var2));
   }

   static class class_a_in_class_afe implements Predicate {
      private final Item a;
      private final int b;

      public class_a_in_class_afe(Item var1) {
         this(var1, -1);
      }

      public class_a_in_class_afe(Item var1, int var2) {
         this.a = var1;
         this.b = var2;
      }

      public boolean a(ItemStack var1) {
         return var1 != null && var1.b() == this.a && (this.b == -1 || this.b == var1.i());
      }

      // $FF: synthetic method
      public boolean apply(Object var1) {
         return this.a((ItemStack)var1);
      }
   }

   static class class_b_in_class_afe {
      final Object a;
      final Predicate b;
      final Object c;

      public class_b_in_class_afe(Object var1, Predicate var2, Object var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }
   }
}
