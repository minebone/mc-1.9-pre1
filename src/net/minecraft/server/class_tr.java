package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.EntityCreature;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.World;
import net.minecraft.server.class_ayo;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_tj;

public class class_tr extends class_tj {
   World a;
   protected EntityCreature b;
   int c;
   double d;
   boolean e;
   class_ayo f;
   private int h;
   private double i;
   private double j;
   private double k;
   protected final int g = 20;

   public class_tr(EntityCreature var1, double var2, boolean var4) {
      this.b = var1;
      this.a = var1.world;
      this.d = var2;
      this.e = var4;
      this.a(3);
   }

   public boolean a() {
      class_rz var1 = this.b.A();
      if(var1 == null) {
         return false;
      } else if(!var1.at()) {
         return false;
      } else {
         this.f = this.b.x().a((Entity)var1);
         return this.f != null;
      }
   }

   public boolean b() {
      class_rz var1 = this.b.A();
      return var1 == null?false:(!var1.at()?false:(!this.e?!this.b.x().n():(!this.b.f(new BlockPosition(var1))?false:!(var1 instanceof EntityHuman) || !((EntityHuman)var1).y() && !((EntityHuman)var1).l_())));
   }

   public void c() {
      this.b.x().a(this.f, this.d);
      this.h = 0;
   }

   public void d() {
      class_rz var1 = this.b.A();
      if(var1 instanceof EntityHuman && (((EntityHuman)var1).y() || ((EntityHuman)var1).l_())) {
         this.b.c((class_rz)null);
      }

      this.b.x().o();
   }

   public void e() {
      class_rz var1 = this.b.A();
      this.b.t().a(var1, 30.0F, 30.0F);
      double var2 = this.b.e(var1.locX, var1.bk().b, var1.locZ);
      double var4 = this.a(var1);
      --this.h;
      if((this.e || this.b.y().a(var1)) && this.h <= 0 && (this.i == 0.0D && this.j == 0.0D && this.k == 0.0D || var1.e(this.i, this.j, this.k) >= 1.0D || this.b.bE().nextFloat() < 0.05F)) {
         this.i = var1.locX;
         this.j = var1.bk().b;
         this.k = var1.locZ;
         this.h = 4 + this.b.bE().nextInt(7);
         if(var2 > 1024.0D) {
            this.h += 10;
         } else if(var2 > 256.0D) {
            this.h += 5;
         }

         if(!this.b.x().a((Entity)var1, this.d)) {
            this.h += 15;
         }
      }

      this.c = Math.max(this.c - 1, 0);
      if(var2 <= var4 && this.c <= 0) {
         this.c = 20;
         this.b.a(EnumHand.MAIN_HAND);
         this.b.B(var1);
      }

   }

   protected double a(class_rz var1) {
      return (double)(this.b.width * 2.0F * this.b.width * 2.0F + var1.width);
   }
}
