package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.StructureGenerator;
import net.minecraft.server.StructurePiece;
import net.minecraft.server.StructureStart;
import net.minecraft.server.World;
import net.minecraft.server.WorldGenStrongholdPieces;
import net.minecraft.server.class_ahm;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;

public class WorldGenStronghold extends StructureGenerator {
   private List a;
   private boolean b;
   private class_ahm[] d;
   private double h;
   private int i;

   public WorldGenStronghold() {
      this.d = new class_ahm[128];
      this.h = 32.0D;
      this.i = 3;
      this.a = Lists.newArrayList();
      Iterator var1 = BiomeBase.q.iterator();

      while(var1.hasNext()) {
         BiomeBase var2 = (BiomeBase)var1.next();
         if(var2 != null && var2.j() > 0.0F) {
            this.a.add(var2);
         }
      }

   }

   public WorldGenStronghold(Map var1) {
      this();
      Iterator var2 = var1.entrySet().iterator();

      while(var2.hasNext()) {
         Entry var3 = (Entry)var2.next();
         if(((String)var3.getKey()).equals("distance")) {
            this.h = MathHelper.a((String)var3.getValue(), this.h, 1.0D);
         } else if(((String)var3.getKey()).equals("count")) {
            this.d = new class_ahm[MathHelper.a((String)((String)var3.getValue()), this.d.length, 1)];
         } else if(((String)var3.getKey()).equals("spread")) {
            this.i = MathHelper.a((String)((String)var3.getValue()), this.i, 1);
         }
      }

   }

   public String a() {
      return "Stronghold";
   }

   protected boolean a(int var1, int var2) {
      if(!this.b) {
         Random var3 = new Random();
         var3.setSeed(this.g.O());
         double var4 = var3.nextDouble() * 3.141592653589793D * 2.0D;
         int var6 = 0;
         int var7 = 0;

         for(int var8 = 0; var8 < this.d.length; ++var8) {
            double var9 = 4.0D * this.h + this.h * (double)var6 * 6.0D + (var3.nextDouble() - 0.5D) * this.h * 2.5D;
            int var11 = (int)Math.round(Math.cos(var4) * var9);
            int var12 = (int)Math.round(Math.sin(var4) * var9);
            BlockPosition var13 = this.g.A().a((var11 << 4) + 8, (var12 << 4) + 8, 112, this.a, var3);
            if(var13 != null) {
               var11 = var13.p() >> 4;
               var12 = var13.r() >> 4;
            }

            this.d[var8] = new class_ahm(var11, var12);
            var4 += 6.283185307179586D / (double)this.i;
            ++var7;
            if(var7 == this.i) {
               ++var6;
               var7 = 0;
               this.i += 2 * this.i / (var6 + 1);
               this.i = Math.min(this.i, this.d.length - var8);
               var4 += var3.nextDouble() * 3.141592653589793D * 2.0D;
            }
         }

         this.b = true;
      }

      class_ahm[] var14 = this.d;
      int var15 = var14.length;

      for(int var5 = 0; var5 < var15; ++var5) {
         class_ahm var16 = var14[var5];
         if(var1 == var16.a && var2 == var16.b) {
            return true;
         }
      }

      return false;
   }

   protected List E_() {
      ArrayList var1 = Lists.newArrayList();
      class_ahm[] var2 = this.d;
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         class_ahm var5 = var2[var4];
         if(var5 != null) {
            var1.add(var5.a(64));
         }
      }

      return var1;
   }

   protected StructureStart b(int var1, int var2) {
      WorldGenStronghold.class_a_in_class_awa var3;
      for(var3 = new WorldGenStronghold.class_a_in_class_awa(this.g, this.f, var1, var2); var3.c().isEmpty() || ((WorldGenStrongholdPieces.class_m_in_class_awb)var3.c().get(0)).b == null; var3 = new WorldGenStronghold.class_a_in_class_awa(this.g, this.f, var1, var2)) {
         ;
      }

      return var3;
   }

   public static class class_a_in_class_awa extends StructureStart {
      public class_a_in_class_awa() {
      }

      public class_a_in_class_awa(World var1, Random var2, int var3, int var4) {
         super(var3, var4);
         WorldGenStrongholdPieces.b();
         WorldGenStrongholdPieces.class_m_in_class_awb var5 = new WorldGenStrongholdPieces.class_m_in_class_awb(0, var2, (var3 << 4) + 2, (var4 << 4) + 2);
         this.a.add(var5);
         var5.a(var5, this.a, var2);
         List var6 = var5.c;

         while(!var6.isEmpty()) {
            int var7 = var2.nextInt(var6.size());
            StructurePiece var8 = (StructurePiece)var6.remove(var7);
            var8.a((StructurePiece)var5, (List)this.a, (Random)var2);
         }

         this.d();
         this.a(var1, var2, 10);
      }
   }
}
