package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.Enchantment;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cb;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.class_rz;

public class CommandEnchant extends CommandAbstract {
   public String c() {
      return "enchant";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.enchant.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 2) {
         throw new class_cf("commands.enchant.usage", new Object[0]);
      } else {
         class_rz var4 = (class_rz)a(var1, var2, var3[0], class_rz.class);
         var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_ITEMS, 0);

         Enchantment var5;
         try {
            var5 = Enchantment.c(a(var3[1], 0));
         } catch (class_cb var12) {
            var5 = Enchantment.b(var3[1]);
         }

         if(var5 == null) {
            throw new class_cb("commands.enchant.notFound", new Object[]{Integer.valueOf(Enchantment.b(var5))});
         } else {
            int var6 = 1;
            ItemStack var7 = var4.ca();
            if(var7 == null) {
               throw new class_bz("commands.enchant.noItem", new Object[0]);
            } else if(!var5.a(var7)) {
               throw new class_bz("commands.enchant.cantEnchant", new Object[0]);
            } else {
               if(var3.length >= 3) {
                  var6 = a(var3[2], var5.d(), var5.b());
               }

               if(var7.n()) {
                  NBTTagList var8 = var7.p();
                  if(var8 != null) {
                     for(int var9 = 0; var9 < var8.c(); ++var9) {
                        short var10 = var8.b(var9).g("id");
                        if(Enchantment.c(var10) != null) {
                           Enchantment var11 = Enchantment.c(var10);
                           if(!var5.a(var11)) {
                              throw new class_bz("commands.enchant.cantCombine", new Object[]{var5.d(var6), var11.d(var8.b(var9).g("lvl"))});
                           }
                        }
                     }
                  }
               }

               var7.a(var5, var6);
               a(var2, this, "commands.enchant.success", new Object[0]);
               var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_ITEMS, 1);
            }
         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, var1.J()):(var3.length == 2?a(var3, Enchantment.b.c()):Collections.emptyList());
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 0;
   }
}
