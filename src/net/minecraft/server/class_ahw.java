package net.minecraft.server;

import net.minecraft.server.EnumDirection;
import net.minecraft.server.TileEntity;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;

public interface class_ahw {
   TileEntity r(BlockPosition var1);

   IBlockData getType(BlockPosition var1);

   boolean d(BlockPosition var1);

   int a(BlockPosition var1, EnumDirection var2);
}
