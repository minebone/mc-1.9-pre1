package net.minecraft.server;

import com.google.common.collect.Multimap;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_ys;

public class class_aew extends Item {
   private final float a;
   private final Item.class_a_in_class_adn b;

   public class_aew(Item.class_a_in_class_adn var1) {
      this.b = var1;
      this.j = 1;
      this.e(var1.a());
      this.a((CreativeModeTab)CreativeModeTab.j);
      this.a = 3.0F + var1.c();
   }

   public float g() {
      return this.b.c();
   }

   public float a(ItemStack var1, IBlockData var2) {
      Block var3 = var2.getBlock();
      if(var3 == Blocks.G) {
         return 15.0F;
      } else {
         Material var4 = var2.getMaterial();
         return var4 != Material.k && var4 != Material.l && var4 != Material.v && var4 != Material.j && var4 != Material.C?1.0F:1.5F;
      }
   }

   public boolean a(ItemStack var1, class_rz var2, class_rz var3) {
      var1.a(1, (class_rz)var3);
      return true;
   }

   public boolean a(ItemStack var1, World var2, IBlockData var3, BlockPosition var4, class_rz var5) {
      if((double)var3.b(var2, var4) != 0.0D) {
         var1.a(2, (class_rz)var5);
      }

      return true;
   }

   public boolean a(IBlockData var1) {
      return var1.getBlock() == Blocks.G;
   }

   public int c() {
      return this.b.e();
   }

   public String h() {
      return this.b.toString();
   }

   public boolean a(ItemStack var1, ItemStack var2) {
      return this.b.f() == var2.b()?true:super.a(var1, var2);
   }

   public Multimap a(EnumInventorySlot var1) {
      Multimap var2 = super.a(var1);
      if(var1 == EnumInventorySlot.MAINHAND) {
         var2.put(class_ys.e.a(), new AttributeModifier(g, "Weapon modifier", (double)this.a, 0));
         var2.put(class_ys.f.a(), new AttributeModifier(h, "Weapon modifier", -2.4000000953674316D, 0));
      }

      return var2;
   }
}
