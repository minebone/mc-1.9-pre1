package net.minecraft.server;

import net.minecraft.server.BlockDaylightDetector;
import net.minecraft.server.TileEntity;
import net.minecraft.server.class_ky;

public class class_apz extends TileEntity implements class_ky {
   public void c() {
      if(this.b != null && !this.b.E && this.b.P() % 20L == 0L) {
         this.e = this.w();
         if(this.e instanceof BlockDaylightDetector) {
            ((BlockDaylightDetector)this.e).c(this.b, this.c);
         }
      }

   }
}
