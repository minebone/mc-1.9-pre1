package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockFlowers;
import net.minecraft.server.BlockLongGrass;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.TileEntityFlowerPot;
import net.minecraft.server.World;
import net.minecraft.server.class_acb;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_aoa;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;
import net.minecraft.server.class_or;
import net.minecraft.server.EnumHand;

public class BlockFlowerPot extends class_ajm {
   public static final BlockStateInteger a = BlockStateInteger.a("legacy_data", 0, 15);
   public static final BlockStateEnum b = BlockStateEnum.a("contents", BlockFlowerPot.EnumFlowerPotContents.class);
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.3125D, 0.0D, 0.3125D, 0.6875D, 0.375D, 0.6875D);

   public BlockFlowerPot() {
      super(Material.q);
      this.w(this.A.b().set(b, BlockFlowerPot.EnumFlowerPotContents.EMPTY).set(a, Integer.valueOf(0)));
   }

   public String c() {
      return class_di.a("item.flowerPot.name");
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return c;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.MODEL;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(var6 != null && var6.b() instanceof class_acb) {
         TileEntityFlowerPot var11 = this.c(var1, var2);
         if(var11 == null) {
            return false;
         } else if(var11.c() != null) {
            return false;
         } else {
            Block var12 = Block.a(var6.b());
            if(!this.a(var12, var6.i())) {
               return false;
            } else {
               var11.a(var6.b(), var6.i());
               var11.v_();
               var1.a(var2, var3, var3, 3);
               var4.b(StatisticList.V);
               if(!var4.abilities.d) {
                  --var6.b;
               }

               return true;
            }
         }
      } else {
         return false;
      }
   }

   private boolean a(Block var1, int var2) {
      return var1 != Blocks.N && var1 != Blocks.O && var1 != Blocks.aK && var1 != Blocks.P && var1 != Blocks.Q && var1 != Blocks.g && var1 != Blocks.I?var1 == Blocks.H && var2 == BlockLongGrass.EnumTallGrassType.FERN.a():true;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      TileEntityFlowerPot var4 = this.c(var1, var2);
      if(var4 != null) {
         ItemStack var5 = var4.b();
         if(var5 != null) {
            return var5;
         }
      }

      return new ItemStack(Items.ca);
   }

   public boolean a(World var1, BlockPosition var2) {
      return super.a(var1, var2) && var1.getType(var2.b()).q();
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!var1.getType(var2.b()).q()) {
         this.b(var1, var2, var3, 0);
         var1.g(var2);
      }

   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      TileEntityFlowerPot var4 = this.c(var1, var2);
      if(var4 != null && var4.c() != null) {
         a((World)var1, (BlockPosition)var2, (ItemStack)(new ItemStack(var4.c(), 1, var4.d())));
      }

      super.b(var1, var2, var3);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4) {
      super.a(var1, var2, var3, var4);
      if(var4.abilities.d) {
         TileEntityFlowerPot var5 = this.c(var1, var2);
         if(var5 != null) {
            var5.a((Item)null, 0);
         }
      }

   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Items.ca;
   }

   private TileEntityFlowerPot c(World var1, BlockPosition var2) {
      TileEntity var3 = var1.r(var2);
      return var3 instanceof TileEntityFlowerPot?(TileEntityFlowerPot)var3:null;
   }

   public TileEntity a(World var1, int var2) {
      Object var3 = null;
      int var4 = 0;
      switch(var2) {
      case 1:
         var3 = Blocks.O;
         var4 = BlockFlowers.EnumFlowerVarient.POPPY.b();
         break;
      case 2:
         var3 = Blocks.N;
         break;
      case 3:
         var3 = Blocks.g;
         var4 = BlockWood.EnumLogVariant.OAK.a();
         break;
      case 4:
         var3 = Blocks.g;
         var4 = BlockWood.EnumLogVariant.SPRUCE.a();
         break;
      case 5:
         var3 = Blocks.g;
         var4 = BlockWood.EnumLogVariant.BIRCH.a();
         break;
      case 6:
         var3 = Blocks.g;
         var4 = BlockWood.EnumLogVariant.JUNGLE.a();
         break;
      case 7:
         var3 = Blocks.Q;
         break;
      case 8:
         var3 = Blocks.P;
         break;
      case 9:
         var3 = Blocks.aK;
         break;
      case 10:
         var3 = Blocks.I;
         break;
      case 11:
         var3 = Blocks.H;
         var4 = BlockLongGrass.EnumTallGrassType.FERN.a();
         break;
      case 12:
         var3 = Blocks.g;
         var4 = BlockWood.EnumLogVariant.ACACIA.a();
         break;
      case 13:
         var3 = Blocks.g;
         var4 = BlockWood.EnumLogVariant.DARK_OAK.a();
      }

      return new TileEntityFlowerPot(Item.a((Block)var3), var4);
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{b, a});
   }

   public int e(IBlockData var1) {
      return ((Integer)var1.get(a)).intValue();
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      BlockFlowerPot.EnumFlowerPotContents var4 = BlockFlowerPot.EnumFlowerPotContents.EMPTY;
      TileEntity var5 = var2.r(var3);
      if(var5 instanceof TileEntityFlowerPot) {
         TileEntityFlowerPot var6 = (TileEntityFlowerPot)var5;
         Item var7 = var6.c();
         if(var7 instanceof class_acb) {
            int var8 = var6.d();
            Block var9 = Block.a(var7);
            if(var9 == Blocks.g) {
               switch(BlockFlowerPot.SyntheticClass_1.a[BlockWood.EnumLogVariant.a(var8).ordinal()]) {
               case 1:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.OAK_SAPLING;
                  break;
               case 2:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.SPRUCE_SAPLING;
                  break;
               case 3:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.BIRCH_SAPLING;
                  break;
               case 4:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.JUNGLE_SAPLING;
                  break;
               case 5:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.ACACIA_SAPLING;
                  break;
               case 6:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.DARK_OAK_SAPLING;
                  break;
               default:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.EMPTY;
               }
            } else if(var9 == Blocks.H) {
               switch(var8) {
               case 0:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.DEAD_BUSH;
                  break;
               case 2:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.FERN;
                  break;
               default:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.EMPTY;
               }
            } else if(var9 == Blocks.N) {
               var4 = BlockFlowerPot.EnumFlowerPotContents.DANDELION;
            } else if(var9 == Blocks.O) {
               switch(BlockFlowerPot.SyntheticClass_1.b[BlockFlowers.EnumFlowerVarient.a(BlockFlowers.class_b_in_class_all.RED, var8).ordinal()]) {
               case 1:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.POPPY;
                  break;
               case 2:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.BLUE_ORCHID;
                  break;
               case 3:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.ALLIUM;
                  break;
               case 4:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.HOUSTONIA;
                  break;
               case 5:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.RED_TULIP;
                  break;
               case 6:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.ORANGE_TULIP;
                  break;
               case 7:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.WHITE_TULIP;
                  break;
               case 8:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.PINK_TULIP;
                  break;
               case 9:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.OXEYE_DAISY;
                  break;
               default:
                  var4 = BlockFlowerPot.EnumFlowerPotContents.EMPTY;
               }
            } else if(var9 == Blocks.Q) {
               var4 = BlockFlowerPot.EnumFlowerPotContents.MUSHROOM_RED;
            } else if(var9 == Blocks.P) {
               var4 = BlockFlowerPot.EnumFlowerPotContents.MUSHROOM_BROWN;
            } else if(var9 == Blocks.I) {
               var4 = BlockFlowerPot.EnumFlowerPotContents.DEAD_BUSH;
            } else if(var9 == Blocks.aK) {
               var4 = BlockFlowerPot.EnumFlowerPotContents.CACTUS;
            }
         }
      }

      return var1.set(b, var4);
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b = new int[BlockFlowers.EnumFlowerVarient.values().length];

      static {
         try {
            b[BlockFlowers.EnumFlowerVarient.POPPY.ordinal()] = 1;
         } catch (NoSuchFieldError var15) {
            ;
         }

         try {
            b[BlockFlowers.EnumFlowerVarient.BLUE_ORCHID.ordinal()] = 2;
         } catch (NoSuchFieldError var14) {
            ;
         }

         try {
            b[BlockFlowers.EnumFlowerVarient.ALLIUM.ordinal()] = 3;
         } catch (NoSuchFieldError var13) {
            ;
         }

         try {
            b[BlockFlowers.EnumFlowerVarient.HOUSTONIA.ordinal()] = 4;
         } catch (NoSuchFieldError var12) {
            ;
         }

         try {
            b[BlockFlowers.EnumFlowerVarient.RED_TULIP.ordinal()] = 5;
         } catch (NoSuchFieldError var11) {
            ;
         }

         try {
            b[BlockFlowers.EnumFlowerVarient.ORANGE_TULIP.ordinal()] = 6;
         } catch (NoSuchFieldError var10) {
            ;
         }

         try {
            b[BlockFlowers.EnumFlowerVarient.WHITE_TULIP.ordinal()] = 7;
         } catch (NoSuchFieldError var9) {
            ;
         }

         try {
            b[BlockFlowers.EnumFlowerVarient.PINK_TULIP.ordinal()] = 8;
         } catch (NoSuchFieldError var8) {
            ;
         }

         try {
            b[BlockFlowers.EnumFlowerVarient.OXEYE_DAISY.ordinal()] = 9;
         } catch (NoSuchFieldError var7) {
            ;
         }

         a = new int[BlockWood.EnumLogVariant.values().length];

         try {
            a[BlockWood.EnumLogVariant.OAK.ordinal()] = 1;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[BlockWood.EnumLogVariant.SPRUCE.ordinal()] = 2;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[BlockWood.EnumLogVariant.BIRCH.ordinal()] = 3;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[BlockWood.EnumLogVariant.JUNGLE.ordinal()] = 4;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[BlockWood.EnumLogVariant.ACACIA.ordinal()] = 5;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[BlockWood.EnumLogVariant.DARK_OAK.ordinal()] = 6;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum EnumFlowerPotContents implements class_or {
      EMPTY("empty"),
      POPPY("rose"),
      BLUE_ORCHID("blue_orchid"),
      ALLIUM("allium"),
      HOUSTONIA("houstonia"),
      RED_TULIP("red_tulip"),
      ORANGE_TULIP("orange_tulip"),
      WHITE_TULIP("white_tulip"),
      PINK_TULIP("pink_tulip"),
      OXEYE_DAISY("oxeye_daisy"),
      DANDELION("dandelion"),
      OAK_SAPLING("oak_sapling"),
      SPRUCE_SAPLING("spruce_sapling"),
      BIRCH_SAPLING("birch_sapling"),
      JUNGLE_SAPLING("jungle_sapling"),
      ACACIA_SAPLING("acacia_sapling"),
      DARK_OAK_SAPLING("dark_oak_sapling"),
      MUSHROOM_RED("mushroom_red"),
      MUSHROOM_BROWN("mushroom_brown"),
      DEAD_BUSH("dead_bush"),
      FERN("fern"),
      CACTUS("cactus");

      private final String w;

      private EnumFlowerPotContents(String var3) {
         this.w = var3;
      }

      public String toString() {
         return this.w;
      }

      public String m() {
         return this.w;
      }
   }
}
