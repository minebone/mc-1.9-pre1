package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;
import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ape;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;

public class class_any extends class_ape {
   private static Map g = Maps.newHashMap();
   private final boolean B;

   private boolean a(World var1, BlockPosition var2, boolean var3) {
      if(!g.containsKey(var1)) {
         g.put(var1, Lists.newArrayList());
      }

      List var4 = (List)g.get(var1);
      if(var3) {
         var4.add(new class_any.class_a_in_class_any(var2, var1.P()));
      }

      int var5 = 0;

      for(int var6 = 0; var6 < var4.size(); ++var6) {
         class_any.class_a_in_class_any var7 = (class_any.class_a_in_class_any)var4.get(var6);
         if(var7.a.equals(var2)) {
            ++var5;
            if(var5 >= 8) {
               return true;
            }
         }
      }

      return false;
   }

   protected class_any(boolean var1) {
      this.B = var1;
      this.a(true);
      this.a((CreativeModeTab)null);
   }

   public int a(World var1) {
      return 2;
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      if(this.B) {
         EnumDirection[] var4 = EnumDirection.values();
         int var5 = var4.length;

         for(int var6 = 0; var6 < var5; ++var6) {
            EnumDirection var7 = var4[var6];
            var1.d(var2.a(var7), this);
         }
      }

   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      if(this.B) {
         EnumDirection[] var4 = EnumDirection.values();
         int var5 = var4.length;

         for(int var6 = 0; var6 < var5; ++var6) {
            EnumDirection var7 = var4[var6];
            var1.d(var2.a(var7), this);
         }
      }

   }

   public int b(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return this.B && var1.get(a) != var4?15:0;
   }

   private boolean g(World var1, BlockPosition var2, IBlockData var3) {
      EnumDirection var4 = ((EnumDirection)var3.get(a)).d();
      return var1.b(var2.a(var4), var4);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Random var4) {
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      boolean var5 = this.g(var1, var2, var3);
      List var6 = (List)g.get(var1);

      while(var6 != null && !var6.isEmpty() && var1.P() - ((class_any.class_a_in_class_any)var6.get(0)).b > 60L) {
         var6.remove(0);
      }

      if(this.B) {
         if(var5) {
            var1.a((BlockPosition)var2, (IBlockData)Blocks.aE.u().set(a, var3.get(a)), 3);
            if(this.a(var1, var2, true)) {
               var1.a((EntityHuman)null, var2, class_ng.eA, EnumSoundCategory.BLOCKS, 0.5F, 2.6F + (var1.r.nextFloat() - var1.r.nextFloat()) * 0.8F);

               for(int var7 = 0; var7 < 5; ++var7) {
                  double var8 = (double)var2.p() + var4.nextDouble() * 0.6D + 0.2D;
                  double var10 = (double)var2.q() + var4.nextDouble() * 0.6D + 0.2D;
                  double var12 = (double)var2.r() + var4.nextDouble() * 0.6D + 0.2D;
                  var1.a(EnumParticle.SMOKE_NORMAL, var8, var10, var12, 0.0D, 0.0D, 0.0D, new int[0]);
               }

               var1.a((BlockPosition)var2, (Block)var1.getType(var2).getBlock(), 160);
            }
         }
      } else if(!var5 && !this.a(var1, var2, false)) {
         var1.a((BlockPosition)var2, (IBlockData)Blocks.aF.u().set(a, var3.get(a)), 3);
      }

   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!this.e(var1, var2, var3)) {
         if(this.B == this.g(var1, var2, var3)) {
            var1.a((BlockPosition)var2, (Block)this, this.a(var1));
         }

      }
   }

   public int c(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return var4 == EnumDirection.DOWN?var1.a(var2, var3, var4):0;
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Item.a(Blocks.aF);
   }

   public boolean g(IBlockData var1) {
      return true;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Blocks.aF);
   }

   public boolean b(Block var1) {
      return var1 == Blocks.aE || var1 == Blocks.aF;
   }

   static class class_a_in_class_any {
      BlockPosition a;
      long b;

      public class_a_in_class_any(BlockPosition var1, long var2) {
         this.a = var1;
         this.b = var2;
      }
   }
}
