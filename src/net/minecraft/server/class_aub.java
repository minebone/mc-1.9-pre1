package net.minecraft.server;

import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.class_ape;
import net.minecraft.server.class_auc;
import net.minecraft.server.BlockPosition;

public class class_aub extends class_auc {
   public static final BlockPosition a = BlockPosition.a;
   public static final BlockPosition b = new BlockPosition(a.p() - 4 & -16, 0, a.r() - 4 & -16);
   private final boolean c;

   public class_aub(boolean var1) {
      this.c = var1;
   }

   public boolean b(World var1, Random var2, BlockPosition var3) {
      Iterator var4 = BlockPosition.b(new BlockPosition(var3.p() - 4, var3.q() - 1, var3.r() - 4), new BlockPosition(var3.p() + 4, var3.q() + 32, var3.r() + 4)).iterator();

      while(var4.hasNext()) {
         BlockPosition.class_a_in_class_cj var5 = (BlockPosition.class_a_in_class_cj)var4.next();
         double var6 = var5.f(var3.p(), var5.q(), var3.r());
         if(var6 <= 3.5D) {
            if(var5.q() < var3.q()) {
               if(var6 <= 2.5D) {
                  this.a(var1, var5, Blocks.h.u());
               } else if(var5.q() < var3.q()) {
                  this.a(var1, var5, Blocks.bH.u());
               }
            } else if(var5.q() > var3.q()) {
               this.a(var1, var5, Blocks.AIR.u());
            } else if(var6 > 2.5D) {
               this.a(var1, var5, Blocks.h.u());
            } else if(this.c) {
               this.a(var1, new BlockPosition(var5), Blocks.bF.u());
            } else {
               this.a(var1, new BlockPosition(var5), Blocks.AIR.u());
            }
         }
      }

      for(int var8 = 0; var8 < 4; ++var8) {
         this.a(var1, var3.b(var8), Blocks.h.u());
      }

      BlockPosition var9 = var3.b(2);
      Iterator var10 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

      while(var10.hasNext()) {
         EnumDirection var11 = (EnumDirection)var10.next();
         this.a(var1, var9.a(var11), Blocks.aa.u().set(class_ape.a, var11));
      }

      return true;
   }
}
