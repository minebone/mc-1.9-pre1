package net.minecraft.server;

import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockFenceGate;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ads;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;

public class class_ali extends Block {
   public static final class_arm a = class_arm.a("north");
   public static final class_arm b = class_arm.a("east");
   public static final class_arm c = class_arm.a("south");
   public static final class_arm d = class_arm.a("west");
   protected static final AxisAlignedBB[] e = new AxisAlignedBB[]{new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 1.0D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 1.0D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.375D, 0.625D, 1.0D, 0.625D), new AxisAlignedBB(0.0D, 0.0D, 0.375D, 0.625D, 1.0D, 1.0D), new AxisAlignedBB(0.375D, 0.0D, 0.0D, 0.625D, 1.0D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.0D, 0.625D, 1.0D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.625D, 1.0D, 0.625D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.625D, 1.0D, 1.0D), new AxisAlignedBB(0.375D, 0.0D, 0.375D, 1.0D, 1.0D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.375D, 1.0D, 1.0D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.375D, 1.0D, 1.0D, 0.625D), new AxisAlignedBB(0.0D, 0.0D, 0.375D, 1.0D, 1.0D, 1.0D), new AxisAlignedBB(0.375D, 0.0D, 0.0D, 1.0D, 1.0D, 0.625D), new AxisAlignedBB(0.375D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.625D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D)};
   public static final AxisAlignedBB f = new AxisAlignedBB(0.375D, 0.0D, 0.375D, 0.625D, 1.5D, 0.625D);
   public static final AxisAlignedBB g = new AxisAlignedBB(0.375D, 0.0D, 0.625D, 0.625D, 1.5D, 1.0D);
   public static final AxisAlignedBB B = new AxisAlignedBB(0.0D, 0.0D, 0.375D, 0.375D, 1.5D, 0.625D);
   public static final AxisAlignedBB C = new AxisAlignedBB(0.375D, 0.0D, 0.0D, 0.625D, 1.5D, 0.375D);
   public static final AxisAlignedBB D = new AxisAlignedBB(0.625D, 0.0D, 0.375D, 1.0D, 1.5D, 0.625D);

   public class_ali(Material var1, MaterialMapColor var2) {
      super(var1, var2);
      this.w(this.A.b().set(a, Boolean.valueOf(false)).set(b, Boolean.valueOf(false)).set(c, Boolean.valueOf(false)).set(d, Boolean.valueOf(false)));
      this.a(CreativeModeTab.c);
   }

   public void a(IBlockData var1, World var2, BlockPosition var3, AxisAlignedBB var4, List var5, Entity var6) {
      var1.b(var2, var3);
      a(var3, var4, var5, f);
      if(((Boolean)var1.get(a)).booleanValue()) {
         a(var3, var4, var5, C);
      }

      if(((Boolean)var1.get(b)).booleanValue()) {
         a(var3, var4, var5, D);
      }

      if(((Boolean)var1.get(c)).booleanValue()) {
         a(var3, var4, var5, g);
      }

      if(((Boolean)var1.get(d)).booleanValue()) {
         a(var3, var4, var5, B);
      }

   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      var1 = this.b(var1, var2, var3);
      return e[i(var1)];
   }

   private static int i(IBlockData var0) {
      int var1 = 0;
      if(((Boolean)var0.get(a)).booleanValue()) {
         var1 |= 1 << EnumDirection.NORTH.b();
      }

      if(((Boolean)var0.get(b)).booleanValue()) {
         var1 |= 1 << EnumDirection.EAST.b();
      }

      if(((Boolean)var0.get(c)).booleanValue()) {
         var1 |= 1 << EnumDirection.SOUTH.b();
      }

      if(((Boolean)var0.get(d)).booleanValue()) {
         var1 |= 1 << EnumDirection.WEST.b();
      }

      return var1;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(class_ahw var1, BlockPosition var2) {
      return false;
   }

   public boolean c(class_ahw var1, BlockPosition var2) {
      IBlockData var3 = var1.getType(var2);
      Block var4 = var3.getBlock();
      return var4 == Blocks.cv?false:((!(var4 instanceof class_ali) || var4.x != this.x) && !(var4 instanceof BlockFenceGate)?(var4.x.k() && var3.h()?var4.x != Material.C:false):true);
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      return var1.E?true:class_ads.a(var4, var1, var2);
   }

   public int e(IBlockData var1) {
      return 0;
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return var1.set(a, Boolean.valueOf(this.c(var2, var3.c()))).set(b, Boolean.valueOf(this.c(var2, var3.f()))).set(c, Boolean.valueOf(this.c(var2, var3.d()))).set(d, Boolean.valueOf(this.c(var2, var3.e())));
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      switch(class_ali.SyntheticClass_1.a[var2.ordinal()]) {
      case 1:
         return var1.set(a, var1.get(c)).set(b, var1.get(d)).set(c, var1.get(a)).set(d, var1.get(b));
      case 2:
         return var1.set(a, var1.get(b)).set(b, var1.get(c)).set(c, var1.get(d)).set(d, var1.get(a));
      case 3:
         return var1.set(a, var1.get(d)).set(b, var1.get(a)).set(c, var1.get(b)).set(d, var1.get(c));
      default:
         return var1;
      }
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      switch(class_ali.SyntheticClass_1.b[var2.ordinal()]) {
      case 1:
         return var1.set(a, var1.get(c)).set(c, var1.get(a));
      case 2:
         return var1.set(b, var1.get(d)).set(d, var1.get(b));
      default:
         return super.a(var1, var2);
      }
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b, d, c});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b = new int[class_amq.values().length];

      static {
         try {
            b[class_amq.LEFT_RIGHT.ordinal()] = 1;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            b[class_amq.FRONT_BACK.ordinal()] = 2;
         } catch (NoSuchFieldError var4) {
            ;
         }

         a = new int[class_aod.values().length];

         try {
            a[class_aod.CLOCKWISE_180.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[class_aod.CLOCKWISE_90.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
