package net.minecraft.server;

import com.google.common.base.Charsets;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.io.Files;
import com.google.common.io.Resources;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import net.minecraft.server.class_azs;
import net.minecraft.server.class_azv;
import net.minecraft.server.class_azw;
import net.minecraft.server.class_azx;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_bab;
import net.minecraft.server.class_bae;
import net.minecraft.server.class_baf;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_bar;
import net.minecraft.server.class_kk;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_baa {
   private static final Logger a = LogManager.getLogger();
   private static final Gson b = (new GsonBuilder()).registerTypeAdapter(class_bab.class, new class_bab.class_a_in_class_bab()).registerTypeAdapter(class_azv.class, new class_azv.class_a_in_class_azv()).registerTypeAdapter(class_azx.class, new class_azx.class_a_in_class_azx()).registerTypeHierarchyAdapter(class_azw.class, new class_azw.class_a_in_class_azw()).registerTypeHierarchyAdapter(class_bae.class, new class_baf.class_a_in_class_baf()).registerTypeHierarchyAdapter(class_baq.class, new class_bar.class_a_in_class_bar()).registerTypeHierarchyAdapter(class_azy.class_b_in_class_azy.class, new class_azy.class_b_in_class_azy.class_b_in_class_azy$class_a_in_class_b_in_class_azy()).create();
   private final LoadingCache c = CacheBuilder.newBuilder().build(new class_baa.class_a_in_class_baa());
   private final File d;

   public class_baa(File var1) {
      this.d = var1;
      this.a();
   }

   public class_azx a(class_kk var1) {
      return (class_azx)this.c.getUnchecked(var1);
   }

   public void a() {
      this.c.invalidateAll();
      Iterator var1 = class_azs.a().iterator();

      while(var1.hasNext()) {
         class_kk var2 = (class_kk)var1.next();
         this.a(var2);
      }

   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
   }

   class class_a_in_class_baa extends CacheLoader {
      private class_a_in_class_baa() {
      }

      public class_azx a(class_kk var1) throws Exception {
         if(var1.a().contains(".")) {
            class_baa.a.debug("Invalid loot table name \'" + var1 + "\' (can\'t contain periods)");
            return class_azx.a;
         } else {
            class_azx var2 = this.b(var1);
            if(var2 == null) {
               var2 = this.c(var1);
            }

            if(var2 == null) {
               var2 = class_azx.a;
               class_baa.a.warn("Couldn\'t find resource table {}", new Object[]{var1});
            }

            return var2;
         }
      }

      private class_azx b(class_kk var1) {
         File var2 = new File(new File(class_baa.this.d, var1.b()), var1.a() + ".json");
         if(var2.exists()) {
            if(var2.isFile()) {
               String var3;
               try {
                  var3 = Files.toString(var2, Charsets.UTF_8);
               } catch (IOException var6) {
                  class_baa.a.warn((String)("Couldn\'t load loot table " + var1 + " from " + var2), (Throwable)var6);
                  return class_azx.a;
               }

               try {
                  return (class_azx)class_baa.b.fromJson(var3, class_azx.class);
               } catch (JsonParseException var5) {
                  class_baa.a.error((String)("Couldn\'t load loot table " + var1 + " from " + var2), (Throwable)var5);
                  return class_azx.a;
               }
            } else {
               class_baa.a.warn("Expected to find loot table " + var1 + " at " + var2 + " but it was a folder.");
               return class_azx.a;
            }
         } else {
            return null;
         }
      }

      private class_azx c(class_kk var1) {
         URL var2 = class_baa.class.getResource("/assets/" + var1.b() + "/loot_tables/" + var1.a() + ".json");
         if(var2 != null) {
            String var3;
            try {
               var3 = Resources.toString(var2, Charsets.UTF_8);
            } catch (IOException var6) {
               class_baa.a.warn((String)("Couldn\'t load loot table " + var1 + " from " + var2), (Throwable)var6);
               return class_azx.a;
            }

            try {
               return (class_azx)class_baa.b.fromJson(var3, class_azx.class);
            } catch (JsonParseException var5) {
               class_baa.a.error((String)("Couldn\'t load loot table " + var1 + " from " + var2), (Throwable)var5);
               return class_azx.a;
            }
         } else {
            return null;
         }
      }

      // $FF: synthetic method
      public Object load(Object var1) throws Exception {
         return this.a((class_kk)var1);
      }

      // $FF: synthetic method
      class_a_in_class_baa(class_baa.SyntheticClass_1 var2) {
         this();
      }
   }
}
