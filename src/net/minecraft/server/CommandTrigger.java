package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.Entity;
import net.minecraft.server.IScoreboardCriteria;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.Scoreboard;
import net.minecraft.server.ScoreboardScore;
import net.minecraft.server.class_bbk;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;

public class CommandTrigger extends CommandAbstract {
   public String c() {
      return "trigger";
   }

   public int a() {
      return 0;
   }

   public String b(ICommandListener var1) {
      return "commands.trigger.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 3) {
         throw new class_cf("commands.trigger.usage", new Object[0]);
      } else {
         EntityPlayer var4;
         if(var2 instanceof EntityPlayer) {
            var4 = (EntityPlayer)var2;
         } else {
            Entity var5 = var2.f();
            if(!(var5 instanceof EntityPlayer)) {
               throw new class_bz("commands.trigger.invalidPlayer", new Object[0]);
            }

            var4 = (EntityPlayer)var5;
         }

         Scoreboard var9 = var1.a(0).ad();
         class_bbk var6 = var9.b(var3[0]);
         if(var6 != null && var6.c() == IScoreboardCriteria.c) {
            int var7 = a(var3[2]);
            if(!var9.b(var4.h_(), var6)) {
               throw new class_bz("commands.trigger.invalidObjective", new Object[]{var3[0]});
            } else {
               ScoreboardScore var8 = var9.c(var4.h_(), var6);
               if(var8.g()) {
                  throw new class_bz("commands.trigger.disabled", new Object[]{var3[0]});
               } else {
                  if("set".equals(var3[1])) {
                     var8.c(var7);
                  } else {
                     if(!"add".equals(var3[1])) {
                        throw new class_bz("commands.trigger.invalidMode", new Object[]{var3[1]});
                     }

                     var8.a(var7);
                  }

                  var8.a(true);
                  if(var4.c.isCreative()) {
                     a(var2, this, "commands.trigger.success", new Object[]{var3[0], var3[1], var3[2]});
                  }

               }
            }
         } else {
            throw new class_bz("commands.trigger.invalidObjective", new Object[]{var3[0]});
         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      if(var3.length == 1) {
         Scoreboard var5 = var1.a(0).ad();
         ArrayList var6 = Lists.newArrayList();
         Iterator var7 = var5.c().iterator();

         while(var7.hasNext()) {
            class_bbk var8 = (class_bbk)var7.next();
            if(var8.c() == IScoreboardCriteria.c) {
               var6.add(var8.b());
            }
         }

         return a(var3, (String[])var6.toArray(new String[var6.size()]));
      } else {
         return var3.length == 2?a(var3, new String[]{"add", "set"}):Collections.emptyList();
      }
   }
}
