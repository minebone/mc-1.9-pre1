package net.minecraft.server;

import com.google.common.base.Objects;
import net.minecraft.server.MathHelper;

public class BaseBlockPosition implements Comparable {
   public static final BaseBlockPosition b = new BaseBlockPosition(0, 0, 0);
   private final int a;
   private final int c;
   private final int d;

   public BaseBlockPosition(int var1, int var2, int var3) {
      this.a = var1;
      this.c = var2;
      this.d = var3;
   }

   public BaseBlockPosition(double var1, double var3, double var5) {
      this(MathHelper.c(var1), MathHelper.c(var3), MathHelper.c(var5));
   }

   public boolean equals(Object var1) {
      if(this == var1) {
         return true;
      } else if(!(var1 instanceof BaseBlockPosition)) {
         return false;
      } else {
         BaseBlockPosition var2 = (BaseBlockPosition)var1;
         return this.p() != var2.p()?false:(this.q() != var2.q()?false:this.r() == var2.r());
      }
   }

   public int hashCode() {
      return (this.q() + this.r() * 31) * 31 + this.p();
   }

   public int i(BaseBlockPosition var1) {
      return this.q() == var1.q()?(this.r() == var1.r()?this.p() - var1.p():this.r() - var1.r()):this.q() - var1.q();
   }

   public int p() {
      return this.a;
   }

   public int q() {
      return this.c;
   }

   public int r() {
      return this.d;
   }

   public BaseBlockPosition d(BaseBlockPosition var1) {
      return new BaseBlockPosition(this.q() * var1.r() - this.r() * var1.q(), this.r() * var1.p() - this.p() * var1.r(), this.p() * var1.q() - this.q() * var1.p());
   }

   public double f(int var1, int var2, int var3) {
      double var4 = (double)(this.p() - var1);
      double var6 = (double)(this.q() - var2);
      double var8 = (double)(this.r() - var3);
      return Math.sqrt(var4 * var4 + var6 * var6 + var8 * var8);
   }

   public double e(double var1, double var3, double var5) {
      double var7 = (double)this.p() - var1;
      double var9 = (double)this.q() - var3;
      double var11 = (double)this.r() - var5;
      return var7 * var7 + var9 * var9 + var11 * var11;
   }

   public double f(double var1, double var3, double var5) {
      double var7 = (double)this.p() + 0.5D - var1;
      double var9 = (double)this.q() + 0.5D - var3;
      double var11 = (double)this.r() + 0.5D - var5;
      return var7 * var7 + var9 * var9 + var11 * var11;
   }

   public double k(BaseBlockPosition var1) {
      return this.e((double)var1.p(), (double)var1.q(), (double)var1.r());
   }

   public String toString() {
      return Objects.toStringHelper((Object)this).add("x", this.p()).add("y", this.q()).add("z", this.r()).toString();
   }

   // $FF: synthetic method
   public int compareTo(Object var1) {
      return this.i((BaseBlockPosition)var1);
   }
}
