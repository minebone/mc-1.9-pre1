package net.minecraft.server;

import com.mojang.authlib.GameProfile;
import java.util.UUID;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.GameProfileSerializer;
import net.minecraft.server.Item;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aoj;
import net.minecraft.server.class_aqn;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;

public class ItemSkull extends Item {
   private static final String[] a = new String[]{"skeleton", "wither", "zombie", "char", "creeper", "dragon"};

   public ItemSkull() {
      this.a((CreativeModeTab)CreativeModeTab.c);
      this.e(0);
      this.a(true);
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      if(var6 == EnumDirection.DOWN) {
         return EnumResult.FAIL;
      } else {
         IBlockData var10 = var3.getType(var4);
         Block var11 = var10.getBlock();
         boolean var12 = var11.a((class_ahw)var3, (BlockPosition)var4);
         if(!var12) {
            if(!var3.getType(var4).getMaterial().a()) {
               return EnumResult.FAIL;
            }

            var4 = var4.a(var6);
         }

         if(var2.a(var4, var6, var1) && Blocks.ce.a((World)var3, (BlockPosition)var4)) {
            if(var3.E) {
               return EnumResult.SUCCESS;
            } else {
               var3.a((BlockPosition)var4, (IBlockData)Blocks.ce.u().set(class_aoj.a, var6), 11);
               int var13 = 0;
               if(var6 == EnumDirection.UP) {
                  var13 = MathHelper.c((double)(var2.yaw * 16.0F / 360.0F) + 0.5D) & 15;
               }

               TileEntity var14 = var3.r(var4);
               if(var14 instanceof class_aqn) {
                  class_aqn var15 = (class_aqn)var14;
                  if(var1.i() == 3) {
                     GameProfile var16 = null;
                     if(var1.n()) {
                        NBTTagCompound var17 = var1.o();
                        if(var17.b("SkullOwner", 10)) {
                           var16 = GameProfileSerializer.a(var17.o("SkullOwner"));
                        } else if(var17.b("SkullOwner", 8) && !var17.l("SkullOwner").isEmpty()) {
                           var16 = new GameProfile((UUID)null, var17.l("SkullOwner"));
                        }
                     }

                     var15.a(var16);
                  } else {
                     var15.a(var1.i());
                  }

                  var15.b(var13);
                  Blocks.ce.a(var3, var4, var15);
               }

               --var1.b;
               return EnumResult.SUCCESS;
            }
         } else {
            return EnumResult.FAIL;
         }
      }
   }

   public int a(int var1) {
      return var1;
   }

   public String f_(ItemStack var1) {
      int var2 = var1.i();
      if(var2 < 0 || var2 >= a.length) {
         var2 = 0;
      }

      return super.a() + "." + a[var2];
   }

   public String a(ItemStack var1) {
      if(var1.i() == 3 && var1.n()) {
         if(var1.o().b("SkullOwner", 8)) {
            return class_di.a("item.skull.player.name", new Object[]{var1.o().l("SkullOwner")});
         }

         if(var1.o().b("SkullOwner", 10)) {
            NBTTagCompound var2 = var1.o().o("SkullOwner");
            if(var2.b("Name", 8)) {
               return class_di.a("item.skull.player.name", new Object[]{var2.l("Name")});
            }
         }
      }

      return super.a(var1);
   }

   public boolean a(NBTTagCompound var1) {
      super.a(var1);
      if(var1.b("SkullOwner", 8) && !var1.l("SkullOwner").isEmpty()) {
         GameProfile var2 = new GameProfile((UUID)null, var1.l("SkullOwner"));
         var2 = class_aqn.b(var2);
         var1.a((String)"SkullOwner", (NBTTag)GameProfileSerializer.a(new NBTTagCompound(), var2));
         return true;
      } else {
         return false;
      }
   }
}
