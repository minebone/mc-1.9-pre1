package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.StructureBoundingBox;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.class_aid;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.BaseBlockPosition;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.IInventory;

public class CommandClone extends CommandAbstract {
   public String c() {
      return "clone";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.clone.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 9) {
         throw new class_cf("commands.clone.usage", new Object[0]);
      } else {
         var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_BLOCKS, 0);
         BlockPosition var4 = a(var2, var3, 0, false);
         BlockPosition var5 = a(var2, var3, 3, false);
         BlockPosition var6 = a(var2, var3, 6, false);
         StructureBoundingBox var7 = new StructureBoundingBox(var4, var5);
         StructureBoundingBox var8 = new StructureBoundingBox(var6, var6.a(var7.b()));
         int var9 = var7.c() * var7.d() * var7.e();
         if(var9 > '耀') {
            throw new class_bz("commands.clone.tooManyBlocks", new Object[]{Integer.valueOf(var9), Integer.valueOf('耀')});
         } else {
            boolean var10 = false;
            Block var11 = null;
            int var12 = -1;
            if((var3.length < 11 || !var3[10].equals("force") && !var3[10].equals("move")) && var7.a(var8)) {
               throw new class_bz("commands.clone.noOverlap", new Object[0]);
            } else {
               if(var3.length >= 11 && var3[10].equals("move")) {
                  var10 = true;
               }

               if(var7.b >= 0 && var7.e < 256 && var8.b >= 0 && var8.e < 256) {
                  World var13 = var2.e();
                  if(var13.a(var7) && var13.a(var8)) {
                     boolean var14 = false;
                     if(var3.length >= 10) {
                        if(var3[9].equals("masked")) {
                           var14 = true;
                        } else if(var3[9].equals("filtered")) {
                           if(var3.length < 12) {
                              throw new class_cf("commands.clone.usage", new Object[0]);
                           }

                           var11 = b(var2, var3[11]);
                           if(var3.length >= 13) {
                              var12 = a(var3[12], 0, 15);
                           }
                        }
                     }

                     ArrayList var15 = Lists.newArrayList();
                     ArrayList var16 = Lists.newArrayList();
                     ArrayList var17 = Lists.newArrayList();
                     LinkedList var18 = Lists.newLinkedList();
                     BlockPosition var19 = new BlockPosition(var8.a - var7.a, var8.b - var7.b, var8.c - var7.c);

                     for(int var20 = var7.c; var20 <= var7.f; ++var20) {
                        for(int var21 = var7.b; var21 <= var7.e; ++var21) {
                           for(int var22 = var7.a; var22 <= var7.d; ++var22) {
                              BlockPosition var23 = new BlockPosition(var22, var21, var20);
                              BlockPosition var24 = var23.a((BaseBlockPosition)var19);
                              IBlockData var25 = var13.getType(var23);
                              if((!var14 || var25.getBlock() != Blocks.AIR) && (var11 == null || var25.getBlock() == var11 && (var12 < 0 || var25.getBlock().e(var25) == var12))) {
                                 TileEntity var26 = var13.r(var23);
                                 if(var26 != null) {
                                    NBTTagCompound var27 = new NBTTagCompound();
                                    var26.a(var27);
                                    var16.add(new CommandClone.class_a_in_class_u(var24, var25, var27));
                                    var18.addLast(var23);
                                 } else if(!var25.b() && !var25.h()) {
                                    var17.add(new CommandClone.class_a_in_class_u(var24, var25, (NBTTagCompound)null));
                                    var18.addFirst(var23);
                                 } else {
                                    var15.add(new CommandClone.class_a_in_class_u(var24, var25, (NBTTagCompound)null));
                                    var18.addLast(var23);
                                 }
                              }
                           }
                        }
                     }

                     if(var10) {
                        Iterator var28;
                        BlockPosition var30;
                        for(var28 = var18.iterator(); var28.hasNext(); var13.a((BlockPosition)var30, (IBlockData)Blocks.cv.u(), 2)) {
                           var30 = (BlockPosition)var28.next();
                           TileEntity var32 = var13.r(var30);
                           if(var32 instanceof IInventory) {
                              ((IInventory)var32).l();
                           }
                        }

                        var28 = var18.iterator();

                        while(var28.hasNext()) {
                           var30 = (BlockPosition)var28.next();
                           var13.a((BlockPosition)var30, (IBlockData)Blocks.AIR.u(), 3);
                        }
                     }

                     ArrayList var29 = Lists.newArrayList();
                     var29.addAll(var15);
                     var29.addAll(var16);
                     var29.addAll(var17);
                     List var31 = Lists.reverse(var29);

                     Iterator var33;
                     CommandClone.class_a_in_class_u var34;
                     TileEntity var35;
                     for(var33 = var31.iterator(); var33.hasNext(); var13.a((BlockPosition)var34.a, (IBlockData)Blocks.cv.u(), 2)) {
                        var34 = (CommandClone.class_a_in_class_u)var33.next();
                        var35 = var13.r(var34.a);
                        if(var35 instanceof IInventory) {
                           ((IInventory)var35).l();
                        }
                     }

                     var9 = 0;
                     var33 = var29.iterator();

                     while(var33.hasNext()) {
                        var34 = (CommandClone.class_a_in_class_u)var33.next();
                        if(var13.a((BlockPosition)var34.a, (IBlockData)var34.b, 2)) {
                           ++var9;
                        }
                     }

                     for(var33 = var16.iterator(); var33.hasNext(); var13.a((BlockPosition)var34.a, (IBlockData)var34.b, 2)) {
                        var34 = (CommandClone.class_a_in_class_u)var33.next();
                        var35 = var13.r(var34.a);
                        if(var34.c != null && var35 != null) {
                           var34.c.a("x", var34.a.p());
                           var34.c.a("y", var34.a.q());
                           var34.c.a("z", var34.a.r());
                           var35.a(var1, var34.c);
                           var35.v_();
                        }
                     }

                     var33 = var31.iterator();

                     while(var33.hasNext()) {
                        var34 = (CommandClone.class_a_in_class_u)var33.next();
                        var13.c(var34.a, var34.b.getBlock());
                     }

                     List var36 = var13.a(var7, false);
                     if(var36 != null) {
                        Iterator var37 = var36.iterator();

                        while(var37.hasNext()) {
                           class_aid var38 = (class_aid)var37.next();
                           if(var7.b((BaseBlockPosition)var38.a)) {
                              BlockPosition var39 = var38.a.a((BaseBlockPosition)var19);
                              var13.b(var39, var38.a(), (int)(var38.b - var13.T().e()), var38.c);
                           }
                        }
                     }

                     if(var9 <= 0) {
                        throw new class_bz("commands.clone.failed", new Object[0]);
                     } else {
                        var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_BLOCKS, var9);
                        a(var2, this, "commands.clone.success", new Object[]{Integer.valueOf(var9)});
                     }
                  } else {
                     throw new class_bz("commands.clone.outOfWorld", new Object[0]);
                  }
               } else {
                  throw new class_bz("commands.clone.outOfWorld", new Object[0]);
               }
            }
         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length > 0 && var3.length <= 3?a(var3, 0, var4):(var3.length > 3 && var3.length <= 6?a(var3, 3, var4):(var3.length > 6 && var3.length <= 9?a(var3, 6, var4):(var3.length == 10?a(var3, new String[]{"replace", "masked", "filtered"}):(var3.length == 11?a(var3, new String[]{"normal", "force", "move"}):(var3.length == 12 && "filtered".equals(var3[9])?a(var3, Block.h.c()):Collections.emptyList())))));
   }

   static class class_a_in_class_u {
      public final BlockPosition a;
      public final IBlockData b;
      public final NBTTagCompound c;

      public class_a_in_class_u(BlockPosition var1, IBlockData var2, NBTTagCompound var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }
   }
}
