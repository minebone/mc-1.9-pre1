package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_or;
import net.minecraft.server.class_rz;

public class BlockHugeMushroom extends Block {
   public static final BlockStateEnum a = BlockStateEnum.a("variant", BlockHugeMushroom.EnumHugeMushroomVariant.class);
   private final Block b;

   public BlockHugeMushroom(Material var1, MaterialMapColor var2, Block var3) {
      super(var1, var2);
      this.w(this.A.b().set(a, BlockHugeMushroom.EnumHugeMushroomVariant.ALL_OUTSIDE));
      this.b = var3;
   }

   public int a(Random var1) {
      return Math.max(0, var1.nextInt(10) - 7);
   }

   public MaterialMapColor r(IBlockData var1) {
      switch(BlockHugeMushroom.SyntheticClass_1.a[((BlockHugeMushroom.EnumHugeMushroomVariant)var1.get(a)).ordinal()]) {
      case 1:
         return MaterialMapColor.e;
      case 2:
         return MaterialMapColor.d;
      case 3:
         return MaterialMapColor.d;
      default:
         return super.r(var1);
      }
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Item.a(this.b);
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(this.b);
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return this.u();
   }

   public IBlockData a(int var1) {
      return this.u().set(a, BlockHugeMushroom.EnumHugeMushroomVariant.a(var1));
   }

   public int e(IBlockData var1) {
      return ((BlockHugeMushroom.EnumHugeMushroomVariant)var1.get(a)).a();
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      switch(BlockHugeMushroom.SyntheticClass_1.b[var2.ordinal()]) {
      case 1:
         switch(BlockHugeMushroom.SyntheticClass_1.a[((BlockHugeMushroom.EnumHugeMushroomVariant)var1.get(a)).ordinal()]) {
         case 3:
            break;
         case 4:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH_EAST);
         case 5:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH);
         case 6:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH_WEST);
         case 7:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.EAST);
         case 8:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.WEST);
         case 9:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.NORTH_EAST);
         case 10:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.NORTH);
         case 11:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.NORTH_WEST);
         default:
            return var1;
         }
      case 2:
         switch(BlockHugeMushroom.SyntheticClass_1.a[((BlockHugeMushroom.EnumHugeMushroomVariant)var1.get(a)).ordinal()]) {
         case 3:
            break;
         case 4:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH_WEST);
         case 5:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.WEST);
         case 6:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.NORTH_WEST);
         case 7:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH);
         case 8:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.NORTH);
         case 9:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH_EAST);
         case 10:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.EAST);
         case 11:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.NORTH_EAST);
         default:
            return var1;
         }
      case 3:
         switch(BlockHugeMushroom.SyntheticClass_1.a[((BlockHugeMushroom.EnumHugeMushroomVariant)var1.get(a)).ordinal()]) {
         case 3:
            break;
         case 4:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.NORTH_EAST);
         case 5:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.EAST);
         case 6:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH_EAST);
         case 7:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.NORTH);
         case 8:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH);
         case 9:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.NORTH_WEST);
         case 10:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.WEST);
         case 11:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH_WEST);
         default:
            return var1;
         }
      default:
         return var1;
      }
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      BlockHugeMushroom.EnumHugeMushroomVariant var3 = (BlockHugeMushroom.EnumHugeMushroomVariant)var1.get(a);
      switch(BlockHugeMushroom.SyntheticClass_1.c[var2.ordinal()]) {
      case 1:
         switch(BlockHugeMushroom.SyntheticClass_1.a[var3.ordinal()]) {
         case 4:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH_WEST);
         case 5:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH);
         case 6:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH_EAST);
         case 7:
         case 8:
         default:
            return super.a(var1, var2);
         case 9:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.NORTH_WEST);
         case 10:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.NORTH);
         case 11:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.NORTH_EAST);
         }
      case 2:
         switch(BlockHugeMushroom.SyntheticClass_1.a[var3.ordinal()]) {
         case 4:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.NORTH_EAST);
         case 5:
         case 10:
         default:
            break;
         case 6:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.NORTH_WEST);
         case 7:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.EAST);
         case 8:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.WEST);
         case 9:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH_EAST);
         case 11:
            return var1.set(a, BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH_WEST);
         }
      }

      return super.a(var1, var2);
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b;
      // $FF: synthetic field
      static final int[] c = new int[class_amq.values().length];

      static {
         try {
            c[class_amq.LEFT_RIGHT.ordinal()] = 1;
         } catch (NoSuchFieldError var16) {
            ;
         }

         try {
            c[class_amq.FRONT_BACK.ordinal()] = 2;
         } catch (NoSuchFieldError var15) {
            ;
         }

         b = new int[class_aod.values().length];

         try {
            b[class_aod.CLOCKWISE_180.ordinal()] = 1;
         } catch (NoSuchFieldError var14) {
            ;
         }

         try {
            b[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var13) {
            ;
         }

         try {
            b[class_aod.CLOCKWISE_90.ordinal()] = 3;
         } catch (NoSuchFieldError var12) {
            ;
         }

         a = new int[BlockHugeMushroom.EnumHugeMushroomVariant.values().length];

         try {
            a[BlockHugeMushroom.EnumHugeMushroomVariant.ALL_STEM.ordinal()] = 1;
         } catch (NoSuchFieldError var11) {
            ;
         }

         try {
            a[BlockHugeMushroom.EnumHugeMushroomVariant.ALL_INSIDE.ordinal()] = 2;
         } catch (NoSuchFieldError var10) {
            ;
         }

         try {
            a[BlockHugeMushroom.EnumHugeMushroomVariant.STEM.ordinal()] = 3;
         } catch (NoSuchFieldError var9) {
            ;
         }

         try {
            a[BlockHugeMushroom.EnumHugeMushroomVariant.NORTH_WEST.ordinal()] = 4;
         } catch (NoSuchFieldError var8) {
            ;
         }

         try {
            a[BlockHugeMushroom.EnumHugeMushroomVariant.NORTH.ordinal()] = 5;
         } catch (NoSuchFieldError var7) {
            ;
         }

         try {
            a[BlockHugeMushroom.EnumHugeMushroomVariant.NORTH_EAST.ordinal()] = 6;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[BlockHugeMushroom.EnumHugeMushroomVariant.WEST.ordinal()] = 7;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[BlockHugeMushroom.EnumHugeMushroomVariant.EAST.ordinal()] = 8;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH_WEST.ordinal()] = 9;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH.ordinal()] = 10;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[BlockHugeMushroom.EnumHugeMushroomVariant.SOUTH_EAST.ordinal()] = 11;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum EnumHugeMushroomVariant implements class_or {
      NORTH_WEST(1, "north_west"),
      NORTH(2, "north"),
      NORTH_EAST(3, "north_east"),
      WEST(4, "west"),
      CENTER(5, "center"),
      EAST(6, "east"),
      SOUTH_WEST(7, "south_west"),
      SOUTH(8, "south"),
      SOUTH_EAST(9, "south_east"),
      STEM(10, "stem"),
      ALL_INSIDE(0, "all_inside"),
      ALL_OUTSIDE(14, "all_outside"),
      ALL_STEM(15, "all_stem");

      private static final BlockHugeMushroom.EnumHugeMushroomVariant[] n = new BlockHugeMushroom.EnumHugeMushroomVariant[16];
      private final int o;
      private final String p;

      private EnumHugeMushroomVariant(int var3, String var4) {
         this.o = var3;
         this.p = var4;
      }

      public int a() {
         return this.o;
      }

      public String toString() {
         return this.p;
      }

      public static BlockHugeMushroom.EnumHugeMushroomVariant a(int var0) {
         if(var0 < 0 || var0 >= n.length) {
            var0 = 0;
         }

         BlockHugeMushroom.EnumHugeMushroomVariant var1 = n[var0];
         return var1 == null?n[0]:var1;
      }

      public String m() {
         return this.p;
      }

      static {
         BlockHugeMushroom.EnumHugeMushroomVariant[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            BlockHugeMushroom.EnumHugeMushroomVariant var3 = var0[var2];
            n[var3.a()] = var3;
         }

      }
   }
}
