package net.minecraft.server;

import com.google.common.collect.Sets;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutBossBar;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.MathHelper;
import net.minecraft.server.BossBar;

public class class_ln extends BossBar {
   private final Set h = Sets.newHashSet();
   private final Set i;
   private boolean j;

   public class_ln(IChatBaseComponent var1, BossBar.EnumBossBarColor var2, BossBar.EnumBossBarType var3) {
      super(MathHelper.a(), var1, var2, var3);
      this.i = Collections.unmodifiableSet(this.h);
      this.j = true;
   }

   public void a(float var1) {
      if(var1 != this.health) {
         super.a(var1);
         this.a(PacketPlayOutBossBar.class_a_in_class_fv.UPDATE_PCT);
      }

   }

   public BossBar a(boolean var1) {
      if(var1 != this.e) {
         super.a(var1);
         this.a(PacketPlayOutBossBar.class_a_in_class_fv.UPDATE_PROPERTIES);
      }

      return this;
   }

   public BossBar b(boolean var1) {
      if(var1 != this.f) {
         super.b(var1);
         this.a(PacketPlayOutBossBar.class_a_in_class_fv.UPDATE_PROPERTIES);
      }

      return this;
   }

   public BossBar c(boolean var1) {
      if(var1 != this.g) {
         super.c(var1);
         this.a(PacketPlayOutBossBar.class_a_in_class_fv.UPDATE_PROPERTIES);
      }

      return this;
   }

   private void a(PacketPlayOutBossBar.class_a_in_class_fv var1) {
      if(this.j) {
         PacketPlayOutBossBar var2 = new PacketPlayOutBossBar(var1, this);
         Iterator var3 = this.h.iterator();

         while(var3.hasNext()) {
            EntityPlayer var4 = (EntityPlayer)var3.next();
            var4.a.a((Packet)var2);
         }
      }

   }

   public void a(EntityPlayer var1) {
      if(this.h.add(var1) && this.j) {
         var1.a.a((Packet)(new PacketPlayOutBossBar(PacketPlayOutBossBar.class_a_in_class_fv.ADD, this)));
      }

   }

   public void b(EntityPlayer var1) {
      if(this.h.remove(var1) && this.j) {
         var1.a.a((Packet)(new PacketPlayOutBossBar(PacketPlayOutBossBar.class_a_in_class_fv.REMOVE, this)));
      }

   }

   public void d(boolean var1) {
      if(var1 != this.j) {
         this.j = var1;
         Iterator var2 = this.h.iterator();

         while(var2.hasNext()) {
            EntityPlayer var3 = (EntityPlayer)var2.next();
            var3.a.a((Packet)(new PacketPlayOutBossBar(var1?PacketPlayOutBossBar.class_a_in_class_fv.ADD:PacketPlayOutBossBar.class_a_in_class_fv.REMOVE, this)));
         }
      }

   }

   public Collection c() {
      return this.i;
   }
}
