package net.minecraft.server;

import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Item;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aky;
import net.minecraft.server.class_aoa;
import net.minecraft.server.class_aou;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;

public abstract class class_amn extends Block {
   public static final BlockStateInteger b = BlockStateInteger.a("level", 0, 15);

   protected class_amn(Material var1) {
      super(var1);
      this.w(this.A.b().set(b, Integer.valueOf(0)));
      this.a(true);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return j;
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public boolean b(class_ahw var1, BlockPosition var2) {
      return this.x != Material.i;
   }

   public static float e(int var0) {
      if(var0 >= 8) {
         var0 = 0;
      }

      return (float)(var0 + 1) / 9.0F;
   }

   protected int c(class_ahw var1, BlockPosition var2) {
      return var1.getType(var2).getMaterial() == this.x?((Integer)var1.getType(var2).get(b)).intValue():-1;
   }

   protected int d(class_ahw var1, BlockPosition var2) {
      int var3 = this.c(var1, var2);
      return var3 >= 8?0:var3;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean a(IBlockData var1, boolean var2) {
      return var2 && ((Integer)var1.get(b)).intValue() == 0;
   }

   public boolean a(class_ahw var1, BlockPosition var2, EnumDirection var3) {
      Material var4 = var1.getType(var2).getMaterial();
      return var4 == this.x?false:(var3 == EnumDirection.UP?true:(var4 == Material.w?false:super.a(var1, var2, var3)));
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.LIQUID;
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return null;
   }

   public int a(Random var1) {
      return 0;
   }

   protected Vec3D f(class_ahw var1, BlockPosition var2) {
      double var3 = 0.0D;
      double var5 = 0.0D;
      double var7 = 0.0D;
      int var9 = this.d(var1, var2);
      BlockPosition.class_b_in_class_cj var10 = BlockPosition.class_b_in_class_cj.s();
      Iterator var11 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

      while(var11.hasNext()) {
         EnumDirection var12 = (EnumDirection)var11.next();
         var10.h(var2).c(var12);
         int var13 = this.d(var1, var10);
         int var14;
         if(var13 < 0) {
            if(!var1.getType(var10).getMaterial().c()) {
               var13 = this.d(var1, var10.b());
               if(var13 >= 0) {
                  var14 = var13 - (var9 - 8);
                  var3 += (double)(var12.g() * var14);
                  var5 += (double)(var12.h() * var14);
                  var7 += (double)(var12.i() * var14);
               }
            }
         } else if(var13 >= 0) {
            var14 = var13 - var9;
            var3 += (double)(var12.g() * var14);
            var5 += (double)(var12.h() * var14);
            var7 += (double)(var12.i() * var14);
         }
      }

      Vec3D var15 = new Vec3D(var3, var5, var7);
      if(((Integer)var1.getType(var2).get(b)).intValue() >= 8) {
         label44: {
            Iterator var16 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

            EnumDirection var17;
            do {
               if(!var16.hasNext()) {
                  break label44;
               }

               var17 = (EnumDirection)var16.next();
               var10.h(var2).c(var17);
            } while(!this.a((class_ahw)var1, (BlockPosition)var10, (EnumDirection)var17) && !this.a(var1, var10.a(), var17));

            var15 = var15.a().b(0.0D, -6.0D, 0.0D);
         }
      }

      var10.t();
      return var15.a();
   }

   public Vec3D a(World var1, BlockPosition var2, Entity var3, Vec3D var4) {
      return var4.e(this.f(var1, var2));
   }

   public int a(World var1) {
      return this.x == Material.h?5:(this.x == Material.i?(var1.s.isNotOverworld()?10:30):0);
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      this.e(var1, var2, var3);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      this.e(var1, var2, var3);
   }

   public boolean e(World var1, BlockPosition var2, IBlockData var3) {
      if(this.x == Material.i) {
         boolean var4 = false;
         EnumDirection[] var5 = EnumDirection.values();
         int var6 = var5.length;

         for(int var7 = 0; var7 < var6; ++var7) {
            EnumDirection var8 = var5[var7];
            if(var8 != EnumDirection.DOWN && var1.getType(var2.a(var8)).getMaterial() == Material.h) {
               var4 = true;
               break;
            }
         }

         if(var4) {
            Integer var9 = (Integer)var3.get(b);
            if(var9.intValue() == 0) {
               var1.a(var2, Blocks.Z.u());
               this.b(var1, var2);
               return true;
            }

            if(var9.intValue() <= 4) {
               var1.a(var2, Blocks.e.u());
               this.b(var1, var2);
               return true;
            }
         }
      }

      return false;
   }

   protected void b(World var1, BlockPosition var2) {
      double var3 = (double)var2.p();
      double var5 = (double)var2.q();
      double var7 = (double)var2.r();
      var1.a((EntityHuman)null, var2, class_ng.cY, EnumSoundCategory.BLOCKS, 0.5F, 2.6F + (var1.r.nextFloat() - var1.r.nextFloat()) * 0.8F);

      for(int var9 = 0; var9 < 8; ++var9) {
         var1.a(EnumParticle.SMOKE_LARGE, var3 + Math.random(), var5 + 1.2D, var7 + Math.random(), 0.0D, 0.0D, 0.0D, new int[0]);
      }

   }

   public IBlockData a(int var1) {
      return this.u().set(b, Integer.valueOf(var1));
   }

   public int e(IBlockData var1) {
      return ((Integer)var1.get(b)).intValue();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{b});
   }

   public static class_aky a(Material var0) {
      if(var0 == Material.h) {
         return Blocks.i;
      } else if(var0 == Material.i) {
         return Blocks.k;
      } else {
         throw new IllegalArgumentException("Invalid material");
      }
   }

   public static class_aou b(Material var0) {
      if(var0 == Material.h) {
         return Blocks.j;
      } else if(var0 == Material.i) {
         return Blocks.l;
      } else {
         throw new IllegalArgumentException("Invalid material");
      }
   }
}
