package net.minecraft.server;

import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.class_auc;
import net.minecraft.server.BlockPosition;

public class class_atz extends class_auc {
   public boolean b(World var1, Random var2, BlockPosition var3) {
      Iterator var4 = BlockPosition.b(var3.a(-1, -2, -1), var3.a(1, 2, 1)).iterator();

      while(true) {
         while(var4.hasNext()) {
            BlockPosition.class_a_in_class_cj var5 = (BlockPosition.class_a_in_class_cj)var4.next();
            boolean var6 = var5.p() == var3.p();
            boolean var7 = var5.q() == var3.q();
            boolean var8 = var5.r() == var3.r();
            boolean var9 = Math.abs(var5.q() - var3.q()) == 2;
            if(var6 && var7 && var8) {
               this.a(var1, new BlockPosition(var5), Blocks.db.u());
            } else if(var7) {
               this.a(var1, var5, Blocks.AIR.u());
            } else if(var9 && var6 && var8) {
               this.a(var1, var5, Blocks.h.u());
            } else if((var6 || var8) && !var9) {
               this.a(var1, var5, Blocks.h.u());
            } else {
               this.a(var1, var5, Blocks.AIR.u());
            }
         }

         return true;
      }
   }
}
