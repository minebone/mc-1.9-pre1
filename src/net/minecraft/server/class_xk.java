package net.minecraft.server;

import net.minecraft.server.class_wt;
import net.minecraft.server.class_xd;
import net.minecraft.server.class_xj;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_xk {
   private static final Logger a = LogManager.getLogger();
   private final class_wt b;
   private final class_xd[] c = new class_xd[class_xj.c()];
   private class_xd d;

   public class_xk(class_wt var1) {
      this.b = var1;
      this.a(class_xj.k);
   }

   public void a(class_xj var1) {
      if(this.d == null || var1 != this.d.i()) {
         if(this.d != null) {
            this.d.e();
         }

         this.d = this.b(var1);
         if(!this.b.world.E) {
            this.b.Q().b(class_wt.a, Integer.valueOf(var1.b()));
         }

         a.debug("Dragon is now in phase {} on the {}", new Object[]{var1, this.b.world.E?"client":"server"});
         this.d.d();
      }
   }

   public class_xd a() {
      return this.d;
   }

   public class_xd b(class_xj var1) {
      int var2 = var1.b();
      if(this.c[var2] == null) {
         this.c[var2] = var1.a(this.b);
      }

      return this.c[var2];
   }
}
