package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_or;
import net.minecraft.server.class_rz;

public class BlockQuartz extends Block {
   public static final BlockStateEnum a = BlockStateEnum.a("variant", BlockQuartz.EnumQuartzVariant.class);

   public BlockQuartz() {
      super(Material.e);
      this.w(this.A.b().set(a, BlockQuartz.EnumQuartzVariant.DEFAULT));
      this.a(CreativeModeTab.b);
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      if(var7 == BlockQuartz.EnumQuartzVariant.LINES_Y.a()) {
         switch(BlockQuartz.SyntheticClass_1.a[var3.k().ordinal()]) {
         case 1:
            return this.u().set(a, BlockQuartz.EnumQuartzVariant.LINES_Z);
         case 2:
            return this.u().set(a, BlockQuartz.EnumQuartzVariant.LINES_X);
         case 3:
         default:
            return this.u().set(a, BlockQuartz.EnumQuartzVariant.LINES_Y);
         }
      } else {
         return var7 == BlockQuartz.EnumQuartzVariant.CHISELED.a()?this.u().set(a, BlockQuartz.EnumQuartzVariant.CHISELED):this.u().set(a, BlockQuartz.EnumQuartzVariant.DEFAULT);
      }
   }

   public int d(IBlockData var1) {
      BlockQuartz.EnumQuartzVariant var2 = (BlockQuartz.EnumQuartzVariant)var1.get(a);
      return var2 != BlockQuartz.EnumQuartzVariant.LINES_X && var2 != BlockQuartz.EnumQuartzVariant.LINES_Z?var2.a():BlockQuartz.EnumQuartzVariant.LINES_Y.a();
   }

   protected ItemStack u(IBlockData var1) {
      BlockQuartz.EnumQuartzVariant var2 = (BlockQuartz.EnumQuartzVariant)var1.get(a);
      return var2 != BlockQuartz.EnumQuartzVariant.LINES_X && var2 != BlockQuartz.EnumQuartzVariant.LINES_Z?super.u(var1):new ItemStack(Item.a((Block)this), 1, BlockQuartz.EnumQuartzVariant.LINES_Y.a());
   }

   public MaterialMapColor r(IBlockData var1) {
      return MaterialMapColor.p;
   }

   public IBlockData a(int var1) {
      return this.u().set(a, BlockQuartz.EnumQuartzVariant.a(var1));
   }

   public int e(IBlockData var1) {
      return ((BlockQuartz.EnumQuartzVariant)var1.get(a)).a();
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      switch(BlockQuartz.SyntheticClass_1.c[var2.ordinal()]) {
      case 1:
      case 2:
         switch(BlockQuartz.SyntheticClass_1.b[((BlockQuartz.EnumQuartzVariant)var1.get(a)).ordinal()]) {
         case 1:
            return var1.set(a, BlockQuartz.EnumQuartzVariant.LINES_Z);
         case 2:
            return var1.set(a, BlockQuartz.EnumQuartzVariant.LINES_X);
         default:
            return var1;
         }
      default:
         return var1;
      }
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b;
      // $FF: synthetic field
      static final int[] c = new int[class_aod.values().length];

      static {
         try {
            c[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 1;
         } catch (NoSuchFieldError var7) {
            ;
         }

         try {
            c[class_aod.CLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var6) {
            ;
         }

         b = new int[BlockQuartz.EnumQuartzVariant.values().length];

         try {
            b[BlockQuartz.EnumQuartzVariant.LINES_X.ordinal()] = 1;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            b[BlockQuartz.EnumQuartzVariant.LINES_Z.ordinal()] = 2;
         } catch (NoSuchFieldError var4) {
            ;
         }

         a = new int[EnumDirection.class_a_in_class_cq.values().length];

         try {
            a[EnumDirection.class_a_in_class_cq.Z.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[EnumDirection.class_a_in_class_cq.X.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[EnumDirection.class_a_in_class_cq.Y.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum EnumQuartzVariant implements class_or {
      DEFAULT(0, "default", "default"),
      CHISELED(1, "chiseled", "chiseled"),
      LINES_Y(2, "lines_y", "lines"),
      LINES_X(3, "lines_x", "lines"),
      LINES_Z(4, "lines_z", "lines");

      private static final BlockQuartz.EnumQuartzVariant[] f = new BlockQuartz.EnumQuartzVariant[values().length];
      private final int g;
      private final String h;
      private final String i;

      private EnumQuartzVariant(int var3, String var4, String var5) {
         this.g = var3;
         this.h = var4;
         this.i = var5;
      }

      public int a() {
         return this.g;
      }

      public String toString() {
         return this.i;
      }

      public static BlockQuartz.EnumQuartzVariant a(int var0) {
         if(var0 < 0 || var0 >= f.length) {
            var0 = 0;
         }

         return f[var0];
      }

      public String m() {
         return this.h;
      }

      static {
         BlockQuartz.EnumQuartzVariant[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            BlockQuartz.EnumQuartzVariant var3 = var0[var2];
            f[var3.a()] = var3;
         }

      }
   }
}
