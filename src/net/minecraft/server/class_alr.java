package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.Container;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.TileEntityFurnace;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_amf;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aoa;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arn;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_qi;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;

public class class_alr extends class_ajm {
   public static final class_arn a = class_amf.D;
   private final boolean b;
   private static boolean c;

   protected class_alr(boolean var1) {
      super(Material.e);
      this.w(this.A.b().set(a, EnumDirection.NORTH));
      this.b = var1;
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Item.a(Blocks.al);
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      this.e(var1, var2, var3);
   }

   private void e(World var1, BlockPosition var2, IBlockData var3) {
      if(!var1.E) {
         IBlockData var4 = var1.getType(var2.c());
         IBlockData var5 = var1.getType(var2.d());
         IBlockData var6 = var1.getType(var2.e());
         IBlockData var7 = var1.getType(var2.f());
         EnumDirection var8 = (EnumDirection)var3.get(a);
         if(var8 == EnumDirection.NORTH && var4.b() && !var5.b()) {
            var8 = EnumDirection.SOUTH;
         } else if(var8 == EnumDirection.SOUTH && var5.b() && !var4.b()) {
            var8 = EnumDirection.NORTH;
         } else if(var8 == EnumDirection.WEST && var6.b() && !var7.b()) {
            var8 = EnumDirection.EAST;
         } else if(var8 == EnumDirection.EAST && var7.b() && !var6.b()) {
            var8 = EnumDirection.WEST;
         }

         var1.a((BlockPosition)var2, (IBlockData)var3.set(a, var8), 2);
      }
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(var1.E) {
         return true;
      } else {
         TileEntity var11 = var1.r(var2);
         if(var11 instanceof TileEntityFurnace) {
            var4.openContainer((IInventory)((TileEntityFurnace)var11));
            var4.b(StatisticList.aa);
         }

         return true;
      }
   }

   public static void a(boolean var0, World var1, BlockPosition var2) {
      IBlockData var3 = var1.getType(var2);
      TileEntity var4 = var1.r(var2);
      c = true;
      if(var0) {
         var1.a((BlockPosition)var2, (IBlockData)Blocks.am.u().set(a, var3.get(a)), 3);
         var1.a((BlockPosition)var2, (IBlockData)Blocks.am.u().set(a, var3.get(a)), 3);
      } else {
         var1.a((BlockPosition)var2, (IBlockData)Blocks.al.u().set(a, var3.get(a)), 3);
         var1.a((BlockPosition)var2, (IBlockData)Blocks.al.u().set(a, var3.get(a)), 3);
      }

      c = false;
      if(var4 != null) {
         var4.z();
         var1.a(var2, var4);
      }

   }

   public TileEntity a(World var1, int var2) {
      return new TileEntityFurnace();
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return this.u().set(a, var8.bh().d());
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, class_rz var4, ItemStack var5) {
      var1.a((BlockPosition)var2, (IBlockData)var3.set(a, var4.bh().d()), 2);
      if(var5.s()) {
         TileEntity var6 = var1.r(var2);
         if(var6 instanceof TileEntityFurnace) {
            ((TileEntityFurnace)var6).a(var5.q());
         }
      }

   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      if(!c) {
         TileEntity var4 = var1.r(var2);
         if(var4 instanceof TileEntityFurnace) {
            class_qi.a(var1, (BlockPosition)var2, (TileEntityFurnace)var4);
            var1.f(var2, this);
         }
      }

      super.b(var1, var2, var3);
   }

   public boolean v(IBlockData var1) {
      return true;
   }

   public int d(IBlockData var1, World var2, BlockPosition var3) {
      return Container.a(var2.r(var3));
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Blocks.al);
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.MODEL;
   }

   public IBlockData a(int var1) {
      EnumDirection var2 = EnumDirection.a(var1);
      if(var2.k() == EnumDirection.class_a_in_class_cq.Y) {
         var2 = EnumDirection.NORTH;
      }

      return this.u().set(a, var2);
   }

   public int e(IBlockData var1) {
      return ((EnumDirection)var1.get(a)).a();
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(a, var2.a((EnumDirection)var1.get(a)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(a)));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }
}
