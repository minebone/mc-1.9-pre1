package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.BlockDirt;
import net.minecraft.server.BlockLongGrass;
import net.minecraft.server.BlockTallPlant;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.class_ate;
import net.minecraft.server.class_ato;
import net.minecraft.server.class_atr;
import net.minecraft.server.class_auc;
import net.minecraft.server.class_auo;
import net.minecraft.server.class_aut;
import net.minecraft.server.class_avc;
import net.minecraft.server.class_ave;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_wc;
import net.minecraft.server.class_wi;

public class class_ajc extends BiomeBase {
   private static final class_aut y = new class_aut();
   private static final class_avc z = new class_avc(false);
   private static final class_auo A = new class_auo(false, false);
   private static final class_auo B = new class_auo(false, true);
   private static final class_atr C = new class_atr(Blocks.Y, 0);
   private class_ajc.class_a_in_class_ajc D;

   public class_ajc(class_ajc.class_a_in_class_ajc var1, BiomeBase.class_a_in_class_aif var2) {
      super(var2);
      this.D = var1;
      this.v.add(new BiomeBase.BiomeMeta(class_wi.class, 8, 4, 4));
      this.v.add(new BiomeBase.BiomeMeta(class_wc.class, 4, 2, 3));
      this.t.z = 10;
      if(var1 != class_ajc.class_a_in_class_ajc.MEGA && var1 != class_ajc.class_a_in_class_ajc.MEGA_SPRUCE) {
         this.t.B = 1;
         this.t.D = 1;
      } else {
         this.t.B = 7;
         this.t.C = 1;
         this.t.D = 3;
      }

   }

   public class_ato a(Random var1) {
      return (class_ato)((this.D == class_ajc.class_a_in_class_ajc.MEGA || this.D == class_ajc.class_a_in_class_ajc.MEGA_SPRUCE) && var1.nextInt(3) == 0?(this.D != class_ajc.class_a_in_class_ajc.MEGA_SPRUCE && var1.nextInt(13) != 0?A:B):(var1.nextInt(3) == 0?y:z));
   }

   public class_auc b(Random var1) {
      return var1.nextInt(5) > 0?new class_ave(BlockLongGrass.EnumTallGrassType.FERN):new class_ave(BlockLongGrass.EnumTallGrassType.GRASS);
   }

   public void a(World var1, Random var2, BlockPosition var3) {
      int var4;
      int var5;
      int var6;
      int var7;
      if(this.D == class_ajc.class_a_in_class_ajc.MEGA || this.D == class_ajc.class_a_in_class_ajc.MEGA_SPRUCE) {
         var4 = var2.nextInt(3);

         for(var5 = 0; var5 < var4; ++var5) {
            var6 = var2.nextInt(16) + 8;
            var7 = var2.nextInt(16) + 8;
            BlockPosition var8 = var1.l(var3.a(var6, 0, var7));
            C.b(var1, var2, var8);
         }
      }

      m.a(BlockTallPlant.EnumTallFlowerVariants.FERN);

      for(var4 = 0; var4 < 7; ++var4) {
         var5 = var2.nextInt(16) + 8;
         var6 = var2.nextInt(16) + 8;
         var7 = var2.nextInt(var1.l(var3.a(var5, 0, var6)).q() + 32);
         m.b(var1, var2, var3.a(var5, var7, var6));
      }

      super.a(var1, var2, var3);
   }

   public void a(World var1, Random var2, class_ate var3, int var4, int var5, double var6) {
      if(this.D == class_ajc.class_a_in_class_ajc.MEGA || this.D == class_ajc.class_a_in_class_ajc.MEGA_SPRUCE) {
         this.r = Blocks.c.u();
         this.s = Blocks.d.u();
         if(var6 > 1.75D) {
            this.r = Blocks.d.u().set(BlockDirt.a, BlockDirt.EnumDirtVariant.COARSE_DIRT);
         } else if(var6 > -0.95D) {
            this.r = Blocks.d.u().set(BlockDirt.a, BlockDirt.EnumDirtVariant.PODZOL);
         }
      }

      this.b(var1, var2, var3, var4, var5, var6);
   }

   public static enum class_a_in_class_ajc {
      NORMAL,
      MEGA,
      MEGA_SPRUCE;
   }
}
