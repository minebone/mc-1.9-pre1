package net.minecraft.server;

import java.util.List;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Container;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.TileEntityBrewingStand;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_aoa;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_qi;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;

public class BlockBrewingStand extends class_ajm {
   public static final class_arm[] a = new class_arm[]{class_arm.a("has_bottle_0"), class_arm.a("has_bottle_1"), class_arm.a("has_bottle_2")};
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.125D, 1.0D);
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.4375D, 0.0D, 0.4375D, 0.5625D, 0.875D, 0.5625D);

   public BlockBrewingStand() {
      super(Material.f);
      this.w(this.A.b().set(a[0], Boolean.valueOf(false)).set(a[1], Boolean.valueOf(false)).set(a[2], Boolean.valueOf(false)));
   }

   public String c() {
      return class_di.a("item.brewingStand.name");
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.MODEL;
   }

   public TileEntity a(World var1, int var2) {
      return new TileEntityBrewingStand();
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public void a(IBlockData var1, World var2, BlockPosition var3, AxisAlignedBB var4, List var5, Entity var6) {
      a(var3, var4, var5, c);
      a(var3, var4, var5, b);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return b;
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(var1.E) {
         return true;
      } else {
         TileEntity var11 = var1.r(var2);
         if(var11 instanceof TileEntityBrewingStand) {
            var4.openContainer((IInventory)((TileEntityBrewingStand)var11));
            var4.b(StatisticList.O);
         }

         return true;
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, class_rz var4, ItemStack var5) {
      if(var5.s()) {
         TileEntity var6 = var1.r(var2);
         if(var6 instanceof TileEntityBrewingStand) {
            ((TileEntityBrewingStand)var6).a(var5.q());
         }
      }

   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      TileEntity var4 = var1.r(var2);
      if(var4 instanceof TileEntityBrewingStand) {
         class_qi.a(var1, (BlockPosition)var2, (TileEntityBrewingStand)var4);
      }

      super.b(var1, var2, var3);
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Items.bP;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Items.bP);
   }

   public boolean v(IBlockData var1) {
      return true;
   }

   public int d(IBlockData var1, World var2, BlockPosition var3) {
      return Container.a(var2.r(var3));
   }

   public IBlockData a(int var1) {
      IBlockData var2 = this.u();

      for(int var3 = 0; var3 < 3; ++var3) {
         var2 = var2.set(a[var3], Boolean.valueOf((var1 & 1 << var3) > 0));
      }

      return var2;
   }

   public int e(IBlockData var1) {
      int var2 = 0;

      for(int var3 = 0; var3 < 3; ++var3) {
         if(((Boolean)var1.get(a[var3])).booleanValue()) {
            var2 |= 1 << var3;
         }
      }

      return var2;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a[0], a[1], a[2]});
   }
}
