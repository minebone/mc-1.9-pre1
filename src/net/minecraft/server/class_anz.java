package net.minecraft.server;

import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_anz extends Block {
   public static final BlockStateInteger a = BlockStateInteger.a("age", 0, 15);
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.125D, 0.0D, 0.125D, 0.875D, 1.0D, 0.875D);

   protected class_anz() {
      super(Material.k);
      this.w(this.A.b().set(a, Integer.valueOf(0)));
      this.a(true);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return b;
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(var1.getType(var2.b()).getBlock() == Blocks.aM || this.e(var1, var2, var3)) {
         if(var1.d(var2.a())) {
            int var5;
            for(var5 = 1; var1.getType(var2.c(var5)).getBlock() == this; ++var5) {
               ;
            }

            if(var5 < 3) {
               int var6 = ((Integer)var3.get(a)).intValue();
               if(var6 == 15) {
                  var1.a(var2.a(), this.u());
                  var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Integer.valueOf(0)), 4);
               } else {
                  var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Integer.valueOf(var6 + 1)), 4);
               }
            }
         }

      }
   }

   public boolean a(World var1, BlockPosition var2) {
      Block var3 = var1.getType(var2.b()).getBlock();
      if(var3 == this) {
         return true;
      } else if(var3 != Blocks.c && var3 != Blocks.d && var3 != Blocks.m) {
         return false;
      } else {
         BlockPosition var4 = var2.b();
         Iterator var5 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

         IBlockData var7;
         do {
            if(!var5.hasNext()) {
               return false;
            }

            EnumDirection var6 = (EnumDirection)var5.next();
            var7 = var1.getType(var4.a(var6));
         } while(var7.getMaterial() != Material.h && var7.getBlock() != Blocks.de);

         return true;
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      this.e(var1, var2, var3);
   }

   protected final boolean e(World var1, BlockPosition var2, IBlockData var3) {
      if(this.b(var1, var2)) {
         return true;
      } else {
         this.b(var1, var2, var3, 0);
         var1.g(var2);
         return false;
      }
   }

   public boolean b(World var1, BlockPosition var2) {
      return this.a(var1, var2);
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Items.aQ;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Items.aQ);
   }

   public IBlockData a(int var1) {
      return this.u().set(a, Integer.valueOf(var1));
   }

   public int e(IBlockData var1) {
      return ((Integer)var1.get(a)).intValue();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }
}
