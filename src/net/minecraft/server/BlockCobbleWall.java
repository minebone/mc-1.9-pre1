package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockFenceGate;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;
import net.minecraft.server.class_or;

public class BlockCobbleWall extends Block {
   public static final class_arm a = class_arm.a("up");
   public static final class_arm b = class_arm.a("north");
   public static final class_arm c = class_arm.a("east");
   public static final class_arm d = class_arm.a("south");
   public static final class_arm e = class_arm.a("west");
   public static final BlockStateEnum f = BlockStateEnum.a("variant", BlockCobbleWall.EnumCobbleVariant.class);
   protected static final AxisAlignedBB[] g = new AxisAlignedBB[]{new AxisAlignedBB(0.25D, 0.0D, 0.25D, 0.75D, 1.0D, 0.75D), new AxisAlignedBB(0.25D, 0.0D, 0.25D, 0.75D, 1.0D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.25D, 0.75D, 1.0D, 0.75D), new AxisAlignedBB(0.0D, 0.0D, 0.25D, 0.75D, 1.0D, 1.0D), new AxisAlignedBB(0.25D, 0.0D, 0.0D, 0.75D, 1.0D, 0.75D), new AxisAlignedBB(0.3125D, 0.0D, 0.0D, 0.6875D, 0.875D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.75D, 1.0D, 0.75D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.75D, 1.0D, 1.0D), new AxisAlignedBB(0.25D, 0.0D, 0.25D, 1.0D, 1.0D, 0.75D), new AxisAlignedBB(0.25D, 0.0D, 0.25D, 1.0D, 1.0D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.3125D, 1.0D, 0.875D, 0.6875D), new AxisAlignedBB(0.0D, 0.0D, 0.25D, 1.0D, 1.0D, 1.0D), new AxisAlignedBB(0.25D, 0.0D, 0.0D, 1.0D, 1.0D, 0.75D), new AxisAlignedBB(0.25D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.75D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D)};
   protected static final AxisAlignedBB[] B = new AxisAlignedBB[]{g[0].e(1.5D), g[1].e(1.5D), g[2].e(1.5D), g[3].e(1.5D), g[4].e(1.5D), g[5].e(1.5D), g[6].e(1.5D), g[7].e(1.5D), g[8].e(1.5D), g[9].e(1.5D), g[10].e(1.5D), g[11].e(1.5D), g[12].e(1.5D), g[13].e(1.5D), g[14].e(1.5D), g[15].e(1.5D)};

   public BlockCobbleWall(Block var1) {
      super(var1.x);
      this.w(this.A.b().set(a, Boolean.valueOf(false)).set(b, Boolean.valueOf(false)).set(c, Boolean.valueOf(false)).set(d, Boolean.valueOf(false)).set(e, Boolean.valueOf(false)).set(f, BlockCobbleWall.EnumCobbleVariant.NORMAL));
      this.c(var1.q);
      this.b(var1.r / 3.0F);
      this.a(var1.v);
      this.a(CreativeModeTab.b);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      var1 = this.b(var1, var2, var3);
      return g[i(var1)];
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      var1 = this.b(var1, var2, var3);
      return B[i(var1)];
   }

   private static int i(IBlockData var0) {
      int var1 = 0;
      if(((Boolean)var0.get(b)).booleanValue()) {
         var1 |= 1 << EnumDirection.NORTH.b();
      }

      if(((Boolean)var0.get(c)).booleanValue()) {
         var1 |= 1 << EnumDirection.EAST.b();
      }

      if(((Boolean)var0.get(d)).booleanValue()) {
         var1 |= 1 << EnumDirection.SOUTH.b();
      }

      if(((Boolean)var0.get(e)).booleanValue()) {
         var1 |= 1 << EnumDirection.WEST.b();
      }

      return var1;
   }

   public String c() {
      return class_di.a(this.a() + "." + BlockCobbleWall.EnumCobbleVariant.NORMAL.c() + ".name");
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(class_ahw var1, BlockPosition var2) {
      return false;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   private boolean c(class_ahw var1, BlockPosition var2) {
      IBlockData var3 = var1.getType(var2);
      Block var4 = var3.getBlock();
      return var4 == Blocks.cv?false:(var4 != this && !(var4 instanceof BlockFenceGate)?(var4.x.k() && var3.h()?var4.x != Material.C:false):true);
   }

   public int d(IBlockData var1) {
      return ((BlockCobbleWall.EnumCobbleVariant)var1.get(f)).a();
   }

   public IBlockData a(int var1) {
      return this.u().set(f, BlockCobbleWall.EnumCobbleVariant.a(var1));
   }

   public int e(IBlockData var1) {
      return ((BlockCobbleWall.EnumCobbleVariant)var1.get(f)).a();
   }

   public IBlockData b(IBlockData var1, class_ahw var2, BlockPosition var3) {
      boolean var4 = this.c(var2, var3.c());
      boolean var5 = this.c(var2, var3.f());
      boolean var6 = this.c(var2, var3.d());
      boolean var7 = this.c(var2, var3.e());
      boolean var8 = var4 && !var5 && var6 && !var7 || !var4 && var5 && !var6 && var7;
      return var1.set(a, Boolean.valueOf(!var8 || !var2.d(var3.a()))).set(b, Boolean.valueOf(var4)).set(c, Boolean.valueOf(var5)).set(d, Boolean.valueOf(var6)).set(e, Boolean.valueOf(var7));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b, c, e, d, f});
   }

   public static enum EnumCobbleVariant implements class_or {
      NORMAL(0, "cobblestone", "normal"),
      MOSSY(1, "mossy_cobblestone", "mossy");

      private static final BlockCobbleWall.EnumCobbleVariant[] c = new BlockCobbleWall.EnumCobbleVariant[values().length];
      private final int d;
      private final String e;
      private String f;

      private EnumCobbleVariant(int var3, String var4, String var5) {
         this.d = var3;
         this.e = var4;
         this.f = var5;
      }

      public int a() {
         return this.d;
      }

      public String toString() {
         return this.e;
      }

      public static BlockCobbleWall.EnumCobbleVariant a(int var0) {
         if(var0 < 0 || var0 >= c.length) {
            var0 = 0;
         }

         return c[var0];
      }

      public String m() {
         return this.e;
      }

      public String c() {
         return this.f;
      }

      static {
         BlockCobbleWall.EnumCobbleVariant[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            BlockCobbleWall.EnumCobbleVariant var3 = var0[var2];
            c[var3.a()] = var3;
         }

      }
   }
}
