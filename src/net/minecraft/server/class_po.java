package net.minecraft.server;

import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.class_ox;

public class class_po implements class_ox {
   public int a() {
      return 107;
   }

   public NBTTagCompound a(NBTTagCompound var1) {
      if(!"MobSpawner".equals(var1.l("id"))) {
         return var1;
      } else {
         if(var1.b("EntityId", 8)) {
            String var2 = var1.l("EntityId");
            NBTTagCompound var3 = var1.o("SpawnData");
            var3.a("id", var2.isEmpty()?"Pig":var2);
            var1.a((String)"SpawnData", (NBTTag)var3);
            var1.q("EntityId");
         }

         if(var1.b("SpawnPotentials", 9)) {
            NBTTagList var6 = var1.c("SpawnPotentials", 10);

            for(int var7 = 0; var7 < var6.c(); ++var7) {
               NBTTagCompound var4 = var6.b(var7);
               if(var4.b("Type", 8)) {
                  NBTTagCompound var5 = var4.o("Properties");
                  var5.a("id", var4.l("Type"));
                  var4.a((String)"Entity", (NBTTag)var5);
                  var4.q("Type");
                  var4.q("Properties");
               }
            }
         }

         return var1;
      }
   }
}
