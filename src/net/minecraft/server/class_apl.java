package net.minecraft.server;

import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.World;
import net.minecraft.server.EntityBoat;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ajx;
import net.minecraft.server.class_amn;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_apl extends class_ajx {
   protected static final AxisAlignedBB a = new AxisAlignedBB(0.0625D, 0.0D, 0.0625D, 0.9375D, 0.09375D, 0.9375D);

   protected class_apl() {
      this.a(CreativeModeTab.c);
   }

   public void a(IBlockData var1, World var2, BlockPosition var3, AxisAlignedBB var4, List var5, Entity var6) {
      if(!(var6 instanceof EntityBoat)) {
         a(var3, var4, var5, a);
      }

   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Entity var4) {
      super.a(var1, var2, var3, var4);
      if(var4 instanceof EntityBoat) {
         var1.b(new BlockPosition(var2), true);
      }

   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return a;
   }

   protected boolean i(IBlockData var1) {
      return var1.getBlock() == Blocks.j || var1.getMaterial() == Material.w;
   }

   public boolean f(World var1, BlockPosition var2, IBlockData var3) {
      if(var2.q() >= 0 && var2.q() < 256) {
         IBlockData var4 = var1.getType(var2.b());
         Material var5 = var4.getMaterial();
         return var5 == Material.h && ((Integer)var4.get(class_amn.b)).intValue() == 0 || var5 == Material.w;
      } else {
         return false;
      }
   }

   public int e(IBlockData var1) {
      return 0;
   }
}
