package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_zp;

public class class_adf extends Item {
   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      if(!var3.E) {
         class_zp var10 = new class_zp(var3, (double)((float)var4.p() + var7), (double)((float)var4.q() + var8), (double)((float)var4.r() + var9), var1);
         var3.a((Entity)var10);
         if(!var2.abilities.d) {
            --var1.b;
         }
      }

      return EnumResult.SUCCESS;
   }
}
