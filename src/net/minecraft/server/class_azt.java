package net.minecraft.server;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import java.util.Collection;
import java.util.Random;
import net.minecraft.server.class_azw;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_baq;

public class class_azt extends class_azw {
   public class_azt(int var1, int var2, class_baq[] var3) {
      super(var1, var2, var3);
   }

   public void a(Collection var1, Random var2, class_azy var3) {
   }

   protected void a(JsonObject var1, JsonSerializationContext var2) {
   }

   public static class_azt a(JsonObject var0, JsonDeserializationContext var1, int var2, int var3, class_baq[] var4) {
      return new class_azt(var2, var3, var4);
   }
}
