package net.minecraft.server;

import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.World;
import net.minecraft.server.class_ayl;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_to;
import net.minecraft.server.class_tv;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_uu;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_yp;
import net.minecraft.server.class_ys;
import net.minecraft.server.class_zu;

public class class_yf extends class_yp {
   private float a = 0.5F;
   private int b;
   private static final class_ke c = DataWatcher.a(class_yf.class, class_kg.a);

   public class_yf(World var1) {
      super(var1);
      this.a(class_ayl.WATER, -1.0F);
      this.a(class_ayl.LAVA, 8.0F);
      this.a(class_ayl.DANGER_FIRE, 0.0F);
      this.a(class_ayl.DAMAGE_FIRE, 0.0F);
      this.fireProof = true;
      this.b_ = 10;
   }

   protected void r() {
      this.bp.a(4, new class_yf.class_a_in_class_yf(this));
      this.bp.a(5, new class_tv(this, 1.0D));
      this.bp.a(7, new class_uf(this, 1.0D));
      this.bp.a(8, new class_to(this, EntityHuman.class, 8.0F));
      this.bp.a(8, new class_ue(this));
      this.bq.a(1, new class_uu(this, true, new Class[0]));
      this.bq.a(2, new class_ux(this, EntityHuman.class, true));
   }

   protected void bz() {
      super.bz();
      this.a(class_ys.e).a(6.0D);
      this.a(class_ys.d).a(0.23000000417232513D);
      this.a(class_ys.b).a(48.0D);
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)c, (Object)Byte.valueOf((byte)0));
   }

   protected class_nf G() {
      return class_ng.A;
   }

   protected class_nf bQ() {
      return class_ng.D;
   }

   protected class_nf bR() {
      return class_ng.C;
   }

   public float e(float var1) {
      return 1.0F;
   }

   public void n() {
      if(!this.onGround && this.motY < 0.0D) {
         this.motY *= 0.6D;
      }

      if(this.world.E) {
         if(this.random.nextInt(24) == 0 && !this.ac()) {
            this.world.a(this.locX + 0.5D, this.locY + 0.5D, this.locZ + 0.5D, class_ng.B, this.by(), 1.0F + this.random.nextFloat(), this.random.nextFloat() * 0.7F + 0.3F, false);
         }

         for(int var1 = 0; var1 < 2; ++var1) {
            this.world.a(EnumParticle.SMOKE_LARGE, this.locX + (this.random.nextDouble() - 0.5D) * (double)this.width, this.locY + this.random.nextDouble() * (double)this.length, this.locZ + (this.random.nextDouble() - 0.5D) * (double)this.width, 0.0D, 0.0D, 0.0D, new int[0]);
         }
      }

      super.n();
   }

   protected void M() {
      if(this.ag()) {
         this.a(DamageSource.f, 1.0F);
      }

      --this.b;
      if(this.b <= 0) {
         this.b = 100;
         this.a = 0.5F + (float)this.random.nextGaussian() * 3.0F;
      }

      class_rz var1 = this.A();
      if(var1 != null && var1.locY + (double)var1.bm() > this.locY + (double)this.bm() + (double)this.a) {
         this.motY += (0.30000001192092896D - this.motY) * 0.30000001192092896D;
         this.ai = true;
      }

      super.M();
   }

   public void e(float var1, float var2) {
   }

   public boolean aG() {
      return this.o();
   }

   protected class_kk J() {
      return class_azs.o;
   }

   public boolean o() {
      return (((Byte)this.datawatcher.a(c)).byteValue() & 1) != 0;
   }

   public void a(boolean var1) {
      byte var2 = ((Byte)this.datawatcher.a(c)).byteValue();
      if(var1) {
         var2 = (byte)(var2 | 1);
      } else {
         var2 &= -2;
      }

      this.datawatcher.b(c, Byte.valueOf(var2));
   }

   protected boolean s_() {
      return true;
   }

   static class class_a_in_class_yf extends class_tj {
      private class_yf a;
      private int b;
      private int c;

      public class_a_in_class_yf(class_yf var1) {
         this.a = var1;
         this.a(3);
      }

      public boolean a() {
         class_rz var1 = this.a.A();
         return var1 != null && var1.at();
      }

      public void c() {
         this.b = 0;
      }

      public void d() {
         this.a.a(false);
      }

      public void e() {
         --this.c;
         class_rz var1 = this.a.A();
         double var2 = this.a.h(var1);
         if(var2 < 4.0D) {
            if(this.c <= 0) {
               this.c = 20;
               this.a.B(var1);
            }

            this.a.u().a(var1.locX, var1.locY, var1.locZ, 1.0D);
         } else if(var2 < 256.0D) {
            double var4 = var1.locX - this.a.locX;
            double var6 = var1.bk().b + (double)(var1.length / 2.0F) - (this.a.locY + (double)(this.a.length / 2.0F));
            double var8 = var1.locZ - this.a.locZ;
            if(this.c <= 0) {
               ++this.b;
               if(this.b == 1) {
                  this.c = 60;
                  this.a.a(true);
               } else if(this.b <= 4) {
                  this.c = 6;
               } else {
                  this.c = 100;
                  this.b = 0;
                  this.a.a(false);
               }

               if(this.b > 1) {
                  float var10 = MathHelper.c(MathHelper.a(var2)) * 0.5F;
                  this.a.world.a((EntityHuman)null, 1018, new BlockPosition((int)this.a.locX, (int)this.a.locY, (int)this.a.locZ), 0);

                  for(int var11 = 0; var11 < 1; ++var11) {
                     class_zu var12 = new class_zu(this.a.world, this.a, var4 + this.a.bE().nextGaussian() * (double)var10, var6, var8 + this.a.bE().nextGaussian() * (double)var10);
                     var12.locY = this.a.locY + (double)(this.a.length / 2.0F) + 0.5D;
                     this.a.world.a((Entity)var12);
                  }
               }
            }

            this.a.t().a(var1, 10.0F, 10.0F);
         } else {
            this.a.x().o();
            this.a.u().a(var1.locX, var1.locY, var1.locZ, 1.0D);
         }

         super.e();
      }
   }
}
