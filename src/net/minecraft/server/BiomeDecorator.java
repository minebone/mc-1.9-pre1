package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.BlockFlowers;
import net.minecraft.server.BlockStone;
import net.minecraft.server.Blocks;
import net.minecraft.server.CustomWorldSettingsFinal;
import net.minecraft.server.World;
import net.minecraft.server.class_ato;
import net.minecraft.server.class_att;
import net.minecraft.server.class_atu;
import net.minecraft.server.class_atv;
import net.minecraft.server.class_atw;
import net.minecraft.server.class_auc;
import net.minecraft.server.class_aud;
import net.minecraft.server.class_aui;
import net.minecraft.server.class_aus;
import net.minecraft.server.class_auu;
import net.minecraft.server.class_auv;
import net.minecraft.server.class_auy;
import net.minecraft.server.class_avb;
import net.minecraft.server.class_avh;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class BiomeDecorator {
   protected boolean a;
   protected BlockPosition b;
   protected CustomWorldSettingsFinal c;
   protected class_auc d = new class_atv(4);
   protected class_auc e = new class_auy(Blocks.m, 7);
   protected class_auc f = new class_auy(Blocks.n, 6);
   protected class_auc g;
   protected class_auc h;
   protected class_auc i;
   protected class_auc j;
   protected class_auc k;
   protected class_auc l;
   protected class_auc m;
   protected class_auc n;
   protected class_auc o;
   protected class_auc p;
   protected class_auc q;
   protected class_aud r = new class_aud(Blocks.N, BlockFlowers.EnumFlowerVarient.DANDELION);
   protected class_auc s = new class_att(Blocks.P);
   protected class_auc t = new class_att(Blocks.Q);
   protected class_auc u = new class_aui();
   protected class_auc v = new class_auv();
   protected class_auc w = new class_atu();
   protected class_auc x = new class_avh();
   protected int y;
   protected int z;
   protected int A = 2;
   protected int B = 1;
   protected int C;
   protected int D;
   protected int E;
   protected int F;
   protected int G = 1;
   protected int H = 3;
   protected int I = 1;
   protected int J;
   public boolean K = true;

   public void a(World var1, Random var2, BiomeBase var3, BlockPosition var4) {
      if(this.a) {
         throw new RuntimeException("Already decorating");
      } else {
         this.c = CustomWorldSettingsFinal.class_a_in_class_atf.a(var1.T().A()).b();
         this.b = var4;
         this.g = new class_aus(Blocks.d.u(), this.c.I);
         this.h = new class_aus(Blocks.n.u(), this.c.M);
         this.i = new class_aus(Blocks.b.u().set(BlockStone.a, BlockStone.EnumStoneVariant.GRANITE), this.c.Q);
         this.j = new class_aus(Blocks.b.u().set(BlockStone.a, BlockStone.EnumStoneVariant.DIORITE), this.c.U);
         this.k = new class_aus(Blocks.b.u().set(BlockStone.a, BlockStone.EnumStoneVariant.ANDESITE), this.c.Y);
         this.l = new class_aus(Blocks.q.u(), this.c.ac);
         this.m = new class_aus(Blocks.p.u(), this.c.ag);
         this.n = new class_aus(Blocks.o.u(), this.c.ak);
         this.o = new class_aus(Blocks.aC.u(), this.c.ao);
         this.p = new class_aus(Blocks.ag.u(), this.c.as);
         this.q = new class_aus(Blocks.x.u(), this.c.aw);
         this.a(var3, var1, var2);
         this.a = false;
      }
   }

   protected void a(BiomeBase var1, World var2, Random var3) {
      this.a(var2, var3);

      int var4;
      int var5;
      int var6;
      for(var4 = 0; var4 < this.H; ++var4) {
         var5 = var3.nextInt(16) + 8;
         var6 = var3.nextInt(16) + 8;
         this.e.b(var2, var3, var2.q(this.b.a(var5, 0, var6)));
      }

      for(var4 = 0; var4 < this.I; ++var4) {
         var5 = var3.nextInt(16) + 8;
         var6 = var3.nextInt(16) + 8;
         this.d.b(var2, var3, var2.q(this.b.a(var5, 0, var6)));
      }

      for(var4 = 0; var4 < this.G; ++var4) {
         var5 = var3.nextInt(16) + 8;
         var6 = var3.nextInt(16) + 8;
         this.f.b(var2, var3, var2.q(this.b.a(var5, 0, var6)));
      }

      var4 = this.z;
      if(var3.nextInt(10) == 0) {
         ++var4;
      }

      int var7;
      BlockPosition var9;
      for(var5 = 0; var5 < var4; ++var5) {
         var6 = var3.nextInt(16) + 8;
         var7 = var3.nextInt(16) + 8;
         class_ato var8 = var1.a(var3);
         var8.e();
         var9 = var2.l(this.b.a(var6, 0, var7));
         if(var8.b(var2, var3, var9)) {
            var8.a(var2, var3, var9);
         }
      }

      for(var5 = 0; var5 < this.J; ++var5) {
         var6 = var3.nextInt(16) + 8;
         var7 = var3.nextInt(16) + 8;
         this.u.b(var2, var3, var2.l(this.b.a(var6, 0, var7)));
      }

      BlockPosition var10;
      int var13;
      int var14;
      for(var5 = 0; var5 < this.A; ++var5) {
         var6 = var3.nextInt(16) + 8;
         var7 = var3.nextInt(16) + 8;
         var13 = var2.l(this.b.a(var6, 0, var7)).q() + 32;
         if(var13 > 0) {
            var14 = var3.nextInt(var13);
            var10 = this.b.a(var6, var14, var7);
            BlockFlowers.EnumFlowerVarient var11 = var1.a(var3, var10);
            BlockFlowers var12 = var11.a().a();
            if(var12.u().getMaterial() != Material.a) {
               this.r.a(var12, var11);
               this.r.b(var2, var3, var10);
            }
         }
      }

      for(var5 = 0; var5 < this.B; ++var5) {
         var6 = var3.nextInt(16) + 8;
         var7 = var3.nextInt(16) + 8;
         var13 = var2.l(this.b.a(var6, 0, var7)).q() * 2;
         if(var13 > 0) {
            var14 = var3.nextInt(var13);
            var1.b(var3).b(var2, var3, this.b.a(var6, var14, var7));
         }
      }

      for(var5 = 0; var5 < this.C; ++var5) {
         var6 = var3.nextInt(16) + 8;
         var7 = var3.nextInt(16) + 8;
         var13 = var2.l(this.b.a(var6, 0, var7)).q() * 2;
         if(var13 > 0) {
            var14 = var3.nextInt(var13);
            (new class_atw()).b(var2, var3, this.b.a(var6, var14, var7));
         }
      }

      for(var5 = 0; var5 < this.y; ++var5) {
         var6 = var3.nextInt(16) + 8;
         var7 = var3.nextInt(16) + 8;
         var13 = var2.l(this.b.a(var6, 0, var7)).q() * 2;
         if(var13 > 0) {
            var14 = var3.nextInt(var13);

            BlockPosition var16;
            for(var10 = this.b.a(var6, var14, var7); var10.q() > 0; var10 = var16) {
               var16 = var10.b();
               if(!var2.d(var16)) {
                  break;
               }
            }

            this.x.b(var2, var3, var10);
         }
      }

      for(var5 = 0; var5 < this.D; ++var5) {
         if(var3.nextInt(4) == 0) {
            var6 = var3.nextInt(16) + 8;
            var7 = var3.nextInt(16) + 8;
            BlockPosition var15 = var2.l(this.b.a(var6, 0, var7));
            this.s.b(var2, var3, var15);
         }

         if(var3.nextInt(8) == 0) {
            var6 = var3.nextInt(16) + 8;
            var7 = var3.nextInt(16) + 8;
            var13 = var2.l(this.b.a(var6, 0, var7)).q() * 2;
            if(var13 > 0) {
               var14 = var3.nextInt(var13);
               var10 = this.b.a(var6, var14, var7);
               this.t.b(var2, var3, var10);
            }
         }
      }

      if(var3.nextInt(4) == 0) {
         var5 = var3.nextInt(16) + 8;
         var6 = var3.nextInt(16) + 8;
         var7 = var2.l(this.b.a(var5, 0, var6)).q() * 2;
         if(var7 > 0) {
            var13 = var3.nextInt(var7);
            this.s.b(var2, var3, this.b.a(var5, var13, var6));
         }
      }

      if(var3.nextInt(8) == 0) {
         var5 = var3.nextInt(16) + 8;
         var6 = var3.nextInt(16) + 8;
         var7 = var2.l(this.b.a(var5, 0, var6)).q() * 2;
         if(var7 > 0) {
            var13 = var3.nextInt(var7);
            this.t.b(var2, var3, this.b.a(var5, var13, var6));
         }
      }

      for(var5 = 0; var5 < this.E; ++var5) {
         var6 = var3.nextInt(16) + 8;
         var7 = var3.nextInt(16) + 8;
         var13 = var2.l(this.b.a(var6, 0, var7)).q() * 2;
         if(var13 > 0) {
            var14 = var3.nextInt(var13);
            this.v.b(var2, var3, this.b.a(var6, var14, var7));
         }
      }

      for(var5 = 0; var5 < 10; ++var5) {
         var6 = var3.nextInt(16) + 8;
         var7 = var3.nextInt(16) + 8;
         var13 = var2.l(this.b.a(var6, 0, var7)).q() * 2;
         if(var13 > 0) {
            var14 = var3.nextInt(var13);
            this.v.b(var2, var3, this.b.a(var6, var14, var7));
         }
      }

      if(var3.nextInt(32) == 0) {
         var5 = var3.nextInt(16) + 8;
         var6 = var3.nextInt(16) + 8;
         var7 = var2.l(this.b.a(var5, 0, var6)).q() * 2;
         if(var7 > 0) {
            var13 = var3.nextInt(var7);
            (new class_auu()).b(var2, var3, this.b.a(var5, var13, var6));
         }
      }

      for(var5 = 0; var5 < this.F; ++var5) {
         var6 = var3.nextInt(16) + 8;
         var7 = var3.nextInt(16) + 8;
         var13 = var2.l(this.b.a(var6, 0, var7)).q() * 2;
         if(var13 > 0) {
            var14 = var3.nextInt(var13);
            this.w.b(var2, var3, this.b.a(var6, var14, var7));
         }
      }

      if(this.K) {
         for(var5 = 0; var5 < 50; ++var5) {
            var6 = var3.nextInt(16) + 8;
            var7 = var3.nextInt(16) + 8;
            var13 = var3.nextInt(248) + 8;
            if(var13 > 0) {
               var14 = var3.nextInt(var13);
               var10 = this.b.a(var6, var14, var7);
               (new class_avb(Blocks.i)).b(var2, var3, var10);
            }
         }

         for(var5 = 0; var5 < 20; ++var5) {
            var6 = var3.nextInt(16) + 8;
            var7 = var3.nextInt(16) + 8;
            var13 = var3.nextInt(var3.nextInt(var3.nextInt(240) + 8) + 8);
            var9 = this.b.a(var6, var13, var7);
            (new class_avb(Blocks.k)).b(var2, var3, var9);
         }
      }

   }

   protected void a(World var1, Random var2) {
      this.a(var1, var2, this.c.J, this.g, this.c.K, this.c.L);
      this.a(var1, var2, this.c.N, this.h, this.c.O, this.c.P);
      this.a(var1, var2, this.c.V, this.j, this.c.W, this.c.X);
      this.a(var1, var2, this.c.R, this.i, this.c.S, this.c.T);
      this.a(var1, var2, this.c.Z, this.k, this.c.aa, this.c.ab);
      this.a(var1, var2, this.c.ad, this.l, this.c.ae, this.c.af);
      this.a(var1, var2, this.c.ah, this.m, this.c.ai, this.c.aj);
      this.a(var1, var2, this.c.al, this.n, this.c.am, this.c.an);
      this.a(var1, var2, this.c.ap, this.o, this.c.aq, this.c.ar);
      this.a(var1, var2, this.c.at, this.p, this.c.au, this.c.av);
      this.b(var1, var2, this.c.ax, this.q, this.c.ay, this.c.az);
   }

   protected void a(World var1, Random var2, int var3, class_auc var4, int var5, int var6) {
      int var7;
      if(var6 < var5) {
         var7 = var5;
         var5 = var6;
         var6 = var7;
      } else if(var6 == var5) {
         if(var5 < 255) {
            ++var6;
         } else {
            --var5;
         }
      }

      for(var7 = 0; var7 < var3; ++var7) {
         BlockPosition var8 = this.b.a(var2.nextInt(16), var2.nextInt(var6 - var5) + var5, var2.nextInt(16));
         var4.b(var1, var2, var8);
      }

   }

   protected void b(World var1, Random var2, int var3, class_auc var4, int var5, int var6) {
      for(int var7 = 0; var7 < var3; ++var7) {
         BlockPosition var8 = this.b.a(var2.nextInt(16), var2.nextInt(var6) + var2.nextInt(var6) + var5 - var6, var2.nextInt(16));
         var4.b(var1, var2, var8);
      }

   }
}
