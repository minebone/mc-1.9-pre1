package net.minecraft.server;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import net.minecraft.server.StructureGenerator;
import net.minecraft.server.StructureStart;
import net.minecraft.server.class_avt;
import net.minecraft.server.MathHelper;

public class WorldGenMineshaft extends StructureGenerator {
   private double a = 0.004D;

   public WorldGenMineshaft() {
   }

   public String a() {
      return "Mineshaft";
   }

   public WorldGenMineshaft(Map var1) {
      Iterator var2 = var1.entrySet().iterator();

      while(var2.hasNext()) {
         Entry var3 = (Entry)var2.next();
         if(((String)var3.getKey()).equals("chance")) {
            this.a = MathHelper.a((String)var3.getValue(), this.a);
         }
      }

   }

   protected boolean a(int var1, int var2) {
      return this.f.nextDouble() < this.a && this.f.nextInt(80) < Math.max(Math.abs(var1), Math.abs(var2));
   }

   protected StructureStart b(int var1, int var2) {
      return new class_avt(this.g, this.f, var1, var2);
   }
}
