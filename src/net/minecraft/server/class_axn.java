package net.minecraft.server;

import net.minecraft.server.BiomeBase;
import net.minecraft.server.IntCache;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_axu;

public class class_axn extends class_axu {
   public class_axn(long var1, class_axu var3) {
      super(var1);
      this.a = var3;
   }

   public int[] a(int var1, int var2, int var3, int var4) {
      int[] var5 = this.a.a(var1 - 1, var2 - 1, var3 + 2, var4 + 2);
      int[] var6 = IntCache.a(var3 * var4);

      for(int var7 = 0; var7 < var4; ++var7) {
         for(int var8 = 0; var8 < var3; ++var8) {
            this.a((long)(var8 + var1), (long)(var7 + var2));
            int var9 = var5[var8 + 1 + (var7 + 1) * (var3 + 2)];
            if(!this.a(var5, var6, var8, var7, var3, var9, BiomeBase.a(class_aik.e), BiomeBase.a(class_aik.v)) && !this.b(var5, var6, var8, var7, var3, var9, BiomeBase.a(class_aik.N), BiomeBase.a(class_aik.M)) && !this.b(var5, var6, var8, var7, var3, var9, BiomeBase.a(class_aik.O), BiomeBase.a(class_aik.M)) && !this.b(var5, var6, var8, var7, var3, var9, BiomeBase.a(class_aik.H), BiomeBase.a(class_aik.g))) {
               int var10;
               int var11;
               int var12;
               int var13;
               if(var9 == BiomeBase.a(class_aik.d)) {
                  var10 = var5[var8 + 1 + (var7 + 1 - 1) * (var3 + 2)];
                  var11 = var5[var8 + 1 + 1 + (var7 + 1) * (var3 + 2)];
                  var12 = var5[var8 + 1 - 1 + (var7 + 1) * (var3 + 2)];
                  var13 = var5[var8 + 1 + (var7 + 1 + 1) * (var3 + 2)];
                  if(var10 != BiomeBase.a(class_aik.n) && var11 != BiomeBase.a(class_aik.n) && var12 != BiomeBase.a(class_aik.n) && var13 != BiomeBase.a(class_aik.n)) {
                     var6[var8 + var7 * var3] = var9;
                  } else {
                     var6[var8 + var7 * var3] = BiomeBase.a(class_aik.J);
                  }
               } else if(var9 == BiomeBase.a(class_aik.h)) {
                  var10 = var5[var8 + 1 + (var7 + 1 - 1) * (var3 + 2)];
                  var11 = var5[var8 + 1 + 1 + (var7 + 1) * (var3 + 2)];
                  var12 = var5[var8 + 1 - 1 + (var7 + 1) * (var3 + 2)];
                  var13 = var5[var8 + 1 + (var7 + 1 + 1) * (var3 + 2)];
                  if(var10 != BiomeBase.a(class_aik.d) && var11 != BiomeBase.a(class_aik.d) && var12 != BiomeBase.a(class_aik.d) && var13 != BiomeBase.a(class_aik.d) && var10 != BiomeBase.a(class_aik.F) && var11 != BiomeBase.a(class_aik.F) && var12 != BiomeBase.a(class_aik.F) && var13 != BiomeBase.a(class_aik.F) && var10 != BiomeBase.a(class_aik.n) && var11 != BiomeBase.a(class_aik.n) && var12 != BiomeBase.a(class_aik.n) && var13 != BiomeBase.a(class_aik.n)) {
                     if(var10 != BiomeBase.a(class_aik.w) && var13 != BiomeBase.a(class_aik.w) && var11 != BiomeBase.a(class_aik.w) && var12 != BiomeBase.a(class_aik.w)) {
                        var6[var8 + var7 * var3] = var9;
                     } else {
                        var6[var8 + var7 * var3] = BiomeBase.a(class_aik.y);
                     }
                  } else {
                     var6[var8 + var7 * var3] = BiomeBase.a(class_aik.c);
                  }
               } else {
                  var6[var8 + var7 * var3] = var9;
               }
            }
         }
      }

      return var6;
   }

   private boolean a(int[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8) {
      if(!a(var6, var7)) {
         return false;
      } else {
         int var9 = var1[var3 + 1 + (var4 + 1 - 1) * (var5 + 2)];
         int var10 = var1[var3 + 1 + 1 + (var4 + 1) * (var5 + 2)];
         int var11 = var1[var3 + 1 - 1 + (var4 + 1) * (var5 + 2)];
         int var12 = var1[var3 + 1 + (var4 + 1 + 1) * (var5 + 2)];
         if(this.b(var9, var7) && this.b(var10, var7) && this.b(var11, var7) && this.b(var12, var7)) {
            var2[var3 + var4 * var5] = var6;
         } else {
            var2[var3 + var4 * var5] = var8;
         }

         return true;
      }
   }

   private boolean b(int[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8) {
      if(var6 != var7) {
         return false;
      } else {
         int var9 = var1[var3 + 1 + (var4 + 1 - 1) * (var5 + 2)];
         int var10 = var1[var3 + 1 + 1 + (var4 + 1) * (var5 + 2)];
         int var11 = var1[var3 + 1 - 1 + (var4 + 1) * (var5 + 2)];
         int var12 = var1[var3 + 1 + (var4 + 1 + 1) * (var5 + 2)];
         if(a(var9, var7) && a(var10, var7) && a(var11, var7) && a(var12, var7)) {
            var2[var3 + var4 * var5] = var6;
         } else {
            var2[var3 + var4 * var5] = var8;
         }

         return true;
      }
   }

   private boolean b(int var1, int var2) {
      if(a(var1, var2)) {
         return true;
      } else {
         BiomeBase var3 = BiomeBase.b(var1);
         BiomeBase var4 = BiomeBase.b(var2);
         if(var3 != null && var4 != null) {
            BiomeBase.EnumTemperature var5 = var3.h();
            BiomeBase.EnumTemperature var6 = var4.h();
            return var5 == var6 || var5 == BiomeBase.EnumTemperature.MEDIUM || var6 == BiomeBase.EnumTemperature.MEDIUM;
         } else {
            return false;
         }
      }
   }
}
