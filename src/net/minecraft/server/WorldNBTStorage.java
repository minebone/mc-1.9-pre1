package net.minecraft.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTCompressedStreamTools;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.WorldData;
import net.minecraft.server.class_aht;
import net.minecraft.server.class_asl;
import net.minecraft.server.class_asu;
import net.minecraft.server.class_awl;
import net.minecraft.server.class_azf;
import net.minecraft.server.class_azh;
import net.minecraft.server.class_azp;
import net.minecraft.server.class_oy;
import net.minecraft.server.class_oz;
import net.minecraft.server.class_pb;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WorldNBTStorage implements class_azh, class_azp {
   private static final Logger b = LogManager.getLogger();
   private final File c;
   private final File d;
   private final File e;
   private final long f = MinecraftServer.av();
   private final String g;
   private final class_awl h;
   protected final class_pb a;

   public WorldNBTStorage(File var1, String var2, boolean var3, class_pb var4) {
      this.a = var4;
      this.c = new File(var1, var2);
      this.c.mkdirs();
      this.d = new File(this.c, "playerdata");
      this.e = new File(this.c, "data");
      this.e.mkdirs();
      this.g = var2;
      if(var3) {
         this.d.mkdirs();
         this.h = new class_awl((new File(this.c, "structures")).toString());
      } else {
         this.h = null;
      }

      this.i();
   }

   private void i() {
      try {
         File var1 = new File(this.c, "session.lock");
         DataOutputStream var2 = new DataOutputStream(new FileOutputStream(var1));

         try {
            var2.writeLong(this.f);
         } finally {
            var2.close();
         }

      } catch (IOException var7) {
         var7.printStackTrace();
         throw new RuntimeException("Failed to check session lock, aborting");
      }
   }

   public File b() {
      return this.c;
   }

   public void c() throws class_aht {
      try {
         File var1 = new File(this.c, "session.lock");
         DataInputStream var2 = new DataInputStream(new FileInputStream(var1));

         try {
            if(var2.readLong() != this.f) {
               throw new class_aht("The save is being accessed from another location, aborting");
            }
         } finally {
            var2.close();
         }

      } catch (IOException var7) {
         throw new class_aht("Failed to check session lock, aborting");
      }
   }

   public class_asl a(class_asu var1) {
      throw new RuntimeException("Old Chunk Storage is no longer supported.");
   }

   public WorldData d() {
      File var1 = new File(this.c, "level.dat");
      if(var1.exists()) {
         WorldData var2 = class_azf.a(var1, this.a);
         if(var2 != null) {
            return var2;
         }
      }

      var1 = new File(this.c, "level.dat_old");
      return var1.exists()?class_azf.a(var1, this.a):null;
   }

   public void a(WorldData var1, NBTTagCompound var2) {
      NBTTagCompound var3 = var1.a(var2);
      NBTTagCompound var4 = new NBTTagCompound();
      var4.a((String)"Data", (NBTTag)var3);

      try {
         File var5 = new File(this.c, "level.dat_new");
         File var6 = new File(this.c, "level.dat_old");
         File var7 = new File(this.c, "level.dat");
         NBTCompressedStreamTools.a((NBTTagCompound)var4, (OutputStream)(new FileOutputStream(var5)));
         if(var6.exists()) {
            var6.delete();
         }

         var7.renameTo(var6);
         if(var7.exists()) {
            var7.delete();
         }

         var5.renameTo(var7);
         if(var5.exists()) {
            var5.delete();
         }
      } catch (Exception var8) {
         var8.printStackTrace();
      }

   }

   public void a(WorldData var1) {
      this.a(var1, (NBTTagCompound)null);
   }

   public void a(EntityHuman var1) {
      try {
         NBTTagCompound var2 = new NBTTagCompound();
         var1.e(var2);
         File var3 = new File(this.d, var1.getUniqueId().toString() + ".dat.tmp");
         File var4 = new File(this.d, var1.getUniqueId().toString() + ".dat");
         NBTCompressedStreamTools.a((NBTTagCompound)var2, (OutputStream)(new FileOutputStream(var3)));
         if(var4.exists()) {
            var4.delete();
         }

         var3.renameTo(var4);
      } catch (Exception var5) {
         b.warn("Failed to save player data for " + var1.h_());
      }

   }

   public NBTTagCompound b(EntityHuman var1) {
      NBTTagCompound var2 = null;

      try {
         File var3 = new File(this.d, var1.getUniqueId().toString() + ".dat");
         if(var3.exists() && var3.isFile()) {
            var2 = NBTCompressedStreamTools.a((InputStream)(new FileInputStream(var3)));
         }
      } catch (Exception var4) {
         b.warn("Failed to load player data for " + var1.h_());
      }

      if(var2 != null) {
         var1.f(this.a.a((class_oy)class_oz.PLAYER, (NBTTagCompound)var2));
      }

      return var2;
   }

   public class_azp e() {
      return this;
   }

   public String[] f() {
      String[] var1 = this.d.list();
      if(var1 == null) {
         var1 = new String[0];
      }

      for(int var2 = 0; var2 < var1.length; ++var2) {
         if(var1[var2].endsWith(".dat")) {
            var1[var2] = var1[var2].substring(0, var1[var2].length() - 4);
         }
      }

      return var1;
   }

   public void a() {
   }

   public File a(String var1) {
      return new File(this.e, var1 + ".dat");
   }

   public class_awl h() {
      return this.h;
   }
}
