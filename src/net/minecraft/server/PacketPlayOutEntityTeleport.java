package net.minecraft.server;

import java.io.IOException;
import net.minecraft.server.Entity;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketListenerPlayOut;

/*TODO: Packet changed*/
public class PacketPlayOutEntityTeleport implements Packet {
   private int a;
   private double b;
   private double c;
   private double d;
   private byte e;
   private byte f;
   private boolean g;

   public PacketPlayOutEntityTeleport() {
   }

   public PacketPlayOutEntityTeleport(Entity var1) {
      this.a = var1.getId();
      this.b = var1.locX;
      this.c = var1.locY;
      this.d = var1.locZ;
      this.e = (byte)((int)(var1.yaw * 256.0F / 360.0F));
      this.f = (byte)((int)(var1.pitch * 256.0F / 360.0F));
      this.g = var1.onGround;
   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.readVarInt();
      this.b = var1.readDouble();
      this.c = var1.readDouble();
      this.d = var1.readDouble();
      this.e = var1.readByte();
      this.f = var1.readByte();
      this.g = var1.readBoolean();
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.writeVarInt(this.a);
      var1.writeDouble(this.b);
      var1.writeDouble(this.c);
      var1.writeDouble(this.d);
      var1.writeByte(this.e);
      var1.writeByte(this.f);
      var1.writeBoolean(this.g);
   }

   public void a(PacketListenerPlayOut var1) {
      var1.a(this);
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketListenerPlayOut)var1);
   }
}
