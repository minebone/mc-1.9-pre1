package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.class_axg;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_or;

public abstract class BlockMinecartTrackAbstract extends Block {
   protected static final AxisAlignedBB a = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.125D, 1.0D);
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.15625D, 1.0D);
   protected final boolean c;

   public static boolean b(World var0, BlockPosition var1) {
      return i(var0.getType(var1));
   }

   public static boolean i(IBlockData var0) {
      Block var1 = var0.getBlock();
      return var1 == Blocks.av || var1 == Blocks.D || var1 == Blocks.E || var1 == Blocks.cs;
   }

   protected BlockMinecartTrackAbstract(boolean var1) {
      super(Material.q);
      this.c = var1;
      this.a(CreativeModeTab.e);
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      BlockMinecartTrackAbstract.EnumTrackPosition var4 = var1.getBlock() == this?(BlockMinecartTrackAbstract.EnumTrackPosition)var1.get(this.g()):null;
      return var4 != null && var4.c()?b:a;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean a(World var1, BlockPosition var2) {
      return var1.getType(var2.b()).q();
   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      if(!var1.E) {
         var3 = this.a(var1, var2, var3, true);
         if(this.c) {
            this.a(var1, var2, var3, this);
         }
      }

   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!var1.E) {
         BlockMinecartTrackAbstract.EnumTrackPosition var5 = (BlockMinecartTrackAbstract.EnumTrackPosition)var3.get(this.g());
         boolean var6 = false;
         if(!var1.getType(var2.b()).q()) {
            var6 = true;
         }

         if(var5 == BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST && !var1.getType(var2.f()).q()) {
            var6 = true;
         } else if(var5 == BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST && !var1.getType(var2.e()).q()) {
            var6 = true;
         } else if(var5 == BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH && !var1.getType(var2.c()).q()) {
            var6 = true;
         } else if(var5 == BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH && !var1.getType(var2.d()).q()) {
            var6 = true;
         }

         if(var6 && !var1.d(var2)) {
            this.b(var1, var2, var3, 0);
            var1.g(var2);
         } else {
            this.b(var1, var2, var3, var4);
         }

      }
   }

   protected void b(World var1, BlockPosition var2, IBlockData var3, Block var4) {
   }

   protected IBlockData a(World var1, BlockPosition var2, IBlockData var3, boolean var4) {
      return var1.E?var3:(new BlockMinecartTrackAbstract.class_a_in_class_ajo(var1, var2, var3)).a(var1.y(var2), var4).c();
   }

   public class_axg h(IBlockData var1) {
      return class_axg.NORMAL;
   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      super.b(var1, var2, var3);
      if(((BlockMinecartTrackAbstract.EnumTrackPosition)var3.get(this.g())).c()) {
         var1.d(var2.a(), this);
      }

      if(this.c) {
         var1.d(var2, this);
         var1.d(var2.b(), this);
      }

   }

   public abstract IBlockState g();

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[BlockMinecartTrackAbstract.EnumTrackPosition.values().length];

      static {
         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH.ordinal()] = 1;
         } catch (NoSuchFieldError var10) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST.ordinal()] = 2;
         } catch (NoSuchFieldError var9) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST.ordinal()] = 3;
         } catch (NoSuchFieldError var8) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST.ordinal()] = 4;
         } catch (NoSuchFieldError var7) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH.ordinal()] = 5;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH.ordinal()] = 6;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST.ordinal()] = 7;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST.ordinal()] = 8;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST.ordinal()] = 9;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST.ordinal()] = 10;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum EnumTrackPosition implements class_or {
      NORTH_SOUTH(0, "north_south"),
      EAST_WEST(1, "east_west"),
      ASCENDING_EAST(2, "ascending_east"),
      ASCENDING_WEST(3, "ascending_west"),
      ASCENDING_NORTH(4, "ascending_north"),
      ASCENDING_SOUTH(5, "ascending_south"),
      SOUTH_EAST(6, "south_east"),
      SOUTH_WEST(7, "south_west"),
      NORTH_WEST(8, "north_west"),
      NORTH_EAST(9, "north_east");

      private static final BlockMinecartTrackAbstract.EnumTrackPosition[] k = new BlockMinecartTrackAbstract.EnumTrackPosition[values().length];
      private final int l;
      private final String m;

      private EnumTrackPosition(int var3, String var4) {
         this.l = var3;
         this.m = var4;
      }

      public int a() {
         return this.l;
      }

      public String toString() {
         return this.m;
      }

      public boolean c() {
         return this == ASCENDING_NORTH || this == ASCENDING_EAST || this == ASCENDING_SOUTH || this == ASCENDING_WEST;
      }

      public static BlockMinecartTrackAbstract.EnumTrackPosition a(int var0) {
         if(var0 < 0 || var0 >= k.length) {
            var0 = 0;
         }

         return k[var0];
      }

      public String m() {
         return this.m;
      }

      static {
         BlockMinecartTrackAbstract.EnumTrackPosition[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            BlockMinecartTrackAbstract.EnumTrackPosition var3 = var0[var2];
            k[var3.a()] = var3;
         }

      }
   }

   public class class_a_in_class_ajo {
      private final World b;
      private final BlockPosition c;
      private final BlockMinecartTrackAbstract d;
      private IBlockData e;
      private final boolean f;
      private final List g = Lists.newArrayList();

      public class_a_in_class_ajo(World var2, BlockPosition var3, IBlockData var4) {
         this.b = var2;
         this.c = var3;
         this.e = var4;
         this.d = (BlockMinecartTrackAbstract)var4.getBlock();
         BlockMinecartTrackAbstract.EnumTrackPosition var5 = (BlockMinecartTrackAbstract.EnumTrackPosition)var4.get(this.d.g());
         this.f = this.d.c;
         this.a(var5);
      }

      public List a() {
         return this.g;
      }

      private void a(BlockMinecartTrackAbstract.EnumTrackPosition var1) {
         this.g.clear();
         switch(BlockMinecartTrackAbstract.SyntheticClass_1.a[var1.ordinal()]) {
         case 1:
            this.g.add(this.c.c());
            this.g.add(this.c.d());
            break;
         case 2:
            this.g.add(this.c.e());
            this.g.add(this.c.f());
            break;
         case 3:
            this.g.add(this.c.e());
            this.g.add(this.c.f().a());
            break;
         case 4:
            this.g.add(this.c.e().a());
            this.g.add(this.c.f());
            break;
         case 5:
            this.g.add(this.c.c().a());
            this.g.add(this.c.d());
            break;
         case 6:
            this.g.add(this.c.c());
            this.g.add(this.c.d().a());
            break;
         case 7:
            this.g.add(this.c.f());
            this.g.add(this.c.d());
            break;
         case 8:
            this.g.add(this.c.e());
            this.g.add(this.c.d());
            break;
         case 9:
            this.g.add(this.c.e());
            this.g.add(this.c.c());
            break;
         case 10:
            this.g.add(this.c.f());
            this.g.add(this.c.c());
         }

      }

      private void d() {
         for(int var1 = 0; var1 < this.g.size(); ++var1) {
            BlockMinecartTrackAbstract.class_a_in_class_ajo var2 = this.b((BlockPosition)this.g.get(var1));
            if(var2 != null && var2.a(this)) {
               this.g.set(var1, var2.c);
            } else {
               this.g.remove(var1--);
            }
         }

      }

      private boolean a(BlockPosition var1) {
         return BlockMinecartTrackAbstract.b(this.b, var1) || BlockMinecartTrackAbstract.b(this.b, var1.a()) || BlockMinecartTrackAbstract.b(this.b, var1.b());
      }

      private BlockMinecartTrackAbstract.class_a_in_class_ajo b(BlockPosition var1) {
         IBlockData var3 = this.b.getType(var1);
         if(BlockMinecartTrackAbstract.i(var3)) {
            return BlockMinecartTrackAbstract.this.new class_a_in_class_ajo(this.b, var1, var3);
         } else {
            BlockPosition var2 = var1.a();
            var3 = this.b.getType(var2);
            if(BlockMinecartTrackAbstract.i(var3)) {
               return BlockMinecartTrackAbstract.this.new class_a_in_class_ajo(this.b, var2, var3);
            } else {
               var2 = var1.b();
               var3 = this.b.getType(var2);
               return BlockMinecartTrackAbstract.i(var3)?BlockMinecartTrackAbstract.this.new class_a_in_class_ajo(this.b, var2, var3):null;
            }
         }
      }

      private boolean a(BlockMinecartTrackAbstract.class_a_in_class_ajo var1) {
         return this.c(var1.c);
      }

      private boolean c(BlockPosition var1) {
         for(int var2 = 0; var2 < this.g.size(); ++var2) {
            BlockPosition var3 = (BlockPosition)this.g.get(var2);
            if(var3.p() == var1.p() && var3.r() == var1.r()) {
               return true;
            }
         }

         return false;
      }

      protected int b() {
         int var1 = 0;
         Iterator var2 = EnumDirection.EnumDirectionLimit.HORIZONTAL.iterator();

         while(var2.hasNext()) {
            EnumDirection var3 = (EnumDirection)var2.next();
            if(this.a(this.c.a(var3))) {
               ++var1;
            }
         }

         return var1;
      }

      private boolean b(BlockMinecartTrackAbstract.class_a_in_class_ajo var1) {
         return this.a(var1) || this.g.size() != 2;
      }

      private void c(BlockMinecartTrackAbstract.class_a_in_class_ajo var1) {
         this.g.add(var1.c);
         BlockPosition var2 = this.c.c();
         BlockPosition var3 = this.c.d();
         BlockPosition var4 = this.c.e();
         BlockPosition var5 = this.c.f();
         boolean var6 = this.c(var2);
         boolean var7 = this.c(var3);
         boolean var8 = this.c(var4);
         boolean var9 = this.c(var5);
         BlockMinecartTrackAbstract.EnumTrackPosition var10 = null;
         if(var6 || var7) {
            var10 = BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH;
         }

         if(var8 || var9) {
            var10 = BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST;
         }

         if(!this.f) {
            if(var7 && var9 && !var6 && !var8) {
               var10 = BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST;
            }

            if(var7 && var8 && !var6 && !var9) {
               var10 = BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST;
            }

            if(var6 && var8 && !var7 && !var9) {
               var10 = BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST;
            }

            if(var6 && var9 && !var7 && !var8) {
               var10 = BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST;
            }
         }

         if(var10 == BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH) {
            if(BlockMinecartTrackAbstract.b(this.b, var2.a())) {
               var10 = BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH;
            }

            if(BlockMinecartTrackAbstract.b(this.b, var3.a())) {
               var10 = BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH;
            }
         }

         if(var10 == BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST) {
            if(BlockMinecartTrackAbstract.b(this.b, var5.a())) {
               var10 = BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST;
            }

            if(BlockMinecartTrackAbstract.b(this.b, var4.a())) {
               var10 = BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST;
            }
         }

         if(var10 == null) {
            var10 = BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH;
         }

         this.e = this.e.set(this.d.g(), var10);
         this.b.a((BlockPosition)this.c, (IBlockData)this.e, 3);
      }

      private boolean d(BlockPosition var1) {
         BlockMinecartTrackAbstract.class_a_in_class_ajo var2 = this.b(var1);
         if(var2 == null) {
            return false;
         } else {
            var2.d();
            return var2.b(this);
         }
      }

      public BlockMinecartTrackAbstract.class_a_in_class_ajo a(boolean var1, boolean var2) {
         BlockPosition var3 = this.c.c();
         BlockPosition var4 = this.c.d();
         BlockPosition var5 = this.c.e();
         BlockPosition var6 = this.c.f();
         boolean var7 = this.d(var3);
         boolean var8 = this.d(var4);
         boolean var9 = this.d(var5);
         boolean var10 = this.d(var6);
         BlockMinecartTrackAbstract.EnumTrackPosition var11 = null;
         if((var7 || var8) && !var9 && !var10) {
            var11 = BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH;
         }

         if((var9 || var10) && !var7 && !var8) {
            var11 = BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST;
         }

         if(!this.f) {
            if(var8 && var10 && !var7 && !var9) {
               var11 = BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST;
            }

            if(var8 && var9 && !var7 && !var10) {
               var11 = BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST;
            }

            if(var7 && var9 && !var8 && !var10) {
               var11 = BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST;
            }

            if(var7 && var10 && !var8 && !var9) {
               var11 = BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST;
            }
         }

         if(var11 == null) {
            if(var7 || var8) {
               var11 = BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH;
            }

            if(var9 || var10) {
               var11 = BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST;
            }

            if(!this.f) {
               if(var1) {
                  if(var8 && var10) {
                     var11 = BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST;
                  }

                  if(var9 && var8) {
                     var11 = BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST;
                  }

                  if(var10 && var7) {
                     var11 = BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST;
                  }

                  if(var7 && var9) {
                     var11 = BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST;
                  }
               } else {
                  if(var7 && var9) {
                     var11 = BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_WEST;
                  }

                  if(var10 && var7) {
                     var11 = BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_EAST;
                  }

                  if(var9 && var8) {
                     var11 = BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_WEST;
                  }

                  if(var8 && var10) {
                     var11 = BlockMinecartTrackAbstract.EnumTrackPosition.SOUTH_EAST;
                  }
               }
            }
         }

         if(var11 == BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH) {
            if(BlockMinecartTrackAbstract.b(this.b, var3.a())) {
               var11 = BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_NORTH;
            }

            if(BlockMinecartTrackAbstract.b(this.b, var4.a())) {
               var11 = BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_SOUTH;
            }
         }

         if(var11 == BlockMinecartTrackAbstract.EnumTrackPosition.EAST_WEST) {
            if(BlockMinecartTrackAbstract.b(this.b, var6.a())) {
               var11 = BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_EAST;
            }

            if(BlockMinecartTrackAbstract.b(this.b, var5.a())) {
               var11 = BlockMinecartTrackAbstract.EnumTrackPosition.ASCENDING_WEST;
            }
         }

         if(var11 == null) {
            var11 = BlockMinecartTrackAbstract.EnumTrackPosition.NORTH_SOUTH;
         }

         this.a(var11);
         this.e = this.e.set(this.d.g(), var11);
         if(var2 || this.b.getType(this.c) != this.e) {
            this.b.a((BlockPosition)this.c, (IBlockData)this.e, 3);

            for(int var12 = 0; var12 < this.g.size(); ++var12) {
               BlockMinecartTrackAbstract.class_a_in_class_ajo var13 = this.b((BlockPosition)this.g.get(var12));
               if(var13 != null) {
                  var13.d();
                  if(var13.b(this)) {
                     var13.c(this);
                  }
               }
            }
         }

         return this;
      }

      public IBlockData c() {
         return this.e;
      }
   }
}
