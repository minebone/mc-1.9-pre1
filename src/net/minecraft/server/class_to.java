package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.class_tj;

public class class_to extends class_tj {
   protected EntityInsentient a;
   protected Entity b;
   protected float c;
   private int e;
   private float f;
   protected Class d;

   public class_to(EntityInsentient var1, Class var2, float var3) {
      this.a = var1;
      this.d = var2;
      this.c = var3;
      this.f = 0.02F;
      this.a(2);
   }

   public class_to(EntityInsentient var1, Class var2, float var3, float var4) {
      this.a = var1;
      this.d = var2;
      this.c = var3;
      this.f = var4;
      this.a(2);
   }

   public boolean a() {
      if(this.a.bE().nextFloat() >= this.f) {
         return false;
      } else {
         if(this.a.A() != null) {
            this.b = this.a.A();
         }

         if(this.d == EntityHuman.class) {
            this.b = this.a.world.a(this.a, (double)this.c);
         } else {
            this.b = this.a.world.a((Class)this.d, (AxisAlignedBB)this.a.bk().b((double)this.c, 3.0D, (double)this.c), (Entity)this.a);
         }

         return this.b != null;
      }
   }

   public boolean b() {
      return !this.b.at()?false:(this.a.h(this.b) > (double)(this.c * this.c)?false:this.e > 0);
   }

   public void c() {
      this.e = 40 + this.a.bE().nextInt(40);
   }

   public void d() {
      this.b = null;
   }

   public void e() {
      this.a.t().a(this.b.locX, this.b.locY + (double)this.b.bm(), this.b.locZ, (float)this.a.cE(), (float)this.a.cD());
      --this.e;
   }
}
