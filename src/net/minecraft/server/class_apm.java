package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_apm extends Block {
   public class_apm() {
      super(Material.G);
      this.a(CreativeModeTab.c);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Entity var4) {
      var4.aP();
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return k;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Items.H;
   }

   protected boolean o() {
      return true;
   }

   public void a(World var1, EntityHuman var2, BlockPosition var3, IBlockData var4, TileEntity var5, ItemStack var6) {
      if(!var1.E && var6 != null && var6.b() == Items.bl) {
         var2.b(StatisticList.a((Block)this));
         a(var1, var3, new ItemStack(Item.a((Block)this), 1));
      } else {
         super.a(var1, var2, var3, var4, var5, var6);
      }
   }
}
