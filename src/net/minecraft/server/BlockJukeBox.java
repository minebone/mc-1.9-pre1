package net.minecraft.server;

import net.minecraft.server.BlockStateList;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_aoa;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_nf;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_yc;

public class BlockJukeBox extends class_ajm {
   public static final class_arm a = class_arm.a("has_record");

   protected BlockJukeBox() {
      super(Material.d, MaterialMapColor.l);
      this.w(this.A.b().set(a, Boolean.valueOf(false)));
      this.a(CreativeModeTab.c);
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(((Boolean)var3.get(a)).booleanValue()) {
         this.e(var1, var2, var3);
         var3 = var3.set(a, Boolean.valueOf(false));
         var1.a((BlockPosition)var2, (IBlockData)var3, 2);
         return true;
      } else {
         return false;
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, ItemStack var4) {
      if(!var1.E) {
         TileEntity var5 = var1.r(var2);
         if(var5 instanceof BlockJukeBox.TileEntityRecordPlayer) {
            ((BlockJukeBox.TileEntityRecordPlayer)var5).a(var4.k());
            var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Boolean.valueOf(true)), 2);
         }
      }
   }

   private void e(World var1, BlockPosition var2, IBlockData var3) {
      if(!var1.E) {
         TileEntity var4 = var1.r(var2);
         if(var4 instanceof BlockJukeBox.TileEntityRecordPlayer) {
            BlockJukeBox.TileEntityRecordPlayer var5 = (BlockJukeBox.TileEntityRecordPlayer)var4;
            ItemStack var6 = var5.a();
            if(var6 != null) {
               var1.b(1010, var2, 0);
               var1.a((BlockPosition)var2, (class_nf)null);
               var5.a((ItemStack)null);
               float var7 = 0.7F;
               double var8 = (double)(var1.r.nextFloat() * var7) + (double)(1.0F - var7) * 0.5D;
               double var10 = (double)(var1.r.nextFloat() * var7) + (double)(1.0F - var7) * 0.2D + 0.6D;
               double var12 = (double)(var1.r.nextFloat() * var7) + (double)(1.0F - var7) * 0.5D;
               ItemStack var14 = var6.k();
               class_yc var15 = new class_yc(var1, (double)var2.p() + var8, (double)var2.q() + var10, (double)var2.r() + var12, var14);
               var15.q();
               var1.a((Entity)var15);
            }
         }
      }
   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      this.e(var1, var2, var3);
      super.b(var1, var2, var3);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, float var4, int var5) {
      if(!var1.E) {
         super.a(var1, var2, var3, var4, 0);
      }
   }

   public TileEntity a(World var1, int var2) {
      return new BlockJukeBox.TileEntityRecordPlayer();
   }

   public boolean v(IBlockData var1) {
      return true;
   }

   public int d(IBlockData var1, World var2, BlockPosition var3) {
      TileEntity var4 = var2.r(var3);
      if(var4 instanceof BlockJukeBox.TileEntityRecordPlayer) {
         ItemStack var5 = ((BlockJukeBox.TileEntityRecordPlayer)var4).a();
         if(var5 != null) {
            return Item.a(var5.b()) + 1 - Item.a(Items.cA);
         }
      }

      return 0;
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.MODEL;
   }

   public IBlockData a(int var1) {
      return this.u().set(a, Boolean.valueOf(var1 > 0));
   }

   public int e(IBlockData var1) {
      return ((Boolean)var1.get(a)).booleanValue()?1:0;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   public static class TileEntityRecordPlayer extends TileEntity {
      private ItemStack a;

      public void a(MinecraftServer var1, NBTTagCompound var2) {
         super.a(var1, var2);
         if(var2.b("RecordItem", 10)) {
            this.a(ItemStack.a(var2.o("RecordItem")));
         } else if(var2.h("Record") > 0) {
            this.a(new ItemStack(Item.c(var2.h("Record"))));
         }

      }

      public void a(NBTTagCompound var1) {
         super.a(var1);
         if(this.a() != null) {
            var1.a((String)"RecordItem", (NBTTag)this.a().b(new NBTTagCompound()));
         }

      }

      public ItemStack a() {
         return this.a;
      }

      public void a(ItemStack var1) {
         this.a = var1;
         this.v_();
      }
   }
}
