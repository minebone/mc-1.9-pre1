package net.minecraft.server;

import net.minecraft.server.Vec3D;
import net.minecraft.server.class_aub;
import net.minecraft.server.class_aym;
import net.minecraft.server.class_ayo;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_wt;
import net.minecraft.server.class_wv;
import net.minecraft.server.class_xj;

public class class_xi extends class_wv {
   private boolean b;
   private class_ayo c;
   private Vec3D d;

   public class_xi(class_wt var1) {
      super(var1);
   }

   public void c() {
      if(this.b) {
         this.b = false;
         this.j();
      } else {
         BlockPosition var1 = this.a.world.q(class_aub.a);
         double var2 = this.a.d(var1);
         if(var2 > 100.0D) {
            this.a.cT().a(class_xj.a);
         }
      }

   }

   public void d() {
      this.b = true;
      this.c = null;
      this.d = null;
   }

   private void j() {
      int var1 = this.a.o();
      Vec3D var2 = this.a.a(1.0F);
      int var3 = this.a.l(-var2.b * 40.0D, 105.0D, -var2.d * 40.0D);
      if(this.a.cU() != null && this.a.cU().c() >= 0) {
         var3 %= 12;
         if(var3 < 0) {
            var3 += 12;
         }
      } else {
         var3 -= 12;
         var3 &= 7;
         var3 += 12;
      }

      this.c = this.a.a(var1, var3, (class_aym)null);
      if(this.c != null) {
         this.c.a();
         this.k();
      }

   }

   private void k() {
      Vec3D var1 = this.c.f();
      this.c.a();

      double var2;
      do {
         var2 = var1.c + (double)(this.a.bE().nextFloat() * 20.0F);
      } while(var2 < var1.c);

      this.d = new Vec3D(var1.b, var2, var1.d);
   }

   public Vec3D g() {
      return this.d;
   }

   public class_xj i() {
      return class_xj.e;
   }
}
