package net.minecraft.server;

import com.google.common.collect.ImmutableSet;
import java.util.Collection;
import net.minecraft.server.BlockState;

public class class_arm extends BlockState {
   private final ImmutableSet a = ImmutableSet.of(Boolean.valueOf(true), Boolean.valueOf(false));

   protected class_arm(String var1) {
      super(var1, Boolean.class);
   }

   public Collection c() {
      return this.a;
   }

   public static class_arm a(String var0) {
      return new class_arm(var0);
   }

   public String a(Boolean var1) {
      return var1.toString();
   }

   public boolean equals(Object var1) {
      if(this == var1) {
         return true;
      } else if(var1 instanceof class_arm && super.equals(var1)) {
         class_arm var2 = (class_arm)var1;
         return this.a.equals(var2.a);
      } else {
         return false;
      }
   }

   public int hashCode() {
      return 31 * super.hashCode() + this.a.hashCode();
   }

   // $FF: synthetic method
   // $FF: bridge method
   public String a(Comparable var1) {
      return this.a((Boolean)var1);
   }
}
