package net.minecraft.server;

import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.class_pa;
import net.minecraft.server.class_pd;

public abstract class class_py implements class_pd {
   private final String a;
   private final String b;

   public class_py(String var1, String var2) {
      this.a = var1;
      this.b = var2;
   }

   public NBTTagCompound a(class_pa var1, NBTTagCompound var2, int var3) {
      if(var2.l(this.a).equals(this.b)) {
         var2 = this.b(var1, var2, var3);
      }

      return var2;
   }

   abstract NBTTagCompound b(class_pa var1, NBTTagCompound var2, int var3);
}
