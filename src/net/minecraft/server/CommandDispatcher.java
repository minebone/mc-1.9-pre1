package net.minecraft.server;

import java.util.Iterator;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandAchievement;
import net.minecraft.server.CommandBan;
import net.minecraft.server.CommandBanIp;
import net.minecraft.server.CommandBanList;
import net.minecraft.server.CommandBlockData;
import net.minecraft.server.CommandClear;
import net.minecraft.server.CommandClone;
import net.minecraft.server.CommandDeop;
import net.minecraft.server.CommandDifficulty;
import net.minecraft.server.CommandEffect;
import net.minecraft.server.CommandEnchant;
import net.minecraft.server.CommandFill;
import net.minecraft.server.CommandGamemodeDefault;
import net.minecraft.server.CommandGamerule;
import net.minecraft.server.CommandHandler;
import net.minecraft.server.CommandHelp;
import net.minecraft.server.CommandIdleTimeout;
import net.minecraft.server.CommandKick;
import net.minecraft.server.CommandKill;
import net.minecraft.server.CommandList;
import net.minecraft.server.CommandMe;
import net.minecraft.server.CommandOp;
import net.minecraft.server.CommandPardon;
import net.minecraft.server.CommandPardonIP;
import net.minecraft.server.CommandPlaySound;
import net.minecraft.server.CommandPublish;
import net.minecraft.server.CommandReplaceItem;
import net.minecraft.server.CommandSaveAll;
import net.minecraft.server.CommandSaveOff;
import net.minecraft.server.CommandSaveOn;
import net.minecraft.server.CommandSay;
import net.minecraft.server.CommandScoreboard;
import net.minecraft.server.CommandSeed;
import net.minecraft.server.CommandSetBlock;
import net.minecraft.server.CommandSetWorldSpawn;
import net.minecraft.server.CommandSpawnpoint;
import net.minecraft.server.CommandSpreadPlayers;
import net.minecraft.server.CommandStats;
import net.minecraft.server.CommandStop;
import net.minecraft.server.CommandTell;
import net.minecraft.server.CommandTestFor;
import net.minecraft.server.CommandTestForBlock;
import net.minecraft.server.CommandTestForBlocks;
import net.minecraft.server.CommandTime;
import net.minecraft.server.CommandToggleDownfall;
import net.minecraft.server.CommandTp;
import net.minecraft.server.CommandTrigger;
import net.minecraft.server.CommandWeather;
import net.minecraft.server.CommandWhitelist;
import net.minecraft.server.CommandWorldBorder;
import net.minecraft.server.CommandXp;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumChatFormat;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.RemoteControlCommandListener;
import net.minecraft.server.class_ac;
import net.minecraft.server.class_ad;
import net.minecraft.server.class_ah;
import net.minecraft.server.class_ahi;
import net.minecraft.server.class_aj;
import net.minecraft.server.class_at;
import net.minecraft.server.class_bl;
import net.minecraft.server.class_bn;
import net.minecraft.server.class_bs;
import net.minecraft.server.class_h;
import net.minecraft.server.class_k;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.class_x;

public class CommandDispatcher extends CommandHandler implements class_h {
   private final MinecraftServer a;

   public CommandDispatcher(MinecraftServer var1) {
      this.a = var1;
      this.a(new CommandTime());
      this.a(new class_ah());
      this.a(new CommandDifficulty());
      this.a(new CommandGamemodeDefault());
      this.a(new CommandKill());
      this.a(new CommandToggleDownfall());
      this.a(new CommandWeather());
      this.a(new CommandXp());
      this.a(new CommandTp());
      this.a(new class_aj());
      this.a(new CommandReplaceItem());
      this.a(new CommandStats());
      this.a(new CommandEffect());
      this.a(new CommandEnchant());
      this.a(new class_at());
      this.a(new CommandMe());
      this.a(new CommandSeed());
      this.a(new CommandHelp());
      this.a(new class_x());
      this.a(new CommandTell());
      this.a(new CommandSay());
      this.a(new CommandSpawnpoint());
      this.a(new CommandSetWorldSpawn());
      this.a(new CommandGamerule());
      this.a(new CommandClear());
      this.a(new CommandTestFor());
      this.a(new CommandSpreadPlayers());
      this.a(new CommandPlaySound());
      this.a(new CommandScoreboard());
      this.a(new class_ad());
      this.a(new CommandTrigger());
      this.a(new CommandAchievement());
      this.a(new class_bl());
      this.a(new CommandSetBlock());
      this.a(new CommandFill());
      this.a(new CommandClone());
      this.a(new CommandTestForBlocks());
      this.a(new CommandBlockData());
      this.a(new CommandTestForBlock());
      this.a(new class_bn());
      this.a(new CommandWorldBorder());
      this.a(new class_bs());
      this.a(new class_ac());
      if(var1.aa()) {
         this.a(new CommandOp());
         this.a(new CommandDeop());
         this.a(new CommandStop());
         this.a(new CommandSaveAll());
         this.a(new CommandSaveOff());
         this.a(new CommandSaveOn());
         this.a(new CommandBanIp());
         this.a(new CommandPardonIP());
         this.a(new CommandBan());
         this.a(new CommandBanList());
         this.a(new CommandPardon());
         this.a(new CommandKick());
         this.a(new CommandList());
         this.a(new CommandWhitelist());
         this.a(new CommandIdleTimeout());
      } else {
         this.a(new CommandPublish());
      }

      CommandAbstract.a((class_h)this);
   }

   public void a(ICommandListener var1, class_k var2, int var3, String var4, Object... var5) {
      boolean var6 = true;
      MinecraftServer var7 = this.a;
      if(!var1.z_()) {
         var6 = false;
      }

      ChatMessage var8 = new ChatMessage("chat.type.admin", new Object[]{var1.h_(), new ChatMessage(var4, var5)});
      var8.b().a(EnumChatFormat.GRAY);
      var8.b().b(Boolean.valueOf(true));
      if(var6) {
         Iterator var9 = var7.getPlayerList().v().iterator();

         label85:
         while(true) {
            EntityHuman var10;
            boolean var11;
            boolean var12;
            do {
               do {
                  do {
                     do {
                        if(!var9.hasNext()) {
                           break label85;
                        }

                        var10 = (EntityHuman)var9.next();
                     } while(var10 == var1);
                  } while(!var7.getPlayerList().h(var10.getProfile()));
               } while(!var2.a(this.a, var1));

               var11 = var1 instanceof MinecraftServer && this.a.s();
               var12 = var1 instanceof RemoteControlCommandListener && this.a.r();
            } while(!var11 && !var12 && (var1 instanceof RemoteControlCommandListener || var1 instanceof MinecraftServer));

            var10.a((IChatBaseComponent)var8);
         }
      }

      if(var1 != var7 && var7.d[0].U().b("logAdminCommands")) {
         var7.a((IChatBaseComponent)var8);
      }

      boolean var13 = var7.d[0].U().b("sendCommandFeedback");
      if(var1 instanceof class_ahi) {
         var13 = ((class_ahi)var1).n();
      }

      if((var3 & 1) != 1 && var13 || var1 instanceof MinecraftServer) {
         var1.a(new ChatMessage(var4, var5));
      }

   }

   protected MinecraftServer a() {
      return this.a;
   }
}
