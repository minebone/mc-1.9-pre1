package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.class_acb;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aly;
import net.minecraft.server.class_aoo;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;

public class class_aeq extends class_acb {
   private final class_aly b;
   private final class_aly c;

   public class_aeq(Block var1, class_aly var2, class_aly var3) {
      super(var1);
      this.b = var2;
      this.c = var3;
      this.e(0);
      this.a(true);
   }

   public int a(int var1) {
      return var1;
   }

   public String f_(ItemStack var1) {
      return this.b.e(var1.i());
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      if(var1.b != 0 && var2.a(var4.a(var6), var6, var1)) {
         Comparable var10 = this.b.a(var1);
         IBlockData var11 = var3.getType(var4);
         if(var11.getBlock() == this.b) {
            IBlockState var12 = this.b.g();
            Comparable var13 = var11.get(var12);
            class_aly.class_a_in_class_aly var14 = (class_aly.class_a_in_class_aly)var11.get(class_aly.a);
            if((var6 == EnumDirection.UP && var14 == class_aly.class_a_in_class_aly.BOTTOM || var6 == EnumDirection.DOWN && var14 == class_aly.class_a_in_class_aly.TOP) && var13 == var10) {
               IBlockData var15 = this.a(var12, var13);
               AxisAlignedBB var16 = var15.d(var3, var4);
               if(var16 != Block.k && var3.c(var16.a(var4)) && var3.a((BlockPosition)var4, (IBlockData)var15, 11)) {
                  class_aoo var17 = this.c.w();
                  var3.a(var2, var4, var17.e(), EnumSoundCategory.BLOCKS, (var17.a() + 1.0F) / 2.0F, var17.b() * 0.8F);
                  --var1.b;
               }

               return EnumResult.SUCCESS;
            }
         }

         return this.a(var2, var1, var3, var4.a(var6), var10)?EnumResult.SUCCESS:super.a(var1, var2, var3, var4, var5, var6, var7, var8, var9);
      } else {
         return EnumResult.FAIL;
      }
   }

   private boolean a(EntityHuman var1, ItemStack var2, World var3, BlockPosition var4, Object var5) {
      IBlockData var6 = var3.getType(var4);
      if(var6.getBlock() == this.b) {
         Comparable var7 = var6.get(this.b.g());
         if(var7 == var5) {
            IBlockData var8 = this.a(this.b.g(), var7);
            AxisAlignedBB var9 = var8.d(var3, var4);
            if(var9 != Block.k && var3.c(var9.a(var4)) && var3.a((BlockPosition)var4, (IBlockData)var8, 11)) {
               class_aoo var10 = this.c.w();
               var3.a(var1, var4, var10.e(), EnumSoundCategory.BLOCKS, (var10.a() + 1.0F) / 2.0F, var10.b() * 0.8F);
               --var2.b;
            }

            return true;
         }
      }

      return false;
   }

   protected IBlockData a(IBlockState var1, Comparable var2) {
      return this.c.u().set(var1, var2);
   }
}
