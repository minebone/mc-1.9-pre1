package net.minecraft.server;

import java.util.List;
import java.util.Random;

public class class_ov {
   public static int a(List var0) {
      int var1 = 0;
      int var2 = 0;

      for(int var3 = var0.size(); var2 < var3; ++var2) {
         class_ov.class_a_in_class_ov var4 = (class_ov.class_a_in_class_ov)var0.get(var2);
         var1 += var4.a;
      }

      return var1;
   }

   public static class_ov.class_a_in_class_ov a(Random var0, List var1, int var2) {
      if(var2 <= 0) {
         throw new IllegalArgumentException();
      } else {
         int var3 = var0.nextInt(var2);
         return a(var1, var3);
      }
   }

   public static class_ov.class_a_in_class_ov a(List var0, int var1) {
      int var2 = 0;

      for(int var3 = var0.size(); var2 < var3; ++var2) {
         class_ov.class_a_in_class_ov var4 = (class_ov.class_a_in_class_ov)var0.get(var2);
         var1 -= var4.a;
         if(var1 < 0) {
            return var4;
         }
      }

      return null;
   }

   public static class_ov.class_a_in_class_ov a(Random var0, List var1) {
      return a(var0, var1, a(var1));
   }

   public static class class_a_in_class_ov {
      protected int a;

      public class_a_in_class_ov(int var1) {
         this.a = var1;
      }
   }
}
