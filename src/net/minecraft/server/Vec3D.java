package net.minecraft.server;

import net.minecraft.server.BaseBlockPosition;
import net.minecraft.server.MathHelper;

public class Vec3D {
   public static final Vec3D a = new Vec3D(0.0D, 0.0D, 0.0D);
   public final double b;
   public final double c;
   public final double d;

   public Vec3D(double var1, double var3, double var5) {
      if(var1 == -0.0D) {
         var1 = 0.0D;
      }

      if(var3 == -0.0D) {
         var3 = 0.0D;
      }

      if(var5 == -0.0D) {
         var5 = 0.0D;
      }

      this.b = var1;
      this.c = var3;
      this.d = var5;
   }

   public Vec3D(BaseBlockPosition var1) {
      this((double)var1.p(), (double)var1.q(), (double)var1.r());
   }

   public Vec3D a(Vec3D var1) {
      return new Vec3D(var1.b - this.b, var1.c - this.c, var1.d - this.d);
   }

   public Vec3D a() {
      double var1 = (double)MathHelper.a(this.b * this.b + this.c * this.c + this.d * this.d);
      return var1 < 1.0E-4D?a:new Vec3D(this.b / var1, this.c / var1, this.d / var1);
   }

   public double b(Vec3D var1) {
      return this.b * var1.b + this.c * var1.c + this.d * var1.d;
   }

   public Vec3D d(Vec3D var1) {
      return this.a(var1.b, var1.c, var1.d);
   }

   public Vec3D a(double var1, double var3, double var5) {
      return this.b(-var1, -var3, -var5);
   }

   public Vec3D e(Vec3D var1) {
      return this.b(var1.b, var1.c, var1.d);
   }

   public Vec3D b(double var1, double var3, double var5) {
      return new Vec3D(this.b + var1, this.c + var3, this.d + var5);
   }

   public double f(Vec3D var1) {
      double var2 = var1.b - this.b;
      double var4 = var1.c - this.c;
      double var6 = var1.d - this.d;
      return (double)MathHelper.a(var2 * var2 + var4 * var4 + var6 * var6);
   }

   public double g(Vec3D var1) {
      double var2 = var1.b - this.b;
      double var4 = var1.c - this.c;
      double var6 = var1.d - this.d;
      return var2 * var2 + var4 * var4 + var6 * var6;
   }

   public double c(double var1, double var3, double var5) {
      double var7 = var1 - this.b;
      double var9 = var3 - this.c;
      double var11 = var5 - this.d;
      return var7 * var7 + var9 * var9 + var11 * var11;
   }

   public Vec3D a(double var1) {
      return new Vec3D(this.b * var1, this.c * var1, this.d * var1);
   }

   public double b() {
      return (double)MathHelper.a(this.b * this.b + this.c * this.c + this.d * this.d);
   }

   public Vec3D a(Vec3D var1, double var2) {
      double var4 = var1.b - this.b;
      double var6 = var1.c - this.c;
      double var8 = var1.d - this.d;
      if(var4 * var4 < 1.0000000116860974E-7D) {
         return null;
      } else {
         double var10 = (var2 - this.b) / var4;
         return var10 >= 0.0D && var10 <= 1.0D?new Vec3D(this.b + var4 * var10, this.c + var6 * var10, this.d + var8 * var10):null;
      }
   }

   public Vec3D b(Vec3D var1, double var2) {
      double var4 = var1.b - this.b;
      double var6 = var1.c - this.c;
      double var8 = var1.d - this.d;
      if(var6 * var6 < 1.0000000116860974E-7D) {
         return null;
      } else {
         double var10 = (var2 - this.c) / var6;
         return var10 >= 0.0D && var10 <= 1.0D?new Vec3D(this.b + var4 * var10, this.c + var6 * var10, this.d + var8 * var10):null;
      }
   }

   public Vec3D c(Vec3D var1, double var2) {
      double var4 = var1.b - this.b;
      double var6 = var1.c - this.c;
      double var8 = var1.d - this.d;
      if(var8 * var8 < 1.0000000116860974E-7D) {
         return null;
      } else {
         double var10 = (var2 - this.d) / var8;
         return var10 >= 0.0D && var10 <= 1.0D?new Vec3D(this.b + var4 * var10, this.c + var6 * var10, this.d + var8 * var10):null;
      }
   }

   public boolean equals(Object var1) {
      if(this == var1) {
         return true;
      } else if(!(var1 instanceof Vec3D)) {
         return false;
      } else {
         Vec3D var2 = (Vec3D)var1;
         return Double.compare(var2.b, this.b) != 0?false:(Double.compare(var2.c, this.c) != 0?false:Double.compare(var2.d, this.d) == 0);
      }
   }

   public int hashCode() {
      long var2 = Double.doubleToLongBits(this.b);
      int var1 = (int)(var2 ^ var2 >>> 32);
      var2 = Double.doubleToLongBits(this.c);
      var1 = 31 * var1 + (int)(var2 ^ var2 >>> 32);
      var2 = Double.doubleToLongBits(this.d);
      var1 = 31 * var1 + (int)(var2 ^ var2 >>> 32);
      return var1;
   }

   public String toString() {
      return "(" + this.b + ", " + this.c + ", " + this.d + ")";
   }

   public Vec3D a(float var1) {
      float var2 = MathHelper.b(var1);
      float var3 = MathHelper.a(var1);
      double var4 = this.b;
      double var6 = this.c * (double)var2 + this.d * (double)var3;
      double var8 = this.d * (double)var2 - this.c * (double)var3;
      return new Vec3D(var4, var6, var8);
   }

   public Vec3D b(float var1) {
      float var2 = MathHelper.b(var1);
      float var3 = MathHelper.a(var1);
      double var4 = this.b * (double)var2 + this.d * (double)var3;
      double var6 = this.c;
      double var8 = this.d * (double)var2 - this.b * (double)var3;
      return new Vec3D(var4, var6, var8);
   }
}
