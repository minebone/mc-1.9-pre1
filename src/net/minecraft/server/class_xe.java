package net.minecraft.server;

import net.minecraft.server.class_ng;
import net.minecraft.server.class_wt;
import net.minecraft.server.class_ww;
import net.minecraft.server.class_xj;

public class class_xe extends class_ww {
   private int b;

   public class_xe(class_wt var1) {
      super(var1);
   }

   public void b() {
      this.a.world.a(this.a.locX, this.a.locY, this.a.locZ, class_ng.aO, this.a.by(), 2.5F, 0.8F + this.a.bE().nextFloat() * 0.3F, false);
   }

   public void c() {
      if(this.b++ >= 40) {
         this.a.cT().a(class_xj.f);
      }

   }

   public void d() {
      this.b = 0;
   }

   public class_xj i() {
      return class_xj.h;
   }
}
