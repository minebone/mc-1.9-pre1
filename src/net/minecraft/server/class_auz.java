package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockLeaves;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.class_amy;
import net.minecraft.server.class_amz;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ato;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_auz extends class_ato {
   private static final IBlockData a = Blocks.s.u().set(class_amz.b, BlockWood.EnumLogVariant.ACACIA);
   private static final IBlockData b = Blocks.u.u().set(class_amy.e, BlockWood.EnumLogVariant.ACACIA).set(BlockLeaves.b, Boolean.valueOf(false));

   public class_auz(boolean var1) {
      super(var1);
   }

   public boolean b(World var1, Random var2, BlockPosition var3) {
      int var4 = var2.nextInt(3) + var2.nextInt(3) + 5;
      boolean var5 = true;
      if(var3.q() >= 1 && var3.q() + var4 + 1 <= 256) {
         int var9;
         int var10;
         for(int var6 = var3.q(); var6 <= var3.q() + 1 + var4; ++var6) {
            byte var7 = 1;
            if(var6 == var3.q()) {
               var7 = 0;
            }

            if(var6 >= var3.q() + 1 + var4 - 2) {
               var7 = 2;
            }

            BlockPosition.class_a_in_class_cj var8 = new BlockPosition.class_a_in_class_cj();

            for(var9 = var3.p() - var7; var9 <= var3.p() + var7 && var5; ++var9) {
               for(var10 = var3.r() - var7; var10 <= var3.r() + var7 && var5; ++var10) {
                  if(var6 >= 0 && var6 < 256) {
                     if(!this.a(var1.getType(var8.c(var9, var6, var10)).getBlock())) {
                        var5 = false;
                     }
                  } else {
                     var5 = false;
                  }
               }
            }
         }

         if(!var5) {
            return false;
         } else {
            Block var20 = var1.getType(var3.b()).getBlock();
            if((var20 == Blocks.c || var20 == Blocks.d) && var3.q() < 256 - var4 - 1) {
               this.a(var1, var3.b());
               EnumDirection var21 = EnumDirection.EnumDirectionLimit.HORIZONTAL.a(var2);
               int var22 = var4 - var2.nextInt(4) - 1;
               var9 = 3 - var2.nextInt(3);
               var10 = var3.p();
               int var11 = var3.r();
               int var12 = 0;

               int var14;
               for(int var13 = 0; var13 < var4; ++var13) {
                  var14 = var3.q() + var13;
                  if(var13 >= var22 && var9 > 0) {
                     var10 += var21.g();
                     var11 += var21.i();
                     --var9;
                  }

                  BlockPosition var15 = new BlockPosition(var10, var14, var11);
                  Material var16 = var1.getType(var15).getMaterial();
                  if(var16 == Material.a || var16 == Material.j) {
                     this.b(var1, var15);
                     var12 = var14;
                  }
               }

               BlockPosition var23 = new BlockPosition(var10, var12, var11);

               int var25;
               for(var14 = -3; var14 <= 3; ++var14) {
                  for(var25 = -3; var25 <= 3; ++var25) {
                     if(Math.abs(var14) != 3 || Math.abs(var25) != 3) {
                        this.c(var1, var23.a(var14, 0, var25));
                     }
                  }
               }

               var23 = var23.a();

               for(var14 = -1; var14 <= 1; ++var14) {
                  for(var25 = -1; var25 <= 1; ++var25) {
                     this.c(var1, var23.a(var14, 0, var25));
                  }
               }

               this.c(var1, var23.g(2));
               this.c(var1, var23.f(2));
               this.c(var1, var23.e(2));
               this.c(var1, var23.d(2));
               var10 = var3.p();
               var11 = var3.r();
               EnumDirection var24 = EnumDirection.EnumDirectionLimit.HORIZONTAL.a(var2);
               if(var24 != var21) {
                  var14 = var22 - var2.nextInt(2) - 1;
                  var25 = 1 + var2.nextInt(3);
                  var12 = 0;

                  int var17;
                  for(int var26 = var14; var26 < var4 && var25 > 0; --var25) {
                     if(var26 >= 1) {
                        var17 = var3.q() + var26;
                        var10 += var24.g();
                        var11 += var24.i();
                        BlockPosition var18 = new BlockPosition(var10, var17, var11);
                        Material var19 = var1.getType(var18).getMaterial();
                        if(var19 == Material.a || var19 == Material.j) {
                           this.b(var1, var18);
                           var12 = var17;
                        }
                     }

                     ++var26;
                  }

                  if(var12 > 0) {
                     BlockPosition var27 = new BlockPosition(var10, var12, var11);

                     int var28;
                     for(var17 = -2; var17 <= 2; ++var17) {
                        for(var28 = -2; var28 <= 2; ++var28) {
                           if(Math.abs(var17) != 2 || Math.abs(var28) != 2) {
                              this.c(var1, var27.a(var17, 0, var28));
                           }
                        }
                     }

                     var27 = var27.a();

                     for(var17 = -1; var17 <= 1; ++var17) {
                        for(var28 = -1; var28 <= 1; ++var28) {
                           this.c(var1, var27.a(var17, 0, var28));
                        }
                     }
                  }
               }

               return true;
            } else {
               return false;
            }
         }
      } else {
         return false;
      }
   }

   private void b(World var1, BlockPosition var2) {
      this.a(var1, var2, a);
   }

   private void c(World var1, BlockPosition var2) {
      Material var3 = var1.getType(var2).getMaterial();
      if(var3 == Material.a || var3 == Material.j) {
         this.a(var1, var2, b);
      }

   }
}
