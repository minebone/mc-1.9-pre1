package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.EntityFireball;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.World;
import net.minecraft.server.class_aho;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_xn;

public class class_aad extends EntityFireball {
   private static final class_ke e = DataWatcher.a(class_aad.class, class_kg.h);

   public class_aad(World var1) {
      super(var1);
      this.a(0.3125F, 0.3125F);
   }

   public class_aad(World var1, class_rz var2, double var3, double var5, double var7) {
      super(var1, var2, var3, var5, var7);
      this.a(0.3125F, 0.3125F);
   }

   protected float l() {
      return this.n()?0.73F:super.l();
   }

   public boolean aG() {
      return false;
   }

   public float a(class_aho var1, World var2, BlockPosition var3, IBlockData var4) {
      float var5 = super.a(var1, var2, var3, var4);
      Block var6 = var4.getBlock();
      if(this.n() && class_xn.a(var6)) {
         var5 = Math.min(0.8F, var5);
      }

      return var5;
   }

   protected void a(MovingObjectPosition var1) {
      if(!this.world.E) {
         if(var1.d != null) {
            if(this.a != null) {
               if(var1.d.a(DamageSource.a(this.a), 8.0F)) {
                  if(!var1.d.at()) {
                     this.a.b(5.0F);
                  } else {
                     this.a(this.a, var1.d);
                  }
               }
            } else {
               var1.d.a(DamageSource.m, 5.0F);
            }

            if(var1.d instanceof class_rz) {
               byte var2 = 0;
               if(this.world.ae() == EnumDifficulty.NORMAL) {
                  var2 = 10;
               } else if(this.world.ae() == EnumDifficulty.HARD) {
                  var2 = 40;
               }

               if(var2 > 0) {
                  ((class_rz)var1.d).c(new MobEffect(MobEffectList.t, 20 * var2, 1));
               }
            }
         }

         this.world.a(this, this.locX, this.locY, this.locZ, 1.0F, false, this.world.U().b("mobGriefing"));
         this.S();
      }

   }

   public boolean ao() {
      return false;
   }

   public boolean a(DamageSource var1, float var2) {
      return false;
   }

   protected void i() {
      this.datawatcher.a((class_ke)e, (Object)Boolean.valueOf(false));
   }

   public boolean n() {
      return ((Boolean)this.datawatcher.a(e)).booleanValue();
   }

   public void a(boolean var1) {
      this.datawatcher.b(e, Boolean.valueOf(var1));
   }

   protected boolean k() {
      return false;
   }
}
