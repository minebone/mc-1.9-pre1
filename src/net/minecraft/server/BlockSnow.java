package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumSkyBlock;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class BlockSnow extends Block {
   public static final BlockStateInteger a = BlockStateInteger.a("layers", 1, 8);
   protected static final AxisAlignedBB[] b = new AxisAlignedBB[]{new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.0D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.125D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.25D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.375D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.5D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.625D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.75D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.875D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D)};

   protected BlockSnow() {
      super(Material.y);
      this.w(this.A.b().set(a, Integer.valueOf(1)));
      this.a(true);
      this.a(CreativeModeTab.c);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return b[((Integer)var1.get(a)).intValue()];
   }

   public boolean b(class_ahw var1, BlockPosition var2) {
      return ((Integer)var1.getType(var2).get(a)).intValue() < 5;
   }

   public boolean k(IBlockData var1) {
      return ((Integer)var1.get(a)).intValue() == 7;
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      int var4 = ((Integer)var1.get(a)).intValue() - 1;
      float var5 = 0.125F;
      AxisAlignedBB var6 = var1.c(var2, var3);
      return new AxisAlignedBB(var6.a, var6.b, var6.c, var6.d, (double)((float)var4 * var5), var6.f);
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean a(World var1, BlockPosition var2) {
      IBlockData var3 = var1.getType(var2.b());
      Block var4 = var3.getBlock();
      return var4 != Blocks.aI && var4 != Blocks.cB?(var3.getMaterial() == Material.j?true:(var4 == this && ((Integer)var3.get(a)).intValue() >= 7?true:var3.p() && var3.getMaterial().c())):false;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      this.e(var1, var2, var3);
   }

   private boolean e(World var1, BlockPosition var2, IBlockData var3) {
      if(!this.a(var1, var2)) {
         this.b(var1, var2, var3, 0);
         var1.g(var2);
         return false;
      } else {
         return true;
      }
   }

   public void a(World var1, EntityHuman var2, BlockPosition var3, IBlockData var4, TileEntity var5, ItemStack var6) {
      a((World)var1, (BlockPosition)var3, (ItemStack)(new ItemStack(Items.aF, ((Integer)var4.get(a)).intValue() + 1, 0)));
      var1.g(var3);
      var2.b(StatisticList.a((Block)this));
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Items.aF;
   }

   public int a(Random var1) {
      return 0;
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(var1.b(EnumSkyBlock.BLOCK, var2) > 11) {
         this.b(var1, var2, var1.getType(var2), 0);
         var1.g(var2);
      }

   }

   public IBlockData a(int var1) {
      return this.u().set(a, Integer.valueOf((var1 & 7) + 1));
   }

   public boolean a(class_ahw var1, BlockPosition var2) {
      return ((Integer)var1.getType(var2).get(a)).intValue() == 1;
   }

   public int e(IBlockData var1) {
      return ((Integer)var1.get(a)).intValue() - 1;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }
}
