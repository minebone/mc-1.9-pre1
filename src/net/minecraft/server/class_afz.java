package net.minecraft.server;

import net.minecraft.server.BlockDirt;
import net.minecraft.server.BlockDoubleStepAbstract;
import net.minecraft.server.BlockDoubleStoneStepAbstract;
import net.minecraft.server.BlockPrismarine;
import net.minecraft.server.BlockQuartz;
import net.minecraft.server.BlockRedSandstone;
import net.minecraft.server.BlockSand;
import net.minecraft.server.BlockSandStone;
import net.minecraft.server.BlockSmoothBrick;
import net.minecraft.server.BlockStone;
import net.minecraft.server.Blocks;
import net.minecraft.server.CraftingManager;
import net.minecraft.server.EnumColor;
import net.minecraft.server.Items;
import net.minecraft.server.ItemStack;

public class class_afz {
   public void a(CraftingManager var1) {
      var1.a(new ItemStack(Blocks.ae), new Object[]{"###", "# #", "###", Character.valueOf('#'), Blocks.f});
      var1.b(new ItemStack(Blocks.cg), new Object[]{Blocks.ae, Blocks.bR});
      var1.a(new ItemStack(Blocks.bQ), new Object[]{"###", "#E#", "###", Character.valueOf('#'), Blocks.Z, Character.valueOf('E'), Items.bR});
      var1.a(new ItemStack(Blocks.al), new Object[]{"###", "# #", "###", Character.valueOf('#'), Blocks.e});
      var1.a(new ItemStack(Blocks.ai), new Object[]{"##", "##", Character.valueOf('#'), Blocks.f});
      var1.a(new ItemStack(Blocks.A), new Object[]{"##", "##", Character.valueOf('#'), new ItemStack(Blocks.m, 1, BlockSand.EnumSandVariant.SAND.a())});
      var1.a(new ItemStack(Blocks.cM), new Object[]{"##", "##", Character.valueOf('#'), new ItemStack(Blocks.m, 1, BlockSand.EnumSandVariant.RED_SAND.a())});
      var1.a(new ItemStack(Blocks.A, 4, BlockSandStone.EnumSandstoneVariant.SMOOTH.a()), new Object[]{"##", "##", Character.valueOf('#'), new ItemStack(Blocks.A, 1, BlockSandStone.EnumSandstoneVariant.DEFAULT.a())});
      var1.a(new ItemStack(Blocks.cM, 4, BlockRedSandstone.EnumRedSandstoneVariant.SMOOTH.a()), new Object[]{"##", "##", Character.valueOf('#'), new ItemStack(Blocks.cM, 1, BlockRedSandstone.EnumRedSandstoneVariant.DEFAULT.a())});
      var1.a(new ItemStack(Blocks.A, 1, BlockSandStone.EnumSandstoneVariant.CHISELED.a()), new Object[]{"#", "#", Character.valueOf('#'), new ItemStack(Blocks.U, 1, BlockDoubleStepAbstract.EnumStoneSlabVariant.SAND.a())});
      var1.a(new ItemStack(Blocks.cM, 1, BlockRedSandstone.EnumRedSandstoneVariant.CHISELED.a()), new Object[]{"#", "#", Character.valueOf('#'), new ItemStack(Blocks.cP, 1, BlockDoubleStoneStepAbstract.EnumStoneSlab2Variant.RED_SANDSTONE.a())});
      var1.a(new ItemStack(Blocks.cq, 1, BlockQuartz.EnumQuartzVariant.CHISELED.a()), new Object[]{"#", "#", Character.valueOf('#'), new ItemStack(Blocks.U, 1, BlockDoubleStepAbstract.EnumStoneSlabVariant.QUARTZ.a())});
      var1.a(new ItemStack(Blocks.cq, 2, BlockQuartz.EnumQuartzVariant.LINES_Y.a()), new Object[]{"#", "#", Character.valueOf('#'), new ItemStack(Blocks.cq, 1, BlockQuartz.EnumQuartzVariant.DEFAULT.a())});
      var1.a(new ItemStack(Blocks.bf, 4), new Object[]{"##", "##", Character.valueOf('#'), new ItemStack(Blocks.b, 1, BlockStone.EnumStoneVariant.STONE.a())});
      var1.a(new ItemStack(Blocks.bf, 1, BlockSmoothBrick.e), new Object[]{"#", "#", Character.valueOf('#'), new ItemStack(Blocks.U, 1, BlockDoubleStepAbstract.EnumStoneSlabVariant.SMOOTHBRICK.a())});
      var1.b(new ItemStack(Blocks.bf, 1, BlockSmoothBrick.c), new Object[]{Blocks.bf, Blocks.bn});
      var1.b(new ItemStack(Blocks.Y, 1), new Object[]{Blocks.e, Blocks.bn});
      var1.a(new ItemStack(Blocks.bi, 16), new Object[]{"###", "###", Character.valueOf('#'), Items.l});
      var1.a(new ItemStack(Blocks.bj, 16), new Object[]{"###", "###", Character.valueOf('#'), Blocks.w});
      var1.a(new ItemStack(Blocks.bJ, 1), new Object[]{" R ", "RGR", " R ", Character.valueOf('R'), Items.aE, Character.valueOf('G'), Blocks.aX});
      var1.a(new ItemStack(Blocks.bY, 1), new Object[]{"GGG", "GSG", "OOO", Character.valueOf('G'), Blocks.w, Character.valueOf('S'), Items.cj, Character.valueOf('O'), Blocks.Z});
      var1.a(new ItemStack(Blocks.by, 1), new Object[]{"NN", "NN", Character.valueOf('N'), Items.cp});
      var1.a(new ItemStack(Blocks.b, 2, BlockStone.EnumStoneVariant.DIORITE.a()), new Object[]{"CQ", "QC", Character.valueOf('C'), Blocks.e, Character.valueOf('Q'), Items.cq});
      var1.b(new ItemStack(Blocks.b, 1, BlockStone.EnumStoneVariant.GRANITE.a()), new Object[]{new ItemStack(Blocks.b, 1, BlockStone.EnumStoneVariant.DIORITE.a()), Items.cq});
      var1.b(new ItemStack(Blocks.b, 2, BlockStone.EnumStoneVariant.ANDESITE.a()), new Object[]{new ItemStack(Blocks.b, 1, BlockStone.EnumStoneVariant.DIORITE.a()), Blocks.e});
      var1.a(new ItemStack(Blocks.d, 4, BlockDirt.EnumDirtVariant.COARSE_DIRT.a()), new Object[]{"DG", "GD", Character.valueOf('D'), new ItemStack(Blocks.d, 1, BlockDirt.EnumDirtVariant.DIRT.a()), Character.valueOf('G'), Blocks.n});
      var1.a(new ItemStack(Blocks.b, 4, BlockStone.EnumStoneVariant.DIORITE_SMOOTH.a()), new Object[]{"SS", "SS", Character.valueOf('S'), new ItemStack(Blocks.b, 1, BlockStone.EnumStoneVariant.DIORITE.a())});
      var1.a(new ItemStack(Blocks.b, 4, BlockStone.EnumStoneVariant.GRANITE_SMOOTH.a()), new Object[]{"SS", "SS", Character.valueOf('S'), new ItemStack(Blocks.b, 1, BlockStone.EnumStoneVariant.GRANITE.a())});
      var1.a(new ItemStack(Blocks.b, 4, BlockStone.EnumStoneVariant.ANDESITE_SMOOTH.a()), new Object[]{"SS", "SS", Character.valueOf('S'), new ItemStack(Blocks.b, 1, BlockStone.EnumStoneVariant.ANDESITE.a())});
      var1.a(new ItemStack(Blocks.cI, 1, BlockPrismarine.b), new Object[]{"SS", "SS", Character.valueOf('S'), Items.cM});
      var1.a(new ItemStack(Blocks.cI, 1, BlockPrismarine.c), new Object[]{"SSS", "SSS", "SSS", Character.valueOf('S'), Items.cM});
      var1.a(new ItemStack(Blocks.cI, 1, BlockPrismarine.d), new Object[]{"SSS", "SIS", "SSS", Character.valueOf('S'), Items.cM, Character.valueOf('I'), new ItemStack(Items.bd, 1, EnumColor.BLACK.b())});
      var1.a(new ItemStack(Blocks.cJ, 1, 0), new Object[]{"SCS", "CCC", "SCS", Character.valueOf('S'), Items.cM, Character.valueOf('C'), Items.cN});
      var1.a(new ItemStack(Blocks.cT, 4, 0), new Object[]{"FF", "FF", Character.valueOf('F'), Items.cT});
      var1.a(new ItemStack(Blocks.cV, 4, 0), new Object[]{"#  ", "## ", "###", Character.valueOf('#'), Blocks.cT});
      var1.a(new ItemStack(Blocks.cU, 1, 0), new Object[]{"#", "#", Character.valueOf('#'), Blocks.cX});
      var1.a(new ItemStack(Blocks.cY, 4, 0), new Object[]{"##", "##", Character.valueOf('#'), Blocks.bH});
   }
}
