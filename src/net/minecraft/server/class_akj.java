package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.UtilColor;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahi;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_akr;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aoa;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_apx;
import net.minecraft.server.class_aqt;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.class_arn;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;

public class class_akj extends class_ajm {
   public static final class_arn a = class_akr.H;
   public static final class_arm b = class_arm.a("conditional");

   public class_akj(MaterialMapColor var1) {
      super(Material.f, var1);
      this.w(this.A.b().set(a, EnumDirection.NORTH).set(b, Boolean.valueOf(false)));
   }

   public TileEntity a(World var1, int var2) {
      class_apx var3 = new class_apx();
      var3.b(this == Blocks.dd);
      return var3;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(!var1.E) {
         TileEntity var5 = var1.r(var2);
         if(var5 instanceof class_apx) {
            class_apx var6 = (class_apx)var5;
            boolean var7 = var1.y(var2);
            boolean var8 = var6.d();
            boolean var9 = var6.e();
            if(var7 && !var8) {
               var6.a(true);
               if(var6.i() != class_apx.class_a_in_class_apx.SEQUENCE && !var9) {
                  boolean var10 = !var6.j() || this.e(var1, var2, var3);
                  var6.c(var10);
                  var1.a((BlockPosition)var2, (Block)this, this.a(var1));
                  if(var10) {
                     this.c(var1, var2);
                  }
               }
            } else if(!var7 && var8) {
               var6.a(false);
            }

         }
      }
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(!var1.E) {
         TileEntity var5 = var1.r(var2);
         if(var5 instanceof class_apx) {
            class_apx var6 = (class_apx)var5;
            class_ahi var7 = var6.b();
            boolean var8 = !UtilColor.b(var7.m());
            class_apx.class_a_in_class_apx var9 = var6.i();
            boolean var10 = !var6.j() || this.e(var1, var2, var3);
            boolean var11 = var6.g();
            boolean var12 = false;
            if(var9 != class_apx.class_a_in_class_apx.SEQUENCE && var11 && var8) {
               var7.a(var1);
               var12 = true;
            }

            if(var6.d() || var6.e()) {
               if(var9 == class_apx.class_a_in_class_apx.SEQUENCE && var10 && var8) {
                  var7.a(var1);
                  var12 = true;
               }

               if(var9 == class_apx.class_a_in_class_apx.AUTO) {
                  var1.a((BlockPosition)var2, (Block)this, this.a(var1));
                  if(var10) {
                     this.c(var1, var2);
                  }
               }
            }

            if(!var12) {
               var7.a(0);
            }

            var6.c(var10);
            var1.f(var2, this);
         }

      }
   }

   public boolean e(World var1, BlockPosition var2, IBlockData var3) {
      EnumDirection var4 = (EnumDirection)var3.get(a);
      TileEntity var5 = var1.r(var2.a(var4.d()));
      return var5 instanceof class_apx && ((class_apx)var5).b().k() > 0;
   }

   public int a(World var1) {
      return 1;
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      TileEntity var11 = var1.r(var2);
      if(var11 instanceof class_apx) {
         if(!var4.abilities.d) {
            return false;
         } else {
            var4.a((class_apx)var11);
            return true;
         }
      } else {
         return false;
      }
   }

   public boolean v(IBlockData var1) {
      return true;
   }

   public int d(IBlockData var1, World var2, BlockPosition var3) {
      TileEntity var4 = var2.r(var3);
      return var4 instanceof class_apx?((class_apx)var4).b().k():0;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, class_rz var4, ItemStack var5) {
      TileEntity var6 = var1.r(var2);
      if(var6 instanceof class_apx) {
         class_apx var7 = (class_apx)var6;
         class_ahi var8 = var7.b();
         if(var5.s()) {
            var8.b(var5.q());
         }

         if(!var1.E) {
            NBTTagCompound var9 = var5.o();
            if(var9 == null || !var9.b("BlockEntityTag", 10)) {
               var8.a(var1.U().b("sendCommandFeedback"));
               var7.b(this == Blocks.dd);
            }

            if(var7.i() == class_apx.class_a_in_class_apx.SEQUENCE) {
               boolean var10 = var1.y(var2);
               var7.a(var10);
            }
         }

      }
   }

   public int a(Random var1) {
      return 0;
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.MODEL;
   }

   public IBlockData a(int var1) {
      return this.u().set(a, EnumDirection.a(var1 & 7)).set(b, Boolean.valueOf((var1 & 8) != 0));
   }

   public int e(IBlockData var1) {
      return ((EnumDirection)var1.get(a)).a() | (((Boolean)var1.get(b)).booleanValue()?8:0);
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(a, var2.a((EnumDirection)var1.get(a)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(a)));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b});
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return this.u().set(a, class_aqt.a(var2, var8)).set(b, Boolean.valueOf(false));
   }

   public void c(World var1, BlockPosition var2) {
      IBlockData var3 = var1.getType(var2);
      if(var3.getBlock() == Blocks.bX || var3.getBlock() == Blocks.dc) {
         BlockPosition.class_a_in_class_cj var4 = new BlockPosition.class_a_in_class_cj(var2);
         var4.c((EnumDirection)var3.get(a));

         for(TileEntity var5 = var1.r(var4); var5 instanceof class_apx; var5 = var1.r(var4)) {
            class_apx var6 = (class_apx)var5;
            if(var6.i() != class_apx.class_a_in_class_apx.SEQUENCE) {
               break;
            }

            IBlockData var7 = var1.getType(var4);
            Block var8 = var7.getBlock();
            if(var8 != Blocks.dd || var1.b((BlockPosition)var4, (Block)var8)) {
               break;
            }

            var1.a(new BlockPosition(var4), var8, this.a(var1));
            var4.c((EnumDirection)var7.get(a));
         }

      }
   }
}
