package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.class_acb;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aot;
import net.minecraft.server.class_apk;
import net.minecraft.server.class_aqm;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;

public class class_aen extends Item {
   public class_aen() {
      this.j = 16;
      this.a(CreativeModeTab.c);
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      IBlockData var10 = var3.getType(var4);
      boolean var11 = var10.getBlock().a((class_ahw)var3, (BlockPosition)var4);
      if(var6 == EnumDirection.DOWN || !var10.getMaterial().a() && !var11 || var11 && var6 != EnumDirection.UP) {
         return EnumResult.FAIL;
      } else {
         var4 = var4.a(var6);
         if(var2.a(var4, var6, var1) && Blocks.an.a(var3, var4)) {
            if(var3.E) {
               return EnumResult.SUCCESS;
            } else {
               var4 = var11?var4.b():var4;
               if(var6 == EnumDirection.UP) {
                  int var12 = MathHelper.c((double)((var2.yaw + 180.0F) * 16.0F / 360.0F) + 0.5D) & 15;
                  var3.a((BlockPosition)var4, (IBlockData)Blocks.an.u().set(class_aot.b, Integer.valueOf(var12)), 11);
               } else {
                  var3.a((BlockPosition)var4, (IBlockData)Blocks.ax.u().set(class_apk.b, var6), 11);
               }

               --var1.b;
               TileEntity var13 = var3.r(var4);
               if(var13 instanceof class_aqm && !class_acb.a(var3, var2, var4, var1)) {
                  var2.a((class_aqm)var13);
               }

               return EnumResult.SUCCESS;
            }
         } else {
            return EnumResult.FAIL;
         }
      }
   }
}
