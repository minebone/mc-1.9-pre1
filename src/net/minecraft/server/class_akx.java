package net.minecraft.server;

import net.minecraft.server.EnumDirection;
import net.minecraft.server.TileEntity;
import net.minecraft.server.TileEntityDispenser;
import net.minecraft.server.TileEntityDropper;
import net.minecraft.server.TileEntityHopper;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_akt;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_cl;
import net.minecraft.server.class_cn;
import net.minecraft.server.class_cr;
import net.minecraft.server.IInventory;

public class class_akx extends class_akt {
   private final class_cr e = new class_cn();

   protected class_cr a(ItemStack var1) {
      return this.e;
   }

   public TileEntity a(World var1, int var2) {
      return new TileEntityDropper();
   }

   protected void c(World var1, BlockPosition var2) {
      class_cl var3 = new class_cl(var1, var2);
      TileEntityDispenser var4 = (TileEntityDispenser)var3.h();
      if(var4 != null) {
         int var5 = var4.m();
         if(var5 < 0) {
            var1.b(1001, var2, 0);
         } else {
            ItemStack var6 = var4.a(var5);
            if(var6 != null) {
               EnumDirection var7 = (EnumDirection)var1.getType(var2).get(a);
               BlockPosition var8 = var2.a(var7);
               IInventory var9 = TileEntityHopper.b(var1, (double)var8.p(), (double)var8.q(), (double)var8.r());
               ItemStack var10;
               if(var9 == null) {
                  var10 = this.e.a(var3, var6);
                  if(var10 != null && var10.b <= 0) {
                     var10 = null;
                  }
               } else {
                  var10 = TileEntityHopper.a(var9, var6.k().a(1), var7.d());
                  if(var10 == null) {
                     var10 = var6.k();
                     if(--var10.b <= 0) {
                        var10 = null;
                     }
                  } else {
                     var10 = var6.k();
                  }
               }

               var4.a(var5, var10);
            }
         }
      }
   }
}
