package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_avf;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_aue extends class_avf {
   private final IBlockData a;
   private final IBlockData b;

   public class_aue(IBlockData var1, IBlockData var2) {
      super(false);
      this.b = var1;
      this.a = var2;
   }

   public boolean b(World var1, Random var2, BlockPosition var3) {
      IBlockData var4;
      while(((var4 = var1.getType(var3)).getMaterial() == Material.a || var4.getMaterial() == Material.j) && var3.q() > 0) {
         var3 = var3.b();
      }

      Block var5 = var1.getType(var3).getBlock();
      if(var5 == Blocks.d || var5 == Blocks.c) {
         var3 = var3.a();
         this.a(var1, var3, this.b);

         for(int var6 = var3.q(); var6 <= var3.q() + 2; ++var6) {
            int var7 = var6 - var3.q();
            int var8 = 2 - var7;

            for(int var9 = var3.p() - var8; var9 <= var3.p() + var8; ++var9) {
               int var10 = var9 - var3.p();

               for(int var11 = var3.r() - var8; var11 <= var3.r() + var8; ++var11) {
                  int var12 = var11 - var3.r();
                  if(Math.abs(var10) != var8 || Math.abs(var12) != var8 || var2.nextInt(2) != 0) {
                     BlockPosition var13 = new BlockPosition(var9, var6, var11);
                     Material var14 = var1.getType(var13).getMaterial();
                     if(var14 == Material.a || var14 == Material.j) {
                        this.a(var1, var13, this.a);
                     }
                  }
               }
            }
         }
      }

      return true;
   }
}
