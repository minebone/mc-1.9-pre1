package net.minecraft.server;

import com.google.common.collect.Maps;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Map.Entry;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.Entity;
import net.minecraft.server.Scoreboard;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_bab;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_bbk;
import net.minecraft.server.class_kk;
import net.minecraft.server.EntityPlayer;

public class class_bap implements class_baq {
   private final Map a;
   private final class_azy.class_b_in_class_azy b;

   public class_bap(Map var1, class_azy.class_b_in_class_azy var2) {
      this.a = var1;
      this.b = var2;
   }

   public boolean a(Random var1, class_azy var2) {
      Entity var3 = var2.a(this.b);
      if(var3 == null) {
         return false;
      } else {
         Scoreboard var4 = var3.world.ad();
         Iterator var5 = this.a.entrySet().iterator();

         Entry var6;
         do {
            if(!var5.hasNext()) {
               return true;
            }

            var6 = (Entry)var5.next();
         } while(this.a(var3, var4, (String)var6.getKey(), (class_bab)var6.getValue()));

         return false;
      }
   }

   protected boolean a(Entity var1, Scoreboard var2, String var3, class_bab var4) {
      class_bbk var5 = var2.b(var3);
      if(var5 == null) {
         return false;
      } else {
         String var6 = var1 instanceof EntityPlayer?var1.h_():var1.getUniqueId().toString();
         return !var2.b(var6, var5)?false:var4.a(var2.c(var6, var5).c());
      }
   }

   public static class class_a_in_class_bap extends class_baq.class_a_in_class_baq {
      protected class_a_in_class_bap() {
         super(new class_kk("entity_scores"), class_bap.class);
      }

      public void a(JsonObject var1, class_bap var2, JsonSerializationContext var3) {
         JsonObject var4 = new JsonObject();
         Iterator var5 = var2.a.entrySet().iterator();

         while(var5.hasNext()) {
            Entry var6 = (Entry)var5.next();
            var4.add((String)var6.getKey(), var3.serialize(var6.getValue()));
         }

         var1.add("scores", var4);
         var1.add("entity", var3.serialize(var2.b));
      }

      public class_bap a(JsonObject var1, JsonDeserializationContext var2) {
         Set var3 = ChatDeserializer.t(var1, "scores").entrySet();
         LinkedHashMap var4 = Maps.newLinkedHashMap();
         Iterator var5 = var3.iterator();

         while(var5.hasNext()) {
            Entry var6 = (Entry)var5.next();
            var4.put(var6.getKey(), ChatDeserializer.a((JsonElement)var6.getValue(), "score", var2, class_bab.class));
         }

         return new class_bap(var4, (class_azy.class_b_in_class_azy)ChatDeserializer.a(var1, "entity", var2, class_azy.class_b_in_class_azy.class));
      }

      // $FF: synthetic method
      public class_baq b(JsonObject var1, JsonDeserializationContext var2) {
         return this.a(var1, var2);
      }

      // $FF: synthetic method
      // $FF: bridge method
      public void a(JsonObject var1, class_baq var2, JsonSerializationContext var3) {
         this.a(var1, (class_bap)var2, var3);
      }
   }
}
