package net.minecraft.server;

import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDifficulty;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.class_xy;

public class class_xz extends class_xy {
   private int b;
   public long a;
   private int c;
   private final boolean d;

   public class_xz(World var1, double var2, double var4, double var6, boolean var8) {
      super(var1);
      this.b(var2, var4, var6, 0.0F, 0.0F);
      this.b = 2;
      this.a = this.random.nextLong();
      this.c = this.random.nextInt(3) + 1;
      this.d = var8;
      BlockPosition var9 = new BlockPosition(this);
      if(!var8 && !var1.E && var1.U().b("doFireTick") && (var1.ae() == EnumDifficulty.NORMAL || var1.ae() == EnumDifficulty.HARD) && var1.a((BlockPosition)var9, (int)10)) {
         if(var1.getType(var9).getMaterial() == Material.a && Blocks.ab.a(var1, var9)) {
            var1.a(var9, Blocks.ab.u());
         }

         for(int var10 = 0; var10 < 4; ++var10) {
            BlockPosition var11 = var9.a(this.random.nextInt(3) - 1, this.random.nextInt(3) - 1, this.random.nextInt(3) - 1);
            if(var1.getType(var11).getMaterial() == Material.a && Blocks.ab.a(var1, var11)) {
               var1.a(var11, Blocks.ab.u());
            }
         }
      }

   }

   public EnumSoundCategory by() {
      return EnumSoundCategory.WEATHER;
   }

   public void m() {
      super.m();
      if(this.b == 2) {
         this.world.a((EntityHuman)null, this.locX, this.locY, this.locZ, class_ng.de, EnumSoundCategory.WEATHER, 10000.0F, 0.8F + this.random.nextFloat() * 0.2F);
         this.world.a((EntityHuman)null, this.locX, this.locY, this.locZ, class_ng.dd, EnumSoundCategory.WEATHER, 2.0F, 0.5F + this.random.nextFloat() * 0.2F);
      }

      --this.b;
      if(this.b < 0) {
         if(this.c == 0) {
            this.S();
         } else if(this.b < -this.random.nextInt(10)) {
            --this.c;
            this.b = 1;
            if(!this.d && !this.world.E) {
               this.a = this.random.nextLong();
               BlockPosition var1 = new BlockPosition(this);
               if(this.world.U().b("doFireTick") && this.world.a((BlockPosition)var1, (int)10) && this.world.getType(var1).getMaterial() == Material.a && Blocks.ab.a(this.world, var1)) {
                  this.world.a(var1, Blocks.ab.u());
               }
            }
         }
      }

      if(this.b >= 0) {
         if(this.world.E) {
            this.world.d(2);
         } else if(!this.d) {
            double var6 = 3.0D;
            List var3 = this.world.b((Entity)this, (AxisAlignedBB)(new AxisAlignedBB(this.locX - var6, this.locY - var6, this.locZ - var6, this.locX + var6, this.locY + 6.0D + var6, this.locZ + var6)));

            for(int var4 = 0; var4 < var3.size(); ++var4) {
               Entity var5 = (Entity)var3.get(var4);
               var5.a(this);
            }
         }
      }

   }

   protected void i() {
   }

   protected void a(NBTTagCompound var1) {
   }

   protected void b(NBTTagCompound var1) {
   }
}
