package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.World;
import net.minecraft.server.class_ajn;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.class_rz;

public class BlockPressurePlateBinary extends class_ajn {
   public static final class_arm d = class_arm.a("powered");
   private final BlockPressurePlateBinary.EnumMobType e;

   protected BlockPressurePlateBinary(Material var1, BlockPressurePlateBinary.EnumMobType var2) {
      super(var1);
      this.w(this.A.b().set(d, Boolean.valueOf(false)));
      this.e = var2;
   }

   protected int i(IBlockData var1) {
      return ((Boolean)var1.get(d)).booleanValue()?15:0;
   }

   protected IBlockData a(IBlockData var1, int var2) {
      return var1.set(d, Boolean.valueOf(var2 > 0));
   }

   protected void b(World var1, BlockPosition var2) {
      if(this.x == Material.d) {
         var1.a((EntityHuman)null, var2, class_ng.gX, EnumSoundCategory.BLOCKS, 0.3F, 0.8F);
      } else {
         var1.a((EntityHuman)null, var2, class_ng.ga, EnumSoundCategory.BLOCKS, 0.3F, 0.6F);
      }

   }

   protected void c(World var1, BlockPosition var2) {
      if(this.x == Material.d) {
         var1.a((EntityHuman)null, var2, class_ng.gW, EnumSoundCategory.BLOCKS, 0.3F, 0.7F);
      } else {
         var1.a((EntityHuman)null, var2, class_ng.fZ, EnumSoundCategory.BLOCKS, 0.3F, 0.5F);
      }

   }

   protected int e(World var1, BlockPosition var2) {
      AxisAlignedBB var3 = c.a(var2);
      List var4;
      switch(BlockPressurePlateBinary.SyntheticClass_1.a[this.e.ordinal()]) {
      case 1:
         var4 = var1.b((Entity)null, (AxisAlignedBB)var3);
         break;
      case 2:
         var4 = var1.a(class_rz.class, var3);
         break;
      default:
         return 0;
      }

      if(!var4.isEmpty()) {
         Iterator var5 = var4.iterator();

         while(var5.hasNext()) {
            Entity var6 = (Entity)var5.next();
            if(!var6.aZ()) {
               return 15;
            }
         }
      }

      return 0;
   }

   public IBlockData a(int var1) {
      return this.u().set(d, Boolean.valueOf(var1 == 1));
   }

   public int e(IBlockData var1) {
      return ((Boolean)var1.get(d)).booleanValue()?1:0;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{d});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[BlockPressurePlateBinary.EnumMobType.values().length];

      static {
         try {
            a[BlockPressurePlateBinary.EnumMobType.EVERYTHING.ordinal()] = 1;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[BlockPressurePlateBinary.EnumMobType.MOBS.ordinal()] = 2;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum EnumMobType {
      EVERYTHING,
      MOBS;
   }
}
