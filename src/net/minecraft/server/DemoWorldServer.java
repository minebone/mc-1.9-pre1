package net.minecraft.server;

import net.minecraft.server.MinecraftServer;
import net.minecraft.server.WorldData;
import net.minecraft.server.WorldServer;
import net.minecraft.server.WorldSettings;
import net.minecraft.server.WorldType;
import net.minecraft.server.class_azh;
import net.minecraft.server.class_oo;

public class DemoWorldServer extends WorldServer {
   private static final long I = (long)"North Carolina".hashCode();
   public static final WorldSettings a = (new WorldSettings(I, WorldSettings.EnumGamemode.SURVIVAL, true, false, WorldType.b)).a();

   public DemoWorldServer(MinecraftServer var1, class_azh var2, WorldData var3, int var4, class_oo var5) {
      super(var1, var2, var3, var4, var5);
      this.x.a(a);
   }
}
