package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.List;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_qg;
import net.minecraft.server.class_qh;

public class class_qu implements IInventory {
   private String a;
   private int b;
   private ItemStack[] c;
   private List d;
   private boolean e;

   public class_qu(String var1, boolean var2, int var3) {
      this.a = var1;
      this.e = var2;
      this.b = var3;
      this.c = new ItemStack[var3];
   }

   public void a(class_qh var1) {
      if(this.d == null) {
         this.d = Lists.newArrayList();
      }

      this.d.add(var1);
   }

   public void b(class_qh var1) {
      this.d.remove(var1);
   }

   public ItemStack a(int var1) {
      return var1 >= 0 && var1 < this.c.length?this.c[var1]:null;
   }

   public ItemStack a(int var1, int var2) {
      ItemStack var3 = class_qg.a(this.c, var1, var2);
      if(var3 != null) {
         this.v_();
      }

      return var3;
   }

   public ItemStack a(ItemStack var1) {
      ItemStack var2 = var1.k();

      for(int var3 = 0; var3 < this.b; ++var3) {
         ItemStack var4 = this.a(var3);
         if(var4 == null) {
            this.a(var3, var2);
            this.v_();
            return null;
         }

         if(ItemStack.c(var4, var2)) {
            int var5 = Math.min(this.w_(), var4.c());
            int var6 = Math.min(var2.b, var5 - var4.b);
            if(var6 > 0) {
               var4.b += var6;
               var2.b -= var6;
               if(var2.b <= 0) {
                  this.v_();
                  return null;
               }
            }
         }
      }

      if(var2.b != var1.b) {
         this.v_();
      }

      return var2;
   }

   public ItemStack b(int var1) {
      if(this.c[var1] != null) {
         ItemStack var2 = this.c[var1];
         this.c[var1] = null;
         return var2;
      } else {
         return null;
      }
   }

   public void a(int var1, ItemStack var2) {
      this.c[var1] = var2;
      if(var2 != null && var2.b > this.w_()) {
         var2.b = this.w_();
      }

      this.v_();
   }

   public int u_() {
      return this.b;
   }

   public String h_() {
      return this.a;
   }

   public boolean o_() {
      return this.e;
   }

   public void a(String var1) {
      this.e = true;
      this.a = var1;
   }

   public IChatBaseComponent i_() {
      return (IChatBaseComponent)(this.o_()?new ChatComponentText(this.h_()):new ChatMessage(this.h_(), new Object[0]));
   }

   public int w_() {
      return 64;
   }

   public void v_() {
      if(this.d != null) {
         for(int var1 = 0; var1 < this.d.size(); ++var1) {
            ((class_qh)this.d.get(var1)).a(this);
         }
      }

   }

   public boolean a(EntityHuman var1) {
      return true;
   }

   public void b(EntityHuman var1) {
   }

   public void c(EntityHuman var1) {
   }

   public boolean b(int var1, ItemStack var2) {
      return true;
   }

   public int c_(int var1) {
      return 0;
   }

   public void b(int var1, int var2) {
   }

   public int g() {
      return 0;
   }

   public void l() {
      for(int var1 = 0; var1 < this.c.length; ++var1) {
         this.c[var1] = null;
      }

   }
}
