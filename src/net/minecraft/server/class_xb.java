package net.minecraft.server;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.Vec3D;
import net.minecraft.server.class_aub;
import net.minecraft.server.class_aym;
import net.minecraft.server.class_ayo;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_wt;
import net.minecraft.server.class_wv;
import net.minecraft.server.class_xj;

public class class_xb extends class_wv {
   private class_ayo b;
   private Vec3D c;

   public class_xb(class_wt var1) {
      super(var1);
   }

   public class_xj i() {
      return class_xj.c;
   }

   public void d() {
      this.b = null;
      this.c = null;
   }

   public void c() {
      double var1 = this.c == null?0.0D:this.c.c(this.a.locX, this.a.locY, this.a.locZ);
      if(var1 < 100.0D || var1 > 22500.0D || this.a.positionChanged || this.a.B) {
         this.j();
      }

   }

   public Vec3D g() {
      return this.c;
   }

   private void j() {
      if(this.b == null || this.b.b()) {
         int var1 = this.a.o();
         BlockPosition var2 = this.a.world.q(class_aub.a);
         EntityHuman var3 = this.a.world.a(var2, 128.0D, 128.0D);
         int var4;
         if(var3 != null) {
            Vec3D var5 = (new Vec3D(var3.locX, 0.0D, var3.locZ)).a();
            var4 = this.a.l(-var5.b * 40.0D, 105.0D, -var5.d * 40.0D);
         } else {
            var4 = this.a.l(40.0D, (double)var2.q(), 0.0D);
         }

         class_aym var6 = new class_aym(var2.p(), var2.q(), var2.r());
         this.b = this.a.a(var1, var4, var6);
         if(this.b != null) {
            this.b.a();
         }
      }

      this.k();
      if(this.b != null && this.b.b()) {
         this.a.cT().a(class_xj.d);
      }

   }

   private void k() {
      if(this.b != null && !this.b.b()) {
         Vec3D var1 = this.b.f();
         this.b.a();
         double var2 = var1.b;
         double var4 = var1.d;

         double var6;
         do {
            var6 = var1.c + (double)(this.a.bE().nextFloat() * 20.0F);
         } while(var6 < var1.c);

         this.c = new Vec3D(var2, var6, var4);
      }

   }
}
