package net.minecraft.server;

import java.util.Arrays;
import net.minecraft.server.BlockBrewingStand;
import net.minecraft.server.Container;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.class_aax;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_afe;
import net.minecraft.server.class_aqi;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ky;
import net.minecraft.server.class_qg;
import net.minecraft.server.class_qi;
import net.minecraft.server.class_qx;

public class TileEntityBrewingStand extends class_aqi implements class_ky, class_qx {
   private static final int[] a = new int[]{3};
   private static final int[] f = new int[]{0, 1, 2, 3};
   private static final int[] g = new int[]{0, 1, 2, 4};
   private ItemStack[] h = new ItemStack[5];
   private int i;
   private boolean[] j;
   private Item k;
   private String l;
   private int m;

   public String h_() {
      return this.o_()?this.l:"container.brewing";
   }

   public boolean o_() {
      return this.l != null && !this.l.isEmpty();
   }

   public void a(String var1) {
      this.l = var1;
   }

   public int u_() {
      return this.h.length;
   }

   public void c() {
      if(this.m <= 0 && this.h[4] != null && this.h[4].b() == Items.bN) {
         this.m = 20;
         --this.h[4].b;
         if(this.h[4].b <= 0) {
            this.h[4] = null;
         }

         this.v_();
      }

      boolean var1 = this.n();
      boolean var2 = this.i > 0;
      if(var2) {
         --this.i;
         boolean var3 = this.i == 0;
         if(var3 && var1) {
            this.o();
            this.v_();
         } else if(!var1) {
            this.i = 0;
            this.v_();
         } else if(this.k != this.h[3].b()) {
            this.i = 0;
            this.v_();
         }
      } else if(var1 && this.m > 0) {
         --this.m;
         this.i = 400;
         this.k = this.h[3].b();
         this.v_();
      }

      if(!this.b.E) {
         boolean[] var6 = this.m();
         if(!Arrays.equals(var6, this.j)) {
            this.j = var6;
            IBlockData var4 = this.b.getType(this.v());
            if(!(var4.getBlock() instanceof BlockBrewingStand)) {
               return;
            }

            for(int var5 = 0; var5 < BlockBrewingStand.a.length; ++var5) {
               var4 = var4.set(BlockBrewingStand.a[var5], Boolean.valueOf(var6[var5]));
            }

            this.b.a((BlockPosition)this.c, (IBlockData)var4, 2);
         }
      }

   }

   public boolean[] m() {
      boolean[] var1 = new boolean[3];

      for(int var2 = 0; var2 < 3; ++var2) {
         if(this.h[var2] != null) {
            var1[var2] = true;
         }
      }

      return var1;
   }

   private boolean n() {
      if(this.h[3] != null && this.h[3].b > 0) {
         ItemStack var1 = this.h[3];
         if(!class_afe.a(var1)) {
            return false;
         } else {
            for(int var2 = 0; var2 < 3; ++var2) {
               ItemStack var3 = this.h[var2];
               if(var3 != null && class_afe.a(var3, var1)) {
                  return true;
               }
            }

            return false;
         }
      } else {
         return false;
      }
   }

   private void o() {
      ItemStack var1 = this.h[3];

      for(int var2 = 0; var2 < 3; ++var2) {
         this.h[var2] = class_afe.d(var1, this.h[var2]);
      }

      --var1.b;
      BlockPosition var4 = this.v();
      if(var1.b().r()) {
         ItemStack var3 = new ItemStack(var1.b().q());
         if(var1.b <= 0) {
            var1 = var3;
         } else {
            class_qi.a(this.b, (double)var4.p(), (double)var4.q(), (double)var4.r(), var3);
         }
      }

      if(var1.b <= 0) {
         var1 = null;
      }

      this.h[3] = var1;
      this.b.b(1035, var4, 0);
   }

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      NBTTagList var3 = var2.c("Items", 10);
      this.h = new ItemStack[this.u_()];

      for(int var4 = 0; var4 < var3.c(); ++var4) {
         NBTTagCompound var5 = var3.b(var4);
         byte var6 = var5.f("Slot");
         if(var6 >= 0 && var6 < this.h.length) {
            this.h[var6] = ItemStack.a(var5);
         }
      }

      this.i = var2.g("BrewTime");
      if(var2.b("CustomName", 8)) {
         this.l = var2.l("CustomName");
      }

      this.m = var2.f("Fuel");
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      var1.a("BrewTime", (short)this.i);
      NBTTagList var2 = new NBTTagList();

      for(int var3 = 0; var3 < this.h.length; ++var3) {
         if(this.h[var3] != null) {
            NBTTagCompound var4 = new NBTTagCompound();
            var4.a("Slot", (byte)var3);
            this.h[var3].b(var4);
            var2.a((NBTTag)var4);
         }
      }

      var1.a((String)"Items", (NBTTag)var2);
      if(this.o_()) {
         var1.a("CustomName", this.l);
      }

      var1.a("Fuel", (byte)this.m);
   }

   public ItemStack a(int var1) {
      return var1 >= 0 && var1 < this.h.length?this.h[var1]:null;
   }

   public ItemStack a(int var1, int var2) {
      return class_qg.a(this.h, var1, var2);
   }

   public ItemStack b(int var1) {
      return class_qg.a(this.h, var1);
   }

   public void a(int var1, ItemStack var2) {
      if(var1 >= 0 && var1 < this.h.length) {
         this.h[var1] = var2;
      }

   }

   public int w_() {
      return 64;
   }

   public boolean a(EntityHuman var1) {
      return this.b.r(this.c) != this?false:var1.e((double)this.c.p() + 0.5D, (double)this.c.q() + 0.5D, (double)this.c.r() + 0.5D) <= 64.0D;
   }

   public void b(EntityHuman var1) {
   }

   public void c(EntityHuman var1) {
   }

   public boolean b(int var1, ItemStack var2) {
      if(var1 == 3) {
         return class_afe.a(var2);
      } else {
         Item var3 = var2.b();
         return var1 == 4?var3 == Items.bN:var3 == Items.bG || var3 == Items.bH || var3 == Items.bI || var3 == Items.bJ;
      }
   }

   public int[] a(EnumDirection var1) {
      return var1 == EnumDirection.UP?a:(var1 == EnumDirection.DOWN?f:g);
   }

   public boolean a(int var1, ItemStack var2, EnumDirection var3) {
      return this.b(var1, var2);
   }

   public boolean b(int var1, ItemStack var2, EnumDirection var3) {
      return var1 == 3?var2.b() == Items.bJ:true;
   }

   public String k() {
      return "minecraft:brewing_stand";
   }

   public Container a(PlayerInventory var1, EntityHuman var2) {
      return new class_aax(var1, this);
   }

   public int c_(int var1) {
      switch(var1) {
      case 0:
         return this.i;
      case 1:
         return this.m;
      default:
         return 0;
      }
   }

   public void b(int var1, int var2) {
      switch(var1) {
      case 0:
         this.i = var2;
         break;
      case 1:
         this.m = var2;
      }

   }

   public int g() {
      return 2;
   }

   public void l() {
      Arrays.fill(this.h, (Object)null);
   }
}
