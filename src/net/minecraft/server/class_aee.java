package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Map;
import net.minecraft.server.BlockJukeBox;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumItemRarity;
import net.minecraft.server.Item;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_nf;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;

public class class_aee extends Item {
   private static final Map a = Maps.newHashMap();
   private final class_nf b;
   private final String c;

   protected class_aee(String var1, class_nf var2) {
      this.c = "item.record." + var1 + ".desc";
      this.b = var2;
      this.j = 1;
      this.a(CreativeModeTab.f);
      a.put(this.b, this);
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      IBlockData var10 = var3.getType(var4);
      if(var10.getBlock() == Blocks.aN && !((Boolean)var10.get(BlockJukeBox.a)).booleanValue()) {
         if(!var3.E) {
            ((BlockJukeBox)Blocks.aN).a(var3, var4, var10, var1);
            var3.a((EntityHuman)null, 1010, var4, Item.a((Item)this));
            --var1.b;
            var2.b(StatisticList.Z);
         }

         return EnumResult.SUCCESS;
      } else {
         return EnumResult.PASS;
      }
   }

   public EnumItemRarity g(ItemStack var1) {
      return EnumItemRarity.RARE;
   }
}
