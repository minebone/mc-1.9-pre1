package net.minecraft.server;

import com.google.common.base.Predicate;
import net.minecraft.server.Block;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.class_arn;
import net.minecraft.server.Material;

public abstract class class_amf extends Block {
   public static final class_arn D = class_arn.a("facing", (Predicate)EnumDirection.EnumDirectionLimit.HORIZONTAL);

   protected class_amf(Material var1) {
      super(var1);
   }

   protected class_amf(Material var1, MaterialMapColor var2) {
      super(var1, var2);
   }
}
