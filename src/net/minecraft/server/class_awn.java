package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityPainting;
import net.minecraft.server.EntityTypes;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagDouble;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.StructureBoundingBox;
import net.minecraft.server.TileEntity;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_aqo;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_awm;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.BaseBlockPosition;
import net.minecraft.server.class_dt;
import net.minecraft.server.IInventory;

public class class_awn {
   private final List a = Lists.newArrayList();
   private final List b = Lists.newArrayList();
   private BlockPosition c = BlockPosition.a;
   private String d = "?";

   public BlockPosition a() {
      return this.c;
   }

   public void a(String var1) {
      this.d = var1;
   }

   public String b() {
      return this.d;
   }

   public void a(World var1, BlockPosition var2, BlockPosition var3, boolean var4, Block var5) {
      if(var3.p() >= 1 && var3.q() >= 1 && var3.r() >= 1) {
         BlockPosition var6 = var2.a((BaseBlockPosition)var3).a(-1, -1, -1);
         ArrayList var7 = Lists.newArrayList();
         ArrayList var8 = Lists.newArrayList();
         ArrayList var9 = Lists.newArrayList();
         BlockPosition var10 = new BlockPosition(Math.min(var2.p(), var6.p()), Math.min(var2.q(), var6.q()), Math.min(var2.r(), var6.r()));
         BlockPosition var11 = new BlockPosition(Math.max(var2.p(), var6.p()), Math.max(var2.q(), var6.q()), Math.max(var2.r(), var6.r()));
         this.c = var3;
         Iterator var12 = BlockPosition.b(var10, var11).iterator();

         while(true) {
            while(true) {
               BlockPosition.class_a_in_class_cj var13;
               BlockPosition var14;
               IBlockData var15;
               do {
                  if(!var12.hasNext()) {
                     this.a.clear();
                     this.a.addAll(var7);
                     this.a.addAll(var8);
                     this.a.addAll(var9);
                     if(var4) {
                        this.a(var1, var10, var11.a(1, 1, 1));
                     } else {
                        this.b.clear();
                     }

                     return;
                  }

                  var13 = (BlockPosition.class_a_in_class_cj)var12.next();
                  var14 = var13.b(var10);
                  var15 = var1.getType(var13);
               } while(var5 != null && var5 == var15.getBlock());

               TileEntity var16 = var1.r(var13);
               if(var16 != null) {
                  NBTTagCompound var17 = new NBTTagCompound();
                  var16.a(var17);
                  var17.q("x");
                  var17.q("y");
                  var17.q("z");
                  var8.add(new class_awn.class_a_in_class_awn(var14, var15, var17, null));
               } else if(!var15.b() && !var15.h()) {
                  var9.add(new class_awn.class_a_in_class_awn(var14, var15, (NBTTagCompound)null, null));
               } else {
                  var7.add(new class_awn.class_a_in_class_awn(var14, var15, (NBTTagCompound)null, null));
               }
            }
         }
      }
   }

   private void a(World var1, BlockPosition var2, BlockPosition var3) {
      List var4 = var1.a(Entity.class, new AxisAlignedBB(var2, var3), new Predicate() {
         public boolean a(Entity var1) {
            return !(var1 instanceof EntityHuman);
         }

         // $FF: synthetic method
         public boolean apply(Object var1) {
            return this.a((Entity)var1);
         }
      });
      this.b.clear();

      Vec3D var7;
      NBTTagCompound var8;
      BlockPosition var9;
      for(Iterator var5 = var4.iterator(); var5.hasNext(); this.b.add(new class_awn.class_b_in_class_awn(var7, var9, var8, null))) {
         Entity var6 = (Entity)var5.next();
         var7 = new Vec3D(var6.locX - (double)var2.p(), var6.locY - (double)var2.q(), var6.locZ - (double)var2.r());
         var8 = new NBTTagCompound();
         var6.d(var8);
         if(var6 instanceof EntityPainting) {
            var9 = ((EntityPainting)var6).q().b(var2);
         } else {
            var9 = new BlockPosition(var7);
         }
      }

   }

   public Map a(BlockPosition var1, class_awm var2) {
      HashMap var3 = Maps.newHashMap();
      StructureBoundingBox var4 = var2.g();
      Iterator var5 = this.a.iterator();

      while(true) {
         class_awn.class_a_in_class_awn var6;
         BlockPosition var7;
         do {
            if(!var5.hasNext()) {
               return var3;
            }

            var6 = (class_awn.class_a_in_class_awn)var5.next();
            var7 = a(var2, var6.a).a((BaseBlockPosition)var1);
         } while(var4 != null && !var4.b((BaseBlockPosition)var7));

         IBlockData var8 = var6.b;
         if(var8.getBlock() == Blocks.df && var6.c != null) {
            class_aqo.class_a_in_class_aqo var9 = class_aqo.class_a_in_class_aqo.valueOf(var6.c.l("mode"));
            if(var9 == class_aqo.class_a_in_class_aqo.DATA) {
               var3.put(var7, var6.c.l("metadata"));
            }
         }
      }
   }

   public BlockPosition a(class_awm var1, BlockPosition var2, class_awm var3, BlockPosition var4) {
      BlockPosition var5 = a(var1, var2);
      BlockPosition var6 = a(var3, var4);
      return var5.b(var6);
   }

   public static BlockPosition a(class_awm var0, BlockPosition var1) {
      return a(var1, var0.b(), var0.c());
   }

   public void a(World var1, BlockPosition var2, class_awm var3) {
      var3.i();
      this.b(var1, var2, var3);
   }

   public void b(World var1, BlockPosition var2, class_awm var3) {
      if(!this.a.isEmpty() && this.c.p() >= 1 && this.c.q() >= 1 && this.c.r() >= 1) {
         Block var4 = var3.f();
         StructureBoundingBox var5 = var3.g();
         Iterator var6 = this.a.iterator();

         while(true) {
            class_awn.class_a_in_class_awn var7;
            BlockPosition var9;
            do {
               Block var8;
               do {
                  do {
                     if(!var6.hasNext()) {
                        var6 = this.a.iterator();

                        while(true) {
                           BlockPosition var13;
                           do {
                              do {
                                 if(!var6.hasNext()) {
                                    if(!var3.e()) {
                                       this.a(var1, var2, var3.b(), var3.c(), var5);
                                    }

                                    return;
                                 }

                                 var7 = (class_awn.class_a_in_class_awn)var6.next();
                              } while(var4 != null && var4 == var7.b.getBlock());

                              var13 = a(var3, var7.a).a((BaseBlockPosition)var2);
                           } while(var5 != null && !var5.b((BaseBlockPosition)var13));

                           var1.c(var13, var7.b.getBlock());
                           if(var7.c != null) {
                              TileEntity var14 = var1.r(var13);
                              if(var14 != null) {
                                 var14.v_();
                              }
                           }
                        }
                     }

                     var7 = (class_awn.class_a_in_class_awn)var6.next();
                     var8 = var7.b.getBlock();
                  } while(var4 != null && var4 == var8);
               } while(var3.h() && var8 == Blocks.df);

               var9 = a(var3, var7.a).a((BaseBlockPosition)var2);
            } while(var5 != null && !var5.b((BaseBlockPosition)var9));

            IBlockData var10 = var7.b.a(var3.b());
            IBlockData var11 = var10.a(var3.c());
            TileEntity var12;
            if(var7.c != null) {
               var12 = var1.r(var9);
               if(var12 != null) {
                  if(var12 instanceof IInventory) {
                     ((IInventory)var12).l();
                  }

                  var1.a((BlockPosition)var9, (IBlockData)Blocks.cv.u(), 4);
               }
            }

            if(var1.a((BlockPosition)var9, (IBlockData)var11, 2) && var7.c != null) {
               var12 = var1.r(var9);
               if(var12 != null) {
                  var7.c.a("x", var9.p());
                  var7.c.a("y", var9.q());
                  var7.c.a("z", var9.r());
                  var12.a(var1.u(), var7.c);
               }
            }
         }
      }
   }

   private void a(World var1, BlockPosition var2, class_amq var3, class_aod var4, StructureBoundingBox var5) {
      Iterator var6 = this.b.iterator();

      while(true) {
         class_awn.class_b_in_class_awn var7;
         BlockPosition var8;
         do {
            if(!var6.hasNext()) {
               return;
            }

            var7 = (class_awn.class_b_in_class_awn)var6.next();
            var8 = a(var7.b, var3, var4).a((BaseBlockPosition)var2);
         } while(var5 != null && !var5.b((BaseBlockPosition)var8));

         NBTTagCompound var9 = var7.c;
         Vec3D var10 = a(var7.a, var3, var4);
         Vec3D var11 = var10.b((double)var2.p(), (double)var2.q(), (double)var2.r());
         NBTTagList var12 = new NBTTagList();
         var12.a((NBTTag)(new NBTTagDouble(var11.b)));
         var12.a((NBTTag)(new NBTTagDouble(var11.c)));
         var12.a((NBTTag)(new NBTTagDouble(var11.d)));
         var9.a((String)"Pos", (NBTTag)var12);
         var9.a("UUID", UUID.randomUUID());

         Entity var13;
         try {
            var13 = EntityTypes.a(var9, var1);
         } catch (Exception var15) {
            var13 = null;
         }

         if(var13 != null) {
            if(var13 instanceof EntityPainting) {
               var13.a(var3);
               var13.a(var4);
               var13.b((double)var8.p(), (double)var8.q(), (double)var8.r());
               var13.b(var11.b, var11.c, var11.d, var13.yaw, var13.pitch);
            } else {
               float var14 = var13.a(var3);
               var14 += var13.yaw - var13.a(var4);
               var13.b(var11.b, var11.c, var11.d, var14, var13.pitch);
            }

            var1.a(var13);
         }
      }
   }

   public BlockPosition a(class_aod var1) {
      switch(class_awn.SyntheticClass_1.a[var1.ordinal()]) {
      case 1:
      case 2:
         return new BlockPosition(this.c.r(), this.c.q(), this.c.p());
      default:
         return this.c;
      }
   }

   private static BlockPosition a(BlockPosition var0, class_amq var1, class_aod var2) {
      int var3 = var0.p();
      int var4 = var0.q();
      int var5 = var0.r();
      boolean var6 = true;
      switch(class_awn.SyntheticClass_1.b[var1.ordinal()]) {
      case 1:
         var5 = -var5;
         break;
      case 2:
         var3 = -var3;
         break;
      default:
         var6 = false;
      }

      switch(class_awn.SyntheticClass_1.a[var2.ordinal()]) {
      case 1:
         return new BlockPosition(var5, var4, -var3);
      case 2:
         return new BlockPosition(-var5, var4, var3);
      case 3:
         return new BlockPosition(-var3, var4, -var5);
      default:
         return var6?new BlockPosition(var3, var4, var5):var0;
      }
   }

   private static Vec3D a(Vec3D var0, class_amq var1, class_aod var2) {
      double var3 = var0.b;
      double var5 = var0.c;
      double var7 = var0.d;
      boolean var9 = true;
      switch(class_awn.SyntheticClass_1.b[var1.ordinal()]) {
      case 1:
         var7 = 1.0D - var7;
         break;
      case 2:
         var3 = 1.0D - var3;
         break;
      default:
         var9 = false;
      }

      switch(class_awn.SyntheticClass_1.a[var2.ordinal()]) {
      case 1:
         return new Vec3D(var7, var5, 1.0D - var3);
      case 2:
         return new Vec3D(1.0D - var7, var5, var3);
      case 3:
         return new Vec3D(1.0D - var3, var5, 1.0D - var7);
      default:
         return var9?new Vec3D(var3, var5, var7):var0;
      }
   }

   public void a(NBTTagCompound var1) {
      NBTTagList var2 = new NBTTagList();

      NBTTagCompound var5;
      for(Iterator var3 = this.a.iterator(); var3.hasNext(); var2.a((NBTTag)var5)) {
         class_awn.class_a_in_class_awn var4 = (class_awn.class_a_in_class_awn)var3.next();
         var5 = new NBTTagCompound();
         var5.a((String)"pos", (NBTTag)this.a(new int[]{var4.a.p(), var4.a.q(), var4.a.r()}));
         var5.a("state", Block.j(var4.b));
         if(var4.c != null) {
            var5.a((String)"nbt", (NBTTag)var4.c);
         }
      }

      NBTTagList var7 = new NBTTagList();

      NBTTagCompound var6;
      for(Iterator var8 = this.b.iterator(); var8.hasNext(); var7.a((NBTTag)var6)) {
         class_awn.class_b_in_class_awn var9 = (class_awn.class_b_in_class_awn)var8.next();
         var6 = new NBTTagCompound();
         var6.a((String)"pos", (NBTTag)this.a(new double[]{var9.a.b, var9.a.c, var9.a.d}));
         var6.a((String)"blockPos", (NBTTag)this.a(new int[]{var9.b.p(), var9.b.q(), var9.b.r()}));
         if(var9.c != null) {
            var6.a((String)"nbt", (NBTTag)var9.c);
         }
      }

      var1.a((String)"blocks", (NBTTag)var2);
      var1.a((String)"entities", (NBTTag)var7);
      var1.a((String)"size", (NBTTag)this.a(new int[]{this.c.p(), this.c.q(), this.c.r()}));
      var1.a("version", (int)1);
      var1.a("author", this.d);
   }

   public void b(NBTTagCompound var1) {
      this.a.clear();
      this.b.clear();
      NBTTagList var2 = var1.c("size", 3);
      this.c = new BlockPosition(var2.c(0), var2.c(1), var2.c(2));
      this.d = var1.l("author");
      NBTTagList var3 = var1.c("blocks", 10);

      for(int var4 = 0; var4 < var3.c(); ++var4) {
         NBTTagCompound var5 = var3.b(var4);
         NBTTagList var6 = var5.c("pos", 3);
         BlockPosition var7 = new BlockPosition(var6.c(0), var6.c(1), var6.c(2));
         int var8 = var5.h("state");
         IBlockData var9 = Block.c(var8);
         NBTTagCompound var10;
         if(var5.e("nbt")) {
            var10 = var5.o("nbt");
         } else {
            var10 = null;
         }

         this.a.add(new class_awn.class_a_in_class_awn(var7, var9, var10, null));
      }

      NBTTagList var12 = var1.c("entities", 10);

      for(int var13 = 0; var13 < var12.c(); ++var13) {
         NBTTagCompound var14 = var12.b(var13);
         NBTTagList var15 = var14.c("pos", 6);
         Vec3D var16 = new Vec3D(var15.e(0), var15.e(1), var15.e(2));
         NBTTagList var17 = var14.c("blockPos", 3);
         BlockPosition var18 = new BlockPosition(var17.c(0), var17.c(1), var17.c(2));
         if(var14.e("nbt")) {
            NBTTagCompound var11 = var14.o("nbt");
            this.b.add(new class_awn.class_b_in_class_awn(var16, var18, var11, null));
         }
      }

   }

   private NBTTagList a(int... var1) {
      NBTTagList var2 = new NBTTagList();
      int[] var3 = var1;
      int var4 = var1.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         int var6 = var3[var5];
         var2.a((NBTTag)(new class_dt(var6)));
      }

      return var2;
   }

   private NBTTagList a(double... var1) {
      NBTTagList var2 = new NBTTagList();
      double[] var3 = var1;
      int var4 = var1.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         double var6 = var3[var5];
         var2.a((NBTTag)(new NBTTagDouble(var6)));
      }

      return var2;
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b = new int[class_amq.values().length];

      static {
         try {
            b[class_amq.LEFT_RIGHT.ordinal()] = 1;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            b[class_amq.FRONT_BACK.ordinal()] = 2;
         } catch (NoSuchFieldError var4) {
            ;
         }

         a = new int[class_aod.values().length];

         try {
            a[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[class_aod.CLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[class_aod.CLOCKWISE_180.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   static class class_b_in_class_awn {
      public final Vec3D a;
      public final BlockPosition b;
      public final NBTTagCompound c;

      private class_b_in_class_awn(Vec3D var1, BlockPosition var2, NBTTagCompound var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      // $FF: synthetic method
      class_b_in_class_awn(Vec3D var1, BlockPosition var2, NBTTagCompound var3, Object var4) {
         this(var1, var2, var3);
      }
   }

   static class class_a_in_class_awn {
      public final BlockPosition a;
      public final IBlockData b;
      public final NBTTagCompound c;

      private class_a_in_class_awn(BlockPosition var1, IBlockData var2, NBTTagCompound var3) {
         this.a = var1;
         this.b = var2;
         this.c = var3;
      }

      // $FF: synthetic method
      class_a_in_class_awn(BlockPosition var1, IBlockData var2, NBTTagCompound var3, Object var4) {
         this(var1, var2, var3);
      }
   }
}
