package net.minecraft.server;

import java.util.UUID;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MojangsonParser;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.class_ec;
import net.minecraft.server.ICommandListener;

public class class_ac extends CommandAbstract {
   public String c() {
      return "entitydata";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.entitydata.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 2) {
         throw new class_cf("commands.entitydata.usage", new Object[0]);
      } else {
         Entity var4 = b(var1, var2, var3[0]);
         if(var4 instanceof EntityHuman) {
            throw new class_bz("commands.entitydata.noPlayers", new Object[]{var4.i_()});
         } else {
            NBTTagCompound var5 = a(var4);
            NBTTagCompound var6 = (NBTTagCompound)var5.b();

            NBTTagCompound var7;
            try {
               var7 = MojangsonParser.a(a(var2, var3, 1).c());
            } catch (class_ec var9) {
               throw new class_bz("commands.entitydata.tagError", new Object[]{var9.getMessage()});
            }

            UUID var8 = var4.getUniqueId();
            var5.a(var7);
            var4.a(var8);
            if(var5.equals(var6)) {
               throw new class_bz("commands.entitydata.failed", new Object[]{var5.toString()});
            } else {
               var4.f(var5);
               a(var2, this, "commands.entitydata.success", new Object[]{var5.toString()});
            }
         }
      }
   }

   public boolean b(String[] var1, int var2) {
      return var2 == 0;
   }
}
