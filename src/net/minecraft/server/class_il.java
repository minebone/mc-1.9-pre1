package net.minecraft.server;

import java.io.IOException;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumMainHandOption;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayInListener;

public class class_il implements Packet {
   private String a;
   private int b;
   private EntityHuman.EnumChatVisibility c;
   private boolean d;
   private int e;
   private EnumMainHandOption f;

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.c(7);
      this.b = var1.readByte();
      this.c = (EntityHuman.EnumChatVisibility)var1.a(EntityHuman.EnumChatVisibility.class);
      this.d = var1.readBoolean();
      this.e = var1.readUnsignedByte();
      this.f = (EnumMainHandOption)var1.a(EnumMainHandOption.class);
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.a(this.a);
      var1.writeByte(this.b);
      var1.a((Enum)this.c);
      var1.writeBoolean(this.d);
      var1.writeByte(this.e);
      var1.a((Enum)this.f);
   }

   public void a(PacketPlayInListener var1) {
      var1.a(this);
   }

   public String a() {
      return this.a;
   }

   public EntityHuman.EnumChatVisibility c() {
      return this.c;
   }

   public boolean d() {
      return this.d;
   }

   public int e() {
      return this.e;
   }

   public EnumMainHandOption f() {
      return this.f;
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketPlayInListener)var1);
   }
}
