package net.minecraft.server;

import com.google.common.base.Function;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import net.minecraft.server.Block;
import net.minecraft.server.BlockCobbleWall;
import net.minecraft.server.BlockDirt;
import net.minecraft.server.BlockFlowers;
import net.minecraft.server.BlockMonsterEggs;
import net.minecraft.server.BlockPrismarine;
import net.minecraft.server.BlockRedSandstone;
import net.minecraft.server.BlockSand;
import net.minecraft.server.BlockSandStone;
import net.minecraft.server.BlockSmoothBrick;
import net.minecraft.server.BlockStone;
import net.minecraft.server.BlockTallPlant;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityMinecartAbstract;
import net.minecraft.server.EntityPainting;
import net.minecraft.server.EnumAnimation;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumItemRarity;
import net.minecraft.server.ItemAnvil;
import net.minecraft.server.ItemArmor;
import net.minecraft.server.ItemBanner;
import net.minecraft.server.ItemCoal;
import net.minecraft.server.ItemEnchantedBook;
import net.minecraft.server.ItemFish;
import net.minecraft.server.ItemSkull;
import net.minecraft.server.ItemWrittenBook;
import net.minecraft.server.Items;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.RegistrySimple;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.EntityBoat;
import net.minecraft.server.class_abw;
import net.minecraft.server.class_abx;
import net.minecraft.server.class_aby;
import net.minecraft.server.class_aca;
import net.minecraft.server.class_acb;
import net.minecraft.server.class_acc;
import net.minecraft.server.class_acd;
import net.minecraft.server.class_ace;
import net.minecraft.server.class_acf;
import net.minecraft.server.class_acg;
import net.minecraft.server.class_ach;
import net.minecraft.server.class_aci;
import net.minecraft.server.class_acj;
import net.minecraft.server.class_ack;
import net.minecraft.server.class_acl;
import net.minecraft.server.class_acn;
import net.minecraft.server.class_acr;
import net.minecraft.server.class_act;
import net.minecraft.server.class_acu;
import net.minecraft.server.class_acv;
import net.minecraft.server.class_acw;
import net.minecraft.server.class_acx;
import net.minecraft.server.class_acz;
import net.minecraft.server.class_ada;
import net.minecraft.server.class_adb;
import net.minecraft.server.class_adc;
import net.minecraft.server.class_add;
import net.minecraft.server.class_ade;
import net.minecraft.server.class_adf;
import net.minecraft.server.class_adh;
import net.minecraft.server.class_adi;
import net.minecraft.server.class_adj;
import net.minecraft.server.class_adk;
import net.minecraft.server.class_adl;
import net.minecraft.server.class_adm;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_adq;
import net.minecraft.server.class_ads;
import net.minecraft.server.class_adt;
import net.minecraft.server.class_adu;
import net.minecraft.server.class_adv;
import net.minecraft.server.class_adw;
import net.minecraft.server.class_adx;
import net.minecraft.server.class_ady;
import net.minecraft.server.class_adz;
import net.minecraft.server.class_aea;
import net.minecraft.server.class_aeb;
import net.minecraft.server.class_aec;
import net.minecraft.server.class_aee;
import net.minecraft.server.class_aef;
import net.minecraft.server.class_aeg;
import net.minecraft.server.class_aeh;
import net.minecraft.server.class_aei;
import net.minecraft.server.class_aek;
import net.minecraft.server.class_ael;
import net.minecraft.server.class_aem;
import net.minecraft.server.class_aen;
import net.minecraft.server.class_aeo;
import net.minecraft.server.class_aeq;
import net.minecraft.server.class_aer;
import net.minecraft.server.class_aes;
import net.minecraft.server.class_aet;
import net.minecraft.server.class_aeu;
import net.minecraft.server.class_aev;
import net.minecraft.server.class_aew;
import net.minecraft.server.class_aex;
import net.minecraft.server.class_aey;
import net.minecraft.server.class_afa;
import net.minecraft.server.class_afb;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_cx;
import net.minecraft.server.class_db;
import net.minecraft.server.class_di;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_xr;

public class Item {
   public static final class_cx f = new class_cx();
   private static final Map a = Maps.newHashMap();
   private static final class_adq b = new class_adq() {
   };
   private static final class_adq c = new class_adq() {
   };
   private static final class_adq d = new class_adq() {
   };
   private static final class_adq e = new class_adq() {
   };
   private final class_db m = new RegistrySimple();
   protected static final UUID g = UUID.fromString("CB3F55D3-645C-4F38-A497-9C13A33DB5CF");
   protected static final UUID h = UUID.fromString("FA233E1C-4180-4865-B01B-BCCE9785ACA3");
   private CreativeModeTab n;
   protected static Random i = new Random();
   protected int j = 64;
   private int o;
   protected boolean k;
   protected boolean l;
   private Item p;
   private String q;

   public static int a(Item var0) {
      return var0 == null?0:f.a(var0);
   }

   public static Item c(int var0) {
      return (Item)f.a(var0);
   }

   public static Item a(Block var0) {
      return (Item)a.get(var0);
   }

   public static Item d(String var0) {
      Item var1 = (Item)f.c(new class_kk(var0));
      if(var1 == null) {
         try {
            return c(Integer.parseInt(var0));
         } catch (NumberFormatException var3) {
            ;
         }
      }

      return var1;
   }

   public final void a(class_kk var1, class_adq var2) {
      this.m.a(var1, var2);
   }

   public boolean a(NBTTagCompound var1) {
      return false;
   }

   public Item() {
      this.a(new class_kk("lefthanded"), d);
      this.a(new class_kk("cooldown"), e);
   }

   public Item d(int var1) {
      this.j = var1;
      return this;
   }

   public EnumResult a(ItemStack var1, EntityHuman var2, World var3, BlockPosition var4, EnumHand var5, EnumDirection var6, float var7, float var8, float var9) {
      return EnumResult.PASS;
   }

   public float a(ItemStack var1, IBlockData var2) {
      return 1.0F;
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      return new class_qo(EnumResult.PASS, var1);
   }

   public ItemStack a(ItemStack var1, World var2, class_rz var3) {
      return var1;
   }

   public int j() {
      return this.j;
   }

   public int a(int var1) {
      return 0;
   }

   public boolean k() {
      return this.l;
   }

   protected Item a(boolean var1) {
      this.l = var1;
      return this;
   }

   public int l() {
      return this.o;
   }

   protected Item e(int var1) {
      this.o = var1;
      if(var1 > 0) {
         this.a(new class_kk("damaged"), b);
         this.a(new class_kk("damage"), c);
      }

      return this;
   }

   public boolean m() {
      return this.o > 0 && (!this.l || this.j == 1);
   }

   public boolean a(ItemStack var1, class_rz var2, class_rz var3) {
      return false;
   }

   public boolean a(ItemStack var1, World var2, IBlockData var3, BlockPosition var4, class_rz var5) {
      return false;
   }

   public boolean a(IBlockData var1) {
      return false;
   }

   public boolean a(ItemStack var1, EntityHuman var2, class_rz var3, EnumHand var4) {
      return false;
   }

   public Item n() {
      this.k = true;
      return this;
   }

   public Item c(String var1) {
      this.q = var1;
      return this;
   }

   public String j(ItemStack var1) {
      String var2 = this.f_(var1);
      return var2 == null?"":class_di.a(var2);
   }

   public String a() {
      return "item." + this.q;
   }

   public String f_(ItemStack var1) {
      return "item." + this.q;
   }

   public Item b(Item var1) {
      this.p = var1;
      return this;
   }

   public boolean p() {
      return true;
   }

   public Item q() {
      return this.p;
   }

   public boolean r() {
      return this.p != null;
   }

   public void a(ItemStack var1, World var2, Entity var3, int var4, boolean var5) {
   }

   public void b(ItemStack var1, World var2, EntityHuman var3) {
   }

   public boolean f() {
      return false;
   }

   public EnumAnimation f(ItemStack var1) {
      return EnumAnimation.NONE;
   }

   public int e(ItemStack var1) {
      return 0;
   }

   public void a(ItemStack var1, World var2, class_rz var3, int var4) {
   }

   public String a(ItemStack var1) {
      return ("" + class_di.a(this.j(var1) + ".name")).trim();
   }

   public EnumItemRarity g(ItemStack var1) {
      return var1.w()?EnumItemRarity.RARE:EnumItemRarity.COMMON;
   }

   public boolean g_(ItemStack var1) {
      return this.j() == 1 && this.m();
   }

   protected MovingObjectPosition a(World var1, EntityHuman var2, boolean var3) {
      float var4 = var2.pitch;
      float var5 = var2.yaw;
      double var6 = var2.locX;
      double var8 = var2.locY + (double)var2.bm();
      double var10 = var2.locZ;
      Vec3D var12 = new Vec3D(var6, var8, var10);
      float var13 = MathHelper.b(-var5 * 0.017453292F - 3.1415927F);
      float var14 = MathHelper.a(-var5 * 0.017453292F - 3.1415927F);
      float var15 = -MathHelper.b(-var4 * 0.017453292F);
      float var16 = MathHelper.a(-var4 * 0.017453292F);
      float var17 = var14 * var15;
      float var19 = var13 * var15;
      double var20 = 5.0D;
      Vec3D var22 = var12.b((double)var17 * var20, (double)var16 * var20, (double)var19 * var20);
      return var1.a(var12, var22, var3, !var3, false);
   }

   public int c() {
      return 0;
   }

   public Item a(CreativeModeTab var1) {
      this.n = var1;
      return this;
   }

   public boolean s() {
      return false;
   }

   public boolean a(ItemStack var1, ItemStack var2) {
      return false;
   }

   public Multimap a(EnumInventorySlot var1) {
      return HashMultimap.create();
   }

   public static void t() {
      a((Block)Blocks.b, (Item)(new class_ady(Blocks.b, Blocks.b, new Function() {
         public String a(ItemStack var1) {
            return BlockStone.EnumStoneVariant.a(var1.i()).d();
         }

         // $FF: synthetic method
         public Object apply(Object var1) {
            return this.a((ItemStack)var1);
         }
      })).b("stone"));
      a((Block)Blocks.c, (Item)(new class_aex(Blocks.c, false)));
      a((Block)Blocks.d, (Item)(new class_ady(Blocks.d, Blocks.d, new Function() {
         public String a(ItemStack var1) {
            return BlockDirt.EnumDirtVariant.a(var1.i()).c();
         }

         // $FF: synthetic method
         public Object apply(Object var1) {
            return this.a((ItemStack)var1);
         }
      })).b("dirt"));
      b(Blocks.e);
      a((Block)Blocks.f, (Item)(new class_ady(Blocks.f, Blocks.f, new Function() {
         public String a(ItemStack var1) {
            return BlockWood.EnumLogVariant.a(var1.i()).d();
         }

         // $FF: synthetic method
         public Object apply(Object var1) {
            return this.a((ItemStack)var1);
         }
      })).b("wood"));
      a((Block)Blocks.g, (Item)(new class_ady(Blocks.g, Blocks.g, new Function() {
         public String a(ItemStack var1) {
            return BlockWood.EnumLogVariant.a(var1.i()).d();
         }

         // $FF: synthetic method
         public Object apply(Object var1) {
            return this.a((ItemStack)var1);
         }
      })).b("sapling"));
      b(Blocks.h);
      a((Block)Blocks.m, (Item)(new class_ady(Blocks.m, Blocks.m, new Function() {
         public String a(ItemStack var1) {
            return BlockSand.EnumSandVariant.a(var1.i()).d();
         }

         // $FF: synthetic method
         public Object apply(Object var1) {
            return this.a((ItemStack)var1);
         }
      })).b("sand"));
      b(Blocks.n);
      b(Blocks.o);
      b(Blocks.p);
      b(Blocks.q);
      a((Block)Blocks.r, (Item)(new class_ady(Blocks.r, Blocks.r, new Function() {
         public String a(ItemStack var1) {
            return BlockWood.EnumLogVariant.a(var1.i()).d();
         }

         // $FF: synthetic method
         public Object apply(Object var1) {
            return this.a((ItemStack)var1);
         }
      })).b("log"));
      a((Block)Blocks.s, (Item)(new class_ady(Blocks.s, Blocks.s, new Function() {
         public String a(ItemStack var1) {
            return BlockWood.EnumLogVariant.a(var1.i() + 4).d();
         }

         // $FF: synthetic method
         public Object apply(Object var1) {
            return this.a((ItemStack)var1);
         }
      })).b("log"));
      a((Block)Blocks.t, (Item)(new class_adt(Blocks.t)).b("leaves"));
      a((Block)Blocks.u, (Item)(new class_adt(Blocks.u)).b("leaves"));
      a((Block)Blocks.v, (Item)(new class_ady(Blocks.v, Blocks.v, new Function() {
         public String a(ItemStack var1) {
            return (var1.i() & 1) == 1?"wet":"dry";
         }

         // $FF: synthetic method
         public Object apply(Object var1) {
            return this.a((ItemStack)var1);
         }
      })).b("sponge"));
      b(Blocks.w);
      b(Blocks.x);
      b(Blocks.y);
      b(Blocks.z);
      a((Block)Blocks.A, (Item)(new class_ady(Blocks.A, Blocks.A, new Function() {
         public String a(ItemStack var1) {
            return BlockSandStone.EnumSandstoneVariant.a(var1.i()).c();
         }

         // $FF: synthetic method
         public Object apply(Object var1) {
            return this.a((ItemStack)var1);
         }
      })).b("sandStone"));
      b(Blocks.B);
      b(Blocks.D);
      b(Blocks.E);
      a((Block)Blocks.F, (Item)(new class_aeb(Blocks.F)));
      b(Blocks.G);
      a((Block)Blocks.H, (Item)(new class_aex(Blocks.H, true)).a(new String[]{"shrub", "grass", "fern"}));
      b((Block)Blocks.I);
      a((Block)Blocks.J, (Item)(new class_aeb(Blocks.J)));
      a((Block)Blocks.L, (Item)(new class_acu(Blocks.L)).b("cloth"));
      a((Block)Blocks.N, (Item)(new class_ady(Blocks.N, Blocks.N, new Function() {
         public String a(ItemStack var1) {
            return BlockFlowers.EnumFlowerVarient.a(BlockFlowers.class_b_in_class_all.YELLOW, var1.i()).d();
         }

         // $FF: synthetic method
         public Object apply(Object var1) {
            return this.a((ItemStack)var1);
         }
      })).b("flower"));
      a((Block)Blocks.O, (Item)(new class_ady(Blocks.O, Blocks.O, new Function() {
         public String a(ItemStack var1) {
            return BlockFlowers.EnumFlowerVarient.a(BlockFlowers.class_b_in_class_all.RED, var1.i()).d();
         }

         // $FF: synthetic method
         public Object apply(Object var1) {
            return this.a((ItemStack)var1);
         }
      })).b("rose"));
      b((Block)Blocks.P);
      b((Block)Blocks.Q);
      b(Blocks.R);
      b(Blocks.S);
      a((Block)Blocks.U, (Item)(new class_aeq(Blocks.U, Blocks.U, Blocks.T)).b("stoneSlab"));
      b(Blocks.V);
      b(Blocks.W);
      b(Blocks.X);
      b(Blocks.Y);
      b(Blocks.Z);
      b(Blocks.aa);
      b(Blocks.cQ);
      b(Blocks.cR);
      b(Blocks.cS);
      b(Blocks.cT);
      b(Blocks.cU);
      b(Blocks.cV);
      a((Block)Blocks.cX, (Item)(new class_aeq(Blocks.cX, Blocks.cX, Blocks.cW)).b("purpurSlab"));
      b(Blocks.ac);
      b(Blocks.ad);
      b((Block)Blocks.ae);
      b(Blocks.ag);
      b(Blocks.ah);
      b(Blocks.ai);
      b(Blocks.ak);
      b(Blocks.al);
      b(Blocks.au);
      b(Blocks.av);
      b(Blocks.aw);
      b(Blocks.ay);
      b(Blocks.az);
      b(Blocks.aB);
      b(Blocks.aC);
      b(Blocks.aF);
      b(Blocks.aG);
      a((Block)Blocks.aH, (Item)(new class_aer(Blocks.aH)));
      b(Blocks.aI);
      b(Blocks.aJ);
      b((Block)Blocks.aK);
      b(Blocks.aL);
      b(Blocks.aN);
      b(Blocks.aO);
      b(Blocks.aP);
      b(Blocks.aQ);
      b(Blocks.aR);
      b(Blocks.aS);
      b(Blocks.aT);
      b(Blocks.aU);
      b(Blocks.aV);
      b(Blocks.aW);
      b(Blocks.aX);
      b(Blocks.aZ);
      b(Blocks.bd);
      a((Block)Blocks.be, (Item)(new class_ady(Blocks.be, Blocks.be, new Function() {
         public String a(ItemStack var1) {
            return BlockMonsterEggs.EnumMonsterEggVarient.a(var1.i()).c();
         }

         // $FF: synthetic method
         public Object apply(Object var1) {
            return this.a((ItemStack)var1);
         }
      })).b("monsterStoneEgg"));
      a((Block)Blocks.bf, (Item)(new class_ady(Blocks.bf, Blocks.bf, new Function() {
         public String a(ItemStack var1) {
            return BlockSmoothBrick.EnumStonebrickType.a(var1.i()).c();
         }

         // $FF: synthetic method
         public Object apply(Object var1) {
            return this.a((ItemStack)var1);
         }
      })).b("stonebricksmooth"));
      b(Blocks.bg);
      b(Blocks.bh);
      b(Blocks.bi);
      b(Blocks.bj);
      b(Blocks.bk);
      a((Block)Blocks.bn, (Item)(new class_aex(Blocks.bn, false)));
      b(Blocks.bo);
      b(Blocks.bp);
      b(Blocks.bq);
      b(Blocks.br);
      b(Blocks.bs);
      b(Blocks.bt);
      b(Blocks.bu);
      b(Blocks.bv);
      b((Block)Blocks.bw);
      a((Block)Blocks.bx, (Item)(new class_afa(Blocks.bx)));
      b(Blocks.by);
      b(Blocks.bz);
      b(Blocks.bA);
      b(Blocks.bC);
      b(Blocks.bG);
      b(Blocks.bH);
      b(Blocks.cY);
      b(Blocks.bI);
      b(Blocks.bJ);
      a((Block)Blocks.bM, (Item)(new class_aeq(Blocks.bM, Blocks.bM, Blocks.bL)).b("woodSlab"));
      b(Blocks.bO);
      b(Blocks.bP);
      b(Blocks.bQ);
      b((Block)Blocks.bR);
      b(Blocks.bT);
      b(Blocks.bU);
      b(Blocks.bV);
      b(Blocks.bW);
      b(Blocks.bX);
      b((Block)Blocks.bY);
      a((Block)Blocks.bZ, (Item)(new class_ady(Blocks.bZ, Blocks.bZ, new Function() {
         public String a(ItemStack var1) {
            return BlockCobbleWall.EnumCobbleVariant.a(var1.i()).c();
         }

         // $FF: synthetic method
         public Object apply(Object var1) {
            return this.a((ItemStack)var1);
         }
      })).b("cobbleWall"));
      b(Blocks.cd);
      a((Block)Blocks.cf, (Item)(new ItemAnvil(Blocks.cf)).b("anvil"));
      b(Blocks.cg);
      b(Blocks.ch);
      b(Blocks.ci);
      b((Block)Blocks.cl);
      b(Blocks.cn);
      b(Blocks.co);
      b((Block)Blocks.cp);
      a((Block)Blocks.cq, (Item)(new class_ady(Blocks.cq, Blocks.cq, new String[]{"default", "chiseled", "lines"})).b("quartzBlock"));
      b(Blocks.cr);
      b(Blocks.cs);
      b(Blocks.ct);
      a((Block)Blocks.cu, (Item)(new class_acu(Blocks.cu)).b("clayHardenedStained"));
      b(Blocks.cv);
      b(Blocks.cw);
      b(Blocks.cx);
      a((Block)Blocks.cy, (Item)(new class_acu(Blocks.cy)).b("woolCarpet"));
      b(Blocks.cz);
      b(Blocks.cA);
      b(Blocks.cB);
      b(Blocks.cC);
      b(Blocks.cD);
      b(Blocks.cE);
      b(Blocks.da);
      a((Block)Blocks.cF, (Item)(new class_ady(Blocks.cF, Blocks.cF, new Function() {
         public String a(ItemStack var1) {
            return BlockTallPlant.EnumTallFlowerVariants.a(var1.i()).c();
         }

         // $FF: synthetic method
         public Object apply(Object var1) {
            return this.a((ItemStack)var1);
         }
      })).b("doublePlant"));
      a((Block)Blocks.cG, (Item)(new class_acu(Blocks.cG)).b("stainedGlass"));
      a((Block)Blocks.cH, (Item)(new class_acu(Blocks.cH)).b("stainedGlassPane"));
      a((Block)Blocks.cI, (Item)(new class_ady(Blocks.cI, Blocks.cI, new Function() {
         public String a(ItemStack var1) {
            return BlockPrismarine.EnumPrismarineVariant.a(var1.i()).c();
         }

         // $FF: synthetic method
         public Object apply(Object var1) {
            return this.a((ItemStack)var1);
         }
      })).b("prismarine"));
      b(Blocks.cJ);
      a((Block)Blocks.cM, (Item)(new class_ady(Blocks.cM, Blocks.cM, new Function() {
         public String a(ItemStack var1) {
            return BlockRedSandstone.EnumRedSandstoneVariant.a(var1.i()).c();
         }

         // $FF: synthetic method
         public Object apply(Object var1) {
            return this.a((ItemStack)var1);
         }
      })).b("redSandStone"));
      b(Blocks.cN);
      a((Block)Blocks.cP, (Item)(new class_aeq(Blocks.cP, Blocks.cP, Blocks.cO)).b("stoneSlab2"));
      b(Blocks.dc);
      b(Blocks.dd);
      a(256, (String)"iron_shovel", (new class_aem(Item.class_a_in_class_adn.IRON)).c("shovelIron"));
      a(257, (String)"iron_pickaxe", (new class_aea(Item.class_a_in_class_adn.IRON)).c("pickaxeIron"));
      a(258, (String)"iron_axe", (new class_aby(Item.class_a_in_class_adn.IRON)).c("hatchetIron"));
      a(259, (String)"flint_and_steel", (new class_adi()).c("flintAndSteel"));
      a(260, (String)"apple", (new class_adj(4, 0.3F, false)).c("apple"));
      a(261, (String)"bow", (new class_acg()).c("bow"));
      a(262, (String)"arrow", (new class_abx()).c("arrow"));
      a(263, (String)"coal", (new ItemCoal()).c("coal"));
      a(264, (String)"diamond", (new Item()).c("diamond").a(CreativeModeTab.l));
      a(265, (String)"iron_ingot", (new Item()).c("ingotIron").a(CreativeModeTab.l));
      a(266, (String)"gold_ingot", (new Item()).c("ingotGold").a(CreativeModeTab.l));
      a(267, (String)"iron_sword", (new class_aew(Item.class_a_in_class_adn.IRON)).c("swordIron"));
      a(268, (String)"wooden_sword", (new class_aew(Item.class_a_in_class_adn.WOOD)).c("swordWood"));
      a(269, (String)"wooden_shovel", (new class_aem(Item.class_a_in_class_adn.WOOD)).c("shovelWood"));
      a(270, (String)"wooden_pickaxe", (new class_aea(Item.class_a_in_class_adn.WOOD)).c("pickaxeWood"));
      a(271, (String)"wooden_axe", (new class_aby(Item.class_a_in_class_adn.WOOD)).c("hatchetWood"));
      a(272, (String)"stone_sword", (new class_aew(Item.class_a_in_class_adn.STONE)).c("swordStone"));
      a(273, (String)"stone_shovel", (new class_aem(Item.class_a_in_class_adn.STONE)).c("shovelStone"));
      a(274, (String)"stone_pickaxe", (new class_aea(Item.class_a_in_class_adn.STONE)).c("pickaxeStone"));
      a(275, (String)"stone_axe", (new class_aby(Item.class_a_in_class_adn.STONE)).c("hatchetStone"));
      a(276, (String)"diamond_sword", (new class_aew(Item.class_a_in_class_adn.DIAMOND)).c("swordDiamond"));
      a(277, (String)"diamond_shovel", (new class_aem(Item.class_a_in_class_adn.DIAMOND)).c("shovelDiamond"));
      a(278, (String)"diamond_pickaxe", (new class_aea(Item.class_a_in_class_adn.DIAMOND)).c("pickaxeDiamond"));
      a(279, (String)"diamond_axe", (new class_aby(Item.class_a_in_class_adn.DIAMOND)).c("hatchetDiamond"));
      a(280, (String)"stick", (new Item()).n().c("stick").a(CreativeModeTab.l));
      a(281, (String)"bowl", (new Item()).c("bowl").a(CreativeModeTab.l));
      a(282, (String)"mushroom_stew", (new class_ach(6)).c("mushroomStew"));
      a(283, (String)"golden_sword", (new class_aew(Item.class_a_in_class_adn.GOLD)).c("swordGold"));
      a(284, (String)"golden_shovel", (new class_aem(Item.class_a_in_class_adn.GOLD)).c("shovelGold"));
      a(285, (String)"golden_pickaxe", (new class_aea(Item.class_a_in_class_adn.GOLD)).c("pickaxeGold"));
      a(286, (String)"golden_axe", (new class_aby(Item.class_a_in_class_adn.GOLD)).c("hatchetGold"));
      a(287, (String)"string", (new class_acc(Blocks.bS)).c("string").a(CreativeModeTab.l));
      a(288, (String)"feather", (new Item()).c("feather").a(CreativeModeTab.l));
      a(289, (String)"gunpowder", (new Item()).c("sulphur").a(CreativeModeTab.l));
      a(290, (String)"wooden_hoe", (new class_adm(Item.class_a_in_class_adn.WOOD)).c("hoeWood"));
      a(291, (String)"stone_hoe", (new class_adm(Item.class_a_in_class_adn.STONE)).c("hoeStone"));
      a(292, (String)"iron_hoe", (new class_adm(Item.class_a_in_class_adn.IRON)).c("hoeIron"));
      a(293, (String)"diamond_hoe", (new class_adm(Item.class_a_in_class_adn.DIAMOND)).c("hoeDiamond"));
      a(294, (String)"golden_hoe", (new class_adm(Item.class_a_in_class_adn.GOLD)).c("hoeGold"));
      a(295, (String)"wheat_seeds", (new class_aei(Blocks.aj, Blocks.ak)).c("seeds"));
      a(296, (String)"wheat", (new Item()).c("wheat").a(CreativeModeTab.l));
      a(297, (String)"bread", (new class_adj(5, 0.6F, false)).c("bread"));
      a(298, (String)"leather_helmet", (new ItemArmor(ItemArmor.EnumArmorMaterial.LEATHER, 0, EnumInventorySlot.HEAD)).c("helmetCloth"));
      a(299, (String)"leather_chestplate", (new ItemArmor(ItemArmor.EnumArmorMaterial.LEATHER, 0, EnumInventorySlot.CHEST)).c("chestplateCloth"));
      a(300, (String)"leather_leggings", (new ItemArmor(ItemArmor.EnumArmorMaterial.LEATHER, 0, EnumInventorySlot.LEGS)).c("leggingsCloth"));
      a(301, (String)"leather_boots", (new ItemArmor(ItemArmor.EnumArmorMaterial.LEATHER, 0, EnumInventorySlot.FEET)).c("bootsCloth"));
      a(302, (String)"chainmail_helmet", (new ItemArmor(ItemArmor.EnumArmorMaterial.CHAIN, 1, EnumInventorySlot.HEAD)).c("helmetChain"));
      a(303, (String)"chainmail_chestplate", (new ItemArmor(ItemArmor.EnumArmorMaterial.CHAIN, 1, EnumInventorySlot.CHEST)).c("chestplateChain"));
      a(304, (String)"chainmail_leggings", (new ItemArmor(ItemArmor.EnumArmorMaterial.CHAIN, 1, EnumInventorySlot.LEGS)).c("leggingsChain"));
      a(305, (String)"chainmail_boots", (new ItemArmor(ItemArmor.EnumArmorMaterial.CHAIN, 1, EnumInventorySlot.FEET)).c("bootsChain"));
      a(306, (String)"iron_helmet", (new ItemArmor(ItemArmor.EnumArmorMaterial.IRON, 2, EnumInventorySlot.HEAD)).c("helmetIron"));
      a(307, (String)"iron_chestplate", (new ItemArmor(ItemArmor.EnumArmorMaterial.IRON, 2, EnumInventorySlot.CHEST)).c("chestplateIron"));
      a(308, (String)"iron_leggings", (new ItemArmor(ItemArmor.EnumArmorMaterial.IRON, 2, EnumInventorySlot.LEGS)).c("leggingsIron"));
      a(309, (String)"iron_boots", (new ItemArmor(ItemArmor.EnumArmorMaterial.IRON, 2, EnumInventorySlot.FEET)).c("bootsIron"));
      a(310, (String)"diamond_helmet", (new ItemArmor(ItemArmor.EnumArmorMaterial.DIAMOND, 3, EnumInventorySlot.HEAD)).c("helmetDiamond"));
      a(311, (String)"diamond_chestplate", (new ItemArmor(ItemArmor.EnumArmorMaterial.DIAMOND, 3, EnumInventorySlot.CHEST)).c("chestplateDiamond"));
      a(312, (String)"diamond_leggings", (new ItemArmor(ItemArmor.EnumArmorMaterial.DIAMOND, 3, EnumInventorySlot.LEGS)).c("leggingsDiamond"));
      a(313, (String)"diamond_boots", (new ItemArmor(ItemArmor.EnumArmorMaterial.DIAMOND, 3, EnumInventorySlot.FEET)).c("bootsDiamond"));
      a(314, (String)"golden_helmet", (new ItemArmor(ItemArmor.EnumArmorMaterial.GOLD, 4, EnumInventorySlot.HEAD)).c("helmetGold"));
      a(315, (String)"golden_chestplate", (new ItemArmor(ItemArmor.EnumArmorMaterial.GOLD, 4, EnumInventorySlot.CHEST)).c("chestplateGold"));
      a(316, (String)"golden_leggings", (new ItemArmor(ItemArmor.EnumArmorMaterial.GOLD, 4, EnumInventorySlot.LEGS)).c("leggingsGold"));
      a(317, (String)"golden_boots", (new ItemArmor(ItemArmor.EnumArmorMaterial.GOLD, 4, EnumInventorySlot.FEET)).c("bootsGold"));
      a(318, (String)"flint", (new Item()).c("flint").a(CreativeModeTab.l));
      a(319, (String)"porkchop", (new class_adj(3, 0.3F, true)).c("porkchopRaw"));
      a(320, (String)"cooked_porkchop", (new class_adj(8, 0.8F, true)).c("porkchopCooked"));
      a(321, (String)"painting", (new class_adl(EntityPainting.class)).c("painting"));
      a(322, (String)"golden_apple", (new class_adk(4, 1.2F, false)).h().c("appleGold"));
      a(323, (String)"sign", (new class_aen()).c("sign"));
      a(324, (String)"wooden_door", (new class_acr(Blocks.ao)).c("doorOak"));
      Item var0 = (new class_aci(Blocks.AIR)).c("bucket").d(16);
      a(325, (String)"bucket", var0);
      a(326, (String)"water_bucket", (new class_aci(Blocks.i)).c("bucketWater").b(var0));
      a(327, (String)"lava_bucket", (new class_aci(Blocks.k)).c("bucketLava").b(var0));
      a(328, (String)"minecart", (new class_adx(EntityMinecartAbstract.EnumMinecartType.RIDEABLE)).c("minecart"));
      a(329, (String)"saddle", (new class_aeg()).c("saddle"));
      a(330, (String)"iron_door", (new class_acr(Blocks.aA)).c("doorIron"));
      a(331, (String)"redstone", (new class_aef()).c("redstone"));
      a(332, (String)"snowball", (new class_aes()).c("snowball"));
      a(333, (String)"boat", new class_acd(EntityBoat.EnumBoatVariant.OAK));
      a(334, (String)"leather", (new Item()).c("leather").a(CreativeModeTab.l));
      a(335, (String)"milk_bucket", (new class_adw()).c("milk").b(var0));
      a(336, (String)"brick", (new Item()).c("brick").a(CreativeModeTab.l));
      a(337, (String)"clay_ball", (new Item()).c("clay").a(CreativeModeTab.l));
      a(338, (String)"reeds", (new class_acc(Blocks.aM)).c("reeds").a(CreativeModeTab.l));
      a(339, (String)"paper", (new Item()).c("paper").a(CreativeModeTab.f));
      a(340, (String)"book", (new class_ace()).c("book").a(CreativeModeTab.f));
      a(341, (String)"slime_ball", (new Item()).c("slimeball").a(CreativeModeTab.f));
      a(342, (String)"chest_minecart", (new class_adx(EntityMinecartAbstract.EnumMinecartType.CHEST)).c("minecartChest"));
      a(343, (String)"furnace_minecart", (new class_adx(EntityMinecartAbstract.EnumMinecartType.FURNACE)).c("minecartFurnace"));
      a(344, (String)"egg", (new class_acv()).c("egg"));
      a(345, (String)"compass", (new class_acn()).c("compass").a(CreativeModeTab.i));
      a(346, (String)"fishing_rod", (new class_adh()).c("fishingRod"));
      a(347, (String)"clock", (new class_acl()).c("clock").a(CreativeModeTab.i));
      a(348, (String)"glowstone_dust", (new Item()).c("yellowDust").a(CreativeModeTab.l));
      a(349, (String)"fish", (new ItemFish(false)).c("fish").a(true));
      a(350, (String)"cooked_fish", (new ItemFish(true)).c("fish").a(true));
      a(351, (String)"dye", (new class_act()).c("dyePowder"));
      a(352, (String)"bone", (new Item()).c("bone").n().a(CreativeModeTab.f));
      a(353, (String)"sugar", (new Item()).c("sugar").a(CreativeModeTab.l));
      a(354, (String)"cake", (new class_acc(Blocks.ba)).d(1).c("cake").a(CreativeModeTab.h));
      a(355, (String)"bed", (new class_aca()).d(1).c("bed"));
      a(356, (String)"repeater", (new class_acc(Blocks.bb)).c("diode").a(CreativeModeTab.d));
      a(357, (String)"cookie", (new class_adj(2, 0.1F, false)).c("cookie"));
      a(358, (String)"filled_map", (new class_adv()).c("map"));
      a(359, (String)"shears", (new class_aek()).c("shears"));
      a(360, (String)"melon", (new class_adj(2, 0.3F, false)).c("melon"));
      a(361, (String)"pumpkin_seeds", (new class_aei(Blocks.bl, Blocks.ak)).c("seeds_pumpkin"));
      a(362, (String)"melon_seeds", (new class_aei(Blocks.bm, Blocks.ak)).c("seeds_melon"));
      a(363, (String)"beef", (new class_adj(3, 0.3F, true)).c("beefRaw"));
      a(364, (String)"cooked_beef", (new class_adj(8, 0.8F, true)).c("beefCooked"));
      a(365, (String)"chicken", (new class_adj(2, 0.3F, true)).a(new MobEffect(MobEffectList.q, 600, 0), 0.3F).c("chickenRaw"));
      a(366, (String)"cooked_chicken", (new class_adj(6, 0.6F, true)).c("chickenCooked"));
      a(367, (String)"rotten_flesh", (new class_adj(4, 0.1F, true)).a(new MobEffect(MobEffectList.q, 600, 0), 0.8F).c("rottenFlesh"));
      a(368, (String)"ender_pearl", (new class_adb()).c("enderPearl"));
      a(369, (String)"blaze_rod", (new Item()).c("blazeRod").a(CreativeModeTab.l).n());
      a(370, (String)"ghast_tear", (new Item()).c("ghastTear").a(CreativeModeTab.k));
      a(371, (String)"gold_nugget", (new Item()).c("goldNugget").a(CreativeModeTab.l));
      a(372, (String)"nether_wart", (new class_aei(Blocks.bB, Blocks.aW)).c("netherStalkSeeds"));
      a(373, (String)"potion", (new class_aec()).c("potion"));
      Item var1 = (new class_acf()).c("glassBottle");
      a(374, (String)"glass_bottle", var1);
      a(375, (String)"spider_eye", (new class_adj(2, 0.8F, false)).a(new MobEffect(MobEffectList.s, 100, 0), 1.0F).c("spiderEye"));
      a(376, (String)"fermented_spider_eye", (new Item()).c("fermentedSpiderEye").a(CreativeModeTab.k));
      a(377, (String)"blaze_powder", (new Item()).c("blazePowder").a(CreativeModeTab.k));
      a(378, (String)"magma_cream", (new Item()).c("magmaCream").a(CreativeModeTab.k));
      a(379, (String)"brewing_stand", (new class_acc(Blocks.bD)).c("brewingStand").a(CreativeModeTab.k));
      a(380, (String)"cauldron", (new class_acc(Blocks.bE)).c("cauldron").a(CreativeModeTab.k));
      a(381, (String)"ender_eye", (new class_ada()).c("eyeOfEnder"));
      a(382, (String)"speckled_melon", (new Item()).c("speckledMelon").a(CreativeModeTab.k));
      a(383, (String)"spawn_egg", (new class_aet()).c("monsterPlacer"));
      a(384, (String)"experience_bottle", (new class_adc()).c("expBottle"));
      a(385, (String)"fire_charge", (new class_add()).c("fireball"));
      a(386, (String)"writable_book", (new class_afb()).c("writingBook").a(CreativeModeTab.f));
      a(387, (String)"written_book", (new ItemWrittenBook()).c("writtenBook").d(16));
      a(388, (String)"emerald", (new Item()).c("emerald").a(CreativeModeTab.l));
      a(389, (String)"item_frame", (new class_adl(class_xr.class)).c("frame"));
      a(390, (String)"flower_pot", (new class_acc(Blocks.ca)).c("flowerPot").a(CreativeModeTab.c));
      a(391, (String)"carrot", (new class_aeh(3, 0.6F, Blocks.cb, Blocks.ak)).c("carrots"));
      a(392, (String)"potato", (new class_aeh(1, 0.3F, Blocks.cc, Blocks.ak)).c("potato"));
      a(393, (String)"baked_potato", (new class_adj(5, 0.6F, false)).c("potatoBaked"));
      a(394, (String)"poisonous_potato", (new class_adj(2, 0.3F, false)).a(new MobEffect(MobEffectList.s, 100, 0), 0.6F).c("potatoPoisonous"));
      a(395, (String)"map", (new class_acx()).c("emptyMap"));
      a(396, (String)"golden_carrot", (new class_adj(6, 1.2F, false)).c("carrotGolden").a(CreativeModeTab.k));
      a(397, (String)"skull", (new ItemSkull()).c("skull"));
      a(398, (String)"carrot_on_a_stick", (new class_acj()).c("carrotOnAStick"));
      a(399, (String)"nether_star", (new class_aeo()).c("netherStar").a(CreativeModeTab.l));
      a(400, (String)"pumpkin_pie", (new class_adj(8, 0.3F, false)).c("pumpkinPie").a(CreativeModeTab.h));
      a(401, (String)"fireworks", (new class_adf()).c("fireworks"));
      a(402, (String)"firework_charge", (new class_ade()).c("fireworksCharge").a(CreativeModeTab.f));
      a(403, (String)"enchanted_book", (new ItemEnchantedBook()).d(1).c("enchantedBook"));
      a(404, (String)"comparator", (new class_acc(Blocks.cj)).c("comparator").a(CreativeModeTab.d));
      a(405, (String)"netherbrick", (new Item()).c("netherbrick").a(CreativeModeTab.l));
      a(406, (String)"quartz", (new Item()).c("netherquartz").a(CreativeModeTab.l));
      a(407, (String)"tnt_minecart", (new class_adx(EntityMinecartAbstract.EnumMinecartType.TNT)).c("minecartTnt"));
      a(408, (String)"hopper_minecart", (new class_adx(EntityMinecartAbstract.EnumMinecartType.HOPPER)).c("minecartHopper"));
      a(409, (String)"prismarine_shard", (new Item()).c("prismarineShard").a(CreativeModeTab.l));
      a(410, (String)"prismarine_crystals", (new Item()).c("prismarineCrystals").a(CreativeModeTab.l));
      a(411, (String)"rabbit", (new class_adj(3, 0.3F, true)).c("rabbitRaw"));
      a(412, (String)"cooked_rabbit", (new class_adj(5, 0.6F, true)).c("rabbitCooked"));
      a(413, (String)"rabbit_stew", (new class_ach(10)).c("rabbitStew"));
      a(414, (String)"rabbit_foot", (new Item()).c("rabbitFoot").a(CreativeModeTab.k));
      a(415, (String)"rabbit_hide", (new Item()).c("rabbitHide").a(CreativeModeTab.l));
      a(416, (String)"armor_stand", (new class_abw()).c("armorStand").d(16));
      a(417, (String)"iron_horse_armor", (new Item()).c("horsearmormetal").d(1).a(CreativeModeTab.f));
      a(418, (String)"golden_horse_armor", (new Item()).c("horsearmorgold").d(1).a(CreativeModeTab.f));
      a(419, (String)"diamond_horse_armor", (new Item()).c("horsearmordiamond").d(1).a(CreativeModeTab.f));
      a(420, (String)"lead", (new class_ads()).c("leash"));
      a(421, (String)"name_tag", (new class_adz()).c("nameTag"));
      a(422, (String)"command_block_minecart", (new class_adx(EntityMinecartAbstract.EnumMinecartType.COMMAND_BLOCK)).c("minecartCommandBlock").a((CreativeModeTab)null));
      a(423, (String)"mutton", (new class_adj(2, 0.3F, true)).c("muttonRaw"));
      a(424, (String)"cooked_mutton", (new class_adj(6, 0.8F, true)).c("muttonCooked"));
      a(425, (String)"banner", (new ItemBanner()).b("banner"));
      a(426, (String)"end_crystal", new class_acz());
      a(427, (String)"spruce_door", (new class_acr(Blocks.ap)).c("doorSpruce"));
      a(428, (String)"birch_door", (new class_acr(Blocks.aq)).c("doorBirch"));
      a(429, (String)"jungle_door", (new class_acr(Blocks.ar)).c("doorJungle"));
      a(430, (String)"acacia_door", (new class_acr(Blocks.as)).c("doorAcacia"));
      a(431, (String)"dark_oak_door", (new class_acr(Blocks.at)).c("doorDarkOak"));
      a(432, (String)"chorus_fruit", (new class_ack(4, 0.3F)).h().c("chorusFruit").a(CreativeModeTab.l));
      a(433, (String)"chorus_fruit_popped", (new Item()).c("chorusFruitPopped").a(CreativeModeTab.l));
      a(434, (String)"beetroot", (new class_adj(1, 0.6F, false)).c("beetroot"));
      a(435, (String)"beetroot_seeds", (new class_aei(Blocks.cZ, Blocks.ak)).c("beetroot_seeds"));
      a(436, (String)"beetroot_soup", (new class_ach(6)).c("beetroot_soup"));
      a(437, (String)"dragon_breath", (new Item()).a(CreativeModeTab.k).c("dragon_breath").b(var1));
      a(438, (String)"splash_potion", (new class_aev()).c("splash_potion"));
      a(439, (String)"spectral_arrow", (new class_aeu()).c("spectral_arrow"));
      a(440, (String)"tipped_arrow", (new class_aey()).c("tipped_arrow"));
      a(441, (String)"lingering_potion", (new class_adu()).c("lingering_potion"));
      a(442, (String)"shield", (new class_ael()).c("shield"));
      a(443, (String)"elytra", (new class_acw()).c("elytra"));
      a(444, (String)"spruce_boat", new class_acd(EntityBoat.EnumBoatVariant.SPRUCE));
      a(445, (String)"birch_boat", new class_acd(EntityBoat.EnumBoatVariant.BIRCH));
      a(446, (String)"jungle_boat", new class_acd(EntityBoat.EnumBoatVariant.JUNGLE));
      a(447, (String)"acacia_boat", new class_acd(EntityBoat.EnumBoatVariant.ACACIA));
      a(448, (String)"dark_oak_boat", new class_acd(EntityBoat.EnumBoatVariant.DARK_OAK));
      a(2256, (String)"record_13", (new class_aee("13", class_ng.ep)).c("record"));
      a(2257, (String)"record_cat", (new class_aee("cat", class_ng.er)).c("record"));
      a(2258, (String)"record_blocks", (new class_aee("blocks", class_ng.eq)).c("record"));
      a(2259, (String)"record_chirp", (new class_aee("chirp", class_ng.es)).c("record"));
      a(2260, (String)"record_far", (new class_aee("far", class_ng.et)).c("record"));
      a(2261, (String)"record_mall", (new class_aee("mall", class_ng.eu)).c("record"));
      a(2262, (String)"record_mellohi", (new class_aee("mellohi", class_ng.ev)).c("record"));
      a(2263, (String)"record_stal", (new class_aee("stal", class_ng.ew)).c("record"));
      a(2264, (String)"record_strad", (new class_aee("strad", class_ng.ex)).c("record"));
      a(2265, (String)"record_ward", (new class_aee("ward", class_ng.ez)).c("record"));
      a(2266, (String)"record_11", (new class_aee("11", class_ng.eo)).c("record"));
      a(2267, (String)"record_wait", (new class_aee("wait", class_ng.ey)).c("record"));
   }

   private static void b(Block var0) {
      a((Block)var0, (Item)(new class_acb(var0)));
   }

   protected static void a(Block var0, Item var1) {
      a(Block.a(var0), (class_kk)Block.h.b(var0), var1);
      a.put(var0, var1);
   }

   private static void a(int var0, String var1, Item var2) {
      a(var0, new class_kk(var1), var2);
   }

   private static void a(int var0, class_kk var1, Item var2) {
      f.a(var0, var1, var2);
   }

   public static enum class_a_in_class_adn {
      WOOD(0, 59, 2.0F, 0.0F, 15),
      STONE(1, 131, 4.0F, 1.0F, 5),
      IRON(2, 250, 6.0F, 2.0F, 14),
      DIAMOND(3, 1561, 8.0F, 3.0F, 10),
      GOLD(0, 32, 12.0F, 0.0F, 22);

      private final int f;
      private final int g;
      private final float h;
      private final float i;
      private final int j;

      private class_a_in_class_adn(int var3, int var4, float var5, float var6, int var7) {
         this.f = var3;
         this.g = var4;
         this.h = var5;
         this.i = var6;
         this.j = var7;
      }

      public int a() {
         return this.g;
      }

      public float b() {
         return this.h;
      }

      public float c() {
         return this.i;
      }

      public int d() {
         return this.f;
      }

      public int e() {
         return this.j;
      }

      public Item f() {
         return this == WOOD?Item.a(Blocks.f):(this == STONE?Item.a(Blocks.e):(this == GOLD?Items.m:(this == IRON?Items.l:(this == DIAMOND?Items.k:null))));
      }
   }
}
