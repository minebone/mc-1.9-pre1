package net.minecraft.server;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_qs;

public interface IInventory extends class_qs {
   int u_();

   ItemStack a(int var1);

   ItemStack a(int var1, int var2);

   ItemStack b(int var1);

   void a(int var1, ItemStack var2);

   int w_();

   void v_();

   boolean a(EntityHuman var1);

   void b(EntityHuman var1);

   void c(EntityHuman var1);

   boolean b(int var1, ItemStack var2);

   int c_(int var1);

   void b(int var1, int var2);

   int g();

   void l();
}
