package net.minecraft.server;

import com.google.common.collect.Maps;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSyntaxException;
import java.lang.reflect.Type;
import java.util.Map;
import net.minecraft.server.ChatDeserializer;
import net.minecraft.server.class_bac;
import net.minecraft.server.class_bad;
import net.minecraft.server.class_bae;
import net.minecraft.server.class_bag;
import net.minecraft.server.class_bah;
import net.minecraft.server.class_bai;
import net.minecraft.server.class_baj;
import net.minecraft.server.class_bak;
import net.minecraft.server.class_bal;
import net.minecraft.server.class_bam;
import net.minecraft.server.class_baq;
import net.minecraft.server.class_kk;

public class class_baf {
   private static final Map a = Maps.newHashMap();
   private static final Map b = Maps.newHashMap();

   public static void a(class_bae.class_a_in_class_bae var0) {
      class_kk var1 = var0.a();
      Class var2 = var0.b();
      if(a.containsKey(var1)) {
         throw new IllegalArgumentException("Can\'t re-register item function name " + var1);
      } else if(b.containsKey(var2)) {
         throw new IllegalArgumentException("Can\'t re-register item function class " + var2.getName());
      } else {
         a.put(var1, var0);
         b.put(var2, var0);
      }
   }

   public static class_bae.class_a_in_class_bae a(class_kk var0) {
      class_bae.class_a_in_class_bae var1 = (class_bae.class_a_in_class_bae)a.get(var0);
      if(var1 == null) {
         throw new IllegalArgumentException("Unknown loot item function \'" + var0 + "\'");
      } else {
         return var1;
      }
   }

   public static class_bae.class_a_in_class_bae a(class_bae var0) {
      class_bae.class_a_in_class_bae var1 = (class_bae.class_a_in_class_bae)b.get(var0.getClass());
      if(var1 == null) {
         throw new IllegalArgumentException("Unknown loot item function " + var0);
      } else {
         return var1;
      }
   }

   static {
      a((class_bae.class_a_in_class_bae)(new class_bai.class_a_in_class_bai()));
      a((class_bae.class_a_in_class_bae)(new class_bak.class_a_in_class_bak()));
      a((class_bae.class_a_in_class_bae)(new class_bad.class_a_in_class_bad()));
      a((class_bae.class_a_in_class_bae)(new class_bac.class_a_in_class_bac()));
      a((class_bae.class_a_in_class_bae)(new class_bal.class_a_in_class_bal()));
      a((class_bae.class_a_in_class_bae)(new class_bam.class_a_in_class_bam()));
      a((class_bae.class_a_in_class_bae)(new class_bag.class_a_in_class_bag()));
      a((class_bae.class_a_in_class_bae)(new class_baj.class_a_in_class_baj()));
      a((class_bae.class_a_in_class_bae)(new class_bah.class_b_in_class_bah()));
   }

   public static class class_a_in_class_baf implements JsonDeserializer, JsonSerializer {
      public class_bae a(JsonElement var1, Type var2, JsonDeserializationContext var3) throws JsonParseException {
         JsonObject var4 = ChatDeserializer.m(var1, "function");
         class_kk var5 = new class_kk(ChatDeserializer.h(var4, "function"));

         class_bae.class_a_in_class_bae var6;
         try {
            var6 = class_baf.a(var5);
         } catch (IllegalArgumentException var8) {
            throw new JsonSyntaxException("Unknown function \'" + var5 + "\'");
         }

         return var6.b(var4, var3, (class_baq[])ChatDeserializer.a(var4, "conditions", new class_baq[0], var3, class_baq[].class));
      }

      public JsonElement a(class_bae var1, Type var2, JsonSerializationContext var3) {
         class_bae.class_a_in_class_bae var4 = class_baf.a(var1);
         JsonObject var5 = new JsonObject();
         var4.a(var5, var1, var3);
         var5.addProperty("function", var4.a().toString());
         if(var1.a() != null && var1.a().length > 0) {
            var5.add("conditions", var3.serialize(var1.a()));
         }

         return var5;
      }

      // $FF: synthetic method
      public JsonElement serialize(Object var1, Type var2, JsonSerializationContext var3) {
         return this.a((class_bae)var1, var2, var3);
      }

      // $FF: synthetic method
      public Object deserialize(JsonElement var1, Type var2, JsonDeserializationContext var3) throws JsonParseException {
         return this.a(var1, var2, var3);
      }
   }
}
