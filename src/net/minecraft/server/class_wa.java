package net.minecraft.server;

import com.google.common.base.Predicate;
import java.util.UUID;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityAnimal;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityTameableAnimal;
import net.minecraft.server.Items;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.PathfinderGoalFollowOwner;
import net.minecraft.server.PathfinderGoalTempt;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.class_qk;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rn;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_sk;
import net.minecraft.server.class_sz;
import net.minecraft.server.class_tc;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_tn;
import net.minecraft.server.class_to;
import net.minecraft.server.class_tx;
import net.minecraft.server.class_ty;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_ul;
import net.minecraft.server.class_uy;
import net.minecraft.server.class_vw;
import net.minecraft.server.class_ys;

public class class_wa extends EntityTameableAnimal {
   private static final class_ke bz = DataWatcher.a(class_wa.class, class_kg.b);
   private class_sz bA;
   private PathfinderGoalTempt bB;

   public class_wa(World var1) {
      super(var1);
      this.a(0.6F, 0.7F);
   }

   protected void r() {
      this.bp.a(1, new class_tg(this));
      this.bp.a(2, this.bx = new class_ul(this));
      this.bp.a(3, this.bB = new PathfinderGoalTempt(this, 0.6D, Items.bb, true));
      this.bp.a(5, new PathfinderGoalFollowOwner(this, 1.0D, 10.0F, 5.0F));
      this.bp.a(6, new class_ty(this, 0.8D));
      this.bp.a(7, new class_tn(this, 0.3F));
      this.bp.a(8, new class_tx(this));
      this.bp.a(9, new class_tc(this, 0.8D));
      this.bp.a(10, new class_uf(this, 0.8D));
      this.bp.a(11, new class_to(this, EntityHuman.class, 10.0F));
      this.bq.a(1, new class_uy(this, class_vw.class, false, (Predicate)null));
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)bz, (Object)Integer.valueOf(0));
   }

   public void M() {
      if(this.u().a()) {
         double var1 = this.u().b();
         if(var1 == 0.6D) {
            this.d(true);
            this.e(false);
         } else if(var1 == 1.33D) {
            this.d(false);
            this.e(true);
         } else {
            this.d(false);
            this.e(false);
         }
      } else {
         this.d(false);
         this.e(false);
      }

   }

   protected boolean K() {
      return !this.cZ() && this.ticksLived > 2400;
   }

   protected void bz() {
      super.bz();
      this.a((class_sk)class_ys.a).a(10.0D);
      this.a((class_sk)class_ys.d).a(0.30000001192092896D);
   }

   public void e(float var1, float var2) {
   }

   public void b(NBTTagCompound var1) {
      super.b(var1);
      var1.a("CatType", this.dh());
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      this.l(var1.h("CatType"));
   }

   protected class_nf G() {
      return this.cZ()?(this.df()?class_ng.R:(this.random.nextInt(4) == 0?class_ng.S:class_ng.O)):null;
   }

   protected class_nf bQ() {
      return class_ng.Q;
   }

   protected class_nf bR() {
      return class_ng.P;
   }

   protected float cc() {
      return 0.4F;
   }

   public boolean B(Entity var1) {
      return var1.a(DamageSource.a((class_rz)this), 3.0F);
   }

   public boolean a(DamageSource var1, float var2) {
      if(this.b((DamageSource)var1)) {
         return false;
      } else {
         if(this.bx != null) {
            this.bx.a(false);
         }

         return super.a(var1, var2);
      }
   }

   protected class_kk J() {
      return class_azs.J;
   }

   public boolean a(EntityHuman var1, EnumHand var2, ItemStack var3) {
      if(this.cZ()) {
         if(this.d(var1) && !this.world.E && !this.e(var3)) {
            this.bx.a(!this.db());
         }
      } else if((this.bB == null || this.bB.f()) && var3 != null && var3.b() == Items.bb && var1.h(this) < 9.0D) {
         if(!var1.abilities.d) {
            --var3.b;
         }

         if(!this.world.E) {
            if(this.random.nextInt(3) == 0) {
               this.p(true);
               this.l(1 + this.world.r.nextInt(3));
               this.b((UUID)var1.getUniqueId());
               this.o(true);
               this.bx.a(true);
               this.world.a((Entity)this, (byte)7);
            } else {
               this.o(false);
               this.world.a((Entity)this, (byte)6);
            }
         }

         return true;
      }

      return super.a(var1, var2, var3);
   }

   public class_wa b(class_rn var1) {
      class_wa var2 = new class_wa(this.world);
      if(this.cZ()) {
         var2.b((UUID)this.b());
         var2.p(true);
         var2.l(this.dh());
      }

      return var2;
   }

   public boolean e(ItemStack var1) {
      return var1 != null && var1.b() == Items.bb;
   }

   public boolean a(EntityAnimal var1) {
      if(var1 == this) {
         return false;
      } else if(!this.cZ()) {
         return false;
      } else if(!(var1 instanceof class_wa)) {
         return false;
      } else {
         class_wa var2 = (class_wa)var1;
         return !var2.cZ()?false:this.df() && var2.df();
      }
   }

   public int dh() {
      return ((Integer)this.datawatcher.a(bz)).intValue();
   }

   public void l(int var1) {
      this.datawatcher.b(bz, Integer.valueOf(var1));
   }

   public boolean cF() {
      return this.world.r.nextInt(3) != 0;
   }

   public boolean cG() {
      if(this.world.a((AxisAlignedBB)this.bk(), (Entity)this) && this.world.a((Entity)this, (AxisAlignedBB)this.bk()).isEmpty() && !this.world.e(this.bk())) {
         BlockPosition var1 = new BlockPosition(this.locX, this.bk().b, this.locZ);
         if(var1.q() < this.world.K()) {
            return false;
         }

         IBlockData var2 = this.world.getType(var1.b());
         Block var3 = var2.getBlock();
         if(var3 == Blocks.c || var2.getMaterial() == Material.j) {
            return true;
         }
      }

      return false;
   }

   public String h_() {
      return this.o_()?this.be():(this.cZ()?class_di.a("entity.Cat.name"):super.h_());
   }

   public void p(boolean var1) {
      super.p(var1);
   }

   protected void da() {
      if(this.bA == null) {
         this.bA = new class_sz(this, EntityHuman.class, 16.0F, 0.8D, 1.33D);
      }

      this.bp.a((class_tj)this.bA);
      if(!this.cZ()) {
         this.bp.a(4, this.bA);
      }

   }

   public class_sc a(class_qk var1, class_sc var2) {
      var2 = super.a(var1, var2);
      if(this.world.r.nextInt(7) == 0) {
         for(int var3 = 0; var3 < 2; ++var3) {
            class_wa var4 = new class_wa(this.world);
            var4.b(this.locX, this.locY, this.locZ, this.yaw, 0.0F);
            var4.b_(-24000);
            this.world.a((Entity)var4);
         }
      }

      return var2;
   }

   // $FF: synthetic method
   public class_rn a(class_rn var1) {
      return this.b(var1);
   }
}
