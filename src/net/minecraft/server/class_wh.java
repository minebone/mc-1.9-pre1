package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.World;
import net.minecraft.server.class_rp;

public abstract class class_wh extends EntityInsentient implements class_rp {
   public class_wh(World var1) {
      super(var1);
   }

   public boolean bA() {
      return true;
   }

   public boolean cF() {
      return true;
   }

   public boolean cG() {
      return this.world.a((AxisAlignedBB)this.bk(), (Entity)this);
   }

   public int C() {
      return 120;
   }

   protected boolean K() {
      return true;
   }

   protected int b(EntityHuman var1) {
      return 1 + this.world.r.nextInt(3);
   }

   public void T() {
      int var1 = this.aO();
      super.T();
      if(this.at() && !this.ah()) {
         --var1;
         this.j(var1);
         if(this.aO() == -20) {
            this.j(0);
            this.a(DamageSource.f, 2.0F);
         }
      } else {
         this.j(300);
      }

   }

   public boolean bc() {
      return false;
   }
}
