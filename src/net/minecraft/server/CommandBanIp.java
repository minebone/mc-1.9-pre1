package net.minecraft.server;

import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.ExceptionPlayerNotFound;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.IpBanEntry;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_k;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.class_mp;

public class CommandBanIp extends CommandAbstract {
   public static final Pattern a = Pattern.compile("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");

   public String c() {
      return "ban-ip";
   }

   public int a() {
      return 3;
   }

   public boolean a(MinecraftServer var1, ICommandListener var2) {
      return var1.getPlayerList().i().b() && super.a(var1, var2);
   }

   public String b(ICommandListener var1) {
      return "commands.banip.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length >= 1 && var3[0].length() > 1) {
         IChatBaseComponent var4 = var3.length >= 2?a(var2, var3, 1):null;
         Matcher var5 = a.matcher(var3[0]);
         if(var5.matches()) {
            this.a(var1, var2, var3[0], var4 == null?null:var4.c());
         } else {
            EntityPlayer var6 = var1.getPlayerList().a(var3[0]);
            if(var6 == null) {
               throw new ExceptionPlayerNotFound("commands.banip.invalid", new Object[0]);
            }

            this.a(var1, var2, var6.A(), var4 == null?null:var4.c());
         }

      } else {
         throw new class_cf("commands.banip.usage", new Object[0]);
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length == 1?a(var3, var1.J()):Collections.emptyList();
   }

   protected void a(MinecraftServer var1, ICommandListener var2, String var3, String var4) {
      IpBanEntry var5 = new IpBanEntry(var3, (Date)null, var2.h_(), (Date)null, var4);
      var1.getPlayerList().i().a((class_mp)var5);
      List var6 = var1.getPlayerList().b(var3);
      String[] var7 = new String[var6.size()];
      int var8 = 0;

      EntityPlayer var10;
      for(Iterator var9 = var6.iterator(); var9.hasNext(); var7[var8++] = var10.h_()) {
         var10 = (EntityPlayer)var9.next();
         var10.a.c("You have been IP banned.");
      }

      if(var6.isEmpty()) {
         a((ICommandListener)var2, (class_k)this, (String)"commands.banip.success", (Object[])(new Object[]{var3}));
      } else {
         a((ICommandListener)var2, (class_k)this, (String)"commands.banip.success.players", (Object[])(new Object[]{var3, a(var7)}));
      }

   }
}
