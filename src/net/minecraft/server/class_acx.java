package net.minecraft.server;

import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.class_aco;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ayw;
import net.minecraft.server.class_ayy;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;

public class class_acx extends class_aco {
   protected class_acx() {
      this.a(CreativeModeTab.f);
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      ItemStack var5 = new ItemStack(Items.bk, 1, var2.b("map"));
      String var6 = "map_" + var5.i();
      class_ayy var7 = new class_ayy(var6);
      var2.a((String)var6, (class_ayw)var7);
      var7.f = 0;
      var7.a(var3.locX, var3.locZ, var7.f);
      var7.d = (byte)var2.s.p().a();
      var7.e = true;
      var7.c();
      --var1.b;
      if(var1.b <= 0) {
         return new class_qo(EnumResult.SUCCESS, var5);
      } else {
         if(!var3.br.c(var5.k())) {
            var3.a(var5, false);
         }

         var3.b(StatisticList.b((Item)this));
         return new class_qo(EnumResult.SUCCESS, var1);
      }
   }
}
