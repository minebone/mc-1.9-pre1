package net.minecraft.server;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import net.minecraft.server.AttributeModifier;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Items;
import net.minecraft.server.MobEffect;
import net.minecraft.server.MobEffectList;
import net.minecraft.server.PathfinderGoalArrowAttack;
import net.minecraft.server.World;
import net.minecraft.server.class_aab;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_afd;
import net.minecraft.server.class_aff;
import net.minecraft.server.class_afg;
import net.minecraft.server.Material;
import net.minecraft.server.class_azs;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_sl;
import net.minecraft.server.class_tg;
import net.minecraft.server.class_to;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_uu;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_yp;
import net.minecraft.server.class_yr;
import net.minecraft.server.class_ys;

public class EntityWitch extends class_yp implements class_yr {
   private static final UUID a = UUID.fromString("5CD17E52-A79A-43D3-A529-90FDE04B181E");
   private static final AttributeModifier b = (new AttributeModifier(a, "Drinking speed penalty", -0.25D, 0)).a(false);
   private static final class_ke c = DataWatcher.a(EntityWitch.class, class_kg.h);
   private int bv;

   public EntityWitch(World var1) {
      super(var1);
      this.a(0.6F, 1.95F);
   }

   protected void r() {
      this.bp.a(1, new class_tg(this));
      this.bp.a(2, new PathfinderGoalArrowAttack(this, 1.0D, 60, 10.0F));
      this.bp.a(2, new class_uf(this, 1.0D));
      this.bp.a(3, new class_to(this, EntityHuman.class, 8.0F));
      this.bp.a(3, new class_ue(this));
      this.bq.a(1, new class_uu(this, false, new Class[0]));
      this.bq.a(2, new class_ux(this, EntityHuman.class, true));
   }

   protected void i() {
      super.i();
      this.Q().a((class_ke)c, (Object)Boolean.valueOf(false));
   }

   protected class_nf G() {
      return class_ng.gt;
   }

   protected class_nf bQ() {
      return class_ng.gw;
   }

   protected class_nf bR() {
      return class_ng.gu;
   }

   public void a(boolean var1) {
      this.Q().b(c, Boolean.valueOf(var1));
   }

   public boolean o() {
      return ((Boolean)this.Q().a(c)).booleanValue();
   }

   protected void bz() {
      super.bz();
      this.a(class_ys.a).a(26.0D);
      this.a(class_ys.d).a(0.25D);
   }

   public void n() {
      if(!this.world.E) {
         if(this.o()) {
            if(this.bv-- <= 0) {
               this.a(false);
               ItemStack var6 = this.ca();
               this.a(EnumInventorySlot.MAINHAND, (ItemStack)null);
               if(var6 != null && var6.b() == Items.bG) {
                  List var5 = class_aff.a(var6);
                  if(var5 != null) {
                     Iterator var3 = var5.iterator();

                     while(var3.hasNext()) {
                        MobEffect var4 = (MobEffect)var3.next();
                        this.c(new MobEffect(var4));
                     }
                  }
               }

               this.a(class_ys.d).c(b);
            }
         } else {
            class_afd var1 = null;
            if(this.random.nextFloat() < 0.15F && this.a(Material.h) && !this.a(MobEffectList.m)) {
               var1 = class_afg.t;
            } else if(this.random.nextFloat() < 0.15F && this.aG() && !this.a(MobEffectList.l)) {
               var1 = class_afg.m;
            } else if(this.random.nextFloat() < 0.05F && this.bP() < this.bV()) {
               var1 = class_afg.v;
            } else if(this.random.nextFloat() < 0.5F && this.A() != null && !this.a(MobEffectList.a) && this.A().h(this) > 121.0D) {
               var1 = class_afg.o;
            }

            if(var1 != null) {
               this.a(EnumInventorySlot.MAINHAND, class_aff.a(new ItemStack(Items.bG), var1));
               this.bv = this.ca().l();
               this.a(true);
               this.world.a((EntityHuman)null, this.locX, this.locY, this.locZ, class_ng.gv, this.by(), 1.0F, 0.8F + this.random.nextFloat() * 0.4F);
               class_sl var2 = this.a(class_ys.d);
               var2.c(b);
               var2.b(b);
            }
         }

         if(this.random.nextFloat() < 7.5E-4F) {
            this.world.a((Entity)this, (byte)15);
         }
      }

      super.n();
   }

   protected float c(DamageSource var1, float var2) {
      var2 = super.c(var1, var2);
      if(var1.j() == this) {
         var2 = 0.0F;
      }

      if(var1.s()) {
         var2 = (float)((double)var2 * 0.15D);
      }

      return var2;
   }

   protected class_kk J() {
      return class_azs.n;
   }

   public void a(class_rz var1, float var2) {
      if(!this.o()) {
         double var3 = var1.locY + (double)var1.bm() - 1.100000023841858D;
         double var5 = var1.locX + var1.motX - this.locX;
         double var7 = var3 - this.locY;
         double var9 = var1.locZ + var1.motZ - this.locZ;
         float var11 = MathHelper.a(var5 * var5 + var9 * var9);
         class_afd var12 = class_afg.x;
         if(var11 >= 8.0F && !var1.a(MobEffectList.b)) {
            var12 = class_afg.r;
         } else if(var1.bP() >= 8.0F && !var1.a(MobEffectList.s)) {
            var12 = class_afg.z;
         } else if(var11 <= 3.0F && !var1.a(MobEffectList.r) && this.random.nextFloat() < 0.25F) {
            var12 = class_afg.I;
         }

         class_aab var13 = new class_aab(this.world, this, class_aff.a(new ItemStack(Items.bH), var12));
         var13.pitch -= -20.0F;
         var13.c(var5, var7 + (double)(var11 * 0.2F), var9, 0.75F, 8.0F);
         this.world.a((EntityHuman)null, this.locX, this.locY, this.locZ, class_ng.gx, this.by(), 1.0F, 0.8F + this.random.nextFloat() * 0.4F);
         this.world.a((Entity)var13);
      }
   }

   public float bm() {
      return 1.62F;
   }
}
