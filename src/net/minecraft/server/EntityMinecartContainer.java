package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.ChestLock;
import net.minecraft.server.Container;
import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityMinecartAbstract;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.World;
import net.minecraft.server.WorldServer;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_azx;
import net.minecraft.server.class_azy;
import net.minecraft.server.class_kk;
import net.minecraft.server.IInventory;
import net.minecraft.server.class_qg;
import net.minecraft.server.class_qi;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_qr;
import net.minecraft.server.class_qt;

public abstract class EntityMinecartContainer extends EntityMinecartAbstract implements class_qr, class_qt {
   private ItemStack[] a = new ItemStack[36];
   private boolean b = true;
   private class_kk c;
   private long d;

   public EntityMinecartContainer(World var1) {
      super(var1);
   }

   public EntityMinecartContainer(World var1, double var2, double var4, double var6) {
      super(var1, var2, var4, var6);
   }

   public void a(DamageSource var1) {
      super.a(var1);
      if(this.world.U().b("doEntityDrops")) {
         class_qi.a(this.world, (Entity)this, this);
      }

   }

   public ItemStack a(int var1) {
      this.f((EntityHuman)null);
      return this.a[var1];
   }

   public ItemStack a(int var1, int var2) {
      this.f((EntityHuman)null);
      return class_qg.a(this.a, var1, var2);
   }

   public ItemStack b(int var1) {
      this.f((EntityHuman)null);
      if(this.a[var1] != null) {
         ItemStack var2 = this.a[var1];
         this.a[var1] = null;
         return var2;
      } else {
         return null;
      }
   }

   public void a(int var1, ItemStack var2) {
      this.f((EntityHuman)null);
      this.a[var1] = var2;
      if(var2 != null && var2.b > this.w_()) {
         var2.b = this.w_();
      }

   }

   public void v_() {
   }

   public boolean a(EntityHuman var1) {
      return this.dead?false:var1.h(this) <= 64.0D;
   }

   public void b(EntityHuman var1) {
   }

   public void c(EntityHuman var1) {
   }

   public boolean b(int var1, ItemStack var2) {
      return true;
   }

   public String h_() {
      return this.o_()?this.be():"container.minecart";
   }

   public int w_() {
      return 64;
   }

   public Entity c(int var1) {
      this.b = false;
      return super.c(var1);
   }

   public void S() {
      if(this.b) {
         class_qi.a(this.world, (Entity)this, this);
      }

      super.S();
   }

   public void b(boolean var1) {
      this.b = var1;
   }

   protected void b(NBTTagCompound var1) {
      super.b(var1);
      if(this.c != null) {
         var1.a("LootTable", this.c.toString());
         if(this.d != 0L) {
            var1.a("LootTableSeed", this.d);
         }
      } else {
         NBTTagList var2 = new NBTTagList();

         for(int var3 = 0; var3 < this.a.length; ++var3) {
            if(this.a[var3] != null) {
               NBTTagCompound var4 = new NBTTagCompound();
               var4.a("Slot", (byte)var3);
               this.a[var3].b(var4);
               var2.a((NBTTag)var4);
            }
         }

         var1.a((String)"Items", (NBTTag)var2);
      }

   }

   protected void a(NBTTagCompound var1) {
      super.a(var1);
      this.a = new ItemStack[this.u_()];
      if(var1.b("LootTable", 8)) {
         this.c = new class_kk(var1.l("LootTable"));
         this.d = var1.i("LootTableSeed");
      } else {
         NBTTagList var2 = var1.c("Items", 10);

         for(int var3 = 0; var3 < var2.c(); ++var3) {
            NBTTagCompound var4 = var2.b(var3);
            int var5 = var4.f("Slot") & 255;
            if(var5 >= 0 && var5 < this.a.length) {
               this.a[var5] = ItemStack.a(var4);
            }
         }
      }

   }

   public boolean a(EntityHuman var1, ItemStack var2, EnumHand var3) {
      if(!this.world.E) {
         var1.openContainer((IInventory)this);
      }

      return true;
   }

   protected void r() {
      float var1 = 0.98F;
      if(this.c == null) {
         int var2 = 15 - Container.b((IInventory)this);
         var1 += (float)var2 * 0.001F;
      }

      this.motX *= (double)var1;
      this.motY *= 0.0D;
      this.motZ *= (double)var1;
   }

   public int c_(int var1) {
      return 0;
   }

   public void b(int var1, int var2) {
   }

   public int g() {
      return 0;
   }

   public boolean x_() {
      return false;
   }

   public void a(ChestLock var1) {
   }

   public ChestLock y_() {
      return ChestLock.a;
   }

   public void f(EntityHuman var1) {
      if(this.c != null) {
         class_azx var2 = this.world.ak().a(this.c);
         this.c = null;
         Random var3;
         if(this.d == 0L) {
            var3 = new Random();
         } else {
            var3 = new Random(this.d);
         }

         class_azy.class_a_in_class_azy var4 = new class_azy.class_a_in_class_azy((WorldServer)this.world);
         if(var1 != null) {
            var4.a(var1.da());
         }

         var2.a(this, var3, var4.a());
      }

   }

   public void l() {
      this.f((EntityHuman)null);

      for(int var1 = 0; var1 < this.a.length; ++var1) {
         this.a[var1] = null;
      }

   }

   public void a(class_kk var1, long var2) {
      this.c = var1;
      this.d = var2;
   }

   public class_kk b() {
      return this.c;
   }
}
