package net.minecraft.server;

import java.util.List;
import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.Blocks;
import net.minecraft.server.Chunk;
import net.minecraft.server.CustomWorldSettingsFinal;
import net.minecraft.server.EnumCreatureType;
import net.minecraft.server.World;
import net.minecraft.server.WorldGenDungeons;
import net.minecraft.server.WorldGenLargeFeature;
import net.minecraft.server.WorldGenMineshaft;
import net.minecraft.server.WorldGenMonument;
import net.minecraft.server.WorldGenStronghold;
import net.minecraft.server.WorldGenVillage;
import net.minecraft.server.WorldType;
import net.minecraft.server.class_ahm;
import net.minecraft.server.class_ahz;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_alg;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arx;
import net.minecraft.server.class_atd;
import net.minecraft.server.class_ate;
import net.minecraft.server.class_ati;
import net.minecraft.server.class_atj;
import net.minecraft.server.class_aul;
import net.minecraft.server.class_awt;
import net.minecraft.server.class_awu;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;

public class class_atm implements class_arx {
   protected static final IBlockData a = Blocks.b.u();
   private final Random i;
   private final class_awt j;
   private final class_awt k;
   private final class_awt l;
   private final class_awu m;
   public class_awt b;
   public class_awt c;
   public class_awt d;
   private final World n;
   private final boolean o;
   private final WorldType p;
   private final double[] q;
   private final float[] r;
   private CustomWorldSettingsFinal s;
   private IBlockData t = Blocks.j.u();
   private double[] u = new double[256];
   private final class_atj v = new class_ati();
   private final WorldGenStronghold w = new WorldGenStronghold();
   private final WorldGenVillage x = new WorldGenVillage();
   private final WorldGenMineshaft y = new WorldGenMineshaft();
   private final WorldGenLargeFeature z = new WorldGenLargeFeature();
   private final class_atj A = new class_atd();
   private final WorldGenMonument B = new WorldGenMonument();
   private BiomeBase[] C;
   double[] e;
   double[] f;
   double[] g;
   double[] h;

   public class_atm(World var1, long var2, boolean var4, String var5) {
      this.n = var1;
      this.o = var4;
      this.p = var1.T().t();
      this.i = new Random(var2);
      this.j = new class_awt(this.i, 16);
      this.k = new class_awt(this.i, 16);
      this.l = new class_awt(this.i, 8);
      this.m = new class_awu(this.i, 4);
      this.b = new class_awt(this.i, 10);
      this.c = new class_awt(this.i, 16);
      this.d = new class_awt(this.i, 8);
      this.q = new double[825];
      this.r = new float[25];

      for(int var6 = -2; var6 <= 2; ++var6) {
         for(int var7 = -2; var7 <= 2; ++var7) {
            float var8 = 10.0F / MathHelper.c((float)(var6 * var6 + var7 * var7) + 0.2F);
            this.r[var6 + 2 + (var7 + 2) * 5] = var8;
         }
      }

      if(var5 != null) {
         this.s = CustomWorldSettingsFinal.class_a_in_class_atf.a(var5).b();
         this.t = this.s.E?Blocks.l.u():Blocks.j.u();
         var1.b(this.s.q);
      }

   }

   public void a(int var1, int var2, class_ate var3) {
      this.C = this.n.A().a(this.C, var1 * 4 - 2, var2 * 4 - 2, 10, 10);
      this.a(var1 * 4, 0, var2 * 4);

      for(int var4 = 0; var4 < 4; ++var4) {
         int var5 = var4 * 5;
         int var6 = (var4 + 1) * 5;

         for(int var7 = 0; var7 < 4; ++var7) {
            int var8 = (var5 + var7) * 33;
            int var9 = (var5 + var7 + 1) * 33;
            int var10 = (var6 + var7) * 33;
            int var11 = (var6 + var7 + 1) * 33;

            for(int var12 = 0; var12 < 32; ++var12) {
               double var13 = 0.125D;
               double var15 = this.q[var8 + var12];
               double var17 = this.q[var9 + var12];
               double var19 = this.q[var10 + var12];
               double var21 = this.q[var11 + var12];
               double var23 = (this.q[var8 + var12 + 1] - var15) * var13;
               double var25 = (this.q[var9 + var12 + 1] - var17) * var13;
               double var27 = (this.q[var10 + var12 + 1] - var19) * var13;
               double var29 = (this.q[var11 + var12 + 1] - var21) * var13;

               for(int var31 = 0; var31 < 8; ++var31) {
                  double var32 = 0.25D;
                  double var34 = var15;
                  double var36 = var17;
                  double var38 = (var19 - var15) * var32;
                  double var40 = (var21 - var17) * var32;

                  for(int var42 = 0; var42 < 4; ++var42) {
                     double var43 = 0.25D;
                     double var47 = (var36 - var34) * var43;
                     double var45 = var34 - var47;

                     for(int var49 = 0; var49 < 4; ++var49) {
                        if((var45 += var47) > 0.0D) {
                           var3.a(var4 * 4 + var42, var12 * 8 + var31, var7 * 4 + var49, a);
                        } else if(var12 * 8 + var31 < this.s.q) {
                           var3.a(var4 * 4 + var42, var12 * 8 + var31, var7 * 4 + var49, this.t);
                        }
                     }

                     var34 += var38;
                     var36 += var40;
                  }

                  var15 += var23;
                  var17 += var25;
                  var19 += var27;
                  var21 += var29;
               }
            }
         }
      }

   }

   public void a(int var1, int var2, class_ate var3, BiomeBase[] var4) {
      double var5 = 0.03125D;
      this.u = this.m.a(this.u, (double)(var1 * 16), (double)(var2 * 16), 16, 16, var5 * 2.0D, var5 * 2.0D, 1.0D);

      for(int var7 = 0; var7 < 16; ++var7) {
         for(int var8 = 0; var8 < 16; ++var8) {
            BiomeBase var9 = var4[var8 + var7 * 16];
            var9.a(this.n, this.i, var3, var1 * 16 + var7, var2 * 16 + var8, this.u[var8 + var7 * 16]);
         }
      }

   }

   public Chunk a(int var1, int var2) {
      this.i.setSeed((long)var1 * 341873128712L + (long)var2 * 132897987541L);
      class_ate var3 = new class_ate();
      this.a(var1, var2, var3);
      this.C = this.n.A().b(this.C, var1 * 16, var2 * 16, 16, 16);
      this.a(var1, var2, var3, this.C);
      if(this.s.r) {
         this.v.a(this.n, var1, var2, var3);
      }

      if(this.s.z) {
         this.A.a(this.n, var1, var2, var3);
      }

      if(this.o) {
         if(this.s.w) {
            this.y.a(this.n, var1, var2, var3);
         }

         if(this.s.v) {
            this.x.a(this.n, var1, var2, var3);
         }

         if(this.s.u) {
            this.w.a(this.n, var1, var2, var3);
         }

         if(this.s.x) {
            this.z.a(this.n, var1, var2, var3);
         }

         if(this.s.y) {
            this.B.a(this.n, var1, var2, var3);
         }
      }

      Chunk var4 = new Chunk(this.n, var3, var1, var2);
      byte[] var5 = var4.l();

      for(int var6 = 0; var6 < var5.length; ++var6) {
         var5[var6] = (byte)BiomeBase.a(this.C[var6]);
      }

      var4.b();
      return var4;
   }

   private void a(int var1, int var2, int var3) {
      this.h = this.c.a(this.h, var1, var3, 5, 5, (double)this.s.e, (double)this.s.f, (double)this.s.g);
      float var4 = this.s.a;
      float var5 = this.s.b;
      this.e = this.l.a(this.e, var1, var2, var3, 5, 33, 5, (double)(var4 / this.s.h), (double)(var5 / this.s.i), (double)(var4 / this.s.j));
      this.f = this.j.a(this.f, var1, var2, var3, 5, 33, 5, (double)var4, (double)var5, (double)var4);
      this.g = this.k.a(this.g, var1, var2, var3, 5, 33, 5, (double)var4, (double)var5, (double)var4);
      boolean var37 = false;
      boolean var36 = false;
      int var6 = 0;
      int var7 = 0;

      for(int var8 = 0; var8 < 5; ++var8) {
         for(int var9 = 0; var9 < 5; ++var9) {
            float var10 = 0.0F;
            float var11 = 0.0F;
            float var12 = 0.0F;
            byte var13 = 2;
            BiomeBase var14 = this.C[var8 + 2 + (var9 + 2) * 10];

            for(int var15 = -var13; var15 <= var13; ++var15) {
               for(int var16 = -var13; var16 <= var13; ++var16) {
                  BiomeBase var17 = this.C[var8 + var15 + 2 + (var9 + var16 + 2) * 10];
                  float var18 = this.s.n + var17.j() * this.s.m;
                  float var19 = this.s.p + var17.m() * this.s.o;
                  if(this.p == WorldType.e && var18 > 0.0F) {
                     var18 = 1.0F + var18 * 2.0F;
                     var19 = 1.0F + var19 * 4.0F;
                  }

                  float var20 = this.r[var15 + 2 + (var16 + 2) * 5] / (var18 + 2.0F);
                  if(var17.j() > var14.j()) {
                     var20 /= 2.0F;
                  }

                  var10 += var19 * var20;
                  var11 += var18 * var20;
                  var12 += var20;
               }
            }

            var10 /= var12;
            var11 /= var12;
            var10 = var10 * 0.9F + 0.1F;
            var11 = (var11 * 4.0F - 1.0F) / 8.0F;
            double var38 = this.h[var7] / 8000.0D;
            if(var38 < 0.0D) {
               var38 = -var38 * 0.3D;
            }

            var38 = var38 * 3.0D - 2.0D;
            if(var38 < 0.0D) {
               var38 /= 2.0D;
               if(var38 < -1.0D) {
                  var38 = -1.0D;
               }

               var38 /= 1.4D;
               var38 /= 2.0D;
            } else {
               if(var38 > 1.0D) {
                  var38 = 1.0D;
               }

               var38 /= 8.0D;
            }

            ++var7;
            double var39 = (double)var11;
            double var40 = (double)var10;
            var39 += var38 * 0.2D;
            var39 = var39 * (double)this.s.k / 8.0D;
            double var21 = (double)this.s.k + var39 * 4.0D;

            for(int var23 = 0; var23 < 33; ++var23) {
               double var24 = ((double)var23 - var21) * (double)this.s.l * 128.0D / 256.0D / var40;
               if(var24 < 0.0D) {
                  var24 *= 4.0D;
               }

               double var26 = this.f[var6] / (double)this.s.d;
               double var28 = this.g[var6] / (double)this.s.c;
               double var30 = (this.e[var6] / 10.0D + 1.0D) / 2.0D;
               double var32 = MathHelper.b(var26, var28, var30) - var24;
               if(var23 > 29) {
                  double var34 = (double)((float)(var23 - 29) / 3.0F);
                  var32 = var32 * (1.0D - var34) + -10.0D * var34;
               }

               this.q[var6] = var32;
               ++var6;
            }
         }
      }

   }

   public void b(int var1, int var2) {
      class_alg.f = true;
      int var3 = var1 * 16;
      int var4 = var2 * 16;
      BlockPosition var5 = new BlockPosition(var3, 0, var4);
      BiomeBase var6 = this.n.b(var5.a(16, 0, 16));
      this.i.setSeed(this.n.O());
      long var7 = this.i.nextLong() / 2L * 2L + 1L;
      long var9 = this.i.nextLong() / 2L * 2L + 1L;
      this.i.setSeed((long)var1 * var7 + (long)var2 * var9 ^ this.n.O());
      boolean var11 = false;
      class_ahm var12 = new class_ahm(var1, var2);
      if(this.o) {
         if(this.s.w) {
            this.y.a(this.n, this.i, var12);
         }

         if(this.s.v) {
            var11 = this.x.a(this.n, this.i, var12);
         }

         if(this.s.u) {
            this.w.a(this.n, this.i, var12);
         }

         if(this.s.x) {
            this.z.a(this.n, this.i, var12);
         }

         if(this.s.y) {
            this.B.a(this.n, this.i, var12);
         }
      }

      int var13;
      int var14;
      int var15;
      if(var6 != class_aik.d && var6 != class_aik.s && this.s.A && !var11 && this.i.nextInt(this.s.B) == 0) {
         var13 = this.i.nextInt(16) + 8;
         var14 = this.i.nextInt(256);
         var15 = this.i.nextInt(16) + 8;
         (new class_aul(Blocks.j)).b(this.n, this.i, var5.a(var13, var14, var15));
      }

      if(!var11 && this.i.nextInt(this.s.D / 10) == 0 && this.s.C) {
         var13 = this.i.nextInt(16) + 8;
         var14 = this.i.nextInt(this.i.nextInt(248) + 8);
         var15 = this.i.nextInt(16) + 8;
         if(var14 < this.n.K() || this.i.nextInt(this.s.D / 8) == 0) {
            (new class_aul(Blocks.l)).b(this.n, this.i, var5.a(var13, var14, var15));
         }
      }

      if(this.s.s) {
         for(var13 = 0; var13 < this.s.t; ++var13) {
            var14 = this.i.nextInt(16) + 8;
            var15 = this.i.nextInt(256);
            int var16 = this.i.nextInt(16) + 8;
            (new WorldGenDungeons()).b(this.n, this.i, var5.a(var14, var15, var16));
         }
      }

      var6.a(this.n, this.i, new BlockPosition(var3, 0, var4));
      class_ahz.a(this.n, var6, var3 + 8, var4 + 8, 16, 16, this.i);
      var5 = var5.a(8, 0, 8);

      for(var13 = 0; var13 < 16; ++var13) {
         for(var14 = 0; var14 < 16; ++var14) {
            BlockPosition var17 = this.n.p(var5.a(var13, 0, var14));
            BlockPosition var18 = var17.b();
            if(this.n.u(var18)) {
               this.n.a((BlockPosition)var18, (IBlockData)Blocks.aI.u(), 2);
            }

            if(this.n.f(var17, true)) {
               this.n.a((BlockPosition)var17, (IBlockData)Blocks.aH.u(), 2);
            }
         }
      }

      class_alg.f = false;
   }

   public boolean a(Chunk var1, int var2, int var3) {
      boolean var4 = false;
      if(this.s.y && this.o && var1.x() < 3600L) {
         var4 |= this.B.a(this.n, this.i, new class_ahm(var2, var3));
      }

      return var4;
   }

   public List a(EnumCreatureType var1, BlockPosition var2) {
      BiomeBase var3 = this.n.b(var2);
      if(this.o) {
         if(var1 == EnumCreatureType.MONSTER && this.z.a(var2)) {
            return this.z.b();
         }

         if(var1 == EnumCreatureType.MONSTER && this.s.y && this.B.a(this.n, var2)) {
            return this.B.b();
         }
      }

      return var3.a(var1);
   }

   public BlockPosition a(World var1, String var2, BlockPosition var3) {
      return "Stronghold".equals(var2) && this.w != null?this.w.b(var1, var3):null;
   }

   public void b(Chunk var1, int var2, int var3) {
      if(this.o) {
         if(this.s.w) {
            this.y.a(this.n, var2, var3, (class_ate)null);
         }

         if(this.s.v) {
            this.x.a(this.n, var2, var3, (class_ate)null);
         }

         if(this.s.u) {
            this.w.a(this.n, var2, var3, (class_ate)null);
         }

         if(this.s.x) {
            this.z.a(this.n, var2, var3, (class_ate)null);
         }

         if(this.s.y) {
            this.B.a(this.n, var2, var3, (class_ate)null);
         }
      }

   }
}
