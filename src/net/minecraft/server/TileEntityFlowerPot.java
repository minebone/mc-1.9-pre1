package net.minecraft.server;

import net.minecraft.server.Item;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutTileEntityData;
import net.minecraft.server.class_kk;

public class TileEntityFlowerPot extends TileEntity {
   private Item a;
   private int f;

   public TileEntityFlowerPot() {
   }

   public TileEntityFlowerPot(Item var1, int var2) {
      this.a = var1;
      this.f = var2;
   }

   public void a(NBTTagCompound var1) {
      super.a(var1);
      class_kk var2 = (class_kk)Item.f.b(this.a);
      var1.a("Item", var2 == null?"":var2.toString());
      var1.a("Data", this.f);
   }

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      if(var2.b("Item", 8)) {
         this.a = Item.d(var2.l("Item"));
      } else {
         this.a = Item.c(var2.h("Item"));
      }

      this.f = var2.h("Data");
   }

   public Packet D_() {
      NBTTagCompound var1 = new NBTTagCompound();
      this.a(var1);
      var1.q("Item");
      var1.a("Item", Item.a(this.a));
      return new PacketPlayOutTileEntityData(this.c, 5, var1);
   }

   public void a(Item var1, int var2) {
      this.a = var1;
      this.f = var2;
   }

   public ItemStack b() {
      return this.a == null?null:new ItemStack(this.a, 1, this.f);
   }

   public Item c() {
      return this.a;
   }

   public int d() {
      return this.f;
   }
}
