package net.minecraft.server;

import java.util.Collections;
import java.util.List;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.MojangsonParser;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_bz;
import net.minecraft.server.class_cf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ec;
import net.minecraft.server.ICommandListener;
import net.minecraft.server.IInventory;

public class CommandSetBlock extends CommandAbstract {
   public String c() {
      return "setblock";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.setblock.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      if(var3.length < 4) {
         throw new class_cf("commands.setblock.usage", new Object[0]);
      } else {
         var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_BLOCKS, 0);
         BlockPosition var4 = a(var2, var3, 0, false);
         Block var5 = CommandAbstract.b(var2, var3[3]);
         int var6 = 0;
         if(var3.length >= 5) {
            var6 = a(var3[4], 0, 15);
         }

         World var7 = var2.e();
         if(!var7.e(var4)) {
            throw new class_bz("commands.setblock.outOfWorld", new Object[0]);
         } else {
            NBTTagCompound var8 = new NBTTagCompound();
            boolean var9 = false;
            if(var3.length >= 7 && var5.m()) {
               String var10 = a(var2, var3, 6).c();

               try {
                  var8 = MojangsonParser.a(var10);
                  var9 = true;
               } catch (class_ec var13) {
                  throw new class_bz("commands.setblock.tagError", new Object[]{var13.getMessage()});
               }
            }

            if(var3.length >= 6) {
               if(var3[5].equals("destroy")) {
                  var7.b(var4, true);
                  if(var5 == Blocks.AIR) {
                     a(var2, this, "commands.setblock.success", new Object[0]);
                     return;
                  }
               } else if(var3[5].equals("keep") && !var7.d(var4)) {
                  throw new class_bz("commands.setblock.noChange", new Object[0]);
               }
            }

            TileEntity var14 = var7.r(var4);
            if(var14 != null) {
               if(var14 instanceof IInventory) {
                  ((IInventory)var14).l();
               }

               var7.a(var4, Blocks.AIR.u(), var5 == Blocks.AIR?2:4);
            }

            IBlockData var11 = var5.a(var6);
            if(!var7.a((BlockPosition)var4, (IBlockData)var11, 2)) {
               throw new class_bz("commands.setblock.noChange", new Object[0]);
            } else {
               if(var9) {
                  TileEntity var12 = var7.r(var4);
                  if(var12 != null) {
                     var8.a("x", var4.p());
                     var8.a("y", var4.q());
                     var8.a("z", var4.r());
                     var12.a(var1, var8);
                  }
               }

               var7.c(var4, var11.getBlock());
               var2.a(CommandObjectiveExecutor.EnumCommandResult.AFFECTED_BLOCKS, 1);
               a(var2, this, "commands.setblock.success", new Object[0]);
            }
         }
      }
   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      return var3.length > 0 && var3.length <= 3?a(var3, 0, var4):(var3.length == 4?a(var3, Block.h.c()):(var3.length == 6?a(var3, new String[]{"replace", "destroy", "keep"}):Collections.emptyList()));
   }
}
