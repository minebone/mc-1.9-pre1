package net.minecraft.server;

import net.minecraft.server.ChatMessage;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.World;
import net.minecraft.server.class_bz;
import net.minecraft.server.ICommandListener;

public class CommandSeed extends CommandAbstract {
   public boolean a(MinecraftServer var1, ICommandListener var2) {
      return var1.R() || super.a(var1, var2);
   }

   public String c() {
      return "seed";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.seed.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      Object var4 = var2 instanceof EntityHuman?((EntityHuman)var2).world:var1.a(0);
      var2.a(new ChatMessage("commands.seed.success", new Object[]{Long.valueOf(((World)var4).O())}));
   }
}
