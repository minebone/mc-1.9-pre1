package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.Container;
import net.minecraft.server.ContainerEnchantTable;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.IChatBaseComponent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.PlayerInventory;
import net.minecraft.server.TileEntity;
import net.minecraft.server.class_ky;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_qm;

public class TileEntityEnchantTable extends TileEntity implements class_ky, class_qm {
   public int a;
   public float f;
   public float g;
   public float h;
   public float i;
   public float j;
   public float k;
   public float l;
   public float m;
   public float n;
   private static Random o = new Random();
   private String p;

   public void a(NBTTagCompound var1) {
      super.a(var1);
      if(this.o_()) {
         var1.a("CustomName", this.p);
      }

   }

   public void a(MinecraftServer var1, NBTTagCompound var2) {
      super.a(var1, var2);
      if(var2.b("CustomName", 8)) {
         this.p = var2.l("CustomName");
      }

   }

   public void c() {
      this.k = this.j;
      this.m = this.l;
      EntityHuman var1 = this.b.a((double)((float)this.c.p() + 0.5F), (double)((float)this.c.q() + 0.5F), (double)((float)this.c.r() + 0.5F), 3.0D, false);
      if(var1 != null) {
         double var2 = var1.locX - (double)((float)this.c.p() + 0.5F);
         double var4 = var1.locZ - (double)((float)this.c.r() + 0.5F);
         this.n = (float)MathHelper.b(var4, var2);
         this.j += 0.1F;
         if(this.j < 0.5F || o.nextInt(40) == 0) {
            float var6 = this.h;

            do {
               this.h += (float)(o.nextInt(4) - o.nextInt(4));
            } while(var6 == this.h);
         }
      } else {
         this.n += 0.02F;
         this.j -= 0.1F;
      }

      while(this.l >= 3.1415927F) {
         this.l -= 6.2831855F;
      }

      while(this.l < -3.1415927F) {
         this.l += 6.2831855F;
      }

      while(this.n >= 3.1415927F) {
         this.n -= 6.2831855F;
      }

      while(this.n < -3.1415927F) {
         this.n += 6.2831855F;
      }

      float var7;
      for(var7 = this.n - this.l; var7 >= 3.1415927F; var7 -= 6.2831855F) {
         ;
      }

      while(var7 < -3.1415927F) {
         var7 += 6.2831855F;
      }

      this.l += var7 * 0.4F;
      this.j = MathHelper.a(this.j, 0.0F, 1.0F);
      ++this.a;
      this.g = this.f;
      float var3 = (this.h - this.f) * 0.4F;
      float var8 = 0.2F;
      var3 = MathHelper.a(var3, -var8, var8);
      this.i += (var3 - this.i) * 0.9F;
      this.f += this.i;
   }

   public String h_() {
      return this.o_()?this.p:"container.enchant";
   }

   public boolean o_() {
      return this.p != null && !this.p.isEmpty();
   }

   public void a(String var1) {
      this.p = var1;
   }

   public IChatBaseComponent i_() {
      return (IChatBaseComponent)(this.o_()?new ChatComponentText(this.h_()):new ChatMessage(this.h_(), new Object[0]));
   }

   public Container a(PlayerInventory var1, EntityHuman var2) {
      return new ContainerEnchantTable(var1, this.b, this.c);
   }

   public String k() {
      return "minecraft:enchanting_table";
   }
}
