package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumSkyBlock;
import net.minecraft.server.Item;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ajm;
import net.minecraft.server.class_aoa;
import net.minecraft.server.class_aoo;
import net.minecraft.server.class_apz;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;

public class BlockDaylightDetector extends class_ajm {
   public static final BlockStateInteger a = BlockStateInteger.a("power", 0, 15);
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.375D, 1.0D);
   private final boolean c;

   public BlockDaylightDetector(boolean var1) {
      super(Material.d);
      this.c = var1;
      this.w(this.A.b().set(a, Integer.valueOf(0)));
      this.a(CreativeModeTab.d);
      this.c(0.2F);
      this.a(class_aoo.a);
      this.c("daylightDetector");
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return b;
   }

   public int b(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return ((Integer)var1.get(a)).intValue();
   }

   public void c(World var1, BlockPosition var2) {
      if(!var1.s.isNotOverworld()) {
         IBlockData var3 = var1.getType(var2);
         int var4 = var1.b(EnumSkyBlock.SKY, var2) - var1.af();
         float var5 = var1.d(1.0F);
         if(this.c) {
            var4 = 15 - var4;
         }

         if(var4 > 0 && !this.c) {
            float var6 = var5 < 3.1415927F?0.0F:6.2831855F;
            var5 += (var6 - var5) * 0.2F;
            var4 = Math.round((float)var4 * MathHelper.b(var5));
         }

         var4 = MathHelper.a(var4, 0, 15);
         if(((Integer)var3.get(a)).intValue() != var4) {
            var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Integer.valueOf(var4)), 3);
         }

      }
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(var4.cT()) {
         if(var1.E) {
            return true;
         } else {
            if(this.c) {
               var1.a((BlockPosition)var2, (IBlockData)Blocks.cl.u().set(a, var3.get(a)), 4);
               Blocks.cl.c(var1, var2);
            } else {
               var1.a((BlockPosition)var2, (IBlockData)Blocks.cm.u().set(a, var3.get(a)), 4);
               Blocks.cm.c(var1, var2);
            }

            return true;
         }
      } else {
         return super.a(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
      }
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Item.a((Block)Blocks.cl);
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Blocks.cl);
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public class_aoa a(IBlockData var1) {
      return class_aoa.MODEL;
   }

   public boolean g(IBlockData var1) {
      return true;
   }

   public TileEntity a(World var1, int var2) {
      return new class_apz();
   }

   public IBlockData a(int var1) {
      return this.u().set(a, Integer.valueOf(var1));
   }

   public int e(IBlockData var1) {
      return ((Integer)var1.get(a)).intValue();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }
}
