package net.minecraft.server;

import net.minecraft.server.World;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_yp;
import net.minecraft.server.class_ys;

public class class_ym extends class_yp {
   public class_ym(World var1) {
      super(var1);
      this.a(this.width * 6.0F, this.length * 6.0F);
   }

   public float bm() {
      return 10.440001F;
   }

   protected void bz() {
      super.bz();
      this.a(class_ys.a).a(100.0D);
      this.a(class_ys.d).a(0.5D);
      this.a(class_ys.e).a(50.0D);
   }

   public float a(BlockPosition var1) {
      return this.world.n(var1) - 0.5F;
   }

   protected class_kk J() {
      return class_azs.s;
   }
}
