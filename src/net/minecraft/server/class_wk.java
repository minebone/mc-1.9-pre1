package net.minecraft.server;

import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.ItemStack;

public enum class_wk {
   NONE(0),
   IRON(5, "iron", "meo"),
   GOLD(7, "gold", "goo"),
   DIAMOND(11, "diamond", "dio");

   private final String e;
   private final String f;
   private final int g;

   private class_wk(int var3) {
      this.g = var3;
      this.e = null;
      this.f = "";
   }

   private class_wk(int var3, String var4, String var5) {
      this.g = var3;
      this.e = "textures/entity/horse/armor/horse_armor_" + var4 + ".png";
      this.f = var5;
   }

   public int a() {
      return this.ordinal();
   }

   public int c() {
      return this.g;
   }

   public static class_wk a(int var0) {
      return values()[var0];
   }

   public static class_wk a(ItemStack var0) {
      return var0 == null?NONE:a(var0.b());
   }

   public static class_wk a(Item var0) {
      return var0 == Items.cu?IRON:(var0 == Items.cv?GOLD:(var0 == Items.cw?DIAMOND:NONE));
   }

   public static boolean b(Item var0) {
      return a(var0) != NONE;
   }
}
