package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.IBlockData;

public class class_ate {
   private static final IBlockData a = Blocks.AIR.u();
   private final char[] b = new char[65536];

   public IBlockData a(int var1, int var2, int var3) {
      IBlockData var4 = (IBlockData)Block.i.a(this.b[b(var1, var2, var3)]);
      return var4 == null?a:var4;
   }

   public void a(int var1, int var2, int var3, IBlockData var4) {
      this.b[b(var1, var2, var3)] = (char)Block.i.a(var4);
   }

   private static int b(int var0, int var1, int var2) {
      return var0 << 12 | var2 << 8 | var1;
   }

   public int a(int var1, int var2) {
      int var3 = (var1 << 12 | var2 << 8) + 256 - 1;

      for(int var4 = 255; var4 >= 0; --var4) {
         IBlockData var5 = (IBlockData)Block.i.a(this.b[var3 + var4]);
         if(var5 != null && var5 != a) {
            return var4;
         }
      }

      return 0;
   }
}
