package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockRedstoneWire;
import net.minecraft.server.Blocks;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_amf;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_rz;

public abstract class class_akq extends class_amf {
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.125D, 1.0D);
   protected final boolean d;

   protected class_akq(boolean var1) {
      super(Material.q);
      this.d = var1;
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return c;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public boolean a(World var1, BlockPosition var2) {
      return var1.getType(var2.b()).q()?super.a(var1, var2):false;
   }

   public boolean b(World var1, BlockPosition var2) {
      return var1.getType(var2.b()).q();
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Random var4) {
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(!this.b(var1, var2, (IBlockData)var3)) {
         boolean var5 = this.e(var1, var2, var3);
         if(this.d && !var5) {
            var1.a((BlockPosition)var2, (IBlockData)this.y(var3), 2);
         } else if(!this.d) {
            var1.a((BlockPosition)var2, (IBlockData)this.x(var3), 2);
            if(!var5) {
               var1.a(var2, this.x(var3).getBlock(), this.D(var3), -1);
            }
         }

      }
   }

   protected boolean z(IBlockData var1) {
      return this.d;
   }

   public int c(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return var1.a(var2, var3, var4);
   }

   public int b(IBlockData var1, class_ahw var2, BlockPosition var3, EnumDirection var4) {
      return !this.z(var1)?0:(var1.get(D) == var4?this.a(var2, var3, var1):0);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      if(this.b(var1, var2)) {
         this.g(var1, var2, var3);
      } else {
         this.b(var1, var2, var3, 0);
         var1.g(var2);
         EnumDirection[] var5 = EnumDirection.values();
         int var6 = var5.length;

         for(int var7 = 0; var7 < var6; ++var7) {
            EnumDirection var8 = var5[var7];
            var1.d(var2.a(var8), this);
         }

      }
   }

   protected void g(World var1, BlockPosition var2, IBlockData var3) {
      if(!this.b(var1, var2, (IBlockData)var3)) {
         boolean var4 = this.e(var1, var2, var3);
         if((this.d && !var4 || !this.d && var4) && !var1.a((BlockPosition)var2, (Block)this)) {
            byte var5 = -1;
            if(this.i(var1, var2, var3)) {
               var5 = -3;
            } else if(this.d) {
               var5 = -2;
            }

            var1.a(var2, this, this.i(var3), var5);
         }

      }
   }

   public boolean b(class_ahw var1, BlockPosition var2, IBlockData var3) {
      return false;
   }

   protected boolean e(World var1, BlockPosition var2, IBlockData var3) {
      return this.f(var1, var2, var3) > 0;
   }

   protected int f(World var1, BlockPosition var2, IBlockData var3) {
      EnumDirection var4 = (EnumDirection)var3.get(D);
      BlockPosition var5 = var2.a(var4);
      int var6 = var1.c(var5, var4);
      if(var6 >= 15) {
         return var6;
      } else {
         IBlockData var7 = var1.getType(var5);
         return Math.max(var6, var7.getBlock() == Blocks.af?((Integer)var7.get(BlockRedstoneWire.e)).intValue():0);
      }
   }

   protected int c(class_ahw var1, BlockPosition var2, IBlockData var3) {
      EnumDirection var4 = (EnumDirection)var3.get(D);
      EnumDirection var5 = var4.e();
      EnumDirection var6 = var4.f();
      return Math.max(this.b(var1, var2.a(var5), var5), this.b(var1, var2.a(var6), var6));
   }

   protected int b(class_ahw var1, BlockPosition var2, EnumDirection var3) {
      IBlockData var4 = var1.getType(var2);
      Block var5 = var4.getBlock();
      return this.A(var4)?(var5 == Blocks.cn?15:(var5 == Blocks.af?((Integer)var4.get(BlockRedstoneWire.e)).intValue():var1.a(var2, var3))):0;
   }

   public boolean g(IBlockData var1) {
      return true;
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return this.u().set(D, var8.bh().d());
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, class_rz var4, ItemStack var5) {
      if(this.e(var1, var2, var3)) {
         var1.a((BlockPosition)var2, (Block)this, 1);
      }

   }

   public void c(World var1, BlockPosition var2, IBlockData var3) {
      this.h(var1, var2, var3);
   }

   protected void h(World var1, BlockPosition var2, IBlockData var3) {
      EnumDirection var4 = (EnumDirection)var3.get(D);
      BlockPosition var5 = var2.a(var4.d());
      var1.e(var5, this);
      var1.a((BlockPosition)var5, (Block)this, (EnumDirection)var4);
   }

   public void d(World var1, BlockPosition var2, IBlockData var3) {
      if(this.d) {
         EnumDirection[] var4 = EnumDirection.values();
         int var5 = var4.length;

         for(int var6 = 0; var6 < var5; ++var6) {
            EnumDirection var7 = var4[var6];
            var1.d(var2.a(var7), this);
         }
      }

      super.d(var1, var2, var3);
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   protected boolean A(IBlockData var1) {
      return var1.m();
   }

   protected int a(class_ahw var1, BlockPosition var2, IBlockData var3) {
      return 15;
   }

   public static boolean B(IBlockData var0) {
      return Blocks.bb.C(var0) || Blocks.cj.C(var0);
   }

   public boolean C(IBlockData var1) {
      Block var2 = var1.getBlock();
      return var2 == this.x(this.u()).getBlock() || var2 == this.y(this.u()).getBlock();
   }

   public boolean i(World var1, BlockPosition var2, IBlockData var3) {
      EnumDirection var4 = ((EnumDirection)var3.get(D)).d();
      BlockPosition var5 = var2.a(var4);
      return B(var1.getType(var5))?var1.getType(var5).get(D) != var4:false;
   }

   protected int D(IBlockData var1) {
      return this.i(var1);
   }

   protected abstract int i(IBlockData var1);

   protected abstract IBlockData x(IBlockData var1);

   protected abstract IBlockData y(IBlockData var1);

   public boolean b(Block var1) {
      return this.C(var1.u());
   }
}
