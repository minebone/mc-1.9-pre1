package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityCreature;
import net.minecraft.server.Vec3D;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_vl;

public class class_ub extends class_tj {
   private EntityCreature b;
   protected double a;
   private double c;
   private double d;
   private double e;

   public class_ub(EntityCreature var1, double var2) {
      this.b = var1;
      this.a = var2;
      this.a(1);
   }

   public boolean a() {
      if(this.b.bF() == null && !this.b.aG()) {
         return false;
      } else {
         Vec3D var1 = class_vl.a(this.b, 5, 4);
         if(var1 == null) {
            return false;
         } else {
            this.c = var1.b;
            this.d = var1.c;
            this.e = var1.d;
            if(this.b.aG()) {
               BlockPosition var2 = this.a(this.b.world, this.b, 5, 4);
               if(var2 != null) {
                  this.c = (double)var2.p();
                  this.d = (double)var2.q();
                  this.e = (double)var2.r();
               }
            }

            return true;
         }
      }
   }

   public void c() {
      this.b.x().a(this.c, this.d, this.e, this.a);
   }

   public boolean b() {
      return !this.b.x().n();
   }

   private BlockPosition a(World var1, Entity var2, int var3, int var4) {
      BlockPosition var5 = new BlockPosition(var2);
      BlockPosition.class_a_in_class_cj var6 = new BlockPosition.class_a_in_class_cj();
      int var7 = var5.p();
      int var8 = var5.q();
      int var9 = var5.r();
      float var10 = (float)(var3 * var3 * var4 * 2);
      BlockPosition var11 = null;

      for(int var12 = var7 - var3; var12 <= var7 + var3; ++var12) {
         for(int var13 = var8 - var4; var13 <= var8 + var4; ++var13) {
            for(int var14 = var9 - var3; var14 <= var9 + var3; ++var14) {
               var6.c(var12, var13, var14);
               IBlockData var15 = var1.getType(var6);
               Block var16 = var15.getBlock();
               if(var16 == Blocks.j || var16 == Blocks.i) {
                  float var17 = (float)((var12 - var7) * (var12 - var7) + (var13 - var8) * (var13 - var8) + (var14 - var9) * (var14 - var9));
                  if(var17 < var10) {
                     var10 = var17;
                     var11 = new BlockPosition(var6);
                  }
               }
            }
         }
      }

      return var11;
   }
}
