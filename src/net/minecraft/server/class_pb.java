package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.SystemUtils;
import net.minecraft.server.class_ox;
import net.minecraft.server.class_oy;
import net.minecraft.server.class_oz;
import net.minecraft.server.class_pa;
import net.minecraft.server.class_pd;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_pb implements class_pa {
   private static final Logger a = LogManager.getLogger();
   private final Map b = Maps.newHashMap();
   private final Map c = Maps.newHashMap();
   private final int d;

   public class_pb(int var1) {
      this.d = var1;
   }

   public NBTTagCompound a(class_oy var1, NBTTagCompound var2) {
      int var3 = var2.b("DataVersion", 99)?var2.h("DataVersion"):-1;
      return var3 >= 165?var2:this.a(var1, var2, var3);
   }

   public NBTTagCompound a(class_oy var1, NBTTagCompound var2, int var3) {
      if(var3 < this.d) {
         var2 = this.b(var1, var2, var3);
         var2 = this.c(var1, var2, var3);
      }

      return var2;
   }

   private NBTTagCompound b(class_oy var1, NBTTagCompound var2, int var3) {
      List var4 = (List)this.c.get(var1);
      if(var4 != null) {
         for(int var5 = 0; var5 < var4.size(); ++var5) {
            class_ox var6 = (class_ox)var4.get(var5);
            if(var6.a() > var3) {
               var2 = var6.a(var2);
            }
         }
      }

      return var2;
   }

   private NBTTagCompound c(class_oy var1, NBTTagCompound var2, int var3) {
      List var4 = (List)this.b.get(var1);
      if(var4 != null) {
         for(int var5 = 0; var5 < var4.size(); ++var5) {
            var2 = ((class_pd)var4.get(var5)).a(this, var2, var3);
         }
      }

      return var2;
   }

   public void a(class_oz var1, class_pd var2) {
      this.a((class_oy)var1, (class_pd)var2);
   }

   public void a(class_oy var1, class_pd var2) {
      this.a(this.b, var1).add(var2);
   }

   public void a(class_oy var1, class_ox var2) {
      List var3 = this.a(this.c, var1);
      int var4 = var2.a();
      if(var4 > this.d) {
         a.warn("Ignored fix registered for version: {} as the DataVersion of the game is: {}", new Object[]{Integer.valueOf(var4), Integer.valueOf(this.d)});
      } else {
         if(!var3.isEmpty() && ((class_ox)SystemUtils.a(var3)).a() > var4) {
            for(int var5 = 0; var5 < var3.size(); ++var5) {
               if(((class_ox)var3.get(var5)).a() > var4) {
                  var3.add(var5, var2);
                  break;
               }
            }
         } else {
            var3.add(var2);
         }

      }
   }

   private List a(Map var1, class_oy var2) {
      Object var3 = (List)var1.get(var2);
      if(var3 == null) {
         var3 = Lists.newArrayList();
         var1.put(var2, var3);
      }

      return (List)var3;
   }
}
