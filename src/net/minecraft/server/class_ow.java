package net.minecraft.server;

import java.util.List;
import java.util.Random;
import net.minecraft.server.Item;
import net.minecraft.server.TileEntityDispenser;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ov;

public class class_ow extends class_ov.class_a_in_class_ov {
   private ItemStack b;
   private int c;
   private int d;

   public class_ow(Item var1, int var2, int var3, int var4, int var5) {
      super(var5);
      this.b = new ItemStack(var1, 1, var2);
      this.c = var3;
      this.d = var4;
   }

   public static void a(Random var0, List var1, TileEntityDispenser var2, int var3) {
      for(int var4 = 0; var4 < var3; ++var4) {
         class_ow var5 = (class_ow)class_ov.a(var0, var1);
         int var6 = var5.c + var0.nextInt(var5.d - var5.c + 1);
         if(var5.b.c() >= var6) {
            ItemStack var9 = var5.b.k();
            var9.b = var6;
            var2.a(var0.nextInt(var2.u_()), var9);
         } else {
            for(int var7 = 0; var7 < var6; ++var7) {
               ItemStack var8 = var5.b.k();
               var8.b = 1;
               var2.a(var0.nextInt(var2.u_()), var8);
            }
         }
      }

   }
}
