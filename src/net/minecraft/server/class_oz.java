package net.minecraft.server;

import net.minecraft.server.class_oy;

public enum class_oz implements class_oy {
   LEVEL,
   PLAYER,
   CHUNK,
   BLOCK_ENTITY,
   ENTITY,
   ITEM_INSTANCE;
}
