package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.StatisticList;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_amn;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.EnumHand;
import net.minecraft.server.EnumResult;
import net.minecraft.server.class_qo;

public class class_aci extends Item {
   private Block a;

   public class_aci(Block var1) {
      this.j = 1;
      this.a = var1;
      this.a(CreativeModeTab.f);
   }

   public class_qo a(ItemStack var1, World var2, EntityHuman var3, EnumHand var4) {
      boolean var5 = this.a == Blocks.AIR;
      MovingObjectPosition var6 = this.a(var2, var3, var5);
      if(var6 == null) {
         return new class_qo(EnumResult.PASS, var1);
      } else if(var6.a != MovingObjectPosition.EnumMovingObjectType.BLOCK) {
         return new class_qo(EnumResult.PASS, var1);
      } else {
         BlockPosition var7 = var6.a();
         if(!var2.a(var3, var7)) {
            return new class_qo(EnumResult.FAIL, var1);
         } else if(var5) {
            if(!var3.a(var7.a(var6.b), var6.b, var1)) {
               return new class_qo(EnumResult.FAIL, var1);
            } else {
               IBlockData var10 = var2.getType(var7);
               Material var11 = var10.getMaterial();
               if(var11 == Material.h && ((Integer)var10.get(class_amn.b)).intValue() == 0) {
                  var2.a((BlockPosition)var7, (IBlockData)Blocks.AIR.u(), 11);
                  var3.b(StatisticList.b((Item)this));
                  var3.a(class_ng.M, 1.0F, 1.0F);
                  return new class_qo(EnumResult.SUCCESS, this.a(var1, var3, Items.az));
               } else if(var11 == Material.i && ((Integer)var10.get(class_amn.b)).intValue() == 0) {
                  var3.a(class_ng.N, 1.0F, 1.0F);
                  var2.a((BlockPosition)var7, (IBlockData)Blocks.AIR.u(), 11);
                  var3.b(StatisticList.b((Item)this));
                  return new class_qo(EnumResult.SUCCESS, this.a(var1, var3, Items.aA));
               } else {
                  return new class_qo(EnumResult.FAIL, var1);
               }
            }
         } else {
            boolean var8 = var2.getType(var7).getBlock().a((class_ahw)var2, (BlockPosition)var7);
            BlockPosition var9 = var8 && var6.b == EnumDirection.UP?var7:var7.a(var6.b);
            if(!var3.a(var9, var6.b, var1)) {
               return new class_qo(EnumResult.FAIL, var1);
            } else if(this.a(var3, var2, var9)) {
               var3.b(StatisticList.b((Item)this));
               return !var3.abilities.d?new class_qo(EnumResult.SUCCESS, new ItemStack(Items.ay)):new class_qo(EnumResult.SUCCESS, var1);
            } else {
               return new class_qo(EnumResult.FAIL, var1);
            }
         }
      }
   }

   private ItemStack a(ItemStack var1, EntityHuman var2, Item var3) {
      if(var2.abilities.d) {
         return var1;
      } else if(--var1.b <= 0) {
         return new ItemStack(var3);
      } else {
         if(!var2.br.c(new ItemStack(var3))) {
            var2.a(new ItemStack(var3), false);
         }

         return var1;
      }
   }

   public boolean a(EntityHuman var1, World var2, BlockPosition var3) {
      if(this.a == Blocks.AIR) {
         return false;
      } else {
         IBlockData var4 = var2.getType(var3);
         Material var5 = var4.getMaterial();
         boolean var6 = !var5.a();
         boolean var7 = var4.getBlock().a((class_ahw)var2, (BlockPosition)var3);
         if(!var2.d(var3) && !var6 && !var7) {
            return false;
         } else {
            if(var2.s.l() && this.a == Blocks.i) {
               int var12 = var3.p();
               int var9 = var3.q();
               int var10 = var3.r();
               var2.a(var1, var3, class_ng.bt, EnumSoundCategory.BLOCKS, 0.5F, 2.6F + (var2.r.nextFloat() - var2.r.nextFloat()) * 0.8F);

               for(int var11 = 0; var11 < 8; ++var11) {
                  var2.a(EnumParticle.SMOKE_LARGE, (double)var12 + Math.random(), (double)var9 + Math.random(), (double)var10 + Math.random(), 0.0D, 0.0D, 0.0D, new int[0]);
               }
            } else {
               if(!var2.E && (var6 || var7) && !var5.d()) {
                  var2.b(var3, true);
               }

               class_nf var8 = this.a == Blocks.k?class_ng.L:class_ng.K;
               var2.a(var1, var3, var8, EnumSoundCategory.BLOCKS, 1.0F, 1.0F);
               var2.a((BlockPosition)var3, (IBlockData)this.a.u(), 11);
            }

            return true;
         }
      }
   }
}
