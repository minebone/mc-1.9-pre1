package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Item;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aly;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_arm;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_or;

public abstract class BlockDoubleStepAbstract extends class_aly {
   public static final class_arm d = class_arm.a("seamless");
   public static final BlockStateEnum e = BlockStateEnum.a("variant", BlockDoubleStepAbstract.EnumStoneSlabVariant.class);

   public BlockDoubleStepAbstract() {
      super(Material.e);
      IBlockData var1 = this.A.b();
      if(this.e()) {
         var1 = var1.set(d, Boolean.valueOf(false));
      } else {
         var1 = var1.set(a, class_aly.class_a_in_class_aly.BOTTOM);
      }

      this.w(var1.set(e, BlockDoubleStepAbstract.EnumStoneSlabVariant.STONE));
      this.a(CreativeModeTab.b);
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Item.a((Block)Blocks.U);
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Blocks.U, 1, ((BlockDoubleStepAbstract.EnumStoneSlabVariant)var3.get(e)).a());
   }

   public String e(int var1) {
      return super.a() + "." + BlockDoubleStepAbstract.EnumStoneSlabVariant.a(var1).d();
   }

   public IBlockState g() {
      return e;
   }

   public Comparable a(ItemStack var1) {
      return BlockDoubleStepAbstract.EnumStoneSlabVariant.a(var1.i() & 7);
   }

   public IBlockData a(int var1) {
      IBlockData var2 = this.u().set(e, BlockDoubleStepAbstract.EnumStoneSlabVariant.a(var1 & 7));
      if(this.e()) {
         var2 = var2.set(d, Boolean.valueOf((var1 & 8) != 0));
      } else {
         var2 = var2.set(a, (var1 & 8) == 0?class_aly.class_a_in_class_aly.BOTTOM:class_aly.class_a_in_class_aly.TOP);
      }

      return var2;
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((BlockDoubleStepAbstract.EnumStoneSlabVariant)var1.get(e)).a();
      if(this.e()) {
         if(((Boolean)var1.get(d)).booleanValue()) {
            var3 |= 8;
         }
      } else if(var1.get(a) == class_aly.class_a_in_class_aly.TOP) {
         var3 |= 8;
      }

      return var3;
   }

   protected BlockStateList b() {
      return this.e()?new BlockStateList(this, new IBlockState[]{d, e}):new BlockStateList(this, new IBlockState[]{a, e});
   }

   public int d(IBlockData var1) {
      return ((BlockDoubleStepAbstract.EnumStoneSlabVariant)var1.get(e)).a();
   }

   public MaterialMapColor r(IBlockData var1) {
      return ((BlockDoubleStepAbstract.EnumStoneSlabVariant)var1.get(e)).c();
   }

   public static enum EnumStoneSlabVariant implements class_or {
      STONE(0, MaterialMapColor.m, "stone"),
      SAND(1, MaterialMapColor.d, "sandstone", "sand"),
      WOOD(2, MaterialMapColor.o, "wood_old", "wood"),
      COBBLESTONE(3, MaterialMapColor.m, "cobblestone", "cobble"),
      BRICK(4, MaterialMapColor.D, "brick"),
      SMOOTHBRICK(5, MaterialMapColor.m, "stone_brick", "smoothStoneBrick"),
      NETHERBRICK(6, MaterialMapColor.K, "nether_brick", "netherBrick"),
      QUARTZ(7, MaterialMapColor.p, "quartz");

      private static final BlockDoubleStepAbstract.EnumStoneSlabVariant[] i = new BlockDoubleStepAbstract.EnumStoneSlabVariant[values().length];
      private final int j;
      private final MaterialMapColor k;
      private final String l;
      private final String m;

      private EnumStoneSlabVariant(int var3, MaterialMapColor var4, String var5) {
         this(var3, var4, var5, var5);
      }

      private EnumStoneSlabVariant(int var3, MaterialMapColor var4, String var5, String var6) {
         this.j = var3;
         this.k = var4;
         this.l = var5;
         this.m = var6;
      }

      public int a() {
         return this.j;
      }

      public MaterialMapColor c() {
         return this.k;
      }

      public String toString() {
         return this.l;
      }

      public static BlockDoubleStepAbstract.EnumStoneSlabVariant a(int var0) {
         if(var0 < 0 || var0 >= i.length) {
            var0 = 0;
         }

         return i[var0];
      }

      public String m() {
         return this.l;
      }

      public String d() {
         return this.m;
      }

      static {
         BlockDoubleStepAbstract.EnumStoneSlabVariant[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            BlockDoubleStepAbstract.EnumStoneSlabVariant var3 = var0[var2];
            i[var3.a()] = var3;
         }

      }
   }
}
