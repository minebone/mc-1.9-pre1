package net.minecraft.server;

import java.util.Iterator;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockDirt;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Item;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_akm;
import net.minecraft.server.class_aov;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_rz;

public class BlockSoil extends Block {
   public static final BlockStateInteger a = BlockStateInteger.a("moisture", 0, 7);
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.9375D, 1.0D);

   protected BlockSoil() {
      super(Material.c);
      this.w(this.A.b().set(a, Integer.valueOf(0)));
      this.a(true);
      this.d(255);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return b;
   }

   public AxisAlignedBB a(IBlockData var1, World var2, BlockPosition var3) {
      return j;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      int var5 = ((Integer)var3.get(a)).intValue();
      if(!this.c(var1, var2) && !var1.B(var2.a())) {
         if(var5 > 0) {
            var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Integer.valueOf(var5 - 1)), 2);
         } else if(!this.b(var1, var2)) {
            var1.a(var2, Blocks.d.u());
         }
      } else if(var5 < 7) {
         var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Integer.valueOf(7)), 2);
      }

   }

   public void a(World var1, BlockPosition var2, Entity var3, float var4) {
      if(!var1.E && var1.r.nextFloat() < var4 - 0.5F && var3 instanceof class_rz && (var3 instanceof EntityHuman || var1.U().b("mobGriefing")) && var3.width * var3.width * var3.length > 0.512F) {
         var1.a(var2, Blocks.d.u());
      }

      super.a(var1, var2, var3, var4);
   }

   private boolean b(World var1, BlockPosition var2) {
      Block var3 = var1.getType(var2.a()).getBlock();
      return var3 instanceof class_akm || var3 instanceof class_aov;
   }

   private boolean c(World var1, BlockPosition var2) {
      Iterator var3 = BlockPosition.b(var2.a(-4, 0, -4), var2.a(4, 1, 4)).iterator();

      BlockPosition.class_a_in_class_cj var4;
      do {
         if(!var3.hasNext()) {
            return false;
         }

         var4 = (BlockPosition.class_a_in_class_cj)var3.next();
      } while(var1.getType(var4).getMaterial() != Material.h);

      return true;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Block var4) {
      super.a(var1, var2, var3, var4);
      if(var1.getType(var2.a()).getMaterial().a()) {
         var1.a(var2, Blocks.d.u());
      }

   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Blocks.d.a(Blocks.d.u().set(BlockDirt.a, BlockDirt.EnumDirtVariant.DIRT), var2, var3);
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Blocks.d);
   }

   public IBlockData a(int var1) {
      return this.u().set(a, Integer.valueOf(var1 & 7));
   }

   public int e(IBlockData var1) {
      return ((Integer)var1.get(a)).intValue();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }
}
