package net.minecraft.server;

import net.minecraft.server.BiomeBase;
import net.minecraft.server.class_yl;
import net.minecraft.server.class_yo;
import net.minecraft.server.class_yq;

public class class_aip extends BiomeBase {
   public class_aip(BiomeBase.class_a_in_class_aif var1) {
      super(var1);
      this.u.clear();
      this.v.clear();
      this.w.clear();
      this.x.clear();
      this.u.add(new BiomeBase.BiomeMeta(class_yl.class, 50, 4, 4));
      this.u.add(new BiomeBase.BiomeMeta(class_yq.class, 100, 4, 4));
      this.u.add(new BiomeBase.BiomeMeta(class_yo.class, 1, 4, 4));
   }
}
