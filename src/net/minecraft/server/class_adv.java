package net.minecraft.server;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multisets;
import net.minecraft.server.BlockDirt;
import net.minecraft.server.BlockStone;
import net.minecraft.server.Blocks;
import net.minecraft.server.Chunk;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Items;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.World;
import net.minecraft.server.class_aco;
import net.minecraft.server.ItemStack;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ayw;
import net.minecraft.server.class_ayy;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.MathHelper;

public class class_adv extends class_aco {
   protected class_adv() {
      this.a(true);
   }

   public class_ayy a(ItemStack var1, World var2) {
      String var3 = "map_" + var1.i();
      class_ayy var4 = (class_ayy)var2.a(class_ayy.class, var3);
      if(var4 == null && !var2.E) {
         var1.b(var2.b("map"));
         var3 = "map_" + var1.i();
         var4 = new class_ayy(var3);
         var4.f = 3;
         var4.a((double)var2.T().b(), (double)var2.T().d(), var4.f);
         var4.d = (byte)var2.s.p().a();
         var4.c();
         var2.a((String)var3, (class_ayw)var4);
      }

      return var4;
   }

   public void a(World var1, Entity var2, class_ayy var3) {
      if(var1.s.p().a() == var3.d && var2 instanceof EntityHuman) {
         int var4 = 1 << var3.f;
         int var5 = var3.b;
         int var6 = var3.c;
         int var7 = MathHelper.c(var2.locX - (double)var5) / var4 + 64;
         int var8 = MathHelper.c(var2.locZ - (double)var6) / var4 + 64;
         int var9 = 128 / var4;
         if(var1.s.isNotOverworld()) {
            var9 /= 2;
         }

         class_ayy.class_a_in_class_ayy var10 = var3.a((EntityHuman)var2);
         ++var10.b;
         boolean var11 = false;

         for(int var12 = var7 - var9 + 1; var12 < var7 + var9; ++var12) {
            if((var12 & 15) == (var10.b & 15) || var11) {
               var11 = false;
               double var13 = 0.0D;

               for(int var15 = var8 - var9 - 1; var15 < var8 + var9; ++var15) {
                  if(var12 >= 0 && var15 >= -1 && var12 < 128 && var15 < 128) {
                     int var16 = var12 - var7;
                     int var17 = var15 - var8;
                     boolean var18 = var16 * var16 + var17 * var17 > (var9 - 2) * (var9 - 2);
                     int var19 = (var5 / var4 + var12 - 64) * var4;
                     int var20 = (var6 / var4 + var15 - 64) * var4;
                     HashMultiset var21 = HashMultiset.create();
                     Chunk var22 = var1.f(new BlockPosition(var19, 0, var20));
                     if(!var22.f()) {
                        int var23 = var19 & 15;
                        int var24 = var20 & 15;
                        int var25 = 0;
                        double var26 = 0.0D;
                        if(var1.s.isNotOverworld()) {
                           int var28 = var19 + var20 * 231871;
                           var28 = var28 * var28 * 31287121 + var28 * 11;
                           if((var28 >> 20 & 1) == 0) {
                              var21.add(Blocks.d.u().set(BlockDirt.a, BlockDirt.EnumDirtVariant.DIRT).g(), 10);
                           } else {
                              var21.add(Blocks.b.u().set(BlockStone.a, BlockStone.EnumStoneVariant.STONE).g(), 100);
                           }

                           var26 = 100.0D;
                        } else {
                           BlockPosition.class_a_in_class_cj var35 = new BlockPosition.class_a_in_class_cj();

                           for(int var29 = 0; var29 < var4; ++var29) {
                              for(int var30 = 0; var30 < var4; ++var30) {
                                 int var31 = var22.b(var29 + var23, var30 + var24) + 1;
                                 IBlockData var32 = Blocks.AIR.u();
                                 if(var31 > 1) {
                                    do {
                                       --var31;
                                       var32 = var22.a((BlockPosition)var35.c(var29 + var23, var31, var30 + var24));
                                    } while(var32.g() == MaterialMapColor.b && var31 > 0);

                                    if(var31 > 0 && var32.getMaterial().d()) {
                                       int var33 = var31 - 1;

                                       IBlockData var34;
                                       do {
                                          var34 = var22.a(var29 + var23, var33--, var30 + var24);
                                          ++var25;
                                       } while(var33 > 0 && var34.getMaterial().d());
                                    }
                                 }

                                 var26 += (double)var31 / (double)(var4 * var4);
                                 var21.add(var32.g());
                              }
                           }
                        }

                        var25 /= var4 * var4;
                        double var36 = (var26 - var13) * 4.0D / (double)(var4 + 4) + ((double)(var12 + var15 & 1) - 0.5D) * 0.4D;
                        byte var37 = 1;
                        if(var36 > 0.6D) {
                           var37 = 2;
                        }

                        if(var36 < -0.6D) {
                           var37 = 0;
                        }

                        MaterialMapColor var38 = (MaterialMapColor)Iterables.getFirst(Multisets.copyHighestCountFirst(var21), MaterialMapColor.b);
                        if(var38 == MaterialMapColor.n) {
                           var36 = (double)var25 * 0.1D + (double)(var12 + var15 & 1) * 0.2D;
                           var37 = 1;
                           if(var36 < 0.5D) {
                              var37 = 2;
                           }

                           if(var36 > 0.9D) {
                              var37 = 0;
                           }
                        }

                        var13 = var26;
                        if(var15 >= 0 && var16 * var16 + var17 * var17 < var9 * var9 && (!var18 || (var12 + var15 & 1) != 0)) {
                           byte var39 = var3.g[var12 + var15 * 128];
                           byte var40 = (byte)(var38.M * 4 + var37);
                           if(var39 != var40) {
                              var3.g[var12 + var15 * 128] = var40;
                              var3.a(var12, var15);
                              var11 = true;
                           }
                        }
                     }
                  }
               }
            }
         }

      }
   }

   public void a(ItemStack var1, World var2, Entity var3, int var4, boolean var5) {
      if(!var2.E) {
         class_ayy var6 = this.a(var1, var2);
         if(var3 instanceof EntityHuman) {
            EntityHuman var7 = (EntityHuman)var3;
            var6.a(var7, var1);
         }

         if(var5 || var3 instanceof EntityHuman && ((EntityHuman)var3).cb() == var1) {
            this.a(var2, var3, var6);
         }

      }
   }

   public Packet a(ItemStack var1, World var2, EntityHuman var3) {
      return this.a(var1, var2).a(var1, var2, var3);
   }

   public void b(ItemStack var1, World var2, EntityHuman var3) {
      NBTTagCompound var4 = var1.o();
      if(var4 != null) {
         if(var4.b("map_scale_direction", 99)) {
            a(var1, var2, var4.h("map_scale_direction"));
            var4.q("map_scale_direction");
         } else if(var4.p("map_tracking_position")) {
            b(var1, var2);
            var4.q("map_tracking_position");
         }
      }

   }

   protected static void a(ItemStack var0, World var1, int var2) {
      class_ayy var3 = Items.bk.a(var0, var1);
      var0.b(var1.b("map"));
      class_ayy var4 = new class_ayy("map_" + var0.i());
      var4.f = (byte)MathHelper.a(var3.f + var2, 0, 4);
      var4.e = var3.e;
      var4.a((double)var3.b, (double)var3.c, var4.f);
      var4.d = var3.d;
      var4.c();
      var1.a((String)("map_" + var0.i()), (class_ayw)var4);
   }

   protected static void b(ItemStack var0, World var1) {
      class_ayy var2 = Items.bk.a(var0, var1);
      var0.b(var1.b("map"));
      class_ayy var3 = new class_ayy("map_" + var0.i());
      var3.e = true;
      var3.b = var2.b;
      var3.c = var2.c;
      var3.f = var2.f;
      var3.d = var2.d;
      var3.c();
      var1.a((String)("map_" + var0.i()), (class_ayw)var3);
   }
}
