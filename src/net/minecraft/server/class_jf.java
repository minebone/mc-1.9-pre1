package net.minecraft.server;

import java.io.IOException;
import java.util.UUID;
import net.minecraft.server.Entity;
import net.minecraft.server.PacketDataSerializer;
import net.minecraft.server.WorldServer;
import net.minecraft.server.PacketListener;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayInListener;

public class class_jf implements Packet {
   private UUID a;

   public class_jf() {
   }

   public class_jf(UUID var1) {
      this.a = var1;
   }

   public void decode(PacketDataSerializer var1) throws IOException {
      this.a = var1.i();
   }

   public void encode(PacketDataSerializer var1) throws IOException {
      var1.a(this.a);
   }

   public void a(PacketPlayInListener var1) {
      var1.a(this);
   }

   public Entity a(WorldServer var1) {
      return var1.a(this.a);
   }

   // $FF: synthetic method
   // $FF: bridge method
   public void handle(PacketListener var1) {
      this.a((PacketPlayInListener)var1);
   }
}
