package net.minecraft.server;

import net.minecraft.server.DamageSource;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityProjectile;
import net.minecraft.server.EnumParticle;
import net.minecraft.server.MovingObjectPosition;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.class_aqp;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_yj;

public class class_zz extends EntityProjectile {
   private class_rz d;

   public class_zz(World var1) {
      super(var1);
   }

   public class_zz(World var1, class_rz var2) {
      super(var1, var2);
      this.d = var2;
   }

   protected void a(MovingObjectPosition var1) {
      class_rz var2 = this.k();
      if(var1.d != null) {
         if(var1.d == this.d) {
            return;
         }

         var1.d.a(DamageSource.a((Entity)this, (Entity)var2), 0.0F);
      }

      if(var1.a == MovingObjectPosition.EnumMovingObjectType.BLOCK) {
         BlockPosition var3 = var1.a();
         TileEntity var4 = this.world.r(var3);
         if(var4 instanceof class_aqp) {
            class_aqp var5 = (class_aqp)var4;
            if(var2 != null) {
               var5.a((Entity)var2);
               this.S();
               return;
            }

            var5.a((Entity)this);
            return;
         }
      }

      for(int var6 = 0; var6 < 32; ++var6) {
         this.world.a(EnumParticle.PORTAL, this.locX, this.locY + this.random.nextDouble() * 2.0D, this.locZ, this.random.nextGaussian(), 0.0D, this.random.nextGaussian(), new int[0]);
      }

      if(!this.world.E) {
         if(var2 instanceof EntityPlayer) {
            EntityPlayer var7 = (EntityPlayer)var2;
            if(var7.a.a().g() && var7.world == this.world && !var7.ck()) {
               if(this.random.nextFloat() < 0.05F && this.world.U().b("doMobSpawning")) {
                  class_yj var8 = new class_yj(this.world);
                  var8.a(true);
                  var8.b(var2.locX, var2.locY, var2.locZ, var2.yaw, var2.pitch);
                  this.world.a((Entity)var8);
               }

               if(var2.aH()) {
                  this.p();
               }

               var2.a(this.locX, this.locY, this.locZ);
               var2.fallDistance = 0.0F;
               var2.a(DamageSource.i, 5.0F);
            }
         } else if(var2 != null) {
            var2.a(this.locX, this.locY, this.locZ);
            var2.fallDistance = 0.0F;
         }

         this.S();
      }

   }

   public void m() {
      class_rz var1 = this.k();
      if(var1 != null && var1 instanceof EntityHuman && !var1.at()) {
         this.S();
      } else {
         super.m();
      }

   }
}
