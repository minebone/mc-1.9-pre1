package net.minecraft.server;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import java.util.List;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.BlockStatePredicate;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.ShapeDetector;
import net.minecraft.server.ShapeDetectorBuilder;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_amf;
import net.minecraft.server.class_amq;
import net.minecraft.server.class_aod;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_are;
import net.minecraft.server.class_arm;
import net.minecraft.server.class_arn;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_rz;

public class class_alc extends Block {
   public static final class_arn a = class_amf.D;
   public static final class_arm b = class_arm.a("eye");
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.8125D, 1.0D);
   protected static final AxisAlignedBB d = new AxisAlignedBB(0.3125D, 0.8125D, 0.3125D, 0.6875D, 1.0D, 0.6875D);
   private static ShapeDetector e;

   public class_alc() {
      super(Material.e, MaterialMapColor.C);
      this.w(this.A.b().set(a, EnumDirection.NORTH).set(b, Boolean.valueOf(false)));
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return c;
   }

   public void a(IBlockData var1, World var2, BlockPosition var3, AxisAlignedBB var4, List var5, Entity var6) {
      a(var3, var4, var5, c);
      if(((Boolean)var2.getType(var3).get(b)).booleanValue()) {
         a(var3, var4, var5, d);
      }

   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return null;
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return this.u().set(a, var8.bh().d()).set(b, Boolean.valueOf(false));
   }

   public boolean v(IBlockData var1) {
      return true;
   }

   public int d(IBlockData var1, World var2, BlockPosition var3) {
      return ((Boolean)var1.get(b)).booleanValue()?15:0;
   }

   public IBlockData a(int var1) {
      return this.u().set(b, Boolean.valueOf((var1 & 4) != 0)).set(a, EnumDirection.b(var1 & 3));
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((EnumDirection)var1.get(a)).b();
      if(((Boolean)var1.get(b)).booleanValue()) {
         var3 |= 4;
      }

      return var3;
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      return var1.set(a, var2.a((EnumDirection)var1.get(a)));
   }

   public IBlockData a(IBlockData var1, class_amq var2) {
      return var1.a(var2.a((EnumDirection)var1.get(a)));
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, b});
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public static ShapeDetector e() {
      if(e == null) {
         e = ShapeDetectorBuilder.a().a(new String[]{"?vvv?", ">   <", ">   <", ">   <", "?^^^?"}).a('?', class_are.a(BlockStatePredicate.a)).a('^', class_are.a((Predicate)BlockStatePredicate.a(Blocks.bG).a(b, Predicates.equalTo(Boolean.valueOf(true))).a(a, Predicates.equalTo(EnumDirection.SOUTH)))).a('>', class_are.a((Predicate)BlockStatePredicate.a(Blocks.bG).a(b, Predicates.equalTo(Boolean.valueOf(true))).a(a, Predicates.equalTo(EnumDirection.WEST)))).a('v', class_are.a((Predicate)BlockStatePredicate.a(Blocks.bG).a(b, Predicates.equalTo(Boolean.valueOf(true))).a(a, Predicates.equalTo(EnumDirection.NORTH)))).a('<', class_are.a((Predicate)BlockStatePredicate.a(Blocks.bG).a(b, Predicates.equalTo(Boolean.valueOf(true))).a(a, Predicates.equalTo(EnumDirection.EAST)))).a(' ', class_are.a(Blocks.AIR.u())).b();
      }

      return e;
   }
}
