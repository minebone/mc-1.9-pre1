package net.minecraft.server;

import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_akm;
import net.minecraft.server.IBlockData;
import net.minecraft.server.BlockPosition;

public class class_akb extends class_akm {
   private static final AxisAlignedBB[] a = new AxisAlignedBB[]{new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.125D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.1875D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.25D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.3125D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.375D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.4375D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.5D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.5625D, 1.0D)};

   protected Item h() {
      return Items.cb;
   }

   protected Item i() {
      return Items.cb;
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return a[((Integer)var1.get(this.e())).intValue()];
   }
}
