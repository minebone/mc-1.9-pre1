package net.minecraft.server;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import net.minecraft.server.ChatComponentText;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.GameRules;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.Packet;
import net.minecraft.server.PacketPlayOutEntityStatus;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.ICommandListener;

public class CommandGamerule extends CommandAbstract {
   public String c() {
      return "gamerule";
   }

   public int a() {
      return 2;
   }

   public String b(ICommandListener var1) {
      return "commands.gamerule.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      GameRules var4 = this.a(var1);
      String var5 = var3.length > 0?var3[0]:"";
      String var6 = var3.length > 1?a(var3, 1):"";
      switch(var3.length) {
      case 0:
         var2.a(new ChatComponentText(a(var4.b())));
         break;
      case 1:
         if(!var4.e(var5)) {
            throw new class_bz("commands.gamerule.norule", new Object[]{var5});
         }

         String var7 = var4.a(var5);
         var2.a((new ChatComponentText(var5)).a(" = ").a(var7));
         var2.a(CommandObjectiveExecutor.EnumCommandResult.QUERY_RESULT, var4.c(var5));
         break;
      default:
         if(var4.a(var5, GameRules.EnumGameRuleType.BOOLEAN_VALUE) && !"true".equals(var6) && !"false".equals(var6)) {
            throw new class_bz("commands.generic.boolean.invalid", new Object[]{var6});
         }

         var4.a(var5, var6);
         a(var4, var5, var1);
         a(var2, this, "commands.gamerule.success", new Object[]{var5, var6});
      }

   }

   public static void a(GameRules var0, String var1, MinecraftServer var2) {
      if("reducedDebugInfo".equals(var1)) {
         int var3 = var0.b(var1)?22:23;
         Iterator var4 = var2.getPlayerList().v().iterator();

         while(var4.hasNext()) {
            EntityPlayer var5 = (EntityPlayer)var4.next();
            var5.a.a((Packet)(new PacketPlayOutEntityStatus(var5, (byte)var3)));
         }
      }

   }

   public List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4) {
      if(var3.length == 1) {
         return a(var3, this.a(var1).b());
      } else {
         if(var3.length == 2) {
            GameRules var5 = this.a(var1);
            if(var5.a(var3[0], GameRules.EnumGameRuleType.BOOLEAN_VALUE)) {
               return a(var3, new String[]{"true", "false"});
            }
         }

         return Collections.emptyList();
      }
   }

   private GameRules a(MinecraftServer var1) {
      return var1.a(0).U();
   }
}
