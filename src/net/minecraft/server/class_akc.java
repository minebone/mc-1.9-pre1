package net.minecraft.server;

import java.util.List;
import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.Block;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.Item;
import net.minecraft.server.ItemArmor;
import net.minecraft.server.ItemBanner;
import net.minecraft.server.Items;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.StatisticList;
import net.minecraft.server.TileEntityBanner;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_aff;
import net.minecraft.server.class_afg;
import net.minecraft.server.class_ahw;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;

public class class_akc extends Block {
   public static final BlockStateInteger a = BlockStateInteger.a("level", 0, 3);
   protected static final AxisAlignedBB b = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.3125D, 1.0D);
   protected static final AxisAlignedBB c = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 0.125D);
   protected static final AxisAlignedBB d = new AxisAlignedBB(0.0D, 0.0D, 0.875D, 1.0D, 1.0D, 1.0D);
   protected static final AxisAlignedBB e = new AxisAlignedBB(0.875D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);
   protected static final AxisAlignedBB f = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 0.125D, 1.0D, 1.0D);

   public class_akc() {
      super(Material.f, MaterialMapColor.m);
      this.w(this.A.b().set(a, Integer.valueOf(0)));
   }

   public void a(IBlockData var1, World var2, BlockPosition var3, AxisAlignedBB var4, List var5, Entity var6) {
      a(var3, var4, var5, b);
      a(var3, var4, var5, f);
      a(var3, var4, var5, c);
      a(var3, var4, var5, e);
      a(var3, var4, var5, d);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return j;
   }

   public boolean b(IBlockData var1) {
      return false;
   }

   public boolean c(IBlockData var1) {
      return false;
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, Entity var4) {
      int var5 = ((Integer)var3.get(a)).intValue();
      float var6 = (float)var2.q() + (6.0F + (float)(3 * var5)) / 16.0F;
      if(!var1.E && var4.aG() && var5 > 0 && var4.bk().b <= (double)var6) {
         var4.W();
         this.a(var1, var2, var3, var5 - 1);
      }

   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, EntityHuman var4, EnumHand var5, ItemStack var6, EnumDirection var7, float var8, float var9, float var10) {
      if(var6 == null) {
         return true;
      } else {
         int var11 = ((Integer)var3.get(a)).intValue();
         Item var12 = var6.b();
         if(var12 == Items.az) {
            if(var11 < 3 && !var1.E) {
               if(!var4.abilities.d) {
                  var4.a((EnumHand)var5, (ItemStack)(new ItemStack(Items.ay)));
               }

               var4.b(StatisticList.K);
               this.a(var1, var2, var3, 3);
            }

            return true;
         } else if(var12 == Items.ay) {
            if(var11 == 3 && !var1.E) {
               if(!var4.abilities.d) {
                  --var6.b;
                  if(var6.b == 0) {
                     var4.a((EnumHand)var5, (ItemStack)(new ItemStack(Items.az)));
                  } else if(!var4.br.c(new ItemStack(Items.az))) {
                     var4.a(new ItemStack(Items.az), false);
                  }
               }

               var4.b(StatisticList.L);
               this.a(var1, var2, var3, 0);
            }

            return true;
         } else {
            ItemStack var14;
            if(var12 == Items.bJ) {
               if(var11 > 0 && !var1.E) {
                  if(!var4.abilities.d) {
                     var14 = class_aff.a(new ItemStack(Items.bG), class_afg.b);
                     var4.b(StatisticList.L);
                     if(--var6.b == 0) {
                        var4.a((EnumHand)var5, (ItemStack)var14);
                     } else if(!var4.br.c(var14)) {
                        var4.a(var14, false);
                     } else if(var4 instanceof EntityPlayer) {
                        ((EntityPlayer)var4).a(var4.bs);
                     }
                  }

                  this.a(var1, var2, var3, var11 - 1);
               }

               return true;
            } else {
               if(var11 > 0 && var12 instanceof ItemArmor) {
                  ItemArmor var13 = (ItemArmor)var12;
                  if(var13.d() == ItemArmor.EnumArmorMaterial.LEATHER && var13.e_(var6) && !var1.E) {
                     var13.c(var6);
                     this.a(var1, var2, var3, var11 - 1);
                     var4.b(StatisticList.M);
                     return true;
                  }
               }

               if(var11 > 0 && var12 instanceof ItemBanner) {
                  if(TileEntityBanner.c(var6) > 0 && !var1.E) {
                     var14 = var6.k();
                     var14.b = 1;
                     TileEntityBanner.e(var14);
                     var4.b(StatisticList.N);
                     if(!var4.abilities.d) {
                        --var6.b;
                     }

                     if(var6.b == 0) {
                        var4.a((EnumHand)var5, (ItemStack)var14);
                     } else if(!var4.br.c(var14)) {
                        var4.a(var14, false);
                     } else if(var4 instanceof EntityPlayer) {
                        ((EntityPlayer)var4).a(var4.bs);
                     }

                     if(!var4.abilities.d) {
                        this.a(var1, var2, var3, var11 - 1);
                     }
                  }

                  return true;
               } else {
                  return false;
               }
            }
         }
      }
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, int var4) {
      var1.a((BlockPosition)var2, (IBlockData)var3.set(a, Integer.valueOf(MathHelper.a(var4, 0, 3))), 2);
      var1.f(var2, this);
   }

   public void h(World var1, BlockPosition var2) {
      if(var1.r.nextInt(20) == 1) {
         float var3 = var1.b(var2).a(var2);
         if(var1.A().a(var3, var2.q()) >= 0.15F) {
            IBlockData var4 = var1.getType(var2);
            if(((Integer)var4.get(a)).intValue() < 3) {
               var1.a((BlockPosition)var2, (IBlockData)var4.a(a), 2);
            }

         }
      }
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return Items.bQ;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Items.bQ);
   }

   public boolean v(IBlockData var1) {
      return true;
   }

   public int d(IBlockData var1, World var2, BlockPosition var3) {
      return ((Integer)var1.get(a)).intValue();
   }

   public IBlockData a(int var1) {
      return this.u().set(a, Integer.valueOf(var1));
   }

   public int e(IBlockData var1) {
      return ((Integer)var1.get(a)).intValue();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }

   public boolean b(class_ahw var1, BlockPosition var2) {
      return true;
   }
}
