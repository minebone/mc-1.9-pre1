package net.minecraft.server;

import com.google.common.collect.Maps;
import java.util.Map;
import net.minecraft.server.EnumChatFormat;
import net.minecraft.server.class_bbr;
import net.minecraft.server.class_bbs;
import net.minecraft.server.class_bbt;
import net.minecraft.server.class_bbv;

public interface IScoreboardCriteria {
   Map a = Maps.newHashMap();
   IScoreboardCriteria b = new class_bbs("dummy");
   IScoreboardCriteria c = new class_bbs("trigger");
   IScoreboardCriteria d = new class_bbs("deathCount");
   IScoreboardCriteria e = new class_bbs("playerKillCount");
   IScoreboardCriteria f = new class_bbs("totalKillCount");
   IScoreboardCriteria g = new class_bbt("health");
   IScoreboardCriteria h = new class_bbv("food");
   IScoreboardCriteria i = new class_bbv("air");
   IScoreboardCriteria j = new class_bbv("armor");
   IScoreboardCriteria k = new class_bbv("xp");
   IScoreboardCriteria l = new class_bbv("level");
   IScoreboardCriteria[] m = new IScoreboardCriteria[]{new class_bbr("teamkill.", EnumChatFormat.BLACK), new class_bbr("teamkill.", EnumChatFormat.DARK_BLUE), new class_bbr("teamkill.", EnumChatFormat.DARK_GREEN), new class_bbr("teamkill.", EnumChatFormat.DARK_AQUA), new class_bbr("teamkill.", EnumChatFormat.DARK_RED), new class_bbr("teamkill.", EnumChatFormat.DARK_PURPLE), new class_bbr("teamkill.", EnumChatFormat.GOLD), new class_bbr("teamkill.", EnumChatFormat.GRAY), new class_bbr("teamkill.", EnumChatFormat.DARK_GRAY), new class_bbr("teamkill.", EnumChatFormat.BLUE), new class_bbr("teamkill.", EnumChatFormat.GREEN), new class_bbr("teamkill.", EnumChatFormat.AQUA), new class_bbr("teamkill.", EnumChatFormat.RED), new class_bbr("teamkill.", EnumChatFormat.LIGHT_PURPLE), new class_bbr("teamkill.", EnumChatFormat.YELLOW), new class_bbr("teamkill.", EnumChatFormat.WHITE)};
   IScoreboardCriteria[] n = new IScoreboardCriteria[]{new class_bbr("killedByTeam.", EnumChatFormat.BLACK), new class_bbr("killedByTeam.", EnumChatFormat.DARK_BLUE), new class_bbr("killedByTeam.", EnumChatFormat.DARK_GREEN), new class_bbr("killedByTeam.", EnumChatFormat.DARK_AQUA), new class_bbr("killedByTeam.", EnumChatFormat.DARK_RED), new class_bbr("killedByTeam.", EnumChatFormat.DARK_PURPLE), new class_bbr("killedByTeam.", EnumChatFormat.GOLD), new class_bbr("killedByTeam.", EnumChatFormat.GRAY), new class_bbr("killedByTeam.", EnumChatFormat.DARK_GRAY), new class_bbr("killedByTeam.", EnumChatFormat.BLUE), new class_bbr("killedByTeam.", EnumChatFormat.GREEN), new class_bbr("killedByTeam.", EnumChatFormat.AQUA), new class_bbr("killedByTeam.", EnumChatFormat.RED), new class_bbr("killedByTeam.", EnumChatFormat.LIGHT_PURPLE), new class_bbr("killedByTeam.", EnumChatFormat.YELLOW), new class_bbr("killedByTeam.", EnumChatFormat.WHITE)};

   String a();

   boolean b();

   IScoreboardCriteria.EnumScoreboardHealthDisplay c();

   public static enum EnumScoreboardHealthDisplay {
      INTEGER("integer"),
      HEARTS("hearts");

      private static final Map c = Maps.newHashMap();
      private final String d;

      private EnumScoreboardHealthDisplay(String var3) {
         this.d = var3;
      }

      public String a() {
         return this.d;
      }

      public static IScoreboardCriteria.EnumScoreboardHealthDisplay a(String var0) {
         IScoreboardCriteria.EnumScoreboardHealthDisplay var1 = (IScoreboardCriteria.EnumScoreboardHealthDisplay)c.get(var0);
         return var1 == null?INTEGER:var1;
      }

      static {
         IScoreboardCriteria.EnumScoreboardHealthDisplay[] var0 = values();
         int var1 = var0.length;

         for(int var2 = 0; var2 < var1; ++var2) {
            IScoreboardCriteria.EnumScoreboardHealthDisplay var3 = var0[var2];
            c.put(var3.a(), var3);
         }

      }
   }
}
