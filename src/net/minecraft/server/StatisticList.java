package net.minecraft.server;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.minecraft.server.AchievementList;
import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.CraftingManager;
import net.minecraft.server.EntityTypes;
import net.minecraft.server.Item;
import net.minecraft.server.Statistic;
import net.minecraft.server.class_acb;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_afp;
import net.minecraft.server.class_aft;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nm;
import net.minecraft.server.class_nn;

public class StatisticList {
   protected static final Map a = Maps.newHashMap();
   public static final List b = Lists.newArrayList();
   public static final List c = Lists.newArrayList();
   public static final List d = Lists.newArrayList();
   public static final List e = Lists.newArrayList();
   public static final Statistic f = (new class_nm("stat.leaveGame", new ChatMessage("stat.leaveGame", new Object[0]))).i().h();
   public static final Statistic g = (new class_nm("stat.playOneMinute", new ChatMessage("stat.playOneMinute", new Object[0]), Statistic.h)).i().h();
   public static final Statistic h = (new class_nm("stat.timeSinceDeath", new ChatMessage("stat.timeSinceDeath", new Object[0]), Statistic.h)).i().h();
   public static final Statistic i = (new class_nm("stat.sneakTime", new ChatMessage("stat.sneakTime", new Object[0]), Statistic.h)).i().h();
   public static final Statistic j = (new class_nm("stat.walkOneCm", new ChatMessage("stat.walkOneCm", new Object[0]), Statistic.i)).i().h();
   public static final Statistic k = (new class_nm("stat.crouchOneCm", new ChatMessage("stat.crouchOneCm", new Object[0]), Statistic.i)).i().h();
   public static final Statistic l = (new class_nm("stat.sprintOneCm", new ChatMessage("stat.sprintOneCm", new Object[0]), Statistic.i)).i().h();
   public static final Statistic m = (new class_nm("stat.swimOneCm", new ChatMessage("stat.swimOneCm", new Object[0]), Statistic.i)).i().h();
   public static final Statistic n = (new class_nm("stat.fallOneCm", new ChatMessage("stat.fallOneCm", new Object[0]), Statistic.i)).i().h();
   public static final Statistic o = (new class_nm("stat.climbOneCm", new ChatMessage("stat.climbOneCm", new Object[0]), Statistic.i)).i().h();
   public static final Statistic p = (new class_nm("stat.flyOneCm", new ChatMessage("stat.flyOneCm", new Object[0]), Statistic.i)).i().h();
   public static final Statistic q = (new class_nm("stat.diveOneCm", new ChatMessage("stat.diveOneCm", new Object[0]), Statistic.i)).i().h();
   public static final Statistic r = (new class_nm("stat.minecartOneCm", new ChatMessage("stat.minecartOneCm", new Object[0]), Statistic.i)).i().h();
   public static final Statistic s = (new class_nm("stat.boatOneCm", new ChatMessage("stat.boatOneCm", new Object[0]), Statistic.i)).i().h();
   public static final Statistic t = (new class_nm("stat.pigOneCm", new ChatMessage("stat.pigOneCm", new Object[0]), Statistic.i)).i().h();
   public static final Statistic u = (new class_nm("stat.horseOneCm", new ChatMessage("stat.horseOneCm", new Object[0]), Statistic.i)).i().h();
   public static final Statistic v = (new class_nm("stat.aviateOneCm", new ChatMessage("stat.aviateOneCm", new Object[0]), Statistic.i)).i().h();
   public static final Statistic w = (new class_nm("stat.jump", new ChatMessage("stat.jump", new Object[0]))).i().h();
   public static final Statistic x = (new class_nm("stat.drop", new ChatMessage("stat.drop", new Object[0]))).i().h();
   public static final Statistic y = (new class_nm("stat.damageDealt", new ChatMessage("stat.damageDealt", new Object[0]), Statistic.j)).h();
   public static final Statistic z = (new class_nm("stat.damageTaken", new ChatMessage("stat.damageTaken", new Object[0]), Statistic.j)).h();
   public static final Statistic A = (new class_nm("stat.deaths", new ChatMessage("stat.deaths", new Object[0]))).h();
   public static final Statistic B = (new class_nm("stat.mobKills", new ChatMessage("stat.mobKills", new Object[0]))).h();
   public static final Statistic C = (new class_nm("stat.animalsBred", new ChatMessage("stat.animalsBred", new Object[0]))).h();
   public static final Statistic D = (new class_nm("stat.playerKills", new ChatMessage("stat.playerKills", new Object[0]))).h();
   public static final Statistic E = (new class_nm("stat.fishCaught", new ChatMessage("stat.fishCaught", new Object[0]))).h();
   public static final Statistic F = (new class_nm("stat.junkFished", new ChatMessage("stat.junkFished", new Object[0]))).h();
   public static final Statistic G = (new class_nm("stat.treasureFished", new ChatMessage("stat.treasureFished", new Object[0]))).h();
   public static final Statistic H = (new class_nm("stat.talkedToVillager", new ChatMessage("stat.talkedToVillager", new Object[0]))).h();
   public static final Statistic I = (new class_nm("stat.tradedWithVillager", new ChatMessage("stat.tradedWithVillager", new Object[0]))).h();
   public static final Statistic J = (new class_nm("stat.cakeSlicesEaten", new ChatMessage("stat.cakeSlicesEaten", new Object[0]))).h();
   public static final Statistic K = (new class_nm("stat.cauldronFilled", new ChatMessage("stat.cauldronFilled", new Object[0]))).h();
   public static final Statistic L = (new class_nm("stat.cauldronUsed", new ChatMessage("stat.cauldronUsed", new Object[0]))).h();
   public static final Statistic M = (new class_nm("stat.armorCleaned", new ChatMessage("stat.armorCleaned", new Object[0]))).h();
   public static final Statistic N = (new class_nm("stat.bannerCleaned", new ChatMessage("stat.bannerCleaned", new Object[0]))).h();
   public static final Statistic O = (new class_nm("stat.brewingstandInteraction", new ChatMessage("stat.brewingstandInteraction", new Object[0]))).h();
   public static final Statistic P = (new class_nm("stat.beaconInteraction", new ChatMessage("stat.beaconInteraction", new Object[0]))).h();
   public static final Statistic Q = (new class_nm("stat.dropperInspected", new ChatMessage("stat.dropperInspected", new Object[0]))).h();
   public static final Statistic R = (new class_nm("stat.hopperInspected", new ChatMessage("stat.hopperInspected", new Object[0]))).h();
   public static final Statistic S = (new class_nm("stat.dispenserInspected", new ChatMessage("stat.dispenserInspected", new Object[0]))).h();
   public static final Statistic T = (new class_nm("stat.noteblockPlayed", new ChatMessage("stat.noteblockPlayed", new Object[0]))).h();
   public static final Statistic U = (new class_nm("stat.noteblockTuned", new ChatMessage("stat.noteblockTuned", new Object[0]))).h();
   public static final Statistic V = (new class_nm("stat.flowerPotted", new ChatMessage("stat.flowerPotted", new Object[0]))).h();
   public static final Statistic W = (new class_nm("stat.trappedChestTriggered", new ChatMessage("stat.trappedChestTriggered", new Object[0]))).h();
   public static final Statistic X = (new class_nm("stat.enderchestOpened", new ChatMessage("stat.enderchestOpened", new Object[0]))).h();
   public static final Statistic Y = (new class_nm("stat.itemEnchanted", new ChatMessage("stat.itemEnchanted", new Object[0]))).h();
   public static final Statistic Z = (new class_nm("stat.recordPlayed", new ChatMessage("stat.recordPlayed", new Object[0]))).h();
   public static final Statistic aa = (new class_nm("stat.furnaceInteraction", new ChatMessage("stat.furnaceInteraction", new Object[0]))).h();
   public static final Statistic ab = (new class_nm("stat.craftingTableInteraction", new ChatMessage("stat.workbenchInteraction", new Object[0]))).h();
   public static final Statistic ac = (new class_nm("stat.chestOpened", new ChatMessage("stat.chestOpened", new Object[0]))).h();
   public static final Statistic ad = (new class_nm("stat.sleepInBed", new ChatMessage("stat.sleepInBed", new Object[0]))).h();
   private static final Statistic[] ae = new Statistic[4096];
   private static final Statistic[] af = new Statistic[32000];
   private static final Statistic[] ag = new Statistic[32000];
   private static final Statistic[] ah = new Statistic[32000];
   private static final Statistic[] ai = new Statistic[32000];
   private static final Statistic[] aj = new Statistic[32000];

   public static Statistic a(Block var0) {
      return ae[Block.a(var0)];
   }

   public static Statistic a(Item var0) {
      return af[Item.a(var0)];
   }

   public static Statistic b(Item var0) {
      return ag[Item.a(var0)];
   }

   public static Statistic c(Item var0) {
      return ah[Item.a(var0)];
   }

   public static Statistic d(Item var0) {
      return ai[Item.a(var0)];
   }

   public static Statistic e(Item var0) {
      return aj[Item.a(var0)];
   }

   public static void a() {
      c();
      d();
      e();
      b();
      f();
      AchievementList.a();
      EntityTypes.a();
   }

   private static void b() {
      HashSet var0 = Sets.newHashSet();
      Iterator var1 = CraftingManager.a().b().iterator();

      while(var1.hasNext()) {
         class_aft var2 = (class_aft)var1.next();
         if(var2.b() != null) {
            var0.add(var2.b().b());
         }
      }

      var1 = class_afp.a().b().values().iterator();

      while(var1.hasNext()) {
         ItemStack var5 = (ItemStack)var1.next();
         var0.add(var5.b());
      }

      var1 = var0.iterator();

      while(var1.hasNext()) {
         Item var6 = (Item)var1.next();
         if(var6 != null) {
            int var3 = Item.a(var6);
            String var4 = f(var6);
            if(var4 != null) {
               af[var3] = (new class_nn("stat.craftItem.", var4, new ChatMessage("stat.craftItem", new Object[]{(new ItemStack(var6)).B()}), var6)).h();
            }
         }
      }

      a(af);
   }

   private static void c() {
      Iterator var0 = Block.h.iterator();

      while(var0.hasNext()) {
         Block var1 = (Block)var0.next();
         Item var2 = Item.a(var1);
         if(var2 != null) {
            int var3 = Block.a(var1);
            String var4 = f(var2);
            if(var4 != null && var1.p()) {
               ae[var3] = (new class_nn("stat.mineBlock.", var4, new ChatMessage("stat.mineBlock", new Object[]{(new ItemStack(var1)).B()}), var2)).h();
               e.add((class_nn)ae[var3]);
            }
         }
      }

      a(ae);
   }

   private static void d() {
      Iterator var0 = Item.f.iterator();

      while(var0.hasNext()) {
         Item var1 = (Item)var0.next();
         if(var1 != null) {
            int var2 = Item.a(var1);
            String var3 = f(var1);
            if(var3 != null) {
               ag[var2] = (new class_nn("stat.useItem.", var3, new ChatMessage("stat.useItem", new Object[]{(new ItemStack(var1)).B()}), var1)).h();
               if(!(var1 instanceof class_acb)) {
                  d.add((class_nn)ag[var2]);
               }
            }
         }
      }

      a(ag);
   }

   private static void e() {
      Iterator var0 = Item.f.iterator();

      while(var0.hasNext()) {
         Item var1 = (Item)var0.next();
         if(var1 != null) {
            int var2 = Item.a(var1);
            String var3 = f(var1);
            if(var3 != null && var1.m()) {
               ah[var2] = (new class_nn("stat.breakItem.", var3, new ChatMessage("stat.breakItem", new Object[]{(new ItemStack(var1)).B()}), var1)).h();
            }
         }
      }

      a(ah);
   }

   private static void f() {
      Iterator var0 = Item.f.iterator();

      while(var0.hasNext()) {
         Item var1 = (Item)var0.next();
         if(var1 != null) {
            int var2 = Item.a(var1);
            String var3 = f(var1);
            if(var3 != null) {
               ai[var2] = (new class_nn("stat.pickup.", var3, new ChatMessage("stat.pickup", new Object[]{(new ItemStack(var1)).B()}), var1)).h();
               aj[var2] = (new class_nn("stat.drop.", var3, new ChatMessage("stat.drop", new Object[]{(new ItemStack(var1)).B()}), var1)).h();
            }
         }
      }

      a(ah);
   }

   private static String f(Item var0) {
      class_kk var1 = (class_kk)Item.f.b(var0);
      return var1 != null?var1.toString().replace(':', '.'):null;
   }

   private static void a(Statistic[] var0) {
      a(var0, Blocks.j, Blocks.i);
      a(var0, Blocks.l, Blocks.k);
      a(var0, Blocks.aZ, Blocks.aU);
      a(var0, Blocks.am, Blocks.al);
      a(var0, Blocks.aD, Blocks.aC);
      a(var0, Blocks.bc, Blocks.bb);
      a(var0, Blocks.ck, Blocks.cj);
      a(var0, Blocks.aF, Blocks.aE);
      a(var0, Blocks.bK, Blocks.bJ);
      a(var0, Blocks.T, Blocks.U);
      a(var0, Blocks.bL, Blocks.bM);
      a(var0, Blocks.cO, Blocks.cP);
      a(var0, Blocks.c, Blocks.d);
      a(var0, Blocks.ak, Blocks.d);
   }

   private static void a(Statistic[] var0, Block var1, Block var2) {
      int var3 = Block.a(var1);
      int var4 = Block.a(var2);
      if(var0[var3] != null && var0[var4] == null) {
         var0[var4] = var0[var3];
      } else {
         b.remove(var0[var3]);
         e.remove(var0[var3]);
         c.remove(var0[var3]);
         var0[var3] = var0[var4];
      }
   }

   public static Statistic a(EntityTypes.class_a_in_class_rs var0) {
      return var0.a == null?null:(new Statistic("stat.killEntity." + var0.a, new ChatMessage("stat.entityKill", new Object[]{new ChatMessage("entity." + var0.a + ".name", new Object[0])}))).h();
   }

   public static Statistic b(EntityTypes.class_a_in_class_rs var0) {
      return var0.a == null?null:(new Statistic("stat.entityKilledBy." + var0.a, new ChatMessage("stat.entityKilledBy", new Object[]{new ChatMessage("entity." + var0.a + ".name", new Object[0])}))).h();
   }

   public static Statistic a(String var0) {
      return (Statistic)a.get(var0);
   }
}
