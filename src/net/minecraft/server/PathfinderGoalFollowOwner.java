package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.Blocks;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityTameableAnimal;
import net.minecraft.server.NavigationAbstract;
import net.minecraft.server.World;
import net.minecraft.server.IBlockData;
import net.minecraft.server.class_ayl;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_vd;

public class PathfinderGoalFollowOwner extends class_tj {
   private EntityTameableAnimal d;
   private class_rz e;
   World a;
   private double f;
   private NavigationAbstract g;
   private int h;
   float b;
   float c;
   private float i;

   public PathfinderGoalFollowOwner(EntityTameableAnimal var1, double var2, float var4, float var5) {
      this.d = var1;
      this.a = var1.world;
      this.f = var2;
      this.g = var1.x();
      this.c = var4;
      this.b = var5;
      this.a(3);
      if(!(var1.x() instanceof class_vd)) {
         throw new IllegalArgumentException("Unsupported mob type for FollowOwnerGoal");
      }
   }

   public boolean a() {
      class_rz var1 = this.d.dc();
      if(var1 == null) {
         return false;
      } else if(var1 instanceof EntityHuman && ((EntityHuman)var1).y()) {
         return false;
      } else if(this.d.db()) {
         return false;
      } else if(this.d.h(var1) < (double)(this.c * this.c)) {
         return false;
      } else {
         this.e = var1;
         return true;
      }
   }

   public boolean b() {
      return !this.g.n() && this.d.h(this.e) > (double)(this.b * this.b) && !this.d.db();
   }

   public void c() {
      this.h = 0;
      this.i = this.d.a((class_ayl)class_ayl.WATER);
      this.d.a(class_ayl.WATER, 0.0F);
   }

   public void d() {
      this.e = null;
      this.g.o();
      this.d.a(class_ayl.WATER, this.i);
   }

   private boolean a(BlockPosition var1) {
      IBlockData var2 = this.a.getType(var1);
      Block var3 = var2.getBlock();
      return var3 == Blocks.AIR?true:!var2.h();
   }

   public void e() {
      this.d.t().a(this.e, 10.0F, (float)this.d.cD());
      if(!this.d.db()) {
         if(--this.h <= 0) {
            this.h = 10;
            if(!this.g.a((Entity)this.e, this.f)) {
               if(!this.d.cP()) {
                  if(this.d.h(this.e) >= 144.0D) {
                     int var1 = MathHelper.c(this.e.locX) - 2;
                     int var2 = MathHelper.c(this.e.locZ) - 2;
                     int var3 = MathHelper.c(this.e.bk().b);

                     for(int var4 = 0; var4 <= 4; ++var4) {
                        for(int var5 = 0; var5 <= 4; ++var5) {
                           if((var4 < 1 || var5 < 1 || var4 > 3 || var5 > 3) && this.a.getType(new BlockPosition(var1 + var4, var3 - 1, var2 + var5)).q() && this.a(new BlockPosition(var1 + var4, var3, var2 + var5)) && this.a(new BlockPosition(var1 + var4, var3 + 1, var2 + var5))) {
                              this.d.b((double)((float)(var1 + var4) + 0.5F), (double)var3, (double)((float)(var2 + var5) + 0.5F), this.d.yaw, this.d.pitch);
                              this.g.o();
                              return;
                           }
                        }
                     }

                  }
               }
            }
         }
      }
   }
}
