package net.minecraft.server;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Random;
import net.minecraft.server.BiomeBase;
import net.minecraft.server.StructureGenerator;
import net.minecraft.server.StructurePiece;
import net.minecraft.server.StructureStart;
import net.minecraft.server.World;
import net.minecraft.server.WorldGenNetherPieces;
import net.minecraft.server.class_yf;
import net.minecraft.server.class_yo;
import net.minecraft.server.class_yq;
import net.minecraft.server.class_yv;

public class class_avu extends StructureGenerator {
   private final List a = Lists.newArrayList();

   public class_avu() {
      this.a.add(new BiomeBase.BiomeMeta(class_yf.class, 10, 2, 3));
      this.a.add(new BiomeBase.BiomeMeta(class_yq.class, 5, 4, 4));
      this.a.add(new BiomeBase.BiomeMeta(class_yv.class, 10, 4, 4));
      this.a.add(new BiomeBase.BiomeMeta(class_yo.class, 3, 4, 4));
   }

   public String a() {
      return "Fortress";
   }

   public List b() {
      return this.a;
   }

   protected boolean a(int var1, int var2) {
      int var3 = var1 >> 4;
      int var4 = var2 >> 4;
      this.f.setSeed((long)(var3 ^ var4 << 4) ^ this.g.O());
      this.f.nextInt();
      return this.f.nextInt(3) != 0?false:(var1 != (var3 << 4) + 4 + this.f.nextInt(8)?false:var2 == (var4 << 4) + 4 + this.f.nextInt(8));
   }

   protected StructureStart b(int var1, int var2) {
      return new class_avu.class_a_in_class_avu(this.g, this.f, var1, var2);
   }

   public static class class_a_in_class_avu extends StructureStart {
      public class_a_in_class_avu() {
      }

      public class_a_in_class_avu(World var1, Random var2, int var3, int var4) {
         super(var3, var4);
         WorldGenNetherPieces.class_q_in_class_avv var5 = new WorldGenNetherPieces.class_q_in_class_avv(var2, (var3 << 4) + 2, (var4 << 4) + 2);
         this.a.add(var5);
         var5.a(var5, this.a, var2);
         List var6 = var5.d;

         while(!var6.isEmpty()) {
            int var7 = var2.nextInt(var6.size());
            StructurePiece var8 = (StructurePiece)var6.remove(var7);
            var8.a((StructurePiece)var5, (List)this.a, (Random)var2);
         }

         this.d();
         this.a(var1, var2, 48, 70);
      }
   }
}
