package net.minecraft.server;

import net.minecraft.server.Block;
import net.minecraft.server.EnumColor;
import net.minecraft.server.class_acb;
import net.minecraft.server.ItemStack;

public class class_acu extends class_acb {
   public class_acu(Block var1) {
      super(var1);
      this.e(0);
      this.a(true);
   }

   public int a(int var1) {
      return var1;
   }

   public String f_(ItemStack var1) {
      return super.a() + "." + EnumColor.b(var1.i()).d();
   }
}
