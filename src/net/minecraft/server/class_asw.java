package net.minecraft.server;

import net.minecraft.server.WorldProviderNormal;
import net.minecraft.server.class_aik;
import net.minecraft.server.class_ain;
import net.minecraft.server.class_aru;
import net.minecraft.server.class_arx;
import net.minecraft.server.class_asu;
import net.minecraft.server.class_atl;

public class class_asw extends class_asu {
   public void b() {
      this.c = new class_ain(class_aik.j);
      this.d = true;
      this.isNotOverworld = true;
   }

   protected void a() {
      float var1 = 0.1F;

      for(int var2 = 0; var2 <= 15; ++var2) {
         float var3 = 1.0F - (float)var2 / 15.0F;
         this.f[var2] = (1.0F - var3) / (var3 * 3.0F + 1.0F) * (1.0F - var1) + var1;
      }

   }

   public class_arx c() {
      return new class_atl(this.b, this.b.T().r(), this.b.O());
   }

   public boolean d() {
      return false;
   }

   public boolean a(int var1, int var2) {
      return false;
   }

   public float a(long var1, float var3) {
      return 0.5F;
   }

   public boolean e() {
      return false;
   }

   public class_aru o() {
      return new class_aru() {
         public double f() {
            return super.f() / 8.0D;
         }

         public double g() {
            return super.g() / 8.0D;
         }
      };
   }

   public WorldProviderNormal p() {
      return WorldProviderNormal.NETHER;
   }
}
