package net.minecraft.server;

import java.util.List;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.ICommandListener;

public interface class_k extends Comparable {
   String c();

   String b(ICommandListener var1);

   List b();

   void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz;

   boolean a(MinecraftServer var1, ICommandListener var2);

   List a(MinecraftServer var1, ICommandListener var2, String[] var3, BlockPosition var4);

   boolean b(String[] var1, int var2);
}
