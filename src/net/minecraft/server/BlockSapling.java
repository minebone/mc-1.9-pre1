package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BlockLeaves;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.BlockWood;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.World;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_aju;
import net.minecraft.server.class_ajx;
import net.minecraft.server.class_ane;
import net.minecraft.server.class_anf;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.class_atp;
import net.minecraft.server.class_atq;
import net.minecraft.server.class_auc;
import net.minecraft.server.class_aun;
import net.minecraft.server.class_auo;
import net.minecraft.server.class_auw;
import net.minecraft.server.class_auz;
import net.minecraft.server.class_avc;
import net.minecraft.server.class_avf;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_di;

public class BlockSapling extends class_ajx implements class_aju {
   public static final BlockStateEnum a = BlockStateEnum.a("type", BlockWood.EnumLogVariant.class);
   public static final BlockStateInteger c = BlockStateInteger.a("stage", 0, 1);
   protected static final AxisAlignedBB d = new AxisAlignedBB(0.09999999403953552D, 0.0D, 0.09999999403953552D, 0.8999999761581421D, 0.800000011920929D, 0.8999999761581421D);

   protected BlockSapling() {
      this.w(this.A.b().set(a, BlockWood.EnumLogVariant.OAK).set(c, Integer.valueOf(0)));
      this.a(CreativeModeTab.c);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return d;
   }

   public String c() {
      return class_di.a(this.a() + "." + BlockWood.EnumLogVariant.OAK.d() + ".name");
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(!var1.E) {
         super.b(var1, var2, var3, var4);
         if(var1.k(var2.a()) >= 9 && var4.nextInt(7) == 0) {
            this.c(var1, var2, var3, var4);
         }

      }
   }

   public void c(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      if(((Integer)var3.get(c)).intValue() == 0) {
         var1.a((BlockPosition)var2, (IBlockData)var3.a(c), 4);
      } else {
         this.d(var1, var2, var3, var4);
      }

   }

   public void d(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      Object var5 = var4.nextInt(10) == 0?new class_atp(true):new class_avf(true);
      int var6 = 0;
      int var7 = 0;
      boolean var8 = false;
      IBlockData var9;
      switch(BlockSapling.SyntheticClass_1.a[((BlockWood.EnumLogVariant)var3.get(a)).ordinal()]) {
      case 1:
         label68:
         for(var6 = 0; var6 >= -1; --var6) {
            for(var7 = 0; var7 >= -1; --var7) {
               if(this.a(var1, var2, var6, var7, BlockWood.EnumLogVariant.SPRUCE)) {
                  var5 = new class_auo(false, var4.nextBoolean());
                  var8 = true;
                  break label68;
               }
            }
         }

         if(!var8) {
            var7 = 0;
            var6 = 0;
            var5 = new class_avc(true);
         }
         break;
      case 2:
         var5 = new class_atq(true, false);
         break;
      case 3:
         var9 = Blocks.r.u().set(class_anf.b, BlockWood.EnumLogVariant.JUNGLE);
         IBlockData var10 = Blocks.t.u().set(class_ane.e, BlockWood.EnumLogVariant.JUNGLE).set(BlockLeaves.b, Boolean.valueOf(false));

         label82:
         for(var6 = 0; var6 >= -1; --var6) {
            for(var7 = 0; var7 >= -1; --var7) {
               if(this.a(var1, var2, var6, var7, BlockWood.EnumLogVariant.JUNGLE)) {
                  var5 = new class_aun(true, 10, 20, var9, var10);
                  var8 = true;
                  break label82;
               }
            }
         }

         if(!var8) {
            var7 = 0;
            var6 = 0;
            var5 = new class_avf(true, 4 + var4.nextInt(7), var9, var10, false);
         }
         break;
      case 4:
         var5 = new class_auz(true);
         break;
      case 5:
         label96:
         for(var6 = 0; var6 >= -1; --var6) {
            for(var7 = 0; var7 >= -1; --var7) {
               if(this.a(var1, var2, var6, var7, BlockWood.EnumLogVariant.DARK_OAK)) {
                  var5 = new class_auw(true);
                  var8 = true;
                  break label96;
               }
            }
         }

         if(!var8) {
            return;
         }
      case 6:
      }

      var9 = Blocks.AIR.u();
      if(var8) {
         var1.a((BlockPosition)var2.a(var6, 0, var7), (IBlockData)var9, 4);
         var1.a((BlockPosition)var2.a(var6 + 1, 0, var7), (IBlockData)var9, 4);
         var1.a((BlockPosition)var2.a(var6, 0, var7 + 1), (IBlockData)var9, 4);
         var1.a((BlockPosition)var2.a(var6 + 1, 0, var7 + 1), (IBlockData)var9, 4);
      } else {
         var1.a((BlockPosition)var2, (IBlockData)var9, 4);
      }

      if(!((class_auc)var5).b(var1, var4, var2.a(var6, 0, var7))) {
         if(var8) {
            var1.a((BlockPosition)var2.a(var6, 0, var7), (IBlockData)var3, 4);
            var1.a((BlockPosition)var2.a(var6 + 1, 0, var7), (IBlockData)var3, 4);
            var1.a((BlockPosition)var2.a(var6, 0, var7 + 1), (IBlockData)var3, 4);
            var1.a((BlockPosition)var2.a(var6 + 1, 0, var7 + 1), (IBlockData)var3, 4);
         } else {
            var1.a((BlockPosition)var2, (IBlockData)var3, 4);
         }
      }

   }

   private boolean a(World var1, BlockPosition var2, int var3, int var4, BlockWood.EnumLogVariant var5) {
      return this.a(var1, var2.a(var3, 0, var4), var5) && this.a(var1, var2.a(var3 + 1, 0, var4), var5) && this.a(var1, var2.a(var3, 0, var4 + 1), var5) && this.a(var1, var2.a(var3 + 1, 0, var4 + 1), var5);
   }

   public boolean a(World var1, BlockPosition var2, BlockWood.EnumLogVariant var3) {
      IBlockData var4 = var1.getType(var2);
      return var4.getBlock() == this && var4.get(a) == var3;
   }

   public int d(IBlockData var1) {
      return ((BlockWood.EnumLogVariant)var1.get(a)).a();
   }

   public boolean a(World var1, BlockPosition var2, IBlockData var3, boolean var4) {
      return true;
   }

   public boolean a(World var1, Random var2, BlockPosition var3, IBlockData var4) {
      return (double)var1.r.nextFloat() < 0.45D;
   }

   public void b(World var1, Random var2, BlockPosition var3, IBlockData var4) {
      this.c(var1, var3, var4, var2);
   }

   public IBlockData a(int var1) {
      return this.u().set(a, BlockWood.EnumLogVariant.a(var1 & 7)).set(c, Integer.valueOf((var1 & 8) >> 3));
   }

   public int e(IBlockData var1) {
      byte var2 = 0;
      int var3 = var2 | ((BlockWood.EnumLogVariant)var1.get(a)).a();
      var3 |= ((Integer)var1.get(c)).intValue() << 3;
      return var3;
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a, c});
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a = new int[BlockWood.EnumLogVariant.values().length];

      static {
         try {
            a[BlockWood.EnumLogVariant.SPRUCE.ordinal()] = 1;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            a[BlockWood.EnumLogVariant.BIRCH.ordinal()] = 2;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            a[BlockWood.EnumLogVariant.JUNGLE.ordinal()] = 3;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            a[BlockWood.EnumLogVariant.ACACIA.ordinal()] = 4;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            a[BlockWood.EnumLogVariant.DARK_OAK.ordinal()] = 5;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[BlockWood.EnumLogVariant.OAK.ordinal()] = 6;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
