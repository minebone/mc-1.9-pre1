package net.minecraft.server;

import net.minecraft.server.Blocks;
import net.minecraft.server.DamageSource;
import net.minecraft.server.DataWatcher;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.EntityInsentient;
import net.minecraft.server.Items;
import net.minecraft.server.PathfinderGoalArrowAttack;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.Material;
import net.minecraft.server.class_azs;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_ke;
import net.minecraft.server.class_kg;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_nf;
import net.minecraft.server.class_ng;
import net.minecraft.server.MathHelper;
import net.minecraft.server.EnumHand;
import net.minecraft.server.class_rz;
import net.minecraft.server.class_to;
import net.minecraft.server.class_ue;
import net.minecraft.server.class_uf;
import net.minecraft.server.class_ux;
import net.minecraft.server.class_vy;
import net.minecraft.server.class_yk;
import net.minecraft.server.class_yr;
import net.minecraft.server.class_ys;
import net.minecraft.server.class_zv;

public class class_we extends class_vy implements class_yr {
   private static final class_ke a = DataWatcher.a(class_we.class, class_kg.a);

   public class_we(World var1) {
      super(var1);
      this.a(0.7F, 1.9F);
   }

   protected void r() {
      this.bp.a(1, new PathfinderGoalArrowAttack(this, 1.25D, 20, 10.0F));
      this.bp.a(2, new class_uf(this, 1.0D));
      this.bp.a(3, new class_to(this, EntityHuman.class, 6.0F));
      this.bp.a(4, new class_ue(this));
      this.bq.a(1, new class_ux(this, EntityInsentient.class, 10, true, false, class_yk.d));
   }

   protected void bz() {
      super.bz();
      this.a(class_ys.a).a(4.0D);
      this.a(class_ys.d).a(0.20000000298023224D);
   }

   protected void i() {
      super.i();
      this.datawatcher.a((class_ke)a, (Object)Byte.valueOf((byte)0));
   }

   public void n() {
      super.n();
      if(!this.world.E) {
         int var1 = MathHelper.c(this.locX);
         int var2 = MathHelper.c(this.locY);
         int var3 = MathHelper.c(this.locZ);
         if(this.ag()) {
            this.a(DamageSource.f, 1.0F);
         }

         if(this.world.b(new BlockPosition(var1, 0, var3)).a(new BlockPosition(var1, var2, var3)) > 1.0F) {
            this.a(DamageSource.c, 1.0F);
         }

         if(!this.world.U().b("mobGriefing")) {
            return;
         }

         for(int var4 = 0; var4 < 4; ++var4) {
            var1 = MathHelper.c(this.locX + (double)((float)(var4 % 2 * 2 - 1) * 0.25F));
            var2 = MathHelper.c(this.locY);
            var3 = MathHelper.c(this.locZ + (double)((float)(var4 / 2 % 2 * 2 - 1) * 0.25F));
            BlockPosition var5 = new BlockPosition(var1, var2, var3);
            if(this.world.getType(var5).getMaterial() == Material.a && this.world.b(new BlockPosition(var1, 0, var3)).a(var5) < 0.8F && Blocks.aH.a(this.world, var5)) {
               this.world.a(var5, Blocks.aH.u());
            }
         }
      }

   }

   protected class_kk J() {
      return class_azs.z;
   }

   public void a(class_rz var1, float var2) {
      class_zv var3 = new class_zv(this.world, this);
      double var4 = var1.locY + (double)var1.bm() - 1.100000023841858D;
      double var6 = var1.locX - this.locX;
      double var8 = var4 - var3.locY;
      double var10 = var1.locZ - this.locZ;
      float var12 = MathHelper.a(var6 * var6 + var10 * var10) * 0.2F;
      var3.c(var6, var8 + (double)var12, var10, 1.6F, 12.0F);
      this.a(class_ng.fE, 1.0F, 1.0F / (this.bE().nextFloat() * 0.4F + 0.8F));
      this.world.a((Entity)var3);
   }

   public float bm() {
      return 1.7F;
   }

   protected boolean a(EntityHuman var1, EnumHand var2, ItemStack var3) {
      if(var3 != null && var3.b() == Items.bl && !this.o() && !this.world.E) {
         this.a(true);
         var3.a(1, (class_rz)var1);
      }

      return super.a(var1, var2, var3);
   }

   public boolean o() {
      return (((Byte)this.datawatcher.a(a)).byteValue() & 16) != 0;
   }

   public void a(boolean var1) {
      byte var2 = ((Byte)this.datawatcher.a(a)).byteValue();
      if(var1) {
         this.datawatcher.b(a, Byte.valueOf((byte)(var2 | 16)));
      } else {
         this.datawatcher.b(a, Byte.valueOf((byte)(var2 & -17)));
      }

   }

   protected class_nf G() {
      return class_ng.fB;
   }

   protected class_nf bQ() {
      return class_ng.fD;
   }

   protected class_nf bR() {
      return class_ng.fC;
   }
}
