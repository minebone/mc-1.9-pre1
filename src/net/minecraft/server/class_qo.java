package net.minecraft.server;

import net.minecraft.server.EnumResult;

public class class_qo {
   private final EnumResult a;
   private final Object b;

   public class_qo(EnumResult var1, Object var2) {
      this.a = var1;
      this.b = var2;
   }

   public EnumResult a() {
      return this.a;
   }

   public Object b() {
      return this.b;
   }
}
