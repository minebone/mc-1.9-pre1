package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.EntityHuman;
import net.minecraft.server.Vec3D;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_vl;
import net.minecraft.server.class_wj;

public class class_uk extends class_tj {
   private class_wj a;
   private double b;
   private double c;
   private double d;
   private double e;

   public class_uk(class_wj var1, double var2) {
      this.a = var1;
      this.b = var2;
      this.a(1);
   }

   public boolean a() {
      if(!this.a.dc() && this.a.aI()) {
         Vec3D var1 = class_vl.a(this.a, 5, 4);
         if(var1 == null) {
            return false;
         } else {
            this.c = var1.b;
            this.d = var1.c;
            this.e = var1.d;
            return true;
         }
      } else {
         return false;
      }
   }

   public void c() {
      this.a.x().a(this.c, this.d, this.e, this.b);
   }

   public boolean b() {
      return !this.a.x().n() && this.a.aI();
   }

   public void e() {
      if(this.a.bE().nextInt(50) == 0) {
         Entity var1 = (Entity)this.a.bt().get(0);
         if(var1 == null) {
            return;
         }

         if(var1 instanceof EntityHuman) {
            int var2 = this.a.dq();
            int var3 = this.a.dw();
            if(var3 > 0 && this.a.bE().nextInt(var3) < var2) {
               this.a.g((EntityHuman)var1);
               this.a.world.a((Entity)this.a, (byte)7);
               return;
            }

            this.a.n(5);
         }

         this.a.ay();
         this.a.dE();
         this.a.world.a((Entity)this.a, (byte)6);
      }

   }
}
