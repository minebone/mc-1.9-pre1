package net.minecraft.server;

import com.google.common.collect.Sets;
import java.util.Set;
import net.minecraft.server.Bootstrap;
import net.minecraft.server.class_afd;
import net.minecraft.server.class_kk;

public class class_afg {
   private static final Set K;
   public static final class_afd a;
   public static final class_afd b;
   public static final class_afd c;
   public static final class_afd d;
   public static final class_afd e;
   public static final class_afd f;
   public static final class_afd g;
   public static final class_afd h;
   public static final class_afd i;
   public static final class_afd j;
   public static final class_afd k;
   public static final class_afd l;
   public static final class_afd m;
   public static final class_afd n;
   public static final class_afd o;
   public static final class_afd p;
   public static final class_afd q;
   public static final class_afd r;
   public static final class_afd s;
   public static final class_afd t;
   public static final class_afd u;
   public static final class_afd v;
   public static final class_afd w;
   public static final class_afd x;
   public static final class_afd y;
   public static final class_afd z;
   public static final class_afd A;
   public static final class_afd B;
   public static final class_afd C;
   public static final class_afd D;
   public static final class_afd E;
   public static final class_afd F;
   public static final class_afd G;
   public static final class_afd H;
   public static final class_afd I;
   public static final class_afd J;

   private static class_afd a(String var0) {
      class_afd var1 = (class_afd)class_afd.a.c(new class_kk(var0));
      if(!K.add(var1)) {
         throw new IllegalStateException("Invalid Potion requested: " + var0);
      } else {
         return var1;
      }
   }

   static {
      if(!Bootstrap.a()) {
         throw new RuntimeException("Accessed Potions before Bootstrap!");
      } else {
         K = Sets.newHashSet();
         a = a("empty");
         b = a("water");
         c = a("mundane");
         d = a("thick");
         e = a("awkward");
         f = a("night_vision");
         g = a("long_night_vision");
         h = a("invisibility");
         i = a("long_invisibility");
         j = a("leaping");
         k = a("long_leaping");
         l = a("strong_leaping");
         m = a("fire_resistance");
         n = a("long_fire_resistance");
         o = a("swiftness");
         p = a("long_swiftness");
         q = a("strong_swiftness");
         r = a("slowness");
         s = a("long_slowness");
         t = a("water_breathing");
         u = a("long_water_breathing");
         v = a("healing");
         w = a("strong_healing");
         x = a("harming");
         y = a("strong_harming");
         z = a("poison");
         A = a("long_poison");
         B = a("strong_poison");
         C = a("regeneration");
         D = a("long_regeneration");
         E = a("strong_regeneration");
         F = a("strength");
         G = a("long_strength");
         H = a("strong_strength");
         I = a("weakness");
         J = a("long_weakness");
         K.clear();
      }
   }
}
