package net.minecraft.server;

import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import net.minecraft.server.Block;
import net.minecraft.server.Chunk;
import net.minecraft.server.Entity;
import net.minecraft.server.EntityTypes;
import net.minecraft.server.FileIOThread;
import net.minecraft.server.NBTCompressedStreamTools;
import net.minecraft.server.NBTTag;
import net.minecraft.server.NBTTagCompound;
import net.minecraft.server.NBTTagList;
import net.minecraft.server.NibbleArray;
import net.minecraft.server.RegionFileCache;
import net.minecraft.server.TileEntity;
import net.minecraft.server.World;
import net.minecraft.server.class_ahm;
import net.minecraft.server.class_aht;
import net.minecraft.server.class_aid;
import net.minecraft.server.ChunkSection;
import net.minecraft.server.class_asl;
import net.minecraft.server.class_bbd;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_kk;
import net.minecraft.server.class_oy;
import net.minecraft.server.class_oz;
import net.minecraft.server.class_pb;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class class_asr implements class_asl, class_bbd {
   private static final Logger a = LogManager.getLogger();
   private Map b = new ConcurrentHashMap();
   private Set c = Collections.newSetFromMap(new ConcurrentHashMap());
   private final File d;
   private final class_pb e;
   private boolean f = false;

   public class_asr(File var1, class_pb var2) {
      this.d = var1;
      this.e = var2;
   }

   public Chunk a(World var1, int var2, int var3) throws IOException {
      class_ahm var4 = new class_ahm(var2, var3);
      NBTTagCompound var5 = (NBTTagCompound)this.b.get(var4);
      if(var5 == null) {
         DataInputStream var6 = RegionFileCache.c(this.d, var2, var3);
         if(var6 == null) {
            return null;
         }

         var5 = this.e.a((class_oy)class_oz.CHUNK, (NBTTagCompound)NBTCompressedStreamTools.a(var6));
      }

      return this.a(var1, var2, var3, var5);
   }

   protected Chunk a(World var1, int var2, int var3, NBTTagCompound var4) {
      if(!var4.b("Level", 10)) {
         a.error("Chunk file at " + var2 + "," + var3 + " is missing level data, skipping");
         return null;
      } else {
         NBTTagCompound var5 = var4.o("Level");
         if(!var5.b("Sections", 9)) {
            a.error("Chunk file at " + var2 + "," + var3 + " is missing block data, skipping");
            return null;
         } else {
            Chunk var6 = this.a(var1, var5);
            if(!var6.a(var2, var3)) {
               a.error("Chunk file at " + var2 + "," + var3 + " is in the wrong location; relocating. (Expected " + var2 + ", " + var3 + ", got " + var6.posX + ", " + var6.posZ + ")");
               var5.a("xPos", var2);
               var5.a("zPos", var3);
               var6 = this.a(var1, var5);
            }

            return var6;
         }
      }
   }

   public void a(World var1, Chunk var2) throws IOException, class_aht {
      var1.N();

      try {
         NBTTagCompound var3 = new NBTTagCompound();
         NBTTagCompound var4 = new NBTTagCompound();
         var3.a((String)"Level", (NBTTag)var4);
         var3.a("DataVersion", (int)165);
         this.a(var2, var1, var4);
         this.a(var2.k(), var3);
      } catch (Exception var5) {
         a.error((String)"Failed to save chunk", (Throwable)var5);
      }

   }

   protected void a(class_ahm var1, NBTTagCompound var2) {
      if(!this.c.contains(var1)) {
         this.b.put(var1, var2);
      }

      FileIOThread.a().a(this);
   }

   public boolean c() {
      if(this.b.isEmpty()) {
         if(this.f) {
            a.info("ThreadedAnvilChunkStorage ({}): All chunks are saved", new Object[]{this.d.getName()});
         }

         return false;
      } else {
         class_ahm var1 = (class_ahm)this.b.keySet().iterator().next();

         boolean var3;
         try {
            this.c.add(var1);
            NBTTagCompound var2 = (NBTTagCompound)this.b.remove(var1);
            if(var2 != null) {
               try {
                  this.b(var1, var2);
               } catch (Exception var7) {
                  a.error((String)"Failed to save chunk", (Throwable)var7);
               }
            }

            var3 = true;
         } finally {
            this.c.remove(var1);
         }

         return var3;
      }
   }

   private void b(class_ahm var1, NBTTagCompound var2) throws IOException {
      DataOutputStream var3 = RegionFileCache.d(this.d, var1.a, var1.b);
      NBTCompressedStreamTools.a((NBTTagCompound)var2, (DataOutput)var3);
      var3.close();
   }

   public void b(World var1, Chunk var2) throws IOException {
   }

   public void a() {
   }

   public void b() {
      try {
         this.f = true;

         while(true) {
            if(this.c()) {
               continue;
            }
         }
      } finally {
         this.f = false;
      }

   }

   private void a(Chunk var1, World var2, NBTTagCompound var3) {
      var3.a("xPos", var1.posX);
      var3.a("zPos", var1.posZ);
      var3.a("LastUpdate", var2.P());
      var3.a("HeightMap", var1.r());
      var3.a("TerrainPopulated", var1.u());
      var3.a("LightPopulated", var1.v());
      var3.a("InhabitedTime", var1.x());
      ChunkSection[] var4 = var1.getChunkSections();
      NBTTagList var5 = new NBTTagList();
      boolean var6 = !var2.s.isNotOverworld();
      ChunkSection[] var7 = var4;
      int var8 = var4.length;

      NBTTagCompound var11;
      for(int var9 = 0; var9 < var8; ++var9) {
         ChunkSection var10 = var7[var9];
         if(var10 != Chunk.a) {
            var11 = new NBTTagCompound();
            var11.a("Y", (byte)(var10.getYPosition() >> 4 & 255));
            byte[] var12 = new byte[4096];
            NibbleArray var13 = new NibbleArray();
            NibbleArray var14 = var10.getBlockPalette().a(var12, var13);
            var11.a("Blocks", var12);
            var11.a("Data", var13.a());
            if(var14 != null) {
               var11.a("Add", var14.a());
            }

            var11.a("BlockLight", var10.getEmittedLight().a());
            if(var6) {
               var11.a("SkyLight", var10.getSkyLight().a());
            } else {
               var11.a("SkyLight", new byte[var10.getEmittedLight().a().length]);
            }

            var5.a((NBTTag)var11);
         }
      }

      var3.a((String)"Sections", (NBTTag)var5);
      var3.a("Biomes", var1.l());
      var1.g(false);
      NBTTagList var17 = new NBTTagList();

      Iterator var19;
      for(var8 = 0; var8 < var1.t().length; ++var8) {
         var19 = var1.t()[var8].iterator();

         while(var19.hasNext()) {
            Entity var21 = (Entity)var19.next();
            var11 = new NBTTagCompound();
            if(var21.d(var11)) {
               var1.g(true);
               var17.a((NBTTag)var11);
            }
         }
      }

      var3.a((String)"Entities", (NBTTag)var17);
      NBTTagList var18 = new NBTTagList();
      var19 = var1.s().values().iterator();

      while(var19.hasNext()) {
         TileEntity var22 = (TileEntity)var19.next();
         var11 = new NBTTagCompound();
         var22.a(var11);
         var18.a((NBTTag)var11);
      }

      var3.a((String)"TileEntities", (NBTTag)var18);
      List var20 = var2.a(var1, false);
      if(var20 != null) {
         long var23 = var2.P();
         NBTTagList var24 = new NBTTagList();
         Iterator var25 = var20.iterator();

         while(var25.hasNext()) {
            class_aid var26 = (class_aid)var25.next();
            NBTTagCompound var15 = new NBTTagCompound();
            class_kk var16 = (class_kk)Block.h.b(var26.a());
            var15.a("i", var16 == null?"":var16.toString());
            var15.a("x", var26.a.p());
            var15.a("y", var26.a.q());
            var15.a("z", var26.a.r());
            var15.a("t", (int)(var26.b - var23));
            var15.a("p", var26.c);
            var24.a((NBTTag)var15);
         }

         var3.a((String)"TileTicks", (NBTTag)var24);
      }

   }

   private Chunk a(World var1, NBTTagCompound var2) {
      int var3 = var2.h("xPos");
      int var4 = var2.h("zPos");
      Chunk var5 = new Chunk(var1, var3, var4);
      var5.a(var2.n("HeightMap"));
      var5.d(var2.p("TerrainPopulated"));
      var5.e(var2.p("LightPopulated"));
      var5.c(var2.i("InhabitedTime"));
      NBTTagList var6 = var2.c("Sections", 10);
      byte var7 = 16;
      ChunkSection[] var8 = new ChunkSection[var7];
      boolean var9 = !var1.s.isNotOverworld();

      for(int var10 = 0; var10 < var6.c(); ++var10) {
         NBTTagCompound var11 = var6.b(var10);
         byte var12 = var11.f("Y");
         ChunkSection var13 = new ChunkSection(var12 << 4, var9);
         byte[] var14 = var11.m("Blocks");
         NibbleArray var15 = new NibbleArray(var11.m("Data"));
         NibbleArray var16 = var11.b("Add", 7)?new NibbleArray(var11.m("Add")):null;
         var13.getBlockPalette().a(var14, var15, var16);
         var13.setEmittedLight(new NibbleArray(var11.m("BlockLight")));
         if(var9) {
            var13.setSkyLight(new NibbleArray(var11.m("SkyLight")));
         }

         var13.recalcBlockCount();
         var8[var12] = var13;
      }

      var5.a(var8);
      if(var2.b("Biomes", 7)) {
         var5.a(var2.m("Biomes"));
      }

      NBTTagList var17 = var2.c("Entities", 10);
      if(var17 != null) {
         for(int var18 = 0; var18 < var17.c(); ++var18) {
            NBTTagCompound var20 = var17.b(var18);
            a(var20, var1, var5);
            var5.g(true);
         }
      }

      NBTTagList var19 = var2.c("TileEntities", 10);
      if(var19 != null) {
         for(int var21 = 0; var21 < var19.c(); ++var21) {
            NBTTagCompound var23 = var19.b(var21);
            TileEntity var25 = TileEntity.b(var1.u(), var23);
            if(var25 != null) {
               var5.a(var25);
            }
         }
      }

      if(var2.b("TileTicks", 9)) {
         NBTTagList var22 = var2.c("TileTicks", 10);
         if(var22 != null) {
            for(int var24 = 0; var24 < var22.c(); ++var24) {
               NBTTagCompound var26 = var22.b(var24);
               Block var27;
               if(var26.b("i", 8)) {
                  var27 = Block.b(var26.l("i"));
               } else {
                  var27 = Block.b(var26.h("i"));
               }

               var1.b(new BlockPosition(var26.h("x"), var26.h("y"), var26.h("z")), var27, var26.h("t"), var26.h("p"));
            }
         }
      }

      return var5;
   }

   public static Entity a(NBTTagCompound var0, World var1, Chunk var2) {
      Entity var3 = a(var0, var1);
      if(var3 == null) {
         return null;
      } else {
         var2.a(var3);
         if(var0.b("Passengers", 9)) {
            NBTTagList var4 = var0.c("Passengers", 10);

            for(int var5 = 0; var5 < var4.c(); ++var5) {
               Entity var6 = a(var4.b(var5), var1, var2);
               if(var6 != null) {
                  var6.a(var3, true);
               }
            }
         }

         return var3;
      }
   }

   public static Entity a(NBTTagCompound var0, World var1, double var2, double var4, double var6, boolean var8) {
      Entity var9 = a(var0, var1);
      if(var9 == null) {
         return null;
      } else {
         var9.b(var2, var4, var6, var9.yaw, var9.pitch);
         if(var8 && !var1.a(var9)) {
            return null;
         } else {
            if(var0.b("Passengers", 9)) {
               NBTTagList var10 = var0.c("Passengers", 10);

               for(int var11 = 0; var11 < var10.c(); ++var11) {
                  Entity var12 = a(var10.b(var11), var1, var2, var4, var6, var8);
                  if(var12 != null) {
                     var12.a(var9, true);
                  }
               }
            }

            return var9;
         }
      }
   }

   protected static Entity a(NBTTagCompound var0, World var1) {
      try {
         return EntityTypes.a(var0, var1);
      } catch (RuntimeException var3) {
         return null;
      }
   }

   public static void a(Entity var0, World var1) {
      if(var1.a(var0) && var0.aI()) {
         Iterator var2 = var0.bt().iterator();

         while(var2.hasNext()) {
            Entity var3 = (Entity)var2.next();
            a(var3, var1);
         }
      }

   }

   public static Entity a(NBTTagCompound var0, World var1, boolean var2) {
      Entity var3 = a(var0, var1);
      if(var3 == null) {
         return null;
      } else if(var2 && !var1.a(var3)) {
         return null;
      } else {
         if(var0.b("Passengers", 9)) {
            NBTTagList var4 = var0.c("Passengers", 10);

            for(int var5 = 0; var5 < var4.c(); ++var5) {
               Entity var6 = a(var4.b(var5), var1, var2);
               if(var6 != null) {
                  var6.a(var3, true);
               }
            }
         }

         return var3;
      }
   }
}
