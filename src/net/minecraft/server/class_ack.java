package net.minecraft.server;

import net.minecraft.server.EntityHuman;
import net.minecraft.server.World;
import net.minecraft.server.class_adj;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ng;
import net.minecraft.server.EnumSoundCategory;
import net.minecraft.server.MathHelper;
import net.minecraft.server.class_rz;

public class class_ack extends class_adj {
   public class_ack(int var1, float var2) {
      super(var1, var2, false);
   }

   public ItemStack a(ItemStack var1, World var2, class_rz var3) {
      ItemStack var4 = super.a(var1, var2, var3);
      if(!var2.E) {
         double var5 = var3.locX;
         double var7 = var3.locY;
         double var9 = var3.locZ;

         for(int var11 = 0; var11 < 16; ++var11) {
            double var12 = var3.locX + (var3.bE().nextDouble() - 0.5D) * 16.0D;
            double var14 = MathHelper.a(var3.locY + (double)(var3.bE().nextInt(16) - 8), 0.0D, (double)(var2.Z() - 1));
            double var16 = var3.locZ + (var3.bE().nextDouble() - 0.5D) * 16.0D;
            if(var3.k(var12, var14, var16)) {
               var2.a((EntityHuman)null, var5, var7, var9, class_ng.ad, EnumSoundCategory.PLAYERS, 1.0F, 1.0F);
               var3.a(class_ng.ad, 1.0F, 1.0F);
               break;
            }
         }

         if(var3 instanceof EntityHuman) {
            ((EntityHuman)var3).cZ().a(this, 20);
         }
      }

      return var4;
   }
}
