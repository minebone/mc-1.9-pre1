package net.minecraft.server;

import net.minecraft.server.ChatComponentText;
import net.minecraft.server.ChatMessage;
import net.minecraft.server.CommandAbstract;
import net.minecraft.server.CommandObjectiveExecutor;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.class_bz;
import net.minecraft.server.ICommandListener;

public class CommandList extends CommandAbstract {
   public String c() {
      return "list";
   }

   public int a() {
      return 0;
   }

   public String b(ICommandListener var1) {
      return "commands.players.usage";
   }

   public void a(MinecraftServer var1, ICommandListener var2, String[] var3) throws class_bz {
      int var4 = var1.H();
      var2.a(new ChatMessage("commands.players.list", new Object[]{Integer.valueOf(var4), Integer.valueOf(var1.I())}));
      var2.a(new ChatComponentText(var1.getPlayerList().b(var3.length > 0 && "uuids".equalsIgnoreCase(var3[0]))));
      var2.a(CommandObjectiveExecutor.EnumCommandResult.QUERY_RESULT, var4);
   }
}
