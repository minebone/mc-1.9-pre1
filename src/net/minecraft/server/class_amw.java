package net.minecraft.server;

import java.util.Random;
import net.minecraft.server.AxisAlignedBB;
import net.minecraft.server.BlockStateInteger;
import net.minecraft.server.BlockStateList;
import net.minecraft.server.Blocks;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.Item;
import net.minecraft.server.Items;
import net.minecraft.server.MaterialMapColor;
import net.minecraft.server.World;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_ahw;
import net.minecraft.server.class_ajx;
import net.minecraft.server.IBlockData;
import net.minecraft.server.IBlockState;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;

public class class_amw extends class_ajx {
   public static final BlockStateInteger a = BlockStateInteger.a("age", 0, 3);
   private static final AxisAlignedBB[] c = new AxisAlignedBB[]{new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.3125D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.5D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.6875D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.875D, 1.0D)};

   protected class_amw() {
      super(Material.k, MaterialMapColor.D);
      this.w(this.A.b().set(a, Integer.valueOf(0)));
      this.a(true);
      this.a((CreativeModeTab)null);
   }

   public AxisAlignedBB a(IBlockData var1, class_ahw var2, BlockPosition var3) {
      return c[((Integer)var1.get(a)).intValue()];
   }

   protected boolean i(IBlockData var1) {
      return var1.getBlock() == Blocks.aW;
   }

   public boolean f(World var1, BlockPosition var2, IBlockData var3) {
      return this.i(var1.getType(var2.b()));
   }

   public void b(World var1, BlockPosition var2, IBlockData var3, Random var4) {
      int var5 = ((Integer)var3.get(a)).intValue();
      if(var5 < 3 && var4.nextInt(10) == 0) {
         var3 = var3.set(a, Integer.valueOf(var5 + 1));
         var1.a((BlockPosition)var2, (IBlockData)var3, 2);
      }

      super.b(var1, var2, var3, var4);
   }

   public void a(World var1, BlockPosition var2, IBlockData var3, float var4, int var5) {
      if(!var1.E) {
         int var6 = 1;
         if(((Integer)var3.get(a)).intValue() >= 3) {
            var6 = 2 + var1.r.nextInt(3);
            if(var5 > 0) {
               var6 += var1.r.nextInt(var5 + 1);
            }
         }

         for(int var7 = 0; var7 < var6; ++var7) {
            a((World)var1, (BlockPosition)var2, (ItemStack)(new ItemStack(Items.bF)));
         }

      }
   }

   public Item a(IBlockData var1, Random var2, int var3) {
      return null;
   }

   public int a(Random var1) {
      return 0;
   }

   public ItemStack a(World var1, BlockPosition var2, IBlockData var3) {
      return new ItemStack(Items.bF);
   }

   public IBlockData a(int var1) {
      return this.u().set(a, Integer.valueOf(var1));
   }

   public int e(IBlockData var1) {
      return ((Integer)var1.get(a)).intValue();
   }

   protected BlockStateList b() {
      return new BlockStateList(this, new IBlockState[]{a});
   }
}
