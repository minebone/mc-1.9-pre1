package net.minecraft.server;

import net.minecraft.server.Entity;
import net.minecraft.server.Items;
import net.minecraft.server.ItemStack;
import net.minecraft.server.class_agn;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_qk;
import net.minecraft.server.EnumInventorySlot;
import net.minecraft.server.class_sc;
import net.minecraft.server.class_tj;
import net.minecraft.server.class_wj;
import net.minecraft.server.class_wl;
import net.minecraft.server.class_xz;
import net.minecraft.server.class_yv;

public class class_wm extends class_tj {
   private final class_wj a;

   public class_wm(class_wj var1) {
      this.a = var1;
   }

   public boolean a() {
      return this.a.world.a(this.a.locX, this.a.locY, this.a.locZ, 10.0D);
   }

   public void e() {
      class_qk var1 = this.a.world.D(new BlockPosition(this.a));
      this.a.x(false);
      this.a.a(class_wl.SKELETON);
      this.a.o(true);
      this.a.b_(0);
      this.a.world.d((Entity)(new class_xz(this.a.world, this.a.locX, this.a.locY, this.a.locZ, true)));
      class_yv var2 = this.a(var1, this.a);
      var2.m(this.a);

      for(int var3 = 0; var3 < 3; ++var3) {
         class_wj var4 = this.a(var1);
         class_yv var5 = this.a(var1, var4);
         var5.m(var4);
         var4.g(this.a.bE().nextGaussian() * 0.5D, 0.0D, this.a.bE().nextGaussian() * 0.5D);
      }

   }

   private class_wj a(class_qk var1) {
      class_wj var2 = new class_wj(this.a.world);
      var2.a((class_qk)var1, (class_sc)null);
      var2.b(this.a.locX, this.a.locY, this.a.locZ);
      var2.noDamageTicks = 60;
      var2.cL();
      var2.a(class_wl.SKELETON);
      var2.o(true);
      var2.b_(0);
      var2.world.a((Entity)var2);
      return var2;
   }

   private class_yv a(class_qk var1, class_wj var2) {
      class_yv var3 = new class_yv(var2.world);
      var3.a((class_qk)var1, (class_sc)null);
      var3.b(var2.locX, var2.locY, var2.locZ);
      var3.noDamageTicks = 60;
      var3.cL();
      if(var3.a((EnumInventorySlot)EnumInventorySlot.HEAD) == null) {
         var3.a(EnumInventorySlot.HEAD, new ItemStack(Items.aa));
      }

      class_agn.a(var3.bE(), var3.ca(), (int)(5.0F + var1.c() * (float)var3.bE().nextInt(18)), false);
      class_agn.a(var3.bE(), var3.a((EnumInventorySlot)EnumInventorySlot.HEAD), (int)(5.0F + var1.c() * (float)var3.bE().nextInt(18)), false);
      var3.world.a((Entity)var3);
      return var3;
   }
}
