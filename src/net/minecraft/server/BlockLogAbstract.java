package net.minecraft.server;

import java.util.Iterator;
import net.minecraft.server.BlockLeaves;
import net.minecraft.server.BlockStateEnum;
import net.minecraft.server.CreativeModeTab;
import net.minecraft.server.EnumDirection;
import net.minecraft.server.World;
import net.minecraft.server.class_aoc;
import net.minecraft.server.class_aod;
import net.minecraft.server.class_aoo;
import net.minecraft.server.IBlockData;
import net.minecraft.server.Material;
import net.minecraft.server.BlockPosition;
import net.minecraft.server.class_or;
import net.minecraft.server.class_rz;

public abstract class BlockLogAbstract extends class_aoc {
   public static final BlockStateEnum a = BlockStateEnum.a("axis", BlockLogAbstract.EnumLogRotation.class);

   public BlockLogAbstract() {
      super(Material.d);
      this.a(CreativeModeTab.b);
      this.c(2.0F);
      this.a(class_aoo.a);
   }

   public void b(World var1, BlockPosition var2, IBlockData var3) {
      byte var4 = 4;
      int var5 = var4 + 1;
      if(var1.a(var2.a(-var5, -var5, -var5), var2.a(var5, var5, var5))) {
         Iterator var6 = BlockPosition.a(var2.a(-var4, -var4, -var4), var2.a(var4, var4, var4)).iterator();

         while(var6.hasNext()) {
            BlockPosition var7 = (BlockPosition)var6.next();
            IBlockData var8 = var1.getType(var7);
            if(var8.getMaterial() == Material.j && !((Boolean)var8.get(BlockLeaves.b)).booleanValue()) {
               var1.a((BlockPosition)var7, (IBlockData)var8.set(BlockLeaves.b, Boolean.valueOf(true)), 4);
            }
         }

      }
   }

   public IBlockData a(World var1, BlockPosition var2, EnumDirection var3, float var4, float var5, float var6, int var7, class_rz var8) {
      return this.a(var7).set(a, BlockLogAbstract.EnumLogRotation.a(var3.k()));
   }

   public IBlockData a(IBlockData var1, class_aod var2) {
      switch(BlockLogAbstract.SyntheticClass_1.b[var2.ordinal()]) {
      case 1:
      case 2:
         switch(BlockLogAbstract.SyntheticClass_1.a[((BlockLogAbstract.EnumLogRotation)var1.get(a)).ordinal()]) {
         case 1:
            return var1.set(a, BlockLogAbstract.EnumLogRotation.Z);
         case 2:
            return var1.set(a, BlockLogAbstract.EnumLogRotation.X);
         default:
            return var1;
         }
      default:
         return var1;
      }
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] a;
      // $FF: synthetic field
      static final int[] b;
      // $FF: synthetic field
      static final int[] c = new int[EnumDirection.class_a_in_class_cq.values().length];

      static {
         try {
            c[EnumDirection.class_a_in_class_cq.X.ordinal()] = 1;
         } catch (NoSuchFieldError var7) {
            ;
         }

         try {
            c[EnumDirection.class_a_in_class_cq.Y.ordinal()] = 2;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            c[EnumDirection.class_a_in_class_cq.Z.ordinal()] = 3;
         } catch (NoSuchFieldError var5) {
            ;
         }

         b = new int[class_aod.values().length];

         try {
            b[class_aod.COUNTERCLOCKWISE_90.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            b[class_aod.CLOCKWISE_90.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         a = new int[BlockLogAbstract.EnumLogRotation.values().length];

         try {
            a[BlockLogAbstract.EnumLogRotation.X.ordinal()] = 1;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            a[BlockLogAbstract.EnumLogRotation.Z.ordinal()] = 2;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum EnumLogRotation implements class_or {
      X("x"),
      Y("y"),
      Z("z"),
      NONE("none");

      private final String e;

      private EnumLogRotation(String var3) {
         this.e = var3;
      }

      public String toString() {
         return this.e;
      }

      public static BlockLogAbstract.EnumLogRotation a(EnumDirection.class_a_in_class_cq var0) {
         switch(BlockLogAbstract.SyntheticClass_1.c[var0.ordinal()]) {
         case 1:
            return X;
         case 2:
            return Y;
         case 3:
            return Z;
         default:
            return NONE;
         }
      }

      public String m() {
         return this.e;
      }
   }
}
